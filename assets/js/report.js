// report.js (assets/js/report.js)
// Author: Indra Prastha

function display_report(url,method){
	var frm = document.getElementById('form_report');
	if (!frm) {
		console.log('The javascript @ report.js is looking for form element with id "form_report" and could not find it'); 
		return;
	}
	method = (method) ? method : 'default';
	
	frm.action = url + '/' + method;
	
	if (method == 'print' || method == 'email'){
		var report_window = window.open('','report_window','location=no,menubar=no,toolbar=no,scrollbars=yes,width=800,height=500,top=100,left=100',true);
		frm.target = 'report_window';
	}
	
	frm.submit();
	frm.target = ''; // reset the form target
	report_window.focus();
}