var table_selection = {
	init: function(name,type){
		this.last_selrow = 0;
		this.name = name;
		this.trs = document.getElementById(name).tBodies[0].getElementsByTagName('tr');
		this.type = type;
		var that = this;
		$(document).on('click','#'+this.name+ ' tbody > tr',function(event){
			that.handle_click(this.rowIndex,event);
		});
	},	
	select_range: function(indexes,e){
		indexes.sort(function(a, b) {
			return a - b;
		});	
		var f
		for (var i = indexes[0]+1; i <= indexes[1]-1; i++) {
			this.toggleRow(i,e,true);
		}
		this.toggleRow(indexes[1],e,false);
	},	
	handle_click: function(row,e){ 
		if (window.event.ctrlKey) {
			this.toggleRow(row,event,false);
		}	
		if (window.event.button === 0) {
			if (!window.event.ctrlKey && !window.event.shiftKey) {
				this.toggleRow(row,event,false);
			}		
			if (window.event.shiftKey) {
				this.select_range([this.last_selrow, row],event)
			}
		}
	},
	clearAll: function(){
		for (var i = 0; i <this.trs.length; i++) {
			this.trs[i].className = '';
		}
	},
	toggleRow: function(row,e,range){	
		this.last_selrow = row;
		
		$(this.trs[row-1]).toggleClass('selected');
		if(e.target.type !== 'checkbox' || range){			
			$(':checkbox',this.trs[row-1]).prop('checked',!$(':checkbox',this.trs[row-1]).prop('checked'));
		}
	}
}
