var rowCount = 1;
var default_row = $("#default");

$(document).ready(function(){
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			//sky_tbl_refresh_row_num('table_detail');
			//sky_count_col_total('table_detail','qty_total','ast_qty[]');
			table_doc.row_index();
			table_doc.col();
			table_doc.extra();
			table_doc.sum();
		}
	});	
	
	<!---INIT--->
	$("#location_id").val(1).change();
	sky_tbl_refresh_row_num("table_detail");	
	pop_vendor();
	//pop_purchase_receipt();
	//pop_purchase_invoice();
	//$("#table_detail").colResizable();
	table_doc.init('table_detail',default_row);
	table_doc.init_row('sub_total','ast_qty','','ast_price');
	//table_doc.init_col('amount_total','sub_total[]');
	table_doc.init_col('qty_total','ast_qty[]');
	table_doc.row_index();
	
	//IF WITH RCV PARAMETER
	if(param != ""){
		var url_header, url_detail;		
		url_detail = ajax_url_2;
		url_header = ajax_url_3;	
		$.ajax({
			url:url_detail,
			data:{doc_num:param},
			success: function(response) {
				table_detail_insert(JSON.parse(response));			
			}
		});
		var param_header = $.ajax({
			url:url_header,
			data:{doc_num:param},
			success: function(response){
				form_header_insert(JSON.parse(response));
			}
		});

	}
	
});

function validateForm(){
	if(confirm("Are you sure?")==true){
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}


//JS CUSTOM PAGE
function form_header_insert(data_header){ console.log('header');
	$("#buss_name").val(data_header[0].buss_name);
	$("#buss_id").val(data_header[0].buss_id);
	$("#rcv_doc_ref").val(data_header[0].doc_num);
	$("#doc_ref_id").val(data_header[0].doc_id);
	$("#doc_note").val(data_header[0].doc_note);
}
function table_detail_reset(table){ 
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(default_row);
}
function table_detail_insert(data){		//console.log('detail');
	if(data){
		//delete first row
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		}
		for(var p = 0;p<data.length;p++){
			var row = clone_row("table_detail");		
			row.find('td:eq(1)').find('input[name^=ast_code]').val(data[p]['item_code']);
			row.find('td:eq(1)').find('input[type=hidden]').val(data[p]['item_id']);
			row.find('td:eq(2)').find('input').val(data[p]['item_name_po']);
			row.find('td:eq(3)').find('input[name^=ast_qty]').val(0);//data[p]['item_qty']
			row.find('td:eq(3)').find('input[name^=ast_qty_open]').val(data[p]['item_qty']-data[p]['item_qty_closed']);
			row.find('td:eq(4)').find('input[name^=ast_price]').val(data[p]['item_netprice']);
			row.find('td:eq(5)').find('input[name^=sub_total]').val(data[p]['item_total']);
			row.find('input[name^=reff_line]').val(data[p]['doc_line']);
			//row.find('td:eq(6)').find('input').val(data[p]['item_qty']-data[p]['item_qty_closed']);
			//sky_count_row_total('table_detail',row,'ast_qty','ast_price','sub_total');	
		}
		table_doc.row();
		table_doc.clone();
		table_doc.row_del_first();
		//var row = clone_row("table_detail");
		//delete first for after insert
		/*if($('#table_detail > tbody > tr:first').find('td:eq(1) > input').val() == ""){
			$('#table_detail > tbody > tr:first').remove();
		}*/
		table_doc.row_index();
		//sky_tbl_refresh_row_num("table_detail");
		table_doc.col();
		//sky_count_col_total('table_detail','amount_total','sub_total[]')
	}else{
		console.log('error');	
	}	
}

////////POP FUNCTION
////VENDOR
function pop_vendor(){ 
	dialog_vendor_reset_table('dialog_vendor_list');
	dialog_vendor_populate_data('dialog_vendor_list',source_vendor);
}
////PO
function pop_purchase_invoice(){
	return $.ajax({
		url:ajax_url_4,
		data:{buss_id:$("#buss_id").val(),logistic_status:'RECEIVED',billing_status:'UNBILLED'},
		success: function(response) { //console.log(response);
			dialog_purchase_invoice_reset_table('dialog_purchase_invoice_list');
			dialog_purchase_invoice_populate_data('dialog_purchase_invoice_list',JSON.parse(response));
        }
	});
}
function pop_purchase_receipt(){
	return $.ajax({
		url:ajax_url_1,
		data:{buss_id:$("#buss_id").val()},
		success: function(response) { //console.log(response);
			dialog_purchase_receipt_reset_table('dialog_purchase_receipt_list');
			dialog_purchase_receipt_populate_data('dialog_purchase_receipt_list',JSON.parse(response));
        }
	});
}


////////PULL DATA
////VENDOR
function dialog_vendor_pull_data(data_vendor){
	$("#buss_name").val(data_vendor['buss_name']);
	$("#buss_id").val(data_vendor['buss_id']);
	$("#buss_char").val(data_vendor['buss_char']).change();
	$("#bill_addr").val(data_vendor['buss_addr']+' '+data_vendor['buss_city']+' '+data_vendor['buss_country']);
	pop_purchase_receipt();
	pop_purchase_invoice();
}
////PO
function dialog_purchase_receipt_pull_data(data_purchase_order){
	$("#doc_ref_id").val(data_purchase_order.doc_id);
	$("#doc_ref").val(data_purchase_order.doc_num);
	
	$("#rcv_doc_ref_id").val(data_purchase_order.doc_id);
	$("#rcv_doc_ref").val(data_purchase_order.doc_num);
	//$("#po_doc_ref_id").val();
	$("#po_doc_ref").val(data_purchase_order.doc_ref);
	get_rcv_detail();	
}
/*function dialog_purchase_order_pull_data(data_purchase_order){
	$("#doc_ref_id").val(data_purchase_order.doc_id);
	$("#doc_ref").val(data_purchase_order.doc_num);
	
	$("#po_doc_ref_id").val(data_purchase_order.doc_id);
	$("#po_doc_ref").val(data_purchase_order.doc_num);
	//$("#rcv_doc_ref_id").val("");
	//$("#rcv_doc_ref").val("");
	get_po_detail();	
}*/