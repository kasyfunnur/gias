var rowCount = 1;
var default_row = $("#default");
function clone_row(table){
	rowCount++;
	var row = $("#"+table+" > tbody > tr:first").clone().find("input,select").each(function(){		
		var temp_name = $(this).attr('name');
		$(this).attr('id',function(_,id){return temp_name.substring(0,temp_name.length-2)+rowCount;});
		$(this).attr('name',function(_,name){return name;});
		$(this).val('');
	}).end().appendTo("#"+table+" > tbody");
	sky_tbl_refresh_row_num(table);
	return row;
}
function sky_tbl_refresh_row_num(tableid){
	$('#'+tableid+' > tbody > tr').each(function(){
		var t_idx = $(this).index()+1;
		$(this).find('td:eq(0) > input[name="row[]"]').val(t_idx);
	});	
}
function sky_count_row_total(table,obj,qty,price,total){	
	$("#"+table+" > tbody > tr").each(function(){
		var name = $(this).find('td:eq(0)').find('input').attr('name');
		var id = $(this).find('td:eq(0)').find('input').attr('id');
		var cnt = id.substring(name.length-2,name.length);
		var temp_qty = $("input[id^='"+qty+cnt+"']").val();
		var temp_price = $("input[id^='"+price+cnt+"']").val();
		var temp_total = temp_qty * temp_price;
		$("input[id^='"+total+cnt+"']").val(temp_total);
	});
}
function sky_count_col_total(table,target,col){
	var tbl 	= $("#"+table+ " > tbody");
	var tbl_row = tbl[0].rows.length;
	var col_arr = $("input[name='"+col+"']");
	var total 	= 0;
	for(var q = 0; q < tbl_row-1; q++){
		total += parseInt($(col_arr[q]).val());
	}
	$("#"+target).val(total);	
}

$(document).ready(function(){
	$("input,select,textarea,button").attr('readonly','readonly');
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			sky_tbl_refresh_row_num('table_detail');
			sky_count_col_total('table_detail','doc_total','sub_total[]');
		}
	});	
	$(document).on('change','#table_detail > tbody > tr:last',function(){
		clone_row('table_detail');
		sky_tbl_refresh_row_num('table_detail');
	});
	//calc total per line
	$(document).on("change","input[name^='ast_qty'], input[name^='ast_price']",function(){
		sky_count_row_total('table_detail','','ast_qty','ast_price','sub_total');	
		sky_count_col_total('table_detail','doc_total','sub_total[]');
	});
	
	//INIT
	$("#location_id").val(1).change();	
	sky_tbl_refresh_row_num("table_detail");
	pop_inventory();
	//$("#table_detail").colResizable();
	
	if(form_detail){
		table_detail_insert(form_detail);
	}
	
});

function validateForm(){
	if(confirm("Are you sure?")==true){
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}


//JS CUSTOM PAGE
function table_detail_reset(){
	$("#table_detail > tbody > tr").not('tr:last').remove();
	sky_tbl_refresh_row_num("table_detail");
	//$('#table_detail > tbody').append(po_default_row);
}
function table_detail_insert(data){		
	if(data){
		//delete first row
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		}
		for(var p = 0;p<data.length;p++){console.log(data[p]);
			var row = clone_row("table_detail");		
			row.find('input[name^=item_code]').val(data[p]['item_code']);
			row.find('input[name^=item_name]').val(data[p]['item_name']);
			row.find('input[name^=item_qty]').val(data[p]['item_qty']);
			//sky_count_row_total('table_detail',row,'ast_qty','ast_price','sub_total');	
		}
		var row = clone_row("table_detail");
		//delete first for after insert
		if($('#table_detail > tbody > tr:first').find('td:eq(1) > input').val() == ""){
			$('#table_detail > tbody > tr:first').remove();
		}
		sky_tbl_refresh_row_num("table_detail");
		sky_count_col_total('table_detail','doc_total','sub_total[]')
	}else{
		console.log('error');	
	}	
}
function pop_inventory(){
	dialog_inventory_reset_table('dialog_inventory_list');
	dialog_inventory_populate_data('dialog_inventory_list',source_inventory);
}

function dialog_inventory_pull_data(data_inventory){ //console.log(data_inventory);
	var data_inventory_param = $.map(data_inventory, function(value, index) {
		return [value];
	});
	var data_ajax_raw = $.ajax({
		url:ajax_url_1,
		data: {inventory:data_inventory_param},
		dataType: "json",
		success: function(result){
			//dialog_purchase_order_reset_table('table_detail');
			table_detail_reset();
			table_detail_insert(result);			
		}
	});	
}