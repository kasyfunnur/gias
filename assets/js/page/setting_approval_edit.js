$(function() {
    load_approval_detail();
});

/**
 *
 * Load approval detail (json encoded in views/approval_edit)
 * into tbody
 */
function load_approval_detail() {
    if (approval_detail) {
        for (var i = 0; i < approval_detail.length; i++) {
            add_detail(
                approval_detail[i]['requestor_position_id'],
                approval_detail[i]['auto_approve'],
                approval_detail[i]['step_based'],
                approval_detail[i]['approval_matrix']
            );
        }

    }
}

/**
 *
 * Delete entire row relative to the link clicked
 *
 */
function delete_detail(link_object) {
    if (confirm('Delete this setting row?')) {
        $(link_object).closest('tr').empty();
    }
}


/**
 *
 * Clear approver matrix relative to the link clicked
 *
 */
function clear_matrix(link_object) {
    if (confirm('Clear Matrix settings?')) {
        var default_matrix = create_select(
            positions,
            'matrix[]',
            'position_id',
            function(data) {
                return '(' + data.division_name + ') - ' + data.position_code + ' : ' + data.position_name;
            },
            0,
            'matrix-object');
        $(link_object).parent().siblings('.approval_matrix_td').empty().append(default_matrix);
    }
}


/**
 *
 * Add row approval setting detail
 * based on given attributes
 */
function add_detail(requestor_position_id, auto_approve, step_based, matrix) {
    detailrow = '<tr>';
    detailrow += '<td>' +
        create_select(
            positions,
            'sel_requester[]',
            'position_id',
            function(data) {
                return '(' + data.division_name + ') - ' + data.position_code + ' : ' + data.position_name;
            },
            requestor_position_id) + '</td>';
    auto_approve = (auto_approve && auto_approve == 1) ? 'selected' : '';
    detailrow += '<td>' + '<select name="sel_auto_approve[]" class="yesno"><option value="0">No</option><option value="1" '+auto_approve+'>Yes</option></select>' + '</td>';
    step_based = (step_based && step_based == 1) ? 'selected' : '';
    detailrow += '<td>' + '<select name="sel_step_based[]" class="yesno"><option value="0">No</option><option value="1" '+step_based+'>Yes</option></select>' + '</td>';
    detailrow += '<td class="approval_matrix_td">' + translate_matrix(matrix) + '</td>';
    detailrow += '<td>' +
        '<a class="btn btn-info btn-xs" onclick="matrix_add(\'or\',this);"> / ( OR )</a>&nbsp;&nbsp;' +
        '<a class="btn btn-primary btn-xs" onclick="matrix_add(\'and\',this);"> + ( AND )</a><br>' +
        '<a class="btn btn-danger btn-xs" onclick="delete_detail(this);">delete row</a>&nbsp;&nbsp;' +
        '<a class="btn btn-warning btn-xs" onclick="clear_matrix(this);">clear matrix</a>' +
        '</td>';
    detailrow += '</tr>';
    $('#approve_detail_tbody').append(detailrow);
}


/**
 *
 * Triger from "/ (OR)" and "+ (AND)"
 * buttons
 */
function matrix_add(operator, link_object) {
    var matrix_select = '';
    switch (operator) {
        case 'or':
            matrix_select += '<code class="matrix-object">/</code>';
            break;

        case 'and':
            matrix_select += '<br><code class="matrix-object">+</code><br>';
            break;
    }
    matrix_select += create_select(
        positions,
        'matrix[]',
        'position_id',
        function(data) {
            return '(' + data.division_name + ') - ' + data.position_code + ' : ' + data.position_name;
        },
        0,
        'matrix-object');
    $(link_object).parent().siblings('.approval_matrix_td').append(matrix_select);

}

/**
 *
 * translate raw matrix data (3+2/2...) into html selects
 *
 */
function translate_matrix(matrix) {
    if (matrix == '')
        return create_select(
            positions,
            'matrix[]',
            'position_id',
            function(data) {
                return '(' + data.division_name + ') - ' + data.position_code + ' : ' + data.position_name;
            },
            0,
            'matrix-object');

    var vertical, horizontal, html;

    html = '';
    vertical = matrix.split('+');

    for (var i = 0; i < vertical.length; i++) {
        if (i != 0) {
            html += '<br><code class="matrix-object">+</code><br>';
        }
        horizontal = vertical[i].split('/');

        for (var h = 0; h < horizontal.length; h++) {
            if (h != 0) {
                html += '<code class="matrix-object">/</code>';
            }
            html += create_select(
                positions,
                'matrix[]',
                'position_id',
                function(data) {
                    return '(' + data.division_name + ') - ' + data.position_code + ' : ' + data.position_name;
                },
                horizontal[h],
                'matrix-object');
        }
    }

    return html;
}

/**
*
* Encode matrix approval select boxes into matrix code (2+3/2...)
* Passed argument is the td containing the select and code or/and objects
**/
function encode_matrix(td_object)
{
    var tag, encoded;
    encoded = '';
    $(td_object).children('.matrix-object').each(function (index){
        tag = $(this).prop("tagName");
        if(tag == 'CODE'){
            encoded += $(this).text();
        } else {
            encoded += $(this).val();
        }

    });
    return encoded;
}



/**
 *
 * Takes a datasource (array of objects or json encoded)
 * and create select element with the given attributes
 * label_render argument takes a function with the parameter
 * (data) which contains the row's object data
 */
function create_select(datasource, name, value_field, label_render, selected_value, classname) {
    classname = (classname) ? classname : '';
    var selecthtml = '<select name="' + name + '" class="'+classname+'">';
    selecthtml += '<option value="0">-- Select Position --</option>';
    for (var i = 0; i < datasource.length; i++) {
        selected = (selected_value && datasource[i][value_field] == selected_value) ? 'selected' : '';
        selecthtml += '<option value="' + datasource[i][value_field] + '" ' + selected + '>'
        selecthtml += label_render(datasource[i]);
        selecthtml += '</option>';
    };

    selecthtml += '</select>';

    return selecthtml;
}


/**
*
* FormValidation on Submit
*
**/
function FormValidation()
{
    // todo : validate form
    // check if any position select box is set at value = 0 ("-- please select --")
    var emptyposition = false;
    $('select:not(.yesno)').each(function(index){
        if($(this).val() == 0){
            alert('Please select position');
            $(this).focus();
            emptyposition = true;
            return false;
        }
    });
    if(emptyposition){
        return false;
    }


    // finaly encode matrix 
    var matrixdata;
    $('.approval_matrix_td').each( function (index){
        matrixdata = encode_matrix(this);
        $(this).append('<input type="hidden" name="hdn_matrix[]" value="'+matrixdata+'">');
    });

    if(confirm('Save setting?')){
        return true;
    } else {
        return false;
    }
}