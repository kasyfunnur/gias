var table = $('#voucher_list').dataTable({
	'ajax': {	
		'url'  : '../sales/ajax_voucher_list',
		'data' :  function (d) {d.parent = $('#parent').val();}
	},
	"columnDefs": [ {
      "targets": 1,
      "render": function ( data, type, full, meta ) {
      		console.log(full);
	      return '<a href="./voucher_edit/'+full[0]+'">'+data+'</a>';
	    }
    } ]
});
