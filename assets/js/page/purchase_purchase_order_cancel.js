var default_row = $("#default");

$(document).ready(function(){	
  	
	//INIT
	
	table_doc.init('table_detail',default_row);
	table_doc.init_row('sub_total','ast_qty','','ast_price');
	table_doc.init_col('doc_subtotal','sub_total[]');
	table_doc.init_col('qty_total','ast_qty[]');
	table_doc.row_index();

	$(".control, .form-control").attr('disabled',true);
	table_detail_insert(form_detail);
});

//JS CUSTOM PAGE
function table_detail_insert(data){		
	if(data){
		table_doc.row_del_last();
		for(var p = 0;p<data.length;p++){
			var row = table_doc.clone();		
			row.find('a[name^=a_ast_code]').attr('href',url_1+'/'+data[p]["item_code"]);
			row.find('input[name^=ast_code]').val(data[p]['item_code']);
			row.find('input[name^=item_id]').val(data[p]['item_id']);
			row.find('input[name^=ast_name]').val(data[p]['item_name_po']);
			row.find('input[name^=ast_qty]').val(data[p]['item_qty']);
			row.find('input[name^=ast_price]').val(data[p]['item_price']);			
		}
		table_doc.row();
		table_doc.clone();	
		table_doc.row_del_first('item_id');	
		table_doc.row_index();
		table_doc.col();		
		table_doc.sum();		
	}else{
		console.log('error');	
	}	
}
