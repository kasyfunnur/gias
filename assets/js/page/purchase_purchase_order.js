$(function(){
	/* SEARCH FUNCTION */	
	$("#search_field").on("keyup", function() {
		var value = $(this).val();
		var cols = $("#purchase_order_index").find("tr:first th").length;
		$("#purchase_order_index tbody tr").each(function(index) { 
			var show = 0;
			$row = $(this);
			for(var x = 0;x<=cols;x++){
				if($row.hasClass('selected')){show++; break;}
				if($row.find("td:eq("+x+")").find('span.searchable').text().toLowerCase().indexOf(value.toLowerCase()) !== -1){
					show++; break;
				}
			}
			(show>0) ? $row.show(): $row.hide();		
		});
		
	});
	/* SEARCH FUNCTION */	
});