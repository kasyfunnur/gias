/**
*
* Asset new depreciation page js
*
**/
var table;

$(function() {
	table = $('#depreciation').dataTable( {
			"processing" : true,
			"serverSide" : false,
			"ajax" : {
				url: './get_asset_depre/',
				type: 'POST',
				data: function(d){ d.depr_date = $('#depreciation_date').val(); }
			},
			"columns":[
				{"data" : "asset_label"},
				{"data" : "asset_name"},
				{"data" : "group_name"},
				{"data" : "location_name"},
				{"data" : "asset_currency"},
				{"data" : "asset_value", "className" : "text-right"},
				{"data" : "monthly_depre_value", "className" : "text-right"},
				{"data" : 0, "className" : "text-right"}
			],
			"columnDefs": [ 
				{
					"targets" : [5,6], // current value and depre amount
					"render"  : function (data, type, full, meta){
						return accounting.format(data);
					}
				},
				{
			    "targets": 7, // last column 'value after depre'
			    "render": function ( data, type, full, meta ) {
			      return accounting.formatMoney('',full.asset_value - full.monthly_depre_value,',','.');
			    }
			}]
	} );
});

function refresh_asset_data(){
	table.api().ajax.reload();
	/*var table = $('#depreciation').DataTable({
			"ajax" : {
				url: './get_asset_depre/',
				type: 'POST',
				data: {'depr_date' : $('#depreciation_date').val()}
			}});
	table.ajax.reload();
	*/
}

function get_asset_list()
{
	$.ajax({
		url: './get_asset_depre/',
		type: 'POST',
		data: {'depr_date' : $('#depreciation_date').val()}
	})
	.done(function(data) {
		$('#tbody_depreciation').empty();
		for(var i = 0; i < data.length; i++){
			/*add_asset_row(
				data[i].asset_label,
				data[i].asset_name,
				data[i].group_name,
				data[i].location_name,
				data[i].asset_currency,
				data[i].asset_value,
				data[i].monthly_depre_value);
			*/
		}
		

	})
	.fail(function() {
		console.log("error fetching asset");
	});
	
}

function add_asset_row(label, name, group, location, currency, current_value, depre_amount)
{
	var rowDetail = '<tr>'
		+ '<td>' + label + '</td>'
		+ '<td>' + name + '</td>'
		+ '<td>' + group + '</td>'
		+ '<td>' + location + '</td>'
		+ '<td>' + currency + '</td>'
		+ '<td align="right">' + accounting.format(current_value) + '</td>'
		+ '<td align="right">' + depre_amount + '</td>'
		+ '<td align="right">' + Number(Number(current_value) - Number(depre_amount)).toString() + '</td>'
		+ '</tr>'
	$('#tbody_depreciation').append(rowDetail);
}
