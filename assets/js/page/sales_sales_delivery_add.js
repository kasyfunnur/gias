var rowCount = 1;
var default_row = $("#default");
function sky_count_doc_summary(){
	var var_subtotal 	= $("#doc_subtotal").val();
	var pct 			= $("#doc_disc_pct").val(); //console.log(pct);
	//var var_disc		= $("#doc_disc").val((pct/100)*$("#doc_total").val());
	$("#doc_disc").val((pct/100)*$("#doc_subtotal").val());
	var var_disc 		= $("#doc_disc").val();
	var var_total		= $("#doc_total").val(var_subtotal - var_disc);
}

$(document).ready(function(){
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			sky_tbl_refresh_row_num('table_detail');
			sky_count_col_total('table_detail','qty_total','ast_qty[]');
		}
	});	
	$(document).on('change','#table_detail > tbody > tr:last',function(){
		clone_row('table_detail');
		sky_tbl_refresh_row_num('table_detail');
	});
	$(document).on('change','#table_expense > tbody > tr:last',function(){
		clone_row('table_expense');
		sky_tbl_refresh_row_num('table_expense');
	});
	//calc total per line
	$(document).on("change","input[name^='ast_qty'], input[name^='ast_price']",function(){
		sky_count_row_total('table_detail','','ast_qty','ast_price','sub_total');	
		sky_count_col_total('table_detail','qty_total','ast_qty[]');
	});
	
	$("#freight_accrue_party").hide();
	$("#other_accrue_party").hide();
	$(document).on('change','#freight_type',function(event){
		if(event.target.value == "own_accrue"){
			$("#freight_accrue_party").show();			
		}else{
			$("#freight_accrue_party").hide();
		}
	});
	$(document).on('change','#other_type',function(event){
		if(event.target.value == "own_accrue"){
			$("#other_accrue_party").show();			
		}else{
			$("#other_accrue_party").hide();
		}
	});

	$("#location_id").on('change',function(){
		var result;
		for( var i = 0; i < source_location.length; i++ ) {
			if( source_location[i]['location_id'] == $(this).val() ) {
				result = source_location[i];
				break;
				$("#ship_addr").val(result['location_address']+', '+result['location_city']+', '+result['location_state']+', '+result['location_country']);
			}else{
				$("#ship_addr").val("");
			}
		}	
		/*$("#ship_addr").val(
			result['location_address']+', '+
			result['location_city']+', '+
			result['location_state']+', '+
			result['location_country']);*/
	});
	
	<!---INIT--->
	$("#location_id").val(1).change();
	sky_tbl_refresh_row_num("table_detail");	
	pop_customer();
	//$("#table_detail").colResizable();
	
	//IF WITH PO PARAMETER
	if(param != ""){
		$.ajax({
			url:ajax_url_2,
			data:{doc_num:param},
			success: function(response) {
				data = JSON.parse(response);
				table_detail_insert(data);			
			}
		});
		var param_header = $.ajax({
			url:ajax_url_3,
			data:{doc_num:param},
			success: function(response){
				form_header_insert(JSON.parse(response));
			}
		});
	}
	
	//JQ New Location
	$("#location_id").on('change',function(){
		if($(this).val()== -1){ //if new location
			//void window.showModalDialog('<?php echo site_url("purchase/dialog_sales_order/");?>/customer/'+document.getElementById('buss_id').value);
		}
	});	
});

function validateForm(){
	if(confirm("Are you sure?")==true){
		sky_count_row_total('table_detail','','ast_qty','ast_price','sub_total');	
		sky_count_col_total('table_detail','doc_subtotal','sub_total[]');
		sky_count_col_total('table_detail','qty_total','ast_qty[]');
		sky_count_doc_summary();
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}


//JS CUSTOM PAGE
function form_header_insert(data_header){ 
	$("#buss_name").val(data_header[0].buss_name);
	$("#buss_id").val(data_header[0].buss_id);
	$("#doc_ref").val(data_header[0].doc_num);
	$("#doc_ref_id").val(data_header[0].doc_id);
	$("#doc_note").val(data_header[0].doc_note);
	$("#ship_addr").val(data_header[0].ship_addr);
	$("#cust_phone1").val(data_header[0].doc_buss_tlp1);
	$("#cust_phone2").val(data_header[0].doc_buss_tlp2);
	$("#doc_ddt").val(data_header[0].doc_ddt.substr(0,10));
	$("#doc_disc_pct").val(data_header[0].doc_disc_percent);
}
function table_detail_reset(table){
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(default_row);
}
function table_detail_insert(data){		
	if(data){
		//delete first row
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		}
		for(var p = 0;p<data.length;p++){
			var row = clone_row("table_detail");		
			row.find('td:eq(1)').find('input[name^=ast_code]').val(data[p]['item_code']);
			row.find('td:eq(1)').find('input[type=hidden]').val(data[p]['item_id']);
			row.find('td:eq(2)').find('input').val(data[p]['item_name_so']);
			row.find('td:eq(3)').find('input[name^=ast_so_qty]').val(data[p]['item_qty']);
			row.find('td:eq(3)').find('input[name^=ast_price]').val(data[p]['item_netprice']);
			row.find('td:eq(3)').find('input[name^=ast_qty_open]').val(data[p]['item_qty']-data[p]['item_qty_closed']);
			row.find('td:eq(4)').find('input').val(data[p]['item_qty']-data[p]['item_qty_closed']);
			row.find('input[name^=reff_line]').val(data[p]['doc_line']);
			sky_count_row_total('table_detail',row,'ast_qty','ast_price','sub_total');	
		}
		var row = clone_row("table_detail");
		//delete first for after insert
		if($('#table_detail > tbody > tr:first').find('td:eq(1) > input').val() == ""){
			$('#table_detail > tbody > tr:first').remove();
		}
		sky_tbl_refresh_row_num("table_detail");
		sky_count_col_total('table_detail','qty_total','ast_qty[]')
	}else{
		console.log('error');	
	}	
}

////////POP FUNCTION
////customer
function pop_customer(){ 
	dialog_customer_reset_table('dialog_customer_list');
	dialog_customer_populate_data('dialog_customer_list',source_customer);
}
////PO
function pop_sales_order(){
	var where = {};
	where = {
		0: {
			'field'	: 'doc_status',
			'value'	: 'ACTIVE'
		}
	};
	return $.ajax({
		url:ajax_url_1,
		//data:{buss_id:$("#buss_id").val(),logistic_status:"PENDING"},
		data:{buss_id:$("#buss_id").val(),where:where},
		success: function(response) {
			dialog_sales_order_reset_table('dialog_sales_order_list');
			dialog_sales_order_populate_data('dialog_sales_order_list',JSON.parse(response));
        }
	});
}


////////PULL DATA
////customer
function dialog_customer_pull_data(data_customer){
	$("#buss_name").val(data_customer['buss_name']);
	$("#buss_id").val(data_customer['buss_id']);
	$("#buss_char").val(data_customer['buss_char']).change();
	$("#bill_addr").val(data_customer['buss_addr']+' '+data_customer['buss_city']+' '+data_customer['buss_country']);
	pop_sales_order();
}
////PO
function dialog_sales_order_pull_data(data_sales_order){
	var param_header = $.ajax({
		url:ajax_url_3,
		data:{doc_num:data_sales_order.doc_num},
		success: function(response){
			form_header_insert(JSON.parse(response));
		}
	});
	
	param_header.done(function(){		
		get_so_detail();		
	});
}
////INVENTORY
function dialog_inventory_pull_data(data_inventory){	
	var data_inventory_param = $.map(data_inventory, function(value, index) {
		return [value];
	});
	var data_ajax_raw = $.ajax({
		url:ajax_url_1,
		data: {inventory:data_inventory_param},
		dataType: "json",
		success: function(result){				
			table_detail_insert(result);			
		}
	});	
}