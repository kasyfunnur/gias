var table = $('#move_request').dataTable({
		'ajax': {	'url'  : 'ajax_datatable/move_request',
					'data' :  function (d) {
                    d.location_id    = $('#location_id').val();
                    d.assets_checked = $('#assets_checked').val();
                    }
				}
                });
$('#location_id').change(function(){table.api().ajax.reload()});
function populate(){
    vals = $('input[type="checkbox"]:checked').map(function() {
        return this.value;
    }).get().join(',');
    $('#assets_checked').val(vals);}