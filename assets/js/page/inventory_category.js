/**
*
* For this instance, we are not using server side processing 
* since the data will not scale as much as regular data.
* In general.js, data table defaults are set to server side processing
* here we set back to false for server side processing
* 
**/

$('#category_list').DataTable({'processing':false, 'serverSide':false});