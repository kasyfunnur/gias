$(document).ready(function(){
	display_extra_field();
	fillLabel($('#sub_group_id').val());
	});

$('#sub_group_id').change(function(){
	display_extra_field();
	fillLabel($(this).val());
	});

function display_extra_field(){
	var group_id   = $('#sub_group_id').val();
	$('.custom_group').hide();	
	$('#custom_group_'+group_id).show();
	}

function fillLabel(group_id){
	$.get(("get_next_label/"+group_id), function(result) {
    	$('#label').val(result);
		});
	}

function updateSubGroup(selectedGroup){
	subGroupList.options.length=0;
		for (i=0; i<sub_group[selectedGroup].length; i++)
		subGroupList.options[i] = new Option(sub_group[selectedGroup][i].split("|")[1], sub_group[selectedGroup][i].split("|")[0]);
		fillLabel($('#sub_group_id').val());
	}