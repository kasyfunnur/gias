var default_row = $("#default");

$(document).ready(function(){
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			table_doc.row_index();
			table_doc.col();
			table_doc.sum();
		}
	});	

	//INIT
	$("#location_id").val(1).change();	
	pop_inventory();
	table_doc.init('table_detail',default_row);	
	table_doc.row_index();
});

/*function validateForm(){
	if(confirm("Are you sure?")==true){
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}*/


//JS CUSTOM PAGE
function table_detail_reset(){
	$("#table_detail > tbody > tr").not('tr:last').remove();
	table_doc.row_index();
}
function table_detail_insert(data){		
	if(data){		//console.log(data);
		table_doc.row_del_last();
		for(var p = 0;p<data.length;p++){
			var row = table_doc.clone(); 
			row.find('input[name^=item_id]').val(data[p]['item_id']);
			row.find('input[name^=ast_code]').val(data[p]['item_code']);
			row.find('input[name^=item_name]').val(data[p]['item_name']);
			row.find('input[name^=item_qty]').val(1);
		}
		table_doc.row();
		table_doc.clone();
		table_doc.row_del_first('ast_code');
		table_doc.row_index();
		table_doc.col();		
		table_doc.sum();
	}else{
		console.log('error');	
	}	
}
function pop_inventory(){
	dialog_inventory_reset_table('dialog_inventory_list');
	dialog_inventory_populate_data('dialog_inventory_list',source_inventory);
}

function dialog_inventory_pull_data(data_inventory){ 
	var data_inventory_param = $.map(data_inventory, function(value, index) {
		return [value];
	});
	var data_ajax_raw = $.ajax({
		url:ajax_url_1,
		data: {inventory:data_inventory_param},
		dataType: "json",
		success: function(result){
			//dialog_purchase_order_reset_table('table_detail');
			//table_detail_reset();
			table_detail_insert(result);		
		}
	});	
}