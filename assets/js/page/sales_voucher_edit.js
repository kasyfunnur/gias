function FormValidation()
{

	if(new Date($('#active_date').val()) > new Date($('#expiry_date').val())){
		alert('Expiry date should be greater than Active Date');
		return false;
	}

	return true;
}

function void_voucher()
{
	if(confirm('Void remaining unused vouchers?')){
		window.location = window.location + '/true';
	}
}