var rowCount = 1;
var default_row = $("#default");

$(document).ready(function(){
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			sky_tbl_refresh_row_num('table_detail');
			sky_count_col_total('table_detail','doc_total','sub_total[]');
			sky_count_col_total('table_detail','qty_total','ast_qty[]');
		}
	});	
	$(document).on('change','#table_detail > tbody > tr:last',function(){
		clone_row('table_detail');
		sky_tbl_refresh_row_num('table_detail');
	});
	//calc total per line
	$(document).on("change","input[name^='ast_qty'], input[name^='ast_price']",function(){
		sky_count_row_total('table_detail','','ast_qty','ast_price','sub_total');	
		sky_count_col_total('table_detail','doc_total','sub_total[]');
		sky_count_col_total('table_detail','qty_total','ast_qty[]');
	});

	$("#location_id").on('change',function(){
		var result;
		for( var i = 0; i < source_location.length; i++ ) {
			if( source_location[i]['location_id'] == $(this).val() ) {
				result = source_location[i];
				break;
				$("#ship_addr").val(result['location_address']+', '+result['location_city']+', '+result['location_state']+', '+result['location_country']);
			}else{
				$("#ship_addr").val("");
			}
		}	
		$("#ship_addr").val(
			result['location_address']+', '+
			result['location_city']+', '+
			result['location_state']+', '+
			result['location_country']);
	});
	
	
	//INIT
	$("#location_id").val(1).change();	
	sky_tbl_refresh_row_num("table_detail");
	pop_customer();pop_sales_order();
	//pop_inventory();
	
	//ON EDIT PAGE
	$("#location_id").val(form_header[0]['whse_id']).change();
	$("#buss_id").val(form_header[0]['buss_id']);
	$("#buss_name").val(form_header[0]['buss_name']);
	$("#doc_ref").val(form_header[0]['doc_ref']);
	$("#doc_note").val(form_header[0]['doc_note']);
	$("#qty_total").val();
	$("#exp_freight").val(form_header[0]['doc_cost_freight']);
	$("#exp_other").val(form_header[0]['doc_cost_other']);
	$("#cust_phone1").val(form_header[0]['doc_buss_tlp1']);
	$("#cust_phone2").val(form_header[0]['doc_buss_tlp2']);
	$("#ship_addr").val(form_header[0]['ship_addr']);
	$("#doc_freight").val(form_header[0]['doc_freight']);
	
	//if(status_logistic != "PENDING"){
		$('#table_detail > tbody > tr > td > input').attr('disabled',true);
		$('#table_detail > tbody > tr > td > input').css('color','#000');
		$('#table_detail > thead > tr').find('th:eq(6)').hide();
		$('#table_detail > tbody > tr').find('td:eq(6)').hide();
		$('#modal_item,#modal_customer').attr('disabled',true);
		$('#modal_item,#modal_customer').css('color','#000');
		$('#buss_name,#location_id,#doc_dt,#doc_ddt').attr('disabled',true);
		$('#buss_name,#location_id,#doc_dt,#doc_ddt').css('color','#000');
		//$('button[type="submit"]').attr('disabled',true);
		$('input[type="number"]').attr('type','text');
		$('button[name="act_del"]').attr('disabled',true);
	//	}

	if(form_detail){ 
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		} 
		for(var p = 0;p<form_detail.length;p++){
			var row = clone_row("table_detail");		
			row.find('td:eq(1)').find('input[name^=ast_code]').val(form_detail[p]['item_code']);
			row.find('td:eq(1)').find('input[name^=item_id]').val(form_detail[p]['item_id']);
			//row.find('td:eq(2)').find('input').val(form_detail[p]['item_name']);
			row.find('td:eq(2)').find('input').val(form_detail[p]['item_name']);
			row.find('td:eq(3)').find('input').val(form_detail[p]['item_qty']);
			//row.find('td:eq(4)').find('input').val(form_detail[p]['item_sell_price']);
			row.find('td:eq(4)').find('input').val(form_detail[p]['item_qty']);
			sky_count_row_total('table_detail',row,'ast_qty','ast_price','sub_total');	
			//var new_row = $('#table_detail > tbody > tr:last').remove();
		}
		var row = clone_row("table_detail");
		if($('#table_detail > tbody > tr:first').find('td:eq(1) > input').val() == ""){
			$('#table_detail > tbody > tr:first').remove();
		}
		sky_tbl_refresh_row_num("table_detail");
		sky_count_col_total('table_detail','doc_total','sub_total[]');
		sky_count_col_total('table_detail','qty_total','ast_qty[]');
	}else{
		console.log('error');	
	}
});

function validateForm(){
	if(confirm("Are you sure?")==true){
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){	
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}


//JS CUSTOM PAGE
function dialog_inventory_insert_data(data){		
	if(data){
		//delete first row
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		}
		for(var p = 0;p<data.length;p++){
			var row = clone_row("table_detail");		
			row.find('td:eq(1)').find('input[name^=ast_code]').val(data[p]['item_code']);
			row.find('td:eq(1)').find('input[type=hidden]').val(data[p]['item_id']);
			row.find('td:eq(2)').find('input').val(data[p]['item_name']);
			row.find('td:eq(3)').find('input').val(1);
			row.find('td:eq(4)').find('input').val(data[p]['item_buy_price']);
			sky_count_row_total('table_detail',row,'ast_qty','ast_price','sub_total');	
		}
		var row = clone_row("table_detail");
		//delete first for after insert
		if($('#table_detail > tbody > tr:first').find('td:eq(1) > input').val() == ""){
			$('#table_detail > tbody > tr:first').remove();
		}
		sky_tbl_refresh_row_num("table_detail");
		sky_count_col_total('table_detail','doc_total','sub_total[]')
	}else{
		console.log('error');	
	}	
}

function table_detail_reset(table){
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(default_row);
}
function table_detail_insert(data){		
	if(data){
		//delete first row
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		}
		for(var p = 0;p<data.length;p++){
			var row = clone_row("table_detail");		
			row.find('td:eq(1)').find('input[name^=ast_code]').val(data[p]['item_code']);
			row.find('td:eq(1)').find('input[type=hidden]').val(data[p]['item_id']);
			row.find('td:eq(2)').find('input').val(data[p]['item_name_so']);
			row.find('td:eq(3)').find('input[name^=ast_so_qty]').val(data[p]['item_qty']);
			row.find('td:eq(3)').find('input[name^=ast_price]').val(data[p]['item_netprice']);
			row.find('td:eq(3)').find('input[name^=ast_qty_open]').val(data[p]['item_qty']-data[p]['item_qty_closed']);
			row.find('td:eq(4)').find('input').val(data[p]['item_qty']-data[p]['item_qty_closed']);
			row.find('input[name^=reff_line]').val(data[p]['doc_line']);
			sky_count_row_total('table_detail',row,'ast_qty','ast_price','sub_total');	
		}
		var row = clone_row("table_detail");
		//delete first for after insert
		if($('#table_detail > tbody > tr:first').find('td:eq(1) > input').val() == ""){
			$('#table_detail > tbody > tr:first').remove();
		}
		sky_tbl_refresh_row_num("table_detail");
		sky_count_col_total('table_detail','qty_total','ast_qty[]')
	}else{
		console.log('error');	
	}	
}

////////POP FUNCTION
////customer
function pop_customer(){	
	dialog_customer_reset_table('dialog_customer_list');
	dialog_customer_populate_data('dialog_customer_list',source_customer);
}
////PO
function pop_sales_order(){
	return $.ajax({
		url:ajax_url_1,
		data:{buss_id:$("#buss_id").val()},
		success: function(response) {
			dialog_sales_order_reset_table('dialog_sales_order_list');
			dialog_sales_order_populate_data('dialog_sales_order_list',JSON.parse(response));
        }
	});
}

////////PULL DATA
////customer
function dialog_customer_pull_data(data_customer){
	$("#buss_name").val(data_customer['buss_name']);
	$("#buss_id").val(data_customer['buss_id']);
	$("#buss_char").val(data_customer['buss_char']).change();
	$("#bill_addr").val(data_customer['buss_addr']+' '+data_customer['buss_city']+' '+data_customer['buss_country']);
	pop_sales_order();
}
////PO
function dialog_sales_order_pull_data(data_sales_order){
	$("#doc_ref").val(data_sales_order.doc_num);
	$("#doc_ref_id").val(data_sales_order.doc_id);
	get_so_detail();	
}
////INVENTORY
function dialog_inventory_pull_data(data_inventory){	
	var data_inventory_param = $.map(data_inventory, function(value, index) {
		return [value];
	});
	var data_ajax_raw = $.ajax({
		//url:"<?php echo base_url('inventory/ajax_inventory_info');?>/"+data_inventory[i],
		url:ajax_url_1,
		data: {inventory:data_inventory_param},
		dataType: "json",
		success: function(result){				
			dialog_inventory_insert_data(result);			
		}
	});	
}