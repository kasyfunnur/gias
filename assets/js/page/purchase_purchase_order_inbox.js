var default_row = $("#default");

$(document).ready(function(){	
  	//$(".button_action").html($('template.button_action').html());
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			table_doc.row_index();
			table_doc.col();
			table_doc.sum();
		}
	});	

	$("#location_id").on('change',function(){
		var result;
		for( var i = 0; i < source_location.length; i++ ) {
			if( source_location[i]['location_id'] == $(this).val() ) {
				result = source_location[i];
				break;
			}else{
				$("#ship_addr").val("");
			}
		}
	});
	
	//INIT
	//$("#location_id").val(1).change();	
	//pop_vendor();
	//pop_inventory();
	table_doc.init('table_detail',default_row);
	table_doc.init_row('sub_total','ast_qty','','ast_price');
	table_doc.init_col('doc_subtotal','sub_total[]');
	table_doc.init_col('qty_total','ast_qty[]');
	table_doc.row_index();
	
	//ON EDIT PAGE
	$("#location_id").val(form_header[0]['whse_id']).change(); 
	$("#buss_id").val(form_header[0]['buss_id']);
	$("#buss_name").val(form_header[0]['buss_name']);
	$("#doc_ref").val(form_header[0]['doc_num']);
	$("#doc_note").val(form_header[0]['doc_note']);
	$("#doc_disc_pct").val(form_header[0]['doc_disc_percent']);
	$("#doc_pct").val(form_header[0]['doc_disc_amount']);
	
	
	
	$('a,input,button,select,textarea').attr('disabled',true);
	$('a.appr').attr('disabled',false);

	//console.log(form_detail);
	table_detail_insert(form_detail);
});

//JS CUSTOM PAGE
function table_detail_reset(){
	$("#table_detail > tbody > tr").not('tr:last').remove();
	table_doc.row_index();
}
function table_detail_insert(data){		
	if(data){
		table_doc.row_del_last();
		for(var p = 0;p<data.length;p++){
			var row = table_doc.clone();		
			row.find('a[name^=a_ast_code]').attr('href',url_1+'/'+data[p]["item_code"]);
			row.find('input[name^=ast_code]').val(data[p]['item_code']);
			row.find('input[name^=item_id]').val(data[p]['item_id']);
			row.find('input[name^=ast_name]').val(data[p]['item_name_po']);
			row.find('input[name^=ast_qty]').val(data[p]['item_qty']);
			row.find('input[name^=ast_price]').val(data[p]['item_price']);			
		}
		table_doc.row();
		table_doc.clone();	
		table_doc.row_del_first('item_id');	
		table_doc.row_index();
		table_doc.col();		
		table_doc.sum();		
	}else{
		console.log('error');	
	}	
}

function pop_inventory(){
	dialog_inventory_reset_table('dialog_inventory_list');
	dialog_inventory_populate_data('dialog_inventory_list',source_inventory);
}

function pop_vendor(){	
	dialog_vendor_reset_table('dialog_vendor_list');
	dialog_vendor_populate_data('dialog_vendor_list',source_vendor);
}

function dialog_vendor_pull_data(data_vendor){
	$("#buss_name").val(data_vendor['buss_name']);
	$("#buss_id").val(data_vendor['buss_id']);
	$("#buss_char").val(data_vendor['buss_char']).change();
	$("#bill_addr").val(data_vendor['buss_addr']+' '+data_vendor['buss_city']+' '+data_vendor['buss_country']);
}
function dialog_inventory_pull_data(data_inventory){	
	var data_inventory_param = $.map(data_inventory, function(value, index) {
		return [value];
	});
	var data_ajax_raw = $.ajax({
		//url:"<?php echo base_url('inventory/ajax_inventory_info');?>/"+data_inventory[i],
		url:ajax_url_1,
		data: {inventory:data_inventory_param},
		dataType: "json",
		success: function(result){				
			//dialog_inventory_insert_data(result);			
			table_detail_insert(result);
		}
	});	
}