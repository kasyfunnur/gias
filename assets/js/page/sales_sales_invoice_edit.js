function sky_count_doc_summary(){
	var var_subtotal 	= $("#doc_subtotal").val() || 0;
	var pct 			= $("#doc_disc_pct").val() || 0;
	//var var_disc		= $("#doc_disc").val((pct/100)*$("#doc_total").val());
	$("#doc_disc").val((pct/100)*$("#doc_subtotal").val());
	
	var var_disc 		= $("#doc_disc").val() || 0;	
	var var_freight		= $("#exp_freight").val() || 0;
	var var_other		= $("#exp_other").val() || 0;	
	var temp_total 		= parseFloat(var_subtotal) - parseFloat(var_disc) + parseFloat(var_freight) + parseFloat(var_other);
	var var_total		= $("#doc_total").val(temp_total);
}

$(document).ready(function(){
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			sky_tbl_refresh_row_num('table_detail');
			sky_count_col_total('table_detail','qty_total','ast_qty[]');
			sky_count_col_total('table_detail','qty_total','ast_qty[]');
			sky_count_doc_summary();
		}
	});	
	$(document).on('change','#table_detail > tbody > tr:last',function(){
		clone_row('table_detail');
		sky_tbl_refresh_row_num('table_detail');
	});
	$(document).on('change','#table_expense > tbody > tr:last',function(){
		clone_row('table_expense');
		sky_tbl_refresh_row_num('table_expense');
	});
	//calc total per line
	$(document).on("change","input[name^='ast_qty'], input[name^='ast_price']",function(){
		sky_count_row_total('table_detail','','ast_qty','ast_price','sub_total');	
		sky_count_col_total('table_detail','qty_total','ast_qty[]');
		sky_count_doc_summary();
	});
	
	$(document).on('change','#doc_disc_pct',function(){
		sky_count_doc_summary();
	});

	<!---INIT--->
	$("#location_id").val(1).change();
	sky_tbl_refresh_row_num("table_detail");	
	pop_customer();
	//$("#table_detail").colResizable();
	
	//IF WITH del PARAMETER
	/*if(param != ""){
		$.ajax({
			url:ajax_url_2,
			data:{doc_num:param},
			success: function(response) {
				data = JSON.parse(response);
				table_detail_insert(data);			
			}
		});
		var param_header = $.ajax({
			url:ajax_url_3,
			data:{doc_num:param},
			success: function(response){
				form_header_insert(JSON.parse(response));
			}
		});
	}*/
	
	//ON EDIT PAGE
	console.log(form_header);
	$("#location_id").val(form_header[0]['whse_id']).change();
	$("#buss_id").val(form_header[0]['buss_id']);
	$("#buss_name").val(form_header[0]['buss_name']);
	$("#doc_ref").val(form_header[0]['doc_ref']);
	$("#doc_note").val(form_header[0]['doc_note']);
	$("#doc_disc_pct").val(form_header[0]['doc_disc_percent']);
	$("#doc_pct").val(form_header[0]['doc_disc_amount']);
	$("#exp_freight").val(form_header[0]['doc_cost_freight']);
	$("#exp_other").val(form_header[0]['doc_cost_other']);	
	
	table_detail_insert(form_detail);
	/*if(form_detail){
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		}
		for(var p = 0;p<form_detail.length;p++){
			var row = clone_row("table_detail");		
			row.find('input[name^=ast_code]').val(form_detail[p]['item_code']);
			row.find('input[type=hidden]').val(form_detail[p]['item_id']);
			row.find('input[name^=ast_name]').val(form_detail[p]['item_name_so']);
			row.find('input[name^=ast_qty]').val(form_detail[p]['item_qty']);
			row.find('input[name^=ast_price]').val(form_detail[p]['item_price']);
			sky_count_row_total('table_detail',row,'ast_qty','ast_price','sub_total');	
			//var new_row = $('#table_detail > tbody > tr:last').remove();
		}
		var row = clone_row("table_detail");
		if($('#table_detail > tbody > tr:first').find('td:eq(1) > input').val() == ""){			
			$('#table_detail > tbody > tr:first').remove();
		}
		sky_tbl_refresh_row_num("table_detail");
		sky_count_col_total('table_detail','doc_subtotal','sub_total[]');
		sky_count_col_total('table_detail','qty_total','ast_qty[]');
		sky_count_doc_summary();		
	}else{
		console.log('error');	
	}*/
	
	//JQ New Location
	$("#location_id").on('change',function(){
		if($(this).val()== -1){ //if new location
			//void window.showModalDialog('<?php echo site_url("sales/dialog_sales_order/");?>/customer/'+document.getElementById('buss_id').value);
		}
	});	
});

function validateForm(){
	if(confirm("Are you sure?")==true){
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}


//JS CUSTOM PAGE
function form_header_insert(data_header){
	//$("#buss_name").val(data_header[0].buss_name);
	$("#buss_id").val(data_header[0].buss_id);
	$("#doc_ref").val(data_header[0].doc_num);
	$("#doc_ref_id").val(data_header[0].doc_id);
	$("#doc_note").val(data_header[0].doc_note);
	$("#doc_disc_pct").val(data_header[0].doc_disc_percent);
	$("#exp_freight").val(data_header[0].doc_cost_freight);
	$("#exp_other").val(data_header[0].doc_cost_other);	
	/*$("#buss_name").val(data_header.buss_name);
	$("#buss_id").val(data_header.buss_id);
	$("#doc_ref").val(data_header.doc_num);
	$("#doc_ref_id").val(data_header.doc_id);
	$("#doc_note").val(data_header.doc_note);
	$("#doc_disc_pct").val(data_header.doc_disc_percent);
	$("#exp_freight").val(data_header.doc_cost_freight);
	$("#exp_other").val(data_header.doc_cost_other);*/
}
function table_detail_reset(table){
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(default_row);
}
function table_detail_insert(data){		
	if(data){
		//delete first row
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		}
		for(var p = 0;p<data.length;p++){
			var row = clone_row("table_detail");		
			row.find('td:eq(1)').find('input[name^=ast_code]').val(data[p]['item_code']);
			row.find('td:eq(1)').find('input[type=hidden]').val(data[p]['item_id']);
			row.find('td:eq(2)').find('input').val(data[p]['item_name_so']);
			row.find('td:eq(3)').find('input[name^=ast_qty]').val(data[p]['item_qty']);
			row.find('td:eq(3)').find('input[name^=ast_qty_open]').val(data[p]['item_qty']-data[p]['item_qty_closed']);
			row.find('td:eq(4)').find('input[name^=ast_price]').val(data[p]['item_netprice']);
			row.find('td:eq(5)').find('input[name^=sub_total]').val(data[p]['item_total']);
			row.find('input[name^=reff_line]').val(data[p]['doc_line']);
			//row.find('td:eq(6)').find('input').val(data[p]['item_qty']-data[p]['item_qty_closed']);
			sky_count_row_total('table_detail',row,'ast_qty','ast_price','sub_total');	
		}
		var row = clone_row("table_detail");
		//delete first for after insert
		if($('#table_detail > tbody > tr:first').find('td:eq(1) > input').val() == ""){
			$('#table_detail > tbody > tr:first').remove();
		}
		sky_tbl_refresh_row_num("table_detail");
		//sky_count_col_total('table_detail','amount_total','sub_total[]');
		sky_count_col_total('table_detail','doc_subtotal','sub_total[]');
		sky_count_col_total('table_detail','qty_total','ast_qty[]');
		sky_count_doc_summary();
	}else{
		console.log('error');	
	}	
}

////////POP FUNCTION
////CUSTOMER
function pop_customer(){ 
	dialog_customer_reset_table('dialog_customer_list');
	dialog_customer_populate_data('dialog_customer_list',source_customer);
}
////SO
function pop_sales_order(){
	return $.ajax({
		url:ajax_url_4,
		data:{buss_id:$("#buss_id").val(),logistic_status:'DELIVERED',billing_status:'UNBILLED'},
		success: function(response) { //console.log(response);
			dialog_sales_order_reset_table('dialog_sales_order_list');
			dialog_sales_order_populate_data('dialog_sales_order_list',JSON.parse(response));
        }
	});
}
function pop_sales_delivery(){
	return $.ajax({
		url:ajax_url_1,
		data:{buss_id:$("#buss_id").val()},
		success: function(response) { //console.log(response);
			dialog_sales_delivery_reset_table('dialog_sales_delivery_list');
			dialog_sales_delivery_populate_data('dialog_sales_delivery_list',JSON.parse(response));
        }
	});
}


////////PULL DATA
////customer
function dialog_customer_pull_data(data_customer){
	$("#buss_name").val(data_customer['buss_name']);
	$("#buss_id").val(data_customer['buss_id']);
	$("#buss_char").val(data_customer['buss_char']).change();
	$("#bill_addr").val(data_customer['buss_addr']+' '+data_customer['buss_city']+' '+data_customer['buss_country']);
	pop_sales_delivery();
	pop_sales_order();
}
////PO
function dialog_sales_delivery_pull_data(data_sales_order){
	$("#doc_ref_id").val(data_sales_order.doc_id);
	$("#doc_ref").val(data_sales_order.doc_num);
	
	$("#del_doc_ref_id").val(data_sales_order.doc_id);
	$("#del_doc_ref").val(data_sales_order.doc_num);
	//$("#so_doc_ref_id").val();
	$("#so_doc_ref").val(data_sales_order.doc_ref);
	get_del_detail();	
}
function dialog_sales_order_pull_data(data_sales_order){
	$("#doc_ref_id").val(data_sales_order.doc_id);
	$("#doc_ref").val(data_sales_order.doc_num);
	
	$("#so_doc_ref_id").val(data_sales_order.doc_id);
	$("#so_doc_ref").val(data_sales_order.doc_num);
	//$("#del_doc_ref_id").val("");
	$("#del_doc_ref").val("");
	get_so_header();
	get_so_detail();	
	sky_count_doc_summary();
}