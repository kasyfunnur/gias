$('#detail_table').on('click', 'button', function(e){
   e.preventDefault();
   $(this).closest('tr').remove();
   var added_row = (parseInt(document.getElementById('added_row').value))-1;
   document.getElementById('added_row').value = added_row;
   if (added_row <= 0) $('#blank_row').show();
})

function addAsset(id,label,type_id,type_code,type_name) {
  if(typeof(type_id)!=='undefined') printed_type = "<select class='form-control input-sm' name='maint_type_id[]'><option value='"+type_id+"'>"+type_code+" - "+type_name+"</option></select>"; 
  else printed_type = type; 
  var added_row = (parseInt(document.getElementById('added_row').value))+1;
  document.getElementById('added_row').value = added_row;
  if (added_row > 0) $('#blank_row').hide();
  var detail_table 				= document.getElementById('detail_table');
  var row          				= detail_table.insertRow(added_row);
  row.insertCell(0).innerHTML 	= label;
  row.insertCell(1).innerHTML 	= printed_type;
  row.insertCell(2).innerHTML 	= "<input class='form-control input-sm' name='note[]'></input>";
  row.insertCell(3).innerHTML 	= "<div style='text-align:center'><button type='button' class='btn btn-flat btn-danger btn-sm'>Delete</button></div>";
  row.insertCell(4).innerHTML 	= "<input type='hidden' name='asset_id[]' value='"+id+"'></input>";
}

$('#schedule_id').change(function(){
	document.getElementById('detail_table').innerHTML = '<thead><tr><th>Asset Label</th><th>Type</th><th>Note</th><th>Remove</th></tr></thead>     <tbody><tr id="blank_row"><td><i>Nothing Added Yet</i></td><td><br/></td><td><br/></td><td><br/></td><td><br/></td></tr></tbody>';
  	document.getElementById('added_row').value = parseInt(0);
  	if ($(this).val()!=0){
	$(".add-button").prop('disabled',true);
	$.get(("get_maintenance_schedule/"+$(this).val()) , function(result) {
		dataArray = JSON.parse(result);
		for(var i=0; i < dataArray.length; i++)addAsset(dataArray[i]['asset_id'],dataArray[i]['asset_label'],dataArray[i]['maint_type_id'],dataArray[i]['maint_type_code'],dataArray[i]['maint_type_name']);
	});}
	else $(".add-button").prop('disabled',false);
});