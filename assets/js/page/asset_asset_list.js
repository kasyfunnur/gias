var table = $('#asset_list').dataTable({
		'ajax': {	'url'  : 'ajax_datatable/asset_list',
					'data' :  function (d) {d.status = $('#status').val();d.location = $('#location').val()}
				}});
$('#status').change(function(){table.api().ajax.reload()});	
$('#location').change(function(){table.api().ajax.reload()});	