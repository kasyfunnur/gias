var default_row = $("#default");

$(document).ready(function(){
	$(document).on('click','button[name="act_full"]',function(){
		var temp_total = $(this).parent().parent().parent().find('input[name^=doc_total]').val();
		var temp_paid = $(this).parent().parent().parent().find('input[name^=doc_paid]').val();
		var temp_to_pay = temp_total-temp_paid;


		$(this).parent().parent().parent().find('input[name^=to_pay]').val(temp_to_pay);
		//$(this).parent().parent().parent().parent().find('input[name^=to_pay]').val(temp_to_pay);

		//$(this).parent().parent().parent().parent().find('div[id^=to_pay]').html(accounting.formatMoney(temp_to_pay,"",0,",","."));
		

		//$(this).parent().parent().parent().parent().find('div[id^=to_pay]').html('1324635673');

		//row.find('td:eq(4)').find('div[id^=doc_subtotal]').html(accounting.formatMoney(data[p]['doc_subtotal'],"",0,",","."));
		//sky_count_col_total('table_detail','total_payment','to_pay[]');

		//table_doc.row();
		table_doc.col();
	});
	pop_vendor();

	table_doc.init('table_detail',default_row);
	table_doc.init_col('total_payment','to_pay[]');
	table_doc.row_index();
	
	$("#modal_pay").on('show.bs.modal',function(){ 
		//$("#amt_total").val($("#total_payment").val());
		$("#amt_total").val($("input[name^=total_payment]").val());
		$("#amt_payment").val(parseInt($("#bank_amt").val())+parseInt($("#cash_amt").val()));
		$("#amt_balance").val($("#total_payment").val()-(parseInt($("#bank_amt").val())+parseInt($("#cash_amt").val())));
	});

	if(param != ""){
		$("#buss_id").val(buss);
		$("#buss_name").val(buss_name);		
		pop_detail();
	}

	//console.log(param);
	if(param != ""){
		$("#buss_id").val(buss);
		$("#buss_name").val(buss_name);		
		pop_detail();
	}
		
});


//JS CUSTOM PAGE
function form_header_insert(data_header){
	$("#buss_name").val(data_header[0].buss_name);
	$("#buss_id").val(data_header[0].buss_id);
	$("#doc_ref").val(data_header[0].doc_num);
	$("#doc_ref_id").val(data_header[0].doc_id);
	$("#doc_note").val(data_header[0].doc_note);
}
function table_detail_reset(table){
	$("#table_detail > tbody > tr").not('tr:last').remove();
	table_doc.row_index();
}
function table_detail_insert(data){		
	if(data){
		//delete first row
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		}
		for(var p = 0;p<data.length;p++){
			
			var row = clone_row("table_detail");
			row.find('td:eq(1)').find('input[name^=doc_type]').val(data[p]['type']);			
			row.find('td:eq(1)').find('input[name^=doc_id]').val(data[p]['doc_id']);
			row.find('td:eq(1)').find('input[name^=doc_num]').val(data[p]['doc_num']);
			row.find('td:eq(2)').find('input').val(data[p]['doc_dt']);
			row.find('td:eq(3)').find('input[name^=doc_curr]').val(data[p]['doc_curr']);			
			row.find('td:eq(4)').find('input[name^=doc_subtotal]').val(data[p]['doc_subtotal']);
			row.find('td:eq(4)').find('div[id^=doc_subtotal]').html(accounting.formatMoney(data[p]['doc_subtotal'],"",0,",","."));
			row.find('td:eq(5)').find('input[name^=doc_total]').val(data[p]['doc_total']);
			row.find('td:eq(5)').find('div[id^=doc_total]').html(accounting.formatMoney(data[p]['doc_total'],"",0,",","."));
			//sky_count_row_total('table_detail',row,'ast_qty','ast_price','sub_total');	
		}
		table_doc.row();
		table_doc.clone();
		table_doc.row_del_first('doc_type');
		table_doc.row_index();
		table_doc.col();		
		table_doc.sum();
	}else{
		console.log('error');	
	}	
}

////////POP FUNCTION
////VENDOR
function pop_vendor(){ 
	dialog_vendor_reset_table('dialog_vendor_list');
	dialog_vendor_populate_data('dialog_vendor_list',source_vendor);
}

////DEFAULT-DETAIL
function pop_detail(){	
	$.ajax({
		url:ajax_url_1,
		data:{buss_id:$("#buss_id").val()},
		success: function(response) {			
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
		}
	});
}

////////PULL DATA
////VENDOR
function dialog_vendor_pull_data(data_vendor){
	$("#buss_name").val(data_vendor['buss_name']);
	$("#buss_id").val(data_vendor['buss_id']);
	$("#buss_char").val(data_vendor['buss_char']).change();
	$("#bill_addr").val(data_vendor['buss_addr']+' '+data_vendor['buss_city']+' '+data_vendor['buss_country']);
	//pop_purchase_order();
	pop_detail();
}
////PAYMENT
function dialog_make_payment_pull_data(data_make_payment){
	console.log(data_make_payment); 
	$("#payment").val(data_make_payment.total_payment);
	$("#head_bank_amt").val(data_make_payment.bank_amt);
	$("#head_cash_amt").val(data_make_payment.cash_amt);
}
