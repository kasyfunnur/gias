var default_row = $("#default");

$(document).ready(function(){
	$(".button_action").html($('template.button_action').html());
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();			
			table_doc.row_index();
			table_doc.col();
			table_doc.sum();
		}
	});	
	
	//INIT
	//console.log(form_header);
	
	table_doc.init('table_detail',default_row);
	table_doc.init_row('sub_total','ast_qty','','ast_price');
	table_doc.init_col('amount_total','sub_total[]');
	table_doc.row_index();	


	$("#buss_name").val(form_header[0]['buss_name']);
	$("#buss_id").val(form_header[0]['buss_id']);
	$("#doc_ref_id").val(form_header[0]['doc_id']);
	$("#doc_note").val(form_header[0]['doc_note']);
	
	if(form_header[0]['doc_status'] == 'DRAFT'){
		pop_vendor();
		$("#next").attr('disabled',true);
	}
	if(form_header[0]['doc_status'] == 'AWAITING' || form_header[0]['doc_status'] == 'IN REVIEW'){
		$("#next").attr('disabled',true);
		$(".control, .form-control").attr('disabled',true);
	}

	if(form_header[0]['doc_status'] == 'CANCELLED'){
		$(".control, .form-control").attr('disabled',true);
		$("#new").attr('disabled',false);
	}
	if(form_header[0]['doc_status'] == 'UNPAID'){
		//$('a,input,button,select,textarea').attr('disabled',true);
		$(".control, .form-control").attr('disabled',true);
		$("#next").attr('disabled',false);
		$("#view").attr('disabled',false);
		$("#action").attr('disabled',false);	
	}
	if(form_header[0]['doc_status'] == 'PAID'){
		$(".control, .form-control").attr('disabled',true);
		$("#new").attr('disabled',false);
		$("#View").attr('disabled',false);
	}
	pop_vendor();
	
	
	//IF WITH RCV PARAMETER
	//console.log(form_header[0]['doc_ref']);
	/*if(param != ""){
		var url_header, url_detail;		
		url_detail = ajax_url_2;
		url_header = ajax_url_3;	
		$.ajax({
			url:url_detail,
			data:{doc_num:form_header[0]['doc_ref']},
			success: function(response) {
				table_detail_insert(JSON.parse(response));			
			}
		});
		var param_header = $.ajax({
			url:url_header,
			data:{doc_num:form_header[0]['doc_ref']},
			success: function(response){
				form_header_insert(JSON.parse(response));
			}
		});

	}*/
	
	table_detail_insert(form_detail);
	
	
});

/*function validateForm(){
	if(confirm("Are you sure?")==true){
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}*/


//JS CUSTOM PAGE
function form_header_insert(data_header){ //console.log(data_header);
	$("#buss_name").val(data_header[0].buss_name);
	$("#buss_id").val(data_header[0].buss_id);
	$("#doc_ref").val(data_header[0].doc_num);
	$("#doc_ref_id").val(data_header[0].doc_id);
	$("#doc_note").val(data_header[0].doc_note);
}
function table_detail_reset(){ 
	$("#table_detail > tbody > tr").not('tr:last').remove();
	//sky_tbl_refresh_row_num("table_detail");
	table_doc.row_index();
}
function table_detail_insert(data){
	if(data){
		table_doc.row_del_last();
		for(var p = 0;p<data.length;p++){
			var row = clone_row("table_detail");		
			
			row.find('a[name^=a_ast_code]').attr('href',url_1+'/'+data[p]["item_code"]);
			row.find('input[name^=ast_code]').val(data[p]['item_code']);
			row.find('input[type=hidden]').val(data[p]['item_id']);
			row.find('input[name^=ast_name]').val(data[p]['item_name']);			
			row.find('input[name^=ast_qty_open]').val(data[p]['item_qty']-data[p]['item_qty_closed']);
			row.find('input[name^=ast_qty]').val(data[p]['item_qty']);
			row.find('input[name^=ast_price]').val(data[p]['item_netprice']);
			row.find('input[name^=sub_total]').val(data[p]['item_total']);
			row.find('input[name^=reff_line]').val(data[p]['doc_line']);
			
		}
		table_doc.row();
		table_doc.clone();	
		table_doc.row_del_first('ast_code');	
		table_doc.row_index();
		table_doc.col();
		table_doc.extra();
		table_doc.sum();
	}else{
		console.log('error');	
	}	
}

////////POP FUNCTION
////VENDOR
function pop_vendor(){ 
	dialog_vendor_reset_table('dialog_vendor_list');
	dialog_vendor_populate_data('dialog_vendor_list',source_vendor);
}
////PO
/*function pop_purchase_order(){
	return $.ajax({
		url:ajax_url_4,
		data:{buss_id:$("#buss_id").val(),logistic_status:'RECEIVED',billing_status:'UNBILLED'},
		success: function(response) { //console.log(response);
			dialog_purchase_order_reset_table('dialog_purchase_order_list');
			dialog_purchase_order_populate_data('dialog_purchase_order_list',JSON.parse(response));
        }
	});
}*/
function pop_purchase_receipt(){
	return $.ajax({
		url:ajax_url_1,
		data:{buss_id:$("#buss_id").val()},
		success: function(response) { //console.log(response);
			dialog_purchase_receipt_reset_table('dialog_purchase_receipt_list');
			dialog_purchase_receipt_populate_data('dialog_purchase_receipt_list',JSON.parse(response));
        }
	});
}


////////PULL DATA
////VENDOR
function dialog_vendor_pull_data(data_vendor){
	$("#buss_name").val(data_vendor['buss_name']);
	$("#buss_id").val(data_vendor['buss_id']);
	$("#buss_char").val(data_vendor['buss_char']).change();
	$("#bill_addr").val(data_vendor['buss_addr']+' '+data_vendor['buss_city']+' '+data_vendor['buss_country']);
	pop_purchase_receipt();
	//pop_purchase_order();
}
////PO
function dialog_purchase_receipt_pull_data(data_purchase_order){
	$("#doc_ref_id").val(data_purchase_order.doc_id);
	$("#doc_ref").val(data_purchase_order.doc_num);
	
	$("#rcv_doc_ref_id").val(data_purchase_order.doc_id);
	$("#rcv_doc_ref").val(data_purchase_order.doc_num);
	//$("#po_doc_ref_id").val();
	$("#po_doc_ref").val(data_purchase_order.doc_ref);
	get_rcv_detail();	
}
/*function dialog_purchase_order_pull_data(data_purchase_order){
	$("#doc_ref_id").val(data_purchase_order.doc_id);
	$("#doc_ref").val(data_purchase_order.doc_num);
	
	$("#po_doc_ref_id").val(data_purchase_order.doc_id);
	$("#po_doc_ref").val(data_purchase_order.doc_num);
	//$("#rcv_doc_ref_id").val("");
	//$("#rcv_doc_ref").val("");
	get_po_detail();	
}*/