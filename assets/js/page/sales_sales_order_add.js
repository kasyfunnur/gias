var default_row = $("#default");
//CUSTOM
var DataCustomer = source_customer;
var customers = new Bloodhound({
	datumTokenizer: function (d) { 
		for (var prop in d) { return Bloodhound.tokenizers.whitespace(d.buss_name);}
	},
	queryTokenizer: Bloodhound.tokenizers.whitespace,
	limit: 5, //#2 added
	local : DataCustomer
});
customers.initialize();
/*$('.typeahead').typeahead(null,{  // #1 added
	//name : 'stock', //#4 added	
	displayKey: function(stocks) {
        for (var prop in stocks) { 
			return stocks.item_code;
        }
    },
	source:stocks.ttAdapter() ,
	updater: function (stocks) {
		return stocks;
	}
}).on('typeahead:selected', function (obj, datum) {
	var var_datum = [];
	var_datum[0] = datum.item_id;
	dialog_inventory_pull_data(var_datum);
});*/
function apply_typeahead(elm){ //alert('tes and yes');
	$(elm).typeahead(null,{ 
		displayKey: function(customers) {
			for (var prop in customers) { 
				return customers.buss_name;
			}
		},
		source:customers.ttAdapter() ,
		updater: function (customers) {
			return customers+"sfsef";
		}
	}).on('typeahead:selected', function (obj, datum) {
		var var_datum = [];
		//var_datum[0] = datum.buss_id;
		var_datum['buss_id']=datum.buss_id;
		var_datum['buss_name']=datum.buss_name;
		var_datum['buss_addr']=datum.buss_addr;
		var_datum['buss_city']=datum.buss_city;
		var_datum['buss_country']=datum.buss_country;
		var_datum['buss_tlp1']=datum.buss_tlp1;
		var_datum['buss_tlp2']=datum.buss_tlp2;
		
		dialog_customer_pull_data(var_datum);
	});
}
//apply_typeahead('input[name^=ast_code].typeahead');
apply_typeahead('input[name=buss_name]');

$(document).ready(function(){
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			table_doc.row_index();
			table_doc.col();
			table_doc.extra();
			table_doc.sum();
		}
	});	
	$("#doc_disc_pct").css('padding',5);	
	//INIT
	$("#location_id").val(1).change();	
	//sky_tbl_refresh_row_num("table_detail");
	pop_customer();
	pop_inventory();	
	table_doc.init('table_detail',default_row);
	
	table_doc.init_row('const_after_disc[]','const_disc[]','-','ast_disc[]');
	table_doc.init_row('ast_disc_factor[]','const_after_disc[]','/','const_disc[]');
	table_doc.init_row('ast_price_after_disc[]','ast_price[]','*','ast_disc_factor[]');
	
	table_doc.init_row('const_after_tax[]','const_tax[]','+','ast_tax[]');
	table_doc.init_row('ast_tax_factor[]','const_after_tax[]','/','const_tax[]');
	table_doc.init_row('ast_netprice[]','ast_price_after_disc[]','*','ast_tax_factor[]');

	table_doc.init_row('sub_total[]','ast_qty[]','*','ast_netprice[]');

	//table_doc.init_row('sub_total','ast_qty','','ast_price');
	table_doc.init_col('doc_subtotal','sub_total[]');
	table_doc.init_col('qty_total','ast_qty[]');
	table_doc.row_index();	
});

function validateForm(){
	if(confirm("Are you sure?")==true){
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}

//JS CUSTOM PAGE
function table_detail_reset(){
	$("#table_detail > tbody > tr").not('tr:last').remove();
	//sky_tbl_refresh_row_num("table_detail");
	table_doc.row_index();
}
function table_detail_insert(data){		
	if(data){		
		table_doc.row_del_last();
		for(var p = 0;p<data.length;p++){
			var row = table_doc.clone();			
			//row.find('td:eq(1)').html('<a href="'+url_1+'/'+data[p]["item_code"]+'" target="_blank" class="form-control" style="text-decoration:underline;" >'+data[p]["item_code"]+'</a>');
			row.find('a[name^=a_ast_code]').attr('href',url_1+'/'+data[p]["item_code"]);
			row.find('input[name^=ast_code]').val(data[p]['item_code']);
			row.find('input[name^=item_id]').val(data[p]['item_id']);
			row.find('input[name^=ast_name]').val(data[p]['item_name']);
			row.find('input[name^=ast_qty]').val(1);
			row.find('input[name^=ast_price]').val(data[p]['item_sell_price']);
		}
		table_doc.row();
		table_doc.clone();
		table_doc.row_del_first('ast_code');
		table_doc.row_index();
		table_doc.col();
		table_doc.extra();
		table_doc.sum();
	}else{
		console.log('error');	
	}	
}

function pop_inventory(){
	dialog_inventory_reset_table('dialog_inventory_list');
	dialog_inventory_populate_data('dialog_inventory_list',source_inventory);
}
function pop_customer(){	
	dialog_customer_reset_table('dialog_customer_list');
	dialog_customer_populate_data('dialog_customer_list',source_customer);
}

function dialog_customer_pull_data(data_customer){
	$("#buss_name").val(data_customer['buss_name']);
	$("#buss_id").val(data_customer['buss_id']);
	$("#buss_char").val(data_customer['buss_char']).change();
	$("#ship_addr").val(data_customer['buss_addr']+' '+data_customer['buss_city']+' '+data_customer['buss_country']);
	$("#cust_phone1").val(data_customer['buss_tlp1']);
	$("#cust_phone2").val(data_customer['buss_tlp2']);
}
function dialog_inventory_pull_data(data_inventory){ 
	var data_inventory_param = $.map(data_inventory, function(value, index) {
		return [value];
	});
	var data_ajax_raw = $.ajax({
		url:ajax_url_1,
		data: {inventory:data_inventory_param},
		dataType: "json",
		success: function(result){
			//table_detail_reset();			
			table_detail_insert(result);			
		}
	});	
}