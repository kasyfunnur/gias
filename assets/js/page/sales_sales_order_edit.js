var default_row = $("#default");

$(document).ready(function(){
	$(".button_action").html($("#button_action").html());
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			table_doc.row_index();
			table_doc.col();
			table_doc.extra();
			table_doc.sum();
		}
	});		
	$("#doc_disc_pct").css('padding',5);		
	//INIT
	$("#location_id").val(1).change();	
	sky_tbl_refresh_row_num("table_detail");
	pop_customer();
	pop_inventory();
	table_doc.init('table_detail',default_row);
	table_doc.init_row('sub_total','ast_qty','','ast_price');
	table_doc.init_col('doc_subtotal','sub_total[]');
	table_doc.init_col('qty_total','ast_qty[]');
	
	//ON EDIT PAGE
	$("#location_id").val(form_header[0]['whse_id']).change();
	$("#buss_id").val(form_header[0]['buss_id']);
	$("#buss_name").val(form_header[0]['buss_name']);
	$("#ship_addr").val(form_header[0]['buss_addr']);
	$("#doc_ref").val(form_header[0]['doc_num']);
	$("#doc_note").val(form_header[0]['doc_note']);
	$("#doc_disc_pct").val(form_header[0]['doc_disc_percent']);
	$("#doc_pct").val(form_header[0]['doc_disc_amount']);		
	if(status_logistic != "PENDING"){
		$('#buss_name,#location_id,#doc_note,#doc_dt,#doc_ddt').css('color','#000');
		$('input,button,select,textarea').attr('disabled',true);
	}
	table_detail_insert(form_detail);
	/*if(form_detail){
		table_doc.row_del_last();
		for(var p = 0;p<form_detail.length;p++){
			var row = table_doc.clone();	
			row.find('a[name^=a_ast_code]').attr('href',url_1+'/'+form_detail[p]["item_code"]);
			row.find('input[name^=ast_code]').val(form_detail[p]['item_code']);
			row.find('input[name^=item_id]').val(form_detail[p]['item_id']);
			row.find('input[name^=ast_name]').val(form_detail[p]['item_name']);
			row.find('input[name^=ast_qty]').val(form_detail[p]['item_qty']);
			row.find('input[name^=ast_price]').val(form_detail[p]['item_netprice']);
		}
		table_doc.row();
		table_doc.clone();		
		table_doc.row_del_first();	
		table_doc.row_index();
		table_doc.col();
		if(form_header[0]['doc_disc_percent'] == 0){
			//table_doc.extra();
		}
		table_doc.sum();
	}else{
		console.log('error');	
	}*/
});

function validateForm(){
	if(confirm("Are you sure?")==true){
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){	
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}

//JS CUSTOM PAGE
//var so_default_row = '<tr><td><input type="checkbox" name="chk[]" id="chk"/></td><td><input type="hidden" name="doc_id[]" id="doc_id" /><span></span></td><td></td><td></td><td></td><td></td></tr>';
function table_detail_reset(){
	$("#table_detail > tbody > tr").not('tr:last').remove();
	//sky_tbl_refresh_row_num("table_detail");
	table_doc.row_index();
	//$('#table_detail > tbody').append(so_default_row);
}
function table_detail_insert(data){		
	if(data){
		table_doc.row_del_last();
		for(var p = 0;p<data.length;p++){
			var row = table_doc.clone();						
			row.find('a[name^=a_ast_code]').attr('href',url_1+'/'+data[p]["item_code"]);
			row.find('input[name^=ast_code]').val(data[p]['item_code']);
			row.find('input[name^=item_id]').val(data[p]['item_id']);
			row.find('input[name^=ast_name]').val(data[p]['item_name_so'] || data[p]['item_name'] );
			row.find('input[name^=ast_qty]').val(data[p]['item_qty'] || 1);
			row.find('input[name^=ast_price]').val(data[p]['item_sell_price'] || data[p]['item_price']);
		}
		table_doc.row();
		table_doc.clone();
		table_doc.row_del_first();
		table_doc.row_index();
		table_doc.col();
		table_doc.extra();
		table_doc.sum();
	}else{
		console.log('error');	
	}	
}

function pop_inventory(){
	dialog_inventory_reset_table('dialog_inventory_list');
	dialog_inventory_populate_data('dialog_inventory_list',source_inventory);
}
function pop_customer(){	
	dialog_customer_reset_table('dialog_customer_list');
	dialog_customer_populate_data('dialog_customer_list',source_customer);
}

function dialog_customer_pull_data(data_customer){
	$("#buss_name").val(data_customer['buss_name']);
	$("#buss_id").val(data_customer['buss_id']);
	$("#buss_char").val(data_customer['buss_char']).change();
	$("#ship_addr").val(data_customer['buss_addr']+' '+data_customer['buss_city']+' '+data_customer['buss_country']);
	$("#cust_phone1").val(data_customer['buss_tlp1']);
	$("#cust_phone2").val(data_customer['buss_tlp2']);
}
function dialog_inventory_pull_data(data_inventory){
	var data_inventory_param = $.map(data_inventory, function(value, index) {
		return [value];
	});
	var data_ajax_raw = $.ajax({
		url:ajax_url_1,
		data: {inventory:data_inventory_param},
		dataType: "json",
		success: function(result){				
			//table_detail_reset();
			table_detail_insert(result);			
		}
	});	
}