var default_row = $("#default");

$(document).ready(function(){
	$(".button_action").html($('template.button_action').html());
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			table_doc.row_index();
			table_doc.col();
			table_doc.sum();
		}
	});	
	
	//INIT
	$("#location_id").val(1).change();	
	
	table_doc.init('table_detail',default_row);	
	table_doc.row_index();
	//pop_inventory();
	

	if(form_header[0]['doc_status'] == 'DRAFT'){		
		$("#next").attr('disabled',true);
	}
	if(form_header[0]['doc_status'] == 'APPROVED'){
		$('a,input,button,select,textarea').attr('disabled',true);
		$("#next").attr('disabled',false);
		$("#View").attr('disabled',false);
		$("#Action").attr('disabled',false);
	}
	if(form_header[0]['doc_status'] == 'CANCELLED'){
		$('a,input,button,select,textarea').attr('disabled',true);
	}
	if(form_header[0]['doc_status'] == 'CLOSED'){
		$('a,input,button,select,textarea').attr('disabled',true);
		$("#View").attr('disabled',false);
	}

	if(form_detail){
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		}
		for(var p = 0;p<form_detail.length;p++){
			var row = clone_row("table_detail");		
			row.find('input[name^=item_code]').val(form_detail[p]['item_code']);
			row.find('input[name^=item_name]').val(form_detail[p]['item_name']);
			row.find('input[name^=item_qty]').val(form_detail[p]['item_qty']);
		}
		var row = clone_row("table_detail");
		if($('#table_detail > tbody > tr:first').find('td:eq(1) > input').val() == ""){			
			$('#table_detail > tbody > tr:first').remove();
		}
		sky_tbl_refresh_row_num("table_detail");
	}else{
		console.log('error');	
	}
	
});

function validateForm(){
	if(confirm("Are you sure?")==true){
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}


//JS CUSTOM PAGE
function table_detail_reset(){
	$("#table_detail > tbody > tr").not('tr:last').remove();
	sky_tbl_refresh_row_num("table_detail");
	//$('#table_detail > tbody').append(po_default_row);
}
function table_detail_insert(data){		
	if(data){
		//delete first row
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		}
		for(var p = 0;p<data.length;p++){
			var row = clone_row("table_detail");		
			row.find('input[name^=item_code]').val(data[p]['item_code']);
			row.find('input[name^=item_name]').val(data[p]['item_name']);
			row.find('input[name^=item_qty]').val(1);
			//sky_count_row_total('table_detail',row,'ast_qty','ast_price','sub_total');	
		}
		var row = clone_row("table_detail");
		//delete first for after insert
		if($('#table_detail > tbody > tr:first').find('td:eq(1) > input').val() == ""){
			$('#table_detail > tbody > tr:first').remove();
		}
		sky_tbl_refresh_row_num("table_detail");
		sky_count_col_total('table_detail','doc_total','sub_total[]')
	}else{
		console.log('error');	
	}	
}
function pop_inventory(){
	dialog_inventory_reset_table('dialog_inventory_list');
	dialog_inventory_populate_data('dialog_inventory_list',source_inventory);
}

function dialog_inventory_pull_data(data_inventory){ //console.log(data_inventory);
	var data_inventory_param = $.map(data_inventory, function(value, index) {
		return [value];
	});
	var data_ajax_raw = $.ajax({
		url:ajax_url_1,
		data: {inventory:data_inventory_param},
		dataType: "json",
		success: function(result){
			//dialog_purchase_order_reset_table('table_detail');
			table_detail_reset();
			table_detail_insert(result);			
		}
	});	
}