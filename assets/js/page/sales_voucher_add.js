function FormValidation()
{
	if($('#voucher_code').val().trim() == ''){
		alert('Please fill Voucher Code');
		return false;
	}

	$('#voucher_count').val($.trim($('#voucher_count').val()));
	if(isNaN($('#voucher_count').val())){
		alert('Please enter numerical values for Voucher Count');
		return false;
	}

	if(new Date($('#active_date').val()) > new Date($('#expiry_date').val())){
		alert('Expiry date should be greater than Active Date');
		return false;
	}

	return true;
}