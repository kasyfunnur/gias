var rowCount = 1;
var default_row = $("#default");
function clone_row(table){
	rowCount++;
	var row = $("#"+table+" > tbody > tr:first").clone().find("input,select").each(function(){		
		var temp_name = $(this).attr('name');
		$(this).attr('id',function(_,id){return temp_name.substring(0,temp_name.length-2)+rowCount;});
		$(this).attr('name',function(_,name){return name;});
		$(this).val('');
	}).end().appendTo("#"+table+" > tbody");
	sky_tbl_refresh_row_num(table);
	return row;
}
function sky_tbl_refresh_row_num(tableid){
	$('#'+tableid+' > tbody > tr').each(function(){
		var t_idx = $(this).index()+1;
		$(this).find('td:eq(0) > input[name="row[]"]').val(t_idx);
	});	
}
function sky_count_row_total(table,obj,qty,price,total){	
	$("#"+table+" > tbody > tr").each(function(){
		var name = $(this).find('td:eq(0)').find('input').attr('name');
		var id = $(this).find('td:eq(0)').find('input').attr('id');
		var cnt = id.substring(name.length-2,name.length);
		var temp_qty = $("input[id^='"+qty+cnt+"']").val();
		var temp_price = $("input[id^='"+price+cnt+"']").val();
		var temp_total = temp_qty * temp_price;
		$("input[id^='"+total+cnt+"']").val(temp_total);
	});
}
function sky_count_col_total(table,target,col){
	var tbl 	= $("#"+table+ " > tbody");
	var tbl_row = tbl[0].rows.length;
	var col_arr = $("input[name='"+col+"']");
	var total 	= 0;
	for(var q = 0; q < tbl_row-1; q++){
		if($(col_arr[q]).val() != ""){
			total += parseInt($(col_arr[q]).val());
		}		
		//console.log($(col_arr[q]).val());
	}
	$("#"+target).val(total);	
}

$(document).ready(function(){
	$(document).on('click','button[name="act_full"]',function(){
		var temp_total = $(this).parent().parent().parent().parent().find('input[name^=doc_total]').val();
		var temp_paid = $(this).parent().parent().parent().parent().find('input[name^=doc_paid]').val();
		var temp_to_pay = temp_total-temp_paid;
		$(this).parent().parent().parent().parent().find('input[name^=to_pay]').val(temp_to_pay);
		sky_count_col_total('table_detail','total_payment','to_pay[]');
	});
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			sky_tbl_refresh_row_num('table_detail');
			sky_count_col_total('table_detail','qty_total','ast_qty[]');
		}
	});	
	$(document).on('change','#table_detail > tbody > tr:last',function(){
		clone_row('table_detail');
		sky_tbl_refresh_row_num('table_detail');
	});
	$(document).on('change','#table_expense > tbody > tr:last',function(){
		clone_row('table_expense');
		sky_tbl_refresh_row_num('table_expense');
	});
	//calc total per line
	$(document).on("change","input[name^='to_pay']",function(){ 
		//sky_count_row_total('table_detail','','ast_qty','ast_price','sub_total');	
		sky_count_col_total('table_detail','total_payment','to_pay[]');
	});
	
	$("#modal_pay").on('show.bs.modal',function(){ 
		$("#amt_total").val($("#total_payment").val());
		$("#amt_payment").val(parseInt($("#bank_amt").val())+parseInt($("#cash_amt").val()));
		$("#amt_balance").val($("#total_payment").val()-(parseInt($("#bank_amt").val())+parseInt($("#cash_amt").val())));
	});

	$("#location_id").on('change',function(){
		var result;
		for( var i = 0; i < source_location.length; i++ ) {
			if( source_location[i]['location_id'] == $(this).val() ) {
				result = source_location[i];
				break;
				$("#ship_addr").val(result['location_address']+', '+result['location_city']+', '+result['location_state']+', '+result['location_country']);
			}else{
				$("#ship_addr").val("");
			}
		}	
		$("#ship_addr").val(
			result['location_address']+', '+
			result['location_city']+', '+
			result['location_state']+', '+
			result['location_country']);
	});
	
	//INIT
	$("#location_id").val(1).change();
	sky_tbl_refresh_row_num("table_detail");	
	pop_customer();
	//$("#table_detail").colResizable();
	
	//IF WITH PO PARAMETER
	console.log(param);
	if(param != ""){
		$("#buss_id").val(buss);
		$("#buss_name").val(buss_name);		
		pop_detail();
	}
		
});

function validateForm(){
	if(confirm("Are you sure?")==true){
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}


//JS CUSTOM PAGE
function form_header_insert(data_header){
	$("#buss_name").val(data_header[0].buss_name);
	$("#buss_id").val(data_header[0].buss_id);
	$("#doc_ref").val(data_header[0].doc_num);
	$("#doc_ref_id").val(data_header[0].doc_id);
	$("#doc_note").val(data_header[0].doc_note);
}
function table_detail_reset(table){
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(default_row);
}
function table_detail_insert(data){		
	if(data){
		//delete first row
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		}
		for(var p = 0;p<data.length;p++){
			var row = clone_row("table_detail");
			row.find('td:eq(1)').find('input[name^=doc_type]').val(data[p]['type']);			
			row.find('td:eq(1)').find('input[name^=doc_id]').val(data[p]['doc_id']);
			row.find('td:eq(1)').find('input[name^=doc_num]').val(data[p]['doc_num']);
			row.find('td:eq(2)').find('input').val(data[p]['doc_dt']);
			row.find('td:eq(3)').find('input[name^=doc_curr]').val(data[p]['doc_curr']);			
			row.find('td:eq(4)').find('input[name^=doc_subtotal]').val(data[p]['doc_subtotal']);
			row.find('td:eq(5)').find('input[name^=doc_total]').val(data[p]['doc_total']);
			//sky_count_row_total('table_detail',row,'ast_qty','ast_price','sub_total');	
		}
		var row = clone_row("table_detail");
		//delete first for after insert
		if($('#table_detail > tbody > tr:first').find('td:eq(1) > input').val() == ""){
			$('#table_detail > tbody > tr:first').remove();
		}
		sky_tbl_refresh_row_num("table_detail");
		sky_count_col_total('table_detail','total_payment','to_pay[]');
		//sky_count_col_total('table_detail','qty_total','ast_qty[]')
	}else{
		console.log('error');	
	}	
}

////////POP FUNCTION
////VENDOR
function pop_customer(){ 
	dialog_customer_reset_table('dialog_customer_list');
	dialog_customer_populate_data('dialog_customer_list',source_customer);
}

////DEFAULT-DETAIL
function pop_detail(){	
	$.ajax({
		url:ajax_url_1,
		data:{buss_id:$("#buss_id").val()},
		success: function(response) {			
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
		}
	});
}

////////PULL DATA
////VENDOR
function dialog_customer_pull_data(data_customer){
	$("#buss_name").val(data_customer['buss_name']);
	$("#buss_id").val(data_customer['buss_id']);
	$("#buss_char").val(data_customer['buss_char']).change();
	$("#bill_addr").val(data_customer['buss_addr']+' '+data_customer['buss_city']+' '+data_customer['buss_country']);
	//pop_purchase_order();
	pop_detail();
}
////PAYMENT
function dialog_make_payment_pull_data(data_make_payment){
	console.log(data_make_payment); 
	$("#payment").val(data_make_payment.total_payment);
	$("#head_bank_amt").val(data_make_payment.bank_amt);
	$("#head_cash_amt").val(data_make_payment.cash_amt);
}
