$(function() {
	// document ready function

    // collapse the main side bar
    $(document.body).addClass('sidebar-collapse');

	// bind key "enter" on the barcode / itemcode text box
	$("#input_code").bind("keypress", {}, keypressInBox);


    // bind key "enter" on the promo voucher text box
    //$("#input_promo_code").bind("keypress", {}, keypressPromo);

    // bind key "enter" on the voucher code payment
    $("#input_pay_vc_code").bind("keypress", {}, keypressVoucher);

    // focus on the barcode input
    $('#input_code').focus();

    // bind add payments input cash
    $('#input_pay_cash_amount').on('keypress', {}, function(event) {
        var code = (event.keyCode ? event.keyCode : event.which);
        if (code == 13) {
            event.preventDefault();
            add_payment('cash');
        }
    });

    // bind add payments input credit card
    $('#input_pay_cc_amount').on('keypress', {}, function(event) {
        var code = (event.keyCode ? event.keyCode : event.which);
        if (code == 13) {
            event.preventDefault();
            add_payment('creditcard');
        }
    });

    //setup accounting formats (accounting.min.js which is loaded in footer.php)
    accounting.settings = {
        currency: {
            symbol : "Rp",   // default currency symbol is '$'
            format: "%s %v", // controls output: %s = symbol, %v = value/number (can be object: see below)
            decimal : ".",  // decimal point separator
            thousand: ",",  // thousands separator
            precision : 0   // decimal places
        },
        number: {
            precision : 0,  // default precision on numbers is 0
            thousand: ",",
            decimal : "."
        }
    }


});


/**
*
* keypressInBox(e)
* Function called after enter key is pressed on barcode text box
*
**/

function keypressInBox(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) { //Enter keycode
    	// what do we do when "Enter" or Barcode-scan finished inputing the code?
        
        // prevent the submit of the form
            e.preventDefault();

        // look up the item from ajax
            var itemcode = $('#input_code').val();

            // get_item_url is set at the view dynamically by PHP (views/sales/sales_pos_add.php)
            $.ajax(get_item_url,{
                method   : "POST",
                cache    : true,
                dataType : 'json',
                data     : {'itemcode' : itemcode}
            }).done(function (data){
                console.log(data);
                if(data && data.success && data.itemdata.item_id && !isNaN(data.itemdata.item_id)){
                    var item = new Object();
                    item.id = parseInt(data.itemdata.item_id);
                    item.code = data.itemdata.item_code;
                    item.name = data.itemdata.item_name;
                    // item.price = parseFloat(data.itemdata.item_sell_price).toFixed(0);
                    item.price = accounting.unformat(data.itemdata.item_sell_price);
                    item.image = base_url + 'assets/img/prod/' + data.itemdata.item_code + '.jpg';
                    item.qty = 1;

                    // display the item details on the item detail section
                    $('#barcode_placeholder').empty().append(item.code);
                    $('#image_placeholder').empty().append('<img src="'+item.image+'" class="img-responsive">');
                    $('#item_placeholder').empty().append('<h3>'+item.name+'</h3><h4>'+accounting.formatMoney(item.price)+'</h4>');

                    // input those item into the pos box
                    add_sales_item(item);

                    // clears the barcode item
                    clear_barcode_input();

                    // focus back on the barcode input
                    $('#input_code').focus();

                } else {
                    alert((data.message) ? data.message : 'Unable to retrieve item');
                }
            });
    }
};

/**
*
* add_sales_item
* Add itemdata 
*
**/

var items_array = [];

function add_sales_item(item)
{
    
    // add to global items_array
    // check if not exists then add to item array
    if (find_item_index(item.id) == -1){
        items_array.push(item);

        // find the display item div
            var t = $('#pos_item_list');

        // add to display item div
            var itemrow = '';
            itemrow += '<div class="row" id="row_'+item.id+'">';

            //formfields hidden
            itemrow += '<input type="hidden" name="inp_item_id[]" value="'+item.id+'"/>';
            itemrow += '<input type="hidden" name="inp_item_qty[]" value="1"/>';
            itemrow += '<input type="hidden" name="inp_item_price[]" value="'+item.price+'"/>';

            itemrow +=  '<div class="col-xs-2">';
            itemrow +=  item.code;
            itemrow +=  '</div>';

            itemrow +=  '<div class="col-xs-3">';
            itemrow +=  item.name;
            itemrow +=  '</div>';
            
            itemrow +=  '<div class="col-xs-3 text-right">';
            itemrow +=  accounting.formatMoney(item.price);
            itemrow +=  '<br>x <span id="div_qty_'+item.id+'">1</span>';
            itemrow +=  '</div>';
            
            itemrow +=  '<div class="col-xs-3 text-right" id="div_total_'+item.id+'">';
            itemrow +=  accounting.formatMoney(item.price);
            itemrow +=  '</div>';

            itemrow +=  '<div class="col-xs-1 text-right" id="div_action_'+item.id+'">';
            itemrow +=  '<button type="button" onclick="remove_item('+item.id+')" class="btn btn-danger btn-xs"> &mdash; </button>';
            itemrow +=  '</div>';
            
            itemrow += '</div>';
            t.append(itemrow);

    } else {
    // else modify the qty
        items_array[find_item_index(item.id)].qty = items_array[find_item_index(item.id)].qty + parseInt(item.qty);

        // modify the qty div
            var div_qty = $('#div_qty_' + item.id);
            div_qty.empty().html(items_array[find_item_index(item.id)].qty);

        // modify the qty input hidden
            $('#row_'+item.id).children("input[name='inp_item_qty[]']").val(items_array[find_item_index(item.id)].qty)

        // modify the total price
            var div_total_price = $('#div_total_' + item.id);
            div_total_price.empty().html(accounting.formatMoney(items_array[find_item_index(item.id)].qty * item.price));
    }

    // debug the items_array
        console.log(items_array);

    // recalculate total and tax
        calc_sales();

    // in case this is added after payment, recalculate the payment and change
        calc_payment();
}

/**
*
* remove_item(itemid)
* Remove item one by one until the value reach zero then delete item row
**/
function remove_item(itemid)
{
    // get the item index
    var index = find_item_index(itemid);
    var qty = parseInt(items_array[index].qty);
    if (qty - 1 == 0) {
        // delete the item from that array
        items_array.splice(index,1);
        console.log(items_array);

        // remove the item row
        $('#row_' + itemid).remove();

    } else if (qty - 1 > 0) {
        // if reduction in quantity only

        // reduce the item count in items_array
        items_array[index].qty = parseInt(items_array[index].qty) - 1;

        // update the display
        // modify the qty div
            var div_qty = $('#div_qty_' + itemid);
            div_qty.empty().html(items_array[index].qty);

        // modify the total price
            var div_total_price = $('#div_total_' + itemid);
            div_total_price.empty().html(accounting.formatMoney(items_array[index].qty * items_array[index].price));
    }

    // re-calculate total sales
        calc_sales();

    // in case this is added after payment, recalculate the payment and change
        calc_payment();
}



/**
*
* Add Payment to payment list
*
**/
var payments_array = [];
var payment_counter = 1;

function add_payment(method, amount)
{

    var payment = new Object();
    payment_counter ++;
    switch(method) {
        case 'cash':
            payment.id = payment_counter;
            payment.method = 'cash';
            payment.amount = Number(accounting.toFixed($('#input_pay_cash_amount').val(),0));
            payment.identifier = '';
            break;
        case 'creditcard':
            payment.id = payment_counter;
            payment.method = 'creditcard';
            payment.amount = Number(accounting.toFixed($('#input_pay_cc_amount').val(),0));
            payment.identifier = ''; // to do : fill with approval code
            break;
        case 'voucher':
            payment.id = payment_counter;
            payment.method = 'voucher';
            payment.amount = amount;
            payment.identifier = $('#input_pay_vc_code').val();
            break;
        default:
            payment.id = payment_counter;
            payment.method = 'default';
            payment.amount = 0;
            break;
    }

    // update the array
    payments_array.push(payment);

    // debug the payment array
    console.log(payments_array);

    // update the display
    $('#div_payment_list').append(
        '<div class="row" id="row_payment_'+payment.id+'">'+
        '<input type="hidden" name="inp_pay_method[]" value="'+payment.method+'">'+
        '<input type="hidden" name="inp_pay_amount[]" value="'+payment.amount+'">'+
        '<input type="hidden" name="inp_pay_identifier[]" value="'+payment.identifier+'">'+
        '<div class="col-xs-3">'+payment.method+'</div>'+
        '<div class="col-xs-6 text-right">'+accounting.formatMoney(payment.amount)+'</div>'+
        '<div class="col-xs-3">'+ '<button type="button" onclick="remove_payment(\''+payment.id+'\')" class="btn btn-danger btn-xs"> &mdash; </button>' +'</div>'+
        '</div>'
    );

    calc_payment();
}

/**
*
* Calculate total payment and change
*
**/
function calc_payment()
{
    var total_pay = 0;

    for (i = 0; i < payments_array.length; i++) {
        total_pay += Number(accounting.toFixed(payments_array[i].amount,0)) ;
    };

    var sales = Number(accounting.unformat($('#div_total_sales').text()));
    var change = Number(total_pay - sales);
    console.log(change);

    // update div displaying total payment and change
    $('#div_total_payment').empty().html(accounting.formatMoney(total_pay));
    $('#div_change').empty().html(accounting.formatMoney(change) +
        '<input type="hidden" id="hid_change_amount" name="hid_change_amount" value="'+accounting.unformat(change)+'"/>');
}

/**
*
* remove_payment(method)
* remove from the payment list
**/
function remove_payment(paymentid)
{
    // find the payment index
    var index = find_payment_index(paymentid);
    payments_array.splice(index,1);
    $('#row_payment_'+paymentid).remove();
    calc_payment();
}


/**
*
* find_payment_index(paymentid)
* find the numeric index of paymentid within items_array
**/

function find_payment_index(paymentid)
{
    var i, index;
    index = -1;
    for (i = 0; i < payments_array.length; i++) {
        if(payments_array[i].id == paymentid){
            index = i;
            break;
        }
    };
    return index;
}

/**
*
* find_payment_identifier
* similar like find_payment_index, but by parsing the identifier instead
* used in finding duplicate voucher code entry
*/
function find_payment_identifier(identity)
{
    var i, index;
    index = -1;
    for (i = 0; i < payments_array.length; i++) {
        if(payments_array[i].identifier == identity){
            index = i;
            break;
        }
    };
    return index;
}


/**
*
* find_item_index(itemid)
* find the numeric index of itemid within items_array
**/

function find_item_index(itemid)
{
    var i, index;
    index = -1;
    for (i = 0; i < items_array.length; i++) {
        if(items_array[i].id == itemid){
            index = i;
            break;
        }
    };
    return index;
}


/**
*
* calc_sales()
* Calculate total sales , tax and item count
*
**/
function calc_sales()
{
    var total_sales = 0;
    var total_tax = 0;
    var total_count = 0;

    for (i = 0; i < items_array.length; i++) {
        total_sales += Number(accounting.toFixed(items_array[i].price * items_array[i].qty,0)) ;
        total_count += parseInt(items_array[i].qty);
    };

    // VAT Inclusive method for calculating tax (Price includes PPN 10%)
    total_tax = accounting.toFixed(total_sales - accounting.toFixed(total_sales /  1.1,0),0);

    // update div displaying item count, total sales and total tax
    $('#div_item_count').empty().html(total_count + ' Items');
    $('#div_total_sales').empty().html(accounting.formatMoney(total_sales) +
        '<input type="hidden" name="total_sales" value="'+total_sales+'" />'
        );
}

/**
*
* clear_barcode_input()
* Clear any input on the barcode
*
**/
function clear_barcode_input()
{
    $('#input_code').val('');
}

/**
*
* keypressVoucher(e) for voucher entry
*
**/
function keypressVoucher(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) { //Enter keycode
        // what do we do when "Enter" or Barcode-scan finished inputing the code?
        
        // prevent the submit of the form
            e.preventDefault();

        // get the input voucher code
            var vouchercode = $('#input_pay_vc_code').val();

        // initial check whether voucher has been used or not
            if(find_payment_identifier(vouchercode) != -1){
                // means exists
                alert('Duplicate voucher entry : '+ vouchercode);
                return false;
            } 
            
        // look up the voucher from ajax
            // get_voucher_url is set at the view dynamically by PHP (views/sales/sales_pos_add.php)
            $.ajax(get_voucher_url,{
                method   : "POST",
                cache    : true,
                dataType : 'json',
                data     : {'vouchernumber' : vouchercode}
            }).done(function (data){
                console.log(data);
                if(data && data.success && data.voucherdata.used == 0 && !isNaN(data.voucherdata.value)){
                    var voucher = new Object();
                    voucher.code = vouchercode;
                    voucher.value = data.voucherdata.value;

                    // add to payment list with identification
                    add_payment('voucher', data.voucherdata.value);

                    // focus back on the voucher input
                    $('#input_pay_vc_code').val('').focus();

                } else {
                    alert((data.message) ? data.message : 'Unable to retrieve promotion or voucher');
                }
            });
    }
};

/**
*
* keypressPromo(e)
* Function called after enter key is pressed on promo voucher text box
*
**/

function keypressPromo(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) { //Enter keycode
        // what do we do when "Enter" or Barcode-scan finished inputing the code?
        
        // prevent the submit of the form
            e.preventDefault();

        // look up the item from ajax
            var promocode = $('#input_promo_code').val();

            // get_item_url is set at the view dynamically by PHP (views/sales/sales_pos_add.php)
            $.ajax(get_promo_url,{
                method   : "POST",
                cache    : true,
                dataType : 'json',
                data     : {'promocode' : promocode}
            }).done(function (data){
                console.log(data);
                if(data && data.success && data.promodata.type && !isNaN(data.promodata.value)){
                    var promo = new Object();
                    promo.code = promocode;
                    promo.type = data.promodata.type;
                    promo.value = data.promodata.value;

                    // input those promo into the pos box
                    add_promo(promo);

                    // clears the barcode item
                    clear_promocode_input();

                    // focus back on the barcode input
                    $('#input_promo_code').focus();

                } else {
                    alert((data.message) ? data.message : 'Unable to retrieve promotion or voucher');
                }
            });
    }
};


/**
*
* clear_promocode_input()
* Clear any input on the promo voucher text
*
**/
function clear_promocode_input()
{
    $('#input_promo_code').val('');
}

/**
*
* pos_reset_form()
* Simply refresh the page to reset the form
**/
function pos_reset_form(confirm_with_user)
{
    if (!confirm_with_user || (confirm_with_user && confirm('Are you sure to reset this form?')))
        window.location = window.location;
}

/**
*
* pos_submit_form()
* Submit the form with validation
**/
function pos_submit_form(){
    if (pos_validate_form() && confirm('Complete this transaction?'))
        document.getElementById('form_pos').submit();
}


/**
*
* pos_validate_form()
* validate the POS form , return true or false
**/
function pos_validate_form()
{
    // 1. check negative change balance (incomplete payment)
    var change = $('#hid_change_amount').val();
    console.log(change);
    if (change < 0){
        alert('Incomplete Payment');
        return false;
    }

    // 2. check empty items
    if(!items_array || (items_array && items_array.length == 0)){
        alert('Please input at least 1 item');
        return false;
    }

    return true; // for now
}
