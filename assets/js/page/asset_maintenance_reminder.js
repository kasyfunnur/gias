var table = $('#maintenance_reminder').dataTable({
		'ajax': {	'url'  : 'ajax_datatable/maintenance_reminder',
					'data' :  function (d) {
							d.maint_group = $('#sel_reminder_group').val();
							d.pic_id = $('#sel_pic_id').val();
					}
				}});
$('#sel_reminder_group').change(function(){table.api().ajax.reload()});
$('#sel_pic_id').change(function(){table.api().ajax.reload()});