/*
 * BAR CHART
 * ---------
 */

var bar_data = {
    data: [
        ["January", 15.8],
        ["February", 18.5],
        ["March", 12.5],
        ["April", 14.6],
        ["May", 19.2],
        ["June", 20.8]
    ],
    color: "#3c8dbc"
};
$.plot("#bar-chart", [bar_data], {
    grid: {
        borderWidth: 1,
        borderColor: "#f3f3f3",
        tickColor: "#f3f3f3"
    },
    series: {
        bars: {
            show: true,
            barWidth: 0.5,
            align: "center"
        }
    },
    xaxis: {
        mode: "categories",
        tickLength: 0
    }
});
/* END BAR CHART */
