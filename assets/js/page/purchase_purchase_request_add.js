var default_row = $("#default");
//CUSTOM



$(document).ready(function(){
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			table_doc.row_index();
			table_doc.col();
			//table_doc.extra();
			table_doc.sum();
			//sky_tbl_refresh_row_num('table_detail');
			//sky_count_col_total('table_detail','doc_total','sub_total[]');
		}
	});	

	$("#location_id").on('change',function(){
		var result;
		for( var i = 0; i < source_location.length; i++ ) {
			if( source_location[i]['location_id'] == $(this).val() ) {
				result = source_location[i];
				break;
				$("#ship_addr").val(result['location_address']+', '+result['location_city']+', '+result['location_state']+', '+result['location_country']);
			}else{
				$("#ship_addr").val("");
			}
		}	
		$("#ship_addr").val(
			result['location_address']+', '+
			result['location_city']+', '+
			result['location_state']+', '+
			result['location_country']);
	});
	
	//INIT
	$("#location_id").val(1).change();	
	//pop_vendor();
	pop_inventory();
	table_doc.init('table_detail',default_row);
	table_doc.init_row('sub_total','ast_qty','','ast_price');
	table_doc.init_col('doc_subtotal','sub_total[]');
	table_doc.init_col('qty_total','ast_qty[]');
	table_doc.row_index();
});


//JS CUSTOM PAGE
var po_default_row = '<tr><td><input type="checkbox" name="chk[]" id="chk"/></td><td><input type="hidden" name="doc_id[]" id="doc_id" /><span></span></td><td></td><td></td><td></td><td></td></tr>';
function table_detail_reset(){
	$("#table_detail > tbody > tr").not('tr:last').remove();
	table_doc.row_index();
	//sky_tbl_refresh_row_num("table_detail");
	//$('#table_detail > tbody').append(po_default_row);
}
function table_detail_insert(data){		
	if(data){
		table_doc.row_del_last();
		for(var p = 0;p<data.length;p++){
			var row = table_doc.clone();
			row.find('a[name^=a_ast_code]').attr('href',url_1+'/'+data[p]["item_code"]);
			row.find('input[name^=ast_code]').val(data[p]['item_code']);
			row.find('input[name^=item_id]').val(data[p]['item_id']);
			row.find('input[name^=ast_name]').val(data[p]['item_name']);
			row.find('input[name^=ast_qty]').val(1);
			row.find('input[name^=ast_price]').val(data[p]['item_buy_price']);	
		}
		table_doc.row();
		table_doc.clone();
		table_doc.row_del_first('ast_code');
		table_doc.row_index();
		table_doc.col();
		table_doc.sum();
	}else{
		console.log('error');	
	}	
}

function pop_inventory(){
	dialog_inventory_reset_table('dialog_inventory_list');
	dialog_inventory_populate_data('dialog_inventory_list',source_inventory);
}

/*function pop_vendor(){	
	dialog_vendor_reset_table('dialog_vendor_list');
	dialog_vendor_populate_data('dialog_vendor_list',source_vendor);
}*/

/*function dialog_vendor_pull_data(data_vendor){
	$("#buss_name").val(data_vendor['buss_name']);
	$("#buss_id").val(data_vendor['buss_id']);
	$("#buss_char").val(data_vendor['buss_char']).change();
	$("#bill_addr").val(data_vendor['buss_addr']+' '+data_vendor['buss_city']+' '+data_vendor['buss_country']);
}*/

function dialog_inventory_pull_data(data_inventory){ 
	var data_inventory_param = $.map(data_inventory, function(value, index) {
		return [value];
	});
	var data_ajax_raw = $.ajax({
		url:ajax_url_1,
		data: {inventory:data_inventory_param},
		dataType: "json",
		success: function(result){
			table_detail_insert(result);
		}
	});	
}