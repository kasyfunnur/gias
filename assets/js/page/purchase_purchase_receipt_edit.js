var default_row = $("#default");

$(document).ready(function(){
	$(".button_action").html($('template.button_action').html());
	$(document).on('click','button[name="act_del"]',function(){
		var xx = $(this).parent().parent().parent().parent();
		if(! xx.is(":last-child")){
			xx.remove();
			table_doc.row_index();
			table_doc.col();
			table_doc.sum();
		}
	});	

	
	
	//INIT
	//$("#location_id").val(1).change();	
	pop_vendor();
	table_doc.init('table_detail',default_row);
	table_doc.init_row('sub_total','ast_qty','','ast_price');
	table_doc.init_col('doc_subtotal','sub_total[]');
	table_doc.init_col('qty_total','ast_qty[]');
	table_doc.row_index();
	
	//ON EDIT PAGE
	$("#location_id").val(form_header[0]['whse_id']).change();
	$("#buss_id").val(form_header[0]['buss_id']);
	$("#buss_name").val(form_header[0]['buss_name']);
	$("#doc_ref").val(form_header[0]['doc_ref']);
	$("#doc_note").val(form_header[0]['doc_note']);
	
	//if(status_logistic != "PENDING"){
		$('#table_detail > tbody > tr > td > input').attr('disabled',true);
		$('#table_detail > tbody > tr > td > input').css('color','#000');
		$('#table_detail > thead > tr').find('th:eq(6)').hide();
		$('#table_detail > tbody > tr').find('td:eq(6)').hide();
		$('#modal_item,#modal_vendor').attr('disabled',true);
		$('#modal_item,#modal_vendor').css('color','#000');
		$('#buss_name,#location_id,#doc_dt,#doc_ddt').attr('disabled',true);
		$('#buss_name,#location_id,#doc_dt,#doc_ddt').css('color','#000');
		//$('button[type="submit"]').attr('disabled',true);
		$('input[type="number"]').attr('type','text');
	//	}

	
	if(form_header[0]['doc_status'] == 'ACTIVE'){
		$(".control, .form-control").attr('disabled',true);
		$("#next").attr('disabled',false);
		$("#view").attr('disabled',false);
		$("#action").attr('disabled',false);	
	}
	if(form_header[0]['doc_status'] == 'CANCELLED'){
		$(".control, .form-control").attr('disabled',true);
		$("#new").attr('disabled',false);
	}
	if(form_header[0]['doc_status'] == 'CLOSED'){
		$(".control, .form-control").attr('disabled',true);
		$("#new").attr('disabled',false);
		$("#View").attr('disabled',false);
	}

	table_detail_insert(form_detail);	
});

/*function validateForm(){
	if(confirm("Are you sure?")==true){
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){	
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}*/


//JS CUSTOM PAGE
function table_detail_reset(){
	$("#table_detail > tbody > tr").not('tr:last').remove();
	//sky_tbl_refresh_row_num("table_detail");
	table_doc.row_index();
}
function table_detail_insert(data){		
	if(data){
		table_doc.row_del_last();
		for(var p = 0;p<data.length;p++){
			var row = table_doc.clone();
			row.find('a[name^=a_ast_code]').attr('href',url_1+'/'+form_detail[p]["item_code"]);
			row.find('input[name^=ast_code]').val(form_detail[p]['item_code']);
			row.find('input[name^=item_id]').val(form_detail[p]['item_id']);
			row.find('input[name^=ast_name]').val(form_detail[p]['item_name']);
			row.find('input[name^=ast_open_qty]').val(form_detail[p]['item_qty']);
			row.find('input[name^=ast_qty]').val(form_detail[p]['item_qty']);		
		}
		table_doc.row();
		table_doc.clone();	
		table_doc.row_del_first('ast_code');	
		table_doc.row_index();
		table_doc.col();
		table_doc.extra();
		table_doc.sum();	
	}else{
		console.log('error');	
	}	
}

////////POP FUNCTION
////VENDOR
function pop_vendor(){ 
	dialog_vendor_reset_table('dialog_vendor_list');
	dialog_vendor_populate_data('dialog_vendor_list',source_vendor);
}
////PO
function pop_purchase_order(){
	return $.ajax({
		url:ajax_url_1,
		data:{buss_id:$("#buss_id").val()},
		success: function(response) {
			dialog_purchase_order_reset_table('dialog_purchase_order_list');
			dialog_purchase_order_populate_data('dialog_purchase_order_list',JSON.parse(response));
        }
	});
}

////////PULL DATA
////VENDOR
function dialog_vendor_pull_data(data_vendor){
	$("#buss_name").val(data_vendor['buss_name']);
	$("#buss_id").val(data_vendor['buss_id']);
	$("#buss_char").val(data_vendor['buss_char']).change();
	$("#bill_addr").val(data_vendor['buss_addr']+' '+data_vendor['buss_city']+' '+data_vendor['buss_country']);
	pop_purchase_order();
}
////PO
function dialog_purchase_order_pull_data(data_purchase_order){
	$("#doc_ref").val(data_purchase_order.doc_num);
	$("#doc_ref_id").val(data_purchase_order.doc_id);
	get_po_detail();	
}
