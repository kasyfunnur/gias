// prevent error in IE when debugging with console.log
if ( ! window.console ) console = { log: function(){} };

///////////////PAGE DOCUMENT///////////////
var rowCount = 1;
var default_row = $("#default");
function clone_row(table){
	rowCount++;
	var row = $("#"+table+" > tbody > tr:first").clone().find("input,select").each(function(){		
		var temp_name = $(this).attr('name'); //console.log(temp_name);
		$(this).attr('id',function(_,id){return temp_name.substring(0,temp_name.length-2)+rowCount;});
		$(this).attr('name',function(_,name){return name;});
		$(this).val('');
	}).end().appendTo("#"+table+" > tbody");
	sky_tbl_refresh_row_num(table);
	return row;
}
function sky_tbl_refresh_row_num(tableid){
	$('#'+tableid+' > tbody > tr').each(function(){
		var t_idx = $(this).index()+1;
		$(this).find('td:eq(0) > input[name="row[]"]').val(t_idx);
	});	
}
function sky_count_row_total(table,obj,qty,price,total){	
	$("#"+table+" > tbody > tr").each(function(){
		var name = $(this).find('td:eq(0)').find('input').attr('name');
		var id = $(this).find('td:eq(0)').find('input').attr('id');
		var cnt = id.substring(name.length-2,name.length);
		var temp_qty = $("input[id^='"+qty+cnt+"']").val();
		var temp_price = $("input[id^='"+price+cnt+"']").val();
		var temp_total = temp_qty * temp_price;
		$("input[id^='"+total+cnt+"']").val(temp_total);
	});
}
function sky_count_col_total(table,target,col){
	var tbl 	= $("#"+table+ " > tbody");
	var tbl_row = tbl[0].rows.length;
	var col_arr = $("input[name='"+col+"']");
	var total 	= 0;
	for(var q = 0; q < tbl_row-1; q++){
		total += parseInt($(col_arr[q]).val());
	}
	$("#"+target).val(total);	
}
/*function sky_count_doc_summary(){
	var var_subtotal 	= $("#doc_subtotal").val();
	var pct 			= $("#doc_disc_pct").val();
	//var var_disc		= $("#doc_disc").val((pct/100)*$("#doc_total").val());
	$("#doc_disc").val((pct/100)*$("#doc_subtotal").val());
	var var_disc 		= $("#doc_disc").val();
	var var_total		= $("#doc_total").val(var_subtotal - var_disc);
}*/
///////////////PAGE DOCUMENT///////////////

$.fn.extend({ 
	disableSelection: function() { 
		this.each(function() { 
			if (typeof this.onselectstart != 'undefined') {
				this.onselectstart = function() { return false; };
			} else if (typeof this.style.MozUserSelect != 'undefined') {
				this.style.MozUserSelect = 'none';
			} else {
				this.onmousedown = function() { return false; };
			}
		}); 
	} 
});

/*  ////////////////////////////////////////////////////////////////////////////////////////////////
	// JS Object (14.10.14)
	// This is the object for detail document operation, such as cal total, select choices, etc.
	// 1. TABLE SELECTION
	// 2. TABLE DOCUMENT
	////////////////////////////////////////////////////////////////////////////////////////////////
*/
////////////////JM: TABLE SELECTION//////////////////////

//tes
function dialog_mgr(name,pull){
	this.name = name;
	this.pull = pull;
	this.data_push = function(data){
		console.log(this.pull);
		window[this.pull](data);
		//alert('pushhed');
	},
	this.data_pull = function(data){
		this.data_push(data);
	}
}


var table_sel = {
	init: function(name,type){
		this.last_selrow = 0;
		this.name = name;
		this.trs = document.getElementById(name).tBodies[0].getElementsByTagName('tr');
		this.type = type;
		var that = this;
		$(document).on('click','#'+this.name+ ' tbody > tr',function(event){
			//alert('sefs');			
			that.handle_click(this.rowIndex,event);
		});
	},	
	handle_click: function(row,e){ 
		if (window.event.ctrlKey) {
			this.toggleRow(row,event,false);
		}	
		if (window.event.button === 0) {
			if (!window.event.ctrlKey && !window.event.shiftKey) {
				this.toggleRow(row,event,false);
			}		
			if (window.event.shiftKey) {
				this.select_range([this.last_selrow, row],event);
			}
		}
	},
	clearAll: function(){
		for (var i = 0; i <this.trs.length; i++) {
			this.trs[i].className = '';
		}
	},
	toggleRow: function(row,e,range){	
		this.last_selrow = row;
		
		$(this.trs[row-1]).toggleClass('selected');
		if(e.target.type !== 'checkbox' || range){			
			$(':checkbox',this.trs[row-1]).prop('checked',!$(':checkbox',this.trs[row-1]).prop('checked'));
		}
	},
	select_range: function(indexes,e){
		indexes.sort(function(a, b) {
			return a - b;
		});	
		//var f
		for (var i = indexes[0]+1; i <= indexes[1]-1; i++) {
			this.toggleRow(i,e,true);
		}
		this.toggleRow(indexes[1],e,false);
	}	
}

var table_doc = {	
	init:function(name,def_row){
		this.name = name;
		this.table = document.getElementById(name);
		this.default_row = def_row;
		this.default_row = $('#'+this.name+ '> tbody ').find("tr[id='default']");
		$(document).on('click','#'+name+ ' tbody > tr',function(event){
			//that.handle_click(this.rowIndex,event);
		});

		/*$(document).on('change','#'+name+ ' tbody > tr:last',function(e){
			if(e.target.type != 'select-one'){
				table_doc.clone();
				table_doc.row_index();
			}			
		});*/
		$(document).on('click','#'+name+ ' input.form-control, input[type=number]',function(){ 
			$(this).select();
		});
		$(document).on('change','#'+name,function(){
			table_doc.row();
			table_doc.col();	
			table_doc.extra();	
			table_doc.sum();
		});
	},
	col:function(){
		for(var x=0;x<table_doc.col_num;x++){
			var tbl 	= $("#"+this.name+ " > tbody");
			var tbl_row = tbl[0].rows.length;
			var col_arr = $("input[name='"+table_doc.col_col[x]+"']");
			var total 	= 0;
			for(var q = 0; q < tbl_row-1; q++){ console.log($(col_arr[q]));
				total += parseFloat($(col_arr[q]).val() || 0);
			}
			//$("#"+table_doc.col_target[x]).val(total.toFixed(2));
			//$("input[name^="+table_doc.col_target[x]+"]").val(total.toFixed(2));
			$("input[name^="+table_doc.col_target[x]+"]").val(total);
			//.toFixed(2)
			console.log(total);
			//$("#"+table_doc.col_target[x]).html(accounting.formatMoney(total,"",0,",","."));
		}
	},	
	row:function(){ 
		$("#"+this.name+" > tbody > tr").each(function(i,e){ 
			for(var x=0;x<table_doc.row_num;x++){
				/*var a = $(e).find("input[name^='"+table_doc.qty+"']").val() || 0;
				var b = $(e).find("input[name^='"+table_doc.price+"']").val() || 0;
				$(e).find("input[name^='"+table_doc.target+"']").val(a * b);*/
				var a = $(e).find(":input[name^='"+table_doc.row_qty[x]+"']").val() || 0;
				var b = $(e).find(":input[name^='"+table_doc.row_price[x]+"']").val() || 0;
				//console.log(table_doc.row_op[x]);

				switch(table_doc.row_op[x]) {
				    case "*":
				        var result = (a*b) || 0;
				        break;
				    case "+":
				    	//var result = a+b || 0;
				        var result = eval(a)+eval(b) || 0;
				        break;
				    case "-":
				    	//var result = a-b || 0;
				        var result = eval(a)-eval(b) || 0;
				        break;
				    case "/":
				        var result = (a/b) || 0;
				        break;
				    default:
				        var result = (a*b) || 0;
				}
				//var result = a*b || 0;
				//console.log(result.toFixed(2));
				$(e).find("input[name^='"+table_doc.row_target[x]+"']").val(result.toFixed(2));
			}
		});
	},
	row_num: 0, row_target: [], row_price: [], row_qty: [], row_op: [],
	//init_row:function(target,qty,opt,price){
	init_row:function(target,qty,op,price){
		this.row_target[table_doc.row_num] = target;
		this.row_qty[table_doc.row_num] = qty;
		this.row_op[table_doc.row_num] = op;
		this.row_price[table_doc.row_num] = price;
		this.row_num++;
		/*this.target = target;
		this.qty = qty;
		this.price = price;*/
	},
	col_num: 0, col_target: [], col_col: [],
	init_col:function(target,col){				
		this.col_target[table_doc.col_num] = target;
		this.col_col[table_doc.col_num] = col;
		this.col_num++;
	},
	row_reset:function(){
		$("#"+this.name+" > tbody > tr").not('tr:last').remove();
		sky_tbl_refresh_row_num(this.name);
	},
	row_del_first:function(var_name){
		if($('#'+this.name+' > tbody > tr:first').find('input[name^='+var_name+']').val() == ""){
			$('#'+this.name+' > tbody > tr:first').remove();
		}
	},
	row_del_last:function(){
		if($('#'+this.name+' > tbody > tr').length != 1){
			$('#'+this.name+' > tbody > tr:last').remove();
		}
	},
	row_index:function(){
		$('#'+this.name+' > tbody > tr').each(function(){
			var t_idx = $(this).index()+1;
			$(this).find('td:eq(0) > input[name="row[]"]').val(t_idx);
		});	
	},
	col_index:function(){
		//$('#'+this.name+' > tbody > tr')
	},
	clone:function(){
		rowCount++;
//		var row = $("#"+this.name+" > tbody > tr:first").clone().find("input,select").each(function(){		
//			var temp_name = $(this).attr('name');
//			$(this).attr('id',function(_,id){return temp_name.substring(0,temp_name.length-2)+rowCount;});
//			$(this).attr('name',function(_,name){return name;});
//			$(this).val('');
//		}).end().appendTo("#"+this.name+" > tbody");
		var row = $(this.default_row).clone().find("input,select").each(function(){
			var temp_name = $(this).attr('name');
			//$(this).attr('id',function(_,id){return temp_name.substring(0,temp_name.length-2)+rowCount;});
			//$(this).attr('name',function(_,name){return name;});
			$(this).val('');
		}).end().appendTo("#"+this.name+" > tbody");
		//this.typeahead();
		this.row_index();
		return row;
	},
	
	
	sum:function(){
		var var_subtotal 	= $("#doc_subtotal").val();
		var pct 			= $("#doc_disc_pct").val(); 
		$("#doc_disc").val((pct/100)*$("#doc_subtotal").val());
		var var_disc 		= $("#doc_disc").val();
		//console.log(var_subtotal);console.log(pct);
		var total   		= var_subtotal - var_disc; 		
		var var_total		= $("#doc_total").val(total.toFixed(2));
	},
	extra:function(){
		if($(window.event.target).attr('id') != 'doc_disc_pct'){
			/*if($("#qty_total").val() >= 3){
				var pct = $("#doc_disc_pct").val(15);			
			}else{
				var pct = $("#doc_disc_pct").val(0);
			}*/
		}		
	}
}

//var bloodhound =


$(".date-mask").inputmask({"alias":"date"});
$(".currency-mask").inputmask(
	{'alias': 'integer', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': true, 'prefix': 'IDR ', 'placeholder': '0','autoUnmask':true , 'removeMaskOnSubmit':true}
	);
	
$.extend($.fn.dataTable.defaults,{"processing": true,"serverSide": true});
function add_asset(serial){// this will need to go , use the method used in move request
	$('#asset_requested').val($('#asset_requested').val()+serial+';');
	}

	// IP, save opened menu into a cookie to retain menu state (open/close)
	function setActiveMenu(){
		var _am = [];
		var _amlastnode;
		
		var me = ($(this).parent("li[data-lastnode='1']").length) ? $(this).parent("li[data-lastnode='1']").attr('data-menuid') : false;
		_amlastnode = (me !== false && _am.indexOf(me) == -1) ? me : "";
		
		$("#sidebar li[class*='active']").each(function(index){
			_am.push($(this).attr('data-menuid'));
		});
		var d = new Date();
		d.setTime(d.getTime() + (1*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = '_am=' + _am.join(',') + '; ' + expires + '; path=/';
		document.cookie = '_amlastnode=' + _amlastnode + '; ' + expires + '; path=/';
          }
	$(document).ready(function() {
		$('#sidebar li a').bind("click", setActiveMenu);
	});

function sky_curr(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}
function sky_num(input) {
    var output = input
    if (parseFloat(input)) {
        input = new String(input); // so you can perform string operations
        var parts = input.split("."); // remove the decimal part
        parts[0] = parts[0].split("").reverse().join("").replace(/(\d{3})(?!$)/g, "$1,").split("").reverse().join("");
        output = parts.join("");
    }
    return Number(output);
}
