// JavaScript Document

function status_label(group,status){
	var temp = status_parsing(group,status);
	return '<span class="label '+temp.label+'">'+temp.text+'</span>';
}

function status_icon(group,status){
	var temp = status_parsing(group,status);
	return 
		'<span class="label label-xs '+temp.label+'"><i class="fa '+temp.fa+'"></i>'+temp.text+'ssfsef </span>';
}

function status_parsing(group,status){ //(logistic,received,label/icon)
	var status_attr = new Object();
	switch(group){
		case "logistic":
			status_attr.fa	= 'fa-truck';
			break;
		case "billing":
			status_attr.fa	= 'fa-envelope';
			break;
		case "financial":
			status_attr.fa	= 'fa-dollar';
			break;
		default:
			status_attr.fa	= 'fa-file-text';				
	}	
	status_attr.text	= status;
	//status_attr.group	= group;
	switch(status){
		//logistic
		case "PENDING":
			status_attr.label = 'label-danger';
			break;
		case "PARTIAL":
			status_attr.label = 'label-warning';
			break;
		case "RECEIVED":
			status_attr.label = 'label-success';
			break;
		//billing
		case "UNBILLED":
			status_attr.label = 'label-danger';
			break;
		case "BILLED":
			status_attr.label = 'label-success';
			break;
		
		//financial
		case "UNPAID":
			status_attr.label = 'label-default';
			break;			
		case "DP":
			status_attr.label = 'label-warning';
			break;
		case "PAID":
			status_attr.label = 'label-success';
			break;
		case "OVERDUE":
			status_attr.label = 'label-danger';
			break;
		default:
			status_attr.label = 'label-info';
	}
	return status_attr;		
}