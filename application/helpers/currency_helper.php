<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*SWLayout:
(c) 2014 - Indo Skyware.*/

function currency_format($curr, $amount)
{
	$CI =& get_instance();

	$str = '';

	if(! is_numeric($amount) )
		$amount = 0;

	if($amount < 0)
		$str .= '(';


	$CI->load->model('currency_model');
	$currency = $CI->currency_model->get($curr);

	if(empty($curr) || $curr = NULL || empty($currency)){
		$curr = '';
		$dec_places = 2;
		$thousand_spt = ',';
		$decimal_spt = '.';
	} else {
		$curr = $currency['curr_symbol'];
		$dec_places = $currency['curr_decimal'];
		$thousand_spt = ',';
		$decimal_spt = '.';
	}

	$str .= $curr . ' ' . number_format($amount, $dec_places, $decimal_spt, $thousand_spt);

	if($amount < 0)
		$str .= ')';

	return $str;

}

function currency_select($selected_curr = NULL, $attributes = NULL)
{
	$CI =& get_instance();
	$CI->load->model('currency_model');
	$currencies = $CI->currency_model->get();

	if($attributes == NULL || empty($attributes))
	{
		$attributes = array();
	}

	$attributes['name'] = (array_key_exists('name', $attributes)) ? $attributes['name'] : 'sel_curr';
	$attributes['id'] = (array_key_exists('id', $attributes)) ? $attributes['id'] : 'sel_curr';
	$attributes['class'] = (array_key_exists('class', $attributes)) ? $attributes['class'] : 'form-control';

	$str = '<select name="'.$attributes['name'].'" id="'.$attributes['id'].'" class="'.$attributes['class'].'">';

	foreach ($currencies as $curr) {
		$str .= '<option value="'.$curr['curr_code'].'">'.$curr['curr_code'].'</option>';
	}

	$str .= '</select>';

	return $str;
}