<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*SWLayout:
(c) 2014 - Indo Skyware.*/

//BUTTON ACTION
function form_button_action($label,$type,$list){
	$returndata = '
		<div class="btn-group dropdown">
	      	<button type="button" id="'.$label.'" class="btn btn-'.$type.' dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	        	'.$label.' <span class="caret"></span>
	      	</button>';
		$returndata .= '<ul class="dropdown-menu" role="menu">';
	   	foreach($list as $ls){
	   		$returndata .= '<li><a href="'.$ls['link'].'" target="_blank"><i class="fa fa-'.$ls['icon'].'"></i> '.$ls['text'].'</a></li>';
	   	}
	   	$returndata .= '</ul>';
	    $returndata .= '</div>';
	return $returndata;
}


//INPUT FIELD
function form_text($label,$type,$name,$value="",$place=""){
	/*$returndata = '<tr class="info">
		<td>'.$label.'</td>
		<td class="value"><input class="form-control input-sm" type="'.$type.'" 
			name="'.$name.'" id="'.$name.'" value="'.$value.'"
			placeholder="'.$place.'"/></td>
	</tr>';*/
	$returndata = '
		<div class="col-xs-5 text-nowrap">'.$label.'</div>';
		if($type == 'textarea'){
			$returndata .='<div class="col-xs-7"><textarea ';
		}else{
			$returndata .='<div class="col-xs-7"><input ';
		}
		$returndata .= 'class="form-control input-sm" type="'.$type.'" 
			name="'.$name.'" id="'.$name.'" placeholder="'.$place.'"';
		
		if($type == 'textarea'){
			$returndata .='>'.$value.'</textarea></div>';
		}else{
			$returndata .=' value="'.$value.'" /></div>';
		}
	return $returndata;
}
function form_text_ro($label,$type,$name,$value="",$place=""){
	/*$returndata = '<tr class="info">
		<td>'.$label.'</td>
		<td class="value"><input class="form-control input-sm" type="'.$type.'" 
			name="'.$name.'" id="'.$name.'" value="'.$value.'" readonly
			placeholder="'.$place.'"/></td>
	</tr>';*/
	$returndata = '
		<div class="col-xs-5 text-nowrap">'.$label.'</div>';
		if($type == 'textarea'){
			$returndata .='<div class="col-xs-7"><textarea ';
		}else{
			$returndata .='<div class="col-xs-7"><input ';
		}
		$returndata .= 'class="form-control input-sm" type="'.$type.'" 
			name="'.$name.'" id="'.$name.'" placeholder="'.$place.'" readonly';
		
		if($type == 'textarea'){
			$returndata .='>'.$value.'</textarea></div>';
		}else{
			$returndata .=' value="'.$value.'" /></div>';
		}
	return $returndata;
}

function sw_textButton($txtName,$txtPlace,$btnName,$btnText,$icon=NULL){
	if(!$icon)$icon == 'default';
	$returndata = '<input type="text" class="form-control input-sm" id="'.$txtName.'" name="'.$txtName.'" value="" placeholder="'.$txtPlace.'">
                    	<span class="input-group-btn">
							<button id="'.$btnName.'" name"'.$btnName.'" type="button" class="btn btn-warning" style="width:100%;">
                            	<i class="fa fa-'.$icon.'"></i>&nbsp;&nbsp; '.$btnText.'</button>
						</span>';
	return $returndata;
}

function sw_altButton($text,$icon){
	$returndata = '<span class="hidden-xs"><i class="fa '.$icon.'"></i>&nbsp;&nbsp;'.$text.'</span>
						<span class="visible-xs"><i class="fa '.$icon.'"></i></span>';
	return $returndata;
}


//MODAL
function sw_createModal($name,$label,$function,$file_load){
	$CI = get_instance();
	$returndata = '<div class="modal fade" id="'.$name.'"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
		<div class="modal-dialog" >
			<div class="modal-content" >
				<div class="modal-header">
					<h4 class="modal-title">'.$label.'
						<div class="pull-right">                        
							<button type="button" class="btn btn-default" data-dismiss="modal">
								<span class="hidden-xs"><i class="fa fa-arrow-left"></i> Back</span>
								<span class="visible-xs"><i class="fa fa-arrow-left"></i></span>
							</button>
							<button type="button" class="btn btn-primary" data-dismiss="modal"
								onclick="'.$function.'">
								<span class="hidden-xs"><i class="fa fa-save"></i> Save & Close</span>
								<span class="visible-xs"><i class="fa fa-save"></i></span>
							</button>
						</div>
					</h4>                
				</div>
				<div class="modal-body">
				'.$CI->load->view($file_load,'',true).'				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						<span class="hidden-xs"><i class="fa fa-arrow-left"></i> Back</span>
						<span class="visible-xs"><i class="fa fa-arrow-left"></i></span>
					</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" 
						onclick="'.$function.'">
						<span class="hidden-xs"><i class="fa fa-save"></i> Save & Close</span>
						<span class="visible-xs"><i class="fa fa-save"></i></span>
					</button>
				</div>
			</div>
		</div>
	</div>';
	return $returndata;
}

//Textbox Type
function sw_createBox($title){					
	$returndata = '<div class="box box-primary">';
	$returndata .= '<div class="box-header">';
	$returndata .= '<h3>&nbsp;&nbsp;'.$title.'</h3>';
	$returndata .= '</div>';
	return $returndata;
}

function sw_createTable($id,$column,$data,$config=NULL){
	$returndata = '<table class="table table-condensed" id="'.$id.'">';
	$returndata .= '<thead><tr>';
	for($x=0;$x<count($column);$x++){
		$returndata .= '<td>'.$column[$x].'</td>';
	}
	$returndata .= '</tr></thead>';
	$returndata .= '<tbody>';
	for($y=0;$y<count($data);$y++){
		$returndata .= '<tr>';	
		
		$returndata .= '</tr>';	
	}
	$returndata .= '';	
}

function sw_CreateSelect($selectname, $arraydata, $keycolumn, $displaycolumn, $selecteddata = NULL, $options = NULL){
	if(!$selectname) return '[sw_CreateSelect] : Select Name is not provided';
	if(!$keycolumn) return '[sw_CreateSelect] : Key Column is not provided';
	if(!$displaycolumn) return '[sw_CreateSelect] : Display Column is not provided';
	if(!is_array($arraydata)) return '[sw_CreateSelect] : provided data is not an array';
	// if(count($arraydata) <= 0) return '[sw_CreateSelect] : provided data is empty';	
	// IND, should return if empty also because we have 'none' default in initalvalue and it must show

	if($options){
		if(count($arraydata) <= 0 && !array_key_exists('initialvalue',$options) &&
			!array_key_exists('initialdisplay',$options)) return '[sw_CreateSelect] : provided data is empty';
	}
	if(count($arraydata) > 0 && !array_key_exists($keycolumn, $arraydata[0])) return '[sw_CreateSelect] : Keycolumn "' . htmlentities($keycolumn) . '" does not exists in provided data';

	// render select box
	if(is_array($selectname)){
		$returndata = '<select name="' . htmlentities($selectname[0]) . '" id="' . htmlentities($selectname[1]) . '" class="form-control input-sm selection">' ;
	}else{
		$returndata = '<select name="' . htmlentities($selectname) . '" id="' . htmlentities($selectname) . '" class="form-control input-sm selection">' ;
	}
	
	//$returndata = '<select name="' . htmlentities($selectname) . '" id="' . htmlentities($selectname) . '" class="form-control input-sm selection">' ;
	if( is_array($options) &&
		array_key_exists('initialvalue',$options) &&
		array_key_exists('initialdisplay',$options))
			$returndata .= '<option value="'. htmlentities($options['initialvalue']) .'">'. htmlentities($options['initialdisplay']) .'</option>';
	
	// loop data for options
	/*if($selecteddata == NULL){
		$returndata .= '<option value="-1"';		
		$returndata .= '>Select..</option>';
	}*/
	if(!is_null($arraydata)){
		foreach($arraydata as $row){	
			$returndata .= '<option value="' . htmlentities($row[$keycolumn]) . '"';
			if($selecteddata && $row[$keycolumn] == $selecteddata) $returndata .= ' selected';
			$returndata .= '>';
			if(is_array($displaycolumn)){
				foreach($displaycolumn as $i=>$col){
					$returndata .= ($i==0)?"":" - ";
					$returndata .= htmlentities($row[$col]);			
				}
			}else{
				$returndata .= htmlentities($row[$displaycolumn]);
			}
			
			$returndata .= '</option>';
		}
	}
	
	$returndata .= '</select>';		
	return $returndata;
}

function sw_CreateDropdown($selectname, $arraydata, $keycolumn, $displaycolumn, $selecteddata = NULL, $options = NULL){
	if(!$selectname) return '[sw_CreateSelect] : Select Name is not provided';
	if(!$keycolumn) return '[sw_CreateSelect] : Key Column is not provided';
	if(!$displaycolumn) return '[sw_CreateSelect] : Display Column is not provided';
	if(!is_array($arraydata)) return '[sw_CreateSelect] : provided data is not an array';
	// if(count($arraydata) <= 0) return '[sw_CreateSelect] : provided data is empty';	
	// IND, should return if empty also because we have 'none' default in initalvalue and it must show

	if($options){
		if(count($arraydata) <= 0 && !array_key_exists('initialvalue',$options) &&
			!array_key_exists('initialdisplay',$options)) return '[sw_CreateSelect] : provided data is empty';
	}
	if(count($arraydata) > 0 && !array_key_exists($keycolumn, $arraydata[0])) return '[sw_CreateSelect] : Keycolumn "' . htmlentities($keycolumn) . '" does not exists in provided data';

	// render select box
	if(is_array($selectname)){
		$returndata = '<select name="' . htmlentities($selectname[0]) . '" id="' . htmlentities($selectname[1]) . '" class="form-control selection">' ;
	}else{
		$returndata = '<select name="' . htmlentities($selectname) . '" id="' . htmlentities($selectname) . '" class="form-control selection">' ;
	}
	
	//$returndata = '<select name="' . htmlentities($selectname) . '" id="' . htmlentities($selectname) . '" class="form-control input-sm selection">' ;
	if( is_array($options) &&
		array_key_exists('initialvalue',$options) &&
		array_key_exists('initialdisplay',$options))
			$returndata .= '<option value="'. htmlentities($options['initialvalue']) .'">'. htmlentities($options['initialdisplay']) .'</option>';
	
	// loop data for options
	/*if($selecteddata == NULL){
		$returndata .= '<option value="-1"';		
		$returndata .= '>Select..</option>';
	}*/
	if(!is_null($arraydata)){
		foreach($arraydata as $row){	
			$returndata .= '<option value="' . htmlentities($row[$keycolumn]) . '"';
			if($selecteddata && $row[$keycolumn] == $selecteddata) $returndata .= ' selected';
			$returndata .= '>';
			if(is_array($displaycolumn)){
				foreach($displaycolumn as $i=>$col){
					$returndata .= ($i==0)?"":" - ";
					$returndata .= htmlentities($row[$col]);			
				}
			}else{
				$returndata .= htmlentities($row[$displaycolumn]);
			}
			
			$returndata .= '</option>';
		}
	}
	
	$returndata .= '</select>';		
	return $returndata;
}