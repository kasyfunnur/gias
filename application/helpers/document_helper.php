<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

function get_ref_number($document_type = 'asset_request'){
	$CI = &get_instance();
	$CI -> load   -> database();
	$CI -> config -> load ('document');
	$document = $CI -> config -> item('document');
	
	$date = date('dmy');
	$doc  = $document[$document_type];
	$CI -> db -> order_by("timestamp", "desc"); 
	$CI -> db ->like($doc['ref_field'],$doc['prefix'].$date,'after'); 
	$query  = $CI -> db -> get ($doc['table']);
	$result = $query -> row_array();
	$last_number = ($query -> num_rows > 0) ? substr($result[$doc['ref_field']],-$doc['digit']) : 0;
	return $doc['prefix'].$date.(sprintf("%0".$doc["digit"]."d",(int)$last_number + 1));
	}
?>