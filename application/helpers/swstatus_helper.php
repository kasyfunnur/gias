<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*SWStatus:
(c) 2015 - IndoSkyware.*/

	function date_single($label=NULL,$object_name=NULL,$data=NULL){
		if(!$label || $label=="" || $label==NULL) $label = 'Date';
		if(!$object_name || $object_name=="" || $object_name==NULL) $object_name = 'date';
		if(!$data || $data=="" || $data==NULL) $data = date('Y-m-d');
		$returndata = '
			<tr class="info">
				<td>'.$label.'</td>
				<td class="value">
					<input type="date" class="form-control" name="'.$object_name.'" id="'.$object_name.'" value="'.$data.'">
				</td>  
			</tr>';//
		return $returndata;
	}
	function doc_status($status){
		$status_attr = array();
		$status_attr['status'] = $status;
		switch($status){
			case "DRAFT":
				$status_attr['color'] = 'default';
				break;
			case "APPROVED":
				$status_attr['color'] = 'warning';
				break;
			case "ACTIVE":
				$status_attr['color'] = 'warning';
				break;
			case "CLOSED":
				$status_attr['color'] = 'success';
				break;
			case "CANCELLED":
				$status_attr['color'] = 'danger';
				break;
			case "REJECTED":
				$status_attr['color'] = 'danger';
				break;


			case "UNPAID":
				$status_attr['color'] = 'danger';
				break;
			case "PARTIALLY PAID":
				$status_attr['color'] = 'warning';
				break;
			case "PAID":
				$status_attr['color'] = 'success';
				break;				
			default:
				$status_attr['color'] = 'info';
		}
		return $status_attr;
	}

