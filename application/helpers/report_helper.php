<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

function asset_list_select_group($select_properties = array(), $selected_id = NULL){
	if( is_array($select_properties) && count($select_properties) > 0){
		$CI = & get_instance();
		$CI -> load -> database();
		$CI -> load -> model('asset_model');
		
		$options = $CI -> asset_model -> get_asset_group();
		
		$attributes = " ";
		foreach ( $select_properties as $attribute=>$value)
		{
			$attributes .= 	$attribute . '="' . htmlspecialchars($value) . '"' ;
		}
		
		if(!is_array($selected_id))
			$selected_id = ($selected_id !== NULL) ? array($selected_id) : array() ;
		
		$string = "<select $attributes>";
		
		$selected = (in_array(0,$selected_id)) ? 'selected' : '';
		$string .= '<option value="0" '.$selected.'>-- All --</option>';

		foreach ($options as $opt){
			$selected = (in_array($opt['group_id'],$selected_id)) ? 'selected' : '';
			$string .= "<option value=\"$opt[group_id]\" $selected>&nbsp;&nbsp;" . htmlspecialchars($opt['group_tag']) . ' - ' . htmlspecialchars($opt['group_name']) . '</option>';
			
		}

		$string .= "</select>";
		
		echo $string;
		
	} else {
		echo "report_helper : select properties has to be in associative array format";
	}
	
}


function asset_move_select_location($select_properties = array(), $selected_id = NULL){
	if( is_array($select_properties) && count($select_properties) > 0){
		$CI = & get_instance();
		$CI -> load -> database();
		$CI -> load -> model('location_model');
		
		$options = $CI -> location_model -> get_site_location();
		
		$attributes = " ";
		foreach ( $select_properties as $attribute=>$value)
		{
			$attributes .= 	$attribute . '="' . htmlspecialchars($value) . '"' ;
		}
		
		if(!is_array($selected_id))
			$selected_id = ($selected_id !== NULL) ? array($selected_id) : array() ;
		
		$string = "<select $attributes>";
		
		$selected = (in_array(0,$selected_id)) ? 'selected' : '';
		$string .= '<option value="0" '.$selected.'>-- All --</option>';

		foreach ($options as $opt){
			$selected = (in_array($opt['location_id'],$selected_id)) ? 'selected' : '';
			$string .= "<option value=\"$opt[location_id]\" $selected>" . htmlspecialchars($opt['location_name']) . '</option>';
			
		}

		$string .= "</select>";
		
		echo $string;
		
	} else {
		echo "report_helper : select properties has to be in associative array format";
	}
	
}


function generate_report_buttons($url = '')
{
	echo '<button type="button" class="btn btn-primary btn-flat" onClick="display_report(\''.$url.'\')"><i class="fa fa-file-text"></i> View Report</button>&nbsp;';
	echo '<button type="button" class="btn btn-primary btn-flat" onClick="display_report(\''.$url.'\',\'print\')"><i class="fa fa-print"></i> Print Report</button>&nbsp;';
	echo '<button type="button" class="btn btn-primary btn-flat" onClick="display_report(\''.$url.'\',\'xls\')"><i class="fa fa-file-excel-o"></i> Download XLS</button>&nbsp;';
	echo '<button type="button" class="btn btn-primary btn-flat" onClick="display_report(\''.$url.'\',\'pdf\')"><i class="fa fa-file-pdf-o"></i> Download PDF</button>&nbsp;';
	// echo '<button type="button" class="btn btn-primary btn-flat" onClick="display_report(\''.$url.'\',\'email\')"><i class="fa fa-envelope"></i> Send Email to Me</button>&nbsp;'; // no need for email first
	
}

/**
*
* currency_format
* Custom Skyware to display currency format because PHP doesn't have built in
*
*/
function currency_format($number = 0, $currency = 'IDR', $showsymbol = false)
{
	switch ($currency) {
		case 'IDR':
			$sym = 'Rp ';
			$thousand_spt = ',';
			$decimal_spt = '.';
			$decimal_digit = 0;
			$negative_symbol = '()';	// or '-'
			break;
		case 'USD':
			$sym = 'US$ ';
			$thousand_spt = ',';
			$decimal_spt = '.';
			$decimal_digit = 2;
			$negative_symbol = '()';	// or '-'
			break;
		default:
			$sym = 'Rp ';
			$thousand_spt = ',';
			$decimal_spt = '.';
			$decimal_digit = 0;
			$negative_symbol = '()';	// or '-'
			break;
	}

	$return  = $number < 0 ? ($negative_symbol == '()' ? '(' : '-') : '';
	$return .= $showsymbol == true ? $sym : '';
	$return .= number_format(abs($number),$decimal_digit,$decimal_spt,$thousand_spt);
	$return .= $number < 0 ? ($negative_symbol == '()' ? ')' : '') : '';

	return $return;

}
