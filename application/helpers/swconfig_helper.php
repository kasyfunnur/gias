<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function config_form($submit=NULL){
	if(!$submit)$submit = 'return validateForm();';
	$returndata = array(
	    'class'     => 'form-horizontal',
	    'role'      => 'form',
	    'method'    => 'post', 
	    'name'      => 'frm', 
	    'id'        => 'frm',
	    //'onSubmit'  => 'return validateForm();'
	    'onSubmit'  => $submit
    );
	return $returndata;
}
