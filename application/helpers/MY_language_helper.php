<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// MY_language_helper
// Extends Codeigniter Language Helper

// Override deault lang function
// If language line is not defined, display the key value
// with @ prefix (so users / developers can see incomplete translation)

function lang($line, $id = '')
{
	$CI =& get_instance();
	$line = ($CI->lang->line($line) !== FALSE ? $CI->lang->line($line) : '@' . $line);
	if ($id != '')
	{
		$line = '<label for="'.$id.'">'.$line."</label>";
	}
	return $line;
}