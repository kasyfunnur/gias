<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

function html_select_position($select_properties = array(), $selected_id = NULL)
{
	if( is_array($select_properties) && count($select_properties) > 0){
		$CI = & get_instance();
		$CI -> load -> database();
		$CI -> load -> model('position_model');
		
		$options = $CI -> position_model -> get_structure();
		
		$attributes = " ";
		foreach ( $select_properties as $attribute=>$value)
		{
			$attributes .= 	$attribute . '="' . htmlspecialchars($value) . '"' ;
		}
		
		$string = "<select $attributes>";
		
		$string .= '<option value="0">-- Select Position --</option>';
		$firstcount = true;
		foreach ($options as $opt){
			if ( $opt['position_type'] == 'division' ) {
				if (!$firstcount)
					$string .= "</optgroup>";
				$string .= '<optgroup label="'  . htmlspecialchars($opt['position_code']) . ' - ' . htmlspecialchars($opt['position_name']) . '">';
			} else	{
				$selected_string = ($opt['position_id'] == $selected_id) ? "selected" : "";
				$string .= "<option value=\"$opt[position_id]\" {$selected_string}>&nbsp;&nbsp;" . htmlspecialchars($opt['position_code']) . ' - ' . htmlspecialchars($opt['position_name']) . '</option>';
			}
		}
		$string .= "</optgroup>"; // close the last optgroup
		$string .= "</select>";
		
		echo $string;
		
	} else {
		echo "position_helper : select properties has to be in associative array format";
	}
}


//. create select box filled with division data
function html_select_division($select_properties = array(), $selected_id = NULL)
{
	if( is_array($select_properties) && count($select_properties) > 0){
		$CI = & get_instance();
		$CI -> load -> database();
		$CI -> load -> model('position_model');
		
		$options = $CI -> position_model -> get_division();
		
		$attributes = " ";
		foreach ( $select_properties as $attribute=>$value)
		{
			$attributes .= 	$attribute . '="' . htmlspecialchars($value) . '"' ;
		}
		
		$string = "<select $attributes>";
		
		$string .= '<option value="0">-- Select Position --</option>';
		$firstcount = true;
		foreach ($options as $opt){
			
				$string .= "<option value=\"$opt[position_id]\">&nbsp;&nbsp;" . htmlspecialchars($opt['position_code']) . ' - ' . htmlspecialchars($opt['position_name']) . '</option>';
			
		}
		$string .= "</select>";
		
		echo $string;
		
	} else {
		echo "position_helper : select properties has to be in associative array format";
	}
}
