<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

function request_approval($user_id, $appr_code, $document_number){
	$CI = &get_instance();
	$CI -> load -> database();
	$CI -> load -> model('approval_model');
	return $CI->approval_model->request_approval($user_id, $appr_code, $document_number);
}

function expand_matrix($matrix){
	/**
	 * The matrix contains sets of approvers based on their position id,
	 * example, when matrix is:
	 * 	"8" -> means all of the user in position_id 8 has to be notified of the request, anyone can approve
	 * 	"8+10" -> means anyone in position_id 8 and 10 has to approve the document
	 * 	"8/10+12" -> means either position_id 8 or 10 can approve, + (AND) position_id 10 has to
	 *  approve as well
	 */
	
	$set = array();
	
	$vertical = explode("+",$matrix);
	
	$step = 1;
	foreach ($vertical as $group){
		$horizontal = explode("/",$group);
		foreach ($horizontal as $hgroup){
			$set[] = array('step'=>$step, 'position'=>$hgroup);
		}
		$step++;
	}
	
	/**
	 * set will now be something like this (for matrix 8/10+12)
	 *  set[0]['step'] = 1
	 *  		['position'] = 8
	 *  set[1]['step'] = 1
	 *  		['position'] = 10
	 *  set[2]['step'] = 2
	 *  		['position'] = 12
	 */
	
	return $set;
}
?>