<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*SWLayout:
(c) 2014 - Indo Skyware.*/

function date_single($label=NULL,$object_name=NULL,$data=NULL){
	if(!$label || $label=="" || $label==NULL) $label = 'Date';
	if(!$object_name || $object_name=="" || $object_name==NULL) $object_name = 'date';
	if(!$data || $data=="" || $data==NULL) $data = date('Y-m-d');
	$returndata = '
		<tr class="info">
			<td>'.$label.'</td>
			<td class="value">
				<input type="date" class="form-control" name="'.$object_name.'" id="'.$object_name.'" value="'.$data.'">
			</td>  
		</tr>';//
	return $returndata;
}
function date_multi($label=NULL,$object_name=NULL,$data=NULL){
	if(!$label || $label=="" || $label==NULL){
		$object_label[0] = 'Date From';
		$object_label[1] = 'Date To';
	}else{
		$object_label = explode(',',$label); 
	}
	if(!$object_name || $object_name=="" || $object_name==NULL) $object_name = 'date';
	if(!$data || $data=="" || $data==NULL) $data = date('Y-m-d');
	
	$returndata = '
		<tr class="info">
			<td>'.$object_label[0].'</td>
			<td class="value">
				<input type="date" class="form-control" name="'.$object_name.'[]" id="'.$object_name.'" value="'.date('Y-m-01').'">							
			</td>
			<td>'.$object_label[1].'</td>
			<td class="value">
				<input type="date" class="form-control" name="'.$object_name.'[]" id="'.$object_name.'" value="'.date('Y-m-d').'">
			</td>                    
		</tr>';
	return $returndata;
}
function warehouse_multi($label=NULL,$object_name=NULL,$data=NULL){
	if(!$label || $label=="" || $label==NULL) $label = 'Warehouse';
	if(!$object_name || $object_name=="" || $object_name==NULL) $object_name = 'sel_wh';
	if(!$data || $data=="" || $data==NULL) $data = '';
	
	$wh_option = '';

	$CI = &get_instance();
	$CI -> load -> database();	
	$CI -> load -> model('inventory_model');
	$whses = $CI->inventory_model->get_warehouse_all(explode(',',$data));
	
	foreach($whses as $whse){
		$wh_option .= '<option value="'.$whse['location_id'].'">'.$whse['location_code'].' - '.$whse['location_name'];
	}
	$returndata = '
		<tr class="info">
			<td>'.$label.'</td>
			<td class="value">
				<input type="checkbox" id="'.$object_name.'_all" value="ALL"/>
				<label for="'.$object_name.'_all">Select All Warehouse</label>
				<select class="form-control" size="3" style="height:150px;"
					name="'.$object_name.'[]" id="'.$object_name.'" multiple="multiple">						
						'.$wh_option.'
				</select>
			</td>
		</tr>
		<script>
		$(document).ready(function(){
			$("#'.$object_name.'_all").on("click",function(){
				$("#'.$object_name.' option").prop("selected", $(this).prop("checked"));
			});
		});
		</script>
		';

	return $returndata;
}
function item_multi($label=NULL,$object_name=NULL,$data=NULL){
	if(!$label || $label=="" || $label==NULL) $label = 'Item';
	if(!$object_name || $object_name=="" || $object_name==NULL) $object_name = 'sel_item';
	if(!$data || $data=="" || $data==NULL) $data = '';
	
	$item_option = '';
	$CI = &get_instance();
	$CI -> load -> database();
	$CI -> load -> model('inventory_model');
	$items = $CI->inventory_model->get_stock_all(explode(',',$data));
	
	foreach($items as $item){
		$item_option .= '<option value="'.$item['item_id'].'">'.$item['item_code'].' - '.$item['item_name'];
	}
	$returndata = '
		<tr class="info">
			<td>'.$label.'</td>
			<td class="value">
				<input type="checkbox" id="'.$object_name.'_all" value="ALL"/>
				<label for="'.$object_name.'_all">Select All Item</label>
				<select class="form-control" size="3" style="height:150px;"
					name="'.$object_name.'[]" id="'.$object_name.'" multiple="multiple">						
						'.$item_option.'
				</select>
			</td>
		</tr>
		<script>
		$(document).ready(function(){
			$("#'.$object_name.'_all").on("click",function(){
				$("#'.$object_name.' option").prop("selected", $(this).prop("checked"));
			});
		});
		</script>
		';
	return $returndata;
}

function cust_single($label=NULL,$object_name=NULL,$data=NULL){
	if(!$label || $label=="" || $label==NULL) $label = 'Customer';
	if(!$object_name || $object_name=="" || $object_name==NULL) $object_name = 'sel_cust';
	if(!$data || $data=="" || $data==NULL) $data = '';
	
	$buss_option = '<option value="all"> - ALL - ';
	$CI = &get_instance();
	$CI -> load -> database();
	$CI -> load -> model('business_model');	
	$busses = $CI->business_model->getCustomer();

	foreach($busses as $buss){
		$buss_option .= '<option value="'.$buss['buss_id'].'">'.$buss['buss_code'].' - '.$buss['buss_name'];
	}
	$returndata = '
		<tr class="info">
			<td>'.$label.'</td>
			<td class="value">
				<!---<input type="checkbox" id="'.$object_name.'_all" value="ALL"/>
				<label for="'.$object_name.'_all">Select All Item</label>--->
				<select class="form-control" 
					name="'.$object_name.'[]" id="'.$object_name.'" >						
						'.$buss_option.'
				</select>
			</td>
		</tr>
		<script>
		$(document).ready(function(){
			$("#'.$object_name.'_all").on("click",function(){
				$("#'.$object_name.' option").prop("selected", $(this).prop("checked"));
			});
		});
		</script>
		';
	return $returndata;
}
function cons_single($label=NULL,$object_name=NULL,$data=NULL){
	if(!$label || $label=="" || $label==NULL) $label = 'Customer';
	if(!$object_name || $object_name=="" || $object_name==NULL) $object_name = 'sel_cust';
	if(!$data || $data=="" || $data==NULL) $data = '';
	
	$buss_option = '<option value="all"> - ALL - ';
	$CI = &get_instance();
	$CI -> load -> database();
	$CI -> load -> model('business_model');	
	$busses = $CI->business_model->getConsignment();

	foreach($busses as $buss){
		$buss_option .= '<option value="'.$buss['buss_id'].'">'.$buss['buss_code'].' - '.$buss['buss_name'];
	}
	$returndata = '
		<tr class="info">
			<td>'.$label.'</td>
			<td class="value">
				<!---<input type="checkbox" id="'.$object_name.'_all" value="ALL"/>
				<label for="'.$object_name.'_all">Select All Item</label>--->
				<select class="form-control" 
					name="'.$object_name.'[]" id="'.$object_name.'" >						
						'.$buss_option.'
				</select>
			</td>
		</tr>
		<script>
		$(document).ready(function(){
			$("#'.$object_name.'_all").on("click",function(){
				$("#'.$object_name.' option").prop("selected", $(this).prop("checked"));
			});
		});
		</script>
		';
	return $returndata;
}