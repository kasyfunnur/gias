<?php 
class MY_Model extends CI_Model { 
  	public $table;   
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function querying($query){
		$query = $this->db->query($query);		
		return $query->result_array();	
	}
	
	public function querying_simple($query_string){
		$query = $this->db->query($query_string);			
		$field = $query->list_fields();
		//var_dump($field[0]);
		$rows = $query->result_array();
		if ($query->num_fields() == 1){
			//var_dump($this->db->list_fields());
			//$field = $query->list_fields();
			//var_dump($field);
			$cols = array();
			$x = 0;
			foreach($rows as $row){
				//var_dump($row);
				$cols[$x] = $row[$field[0]];
				$x = $x + 1;
			}
			return $cols;
		}else{
			return $rows;
		}	
	}
	
	public function create($data){				
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}
	
	
	public function readAll($field="", $value=""){
		if (($field != "") || ($value != "")){
			return $this->db->where($field, $value)->get($this->table)->result_array();
		} else {
			return $this->db->get($this->table)->result_array();
		}
	}
		
	public function read($field, $value){
		return $this->db->where($field, $value)->limit(1)->get($this->table)->row_array();
	}
	
	public function update($field, $value, $data){
		if(is_array($field)){
			$x=0;
			foreach($field as $single_field){
				$this->db->where($single_field, $value[$x]);
				$x++;
			}
		}else{
			$this->db->where($field, $value);	
		}		
		$this->db->set($data);
		$this->db->update($this->table);
		return $this->db->affected_rows();
	}
	
	public function delete($field, $value){
		$this->db->where($field, $value);
		$this->db->delete($this->table);
		return $this->db->affected_rows();
	}
}