<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$config['pagination']['per_page']    		= 10;
$config['pagination']['num_tag_open'] 		= '<li>';
$config['pagination']['num_tag_close'] 		= '</li>';
$config['pagination']['cur_tag_open'] 		= '<li class="active"><a>';
$config['pagination']['cur_tag_close'] 		= '</a></li>';
$config['pagination']['prev_tag_open'] 		= '<li>';
$config['pagination']['prev_tag_close'] 	= '</li>';
$config['pagination']['next_tag_open'] 		= '<li>';
$config['pagination']['next_tag_close'] 	= '</li>';
$config['pagination']['last_tag_open'] 		= '<li>';
$config['pagination']['last_tag_close'] 	= '</li>';
$config['pagination']['first_tag_open']	 	= '<li>';
$config['pagination']['first_tag_close'] 	= '</li>';     


/* End of file pagination.php */
/* Location: ./application/config/pagination.php */
