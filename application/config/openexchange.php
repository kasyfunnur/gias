<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
*
* OpenExchange - Skyware ERP Integration
* http://openexchangerates.org
*
* Usage : base_url + ...
* latest.json
*  - get the most recent exchange rates
* 
* historical/YYYY-MM-DD.json
*  - get rates for any given day, where available
* 
* currencies.json
*  - get list of currency codes and names
**/



$config['app_id'] = '7a2babe1fbb74902ac6505e7ab795bc1';
$config['oe_base_url'] = 'https://openexchangerates.org/api/';


/* End of file openexchange.php */
/* Location: ./application/config/openexchange.php */