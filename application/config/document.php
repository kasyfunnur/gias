<?php 

$config['document'] =  array (
		'asset_request'		=> array ('prefix' =>'AR' ,'table'=>'t_asset_request_header'    , 'ref_field' => 'document_ref' , 'digit' => 4),
		'purchase_request'	=> array ('prefix' =>'PR' ,'table'=>'t_purchase_request' , 'ref_field' => 'document_ref' , 'digit' => 4),
		'purchase_order'	=> array ('prefix' =>'PO' ,'table'=>'t_purchase_order_header'   , 'ref_field' => 'doc_num' , 'digit' => 4),
		'purchase_receipt'	=> array ('prefix' =>'RCV','table'=>'t_purchase_receipt_header'   , 'ref_field' => 'doc_num' , 'digit' => 4),
		'quotation_request'	=> array ('prefix' =>'QR' ,'table'=>'t_quotation_request', 'ref_field' => 'document_ref' , 'digit' => 4),
		'move_request'		=> array ('prefix' =>'MR' ,'table'=>'t_asset_trans', 'ref_field' => 'document_ref' , 'digit' => 4),
		'disposal_request'	=> array ('prefix' =>'DR' ,'table'=>'t_asset_trans', 'ref_field' => 'document_ref' , 'digit' => 4),
		'depreciation'		=> array ('prefix' =>'AD' ,'table'=>'t_asset_trans', 'ref_field' => 'document_ref' , 'digit' => 4)
);

?>