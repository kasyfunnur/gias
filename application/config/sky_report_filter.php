<?php 
/*
	date_filter:
		1 = single date / as of.
		2 = multi date / range / from - to.
*/
$CI = &get_instance();

$config['report_filter'] =  array (
	'stock_balance'		=> 	array (
		'date'				=>	1,
		'item_group'		=>	1,
		'item'				=>  array(
			'type'				=> 	'multiple',
			'data'				=>	$CI->inventory_model->get_stock_all()
		),
		'whse'				=>  array(
			'type'				=> 	'multiple',
			'data'				=>	$CI->location_model->getAll('is_warehouse = 1 and is_active = 1')
		)
	),
	'stock_transaction'		=> 	array (
		'date'				=>	2,
		'item_group'		=>	1,
		'item'				=>  array(
			'type'				=> 	'multiple',
			'data'				=>	$CI->inventory_model->get_stock_all()
		),
		'whse'				=>  array(
			'type'				=> 	'multiple',
			'data'				=>	$CI->location_model->getAll('is_warehouse = 1 and is_active = 1')
		)
	),
	'sales_report'			=> array(
		'date'				=> 2,
		'item_group'		=> 1
	),
	'account_balance'	=>	array (
		'date'				=> 	1,
		'account'			=>	2
	)
);

?>