<?php
class Login extends CI_Controller {
	
public function __construct(){
	parent::__construct();
	if ($this -> session ->logged_in) header("location:".base_url('home'));
	$this -> load -> model  ('login_lib','',TRUE); 
	$this -> load -> library('email');
	$this -> load -> library('msg');
	$this -> load -> library('form_validation');
	}
	
public function index(){
	$user_email		= $this	-> input -> post("login_username");
	$user_password	= $this -> input -> post("login_password");
	
	$this -> form_validation -> set_rules('login_username', 'Username', "trim|required|callback__login_check[$user_password]");
	
	$this -> form_validation -> set_rules('login_password', 'Password', 'trim|required');
	
	if (!$this->form_validation->run()){
		$this-> sky -> load_page('index',NULL,NULL,1);
	} else {
		$reset = $this->login_lib->check_next_reset($user_email);
		if($reset['reset_next_login'] == 1){			
			header("location:".base_url('home/reset_password'));
		}else{
			// header("location:".base_url('home'));
			redirect('home');
		}
	}
}
	
public function create($username,$password){
	if($this -> login_lib -> create ($username,$password))echo "user $username created";
}	

public function forgetPass(){
	$email	= $this->input->post("forgetpass_email");
	
	$this -> form_validation -> set_rules('forgetpass_email','Email','trim|required|valid_email|max_length[50]|callback__email_registered');
	
	if (!$this -> form_validation -> run())$this->load_page();
	else {
		$this->login_lib->update_verification_code($email);
		$data = $this->login_lib->check_email($email,1,1); //forgetpass needs email to be verified (,1) and returns the data (,1)
		
		if(!empty($data)){
			$user_id			= $data->user_id;
			$verification_code 	= $data->verification_code;
			$url				= base_url("login/getPass/$user_id/$verification_code");	
			
			$this -> email -> to 	  ($email);
			$this -> email -> subject ("Lupa Password Anda?");
			$this -> email -> message ("Hai,<br><br>
			Jika anda mengingkinkan password baru untuk akun anda,<br>
			silahkan klik di {unwrap}<a href='$url'>SINI</a>{/unwrap},<br>
			apabila anda tidak meminta password baru, silahkan abaikan email ini.<br><br>
			Terima Kasih<br>)");
			$this->load_page($this -> email -> send() ? 'forgetPassSent' : 'forgetPassFailed');
		} else {
			$this -> sky -> load_page('forgetPassFailed');
		}
	}
}

public function logout(){
	$this -> login_lib -> logout();
	header("location:".base_url());
	}

//functions , callbacks area

public function _login_check($username,$password){
	if (!$this->login_lib->login($username, $password)){
		$this->form_validation->set_message('_login_check', 'Wrong username or password, please try again');
		return FALSE;
		}
		else return TRUE;	
	}
	
public function _password_valid($str){
    if (!(preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str))) {
	    $this->form_validation->set_message('_password_valid', '%s needs at least one alphabet and one number');
     	return FALSE;
    	}
    else return TRUE;
	}
}

// not required

	/*
public function resendVerification(){
	$email=$this->input->post("verify_email");
	$this->form_validation->set_rules('verify_email', 'Email', 'trim|required|valid_email|max_length[50]|xss_clean');
	$this->form_validation->set_rules('captcha', 'Captcha','trim|required|callback__captcha_check' );
	
	if (!$this->form_validation->run())$this->load_page();
	else {
			$this->login_lib->update_verification_code($email);
			$data = $this->login_lib->check_email($email,0,1); //resend verify requires email that has 0 in verified field (,0)
			if(empty($data))$this->load_page('emailUnregistered');
			
			else {
			$user_id 			= $data->user_id;
			$verification_code 	= $data->verification_code;
			$url 				= base_url("login/verify/$user_id/$verification_code");

			$this -> email -> to ($email);
			$this -> email -> subject  ("Verifikasi Ulang");
			$this -> email -> message  ("Hai, <br><br>Anda baru saja meminta verifikasi ulang akun anda di ChickenPos,<br>
			silahkan klik di {unwrap}<a href='$url'>SINI</a>{/unwrap} untuk verifikasi email anda.<br><br>
			Terima Kasih<br>
			Tim Chickenpos ;)");
			$this->load_page($this -> email -> send() ? 'verifyMailSent' : 'verifyMailFailed');
			}
		}
	}
	
public function getPass($user_id,$verification_code){
	$convert = $this->login_lib->get_data($user_id);
	$user_email = $convert ->user_email;
	if($data=$this->login_lib->check_verification_code($user_email,$verification_code,TRUE)){
		$newPass = $this->login_lib->generate_random_chars(8);
		
		if($this->login_lib->edit_password($user_email,$newPass)){
			$this->login_lib->update_verification_code($user_email);
			
			$this -> email -> to ($data->user_email);
			$this -> email -> subject ("Password Baru untuk ChickenPOS");
			$this -> email -> message ( "Hai,<br>
			Password baru anda adalah : '$newPass' ,<br>
			jangan lupa untuk mengganti password anda dari menu akun anda.<br><br>
			Terima Kasih<br>
			Tim Chickenpos ;)");
			$this->load_page($this -> email -> send() ? 'newPassSent' : 'newPassFailed');
			}
		else $this->load_page('newPassFailed');
	}
	else $this->load_page('newPassFailed');
	}		
	/*
public function verify($user_id,$verification_code){
	$convert = $this->login_lib->get_data($user_id);
	if (!empty($convert)){ 
	$user_email = $convert ->user_email;
	
		if($this->login_lib->check_verification_code($user_email,$verification_code)){
			$this->login_lib->set_verified($user_email);
			$this->load_page('emailVerified');
			}
		else $this->load_page('verificationFailed');
		}
	else $this->load_page('verificationFailed');
	}

public function register(){
	
	$email=$this->input->post("email");
	$password=$this->input->post("password");
	
	$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.user_email]|max_length[200]|xss_clean');
	$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[32]|callback__password_valid');
	$this->form_validation->set_rules('password_conf', 'Password konfirmasi', 'trim|required|matches[password]');
	
	if (!$this->form_validation->run())$this->load_page();

	else {
		$verification_code = $this->login_lib->generate_random_chars();
		if($this->login_lib->create($email,$password,$verification_code)){
			$user_id	= $this->db->insert_id();
			$url 		= base_url("login/verify/$user_id/$verification_code");
			$this -> email -> to		($email);
			$this -> email -> subject ("Verifikasi Email");
			$this -> email -> message ("Hai,<br><br>
			Anda baru saja melakukan registrasi di ChickenPos,<br>
			silahkan klik di {unwrap}<a href='$url'>SINI</a>{/unwrap} untuk verifikasi email anda.<br><br>
			Terima Kasih<br>
			Tim Chickenpos ;)");
			$this->load_page($this -> email -> send() ? 'userRegistered' : 'registerFailed');
			}
		}
	}
*/