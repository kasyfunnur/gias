<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		if (!$this->ion_auth->logged_in()) redirect('auth/login');
	}

	/**
	*
	* User Management Section
	* Author: Indra Prastha
	* 
	**/
	
	public function users($action = 'view'){
		$this -> sky -> load_page ('user');
	}
		
	public function add_user(){
		$this -> load -> model ('user_model');
		$this -> load -> model ('position_model');
		$this -> load -> model ('asset_model');
		$this -> load -> helper ('position');
		
		// ion auth table configurations
		$tables = $this->config->item('tables','ion_auth');

		// validate input
		$this->form_validation->set_rules('first_name', 'first_name', 'required');
		$this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('user_name', 'Username', 'required|is_unique['.$tables['users'].'.username]');
		$this->form_validation->set_rules('user_pass', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[user_pass_confirm]');
		$this->form_validation->set_rules('user_pass_confirm', 'Confirm Password', 'required');
		$this->form_validation->set_rules('group_id[]', 'Groups', 'required');

		if ($this->form_validation->run() == true)
		{
			$username = strtolower($this->input->post('user_name'));
			$email    = strtolower($this->input->post('user_email'));
			$password = $this->input->post('user_pass');
			$group 	  = $this->input->post('group_id');

			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name'  => $this->input->post('last_name')
			);

			$uid = $this->ion_auth->register($username, $password, $email, $additional_data, $group);

			// insert employee data if check box "set as employee" is checked
			if($this->input->post('chk_employee'))
			{
				//id, emp_no, first_name, last_name, birthdate, position_id, user_id, location_id, gender
				$emp_data = array(
					'emp_no' => $this->input->post('empno'),
					'first_name' 	=> $this->input->post('first_name'),
					'last_name'  	=> $this->input->post('last_name'),
					'birthdate'		=> date('Y-m-d',strtotime($this->input->post('birthdate'))),
					'position_id'	=> $this->input->post('position'),
					'user_id'		=> $uid,
					'location_id'	=> $this->input->post('location_id'),
					'gender'		=> $this->input->post('gender')
					);
				$add_employee_result = $this->user_model->add_employee($emp_data);
			}

			if($uid && $add_employee_result){
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("setting/users", 'refresh');
			}
		}
		/*if ($this->form_validation->run() == true && $uid)
		{
			//check to see if we are creating the user
			//redirect them back to the users page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("settings/users", 'refresh');
		}*/
		else
		{
			//display the create user form
			//set the flash data error message if there is one
			$this->data['msg'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$data['group_array'] 	= $this -> ion_auth -> groups() -> result_array();
			$data['site_location'] 	= $this -> asset_model -> get_site_location();
			$this -> sky -> load_page ('user_add',$data, (isset($this->data['msg']) ? $this->data['msg'] : ""));
		}
	}
		
	public function datatable_settings_users(){
		$this->load->model('user_model');
		$this->output->set_content_type('application/json');
		echo json_encode($this -> user_model -> datatable_settings_users());
	}


	/**
	*
	* User Group Management
	* Integrate with ion_auth User Groups
	*
	**/

	public function user_groups()
	{
		$this -> sky -> load_page ('user_group');
	}

	public function add_user_group()
	{
		$this -> sky -> load_page ('user_group_add');
	}
	

}

/* End of file Settings.php */
/* Location: ./application/controllers/Settings.php */