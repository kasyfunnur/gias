<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this -> load -> helper ('report');

		/* dari report yg lama */
		$this->load->helper('url');
		$this->load->helper('swlayout');
		$this->load->helper('approval');
		$this->load->helper('swreport');
		$this->load->model('location_model');		
		$this->load->model('sales_order_model');
		$this->load->model('sales_delivery_model');
		$this->load->model('sales_invoice_model');
		$this->load->model('sales_return_model');
		$this->load->model('document_model');
		$this->load->model('inventory_model');
		$this->load->model('asset_model');
		$this->load->model('business_model');
		$this->load->model('status_model');
		$this->load->model('journal_model');
		$this->load->model('approval_model');
		$this->load->model('account_model');
		$this->load->model('location_model');
		$this->load->model('setting_model');
		$this->load->model('report_model');
		/* dari report yang lama */
	}
	
	/**
	* Function Prepare
	* load report filter page based on $report_name
	* if you 'prepare' 'asset_history_report', you should have the following file:
	*	1. application/views/report/asset_history_report_filter.php
	*/
	public function prepare( $report_name ){
		$filter_view  = $report_name . '_filter';
		$display_view = $report_name . '_display';
		
		$data['report_url'] = site_url("report/display/$report_name");
		$data['report_title'] = ucwords(str_ireplace("_"," ",$report_name));
		
		$this -> sky -> load_page ($filter_view,$data);
	}
	
	
	/**
	* Function Display
	* load report display page based on $report_name
	* if you 'display' 'asset_history_report', you should have the following file:
	*	1. application/views/report/asset_history_report_display.php
	*/
	public function display( $report_name , $method = 'default' ){
		$filter_view  = $report_name . '_filter';
		$display_view = $report_name . '_display';
		
		$data['filters'] = $this -> input -> post (NULL, TRUE);
		$data['report_title'] = ucwords(str_ireplace("_"," ",$report_name));
		$data['filter_url'] = site_url("report/prepare/$report_name");
		
		switch ( $report_name ) {
			
			case 'asset_move' :
				$this -> load -> model ('asset_model','mdl');
				$data['data'] = $this -> mdl -> {"$report_name" . "_report"} ($data['filters']);
				break;
				
			case 'asset_list' :
				$this -> load -> model ('asset_model','mdl');
				$data['data'] = $this -> mdl -> {"$report_name" . "_report"} ($data['filters']);
				break;
				
			case 'asset_history' :
				$this -> load -> model ('asset_model','mdl');
				$data['data'] = $this -> mdl -> {"$report_name" . "_report"} ($data['filters']);
				break;
				
			case 'purchase_request' :
				$this -> load -> model ('purchase_request_model','mdl');
				$data['data'] = $this -> mdl -> {"$report_name" . "_report"} ($data['filters']);
				break;
			
			case 'balance_sheet' :
				$this->load->model('account_model', 'mdl');
				$data['data'] = $this -> mdl -> {"$report_name" . "_report"} ($data['filters']);
				break;

			case 'profit_loss' :
				$this->load->model('account_model', 'mdl');
				$data['data'] = $this -> mdl -> {"$report_name" . "_report"} ($data['filters']);
				break;

			case 'acc_ledger' :
				$this->load->model('account_model', 'mdl');
				$data['data'] = $this -> mdl -> {"$report_name" . "_report"} ($data['filters']);
				break;

			case 'trial_balance' :
				$this->load->model('account_model', 'mdl');
				$data['data'] = $this -> mdl -> {"$report_name" . "_report"} ($data['filters']);
				break;			

			case 'purchase_order'	:
				$this->load->model('purchase_order_model','mdl');
				$data['data'] = $this->mdl->{"$report_name"."_report"} ($data['filters']);
				break;

			case 'purchase_order_detail' :
				$this->load->model('purchase_order_model','mdl');
				$data['data'] = $this->mdl->{"$report_name"."_report"} ($data['filters']);
				break;

			case 'sales_order'	:
				$this->load->model('sales_order_model','mdl');
				$data['data'] = $this->mdl->{"$report_name"."_report"} ($data['filters']);
				break;

			case 'sales_order_detail' :
				$this->load->model('sales_order_model','mdl');
				$data['data'] = $this->mdl->{"$report_name"."_report"} ($data['filters']);
				break;

			case 'inventory_balance' :
				$this->load->model('inventory_model','mdl');
				$data['data'] = $this->mdl->{"$report_name"."_report"} ($data['filters']);
				break;

			case 'inventory_transaction' :
				$this->load->model('inventory_model','mdl');
				$data['data'] = $this->mdl->{"$report_name"."_report"} ($data['filters']);
				break;

			default:
				$data['data'] = '';
				break;
				
		}
		
		$available_methods = array('default','print','pdf','email','xls');
		if (!in_array($method,$available_methods))
			$method = 'default';
		
		switch ($method) {
			case 'default' :
				$this -> sky -> load_page ($display_view, $data, NULL, FALSE, TRUE);
				break;
			
			case 'print' :
				$this -> load -> view ('report/print_header',$data);
				$this -> load -> view ('report/' . $display_view, $data);
				$this -> load -> view ('report/print_footer',$data);
				break;

			case 'xls' :
				header('Content-Type: application/vnd.ms-excel');
				header("Content-Disposition: attachment; filename={$report_name}.xls");
				$this -> load -> view ('report/print_header',$data);
				$this -> load -> view ('report/' . $display_view, $data);
				$this -> load -> view ('report/print_footer',$data);
				//$this -> sky -> load_page ($display_view, $data, NULL, FALSE, TRUE);
				break;
				
			case 'pdf' :
				$this -> load -> helper (array('dompdf','file'));
				$html  = $this -> load -> view ('report/print_header',$data, true);
				$html .= $this -> load -> view ('report/' . $display_view, $data, true);
				$html .= $this -> load -> view ('report/print_footer',$data, true);
				$timestamp = date('Ymd',time());
				pdf_create($html, "{$report_name}_{$timestamp}");
				break;
				
			case 'email' :
				$this -> sky -> load_page ($display_view, $data, NULL, TRUE);
				break;
		}
	}

	/* dari report yg lama */
	public function filtering($code){
		$data['header'] = $this->report_model->get_report_header($code);
		$data['config'] = $this->report_model->get_report_config($code);		
		$this->sky->load_page("filter",$data);			
	}
	public function filter($type){
		$CI = &get_instance();
		$CI->config->load('sky_report_filter');
		$temp_config = $CI->config->item('report_filter');
		switch($type){
			case 'stock_balance':
				$data['type'] = 'stock_balance';
				$data['name'] = 'Stock Balance';
				$data['config']	= $temp_config['stock_balance'];				
				break;
			case 'stock_transaction':
				$data['type'] = 'stock_transaction';
				$data['name'] = 'Stock Card';
				$data['config']	= $temp_config['stock_transaction'];				
				break;
			case 'account_balance':
				$data['type'] = 'balance';
				$data['name'] = 'Balance Sheet';
				$data['config']	= $temp_config['account_balance'];
				break;
			case 'income':				
				$data['type'] = 'income';
				$data['name'] = 'Income Statement';
				$data['config']	= $temp_config['income'];
				break;
			case 'equity':
				$data['type'] = 'equity';
				$data['name'] = "Statement of Owner's Equity";
				$data['config']	= $temp_config['equity'];
				break;
			case 'cashflow':
				$data['type'] = 'cashflow';
				$data['name'] = "Statement of Cashflow";
				$data['config']	= $temp_config['equity'];
				break;
			case 'vend_aging':
				$data['type'] = 'vend_aging';
				$data['name'] = "Vendor Aging";
				$data['filter_date_range'] = false;
				$data['filter_date_asof'] = true;
				$data['filter_account'] = true;
				$data['filter_business'] = true;
				$data['ddl_business'] = $this->business_model->getVendor();
				break;
			case 'cust_aging':
				$data['type'] = 'cust_aging';
				$data['name'] = "Customer Aging";
				$data['filter_date_range'] = false;
				$data['filter_date_asof'] = true;
				$data['filter_account'] = true;
				$data['filter_business'] = true;
				$data['ddl_business'] = $this->business_model->getCustomer();
				break;
			default:
				break;			
		}		
		$this->sky->load_page("filter",$data);		
	}

	public function query($code){
		$data['setting'] = $this->setting_model->get_company_info();
		$data['header'] = $this->report_model->get_report_header($code);
		$data['config'] = $this->report_model->get_report_config($code);
		$data['results'] = $this->report_model->$code($_GET);
		$data['param'] = $_GET;

		$this->sky->load_page($data['header']['report_view'],$data);
	}
}