<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account extends CI_Controller {	
	public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this->load->library('pagination');
        $this->load->library('sky');
        $this->load->helper('url');
		$this->load->helper('swlayout');
		$this->load->helper('approval');
		$this->load->helper('swconfig');
		$this->load->helper('swstatus');
		$this->load->model('location_model');
		$this->load->model('purchase_request_model');		
		$this->load->model('purchase_order_model');
		$this->load->model('purchase_receipt_model');
		$this->load->model('document_model');
		$this->load->model('inventory_model');
		$this->load->model('asset_model');
		$this->load->model('business_model');
		$this->load->model('status_model');
		$this->load->model('finance_model');
		$this->load->model('account_model');
		$this->load->model('journal_model');
		$this->load->model('journal_template_model');
		$this->load->model('approval_model');
		$this->load->model('costcenter_model');

		$this->config->load('pagination',TRUE);
	}
	
	//COA
	public function coa(){
		$data['datatable'] = $this->account_model->getAll();
		$this->sky->load_page("chart_account",$data);
	}
	public function chart_account_add($code=0){
		$query_all = array();		
		if($code != 0){
			$this->db->select("*")->from('t_account')->where('account_number',$code);
			$query = $this->db->get();	
			array_push($query_all,$query->row_array());
		}
		$child_array = $this->account_model->getChild($code);
		foreach ($child_array as $array){
			array_push($query_all,$array);
		}		
		$data['datatable'] = $query_all;
		$data['gl_parent'] = $this->account_model->getAll(array(array('is_postable',0)));
		$this->sky->load_page("chart_account_add",$data);
	}
	public function chart_account_edit($code=NULL){
		if($code==NULL){
			echo "Please choose an account"; return false;
		}
		$data['gl_detail'] = $this->account_model->view(array('account_number'=>$code));
		$data['gl_parent'] = $this->account_model->view(array('is_postable'=>0),NULL,array('account_number'=>'asc'));
		$this->sky->load_page("chart_account_edit",$data);
	}
	public function coa_add_query(){
		//DATA
		$this->db->select('account_level');
		$this->db->from('t_account');
		$this->db->where('account_number',$this->input->post('gl_parent'));
		$query = $this->db->get();
		$tmp_level = $query->row_array();
		$data = array(
			'account_number'		=> $this->input->post('gl_code'),
			'account_name'			=> $this->input->post('gl_name'),
			'account_currency'		=> $this->input->post('gl_curr'),
			'account_parent_number'	=> $this->input->post('gl_parent'),
			'account_level'			=> ($tmp_level['account_level']+1),
			'account_usage'			=> $this->input->post('gl_use'),
			'is_postable'			=> $this->input->post('gl_post'),
			'is_cash'				=> ($this->input->post('gl_cash')) ? $this->input->post('gl_cash') : 0,
			'is_bank'				=> ($this->input->post('gl_bank')) ? $this->input->post('gl_bank') : 0
		);
		//TRANS
		$this->db->trans_begin();
		$this->account_model->add($data);
		$this->sky->trans_end();
	}
	public function coa_edit_query(){
		//DATA
		$this->db->select('account_level');
		$this->db->from('t_account');
		$this->db->where('account_number',$this->input->post('gl_parent'));
		$query = $this->db->get();
		$tmp_level = $query->row_array();
		$data = array(
			'account_name'			=> $this->input->post('gl_name'),
			'account_currency'		=> $this->input->post('gl_curr'),
			'account_parent_number'	=> $this->input->post('gl_parent'),
			'account_level'			=> ($tmp_level['account_level']+1),
			'account_usage'			=> $this->input->post('gl_use'),
			'is_postable'			=> $this->input->post('gl_post'),
			'is_cash'				=> ($this->input->post('gl_cash')) ? $this->input->post('gl_cash') : 0,
			'is_bank'				=> ($this->input->post('gl_bank')) ? $this->input->post('gl_bank') : 0
		);
		//TRANS
		$this -> db -> trans_begin();
		$this->account_model->edit($data,array('account_number'=>$this->input->post('gl_code')));		
		$this->sky->trans_end();
	}

	//JOURNAL ENTRY
	public function journal($page=0){
		$data['rows'] = $this->journal_model->count_doc();
		//$data['datatable'] = $this->journal_model->view();
		$data['datatable'] = $this->journal_model->getAll(NULL,$page);
		$this->sky->load_page("journal",$data);
	}
	public function journal_add(){
		$data['ddl_cc']				= $this->costcenter_model->get();
		$data['doc_num'] 			= $this->document_model->getNextSeries('JRNL');
		$this->account_model->table = 't_account_trans';
		$data['ddl_trans'] 			= $this->account_model->getTransCode();
		$this->account_model->table = 't_account';
		$data['dialog_account'] 	= $this->account_model->view(array('is_postable'=>1));
		$this->sky->load_page("journal_add",$data);
	}
	public function journal_edit($code){
		$data['ddl_cc']				= $this->costcenter_model->get();
		$this->account_model->table = 't_account_trans';
		$data['ddl_trans'] = $this->account_model->getTransCode();
		$this->account_model->table = 't_account';
		$data['dialog_account'] = $this->account_model->view(array('is_postable'=>1));
		$data['form_header'] = $this->journal_model->getHeader($code);
		$data['form_detail'] = $this->journal_model->getDetail($code);
		$this->sky->load_page("journal_edit",$data);
	}


	public function journal_add_query(){
		//DATA
		$document_number = $this->document_model->getNextSeries('JRNL');
		$data_header = array(
			'journal_code'			=> $document_number,
			'posting_date'			=> $this->input->post('post_date'),
			'document_date'			=> $this->input->post('doc_date'),
			'journal_memo'			=> $this->input->post('doc_remark'),
			'journal_type'			=> $this->input->post('trans_code'),
			'journal_ref1'			=> $this->input->post('ref1'),
			'journal_ref2'			=> $this->input->post('ref2'),
			'journal_ref3'			=> $this->input->post('ref3'),
			"create_by"				=> $this->session->userdata("username")		
		);			
		$data_detail= array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){
				array_push($data_detail, array(					
					'journal_line'	=> $x+1,
					'account_id'	=> $this->input->post('account_id')[$x],
					'journal_dr'	=> $this->input->post('dr')[$x],
					'journal_cr'	=> $this->input->post('cr')[$x],
					'journal_cc'	=> $this->input->post('journal_cc')[$x],
					'journal_curr'	=> $this->input->post('curr')[$x],
				));
			}
		}

		//TRANS
		$this -> db -> trans_begin();	
		$data['t_account_journal_header'] = $data_header;
		$data['t_account_journal_detail'] = $data_detail;
		$this->journal_model->add($data);

		$this->sky->trans_end();
	}
	public function journal_edit_query(){
		//DATA
		$data_header = array(			
			'document_date'			=> $this->input->post('doc_date'),
			'journal_memo'			=> $this->input->post('doc_remark'),
			'journal_type'			=> $this->input->post('trans_code'),
			'journal_ref1'			=> $this->input->post('ref1'),
			'journal_ref2'			=> $this->input->post('ref2'),
			'journal_ref3'			=> $this->input->post('ref3')
		);
		
		$data_detail= array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){				
				/*$data_detail = array(
					//'journal_id'	=> $temp_id,
					'journal_line'	=> $x+1,
					'account_id'	=> $this->input->post('account_id')[$x],
					'journal_dr'	=> $this->input->post('dr')[$x],
					'journal_cr'	=> $this->input->post('cr')[$x],
					'journal_cc'	=> '',
					'journal_curr'	=> $this->input->post('curr')[$x],
				);	*/			
				//$this->journal_model->add($data_detail);
				array_push($data_detail, array(					
					'journal_line'	=> $x+1,
					'account_id'	=> $this->input->post('account_id')[$x],
					'journal_dr'	=> $this->input->post('dr')[$x],
					'journal_cr'	=> $this->input->post('cr')[$x],
					'journal_cc'	=> '',
					'journal_curr'	=> $this->input->post('curr')[$x],
				));
			}
		}
		//TRANS
		$this -> db -> trans_begin();
		$data['t_account_journal_header'] = $data_header;
		$data['t_account_journal_detail'] = $data_detail;
		$this->journal_model->edit($data,array('journal_id'=>$this->input->post('journal_id')));
		$this->sky->trans_end();
	}

	/**
	 * JOURNAL TEMPLATE
	 *
	 */
	public function journal_template($page=0){	
		$data['rows']	= $this->journal_template_model->count_doc();
		$data['datatable']	= $this->journal_template_model->getAll(NULL,$page);
		$data['datatable_detail'] = $this->journal_template_model->list_detail();
		$this->sky->load_page("journal_template",$data);
	}
	public function journal_template_add(){	
		$data['doc_num'] = $this->document_model->getNextSeries('JRNT');
		$data['ddl_trans'] = $this->account_model->getTransCode();
		$data['dialog_account']	= $this->account_model->view(array('is_postable'=>1));
		$this->sky->load_page('journal_template_add',$data);
	}
	public function journal_template_edit($code){
		$data['ddl_trans'] = $this->account_model->getTransCode();
		$data['dialog_account'] = $this->account_model->view(array('is_postable'=>1));
		$data['form_header'] = $this->journal_template_model->getHeader($code);
		$data['form_detail'] = $this->journal_template_model->getDetail($code);
		$this->sky->load_page("journal_template_edit",$data);
	}
	public function journal_template_add_query(){
		//DATA
		$data_header = array(
			'code'	=> $this->input->post('jrnl_code'),
			'name'	=> $this->input->post('jrnl_name')
		);			
		$data_detail= array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){
				array_push($data_detail, array(					
					'line'	=> $x+1,
					'account_id'	=> $this->input->post('account_id')[$x],
					'journal_dr'	=> $this->input->post('dr')[$x],
					'journal_cr'	=> $this->input->post('cr')[$x]
				));
			}
		}
		//TRANS
		$this -> db -> trans_begin();	
		$data['t_account_journal_template_header'] = $data_header;
		$data['t_account_journal_template_detail'] = $data_detail;
		$this->journal_template_model->add($data);
		$this->sky->trans_end();
	}
	public function journal_template_edit_query(){
		//DATA
		$data_header = array(
			'id'	=> $this->input->post('id'),
			'name'	=> $this->input->post('jrnl_name')
		);			
		$data_detail= array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){
				array_push($data_detail, array(					
					'line'	=> $x+1,
					'account_id'	=> $this->input->post('account_id')[$x],
					'journal_dr'	=> $this->input->post('dr')[$x],
					'journal_cr'	=> $this->input->post('cr')[$x]
				));
			}
		}
		//TRANS
		$this -> db -> trans_begin();	
		$data['t_account_journal_template_header'] = $data_header;
		$data['t_account_journal_template_detail'] = $data_detail;
		$this->journal_template_model->edit($data,array('id'=>$this->input->post('id')));
		$this->sky->trans_end();
	}
	
	
	//ajax
	public function ajax_get_ap_with_vendor(){
		if(isset($_GET['buss_id']) && $_GET['buss_id'] != NULL){
			echo json_encode($this -> finance_model -> get_ap($_GET['buss_id']));
		}
	}
	
	public function ajax_account_info(){
		echo json_encode($this->account_model->ajax_account_info($_GET['account_number']));
	}


	/*===========================================
	=            Cost Center Section            =
	===========================================*/
	
	/**
	*
	* costcenter()
	* display all the cost center in an index page
	*
	**/
	public function costcenter()
	{
		$this->load->model('costcenter_model');
		$data['costcenter'] = $this -> costcenter_model -> get();
		$this->sky->load_page("costcenter",$data);
	}

	/**
	*
	* costcenter_add()
	* show costcenter form add or save if post exists
	*
	*/
	public function costcenter_add()
	{
		$data['msg'] = '';

		if($_POST){
			$data = $this->input->post(NULL, TRUE);
			$this->load->model('costcenter_model');
			if ($this -> costcenter_model -> costcenter_add($data)){
				$data['msg'] = '<div class="alert alert-success">Cost Center Added Successfully</div>';
			} else {
				$data['msg'] = '<div class="alert alert-danger">Unable to save Cost Center</div>';
			}
		}
		$this -> sky -> load_page ('costcenter_add', $data);
	}


	/**
	*
	* costcenter_edit()
	* show costcenter edit form
	*
	*/
	public function costcenter_edit($id = NULL)
	{
		if($id == NULL)
			redirect('account/costcenter','refresh');

		$this->load->model('costcenter_model');

		$data['msg'] = '';

		

		if($_POST){
		
			$post_data = $this->input->post(NULL, TRUE);
			
			$id = $post_data['id'];
			unset($post_data['id']);

			if ($this -> costcenter_model -> costcenter_edit($id,$post_data)){
				$data['msg'] = '<div class="alert alert-success">Cost Center Updated Successfully</div>';
			} else {
				$data['msg'] = '<div class="alert alert-danger">Unable to update Cost Center</div>';
			}
		}
		
		$data['costcenter'] = $this -> costcenter_model -> get($id);

		$this -> sky -> load_page ('costcenter_edit', $data);
	}

	
	/*-----  End of Cost Center Section  ------*/
	
	
}