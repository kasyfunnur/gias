<?php
class Currency extends CI_Controller {	
public function __construct(){
	parent::__construct();
		if (!$this->ion_auth->logged_in()) redirect('auth/login');
		
		$this->load->model('Currency_model');
		$this->load->library('currency');
	}

	
	public function ajax_get_rate(){
		echo json_encode($this->Currency_model->get_rate($this->input->get('from'),$this->input->get('to'),$this->input->get('date')));
		
	}
		
	
}

