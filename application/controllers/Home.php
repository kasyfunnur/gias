<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {	
	
	public function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in()) redirect('auth/login');
		
	}
	
	// dashboard
	public function index(){
		
		$this -> load -> model ('dashboard_model');
		$data['total_stock_count']  = $this -> dashboard_model -> total_stock_count();
		$data['total_style']  = $this -> dashboard_model -> total_style();
		$data['total_new_order']  = $this -> dashboard_model -> total_new_order();
		$data['total_collection']  = $this -> dashboard_model -> total_collection();
		
		$data['total_purchase']  = $this -> dashboard_model -> total_purchase();
		$data['total_business']  = $this -> dashboard_model -> total_business();
		$data['total_warehouse']  = $this -> dashboard_model -> total_warehouse();
		$data['total_consignment']  = $this -> dashboard_model -> total_business();
		
		
		$this -> sky -> load_page("index", $data);
	}
	
	//JM
	public function reset_password(){
		
		if ($_POST){
			$data = $this -> input -> post (NULL,TRUE);
			
			if ($this -> session -> userdata ('user_id') == $data['hidden_user_id']){
				$newdata['cur_pwd'] = $data['cur_pwd'];
				$newdata['new_pwd'] = $data['new_pwd'];
				$newdata['new_pwd2'] = $data['new_pwd2'];
				$this->load->model('login_lib');				
				if($this->login_lib->change_password($data['hidden_user_id'],$newdata['cur_pwd'],$newdata['new_pwd'],$newdata['new_pwd2'],TRUE)){
					$this->login_lib->logout();
					//$this->form_validation->set_message('reset_password', 'Password has been reset.');
					//return FALSE;
					//$this -> sky -> load_page("profile",NULL,'user_data_updated');
				}else{
					$this -> sky -> load_page("reset_password",NULL,'user_password_reset_failed',1);
					return FALSE;
				}
			}			
		}
		$this -> sky -> load_page("reset_password",NULL,NULL,1);
	}
	public function profile(){
		
		if ($_POST){
			$data = $this -> input -> post (NULL,TRUE);
			//var_dump($data);
			switch ($data['form_type']){
				
				// A. Update user settings and profile data
				case 'update-user-settings':

					// check hidden form user id against the one in the session
					if ($this -> session -> userdata ('user_id') == $data['hidden_user_id'])
					{
						$newdata['full_name'] = $data['full_name'];
						$newdata['user_lang'] = $data['sel_lang'];
						
						// update the current session, so it's reflected in the form
						$this->session->set_userdata($newdata);
						
						$this -> load -> model ('user_model');
						if ($this -> user_model -> update_user_data($data['hidden_user_id'],$newdata))
							$this -> sky -> load_page("profile",NULL,'user_data_updated');
						else
							$this -> sky -> load_page("profile",NULL,'user_data_update_failed');
					} else {
						$this -> sky -> load_page("profile",NULL,'user_data_update_failed');
					}
					break;
				
				case 'reset-pwd':
					// do reset usr pwd
					if ($this -> session -> userdata ('user_id') == $data['hidden_user_id']){
						$newdata['cur_pwd'] = $data['cur_pwd'];
						$newdata['new_pwd'] = $data['new_pwd'];
						$newdata['new_pwd2'] = $data['new_pwd2'];
						$this->load->model('login_lib');
						
						if($this->login_lib->change_password($data['hidden_user_id'],$newdata['cur_pwd'],$newdata['new_pwd'],$newdata['new_pwd2'])){
							//$this -> sky -> load_page("profile",NULL,'user_password_reset');
							//$this->sky->load_page("../login/logout",NULL,'user_password_reset');
							$this->login_lib->logout();
						}else{
							$this -> sky -> load_page("profile",NULL,'user_password_reset_failed');
						}
					}
					break;
					
				default :
					break;	
			}
		} else {
		$this -> sky -> load_page("profile");
		}
	}
	
	/* public function approval(){
		$this -> sky -> load_page("approval");
	} */
}