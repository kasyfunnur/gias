<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Upload extends CI_Controller {	
	public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) redirect('auth/login');
        $this->load->library('upload');
        $this->load->library('sky');
		$this->load->helper('url');
		$this->load->helper('swlayout');
		$this->load->helper('approval');
		$this->load->helper('swconfig');
		$this->load->helper('swstatus');
	}
	
	//function do_upload($doc,$file_name)
	function do_upload()
	{
		$config['upload_path'] = base_url('assets/files/'); 
		$config['allowed_types'] = 'gif|jpg|png|doc|docx|xls|xlsx|pdf|csv|ppt|pptx';
		$config['max_size']	= '2048';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		$this->load->library('upload', $config);

		if ($this->upload->do_upload())
		{
			$data = array('upload_data' => $this->upload->data());
			//$this->load->view('upload_success', $data);
		}else{
			
		}
	}
}