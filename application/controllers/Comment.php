<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Comment extends CI_Controller {	
	public function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this->load->model('comment_model');
	}
	
	public function comment_post(){
		$this->comment_model->postComment($_POST['attach'],$_POST['comment']);
		$user = $this->session->userdata("username");
		//echo "post comment done";
		echo '<tr>
			<td colspan="3">
				<strong>'.$user.'</strong>
				- <span style="font-size:12px;font-weight:lighter !important;color:#888">
					'.date("d M, Y").'
				</span>
				<br />'.$_POST['comment'].'
			</td>
		</tr>';
	}	
}