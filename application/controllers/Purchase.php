<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Purchase extends CI_Controller {	
	public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) redirect('auth/login');
        $this->load->library('pagination');
        $this->load->library('sky');
        $this->load->library('journal');
        $this->load->library('flow');
        //$this->load->library('upload');
		$this->load->helper('url');
		$this->load->helper('swlayout');
		$this->load->helper('approval');
		$this->load->helper('swconfig');
		$this->load->helper('swstatus');

		$this->load->model('location_model');
		$this->load->model('purchase_request_model');		
		$this->load->model('purchase_contract_model');
		$this->load->model('purchase_order_model');
		$this->load->model('purchase_quotation_model');
		$this->load->model('purchase_receipt_model');
		$this->load->model('purchase_invoice_model');
		$this->load->model('purchase_return_model');
		$this->load->model('document_model');
		$this->load->model('inventory_model');
		$this->load->model('asset_model');
		$this->load->model('business_model');
		$this->load->model('status_model');
		$this->load->model('journal_model');
		$this->load->model('approval_model');
		$this->load->model('comment_model');
		$this->load->model('link_account_model');
		$this->load->model('tax_model');
		$this->load->model('currency_model');

		$this->config->load('pagination',TRUE);
	}
	
	public function tes_valid(){
		$this->load->view('purchase/validform');
/*		$this->form_validation->set_rules('userName', 'Username', 'required');
		$this->form_validation->set_rules('userPassword', 'Password', 'required');
		if ($this->form_validation->run() == FALSE){
			$this->load->view('purchase/validform');
		}else{
			$this->load->view('purchase/success');
		}*/
	}

	public function tes_process(){
		$this->form_validation->set_rules('userName', 'Username', 'required');
		$this->form_validation->set_rules('userPassword', 'Password', 'required');
		if ($this->form_validation->run() == FALSE){
			$this->load->view('purchase/validform');
		}else{
			$this->load->view('purchase/success');
		}
	}	
	public function index($page="purchase/purchase_order_list"){
		$this->load_page($page);
	}
	
	//CONTRACT FORM
	public function purchase_contract($page=0)
	{
		if(isset($_POST['btn_filter'])){ 
			$data['datatable'] = $this -> purchase_contract_model -> getAll($_POST['btn_filter'],$page);
		}else{ 
			$data['datatable'] = $this -> purchase_contract_model -> getAll(NULL,$page);
		}
		$data['rows'] = $this->purchase_contract_model->count_doc();
		$this->sky->load_page("purchase_contract_index",$data);
	}
	public function purchase_contract_add()
	{
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_CON');
		$data['ddl_whse'] 			= $this->location_model->getAll(array('is_warehouse'=>1,'is_virtual'=>0));
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$this->sky->load_page("purchase_contract_add",$data);
	}
	public function purchase_contract_edit($code)
	{
		$data['form_header'] 		= $this->purchase_contract_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_contract_model->getDetail($code);
		$data['ddl_whse'] 			= $this->location_model->getAll(array('is_warehouse'=>1,'is_virtual'=>0));
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$this->sky->load_page("purchase_contract_edit",$data);
	}
	public function purchase_contract_cancel($code)
	{
		$data['data']	= "";
		$this->sky->load_page("purchase_contract_cancel",$data);
	}
	public function purchase_contract_close($code)
	{
		$data['data']	= "";
		$this->sky->load_page("purchase_contract_close",$data);
	}

	//CONTRACT QUERY
	public function purchase_contract_add_query()
	{
		//DATA
		$data_business = array(
			"buss_name"		=> $this->input->post('buss_name'),
			"is_vendor"		=> 1			
		);
		$document_number = $this->document_model->getNextSeries('PUR_CON');
		$data_header = array(
			"doc_num" 		=> $document_number,
			"doc_status" 	=> "DRAFT",
			"buss_id" 		=> $this->input->post('buss_id'),
			"contact_name"	=> $this->input->post('contact_name'),
			"contact_phone"	=> $this->input->post('contact_phone'),
			"contact_email"	=> $this->input->post('contact_email'),
			"agreement_method" => $this->input->post('agreement_method'),
			"start_dt"		=> $this->input->post('start_dt'),
			"end_dt"		=> $this->input->post('end_dt'),
			"terminate_dt"	=> $this->input->post('terminate_dt'),
			"sign_dt"		=> $this->input->post('sign_dt'),
			"payment_term"	=> $this->input->post('payment_term'),
			"payment_method"=> $this->input->post('payment_method'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"create_by"		=> $this->session->userdata("username"),
			"create_dt"		=> date('Y-m-d H:m:s')
		);
		$data_detail = array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){
				$temp_qty = $this->input->post('ast_qty')[$x];
				$temp_price = $this->input->post('ast_price')[$x];
				$temp_total = $temp_qty*$temp_price;
				$temp_info = $this->inventory_model->get_asset_info($this->input->post('item_id')[$x]);
				array_push($data_detail, array(
						"doc_line" 		=> $_POST['row'][$x],
						"item_id" 		=> $this->input->post('item_id')[$x],
						"item_name" 	=> $this->input->post('ast_name')[$x],
						"item_qty" 		=> $this->input->post('ast_qty')[$x],
						"item_price" 	=> $this->input->post('ast_price')[$x],
						"item_uom"		=> $this->input->post('ast_uom')[$x],
						"item_total" 	=> $this->input->post('sub_total')[$x]
					)
				);
			}
		}

		//FILE UPLOAD
		$this->load->helper('file');
		$config = array(
			'upload_path' => './assets/files/',
			'allowed_types' => 'gif|jpg|png|jpeg|doc|docx|xls|xlsx|pdf|txt',
			'overwrite' => TRUE,
			'max_size' => "2048000"
			//'file_name' => uniqid()
		);
		$this->load->library('upload',$config);

		$orig_name = $_FILES['upload_file']['name'];
		$_FILES['upload_file']['name'] = uniqid()."_".$orig_name;

		/******* TRANS *******/
		$this -> db -> trans_begin();
		$data['t_business'] = $data_business;	
		$data['t_purchase_contract_header'] = $data_header;
		$data['t_purchase_contract_detail'] = $data_detail;	
		if($this->input->post('save_confirm') == 'confirm'){
			$data['t_purchase_contract_header']['doc_status'] 	= "AWAITING";
			$approval_status = request_approval($this->ion_auth->user()->row()->id, 'PUR_CON', $document_number);
		}
		$this->purchase_contract_model->add($data);
		//$this->inventory_model->recalculate();

		if($this->upload->do_upload('upload_file')){
			$doc_num = $data['t_purchase_contract_header']['doc_num'];
			$file_name = $_FILES['upload_file']['name'];
			$this->document_model->add_attachment($doc_num,$file_name);
		}else{
			echo 'Unable to upload'; return false;
		}

		$this->sky->trans_end($this->input->post('return_url'));
	}
	public function purchase_contract_edit_query()
	{
		$data_header = array(
			"buss_id" 		=> $this->input->post('buss_id'),
			"contact_name"	=> $this->input->post('contact_name'),
			"contact_phone"	=> $this->input->post('contact_phone'),
			"contact_email"	=> $this->input->post('contact_email'),
			"agreement_method" => $this->input->post('agreement_method'),
			"start_dt"		=> $this->input->post('start_dt'),
			"end_dt"		=> $this->input->post('end_dt'),
			"terminate_dt"	=> $this->input->post('terminate_dt'),
			"sign_dt"		=> $this->input->post('sign_dt'),
			"payment_term"	=> $this->input->post('payment_term'),
			"payment_method"=> $this->input->post('payment_method'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"create_by"		=> $this->session->userdata("username"),
			"create_dt"		=> date('Y-m-d H:m:s')
		);
		$data_detail = array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){
				$temp_qty = $this->input->post('ast_qty')[$x];
				$temp_price = $this->input->post('ast_price')[$x];
				$temp_total = $temp_qty*$temp_price;
				$temp_info = $this->inventory_model->get_asset_info($this->input->post('item_id')[$x]);
				array_push($data_detail, array(
						"doc_line" 		=> $_POST['row'][$x],
						"item_id" 		=> $this->input->post('item_id')[$x],
						"item_name" 	=> $this->input->post('ast_name')[$x],
						"item_qty" 		=> $this->input->post('ast_qty')[$x],
						"item_price" 	=> $this->input->post('ast_price')[$x],
						"item_uom"		=> $this->input->post('ast_uom')[$x],
						"item_total" 	=> $this->input->post('sub_total')[$x]
					)
				);
			}
		}

		//TRANS
		$this -> db -> trans_begin();
		//$data['t_business'] = $data_business;	
		$data['t_purchase_contract_header'] = $data_header;
		$data['t_purchase_contract_detail'] = $data_detail;	
		if($this->input->post('save_confirm') == 'confirm'){
			$data['t_purchase_contract_header']['doc_status'] 	= "AWAITING";
			$approval_status = request_approval($this->ion_auth->user()->row()->id, 'PUR_CON', $document_number);
		}
		$this->purchase_contract_model->edit($data,array("doc_id" => $this->input->post('doc_id')));
		//$this->inventory_model->recalculate();
		$this->sky->trans_end();
	}
	public function purchase_contract_cancel_query()
	{
		# code...
	}
	public function purchase_contract_close_query()
	{
		# code...
	}

	//QUOTATION FORM
	public function purchase_quotation($page=0){
		if(isset($_POST['btn_filter'])){ 
			$data['datatable'] = $this -> purchase_quotation_model -> getAll($_POST['btn_filter'],$page);
		}else{ 
			$data['datatable'] = $this -> purchase_quotation_model -> getAll(NULL,$page);
		}
		$attach = array();
		foreach($data['datatable'] as $row){
			$attach[] = $row['doc_num'];
		}	
		$data['rows'] = $this->purchase_quotation_model->count_doc(); 		
		$this->sky->load_page("purchase_quotation_index",$data);
	}
	public function purchase_quotation_add(){
		//$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['ddl_curr']			= $this->currency_model->get();
		$data['ddl_whse'] 			= $this->location_model->getAll(array('is_warehouse'=>1,'is_virtual'=>0));
		$data['ddl_tax']			= $this->tax_model->get_all();
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_QUO');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$data['arr_curr_rate']		= $this->currency_model->get_rate_to("IDR");
		$this->load->library('form_validation');
		$this->form_validation->set_rules('demail', 'Email', 'required|valid_email');
		$this->sky->load_page('purchase_quotation_add',$data);
	}
	public function purchase_quotation_edit($code){
		$this->load->helper('download');
		$data['form_header'] 		= $this->purchase_quotation_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_quotation_model->getDetail($code); 
		$data['form_attach']		= $this->purchase_quotation_model->getAttachment($code);
		$data['ddl_curr']			= $this->currency_model->get();
		$data['doc_flow']			= $this->flow->get_flow('purchase','purchase_quotation',$data['form_header'][0]['doc_id']);
		$data['ddl_whse'] 			= $this->location_model->get_site_location();
		$data['ddl_tax']			= $this->tax_model->get_all();
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_ORD');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$data['arr_curr_rate']		= $this->currency_model->get_rate_to("IDR");
		$this->sky->load_page('purchase_quotation_edit',$data);
	}
	public function purchase_quotation_cancel($code){
		$tmp_status = $this->purchase_order_model->getStatus($code);
		$forbid_status = array("CANCELLED","CLOSED");
		if(in_array($tmp_status[0]['doc_status'],$forbid_status)){
			//$this->session->set_flashdata('flash_notification','Setting Saved');
			redirect('purchase/purchase_order','refresh');
		}
		$data['form_header'] = $this->purchase_order_model->getHeader($code);
		$data['form_detail'] = $this->purchase_order_model->getDetail($code);
		$this->sky->load_page('purchase_order_cancel',$data);
	}
	public function purchase_quotation_close($code){
		$tmp_status = $this->purchase_order_model->getStatus($code);
		$forbid_status = array("CANCELLED","CLOSED");
		if(in_array($tmp_status[0]['doc_status'],$forbid_status)){
			//$this->session->set_flashdata('flash_notification','Setting Saved');
			redirect('purchase/purchase_order','refresh');
		}
		$data['form_header'] 		= $this->purchase_order_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_order_model->getDetail($code);
		$this->sky->load_page('purchase_order_close',$data);
	}
	public function purchase_quotation_print($code){
		$data['title'] 				= 'Purchase Order';
		$data['subtitle'] 			= $code;
		$data['form_header'] 		= $this->purchase_order_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_order_model->getDetail($code);
		$this->sky->load_page('print_purchase_order',$data,NULL,1);	
	}
	public function purchase_quotation_inbox($code){
		$data['form_approval']		= $this->approval_model->get_pending_doc($code);
		$data['form_header'] 		= $this->purchase_order_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_order_model->getDetail($code);
		$this->sky->load_page('purchase_order_inbox',$data);
	}
	public function purchase_quotation_duplicate($code){
		$data['form_header'] 		= $this->purchase_order_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_order_model->getDetail($code);
		$data['ddl_whse'] 			= $this->location_model->get_site_location();
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_ORD');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$this->sky->load_page('purchase_order_add',$data);
	}

	//QUOTATION QUERY
	public function purchase_quotation_add_query(){
		//DATA
		$data_business = array(
			"buss_name"		=> $this->input->post('buss_name'),
			"is_vendor"		=> 1			
		);
		$document_number = $this->document_model->getNextSeries('PUR_QUO');
		$data_header = array(
			"doc_num" 		=> $document_number,
			"doc_dt" 		=> $this->input->post('doc_dt'),
			"doc_ddt" 		=> $this->input->post('doc_ddt'),
			"doc_status" 	=> "DRAFT", 
			"buss_id" 		=> $this->input->post('buss_id'),
			"ship_addr" 	=> $this->input->post('ship_addr'),
			//"bill_addr" 	=> $this->input->post('bill_addr'),
			"whse_id" 		=> $this->input->post('location_id'),
			"doc_type" 		=> "ITEM",
			//"doc_ref" 	=> $this->input->post('doc_ref'),
			"doc_curr" 		=> "IDR",
			"doc_rate" 		=> 1,
			"doc_subtotal" 	=> $this->input->post('doc_subtotal'),
			"doc_disc_percent" 	=> $this->input->post('doc_disc_pct'),
			"doc_disc_amount" 	=> $this->input->post('doc_disc'),
			"doc_total" 	=> $this->input->post('doc_total'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"create_by"		=> $this->session->userdata("username"),
			"create_dt"		=> date('Y-m-d H:m:s')
		);
		$data_detail = array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){
				$temp_qty = $this->input->post('ast_qty')[$x];
				$temp_price = $this->input->post('ast_price')[$x];
				$temp_total = $temp_qty*$temp_price;
				$temp_info = $this->inventory_model->get_asset_info($this->input->post('item_id')[$x]);
				array_push($data_detail, array(
						"doc_line" 		=> $_POST['row'][$x],
						"doc_line_type" => 'ITEM',
						"item_group" 	=> $temp_info['item_g_id'],
						"item_id" 		=> $this->input->post('item_id')[$x],
						"item_name" 	=> $this->input->post('ast_name')[$x],
						"item_qty" 		=> $this->input->post('ast_qty')[$x],
						"item_disc_pct1"=> $this->input->post('ast_disc')[$x],
						"item_tax"		=> $this->input->post('ast_tax')[$x],
						"item_price" 	=> $this->input->post('ast_price')[$x],
						"item_netprice" => $this->input->post('ast_netprice')[$x],
						"item_cost" 	=> $this->input->post('ast_price')[$x],
						"item_total" 	=> $this->input->post('sub_total')[$x]
					)
				);
			}
		}
		
		//FILE UPLOAD
		$this->load->helper('file');
		$config = array(
			'upload_path' => './assets/files/',
			'allowed_types' => 'gif|jpg|png|jpeg|doc|docx|xls|xlsx|pdf|txt',
			'overwrite' => TRUE,
			'max_size' => "2048000"
			//'file_name' => uniqid()
		);
		$this->load->library('upload',$config);

		$orig_name = $_FILES['upload_file']['name'];
		$_FILES['upload_file']['name'] = uniqid()."_".$orig_name;
		//var_dump($_FILES['upload_file']); return false;
		//$this->upload->do_upload('upload_file');
		//var_dump($_FILES); return false;

		//VALIDATION
		
		//TRANS
		$this -> db -> trans_begin();
		$data['t_business'] = $data_business;
		if($this->input->post('buss_id') == 0){
			if(!$this->input->post('buss_name')){
				echo "Vendor name must be inserted.";
				return TRUE;
			}
			$temp_buss_id = $this->business_model->add($data['t_business'],'VEND');
			$data_header['buss_id'] = $temp_buss_id;
		}
		
		$data['t_purchase_quotation_header'] = $data_header;
		$data['t_purchase_quotation_detail'] = $data_detail;		

		//$approval_status = request_approval($this->session->userdata('user_id'), 'PUR_ORD', $document_number);		
		//$data['t_purchase_order_header']['approval_status'] = $approval_status;
		// IND, default di jadikan 1 dulu (pending belum ada approval)
		$data['t_purchase_quotation_header']['approval_status'] = 1;
		

		// IND new user id retrieval from ion auth
		// IND, move request approval AFTER document is created, this is due to the email being sent is empty (review function of the purchase model)
		if($this->input->post('save_confirm') == 'confirm'){
			$data['t_purchase_quotation_header']['doc_status'] 	= "AWAITING";
			$approval_status = request_approval($this->ion_auth->user()->row()->id, 'PUR_QUO', $document_number);
		}
		$this->purchase_quotation_model->add($data);
		$this->inventory_model->recalculate();

		if($this->upload->do_upload('upload_file')){
			$doc_num = $data['t_purchase_quotation_header']['doc_num'];
			$file_name = $_FILES['upload_file']['name'];
			$this->document_model->add_attachment($doc_num,$file_name);
		}else{
			echo 'Unable to upload'; return false;
		}

		$this->sky->trans_end();
	}
	public function purchase_quotation_edit_query(){
		//DATA
		//$document_number = $this->document_model->getNextSeries('PUR_ORD');
		$document_number = $this->input->post('doc_num');
		$data_header = array(			
			"doc_dt" 		=> $this->input->post('doc_dt'),
			"doc_ddt" 		=> $this->input->post('doc_ddt'),
			"buss_id" 		=> $this->input->post('buss_id'),
			"ship_addr" 	=> $this->input->post('ship_addr'),
			"whse_id" 		=> $this->input->post('location_id'),
			"doc_type" 		=> "ITEM",
			"doc_curr" 		=> "IDR",
			"doc_rate" 		=> 1,
			"doc_subtotal" 	=> $this->input->post('doc_subtotal'),
			"doc_disc_percent" 	=> $this->input->post('doc_disc_pct'),
			"doc_disc_amount" 	=> $this->input->post('doc_disc'),
			"doc_total" 	=> $this->input->post('doc_total'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"update_by"		=> $this->session->userdata("username"),
			"update_dt"		=> date('Y-m-d H:m:s')
		);
		$data_detail = array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){
				$temp_qty = $this->input->post('ast_qty')[$x];
				$temp_price = $this->input->post('ast_price')[$x];
				$temp_total = $temp_qty*$temp_price;
				$temp_info = $this->inventory_model->get_asset_info($this->input->post('item_id')[$x]);
				array_push($data_detail, array(
						"doc_line" 		=> $_POST['row'][$x],
						"doc_line_type" => 'ITEM',
						"item_group" 	=> $temp_info['item_g_id'],
						"item_id" 		=> $this->input->post('item_id')[$x],
						"item_name" 	=> $this->input->post('ast_name')[$x],
						"item_qty" 		=> $temp_qty,
						"item_price" 	=> $temp_price,
						/*"item_netprice" => $temp_price,
						"item_cost" 	=> $temp_price,
						"item_total" 	=> $temp_total*/
						"item_disc_pct1"=> $this->input->post('ast_disc')[$x],
						"item_tax"		=> $this->input->post('ast_tax')[$x],
						"item_netprice" => $this->input->post('ast_netprice')[$x],
						"item_cost" 	=> $temp_price,
						"item_total" 	=> $this->input->post('sub_total')[$x]
					)
				);
			}
		}
		
		//VALIDATION
		

		//TRANS
		$this -> db -> trans_begin();
		$data['t_purchase_order_detail'] = $data_detail;
		$data['t_purchase_order_header'] = $data_header;
		
		if($this->input->post('save_confirm') == 'confirm'){
			$data['t_purchase_order_header']['doc_status'] 	= "AWAITING";
			$approval_status = request_approval($this->ion_auth->user()->row()->id, 'PUR_ORD', $document_number);
		}
		$this->purchase_order_model->edit($data,array('doc_id'=>$this->input->post('doc_id')));
		$this->inventory_model->recalculate();
		$this->sky->trans_end();
	}
	public function purchase_quotation_cancel_query(){
		//DATA		
		$data_header = array(
			"doc_status" 	=> "CANCELLED",
			"cancel_by"		=> $this->session->userdata("username"),
			"cancel_dt"		=> date('Y-m-d H:m:s'),
			"cancel_reason" => $this->input->post('reason')
		);
		
		//TRANS	
		$this -> db -> trans_begin();		
		$this -> purchase_order_model -> update_header($data_header,array('doc_id'=>$this->input->post('doc_id')));
		$this -> db -> query("call procUpdateQtyOnHand()");
		$this -> sky -> trans_end();
	}
	public function purchase_quotation_close_query($code){
		//DATA		
		$data_header = array(
			"doc_status" 	=> "CLOSED",
			"update_by"		=> $this->session->userdata("username"),
			"update_dt"		=> date('Y-m-d H:m:s'),
			"cancel_reason" => $this->input->post('reason')
		);

		//TRANS	
		$this -> db -> trans_begin();
		$this -> purchase_order_model -> update_header($data_header,array('doc_id'=>$this->input->post('doc_id')));
		$this -> db -> query("call procUpdateQtyOnHand()");
		$this -> sky -> trans_end();
	}

	//REQUEST FORM
	public function purchase_request(){
		$data['datatable'] 	= $this -> purchase_request_model -> getAll();
		$attach = array();
		foreach($data['datatable'] as $row){
			$attach[] = $row['doc_num'];
		}
		//$data['comments'] 	= $this -> comment_model -> getAll($attach);
		$data['datatableraw'] = $this->purchase_order_model->getAll(); 
		$config['total_rows'] = count($data['datatableraw']);
		$this->sky->load_page("purchase_request_index",$data);
	}
	public function purchase_request_add(){
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_REQ');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('demail', 'Email', 'required|valid_email');
		$this->sky->load_page('purchase_request_add',$data);
	}
	public function purchase_request_edit($code){
		$data['form_header'] 		= $this->purchase_request_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_request_model->getDetail($code);
		$data['ddl_whse'] 			= $this->location_model->get_site_location();
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_REQ');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();		
		$this->sky->load_page('purchase_request_edit',$data);
	}
	public function purchase_request_cancel(){
	}
	public function purchase_request_close(){
	}

	//REQUEST QUERY
	public function purchase_request_add_query(){
		//DATA
		$document_number = $this->document_model->getNextSeries('PUR_REQ');
		$data_header = array(
			//"doc_id" 		=> $this->db->insert_id(),//$temp_doc_id,
			"doc_num" 		=> $document_number,
			"doc_dt" 		=> $this->input->post('doc_dt'),
			"doc_ddt" 		=> $this->input->post('doc_ddt'),
			"doc_status" 	=> "ACTIVE",
			"ship_addr" 	=> $this->input->post('ship_addr'),
			"whse_id" 		=> $this->input->post('location_id'),
			"doc_type" 		=> "ITEM",						
			"doc_note" 		=> $this->input->post('doc_note'),
			"create_by"		=> $this->session->userdata("username"),
			"create_dt"		=> date('Y-m-d H:m:s')
		);
		$data_detail = array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){				
				$temp_info = $this->inventory_model->get_asset_info($this->input->post('item_id')[$x]);
				$temp_qty = $this->input->post('ast_qty')[$x];
				array_push($data_detail, array(
						"doc_line" 		=> $_POST['row'][$x],
						"doc_line_type" => 'ITEM',
						"item_group" 	=> $temp_info['item_g_id'],
						"item_id" 		=> $this->input->post('item_id')[$x],
						"item_name" 	=> $this->input->post('ast_name')[$x],
						"item_qty" 		=> $temp_qty						
					)
				);
			}
		}
		
		//VALIDATION	
		$true = true;
		/*if ($this->form_validation->run('purchase/purchase_order_add_query') == FALSE){
			echo validation_errors();
			!$true;
		}*/
		/*if ($this->form_validation->run('inventory/item_length') == FALSE){
			echo "\n"."At least 1 item must be selected.";
			!$true;
		}*/		
		if(!$true)return false;
		
		//TRANS
		$this -> db -> trans_begin();
		$data['t_purchase_request_header'] = $data_header;
		$data['t_purchase_request_detail'] = $data_detail;
		//$approval_status = request_approval($this->session->userdata('user_id'), 'PUR_ORD', $document_number);
		$this->purchase_request_model->add($data);		
		$this->sky->trans_end();
	}
	public function purchase_request_edit_query(){
	}
	public function purchase_request_cancel_query(){
	}
	public function purchase_request_close_query(){
	}

	public function ajax_curr()
	{
		$this->currency_model->get_rate('usd','idr');
		echo json_encode($this -> inventory_model -> get_stock_by_vendor($_POST['buss_char']));
	}

	//ORDER FORM
	public function purchase_order($page=0){
		if(isset($_POST['btn_filter'])){ 
			$data['datatable'] = $this -> purchase_order_model -> getAll($_POST['btn_filter'],$page);
		}else{ 
			$data['datatable'] = $this -> purchase_order_model -> getAll(NULL,$page);
		}
		$attach = array();
		foreach($data['datatable'] as $row){
			$attach[] = $row['doc_num'];
		}	
		$data['rows'] = $this->purchase_order_model->count_doc(); 		
		$this->sky->load_page("purchase_order_index",$data);
	}
	public function purchase_order_add(){
		//var_dump($this->currency_model->get_rate('usd','idr'));
		//$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['ddl_curr']			= $this->currency_model->get();
		$data['ddl_whse'] 			= $this->location_model->getAll(array('is_warehouse'=>1,'is_virtual'=>0));
		$data['ddl_tax']			= $this->tax_model->get_all();
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_ORD');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$data['dialog_contract']	= $this->purchase_contract_model->list_contract(NULL);
		$data['arr_curr_rate']		= $this->currency_model->get_rate_to("IDR");
		$this->load->library('form_validation');
		$this->form_validation->set_rules('demail', 'Email', 'required|valid_email');
		$this->sky->load_page('purchase_order_add',$data);
	}
	public function purchase_order_edit($code){
		$this->load->helper('download');
		$data['form_header'] 		= $this->purchase_order_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_order_model->getDetail($code);
		$data['form_attach']		= $this->purchase_order_model->getAttachment($code);
		$data['ddl_curr']			= $this->currency_model->get();
		$data['doc_flow']			= $this->flow->get_flow('purchase','purchase_order',$data['form_header'][0]['doc_id']);
		$data['ddl_whse'] 			= $this->location_model->get_site_location();
		$data['ddl_tax']			= $this->tax_model->get_all();
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_ORD');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$data['arr_curr_rate']		= $this->currency_model->get_rate_to("IDR");		
		$this->sky->load_page('purchase_order_edit',$data);
	}
	public function purchase_order_cancel($code){
		$tmp_status = $this->purchase_order_model->getStatus($code);
		$forbid_status = array("CANCELLED","CLOSED");
		if(in_array($tmp_status[0]['doc_status'],$forbid_status)){
			//$this->session->set_flashdata('flash_notification','Setting Saved');
			redirect('purchase/purchase_order','refresh');
		}
		$data['form_header'] = $this->purchase_order_model->getHeader($code);
		$data['form_detail'] = $this->purchase_order_model->getDetail($code);
		$this->sky->load_page('purchase_order_cancel',$data);
	}
	public function purchase_order_close($code){
		$tmp_status = $this->purchase_order_model->getStatus($code);
		$forbid_status = array("CANCELLED","CLOSED");
		if(in_array($tmp_status[0]['doc_status'],$forbid_status)){
			//$this->session->set_flashdata('flash_notification','Setting Saved');
			redirect('purchase/purchase_order','refresh');
		}
		$data['form_header'] 		= $this->purchase_order_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_order_model->getDetail($code);
		$this->sky->load_page('purchase_order_close',$data);
	}
	public function purchase_order_print($code){
		$data['title'] 				= 'Purchase Order';
		$data['subtitle'] 			= $code;
		$data['form_header'] 		= $this->purchase_order_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_order_model->getDetail($code);
		$this->sky->load_page('print_purchase_order',$data,NULL,1);	
	}
	public function purchase_order_inbox($code){
		$data['form_approval']		= $this->approval_model->get_pending_doc($code);
		$data['form_header'] 		= $this->purchase_order_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_order_model->getDetail($code);
		$this->sky->load_page('purchase_order_inbox',$data);
	}
	public function purchase_order_duplicate($code){
		$data['form_header'] 		= $this->purchase_order_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_order_model->getDetail($code);
		$data['ddl_whse'] 			= $this->location_model->get_site_location();
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_ORD');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$this->sky->load_page('purchase_order_add',$data);
	}

	public function tes_upload()
	{
		var_dump($this->input->post());
		var_dump($_FILES);
		//return false;
		$this->load->helper('file');
		$config = array(
			//'upload_path' => site_url('assets/files'),
			'upload_path' => './assets/files/',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'max_size' => "2048000"
		);
		$this->load->library('upload',$config);


		//$config['upload_path'] = base_url('assets/files');
		//$config['allowed_types'] = 'gif|jpg|png|jpeg|doc|docx|pdf|xls|xlsx|ppt|pptx|txt';
		//$config['max_size']	= '2048';
		/*$config['max_width']  = '1024';
		$config['max_height']  = '768';*/

		//$this->load->library('upload', $config);

		//$this->upload->do_upload ($_FILES['upload']);
		//if($this->upload->do_upload('upload_file')){
		if($this->upload->do_upload('upload_file')){
			echo 'keupload';
		}else{
			echo 'ga bisa';
		}
	}
	//ORDER QUERY
	public function purchase_order_add_query(){
		//DATA
		$data_business = array(
			"buss_name"		=> $this->input->post('buss_name'),
			"is_vendor"		=> 1			
		);
		$document_number = $this->document_model->getNextSeries('PUR_ORD');
		$data_header = array(
			"doc_num" 		=> $document_number,
			"doc_dt" 		=> $this->input->post('doc_dt'),
			"doc_ddt" 		=> $this->input->post('doc_ddt'),
			"doc_status" 	=> "DRAFT", 
			"buss_id" 		=> $this->input->post('buss_id'),
			"ship_addr" 	=> $this->input->post('ship_addr'),
			//"bill_addr" 	=> $this->input->post('bill_addr'),
			"whse_id" 		=> $this->input->post('location_id'),
			"doc_type" 		=> "ITEM",
			//"doc_ref" 	=> $this->input->post('doc_ref'),
			"doc_curr" 		=> $this->input->post('doc_curr'),
			"doc_rate" 		=> $this->input->post('doc_rate'),
			"doc_subtotal" 	=> $this->input->post('doc_subtotal'),
			"doc_disc_percent" 	=> $this->input->post('doc_disc_pct'),
			"doc_disc_amount" 	=> $this->input->post('doc_disc'),
			"doc_total" 	=> $this->input->post('doc_total'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"create_by"		=> $this->session->userdata("username"),
			"create_dt"		=> date('Y-m-d H:m:s')
		);
		$data_detail = array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){
				$temp_qty = $this->input->post('ast_qty')[$x];
				$temp_price = $this->input->post('ast_price')[$x];
				$temp_total = $temp_qty*$temp_price;
				$temp_info = $this->inventory_model->get_asset_info($this->input->post('item_id')[$x]);
				array_push($data_detail, array(
						"doc_line" 		=> $_POST['row'][$x],
						"doc_line_type" => 'ITEM',
						"item_group" 	=> $temp_info['item_g_id'],
						"item_id" 		=> $this->input->post('item_id')[$x],
						"item_name" 	=> $this->input->post('ast_name')[$x],
						"item_qty" 		=> $this->input->post('ast_qty')[$x],
						"item_disc_pct1"=> $this->input->post('ast_disc')[$x],
						"item_tax"		=> $this->input->post('ast_tax')[$x],
						"item_price" 	=> $this->input->post('ast_price')[$x],
						"item_netprice" => $this->input->post('ast_netprice')[$x],
						"item_cost" 	=> $this->input->post('ast_price')[$x],
						"item_total" 	=> $this->input->post('sub_total')[$x]
					)
				);
			}
		}
		
		//FILE UPLOAD
		$this->load->helper('file');
		$config = array(
			'upload_path' => './assets/files/',
			'allowed_types' => 'gif|jpg|png|jpeg|doc|docx|xls|xlsx|pdf|txt',
			'overwrite' => TRUE,
			'max_size' => "2048000"
			//'file_name' => uniqid()
		);
		$this->load->library('upload',$config);

		$orig_name = $_FILES['upload_file']['name'];
		$_FILES['upload_file']['name'] = uniqid()."_".$orig_name;
		//var_dump($_FILES['upload_file']); return false;
		//$this->upload->do_upload('upload_file');
		//var_dump($_FILES); return false;

		//VALIDATION	
		$true = true;
		if ($this->form_validation->run('purchase/purchase_order_add_query') == FALSE){
			echo validation_errors();
			!$true;
		}
		/*if ($this->form_validation->run('inventory/item_length') == FALSE){
			echo "\n"."At least 1 item must be selected.";
			!$true;
		}*/		
		if(!$true)return false;

		
		/******* TRANS *******/
		$this -> db -> trans_begin();
		$data['t_business'] = $data_business;
		if($this->input->post('buss_id') == 0){
			if(!$this->input->post('buss_name')){
				echo "Vendor name must be inserted.";
				return TRUE;
			}
			$temp_buss_id = $this->business_model->add($data['t_business'],'VEND');
			$data_header['buss_id'] = $temp_buss_id;
		}
		
		$data['t_purchase_order_header'] = $data_header;
		$data['t_purchase_order_detail'] = $data_detail;		

		//$approval_status = request_approval($this->session->userdata('user_id'), 'PUR_ORD', $document_number);		
		//$data['t_purchase_order_header']['approval_status'] = $approval_status;
		// IND, default di jadikan 1 dulu (pending belum ada approval)
		$data['t_purchase_order_header']['approval_status'] = 1;
		

		// IND new user id retrieval from ion auth
		// IND, move request approval AFTER document is created, this is due to the email being sent is empty (review function of the purchase model)
		$this->purchase_order_model->add($data);

		if($this->input->post('save_confirm') == 'confirm'){
			// $data['t_purchase_order_header']['doc_status'] 	= "AWAITING";
			$this -> purchase_order_model -> update_header (
				array('doc_status'=>'AWAITING'), array("doc_num" 		=> $document_number));
			$approval_status = request_approval($this->ion_auth->user()->row()->id, 'PUR_ORD', $document_number);

		}		
		
		$this->inventory_model->recalculate();

		
		if($this->upload->do_upload('upload_file')){
			$doc_num = $data['t_purchase_order_header']['doc_num'];
			$file_name = $_FILES['upload_file']['name'];
			$this->document_model->add_attachment($doc_num,$file_name);
		}else{
			echo 'Unable to upload'; return false;
		}

		//$this->inventory_model->recalculate();
		$this->sky->trans_end($this->input->post('return_url'));
	}
	public function purchase_order_edit_query(){
		//DATA
		//$document_number = $this->document_model->getNextSeries('PUR_ORD');
		$document_number = $this->input->post('doc_num');
		$data_header = array(			
			"doc_dt" 		=> $this->input->post('doc_dt'),
			"doc_ddt" 		=> $this->input->post('doc_ddt'),
			"buss_id" 		=> $this->input->post('buss_id'),
			"ship_addr" 	=> $this->input->post('ship_addr'),
			"whse_id" 		=> $this->input->post('location_id'),
			"doc_type" 		=> "ITEM",
			"doc_curr" 		=> "IDR",
			"doc_rate" 		=> 1,
			"doc_subtotal" 	=> $this->input->post('doc_subtotal'),
			"doc_disc_percent" 	=> $this->input->post('doc_disc_pct'),
			"doc_disc_amount" 	=> $this->input->post('doc_disc'),
			"doc_total" 	=> $this->input->post('doc_total'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"update_by"		=> $this->session->userdata("username"),
			"update_dt"		=> date('Y-m-d H:m:s')
		);
		$data_detail = array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){
				$temp_qty = $this->input->post('ast_qty')[$x];
				$temp_price = $this->input->post('ast_price')[$x];
				$temp_total = $temp_qty*$temp_price;
				$temp_info = $this->inventory_model->get_asset_info($this->input->post('item_id')[$x]);
				array_push($data_detail, array(
						"doc_line" 		=> $_POST['row'][$x],
						"doc_line_type" => 'ITEM',
						"item_group" 	=> $temp_info['item_g_id'],
						"item_id" 		=> $this->input->post('item_id')[$x],
						"item_name" 	=> $this->input->post('ast_name')[$x],
						"item_qty" 		=> $temp_qty,
						"item_price" 	=> $temp_price,
						/*"item_netprice" => $temp_price,
						"item_cost" 	=> $temp_price,
						"item_total" 	=> $temp_total*/
						"item_disc_pct1"=> $this->input->post('ast_disc')[$x],
						"item_tax"		=> $this->input->post('ast_tax')[$x],
						"item_netprice" => $this->input->post('ast_netprice')[$x],
						"item_cost" 	=> $temp_price,
						"item_total" 	=> $this->input->post('sub_total')[$x]
					)
				);
			}
		}
		
		//VALIDATION
		

		//TRANS
		$this -> db -> trans_begin();
		$data['t_purchase_order_detail'] = $data_detail;
		$data['t_purchase_order_header'] = $data_header;
		
		if($this->input->post('save_confirm') == 'confirm'){
			$data['t_purchase_order_header']['doc_status'] 	= "AWAITING";
			$approval_status = request_approval($this->ion_auth->user()->row()->id, 'PUR_ORD', $document_number);
		}
		$this->purchase_order_model->edit($data,array('doc_id'=>$this->input->post('doc_id')));
		$this->inventory_model->recalculate();
		$this->sky->trans_end();
	}
	public function purchase_order_cancel_query(){
		//DATA		
		$data_header = array(
			"doc_status" 	=> "CANCELLED",
			"cancel_by"		=> $this->session->userdata("username"),
			"cancel_dt"		=> date('Y-m-d H:m:s'),
			"cancel_reason" => $this->input->post('reason')
		);
		
		//TRANS	
		$this -> db -> trans_begin();		
		$this -> purchase_order_model -> update_header($data_header,array('doc_id'=>$this->input->post('doc_id')));
		$this -> db -> query("call procUpdateQtyOnHand()");
		$this -> sky -> trans_end();
	}
	public function purchase_order_close_query($code){
		//DATA		
		$data_header = array(
			"doc_status" 	=> "CLOSED",
			"update_by"		=> $this->session->userdata("username"),
			"update_dt"		=> date('Y-m-d H:m:s'),
			"cancel_reason" => $this->input->post('reason')
		);

		//TRANS	
		$this -> db -> trans_begin();
		$this -> purchase_order_model -> update_header($data_header,array('doc_id'=>$this->input->post('doc_id')));
		$this -> db -> query("call procUpdateQtyOnHand()");
		$this -> sky -> trans_end();
	}
	public function purchase_order_attachment_query($code)
	{
		//TRANS
		//echo "yeaaayyy"; return false;
		//$this -> db -> trans_begin();
		//FILE UPLOAD
		$this->load->helper('file');
		$config = array(
			'upload_path' => './assets/files/',
			'allowed_types' => 'gif|jpg|png|jpeg|doc|docx|xls|xlsx|pdf|txt',
			'overwrite' => TRUE,
			'max_size' => "2048000"
			//'file_name' => uniqid()
		);
		$this->load->library('upload',$config);

		//var_dump($_FILES);
		
		//$this->document_model->del_attachment($code);
		//var_dump($this->input->post('upload_filename'));
		$files = $_FILES;

		$total_file = count($_FILES['upload_file']['name']);
		for($x=0;$x<$total_file;$x++){

		  	$_FILES['upload_file']['name']= $files['upload_file']['name'][$x];
	        $_FILES['upload_file']['type']= $files['upload_file']['type'][$x];
	        $_FILES['upload_file']['tmp_name']= $files['upload_file']['tmp_name'][$x];
	        $_FILES['upload_file']['error']= $files['upload_file']['error'][$x];
	        $_FILES['upload_file']['size']= $files['upload_file']['size'][$x];    
		    //$this->upload->do_upload();
			if($this->upload->do_upload('upload_file')){
				$file_name = $_FILES['upload_file']['name'];
				$this->document_model->add_attachment($code,$file_name);
				echo "yesaaaa";
			}else{
				echo 'Unable to upload'; return false;
			}
		}


		
		return false;
		//$this->sky->trans_end(site_url('purchase/purchase_order'));
	}

	//RECEIPT FORM
	public function purchase_receipt($page=0){	
		if(isset($_POST['btn_filter'])){
			$data['datatable'] = $this -> purchase_receipt_model -> getAll($_POST['btn_filter'],$page);
		}else{
			$data['datatable'] = $this -> purchase_receipt_model -> getAll(NULL,$page);
		}		
		$data['rows'] = $this->purchase_receipt_model->count_doc(); 		
		$this->sky->load_page('purchase_receipt_index',$data);
	}
	public function purchase_receipt_add($code=NULL){
		if($code){ 
			$tmp_status = $this->purchase_order_model->getStatus($code);
			$allow_status = array("ACTIVE");
			if(!in_array($tmp_status[0]['doc_status'],$allow_status)){
				//$this->session->set_flashdata('flash_notification','Setting Saved');
				redirect('purchase/purchase_order','refresh');
			}
		}

		$data['param'] = $code;
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_RCV');
		$data['dialog_po'] 			= $this->purchase_order_model->getAll_with_vendor(NULL);
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$this->sky->load_page('purchase_receipt_add',$data);
	}	
	public function purchase_receipt_edit($code){
		$data['form_header'] 		= $this->purchase_receipt_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_receipt_model->getDetail($code);
		$data['ddl_whse'] 			= $this->location_model->get_site_location();
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_RCV');
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$this->sky->load_page('purchase_receipt_edit',$data);
	}
	public function purchase_receipt_cancel($code){	
		$data['form_header'] 		= $this->purchase_receipt_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_receipt_model->getDetail($code);
		$this->sky->load_page('purchase_receipt_cancel',$data);
	}
	public function purchase_receipt_close($code){	
		$data['form_header'] 		= $this->purchase_receipt_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_receipt_model->getDetail($code);
		$this->sky->load_page('purchase_receipt_close',$data);
	}
	public function purchase_receipt_print($code){
		$data['title']				= 'Penerimaan Barang';
		$data['subtitle'] 			= $code;
		$data['form_header'] 		= $this->purchase_receipt_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_receipt_model->getDetail($code);
		$this->sky->load_page('print_purchase_receipt',$data,NULL,1);	
	}

	//RECEIPT QUERY
	public function purchase_receipt_add_query(){
		//DATA
		$document_number = $this->document_model->getNextSeries('PUR_RCV');
		$data_header = array(		
			"doc_num" 			=> $document_number,
			"doc_dt" 			=> $this->input->post('doc_dt'),
			"doc_ddt" 			=> $this->input->post('doc_ddt'),
			"doc_status" 		=> "ACTIVE",
			"buss_id" 			=> $this->input->post('buss_id'),
			"ship_addr" 		=> $this->input->post('ship_addr'),			
			"whse_id" 			=> $this->input->post('location_id'),
			"doc_type" 			=> "ITEM",
			"doc_ref" 			=> $this->input->post('doc_ref'),
			"doc_curr" 			=> "IDR",
			"doc_rate" 			=> 1,
			"doc_subtotal" 		=> $this->input->post('doc_subtotal'),
			"doc_disc_percent" 	=> $this->input->post('doc_disc_pct'),
			"doc_disc_amount" 	=> $this->input->post('doc_disc'),
			"doc_total" 		=> $this->input->post('doc_total'),
			"doc_note" 			=> $this->input->post('doc_note'),
			"create_by"			=> $this->session->userdata("username"),
			"create_dt"			=> date('Y-m-d H:m:s')
		);
		$data_detail = array();
		$data_other = array();
		$journal_total = 0;
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){
				$temp_qty = $this->input->post('ast_qty')[$x];
				$temp_price = $this->input->post('ast_price')[$x];
				$temp_total = $temp_qty*$temp_price;
				$temp_info = $this->inventory_model->get_asset_info($this->input->post('item_id')[$x]);
				array_push($data_detail, array(
						"doc_line" 		=> $_POST['row'][$x],
						"doc_line_type" => 'ITEM',
						"item_group" 	=> $temp_info['item_g_id'],
						"item_id" 		=> $this->input->post('item_id')[$x],
						"item_name" 	=> $this->input->post('ast_name')[$x],
						"item_qty" 		=> $this->input->post('ast_qty')[$x],
						"item_price" 	=> $this->input->post('ast_price')[$x],
						"item_disc_pct1"=> $this->input->post('ast_disc')[$x],
						"item_tax" 		=> $this->input->post('ast_tax')[$x],
						"item_netprice" => $this->input->post('ast_netprice')[$x],
						"item_cost" 	=> $this->input->post('ast_price')[$x],
						"item_total" 	=> $this->input->post('sub_total')[$x],
						"reff_type"		=> "PO",
						"reff_line"		=> $this->input->post('reff_line')[$x]
					)
				);
				array_push($data_other,array(
						"ast_po_qty"	=> $this->input->post('ast_po_qty')[$x],
						"ast_qty_open"	=> $this->input->post('ast_qty_open')[$x]
					)
				);
				$journal_total += $this->input->post('sub_total')[$x];
			}
		}
		
		//VALIDATION		
		
		//TRANS
		$this -> db -> trans_begin();
		$data['t_purchase_receipt_detail'] = $data_detail;
		$data['t_purchase_receipt_header'] = $data_header;
		$data['other'] = $data_other;
		//$approval_status = request_approval($this->session->userdata('user_id'), 'PUR_ORD', $this->document_model->getNextSeries('PUR_RCV'));
		$approval_status = 3;
		$this->purchase_receipt_model->add($data);
		$this->inventory_model->recalculate();
		
		$this->purchase_order_model->set_item_close($this->input->post('doc_ref'));
		$this->purchase_order_model->set_doc_close($this->input->post('doc_ref'));
		//$this->document_model->useDocSeries('PUR_RCV');
		

		/**
 		 * 
		 * $this->journal->start('JRNL');
		 * $this->journal->dr('inventory.asset',200000);
		 * $this->journal->cr('purchase.accrue',200000);
		 * $this->journal->end();
		 * 
		 */

		$header_info = array(
			"doc_dt"	=> $this->input->post('doc_dt'),
			"doc_note"	=> $this->input->post('doc_note'),
			"doc_ref1"	=> $document_number
		);
		$this->journal->start('JRNL',$header_info);
		$this->journal->add('D','inventory.asset',$journal_total);
		$this->journal->add('C','purchase.accrue',$journal_total);
		$this->journal->end();





		////////////////////////
		/*$curr_jur_num = $this->document_model->getNextSeries('JRNL');
		// IND fix, harusnya useDocSeries
		//$curr_jur_num = $this->document_model->useDocSeries('JRNL');
		$data_journal_header = array(
			"journal_code"		=> $curr_jur_num,
			"posting_date"		=> $this->input->post('doc_dt'),
			"document_date"		=> $this->input->post('doc_dt'),
			"journal_memo"		=> $this->input->post('doc_note'),
			"journal_type"		=> 'RCV',
			"journal_ref1"		=> $document_number,
			"create_by"			=> $this->session->userdata("username"),
			"create_dt"			=> date('Y-m-d H:m:s')

		);
		$this->journal_model->table = 't_account_journal_header';
		$jrnl_temp_id = $this->journal_model->create($data_journal_header);
		
		//journal detail
		$inventory_acc = $this -> link_account_model -> get_account('inventory.asset');
        if(!$inventory_acc){
            show_error('Unable to get Link Account for Inventory Asset (code: `inventory.asset`)');
            exit();
        }
		$data_journal_detail_dr = array(
			"journal_id"		=> $jrnl_temp_id,
			"journal_line"		=> 1,
			"account_id"		=> $inventory_acc,
			"journal_dr"		=> $temp_total,
			"journal_cr"		=> 0,
			"journal_cc"		=> 0,
			"journal_curr"		=> 'IDR'
		);
		$purchase_acc = $this -> link_account_model -> get_account('purchase.accrue');
        if(!$purchase_acc){
            show_error('Unable to get Link Account for Purchase Account (code: `purchase.accrue`)');
            exit();
        }
		$data_journal_detail_cr = array(
			"journal_id"		=> $jrnl_temp_id,
			"journal_line"		=> 2,
			"account_id"		=> $purchase_acc,
			"journal_dr"		=> 0,
			"journal_cr"		=> $temp_total,
			"journal_cc"		=> 0,
			"journal_curr"		=> 'IDR'
		);
		$this->journal_model->table = 't_account_journal_detail';
		$this->journal_model->create($data_journal_detail_dr);
		$this->journal_model->create($data_journal_detail_cr);
		$this->document_model->useDocSeries('JRNL');*/

		$this->sky->trans_end();
	}
	public function purchase_receipt_edit_query($code){	
		//1.INIT
		//2.POST VAR & OTHER VAR
		$data_header = array(
			"doc_ext_ref" 	=> $this->input->post('doc_ext_ref'),
			"doc_note" 		=> $this->input->post('doc_note')
		);		
		//3.VALIDATION
		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		//4.2.RUN TRANS
		$where = array();
		$where[0] = array(
			"field"	=> 'doc_num',
			"value" => $code
		);
		$this->purchase_receipt_model->update_header($data_header,$where);
		
		//4.3.END TRANS	
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			echo $this->db->_error_message();
			return false;
		}else{
			echo "1"; return true;
		}
	}	
	public function purchase_receipt_cancel_query($code){		
		//1.INIT
		//2.POST VAR & OTHER VAR
		//3.VALIDATION		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		$data_header = array(
			"doc_status" 	=> "CANCELLED",
			"cancel_by"		=> $this->session->userdata("username"),
			"cancel_dt"		=> date('Y-m-d H:m:s'),
			"cancel_reason" => $this->input->post('reason')
		);
		$where = array();
		$where[0] = array(
			"field"	=> 'doc_num',
			"value" => $code
		);
		$this->purchase_receipt_model->update_header($data_header,$where);
		$this->inventory_model->reverse_entry($code);
		//4.3.END TRANS	
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			return false;
		}else{
			$this->db->trans_commit();
			//$this->document_model->useDocSeries('PUR_ORD');
			echo "1"; return true;
		}
	}
	public function purchase_receipt_close_query($code){		
		//1.INIT
		//2.POST VAR & OTHER VAR
		//3.VALIDATION		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		$data_header = array(
			"doc_status" 	=> "CLOSED",
			"cancel_by"		=> $this->session->userdata("username"),
			"cancel_dt"		=> date('Y-m-d H:m:s'),
			"cancel_reason" => $this->input->post('reason')
		);
		$where = array();
		$where[0] = array(
			"field"	=> 'doc_num',
			"value" => $code
		);
		$this->purchase_receipt_model->update_header($data_header,$where);
		//4.3.END TRANS	
		$this->sky->trans_end();
	}
	
	//INVOICE FORM
	public function purchase_invoice($page=0){
		if(isset($_POST['btn_filter'])){
			$data['datatable'] = $this -> purchase_invoice_model -> getAll($_POST['btn_filter'],$page);
		}else{
			$data['datatable'] = $this -> purchase_invoice_model -> getAll(NULL,$page);
		}		
		$data['rows'] = $this->purchase_invoice_model->count_doc(); 		
		$this->sky->load_page('purchase_invoice_index',$data);
	}
	public function purchase_invoice_add($code=NULL){
		$data['param'] 				= $code;
		$data['ddl_tax']			= $this->tax_model->get_all();
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_INV');
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$this->sky->load_page('purchase_invoice_add',$data);
	}
	public function purchase_invoice_edit($code){
		$data['param'] 				= $code;
		$data['form_header'] 		= $this->purchase_invoice_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_invoice_model->getDetail($code);
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['doc_num'] 			= $code;
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$data['dialog_rcv']			= '';
		$this->sky->load_page('purchase_invoice_edit',$data);
	}
	public function purchase_invoice_cancel(){
	}
	public function purchase_invoice_close(){
	}
	public function purchase_invoice_print($code){
		$data['title']				= 'Purcase Invoice';
		$data['subtitle'] 			= $code;
		$data['form_header'] 		= $this->purchase_invoice_model->getHeader($code);
		$data['form_detail'] 		= $this->purchase_invoice_model->getDetail($code);
		$this->sky->load_page('print_purchase_invoice',$data,NULL,1);	
	}

	public function purchase_invoice_dp_add($code=NULL){
		$data['param'] 				= $code;
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_DP');
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$this->sky->load_page('purchase_invoice_dp_add',$data);
	}
	public function purchase_invoice_add_query($type=NULL){	
		//DATA
		/*$doc_series = 'PUR_INV';
		if($type == "DP")$doc_series = 'PUR_DP';*/
		$document_number = $this->document_model->getNextSeries('PUR_INV');
		$data_header = array(			
			//"doc_num" 		=> $document_number,
			"doc_dt" 		=> $this->input->post('doc_dt'),
			"doc_ddt" 		=> $this->input->post('doc_ddt'),
			"doc_status" 	=> "APPROVED",
			"buss_id" 		=> $this->input->post('buss_id'),
			"ship_addr" 	=> $this->input->post('ship_addr'),
			"whse_id" 		=> $this->input->post('location_id'),
			"doc_type" 		=> "ITEM",
			"doc_ref" 		=> $this->input->post('doc_ref'),
			"doc_ext_ref" 	=> $this->input->post('doc_ext_ref'),
			"doc_curr" 		=> "IDR",
			"doc_rate" 		=> 1,
			"doc_subtotal" 	=> $this->input->post('doc_total'),
			"doc_disc_percent" 	=> $this->input->post('doc_disc_pct'),
			"doc_disc_amount" 	=> $this->input->post('doc_disc'),
			"doc_total" 	=> $this->input->post('doc_total'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"create_by"		=> $this->session->userdata("username"),
			"create_dt"		=> date('Y-m-d H:m:s')
		);		
		$data_detail = array();
		$data_other = array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){
				$temp_qty = $this->input->post('ast_qty')[$x];
				$temp_price = $this->input->post('ast_price')[$x];
				$temp_total = $temp_qty*$temp_price;
				$temp_info = $this->inventory_model->get_asset_info($this->input->post('item_id')[$x]);
				array_push($data_detail, array(
						"doc_line" 		=> $_POST['row'][$x],
						"doc_line_type" => 'ITEM',
						"item_group" 	=> $temp_info['item_g_id'],
						"item_id" 		=> $this->input->post('item_id')[$x],
						"item_name" 	=> $this->input->post('ast_name')[$x],
						"item_qty" 		=> $temp_qty,
						"item_price" 	=> $temp_price,
						"item_netprice" => $temp_price,
						"item_cost" 	=> $temp_price,
						"item_total" 	=> $temp_total,
						"reff_type"		=> "RCV",
						"reff_line"		=> $this->input->post('reff_line')[$x]
					)
				);
				array_push($data_other,array(
						"ast_rcv_qty"	=> $this->input->post('ast_rcv_qty')[$x],
						"ast_qty_open"	=> $this->input->post('ast_qty_open')[$x]
					)
				);
			}
		}

		//VALIDATION
		
		//TRANS
		$this -> db -> trans_begin();
		$data['t_purchase_invoice_header'] = $data_header;
		$data['t_purchase_invoice_detail'] = $data_detail;
		$data['other']		= $data_other;

		$this->purchase_invoice_model->add($data);									   
		$this->purchase_receipt_model->set_item_close($this->input->post('doc_ref'));
		$this->purchase_receipt_model->set_doc_close($this->input->post('doc_ref'));

	
		//journal header		
		$curr_jur_num = $this->document_model->getNextSeries('JRNL');
		$data_journal_header = array(
			"journal_code"		=> $curr_jur_num,
			"posting_date"		=> $this->input->post('doc_dt'),
			"document_date"		=> $this->input->post('doc_dt'),
			"journal_memo"		=> $this->input->post('doc_note'),
			"journal_type"		=> 'PIV',
			"journal_ref1"		=> $document_number,
			"create_by"			=> $this->session->userdata("username"),
			"create_dt"			=> date('Y-m-d H:m:s')
		);
		$this->journal_model->table = 't_account_journal_header';
		$jrnl_temp_id = $this->journal_model->create($data_journal_header);
		
		//journal detail
		$data_journal_detail_dr = array(
			"journal_id"		=> $jrnl_temp_id,
			"journal_line"		=> 1,
			"account_id"		=> '11230', //must create link account
			"journal_dr"		=> $this->input->post('doc_total'),
			"journal_cr"		=> 0,
			"journal_cc"		=> 0,
			"journal_curr"		=> 'IDR'
		);
		$data_journal_detail_cr = array(
			"journal_id"		=> $jrnl_temp_id,
			"journal_line"		=> 2,
			"account_id"		=> '21101', //must create link account
			"journal_dr"		=> 0,
			"journal_cr"		=> $this->input->post('doc_total'),
			"journal_cc"		=> 0,
			"journal_curr"		=> 'IDR'
		);
		$this->journal_model->table = 't_account_journal_detail';
		$this->journal_model->create($data_journal_detail_dr);
		$this->journal_model->create($data_journal_detail_cr);
		$this->document_model->useDocSeries('JRNL');	
		$this->sky->trans_end();
	}	
	public function purchase_invoice_edit_query($code){		
		//1.INIT
		//2.POST VAR & OTHER VAR
		$data_header = array(
			"doc_dt" 		=> $this->input->post('doc_dt'),
			"doc_ddt" 		=> $this->input->post('doc_ddt'),
			"doc_status" 	=> "ACTIVE",
			"buss_id" 		=> $this->input->post('buss_id'),//$buss[0)['buss_id'),
			"ship_addr" 	=> $this->input->post('ship_addr'),
			"whse_id" 		=> $this->input->post('location_id'),
			"doc_type" 		=> "ITEM",
			"doc_ref" 		=> $this->input->post('doc_ref'),
			"doc_curr" 		=> "IDR",
			"doc_rate" 		=> 1,
			"doc_subtotal" 	=> $this->input->post('amount_total'),
			"doc_total" 	=> $this->input->post('amount_total'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"create_by"		=> $this->session->userdata("username"),
			"create_dt"		=> date('Y-m-d H:m:s')
		);		
		//3.VALIDATION
		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		//4.2.RUN TRANS
		$where = array();
		$where[0] = array(
			"field"	=> 'doc_num',
			"value" => $code
		);
		$temp_temp_id = $this->purchase_invoice_model->update_header($data_header,$where);

		//4.3.END TRANS	
		$this->sky->trans_end();
	}
	public function purchase_invoice_cancel_query(){
	}
	public function purchase_invoice_close_query(){
	}
	
	public function purchase_return(){
		$data['datatable'] = $this -> purchase_return_model -> getAll();
		$this->sky->load_page("purchase_return_index",$data);
	}
	public function purchase_return_add(){
		$data['param']				= "";
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_RET');
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$this->sky->load_page('purchase_return_add',$data);

	}
	public function purchase_return_edit(){
	}
	public function purchase_return_cancel(){
	}
	public function purchase_return_close(){
	}
	public function purchase_return_print(){
	}

	public function purchase_return_add_query(){
		//1.INIT
		//2.POST VAR & OTHER VAR
		$doc_series = 'PUR_RET';
		$document_number = $this->document_model->getNextSeries($doc_series);
		$data_header = array(
			"doc_num" 		=> $document_number,
			"doc_dt" 		=> $this->input->post('doc_dt'),
			"doc_ddt" 		=> $this->input->post('doc_ddt'),
			"doc_status" 	=> "ACTIVE",
			"buss_id" 		=> $this->input->post('buss_id'),//$buss[0)['buss_id'),
			//"ship_addr" 	=> $this->input->post('ship_addr'),
			//"bill_addr" 	=> $this->input->post('bill_addr'),
			"whse_id" 		=> $this->input->post('location_id'),
			"doc_type" 		=> "ITEM",
			"doc_ref" 		=> $this->input->post('doc_ref'),
			"doc_curr" 		=> "IDR",
			"doc_rate" 		=> 1,
			"doc_subtotal" 	=> $this->input->post('amount_total'),
			"doc_total" 	=> $this->input->post('amount_total'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"create_by"		=> $this->session->userdata("username"),
			"create_dt"		=> date('Y-m-d H:m:s')
		);		
		//3.VALIDATION
		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		//4.2.RUN TRANS
		$temp_temp_id = $this->purchase_return_model->insert_header($data_header);
		$temp_id = $temp_temp_id;
		for($x=0;$x<=$this->input->post('table_row')-2;$x++){ 			
			$y = $x+1;
			$temp_qty = $_POST['ast_qty'][$x];//$this->input->post('ast_qty'.$y);
			$temp_price = $_POST['ast_price'][$x];//$this->input->post('ast_price'.$y);
			$temp_total = $temp_qty*$temp_price;
				
			$temp_info = $this->inventory_model->get_asset_info($_POST['item_id'][$x]);
			$data_detail = array(
				"doc_id" 		=> $temp_id,
				"doc_line" 		=> $_POST['row'][$x],//$x+1,
				"doc_line_type" => 'ITEM',
				"item_group" 	=> $temp_info['item_g_id'],//$this->input->post('ast_group'.$y),
				"item_id" 		=> $_POST['item_id'][$x],
				"item_name" 	=> $_POST['ast_name'][$x],//$this->input->post('ast_name'.$y),
				"item_qty" 		=> $temp_qty,
				"item_price" 	=> $temp_price,
				"item_netprice" => $temp_price,
				"item_cost" 	=> $temp_price,
				"item_total" 	=> $temp_total,
				"reff_type"		=> "RCV",
				"reff_id"		=> $this->input->post('doc_ref_id'),
				"reff_line"		=> $_POST['reff_line'][$x]
			);
			$this->purchase_return_model->insert_detail($data_detail);			
		}
		
		//billing status rcv
		$rcv_qty_close = $this->purchase_receipt_model->get_qty_close($this->input->post('rcv_doc_ref'));
		$rcv_close_line = $this->purchase_receipt_model->get_close_line($this->input->post('rcv_doc_ref'));
		$rcv_open_line = $this->purchase_receipt_model->get_open_line($this->input->post('rcv_doc_ref'));
		$flag_run = FALSE;
		$billing_status = 'UNBILLED';
		if(count($rcv_qty_close)){
			$billing_status = 'PARTIAL';
			$flag_run = TRUE;
		}
		if(count($rcv_close_line)){
			$billing_status = 'PARTIAL';
			$flag_run = TRUE;
		}
		if(!count($rcv_open_line)){
			$billing_status = 'BILLED';
			$flag_run = TRUE;
		}
		if($flag_run){
			$data_rcv_header = array(
				"billing_status" => $billing_status
			);
			$this->purchase_receipt_model->table = 't_purchase_receipt_header';
			$this->purchase_receipt_model->update('doc_id',$this->input->post('rcv_doc_ref_id'),$data_rcv_header);
		}
		
		//journal detail
		/*$data_journal_detail_dr = array(
			"journal_id"		=> $jrnl_temp_id,
			"journal_line"		=> 1,
			"account_id"		=> '11230', //must create link account
			"journal_dr"		=> $temp_total,
			"journal_cr"		=> 0,
			"journal_cc"		=> 0,
			"journal_curr"		=> 'IDR'
		);
		$data_journal_detail_cr = array(
			"journal_id"		=> $jrnl_temp_id,
			"journal_line"		=> 2,
			"account_id"		=> '21101', //must create link account
			"journal_dr"		=> 0,
			"journal_cr"		=> $temp_total,
			"journal_cc"		=> 0,
			"journal_curr"		=> 'IDR'
		);
		$this->journal_model->table = 't_account_journal_detail';
		$this->journal_model->create($data_journal_detail_dr);
		$this->journal_model->create($data_journal_detail_cr);*/
		
		//echo 's';
		//4.3.END TRANS	
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			return false;
		}else{
			$this->db->trans_commit();
			if($type == "DP"){
				$this->document_model->useDocSeries('PUR_DP');
			}else{
				$this->document_model->useDocSeries('PUR_INV');	
			}
			//$doc_series = 'PUR_DP';
			$this->document_model->useDocSeries('JRNL');
			echo "1"; return true;
		}
	}
	public function purchase_return_edit_query(){
	}
	public function purchase_return_cancel_query(){
	}
	public function purchase_return_close_query(){
	}
	
	
	//NOT USED
	public function purchase_dialog_asset(){//NOT USED
		$data['assets'] = $this->asset_model->get_asset_all();//NOT USED
		$this->sky->load_page('purchase_dialog_asset',$data,NULL,1);//NOT USED	
	}//NOT USED

	//DIALOG
	public function dialog_purchase_vendor(){
		$data['vendors'] = $this->business_model->getAll();
		$this->sky->load_page('dialog_purchase_vendor',$data,NULL,1);	
	}
	public function dialog_purchase_order($filter="",$value=""){ // full/vendor
		if(($filter == 'vendor') and ($value!="")){
			$data['docs'] = $this->purchase_order_model->getAll_with_vendor($value);			
		}else{
			$data['docs'] = $this->purchase_order_model->getAll();
		}
		$data['docs_detail'] = $this->purchase_order_model->getAll_detail();
		$this->sky->load_page('dialog_purchase_order',$data,NULL,1);	
	}
	

	//AJAX
	public function ajax_getAll_pct_with_vendor()
	{
		if(isset($_GET['buss_id']) && $_GET['buss_id'] != NULL && $_GET['buss_id'] != ""){
			echo json_encode($this -> purchase_contract_model -> getAll_with_vendor($_GET['buss_id']));
		}
	}
	public function ajax_getDetail_pct_open()
	{
		if(isset($_GET['doc_num']) && $_GET['doc_num'] != NULL){
			echo json_encode($this -> purchase_contract_model -> getDetail_pct_open($_GET['doc_num']));
		}
	}
	public function ajax_getAll_po_with_vendor(){
		if(isset($_GET['buss_id']) && $_GET['buss_id'] != NULL){
			if(isset($_GET['logistic_status']) && $_GET['logistic_status'] != NULL){
				if(isset($_GET['billing_status']) && $_GET['billing_status'] != NULL){
					echo json_encode($this -> purchase_order_model -> getAll_with_vendor($_GET['buss_id'],$_GET['logistic_status'],$_GET['billing_status']));
				}else{
					echo json_encode($this -> purchase_order_model -> getAll_with_vendor($_GET['buss_id'],$_GET['logistic_status']));
				}				
			}else{				
				echo json_encode($this -> purchase_order_model -> getAll_with_vendor($_GET['buss_id']));
			}			
		}
	}
	public function ajax_getDetail_po_open(){
		if(isset($_GET['doc_num']) && $_GET['doc_num'] != NULL){
			//echo json_encode($this -> purchase_order_model -> getDetail($_GET['doc_num']));
			echo json_encode($this -> purchase_order_model -> getDetail_po_open($_GET['doc_num']));
		}
	}

	public function ajax_getDetail_po(){
		if(isset($_GET['doc_num']) && $_GET['doc_num'] != NULL){
			//echo json_encode($this -> purchase_order_model -> getDetail($_GET['doc_num']));
			echo json_encode($this -> purchase_order_model -> getDetail($_GET['doc_num']));
		}
	}
	public function ajax_getHeader_po(){
		if(isset($_GET['doc_num']) && $_GET['doc_num'] != NULL){
			echo json_encode($this -> purchase_order_model -> getHeader($_GET['doc_num']));
		}
	}
	
	public function ajax_getAll_rcv_with_vendor(){
		if(isset($_GET['buss_id']) && $_GET['buss_id'] != NULL){
			echo json_encode($this -> purchase_receipt_model -> getAll_with_vendor($_GET['buss_id']));
		}
	}
	public function ajax_getDetail_rcv_open(){
		if(isset($_GET['doc_num']) && $_GET['doc_num'] != NULL){
			//echo json_encode($this -> purchase_order_model -> getDetail($_GET['doc_num']));
			echo json_encode($this -> purchase_receipt_model -> getDetail_rcv_open($_GET['doc_num']));
		}
	}
	public function ajax_getHeader_rcv(){
		if(isset($_GET['doc_num']) && $_GET['doc_num'] != NULL){
			echo json_encode($this -> purchase_receipt_model -> getHeader($_GET['doc_num']));
		}
	}
	
	public function ajax_getAll_pinv_with_vendor(){
		if(isset($_GET['buss_id']) && $_GET['buss_id'] != NULL){
			echo json_encode($this -> purchase_invoice_model -> getAll_with_vendor($_GET['buss_id']));
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */