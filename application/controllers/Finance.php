<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Finance extends CI_Controller {	
	public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this->load->helper('url');
		$this->load->helper('swlayout');
		$this->load->model('location_model');
		$this->load->model('purchase_request_model');		
		$this->load->model('purchase_order_model');
		$this->load->model('purchase_receipt_model');
		$this->load->model('purchase_invoice_model');
		$this->load->model('sales_order_model');
		$this->load->model('sales_delivery_model');
		$this->load->model('sales_invoice_model');
		$this->load->model('document_model');
		$this->load->model('inventory_model');
		$this->load->model('asset_model');
		$this->load->model('business_model');
		$this->load->model('status_model');
		$this->load->model('finance_model');
		$this->load->model('payment_model');
		$this->load->model('journal_model');		
	}
	
	public function index($page="purchase/purchase_order_list"){
		$this->load_page($page);
	}
	public function payment_in(){
		$data['datatable'] 		= $this->payment_model->getAll('in');
		$this->sky->load_page("payment_in_index",$data);
	}
	public function payment_in_add($type=NULL,$code=NULL){
		$data['type']				= $type;
		$data['param'] 				= $code;
		$doc_temp 					= "";
		$data['buss']				= "";
		$data['buss_name']			= "";
		if($type=="SAL_ORD"){
			$doc_temp		 		= $this->purchase_order_model->getHeader($code);
			$data['buss']			= $doc_temp[0]['buss_id'];
			$data['buss_name']		= $doc_temp[0]['buss_name'];
		}
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['doc_num'] 			= $this->document_model->getNextSeries('PAY_IN');
		$data['dialog_customer'] 	= $this->business_model->getCustomer();
		$this->sky->load_page("payment_in_add",$data);
	}
	public function payment_in_edit($code){
		$data['param'] 				= $code;
		$data['doc_num'] 				= $code;
		$data['form_header']		= $this->payment_model->getHeader($code);
		$data['form_detail']		= $this->payment_model->getDetail($code);		
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$this->sky->load_page("payment_in_edit",$data);
	}
	public function payment_in_add_query(){
		//DATA
		$document_number = $this->document_model->getNextSeries('PAY_IN');
		$data_header = array(
			"doc_num" 		=> $document_number,
			"doc_dt" 		=> $this->input->post('head_doc_dt'),
			"doc_ddt" 		=> $this->input->post('head_doc_ddt'),
			"doc_status" 	=> "ACTIVE",
			"buss_id" 		=> $this->input->post('buss_id'),
			"doc_type" 		=> "IN",
			"doc_curr" 		=> "IDR",
			"doc_rate" 		=> 1,
			"doc_subtotal" 	=> $this->input->post('payment'),
			"doc_total" 	=> $this->input->post('payment'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"create_by"		=> $this->session->userdata("username"),
			"create_dt"		=> date('Y-m-d H:m:s')
		);		
		//3.VALIDATION
		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		//4.2.RUN TRANS
		$temp_temp_id = $this->payment_model->insert_header($data_header);
		$temp_id = $temp_temp_id;
		//echo 'table row '.$this->input->post('table_row').'<br>';
		for($x=0;$x<=$this->input->post('table_row')-2;$x++){ 			
			$y = $x+1;
			if($_POST['to_pay'][$x] != 0 && $_POST['to_pay'][$x] != "" ){									
				$data_detail = array(
					"doc_id" 		=> $temp_id,
					"doc_line" 		=> $x+1,	
					"reff_type"		=> "",
					"reff_num"		=> $_POST['doc_num'][$x],
					"reff_to_pay"	=> $_POST['to_pay'][$x]
				);
				$this->payment_model->insert_detail($data_detail);
				
				//piv header
				$financial_status = "UNPAID";
				if($_POST['doc_total'][$x]-$_POST['doc_paid'][$x] <= $_POST['to_pay'][$x]){
					$financial_status = "PAID";
				}
				$data_siv_header = array(
					"financial_status" 	=> $financial_status
				);			
				$field = array('doc_num');
				$value = array($_POST['doc_num'][$x]);
				$this->sales_invoice_model->table = 't_sales_invoice_header';
				$this->sales_invoice_model->update($field,$value,$data_siv_header);
				
				//po header
				$data_so_header = array(
					"doc_paid"			=> $_POST['to_pay'][$x]
				);
				if($_POST['doc_type'][$x] == "SAL_ORD"){
					$this->sales_order_model->table = 't_sales_order_header';
					$this->sales_order_model->update('doc_num',$_POST['doc_num'][$x],$data_so_header);
				}				
			}
		}
		
		//journal header
		$curr_jur_num = $this->document_model->getNextSeries('JRNL');
		$data_journal_header = array(
			"journal_code"		=> $curr_jur_num,
			"journal_date"		=> $this->input->post('head_doc_dt'),
			"journal_memo"		=> $this->input->post('doc_note'),
			"journal_type"		=> 'PYO',
			"journal_ref1"		=> $document_number,
			"create_by"			=> $this->session->userdata("username"),
			"create_dt"			=> date('Y-m-d H:m:s')
		);
		$this->journal_model->table = 't_account_journal_header';
		$jrnl_temp_id = $this->journal_model->create($data_journal_header);
		
		//journal detail
		$data_journal_detail_dr = array(
			"journal_id"		=> $jrnl_temp_id,
			"journal_line"		=> 1,
			"account_id"		=> '21102', //must create link account
			"journal_dr"		=> $this->input->post('total_payment'),
			"journal_cr"		=> 0,
			"journal_cc"		=> 0,
			"journal_curr"		=> 'IDR'
		);
		$this->journal_model->table = 't_account_journal_detail';
		$this->journal_model->create($data_journal_detail_dr);
		
		if($this->input->post('head_bank_amt') != NULL && $this->input->post('head_bank_amt') != 0){
			$data_journal_detail_cr = array(
				"journal_id"		=> $jrnl_temp_id,
				"journal_line"		=> 2,
				"account_id"		=> '11101', //must create link account
				"journal_dr"		=> 0,
				"journal_cr"		=> $this->input->post('head_bank_amt'),
				"journal_cc"		=> 0,
				"journal_curr"		=> 'IDR'
			);
			$this->journal_model->table = 't_account_journal_detail';		
			$this->journal_model->create($data_journal_detail_cr);
		}
		if($this->input->post('head_cash_amt') != NULL && $this->input->post('head_cash_amt') != 0){
			$data_journal_detail_cr = array(
				"journal_id"		=> $jrnl_temp_id,
				"journal_line"		=> 3,
				"account_id"		=> '11111', //must create link account
				"journal_dr"		=> 0,
				"journal_cr"		=> $this->input->post('head_cash_amt'),
				"journal_cc"		=> 0,
				"journal_curr"		=> 'IDR'
			);
			$this->journal_model->table = 't_account_journal_detail';		
			$this->journal_model->create($data_journal_detail_cr);
		}
		//4.3.END TRANS	
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			return false;
		}else{
			$this->db->trans_commit();
			$this->document_model->useDocSeries('PAY_IN');
			$this->document_model->useDocSeries('JRNL');
			echo "1"; return true;
		}
	}
	
	
	
	public function payment_out(){
		$data['datatable'] 		= $this->payment_model->getAll('out');
		$this->sky->load_page("payment_out_index",$data);
	}
	public function payment_out_add($type=NULL,$code=NULL){
		$data['type']				= $type;
		$data['param'] 				= $code;
		$doc_temp 					= "";
		$data['buss']				= "";
		$data['buss_name']			= "";
		if($type=="PUR_ORD"){
			$doc_temp		 		= $this->purchase_order_model->getHeader($code);
			$data['buss']			= $doc_temp[0]['buss_id'];
			$data['buss_name']		= $doc_temp[0]['buss_name'];
		}
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['doc_num'] 			= $this->document_model->getNextSeries('PAY_OUT');
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$this->sky->load_page("payment_out_add",$data);
	}
	public function payment_out_edit($code){		
		$data['form_header']		= $this->finance_model->getHeader($code);
		$data['form_detail']		= $this->finance_model->getDetail($code);
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['doc_num'] 			= $this->document_model->getNextSeries('PAY_OUT');
		$data['dialog_vendor'] 		= $this->business_model->getVendor();
		$this->sky->load_page("payment_out_edit",$data);
	}
	public function payment_out_cancel($code){
		
	}
	public function payment_out_close($value='')
	{
		# code...
	}
	
	public function payment_out_add_query(){
		//DATA
		$document_number = $this->document_model->getNextSeries('PAY_OUT');
		$data_header = array(
			"doc_num" 		=> $document_number,
			"doc_dt" 		=> $this->input->post('head_doc_dt'),
			"doc_ddt" 		=> $this->input->post('head_doc_ddt'),
			"doc_status" 	=> "ACTIVE",
			"buss_id" 		=> $this->input->post('buss_id'),
			"doc_type" 		=> "OUT",
			"doc_curr" 		=> "IDR",
			"doc_rate" 		=> 1,
			"doc_subtotal" 	=> $this->input->post('payment'),
			"doc_total" 	=> $this->input->post('payment'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"create_by"		=> $this->session->userdata("username"),
			"create_dt"		=> date('Y-m-d H:m:s')
		);		
		$data_detail = array();
		$z=0;
		foreach($this->input->post('row') as $x=>$row){
			if($this->input->post('to_pay')[$x]){
				$z++;
				array_push($data_detail, array(
						"doc_line" 		=> $z,	
						"reff_type"		=> "PIV",
						"reff_id"		=> $this->input->post('doc_id')[$x],
						"reff_to_pay"	=> $this->input->post('to_pay')[$x]
					)
				);
			}
		}
		
		//TRANS
		$this -> db -> trans_begin();
		
		$data['t_payment_header'] = $data_header;
		$data['t_payment_detail'] = $data_detail;

		$this->finance_model->add($data);
		
		$this->sky->trans_end();
	}
	
	//ajax
	public function ajax_get_ap_with_vendor(){
		if(isset($_GET['buss_id']) && $_GET['buss_id'] != NULL){
			echo json_encode($this -> finance_model -> get_ap($_GET['buss_id']));
		}
	}
	public function ajax_get_ar_with_customer(){
		if(isset($_GET['buss_id']) && $_GET['buss_id'] != NULL){
			echo json_encode($this -> finance_model -> get_ar($_GET['buss_id']));
		}
	}	
	
}