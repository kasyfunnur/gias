<?php
class Setting extends CI_Controller {
public function __construct(){
	parent::__construct();
		if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this->load->model('account_model');
		$this->load->model('currency_model');
		$this->load->helper('swlayout_helper');
		$this->load->library('sky');
		$this->load->helper('url');
	}

	/*===============================================
	=            User Management Section            =
	===============================================*/

	public function users($action = 'view'){
		$this -> sky -> load_page ('user');
	}

	public function add_user(){
		$this -> load -> model ('user_model');
		$this -> load -> model ('position_model');
		$this -> load -> model ('asset_model');
		$this -> load -> helper ('position');

		// ion auth table configurations
		$tables = $this->config->item('tables','ion_auth');

		// validate input
		$this->form_validation->set_rules('first_name', 'first_name', 'required');
		$this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('user_name', 'Username', 'required|is_unique['.$tables['users'].'.username]');
		$this->form_validation->set_rules('user_pass', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[user_pass_confirm]');
		$this->form_validation->set_rules('user_pass_confirm', 'Confirm Password', 'required');
		$this->form_validation->set_rules('group_id[]', 'Groups', 'required');

		if ($this->form_validation->run() == true)
		{
			$username = strtolower($this->input->post('user_name'));
			$email    = strtolower($this->input->post('user_email'));
			$password = $this->input->post('user_pass');
			$group 	  = $this->input->post('group_id');

			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name'  => $this->input->post('last_name')
			);

			$uid = $this->ion_auth->register($username, $password, $email, $additional_data, $group);

			// insert employee data if check box "set as employee" is checked
			$employee_result = true;
			if($this->input->post('chk_employee'))
			{
				//id, emp_no, first_name, last_name, birthdate, position_id, user_id, location_id, gender
				$emp_data = array(
					'emp_no' => $this->input->post('empno'),
					'first_name' 	=> $this->input->post('first_name'),
					'last_name'  	=> $this->input->post('last_name'),
					'birthdate'		=> date('Y-m-d',strtotime($this->input->post('birthdate'))),
					'position_id'	=> $this->input->post('position'),
					'user_id'		=> $uid,
					'location_id'	=> $this->input->post('location_id'),
					'gender'		=> $this->input->post('gender')
					);
				$add_employee_result = $this->user_model->add_employee($emp_data);
				if(!$add_employee_result)$employee_result=false;
			}

			//if($uid && $add_employee_result){
			if($uid && $employee_result){
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("setting/users", 'refresh');
			}
		}
		/*if ($this->form_validation->run() == true && $uid)
		{
			//check to see if we are creating the user
			//redirect them back to the users page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("settings/users", 'refresh');
		}*/
		else
		{
			//display the create user form
			//set the flash data error message if there is one
			$this->data['msg'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$data['group_array'] 	= $this -> ion_auth -> groups() -> result_array();
			$data['site_location'] 	= $this -> asset_model -> get_site_location();
			$this -> sky -> load_page ('user_add',$data, (isset($this->data['msg']) ? $this->data['msg'] : ""));
		}
	}

	public function edit_user($username)
	{
		$this->load->model('User_model');
		$user_id = $this->User_model->_get_id($username);
		$data['user_info'] = $this->User_model->get_user_data($user_id);
		$this -> sky -> load_page ('user_edit',$data, (isset($this->data['msg']) ? $this->data['msg'] : ""));
	}

	public function datatable_settings_users(){
		$this->load->model('user_model');
		$this->output->set_content_type('application/json');
		echo json_encode($this -> user_model -> datatable_settings_users());
	}


	/**
	*
	* User Group Management
	* Integrate with ion_auth User Groups
	*
	**/

	public function user_groups()
	{
		$this -> sky -> load_page ('user_group');
	}

	public function add_user_group()
	{
		$this -> sky -> load_page ('user_group_add');
	}

	/*-----  End of User Management Section  ------*/



	/*=================================================
	=            Approval Settings Section            =
	=================================================*/

	/**
	 * Approval
	 */
	public function approval(){
		$this->load->model('approval_model');
		$data['datatable'] = $this->approval_model->getAll();
		$this->sky->load_page('approval_index',$data);
	}
	public function approval_edit($id)
	{
		$this->load->model('approval_model');
		$this->load->helper('approval_helper');
		$this->load->model('position_model');
		$this->load->helper('position_helper');

		/*==========  edit part  ==========*/
		$data['formdata'] = $this->input->post(null);
		if($_POST)
		{
			$newdata['header'] = array(
				'appr_name' => $data['formdata']['appr_name'],
				'appr_email' => array_key_exists('appr_email', $data['formdata']) ? $data['formdata']['appr_email'] : false
				);

			// appr_detail_id, appr_id, auto_approve, step_based, requestor_position_id, approval_matrix
			$newdata['detail'] = array();
			for ($i=0; $i < count($data['formdata']['sel_requester']); $i++) {
				array_push($newdata['detail'], array(
					'appr_id' => $data['formdata']['appr_id'],
					'requestor_position_id' => $data['formdata']['sel_requester'][$i],
					'auto_approve' => $data['formdata']['sel_auto_approve'][$i],
					'step_based' => $data['formdata']['sel_step_based'][$i],
					'approval_matrix' => $data['formdata']['hdn_matrix'][$i]
					)
				);
			}
			$this -> approval_model -> update_approval_setting($data['formdata']['appr_id'],$newdata);
			$this->session->set_flashdata('flash_notification','Settings Saved');
			redirect('setting/approval','refresh');
		}
		$data['approval_header'] = $this->approval_model->getApprovalById($id);
		$data['approval'] = $this->approval_model->getApprovalDetail($id);
		$data['positions'] = $this -> position_model -> get_positions();
		$this->sky->load_page('approval_edit',$data);
	}


	public function approval_edit_query()
	{
		$this->load->model('approval_model');
		$data = array(
			"appr_name" => $this->input->post('appr_name'),
			"appr_email" => $this->input->post('appr_email'),
			"appr_bypass" => $this->input->post('appr_bypass')
		);

		$this->db->trans_begin();
		$this->approval_model->update_approval_setting($data,array('appr_id'=>$this->input->post('appr_id')));
		$this->sky->trans_end();
	}

	/*-----  End of Approval Settings Section  ------*/




	public function acc_costcenter(){

	}

	/*========================================
	=            Company Settings            =
	========================================*/

	/**
	*
	* Company Information Settings
	*
	**/

	public function company(){
		$this->load->model('company_model');
		$data['companies'] = $this -> company_model -> get_companies();
		$this->sky->load_page('company/company', $data);
	}

	public function company_add(){
		$this->load->model('company_model');

		if($_POST){
			// insert data
			if ($this -> company_model -> add_company($this->input->post(NULL))){
				$this->session->set_flashdata('flash_notification', 'Company Data Saved');
				redirect('setting/company');
			} else {
				$this->session->set_flashdata('flash_notification', 'Failed to Create Company');
			}
		}

		$this->sky->load_page('company/company_add');

	}

	public function company_edit($comp_id){
		if(!is_numeric($comp_id))
			redirect('setting/company','refresh');

		$this->load->model('company_model');

		if($_POST){
			// update data
			if ($this -> company_model -> update_company($comp_id, $this->input->post(NULL))){
				$this->session->set_flashdata('flash_notification', 'Company Data Updated');
				redirect('setting/company');
			} else {
				$this->session->set_flashdata('flash_notification', 'Failed to Update Data');
			}
		}

		$data['company'] = $this -> company_model -> get_companies($comp_id);
		$this->sky->load_page('company/company_edit', $data);

	}

	/**
	*
	* Org Structure settings
	*
	**/
	public function position()
	{
		$this -> sky -> load_page('position');
	}


	public function ajax_position_list($return="json"){
		$this->load->model ('position_model');
		if  ($return == 'json') echo json_encode($this -> position_model -> ajax_position_list());
	}

	public function position_add(){
		$this -> load -> model ('position_model');
		$this -> load -> helper ('position');
		$data = array();
		if($_POST  ){
			$clean_post = $this -> input -> post(NULL, TRUE);
			$data['msg'] = $this -> position_model -> add_position($clean_post);
			}

		$this -> sky -> load_page ('position_add', $data);
	}


	/*-----  End of Company Settings  ------*/


	/*===============================================
	=            Menu Management setting            =
	===============================================*/

	/**
	*
	* Side menu setting index page
	*
	**/
	public function menu()
	{
		$this -> sky -> load_page ('system/menu_index');
	}
	public function menu_assign()
	{
		$this->load->model('User_model');
		$this->load->model('Menu_model');

		if($this->input->post()){
			//var_dump($this->input->post());
			$this->Menu_model->delete_hide_group($this->input->post('user_group_list'));
			$this->Menu_model->apply_hide($this->input->post('user_group_list'));
			$this->Menu_model->delete_array_hide_menu($this->input->post('user_group_list'),$this->input->post('menu'));

		}

		$data['group_menu_hide'] = $this->Menu_model->get_menu_hide();
		$data['groups'] = $this->User_model->get_user_groups();
		$data['menus'] = $this->Menu_model->get_menu_active();
		$data['parent_menu'] = $this->Menu_model->get_menu_child(0);
		$this -> sky -> load_page ('system/menu_assign',$data);
	}

	public function ajax_menu_tree($menu_id = null)
	{
		$this->load->model('menu_model');
		$menu = $this -> menu_model -> get_menu_tree($menu_id);
		header('Content-Type: application/json');
		echo json_encode($menu);
	}

	public function ajax_menu_detail($menu_id = null)
	{
		$this->load->model('menu_model');

		// update data part
		if($_POST){
			$menu_id = $this->input->post('menu_id');
			if($this -> menu_model -> update_menu($menu_id, $this->input->post())){
				$this->session->set_flashdata('flash_notification', 'Menu updated');
			} else {
				$this->session->set_flashdata('flash_notification', 'Unable to update menu');
			}
		}

		$data['menu'] = $this -> menu_model -> get_menu($menu_id);
		$data['menu_parents'] = $this -> menu_model -> get_parents();

		$this->load->view('setting/system/menu_detail', $data);
	}



	/*-----  End of Menu Management setting  ------*/




	public function general(){
		$this->sky->load_page('general');
	}

	/**
	 * Document Numbering
	 */
	public function doc_number(){
		$data['documents']			= $this->document_model->get_all_docs();
		$this->sky->load_page('doc_number',$data);
	}
	public function doc_number_add()
	{
		if($this->input->post()){
			$this->document_model->createDocSeries(
				$this->input->post('doc_code'),
				$this->input->post('doc_name'),
				$this->input->post('doc_prefix'),
				$this->input->post('doc_suffix'),
				$this->input->post('num_next'),
				$this->input->post('num_last')
			);
			header('doc_number','refresh');
		}
		$this->sky->load_page('doc_number_add');
	}


	public function user(){
		$this->sky->load_page('user');
	}
	public function auth(){
		$this->sky->load_page('auth');
	}


	/**
	 * Alert
	 */
	public function alert(){
		$this->sky->load_page('alert');
	}
	public function master_fin_cash_account(){
		$this->sky->load_page('master/fin/cash_account');
	}
	public function master_fin_link_account(){
		$this->load->model('Link_account_model');
		$data['list_account'] = $this->account_model->view(array('is_postable'=>1));
		$data['links'] = $this->Link_account_model->list_account();
		$this->sky->load_page('master/fin/link_account',$data);
	}

	/**
	 * Currency
	 */
	public function master_fin_curr(){
		$data['list_currency'] 		= $this->currency_model->get();
		$this->sky->load_page('master/fin/curr',$data);
	}
	public function master_fin_curr_add()
	{
		if($this->input->post()){
			$this->db->trans_begin();
			$this->currency_model->currency_add($this->input->post());
			$this->sky->trans_end();
		}
		$this->sky->load_page('master/fin/curr_add');
	}
	public function master_fin_curr_edit($code)
	{
		if($this->input->post()){
			$this->db->trans_begin();
			$this->currency_model->currency_edit($code,$this->input->post());
			$this->sky->trans_end();
			return false;
		}
		$data['form_header'] 	= $this->currency_model->get($code);
		$this->sky->load_page('master/fin/curr_edit',$data);
	}


	public function master_fin_curr_rate(){
		$this->sky->load_page('master/fin/curr_rate');
	}
	public function master_fin_trans_code(){
		$this->sky->load_page('master/fin/trans_code');
	}
	public function master_fin_cash_flow(){
		$this->sky->load_page('master/fin/cash_flow');
	}
	public function master_fin_payment_term(){
		$this->sky->load_page('master/fin/payment_term');
	}
	public function master_tax_group(){
		$this->sky->load_page('master/tax/tax_group');
	}
	public function master_tax_code(){
		$this->sky->load_page('master/tax/tax_code');
	}
	public function master_tax_withholding(){
		$this->sky->load_page('master/tax/withholding_tax');
	}
	public function master_pur_landcost(){
		$this->sky->load_page('master/pur/landcost');
	}
	public function master_buss_cust_group(){
		$this->sky->load_page('master/buss/cust_group');
	}
	public function master_buss_vend_group(){
		$this->sky->load_page('master/buss/vend_group');
	}
	public function master_buss_cust(){
		$this->sky->load_page('master/buss/vend_group');
	}
	public function master_buss_vend(){
		$this->sky->load_page('master/buss/vend');
	}
	public function master_bank(){
		$this->sky->load_page('master/bank/bank');
	}
	public function master_bank_account(){
		$this->sky->load_page('master/bank/bank_account');
	}
	public function master_item_group(){
		$this->sky->load_page('master/item/item_group');
	}
	public function master_item(){
		$this->sky->load_page('master/item/item');
	}
	public function master_wh(){
		$this->sky->load_page('master/item/wh');
	}
	public function master_uom(){
		$this->sky->load_page('master/item/uom');
	}
	public function master_manuf(){
		$this->sky->load_page('master/item/manuf');
	}
	public function master_brand(){
		$this->sky->load_page('master/item/brand');
	}

	// SHIFT SETTINGS
	// KASYFUNNUR
	public function shift($action = 'view')
	{
		$this->load->model('shift_model', 'model');
		$data['shift'] = $this->model->ambil();

		$this->sky->load_page('shift/shift', $data);
	}

	public function addShift()
	{
		$this->sky->load_page('shift/addShift');
	}

	public function editShift($id = null)
	{
		$data['id'] = $id;
		$this->load->model('shift_model', 'model');
		$data['e'] = $this->model->ambil($id);

		$this->sky->load_page("shift/editShift", $data);
	}

	public function updateShift($id = null)
	{
		$this->load->model('shift_model', 'model');
		$data = array(
			'shiftCode' => $this->input->post('shiftCode'),
			'startTime' => $this->input->post('startTime'),
			'endTime' => $this->input->post('endTime'),
			'breakStart' => $this->input->post('breakStart'),
			'breakEnd' => $this->input->post('breakEnd')
		);
		$cek = $this->model->update($id, $data);

		if($cek == TRUE)
		{
			$data = $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                Data shift berhasil diupdate.
              </div>');
			redirect('setting/shift', $data);
		}
		else
		{
			$error = $this->db->error(); // Has keys 'code' and 'message'
		}
	}

	public function store()
	{
		$this->load->model('shift_model', 'model');
		$data = array(
			'shiftCode' => $this->input->post('shiftCode'),
			'startTime' => $this->input->post('startTime'),
			'endTime' => $this->input->post('endTime'),
			'breakStart' => $this->input->post('breakStart'),
			'breakEnd' => $this->input->post('breakEnd')
		);
		$cek = $this->model->store($data);

		if ($cek == TRUE)
		{
			$data = $this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                Data shift berhasil ditambah.
              </div>');
			redirect('setting/shift', $data);
		}
		else
		{
			$error = $this->db->error(); // Has keys 'code' and 'message'
		}
	}

	// EMPLOYEE
	public function employee()
	{
		$this->load->model('employee_model', 'empData');
		$data['employee'] = $this->empData->ambil();
		$this->sky->load_page("employee/employee", $data);
	}

	public function showEmployee($id = NULL)
	{
		$this->load->model('employee_model', 'empData');
		$data['em'] = $this->empData->ambilId($id);
		$data['es'] = $this->empData->ambilkId($id);

		$this->sky->load_page("employee/showEmployee", $data);
	}
	// END KASYFUNNUR
}
