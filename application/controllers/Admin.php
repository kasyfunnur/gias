<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {	
	
	public function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this -> load -> model ('admin_model');
		
		}
	
	public function location($action = 'view'){
		$this -> sky -> load_page ('location');
		}
	
	public function ajax_location_list($return="json"){
		if  ($return == 'json') echo json_encode($this -> admin_model -> ajax_location_list());
		}
		
	public function add_location(){	
		$msg = ($_POST) ? $this -> admin_model -> add_location($_POST) : NULL;	
		$this -> sky -> load_page ('add_location',NULL,(isset($msg) ? $msg : ""));
		}
	
	//users 
	public function user($action = 'view'){
		$this -> sky -> load_page ('user');
		}
	
	public function ajax_user_list($return="json"){
		if  ($return == 'json') echo json_encode($this -> admin_model -> ajax_user_list());
		}
		
	public function add_user(){
		$this -> load -> model ('asset_model');
		$this -> load -> model ('login_lib');
		
		// position
		$this -> load -> helper ('position');
		
		$data['group_array'] 	= $this -> admin_model -> get_user_group();
		$data['site_location'] 	= $this -> asset_model -> get_site_location();		
		if($_POST  && $this->form_validation -> run()){
			$clean_post = $this -> input -> post(NULL, TRUE); // clean xss
			$msg = $this -> admin_model -> add_user($clean_post);
			$this -> form_validation -> reset_post_data();
			}
		
		$this -> sky -> load_page ('add_user',$data, (isset($msg) ? $msg : ""));
		}
		
	public function check_username($username){
		$this->form_validation->set_message('check_username', "%s has been registered.");
    	return ($this -> login_lib -> check_username ($username))?FALSE:TRUE;
	}	
	
	//user group
	public function user_group($action = 'view'){
		$this -> sky -> load_page ('user_group');
		}
	
	public function ajax_user_group_list($return="json"){
		if  ($return == 'json') echo json_encode($this -> admin_model -> ajax_user_group_list());
		}
		
	public function add_user_group(){	
		$msg = ($_POST) ? $this -> admin_model -> add_user_group($_POST) : NULL;	
		$this -> sky -> load_page ('add_user_group',NULL, (isset($msg) ? $msg : ""));
		}
		
	public function group_permission($action = 'view'){
		$this -> sky -> load_page ('empty');
		}
		
	public function generate_random_data(){
		
		}	
		
		
	//position
	public function position($action = 'view'){
		$this -> sky -> load_page ('position');
		}
		
	public function ajax_position_list($return="json"){
		$this->load->model ('position_model');
		if  ($return == 'json') echo json_encode($this -> position_model -> ajax_position_list());
		}
		
	public function add_position(){
		$this -> load -> model ('position_model');
		$this -> load -> helper ('position');
		
		if($_POST  ){ // && $this->form_validation -> run()
			$clean_post = $this -> input -> post(NULL, TRUE); // clean xss
			$msg = $this -> position_model -> add_position($clean_post);
			// $this -> form_validation -> reset_post_data();
			}
		
		$this -> sky -> load_page ('add_position', NULL, (isset($msg) ? $msg : ""));
		}
	
	// validate date
	function is_valid_date($date)
	{
		$this->form_validation->set_message('is_valid_date', "Please enter a valid date.");
		
		$test_arr  = explode('/', $date);
		if (checkdate($test_arr[0], $test_arr[1], $test_arr[2])) {
			return true;
		} else
			return false;
		/* if(preg_match("/^(\d{2})-(\d{2})-(\d{4})$/", $date, $matches))
		{
	
			if(checkdate($matches[2], $matches[1], $matches[3]))
			{
				return true;
			} else return false;
		} else return false; */
	}
}
