<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sales extends CI_Controller {	
	public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this->load->helper('url');
		$this->load->helper('swlayout');
		$this->load->helper('approval');
		$this->load->model('location_model');
		
		$this->load->model('sales_order_model');
		$this->load->model('sales_delivery_model');
		$this->load->model('sales_invoice_model');
		$this->load->model('sales_return_model');
		$this->load->model('document_model');
		$this->load->model('inventory_model');
		$this->load->model('asset_model');
		$this->load->model('business_model');
		$this->load->model('status_model');
		$this->load->model('journal_model');
		$this->load->model('approval_model');
		$this->load->model('expense_model');
		$this->load->model('tax_model');
	}
	
	public function index($page="sales/sales_order_list"){
		$this->load_page($page);
	}

	public function sales_order(){				
		$data['datatable'] = $this -> sales_order_model -> getAll();
		$this->sky->load_page("sales_order_index",$data);
	}
	public function sales_order_add(){
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['ddl_tax']			= $this->tax_model->get_all();
		$data['doc_num'] 			= $this->document_model->getNextSeries('SAL_ORD');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$data['dialog_customer']	= $this->business_model->getCustomer();
		$data['ta_inventory']		= $this->inventory_model->ta_get();
		$this->sky->load_page('sales_order_add',$data);
	}
	public function sales_order_edit($code){
		$data['form_header'] 		= $this->sales_order_model->getHeader($code);
		$data['form_detail'] 		= $this->sales_order_model->getDetail($code);
		$data['ddl_whse'] 			= $this->location_model->get_site_location();
		$data['doc_num'] 			= $this->document_model->getNextSeries('SAL_ORD');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$data['dialog_customer'] 	= $this->business_model->getCustomer();
		$this->sky->load_page('sales_order_edit',$data);
	}
	public function sales_order_cancel($code){		
		$data['ddl_whse'] 			= $this->location_model->get_site_location();
		$data['form_header'] 		= $this->sales_order_model->getHeader($code);
		$data['form_detail'] 		= $this->sales_order_model->getDetail($code);
		$this->sky->load_page('sales_order_cancel',$data);
	}
	public function sales_order_close($code){		
		$data['ddl_whse'] 			= $this->location_model->get_site_location();
		$data['form_header'] 		= $this->sales_order_model->getHeader($code);
		$data['form_detail'] 		= $this->sales_order_model->getDetail($code);
		$this->sky->load_page('sales_order_close',$data);
	}
	
	public function sales_order_cancel_query($code){		
		//1.INIT
		//2.POST VAR & OTHER VAR
		//3.VALIDATION		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		$data_header = array(
			"doc_status" 	=> "CANCELLED",
			"cancel_by"		=> $this->session->userdata("username"),
			"cancel_dt"		=> date('Y-m-d H:m:s'),
			"cancel_reason" => $this->input->post('reason')
		);
		$where = array();
		$where[0] = array(
			"field"	=> 'doc_num',
			"value" => $code
		);
		$this->sales_order_model->update_header($data_header,$where);
		//4.3.END TRANS	
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			return false;
		}else{
			$this->db->trans_commit();
			//$this->document_model->useDocSeries('PUR_ORD');
			echo "1"; return true;
		}
	}
	public function sales_order_close_query($code){		
		//1.INIT
		//2.POST VAR & OTHER VAR
		//3.VALIDATION		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		$data_header = array(
			"doc_status" 	=> "CLOSED",
			"cancel_by"		=> $this->session->userdata("username"),
			"cancel_dt"		=> date('Y-m-d H:m:s'),
			"cancel_reason" => $this->input->post('reason')
		);
		$where = array();
		$where[0] = array(
			"field"	=> 'doc_num',
			"value" => $code
		);
		$this->sales_order_model->update_header($data_header,$where);
		//4.3.END TRANS	
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			return false;
		}else{
			$this->db->trans_commit();
			//$this->document_model->useDocSeries('PUR_ORD');
			echo "1"; return true;
		}
	}
	public function sales_order_add_query(){		
		//1.INIT
		//2.POST VAR & OTHER VAR
		$this->business_model->table = 't_business';
		if($this->input->post('buss_id') == ""){
			//$buss_id_temp = $this->business_model->read('buss_code','OTC');			
			//$buss_id = $buss_id_temp['buss_id'];
			$buss_id = $this->business_model->insert_business($this->input->post('buss_name'),"CUST");
			$update_buss = array(
				"buss_tlp1"=>$this->input->post('cust_phone1'),
				"buss_tlp2"=>$this->input->post('cust_phone2'),
				"buss_addr"=>$_POST['ship_addr']
			);
			$this->db->update('t_business',$update_buss,"buss_id = $buss_id");
		}else{
			$buss_id = $this->input->post('buss_id');
		}
		
		//to be continued..
		$document_number = $this->document_model->getNextSeries('SAL_ORD');
		$data_header = array(
			//"doc_id" 			=> $this->db->insert_id(),//$temp_doc_id,
			"doc_num" 			=> $document_number, //SAL-0004
			"doc_dt" 			=> $this->input->post('doc_dt'),
			"doc_ddt" 			=> $this->input->post('doc_ddt'),
			"doc_status" 		=> "ACTIVE",
			"buss_id" 			=> $buss_id,//$this->input->post('buss_id'),//$buss[0)['buss_id'),
			"ship_addr" 		=> $this->input->post('ship_addr'),
			"doc_buss_tlp1"		=>	$this->input->post('cust_phone1'),
			"doc_buss_tlp2"		=>	$this->input->post('cust_phone2'),
			//"bill_addr" 		=> $this->input->post('bill_addr'),
			"whse_id" 			=> $this->input->post('location_id'),
			"doc_type" 			=> "ITEM",
			//"doc_ref" 		=> $this->input->post('doc_ref'),
			"doc_curr" 			=> "IDR",
			"doc_rate" 			=> 1,
			"doc_subtotal" 		=> $this->input->post('doc_subtotal'),
			"doc_disc_percent"	=> $this->input->post('doc_disc_pct'),
			"doc_disc_amount"	=> $this->input->post('doc_disc'),
			"doc_total" 		=> $this->input->post('doc_total'),
			"doc_note" 			=> $this->input->post('doc_note'),
			"create_by"			=> $this->session->userdata("username"),
			"create_dt"			=> date('Y-m-d H:m:s')
		);		
		//3.VALIDATION
		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		//4.2.RUN TRANS
		$temp_temp_id = $this->sales_order_model->insert_header($data_header);
		$temp_id = $temp_temp_id;
		for($x=0;$x<=$this->input->post('table_row')-2;$x++){ 			
			if(!empty($_POST['item_id'][$x])){
				$y = $x+1;
				$temp_qty = $_POST['ast_qty'][$x];//$this->input->post('ast_qty'.$y);
				$temp_price = $_POST['ast_price'][$x];//$this->input->post('ast_price'.$y);
				$temp_total = $temp_qty*$temp_price;
				
				$temp_info = $this->inventory_model->get_asset_info($_POST['item_id'][$x]);
				$data_detail = array(
					"doc_id" 		=> $temp_id,
					"doc_line" 		=> $_POST['row'][$x],//$x+1,
					"doc_line_type" => 'ITEM',
					"item_group" 	=> $temp_info['item_g_id'],//$this->input->post('ast_group'.$y),
					"item_id" 		=> $_POST['item_id'][$x],
					"item_name" 	=> $_POST['ast_name'][$x],//$this->input->post('ast_name'.$y),
					"item_qty" 		=> $temp_qty,
					"item_price" 	=> $temp_price,
					"item_netprice" => $temp_price,
					"item_cost" 	=> $temp_price,
					"item_total" 	=> $temp_total
				);
				$this->sales_order_model->insert_detail($data_detail);
			}
		}
		
		$approval_status = request_approval($this->session->userdata('user_id'), 'SAL_ORD', $document_number);	
		$this -> db -> update ('t_sales_order_header', array("approval_status" => $approval_status), array ('doc_num' => $document_number));

		//4.3.END TRANS	
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			return false;
		}else{
			$this->db->trans_commit();
			$this->document_model->useDocSeries('SAL_ORD');
			echo "1"; return true;
		}
	}
	
	public function sales_order_edit_query(){
		//1.INIT
		//2.POST VAR & OTHER VAR
		$data_header = array(
			"doc_dt" 			=> $this->input->post('doc_dt'),
			"doc_ddt" 			=> $this->input->post('doc_ddt'),
			"doc_status" 		=> "ACTIVE",
			"buss_id" 			=> $this->input->post('buss_id'),//$buss[0)['buss_id'),
			"ship_addr" 		=> $_POST['ship_addr'],
			"doc_buss_tlp1"		=> $this->input->post('cust_phone1'),
			"doc_buss_tlp2"		=> $this->input->post('cust_phone2'),
			//"bill_addr" 		=> $this->input->post('bill_addr'),
			"whse_id" 			=> $this->input->post('location_id'),
			"doc_type" 			=> "ITEM",
			//"doc_ref" 		=> $this->input->post('doc_ref'),
			"doc_curr" 			=> "IDR",
			"doc_rate" 			=> 1,
			"doc_subtotal" 		=> $this->input->post('doc_subtotal'),
			"doc_disc_percent"	=> $this->input->post('doc_disc_pct'),
			"doc_disc_amount"	=> $this->input->post('doc_disc'),
			"doc_total" 		=> $this->input->post('doc_total'),
			"doc_note" 			=> $this->input->post('doc_note'),
			"update_by"			=> $this->session->userdata("username"),
			"update_dt"			=> date('Y-m-d H:m:s')
		);		
		//3.VALIDATION
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		//4.2.RUN TRANS
		//echo $code;
		$where = array();
		$where[0] = array(
			"field"	=> 'doc_num',
			"value" => $this->input->post('doc_num')
		);
		$this->sales_order_model->update_header($data_header,$where);
		$temp_id = $this->input->post('doc_id');
		
		//delete detail
		$this->sales_order_model ->delete_detail($this->input->post('doc_id'));
		//insert detail
		for($x=0;$x<=$this->input->post('table_row')-2;$x++){ 	
			if(!empty($_POST['item_id'][$x])){
				$y = $x+1;			
				$temp_qty = $_POST['ast_qty'][$x];//$this->input->post('ast_qty'.$y);
				$temp_price = $_POST['ast_price'][$x];//$this->input->post('ast_price'.$y);
				$temp_total = $temp_qty*$temp_price;
							
				for($z=1;$z<=$temp_qty;$z++){
					$data_asset = array(				
					'name' 		=> $this->input->post('ast_name'.$y),
					'group_id' 	=> $this->input->post('ast_group'.$y),
					'location_id' => 1,
					'value'		=> 0,
					"currency" 	=> 'IDR',
					"status" 	=> 'incoming',
					//"document_origin" => $document_number,
					"document_line"   => $y	
					);
				} 
				
				$temp_info = $this->inventory_model->get_asset_info($_POST['item_id'][$x]);
				$data_detail = array(
					"doc_id" 		=> $temp_id,
					"doc_line" 		=> $_POST['row'][$x],//$x+1,
					"doc_line_type" => 'ITEM',
					"item_group" 	=> $temp_info['item_g_id'],//$this->input->post('ast_group'.$y),
					"item_id" 		=> $_POST['item_id'][$x],
					"item_name" 	=> $_POST['ast_name'][$x],//$this->input->post('ast_name'.$y),
					"item_qty" 		=> $temp_qty,
					"item_price" 	=> $temp_price,
					"item_netprice" => $temp_price,
					"item_cost" 	=> $temp_price,
					"item_total" 	=> $temp_total
				);
				$this->sales_order_model->insert_detail($data_detail);
			}
		}
		//4.3.END TRANS	
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			echo $this->db->_error_message();
			return false;
		}else{
			echo "1"; return true;
		}
	}
	
	public function sales_delivery(){
		$data['datatable'] = $this-> sales_delivery_model -> getAll();
		$this->sky->load_page('sales_delivery_index',$data,NULL);
	}
	public function sales_delivery_add($code=NULL){
		/*$data['ddl_whse'] 			= $this->location_model->get_site_location();
		$data['ddl_asset_group'] 	= $this->asset_model->get_asset_group();
		//$data['src_location'] 	= $this->location_model->get_site_location();
		$data['doc_num'] 			= $this->document_model->getNextSeries('PUR_RCV');
		$this->sky->load_page('purchase_receipt_add',$data);
		*/
		$data['param'] = $code;
		if($code != NULL){			
			$data['warning'] 			= $this->sales_order_model->getOverDue($code,0);
		}
		$data['ddl_whse'] 				= $this->location_model->get_site_by_type('warehouse');
		$data['ddl_freight']			= $this->sales_order_model->getFreight();
		if(!$data['ddl_freight']){
			$data['ddl_freight'] 		= array(array("freight_code"=> "", "doc_freight"=>""));
		}
		$data['doc_num'] 				= $this->document_model->getNextSeries('SAL_DEL');
		//$data['dialog_po'] 			= $this->purchase_order_model->getAll_with_vendor(NULL);
		$data['dialog_so'] 				= "";//$this->purchase_order_model->getAll_with_vendor(NULL);
		$data['dialog_customer'] 		= $this->business_model->getCustomer();
		$this->sky->load_page('sales_delivery_add',$data);
	}
	public function sales_delivery_edit($code){
		$data['form_header'] 		= $this->sales_delivery_model->getHeader($code);
		$data['form_detail'] 		= $this->sales_delivery_model->getDetail($code);
		$data['ddl_whse'] 			= $this->location_model->get_site_location();
		$data['ddl_freight']		= $this->sales_order_model->getFreight();
		$data['doc_num'] 			= $this->document_model->getNextSeries('SAL_DEL');
		$data['dialog_customer'] 	= $this->business_model->getCustomer();
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$this->sky->load_page('sales_delivery_edit',$data);
	}
	public function tes_expense(){		
		$this->expense_model->add('DEL12345','Desc ajalah','IDR',1234000);
	}
	public function sales_delivery_add_query(){		
		//1.INIT
		//2.POST VAR & OTHER VAR			
		//3.VALIDATION		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this->db->trans_begin();	
		//4.2.RUN TRANS		
		//rcv header
		$curr_doc_num = $this->document_model->getNextSeries('SAL_DEL');
		$temp_doc_freight = ($_POST['doc_freight'] == 0) ? $_POST['doc_freight_new'] : $_POST['doc_freight'];
		$data_header = array(
			"doc_num" 			=> $curr_doc_num,
			"doc_dt" 			=> $this->input->post('doc_dt'),
			"doc_ddt" 			=> $this->input->post('doc_ddt'),
			"doc_status" 		=> "ACTIVE",
			"buss_id" 			=> $this->input->post('buss_id'),//$buss[0)['buss_id'),
			"ship_addr" 		=> $this->input->post('ship_addr'),
			"doc_buss_tlp1"		=> $this->input->post('cust_phone1'),
			"doc_buss_tlp2"		=> $this->input->post('cust_phone2'),
			//"bill_addr" 		=> $this->input->post('bill_addr'),
			"whse_id" 			=> $this->input->post('location_id'),
			"doc_type" 			=> "ITEM",
			"doc_ref" 			=> $this->input->post('doc_ref'),
			"doc_ext_ref"	 	=> $this->input->post('doc_ext_ref'),
			"doc_freight"		=> $temp_doc_freight,
			"doc_cost_freight" 	=> $this->input->post('exp_freight'),
			"doc_cost_other" 	=> $this->input->post('exp_other'),
			"doc_cost_freight_type" 	=> $this->input->post('freight_type'),
			"doc_cost_other_type"		=> $this->input->post('other_type'),
			"doc_cost_freight_party"	=> $this->input->post('freight_accrue_party'),
			"doc_cost_other_party"		=> $this->input->post('other_accrue_party'),
			"doc_curr" 			=> "IDR",
			"doc_rate" 			=> 1,
			"doc_subtotal" 		=> $this->input->post('doc_subtotal'),
			"doc_disc_percent"	=> $this->input->post('doc_disc_pct'),
			"doc_disc_amount"	=> $this->input->post('doc_disc'),
			"doc_total" 		=> $this->input->post('doc_total'),
			"doc_note" 			=> $this->input->post('doc_note'),
			"create_by"			=> $this->session->userdata("username"),
			"create_dt"			=> date('Y-m-d H:m:s')
		);	
		$exp_freight 			= $this->input->post('exp_freight');
		$exp_other 				= $this->input->post('exp_other');
		$freight_accrue_party 	= $this->input->post('freight_accrue_party');
		$other_accrue_party		= $this->input->post('other_accrue_party');
		$freight_type 			= $this->input->post('freight_type');
		$other_type 			= $this->input->post('other_type');
		//expense
		if($freight_type == "cust_accrue"){
			
		}
		
		if($exp_freight != 0){
			if($freight_type == "cust_accrue"){
				$this->expense_model->add($curr_doc_num,'Freight of '.$curr_doc_num,'IDR',$exp_freight);
			}else{
				$this->expense_model->add($curr_doc_num,'Freight of '.$curr_doc_num,'IDR',$exp_freight,$freight_accrue_party);
			}
		}
		if($exp_other != 0){
			if($other_type == "cust_accrue"){
				$this->expense_model->add($curr_doc_num,'Expense of '.$curr_doc_num,'IDR',$exp_other);
			}else{
				$this->expense_model->add($curr_doc_num,'Expense of '.$curr_doc_num,'IDR',$exp_other,$other_accrue_party);
			}
		}
		
		$temp_temp_id = $this->sales_delivery_model->insert_header($data_header);
		
		$temp_id = $temp_temp_id;
		for($x=0;$x<=$this->input->post('table_row')-2;$x++){ 			
			$y = $x+1;
			$temp_qty   = $_POST['ast_qty'][$x];
			$temp_price = $_POST['ast_price'][$x];//$this->input->post('ast_price'.$y);
			$temp_total = $temp_qty*$temp_price;
			//rcv detail
			$data_detail = array(
				"doc_id" 		=> $temp_id,
				"doc_line" 		=> $x+1,
				"doc_line_type" => 'ITEM',
				"item_group" 	=> '',//$this->input->post('ast_group'.$y),
				"item_id" 		=> $_POST['item_id'][$x],
				"item_name" 	=> $_POST['ast_name'][$x],//$this->input->post('ast_name'.$y),
				"item_qty" 		=> $temp_qty,
				"item_price" 	=> $temp_price,
				"item_netprice" => $temp_price,
				"item_cost" 	=> $temp_price,
				"item_total" 	=> $temp_total,
				"reff_type"		=> "SOR",
				"reff_id"		=> $this->input->post('doc_ref_id'),
				"reff_line"		=> $_POST['reff_line'][$x]
			);
			$this->sales_delivery_model->insert_detail($data_detail);
			
			//item transaction
			$data_transaction = array(
				"doc_type"		=> 'DEL',
				"doc_num"		=> $curr_doc_num,
				"doc_date"		=> $this->input->post('doc_dt'),
				"item_id"		=> $_POST['item_id'][$x],
				"item_qty" 		=> -$temp_qty,
				"item_value"	=> $temp_price,
				"total_value"	=> $temp_total,
				"whse_id"		=> $this->input->post('location_id')
			);
			$this->inventory_model->table="t_inventory_transaction";
			$this->inventory_model->create($data_transaction);
			
			
			$data_so_detail = array(
				"item_qty_closed" 	=> ($_POST['ast_so_qty'][$x] - $_POST['ast_qty_open'][$x]) + $temp_qty
			);			
			$field = array('doc_id','item_id');
			$value = array($this->input->post('doc_ref_id'),$_POST['item_id'][$x]);
			$this->sales_order_model->table = 't_sales_order_detail';
			$this->sales_order_model->update($field,$value,$data_so_detail);
		}
		
		$this->sales_order_model->set_item_close($this->input->post('doc_ref'));
		$this->sales_order_model->set_doc_close($this->input->post('doc_ref'));

		//journal header
		$curr_jur_num = $this->document_model->getNextSeries('JRNL');
		$data_journal_header = array(
			"journal_code"		=> $curr_jur_num,
			"journal_date"		=> $this->input->post('doc_dt'),
			"journal_memo"		=> $this->input->post('doc_note'),
			"journal_type"		=> 'DEL',
			"journal_ref1"		=> $curr_doc_num,
			"create_by"			=> $this->session->userdata("username"),
			"create_dt"			=> date('Y-m-d H:m:s')
		);
		$this->journal_model->table = 't_account_journal_header';
		$jrnl_temp_id = $this->journal_model->create($data_journal_header);
		
		//journal detail
		$data_journal_detail_dr = array(
			"journal_id"		=> $jrnl_temp_id,
			"journal_line"		=> 1,
			"account_id"		=> '11230', //must create link account
			"journal_dr"		=> $temp_total,
			"journal_cr"		=> 0,
			"journal_cc"		=> 0,
			"journal_curr"		=> 'IDR'
		);
		$data_journal_detail_cr = array(
			"journal_id"		=> $jrnl_temp_id,
			"journal_line"		=> 2,
			"account_id"		=> '21101', //must create link account
			"journal_dr"		=> 0,
			"journal_cr"		=> $temp_total,
			"journal_cc"		=> 0,
			"journal_curr"		=> 'IDR'
		);
		$this->journal_model->table = 't_account_journal_detail';
		$this->journal_model->create($data_journal_detail_dr);
		$this->journal_model->create($data_journal_detail_cr);
		
		$this -> db -> query("call procUpdateQtyOnHand()");
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			return false;
		}else{
			$this->db->trans_commit();
			$this->document_model->useDocSeries('SAL_DEL');
			$this->document_model->useDocSeries('JRNL');
			echo "1"; return true;
		}
	}
	public function sales_delivery_edit_query($code){
		//1.INIT
		//2.POST VAR & OTHER VAR			
		//3.VALIDATION		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this->db->trans_begin();	
		//4.2.RUN TRANS		
		//echo "freight:".$_POST['doc_freight']."<hr>"; 
		//echo "freight2:".$_POST['doc_freight_new']."<hr>";
		//echo "freight3:".$_POST['doc_freight']."<hr>";
		if($_POST['doc_freight'] === 0){ 
			$temp_doc_freight = $_POST['doc_freight_new'];
		}else{ 
			$temp_doc_freight = $_POST['doc_freight'];
		}
		//echo $temp_doc_freight."temp";
		
		$data_header = array(			
			"ship_addr" 		=> $this->input->post('ship_addr'),
			"doc_buss_tlp1"		=> $this->input->post('cust_phone1'),
			"doc_buss_tlp2"		=> $this->input->post('cust_phone2'),
			"doc_ext_ref"		=> $this->input->post('doc_ext_ref'),
			"doc_freight"		=> $temp_doc_freight,
			"doc_cost_freight" 			=> $this->input->post('exp_freight'),
			"doc_cost_other" 			=> $this->input->post('exp_other'),
			"doc_cost_freight_type" 	=> $this->input->post('freight_type'),
			"doc_cost_other_type"		=> $this->input->post('other_type'),
			"doc_cost_freight_party"	=> $this->input->post('freight_accrue_party'),
			"doc_cost_other_party"		=> $this->input->post('other_accrue_party'),			
			"update_by"			=> $this->session->userdata("username"),
			"update_dt"			=> date('Y-m-d H:m:s')
		);	
		$where = array();
		$where[0] = array(
			"field"	=> 'doc_num',
			"value" => $code
		);		
		$this->sales_delivery_model->update_header($data_header,$where);
		
		$exp_freight 			= $this->input->post('exp_freight');
		$exp_other 				= $this->input->post('exp_other');
		$freight_accrue_party 	= $this->input->post('freight_accrue_party');
		$other_accrue_party		= $this->input->post('other_accrue_party');
		$freight_type 			= $this->input->post('freight_type');
		$other_type 			= $this->input->post('other_type');
		//expense
		if($freight_type == "cust_accrue"){
			
		}
		
		/*if($exp_freight != 0){
			if($freight_type == "cust_accrue"){
				$this->expense_model->add($curr_doc_num,'Freight of '.$curr_doc_num,'IDR',$exp_freight);
			}else{
				$this->expense_model->add($curr_doc_num,'Freight of '.$curr_doc_num,'IDR',$exp_freight,$freight_accrue_party);
			}
		}
		if($exp_other != 0){
			if($other_type == "cust_accrue"){
				$this->expense_model->add($curr_doc_num,'Expense of '.$curr_doc_num,'IDR',$exp_other);
			}else{
				$this->expense_model->add($curr_doc_num,'Expense of '.$curr_doc_num,'IDR',$exp_other,$other_accrue_party);
			}
		}*/
		
		$this -> db -> query("call procUpdateQtyOnHand()");
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			return false;
		}else{
			$this->db->trans_commit();
			//$this->document_model->useDocSeries('SAL_DEL');
			//$this->document_model->useDocSeries('JRNL');
			echo "1"; return true;
		}
	}
	
	public function sales_delivery_cancel($code){		
		$data['form_header'] 		= $this->sales_delivery_model->getHeader($code);
		$data['form_detail'] 		= $this->sales_delivery_model->getDetail($code);
		$this->sky->load_page('sales_delivery_cancel',$data);
	}
	public function sales_delivery_close($code){		
		$data['form_header'] 		= $this->sales_delivery_model->getHeader($code);
		$data['form_detail'] 		= $this->sales_delivery_model->getDetail($code);
		$this->sky->load_page('sales_delivery_close',$data);
	}
	public function sales_delivery_cancel_query($code){		
		//1.INIT
		//2.POST VAR & OTHER VAR
		//3.VALIDATION		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		$data_header = array(
			"doc_status" 	=> "CANCELLED",
			"cancel_by"		=> $this->session->userdata("username"),
			"cancel_dt"		=> date('Y-m-d H:m:s'),
			"cancel_reason" => $this->input->post('reason')
		);
		$where = array();
		$where[0] = array(
			"field"	=> 'doc_num',
			"value" => $code
		);		
		$this->sales_delivery_model->update_header($data_header,$where);
		$temp_after = $this->sales_delivery_model->getHeader($code);
		$this->inventory_model->reverse_entry($code);
		
		//ANNE, request: DO Cancelation no need re-open SO
//		$temp_del_detail = $this->sales_delivery_model->getDetail($code);
//		foreach($temp_del_detail as $detail){
//			$so_detail = $this->sales_order_model->getDetail_line($temp_after[0]['doc_ref'],$detail['reff_line']);
//			$data_so_detail = array(
//				"item_qty_closed" 	=> $so_detail['item_qty_closed'] - $detail['item_qty']
//			);			
//			$field = array('doc_id','item_id');
//			$value = array($detail['reff_id'],$detail['item_id']);
//			$this->sales_order_model->table = 't_sales_order_detail';
//			$this->sales_order_model->update($field,$value,$data_so_detail);			
//		}		
//		$this->sales_order_model->set_item_close($temp_after[0]['doc_ref']);
//		$this->sales_order_model->set_doc_close($temp_after[0]['doc_ref']);
		
		
		/*$data_so_detail = array(
			"item_qty_closed" 	=> ($_POST['ast_so_qty'][$x] - $_POST['ast_qty_open'][$x]) + $temp_qty
		);			
		$field = array('doc_id','item_id');
		$value = array($this->input->post('doc_ref_id'),$_POST['item_id'][$x]);
		$this->sales_order_model->table = 't_sales_order_detail';
		$this->sales_order_model->update($field,$value,$data_so_detail);*/
		//4.3.END TRANS	
		$this->sky->trans_end();
	}
	public function test(){
		//$this->sales_order_model->set_item_close('SAL-0029');
		$this->sales_order_model->auto_doc_close();
	}
	public function sales_delivery_close_query($code){		
		//1.INIT
		//2.POST VAR & OTHER VAR
		//3.VALIDATION		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		$data_header = array(
			"doc_status" 	=> "CLOSED",
			"cancel_by"		=> $this->session->userdata("username"),
			"cancel_dt"		=> date('Y-m-d H:m:s'),
			"cancel_reason" => $this->input->post('reason')
		);
		$where = array();
		$where[0] = array(
			"field"	=> 'doc_num',
			"value" => $code
		);
		$this->sales_delivery_model->update_header($data_header,$where);
		//4.3.END TRANS	
		$this->sky->trans_end();
	}
	
	public function sales_invoice(){
		$data['datatable'] = $this-> sales_invoice_model -> getAll();
		$this->sky->load_page('sales_invoice_index',$data,NULL);
	}
	public function sales_invoice_add($code=NULL){
		$data['param'] = $code;
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['doc_num'] 			= $this->document_model->getNextSeries('SAL_INV');

		$data['dialog_so']			= "";
		$data['dialog_del']			= "";//$this->purchase_order_model->getAll_with_vendor(NULL);
		$data['dialog_customer'] 		= $this->business_model->getCustomer();
		$this->sky->load_page('sales_invoice_add',$data);
	}
	public function sales_invoice_edit($code){
		$data['form_header'] 		= $this->sales_invoice_model->getHeader($code);
		$data['form_detail'] 		= $this->sales_invoice_model->getDetail($code);
		$data['ddl_whse'] 			= $this->location_model->get_site_location();
		$data['doc_num'] 			= $this->document_model->getNextSeries('SAL_INV');
		$data['dialog_so']			= "";
		$data['dialog_del']			= "";
		$data['dialog_customer'] 	= $this->business_model->getCustomer();
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$this->sky->load_page('sales_invoice_edit',$data);
	}
	public function sales_invoice_dp_add($code=NULL){
		$data['param'] 				= $code;
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['doc_num'] 			= $this->document_model->getNextSeries('SAL_DP');
		$data['dialog_customer'] 		= $this->business_model->getCustomer();
		$this->sky->load_page('sales_invoice_dp_add',$data);
	}
	public function sales_invoice_add_query($type=NULL){		
		//1.INIT
		//2.POST VAR & OTHER VAR
		$doc_series = 'SAL_INV';
		if($type == "DP")$doc_series = 'SAL_DP';
		$document_number = $this->document_model->getNextSeries($doc_series);
		$data_header = array(
			//"doc_id" 			=> $this->db->insert_id(),//$temp_doc_id,
			"doc_num" 			=> $document_number,
			"doc_dt" 			=> $this->input->post('doc_dt'),
			"doc_ddt" 			=> $this->input->post('doc_ddt'),
			"doc_status" 		=> "ACTIVE",
			"buss_id" 			=> $this->input->post('buss_id'),//$buss[0)['buss_id'),
			"ship_addr" 		=> $this->input->post('ship_addr'),
			//"bill_addr" 		=> $this->input->post('bill_addr'),
			"whse_id" 			=> $this->input->post('location_id'),
			"doc_type" 			=> "ITEM",
			"doc_ref" 			=> $this->input->post('doc_ref'),
			"doc_curr" 			=> "IDR",
			"doc_rate" 			=> 1,
			"doc_subtotal" 		=> $this->input->post('doc_subtotal'),
			"doc_disc_percent"	=> $this->input->post('doc_disc_pct'),
			"doc_disc_amount"	=> $this->input->post('doc_disc'),
			"doc_cost_freight" 	=> $this->input->post('exp_freight'),
			"doc_cost_other" 	=> $this->input->post('exp_other'),
			"doc_total" 		=> $this->input->post('doc_total'),
			"doc_note" 			=> $this->input->post('doc_note'),
			"create_by"			=> $this->session->userdata("username"),
			"create_dt"			=> date('Y-m-d H:m:s')
		);		
		//3.VALIDATION
		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		//4.2.RUN TRANS
		$temp_temp_id = $this->sales_invoice_model->insert_header($data_header);
		$temp_id = $temp_temp_id;
		//echo 'table row '.$this->input->post('table_row').'<br>';
		for($x=0;$x<=$this->input->post('table_row')-2;$x++){ 			
			if(!empty($_POST['item_id'][$x])){
				$y = $x+1;
				//if(isset($_POST['row'.$y])){
				$temp_qty = $_POST['ast_qty'][$x];//$this->input->post('ast_qty'.$y);
				$temp_price = $_POST['ast_price'][$x];//$this->input->post('ast_price'.$y);
				$temp_total = $temp_qty*$temp_price;
							
				for($z=1;$z<=$temp_qty;$z++){
					$data_asset = array(				
					'name' 		=> $this->input->post('ast_name'.$y),
					'group_id' 	=> $this->input->post('ast_group'.$y),
					'location_id' => 1,
					'value'		=> 0,
					"currency" 	=> 'IDR',
					"status" 	=> 'incoming',
					"document_origin" => $document_number,
					"document_line"   => $y	
					);
					//$this->asset_model->add_new_asset($data_asset);			
				} 
				
				$temp_info = $this->inventory_model->get_asset_info($_POST['item_id'][$x]);
				$data_detail = array(
					"doc_id" 		=> $temp_id,
					"doc_line" 		=> $_POST['row'][$x],//$x+1,
					"doc_line_type" => 'ITEM',
					"item_group" 	=> $temp_info['item_g_id'],//$this->input->post('ast_group'.$y),
					"item_id" 		=> $_POST['item_id'][$x],
					"item_name" 	=> $_POST['ast_name'][$x],//$this->input->post('ast_name'.$y),
					"item_qty" 		=> $temp_qty,
					"item_price" 	=> $temp_price,
					"item_netprice" => $temp_price,
					"item_cost" 	=> $temp_price,
					"item_total" 	=> $temp_total,
					"reff_type"		=> "DEL",
					"reff_id"		=> $this->input->post('doc_ref_id'),
					"reff_line"		=> $_POST['reff_line'][$x]
				);
				$this->sales_invoice_model->insert_detail($data_detail);
				/*if($this->input->post('ast_group'.$y)!= '-1'){				
					$this->purchase_order_model->insert_detail($data_detail);
				}*/
				
				//so detail
				$line = 0;
				if($_POST['ast_qty_open'][$x] <= $temp_qty){
					$line = 1;
				}
				$data_del_detail = array(
					"item_qty_closed" 	=> ($_POST['ast_del_qty'][$x] - $_POST['ast_qty_open'][$x]) + $temp_qty,
					"line_close"		=> $line
				);			
				$field = array('doc_id','item_id');
				$value = array($this->input->post('doc_ref_id'),$_POST['item_id'][$x]);
				$this->sales_delivery_model->table = 't_sales_delivery_detail';
				$this->sales_delivery_model->update($field,$value,$data_del_detail);
			}
		}
		
		//billing status rcv
		/*$del_qty_close = $this->sales_delivery_model->get_qty_close($this->input->post('rcv_doc_ref'));
		$del_close_line = $this->sales_delivery_model->get_close_line($this->input->post('rcv_doc_ref'));
		$del_open_line = $this->sales_delivery_model->get_open_line($this->input->post('rcv_doc_ref'));
		$flag_run = FALSE;
		$billing_status = 'UNBILLED';
		if(count($del_qty_close)){
			$billing_status = 'PARTIAL';
			$flag_run = TRUE;
		}
		if(count($del_close_line)){
			$billing_status = 'PARTIAL';
			$flag_run = TRUE;
		}
		if(!count($del_open_line)){
			$billing_status = 'BILLED';
			$flag_run = TRUE;
		}
		if($flag_run){
			$data_del_header = array(
				"billing_status" => $billing_status
			);
			$this->sales_delivery_model->table = 't_sales_delivery_header';
			$this->sales_delivery_model->update('doc_id',$this->input->post('del_doc_ref_id'),$data_del_header);
		}*/		
		$this->sales_delivery_model->set_item_close($this->input->post('doc_ref'));
		$this->sales_delivery_model->set_doc_close($this->input->post('doc_ref'));
		
		/*$check_temp = $this->sales_invoice_model->getHeader($document_number);
		var_dump($check_temp);
		$this->db->trans_rollback();
		echo $this->db->_error_message();
		return false;*/
		
		//billing status po
		$so_del_status_pending = $this->sales_delivery_model->get_so_del_status_pending($this->input->post('so_doc_ref'));
		$so_del_status_done = $this->sales_delivery_model->get_so_del_status_done($this->input->post('so_doc_ref'));
		$flag_run = FALSE;
		$billing_status = 'UNBILLED';		
		if(count($so_del_status_pending)){ // any rcv with "partial" or "billed", mean at least "partially"
			$billing_status = 'PARTIAL';
			$flag_run = TRUE;
		}
		if(!count($so_del_status_done)){ // no rcv with "unbilldd" or "partial", mean all done
			$billing_status = 'BILLED';
			$flag_run = TRUE;
		}
		if($flag_run){
			$data_so_header = array(
				"billing_status" => $billing_status
			);
			$this->sales_order_model->table = 't_sales_order_header';
			$this->sales_order_model->update('doc_num',$this->input->post('so_doc_ref'),$data_so_header);
		}
		
		//journal header
		$curr_jur_num = $this->document_model->getNextSeries('JRNL');
		$data_journal_header = array(
			"journal_code"		=> $curr_jur_num,
			"journal_date"		=> $this->input->post('doc_dt'),
			"journal_memo"		=> $this->input->post('doc_note'),
			"journal_type"		=> 'SIV',
			"journal_ref1"		=> $document_number,
			"create_by"			=> $this->session->userdata("username"),
			"create_dt"			=> date('Y-m-d H:m:s')
		);
		$this->journal_model->table = 't_account_journal_header';
		$jrnl_temp_id = $this->journal_model->create($data_journal_header);
		
		//journal detail
		$data_journal_detail_dr = array(
			"journal_id"		=> $jrnl_temp_id,
			"journal_line"		=> 1,
			"account_id"		=> '11230', //must create link account
			"journal_dr"		=> $temp_total,
			"journal_cr"		=> 0,
			"journal_cc"		=> 0,
			"journal_curr"		=> 'IDR'
		);
		$data_journal_detail_cr = array(
			"journal_id"		=> $jrnl_temp_id,
			"journal_line"		=> 2,
			"account_id"		=> '21101', //must create link account
			"journal_dr"		=> 0,
			"journal_cr"		=> $temp_total,
			"journal_cc"		=> 0,
			"journal_curr"		=> 'IDR'
		);
		$this->journal_model->table = 't_account_journal_detail';
		$this->journal_model->create($data_journal_detail_dr);
		$this->journal_model->create($data_journal_detail_cr);
		
		//echo 's';
		//4.3.END TRANS	
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			return false;
		}else{
			$this->db->trans_commit();
			$this->document_model->useDocSeries('SAL_INV');
			$this->document_model->useDocSeries('JRNL');
			echo "1"; return true;
		}
	}
	
	public function sales_return(){
		$data['datatable'] = $this -> sales_return_model -> getAll();
		$this->sky->load_page("sales_return_index",$data);
	}
	public function sales_return_add(){
		$data['ddl_whse'] 			= $this->location_model->get_site_by_type('warehouse');
		$data['doc_num'] 			= $this->document_model->getNextSeries('SAL_ORD');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$data['dialog_vendor'] 		= $this->business_model->getCustomer();
		$this->sky->load_page('sales_return_add',$data);
	}
	
	
	//NOT USED
	public function sales_dialog_asset(){
		$data['assets'] = $this->asset_model->get_asset_all();
		$this->sky->load_page('sales_dialog_asset',$data,NULL,1);	
	}
	
	//PRINT
	public function print_so($code){				
		$data['title'] 				= 'Sales Order';
		$data['subtitle'] 			= $code;
		$data['form_header'] 		= $this->sales_order_model->getHeader($code);
		$data['form_detail'] 		= $this->sales_order_model->getDetail($code);
		$this->sky->load_page('print_sales_order',$data,NULL,1);	
	}
	public function print_del($code){
		$data['title']				= 'Surat Jalan';
		$data['subtitle'] 			= $code;
		$data['form_header'] 		= $this->sales_delivery_model->getHeader($code);
		$data['form_detail'] 		= $this->sales_delivery_model->getDetail($code);
		$this->sky->load_page('print_sales_delivery',$data,NULL,1);	
	}
	public function print_sinv($code){
		$data['title']				= 'Sales Invoice';
		$data['subtitle'] 			= $code;
		$data['form_header'] 		= $this->sales_invoice_model->getHeader($code);
		$data['form_detail'] 		= $this->sales_invoice_model->getDetail($code);
		$this->sky->load_page('print_sales_invoice',$data,NULL,1);	
	}
	
	
	//DIALOG
	public function dialog_sales_customer(){
		$data['customers'] = $this->business_model->getAll();
		$this->sky->load_page('dialog_sales_customer',$data,NULL,1);	
	}
	public function dialog_sales_order($filter="",$value=""){ // full/vendor
		if(($filter == 'customer') and ($value!="")){
			$data['docs'] = $this->sales_order_model->getAll_with_customer($value);			
		}else{
			$data['docs'] = $this->sales_order_model->getAll();
		}
		$data['docs_detail'] = $this->sales_order_model->getAll_detail();
		$this->sky->load_page('dialog_sales_order',$data,NULL,1);	
	}

	//AJAX
	public function ajax_getAll_so_where(){
		if(isset($_GET['buss_id']) && $_GET['buss_id'] != NULL){
			if(isset($_GET['where']) && $_GET['where'] != NULL){ 
				echo json_encode($this -> sales_order_model -> getAll_where($_GET['buss_id'],$_GET['where']));
			}else{ 
				echo json_encode($this -> sales_order_model -> getAll_where($_GET['buss_id']));
			}
		}
	}
	public function ajax_getAll_so_with_customer(){
		if(isset($_GET['buss_id']) && $_GET['buss_id'] != NULL){
			if(isset($_GET['logistic_status']) && $_GET['logistic_status'] != NULL){
				if(isset($_GET['billing_status']) && $_GET['billing_status'] != NULL){
					echo json_encode($this -> sales_order_model -> getAll_with_customer($_GET['buss_id'],$_GET['logistic_status'],$_GET['billing_status']));
				}else{
					echo json_encode($this -> sales_order_model -> getAll_with_customer($_GET['buss_id'],$_GET['logistic_status']));
				}				
			}else{
				echo json_encode($this -> sales_order_model -> getAll_with_customer($_GET['buss_id']));
			}			
		}
	}
	public function ajax_getDetail_so_open(){
		if(isset($_GET['doc_num']) && $_GET['doc_num'] != NULL){
			//echo json_encode($this -> sales_order_model -> getDetail($_GET['doc_num']));
			echo json_encode($this -> sales_order_model -> getDetail_so_open($_GET['doc_num']));
		}
	}

	public function ajax_getDetail_so(){
		if(isset($_GET['doc_num']) && $_GET['doc_num'] != NULL){
			//echo json_encode($this -> sales_order_model -> getDetail($_GET['doc_num']));
			echo json_encode($this -> sales_order_model -> getDetail($_GET['doc_num']));
		}
	}
	public function ajax_getHeader_so(){
		if(isset($_GET['doc_num']) && $_GET['doc_num'] != NULL){
			echo json_encode($this -> sales_order_model -> getHeader($_GET['doc_num']));
		}
	}
	
	public function ajax_getAll_del_with_customer(){
		if(isset($_GET['buss_id']) && $_GET['buss_id'] != NULL){
			echo json_encode($this -> sales_delivery_model -> getAll_with_customer($_GET['buss_id']));
		}
	}
	public function ajax_getDetail_del_open(){
		if(isset($_GET['doc_num']) && $_GET['doc_num'] != NULL){
			//echo json_encode($this -> sales_order_model -> getDetail($_GET['doc_num']));
			echo json_encode($this -> sales_delivery_model -> getDetail_del_open($_GET['doc_num']));
		}
	}
	public function ajax_getHeader_del(){
		if(isset($_GET['doc_num']) && $_GET['doc_num'] != NULL){
			echo json_encode($this -> sales_delivery_model -> getHeader($_GET['doc_num']));
		}
	}

	/*===================================
	=            POS Section            =
	===================================*/

	/**
	*
	* POS - load POS index add
	*
	**/
	public function pos()
	{
		$this->load->model('pos_model');
		$data['cashiers'] = $this->pos_model-> get_cashier_info();
		$this -> sky -> load_page('sales_pos_index',$data);
	}

	/**
	*
	* POS - load POS add form
	*
	**/
	public function pos_add($location_id, $cashier_id)
	{
		$this->load->model('pos_model');
		$data['cashier'] = $this -> pos_model -> get_cashier_info($cashier_id);
		$this -> sky -> load_page('sales_pos_add',$data);
	}

	/**
	*
	* pos_submit
	* Complete a POS transaction (submitted form)
	**/
	public function pos_submit()
	{
		$this->load->helper('date');
		$this->load->model('pos_model');

		$formdata = $this->input->post(NULL,TRUE);
		
		// generate sales transaction code
		// based on location id - cashier id - mktime
		$trxcode = $this->input->post('cashier_id') . '-' . $this->input->post('location_id') . '-' . time();

		// generate header cashier trx array (t_sales_pos)
		$cashier = array (
			'trx_code' => $trxcode,
			'trx_datetime' => date("Y-m-d H:i:s"),
			'trx_cashier_id' => $this->input->post('cashier_id'),
			'trx_user_id' => $this->ion_auth->user()->row()->id,
			'trx_sales_amount' => $this->input->post('total_sales')
			);

		// generate items detail array
		$item_line_counter = 1;
		$items = array ();
		for ($i=0; $i < count($formdata['inp_item_id']); $i++) { 
			array_push($items, array(
					'trx_id'		=> 0, // will be set later in the pos_model
					'line_id' 		=> $item_line_counter,
					'item_id' 		=> $formdata['inp_item_id'][$i],
					'item_qty'		=> $formdata['inp_item_qty'][$i],
					'item_price'	=> $formdata['inp_item_price'][$i],
					'item_total_price' =>
						$formdata['inp_item_qty'][$i] * $formdata['inp_item_price'][$i],
					'include_tax'	=> true
				));
			$item_line_counter++;
		}

		// generate payment detail array
		$payment_line_counter = 1;
		$payments = array();

		// for cash we have to combine and deduct the change
		$remaining_sales_amount = $this->input->post('total_sales');

		for ($i=0; $i < count($formdata['inp_pay_method']); $i++) { 
			array_push($payments, array(
					'trx_id'		=> 0, // will be set later in the pos_model
					'line_id' 		=> $payment_line_counter,
					'method' 		=> $formdata['inp_pay_method'][$i],
					'amount'		=> $formdata['inp_pay_amount'][$i],
					'identifier_code' => $formdata['inp_pay_identifier'][$i],
					'change_amount' => 0
				));
			$remaining_sales_amount -= $formdata['inp_pay_amount'][$i];
			$payment_line_counter++;
		}

		// if over paid
		if($remaining_sales_amount < 0){
			// combine the cash and deduct with the remaining
			$total_cash = 0;

			for($p = 0; $p < count($payments) ; $p++){
				if($payments[$p]['method'] == 'cash'){
					$total_cash += $payments[$p]['amount'];
					array_splice($payments,$p);
				}
			}

			array_push($payments, array(
				'trx_id'		=> 0, // will be set later in the pos_model
				'line_id' 		=> $payment_line_counter,
				'method' 		=> 'cash',
				'amount'		=> ($total_cash + $remaining_sales_amount),
				'change_amount'	=> abs($remaining_sales_amount),
				'identifier_code' => ''
			));
		}

		$trx_id = $this -> pos_model -> create_transaction($cashier, $items, $payments);

		if (is_numeric($trx_id)){

			// $this->pos_print($trx_id);
			redirect("sales/pos_print/{$trx_id}",'refresh');
		} else {
			echo 'failed';
		}
	}

	/**
	*
	* pos_print($trx)
	* print receipt of the id
	**/
	public function pos_print($trx)
	{
		$this->load->model('pos_model');
		$data['trx'] = $this -> pos_model -> get_pos_data($trx);
		$this->load->view('sales/sales_pos_print', $data);
	}
	
	
	/**
	*
	* POS - close or open cashier
	*
	**/
	public function pos_toggle_cashier($cashier_id)
	{
		$this->load->model('pos_model');
		if ($this -> pos_model -> toggle_cashier_status($cashier_id)){
			$msg = 'Cashier closed successfully';
			redirect('sales/pos','refresh');
		} else {
			$msg = 'Cashier Not Closed';
			redirect('sales/pos','refresh');
		}
	}


	/**
	*
	* POS - review transactions before close
	*
	**/
	public function pos_review_cashier($cashier_id = NULL, $sales_date = NULL)
	{
		if(!is_numeric($cashier_id))
			$cashier_id = NULL;

		if($this -> input -> post('sales_date') === NULL){
			$data['sales_date'] = date('Y-m-d');
		} else {
			$data['sales_date'] = date('Y-m-d', strtotime($this -> input -> post('sales_date')));
		}
		$filter = array("DATE(input_date) = '{$data['sales_date']}'");

		$this->load->model('pos_model');

		$data['cashier'] = $this -> pos_model -> get_cashier_info($cashier_id);
		$data['transactions'] = $this -> pos_model -> get_cashier_transactions($cashier_id, $filter);
		$data['payments'] = $this -> pos_model -> get_payment_summary($data['sales_date']);

		$this -> sky -> load_page('sales_pos_review',$data);

	}
	
	/**
	*
	* POS - close cahier and post unposted the transactions
	*
	**/
	public function pos_close_cashier($cashier_id, $date)
	{
		if (is_numeric($cashier_id))
		{
			$this->load->model('pos_model');
			$result = $this -> pos_model -> close_cashier($cashier_id, $date);
			if($result['success'] === true){
				redirect('sales/pos','refresh');
			} else {
				echo "<script>alert('{$result['message']}')</script>";
				redirect('sales/pos','refresh');
			}
		} else {
			echo 'error';
		}
	}
	
	/**
	*
	* POS 		- get_item by SKU / Code
	* Return 	- JSON encoded result
	*
	**/
	public function pos_get_item()
	{
		// requires item code to be submitted by POST data
		$itemcode = $this->input->post('itemcode');

		$return = array(
			'success' => false,
			'message' => '',
			'itemdata' => array()
		);

		if (trim($itemcode) !== ''){
			$this->load->model('pos_model');
			$item = $this -> pos_model -> get_item($itemcode);
			if (count($item)){
				$return['success'] = true;
				$return['message'] = 'Item Obtained';
				$return['itemdata'] = $item;
			} else {
				$return['success'] = false;
				$return['message'] = 'Item Not Found';
			}

		} else {
			$return['success'] = false;
			$return['message'] = 'Blank Item Code';
		}

		// $this -> output -> cache(1); // cache the page in one minute
		header("Content-Type: application/json");
		echo json_encode($return);

	}

	/**
	*
	* POS 		- get_voucher by voucher number
	* Return 	- JSON encoded result
	*
	**/
	public function pos_get_voucher()
	{
		// requires item code to be submitted by POST data
		$vouchercode = $this->input->post('vouchernumber');

		$return = array(
			'success' => false,
			'message' => '',
			'voucherdata' => array()
		);

		if (trim($vouchercode) !== ''){
			$this->load->model('pos_model');
			$voucher = $this -> pos_model -> get_voucher($vouchercode);
			if (count($voucher)){
				
				$todays_date = date_create(date('Y-m-d'));
				$active_date = date_create(date('Y-m-d',strtotime($voucher['active_date'])));
				$expiry_date = date_create(date('Y-m-d',strtotime($voucher['expiry_date'])));

				if($voucher['used'] == 1){
					$return['success'] = false;
					$return['message'] = 'Voucher has been used';
				} elseif ($voucher['disabled'] == 1){
					$return['success'] = false;
					$return['message'] = 'Voucher is disabled';
				} elseif ($todays_date < $active_date || $todays_date > $expiry_date){
					$return['success'] = false;
					$return['message'] = 'Voucher not active or expired';
				} else {
					$return['success'] = true;
					$return['message'] = 'Item Obtained';
					$return['voucherdata'] = $voucher;
				}

			} else {
				$return['success'] = false;
				$return['message'] = 'Voucher Not Found';
			}

		} else {
			$return['success'] = false;
			$return['message'] = 'Empty Voucher Code';
		}

		// $this -> output -> cache(1); // cache the page in one minute
		header("Content-Type: application/json");
		echo json_encode($return);

	}

	/**
	*
	* POS - get discount voucher
	*
	**/
	public function pos_get_promo()
	{
		// requires item code to be submitted by POST data
		$promocode = $this->input->post('promocode');

		$return = array(
			'success' => false,
			'message' => '',
			'promodata' => array()
		);

		if (trim($promocode) !== ''){
			$this->load->model('pos_model');
			$promo = $this -> pos_model -> get_promo($promocode);
			if (count($promo)){
				$return['success'] = true;
				$return['message'] = 'Promo Obtained';
				$return['promodata'] = $promo;
			} else {
				$return['success'] = false;
				$return['message'] = 'Promo Not Found';
			}

		} else {
			$return['success'] = false;
			$return['message'] = 'Blank Promo Code';
		}

		// $this -> output -> cache(1); // cache the page in one minute
		header("Content-Type: application/json");
		echo json_encode($return);

	}
	

	/*-----  End of POS Section  ------*/
	

	/*=======================================
	=            Voucher Section            =
	=======================================*/

	/**
	*
	* List out all vouchers
	*
	**/
	
	public function vouchers()
	{
		$this -> sky -> load_page('sales_vouchers');
	}

	public function ajax_voucher_list()
	{
		$this->load->model('voucher_model');
		header('Content-type: application/json');
		echo json_encode($this -> voucher_model -> ajax_voucher_list());
	}

	public function voucher_add()
	{
		$this->load->model('voucher_model');
		$data = array();
		$data['msg'] = '';
		if($_POST){
			$postdata = $this->input->post(NULL,TRUE);
			// id, voucher_code, issued_date, disabled, active_date, expiry_date, created_by, created_datetime, voucher_count
			// the following data are set from here (not exists in the form)
			$postdata['issued_date'] = date('Y-m-d');
			$postdata['disabled'] = false;
			$postdata['created_by'] = $this->ion_auth->user()->row()->id;
			$postdata['created_datetime'] = date('Y-m-d h:i:s');
			if($this -> voucher_model -> add_voucher($postdata)){
				$data['msg'] = 'Voucher added successfully';
			} else {
				$data['msg'] = 'Unable to add voucher';
			}
		}

		$this -> sky -> load_page('sales_voucher_add', $data);
	}

	public function voucher_edit($voucher_id = NULL, $void = false)
	{
		if(!$voucher_id){
			redirect('sales/vouchers','refresh');
		}

		$this->load->model('voucher_model');

		if($void){
			$this->voucher_model->void_remaining_voucher($voucher_id);
		}

		if($_POST)
		{
			$postdata = $this->input->post(NULL,TRUE);
			if($this-> voucher_model ->update_voucher($voucher_id, $postdata)){
				$data['msg'] = 'Voucher edited successfully';
			} else {
				$data['msg'] = 'Unable to edit voucher';
			}
		}

		$data['voucher'] = $this -> voucher_model -> get_voucher($voucher_id);

		$this -> sky -> load_page('sales_voucher_edit',$data);
	}
	
	
	/*-----  End of Voucher Section  ------*/
	
	
}

/* End of file Sales.php */
/* Location: ./application/controllers/Sales.php */
