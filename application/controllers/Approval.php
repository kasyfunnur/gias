<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Approval extends CI_Controller {	
	
	private $current_user;
	
	public function __construct() {
        parent::__construct();
		/* if (!$this -> session -> userdata ("logged_in")) header("location:".base_url('login')); */
		/* does not need user to be logged in, because user can approve from email without logged in */
		$this -> current_user = $this -> session -> userdata('user_id');
		$this -> load -> model ('approval_model');
		}
	
	public function approval_list($appr_id = NULL){
		$data['total_pending'] = $this -> approval_model -> get_approval_count($this->current_user);
		$data['document_count'] = $this -> approval_model -> get_document_approval_count($this->current_user);
		$data['approval_list'] = $this -> approval_model -> get_pending_approvals($this->current_user, $appr_id);
		$data['selected_doc'] = $appr_id == NULL ? 0 : $appr_id;
		$this -> sky -> load_page ('index', $data);
		}
	
	public function approve_document($key, $doc_approval_id, $approve_action){
		$this -> db -> trans_begin();
		$approval_line = $this -> approval_model -> get_line_approval ($doc_approval_id, $key);
		$doc_number = $approval_line['doc_number'];
		$this -> load -> model ( $approval_line['appr_model'], 'mdl' );
		$the_function = $approval_line['appr_function'] ;
		
		// TODO : delete the rest of the position that are within the same step (OR conditioned approvals)
			
		// if step based, check for previous step for incompleteness & notify the next step of users
		$step_based = $approval_line['step_based'] == 1 ? TRUE : FALSE;
		$current_step = $approval_line['approver_step'];
		
		if ( $step_based && $current_step > 1 &&  !$this->approval_model->check_previous_step($doc_number,$current_step))
		{
			// cannot do this because previous step is not complete yet	
			$status_object['success'] = 0;
			$status_object['msg'] = "Previous Approvers have not completed their approvals yet.";
			
		} else {
			if ($approve_action == 3){
				$approval_complete = $this -> approval_model -> check_complete_approval($doc_approval_id);
				
				$new_doc_status = ($approval_complete == TRUE) ? 3 : 2;
			} else {
				$new_doc_status	= $approve_action;
			}
			
			$status_object = $this -> mdl -> {$the_function} ($doc_number, $new_doc_status);
		}
		
		/*
			status object will be something like this
			$status_object['success'] = TRUE / FALSE
			$status_object['message'] = "Unable to approve this document because asset is in another pending transaction"
		
		*/
		
		
		if ( $status_object['success'] == 1 ) {
				 
				 $this -> approval_model -> update_approval($doc_approval_id, $approve_action);
				 
				 // if approve, check completeness of the current step and notify the next step set of users
				 if($approve_action == 3 && $step_based){
					 $this -> approval_model -> notify_next_step($doc_approval_id);
				 }
				 
				 $data['msg'] = 
					'<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.htmlspecialchars($status_object['msg']).'</div>';
				 
				//echo "Successfully Approved \n\r" . htmlspecialchars($status_object['msg']) ;
				 $data['total_pending'] = $this -> approval_model -> get_approval_count($this->current_user);
				 $data['document_count'] = $this -> approval_model -> get_document_approval_count($this->current_user);
				 $data['approval_list'] = $this -> approval_model -> get_pending_approvals($this->current_user);
				 $data['selected_doc'] = !isset($appr_id) ? 0 : $appr_id;
				 
				 if ($this->ion_auth->logged_in())
				 	$this -> sky -> load_page ('index', $data);
				 else
				 	$this -> load -> view ('approval/email_msg',$data);
			}
		else {
			
			$data['msg'] = 
					'<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.htmlspecialchars($status_object['msg']).'</div>';
				 
			//echo "Successfully Approved \n\r" . htmlspecialchars($status_object['msg']) ;
			$data['total_pending'] = $this -> approval_model -> get_approval_count($this->current_user);
			$data['document_count'] = $this -> approval_model -> get_document_approval_count($this->current_user);
			$data['approval_list'] = $this -> approval_model -> get_pending_approvals($this->current_user);
			$data['selected_doc'] = !isset($appr_id) ? 0 : $appr_id;
			
			if ( $this -> session -> userdata ("logged_in") )
				$this -> sky -> load_page ('index', $data);
			else
				$this -> load -> view ('approval/email_msg',$data);
			//echo "Failed to approve \n\r" . htmlspecialchars($status_object['msg']) ;
		}
		
		$this -> db -> trans_complete();
	}
		
	// approval setting
	public function settings(){
		$this -> load -> model ('approval_model');
		$data['approval_documents'] = $this -> approval_model -> getAll();
		$this -> sky -> load_page ('settings', $data);
	}
		
	public function settings_detail($appr_id = NULL){
		$this -> load -> model ('approval_model');
		if($appr_id == NULL || $appr_id == 0){
			$data['approval_documents'] = $this -> approval_model -> getAll();
			$this -> sky -> load_page ('settings', $data);
		} else {
			$this -> load -> helper ('approval');
			$data['settings_detail'] = $this -> approval_model -> getApprovalDetail($appr_id);
			$this -> sky -> load_page ('settings_detail', $data);
		}
	}
}