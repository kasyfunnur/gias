<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Business extends CI_Controller {	
	public function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this -> load -> model ('business_model');
		$this -> load -> model ('dashboard_model');
		$this -> load -> model ('document_model');

		$this->load->helper('url');
		$this->load->helper('swlayout');
		//$this->load->helper('swlayout_helper');
	}
	
	public function customer_list(){
		$data['datatable'] = $this->business_model->getCustomer();
		$this -> sky -> load_page ('customer_list',$data);
	}
	public function vendor_list(){
		$data['datatable'] = $this->business_model->getVendor();
		$this -> sky -> load_page ('vendor_list',$data);
	}
	public function partner(){
		$data['total_vendor'] = $this->dashboard_model->total_vendor();
		$data['total_customer'] = $this->dashboard_model->total_customer();
		$this->sky->load_page('partner',$data);
	}
	
	public function customer_add(){
		$this->sky->load_page('customer_add');
	}
	public function vendor_add(){
		$data['doc_num'] = $this->document_model->getNextSeries('VEND');
		$this->sky->load_page('vendor_add',$data);
	}
	public function vendor_add_query(){
		//DATA
		$data = array(
			//"buss_code"		=> $this->input->post('buss_code'),
			"buss_name"		=> $this->input->post('buss_name'),
			"is_vendor"		=> 1,
			"buss_group"	=> 1,
			"buss_type"		=> 'Supplier',
			"buss_addr"		=> $this->input->post('buss_addr'),
			"buss_city"		=> $this->input->post('buss_city'),
			"buss_state"	=> $this->input->post('buss_state'),
			"buss_country"	=> $this->input->post('buss_country'),
			"buss_website"	=> $this->input->post('buss_website'),
			"buss_tlp1"		=> $this->input->post('buss_tlp1'),
			"buss_tlp2"		=> $this->input->post('buss_tlp2'),
			"buss_email"	=> $this->input->post('buss_email'),
			"buss_curr"		=> $this->input->post('buss_curr'),
			"limit_local"	=> $this->input->post('limit_local')
		);

		//TRANS
		$this->db->trans_begin();
		$this->business_model->add($data,'VEND');
		$this->sky->trans_end();
	}
	public function vendor_edit_query(){
		//DATA
		$data = array(
			//"buss_code"		=> $this->input->post('buss_code'),
			"buss_name"		=> $this->input->post('buss_name'),
			"is_vendor"		=> 1,
			"buss_group"	=> 1,
			"buss_type"		=> 'Supplier',
			"buss_addr"		=> $this->input->post('buss_addr'),
			"buss_city"		=> $this->input->post('buss_city'),
			"buss_state"	=> $this->input->post('buss_state'),
			"buss_country"	=> $this->input->post('buss_country'),
			"buss_website"	=> $this->input->post('buss_website'),
			"buss_tlp1"		=> $this->input->post('buss_tlp1'),
			"buss_tlp2"		=> $this->input->post('buss_tlp2'),
			"buss_email"	=> $this->input->post('buss_email'),
			"buss_curr"		=> $this->input->post('buss_curr'),
			"limit_local"	=> $this->input->post('limit_local')
		);

		//TRANS
		$this->db->trans_begin();
		$this->business_model->edit($data,array('buss_code'=>$this->input->post('buss_code')));
		$this->sky->trans_end();
	}
	
	public function customer($code){
		$data['business'] = $this->business_model->detail($code);
		$this->sky->load_page('customer_detail',$data);
	}
	public function vendor($code){
		$data['business'] = $this->business_model->detail($code);
		$this->sky->load_page('vendor_detail',$data);
	}	
	
	public function update_query($code){
		//1.INIT
		//2.POST VAR & OTHER VAR
		$data = array(
			"buss_name"		=> $this->input->post('buss_name'),
			"buss_addr"		=> $this->input->post('buss_addr'),
			"buss_city"		=> $this->input->post('buss_city'),
			"buss_state"	=> $this->input->post('buss_state'),
			"buss_country"	=> $this->input->post('buss_country'),
			"buss_tlp1"		=> $this->input->post('buss_tlp1'),
			"buss_tlp2"		=> $this->input->post('buss_tlp2'),
			"buss_website"	=> $this->input->post('buss_website'),
			"buss_email"	=> $this->input->post('buss_email')
		);
		//3.VALIDATION		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		$this->business_model->update_business($data,$code);
		//4.3.END TRANS	
		$this->sky->trans_end();
	}
	
	public function vendor_trx($buss_code){
		$data['buss_info'] = $this->business_model->detail($buss_code);
		$data['datatable'] = $this->business_model->get_transaction($buss_code);
		$this->sky->load_page('vendor_trx',$data);
	}
	public function customer_trx($buss_code){
		$data['datatable'] = $this->business_model->get_transaction($buss_code);
		$this->sky->load_page('customer_trx',$data);
	}
	
	//dialog
	/*public function dialog_customer(){
		$data['datasource'] = $this->business_model->getCustomer();
		$this->sky->load_page('dialog_customer',$data,NULL,1);	
	}
	public function dialog_vendor(){
		$data['datasource'] = $this->business_model->getVendor();
		$this->sky->load_page('dialog_vendor',$data,NULL,1);	
	}*/
}