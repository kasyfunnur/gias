<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Flow extends CI_Controller {	
	public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this->load->model('Flow_model');
	}
	
	/**
	 * Get Flow
	 */
	public function get_flow($module,$doc,$doc_id)
	{
		//$var_flow = $this->Flow_model->get_list_flow($module);
		//$flow_position = $this->Flow_model->get_doc_position($module,$doc);
		
		$flow_before = $this->Flow_model->get_flow_before($module,$doc);
		$flow_after = $this->Flow_model->get_flow_after($module,$doc);
		
		/**
		 * Get the Prev proces (BEFORE)
		 */
		$next_doc_id = $doc_id;
		$temp_before = array();		
		for($z = 0;$z < count($flow_before); $z++){ 
			$temp_curr_before = $this->Flow_model->get_data_before($flow_before[$z]['table_reff'],$next_doc_id);
			if(count($temp_curr_before)){
				array_push($temp_before,$temp_curr_before);
				$next_doc_id = $temp_curr_before[0]['reff_id'];
			}
		}		
		//var_dump($temp_before);

		/**
		 * Get the Next process (AFTER)
		 */
		$next_reff_id = $doc_id;
		$temp_after = array();
		for($zz = 0; $zz < count($flow_after); $zz++){ 
			$temp_curr_after = $this->Flow_model->get_data_after($flow_after[$zz]['doc_type'],$flow_after[$zz]['table_reff'],$next_reff_id);
			if(count($temp_curr_after)){
				array_push($temp_after,$temp_curr_after);
				$next_reff_id = $temp_curr_after[0]['doc_id'];
			}
		}
		//var_dump($temp_after);

		$doc_before = array();
		foreach($temp_before as $before){
			foreach($before as $_before){
				if($_before['reff_type']){
					$table_now = $this->Flow_model->get_table_info($_before['reff_type']);
					$curr_doc = $this->Flow_model->get_doc($table_now[0]['table_info'],$_before['reff_id']);
					array_push($doc_before,$curr_doc);
				}				
			}
		}

		$doc_after = array();
		foreach($temp_after as $after){
			foreach($after as $_after){
				if($_after['reff_type']){
					$table_now = $this->Flow_model->get_table_info($_after['reff_type']);
					$curr_doc = $this->Flow_model->get_doc($table_now[0]['table_info'],$_after['doc_id']);
					array_push($doc_after,$curr_doc);
				}				
			}
		}

		//var_dump($doc_before);
		//var_dump($doc_after);
		$all_doc = array();
		array_push($all_doc, $doc_before);
		array_push($all_doc, $doc_after);
		return $all_doc;
	}
}