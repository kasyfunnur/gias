<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventory extends CI_Controller {	
	public function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this->load->library('sky');
		$this->load->helper('url');
		$this->load->helper('swlayout');
		$this->load->helper('swconfig');
		$this->load->helper('swstatus');
		$this->load->model('sales_order_model');
		$this->load->model ('inventory_model');
		$this->load->model ('inventory_transfer_model');
		$this->load->model ('location_model');
		$this->load->model ('document_model');
		$this->load->model ('status_model');		
	}
	
	public function stock(){
		$this -> sky -> load_page ('stock');
	}
	public function stock_add(){
		$data['ddl_group']			= $this->inventory_model->get_group_all();
		$data['ddl_style']			= $this->inventory_model->get_style_all();
		$data['ddl_category']		= $this->inventory_model->get_category_all();
		$data['uom']				= $this->inventory_model->get_uom();
		$this->sky->load_page('stock_add',$data);
	}
	public function stock_add_query(){
		//1.INIT
		//2.POST VAR & OTHER VAR
		$data = array(
			"item_g_id"				=> $_POST['group_id'],
			"item_code"				=> $this->input->post('item_code'),
			"item_name"				=> $this->input->post('item_name'),
			"item_style"			=> $this->input->post('style_code'),
			"item_style_char"		=> $this->input->post('style_code'),
			"item_color"			=> $this->input->post('color_code'),
			"item_color_char"		=> $this->input->post('color_code'),
			"uom"					=> $this->input->post('uom')
		);
		//3.VALIDATION		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		
		$this -> db -> trans_begin();
		//4.2.RUN TRANS
		//$this->inventory_model->create_stock($data);
		$this->inventory_model->add($data);
		//4.3.END TRANS	
		$this->sky->trans_end();
	}
	public function style(){
		$data['datatable'] = $this->inventory_model->get_style_all();
		$this -> sky -> load_page ('style', $data);
	}
	public function style_add(){
		$data['ddl_group']			= $this->inventory_model->get_group_all();
		$data['ddl_category']		= $this->inventory_model->get_category_all();
		$this -> sky -> load_page ('style_add', $data);
	}
	public function style_detail($code){
		$data['form_header']		= $this->inventory_model->get_style($code);
		$data['form_detail']		= $this->inventory_model->get_stock_by_style($code);
		$this -> sky -> load_page ('style_detail', $data);
	}
	public function style_add_query(){
		//1.INIT
		$this->load->model('inventory_model');
		$this->load->model('location_model');
		//2.POST VAR & OTHER VAR
		//3.VALIDATION		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		for($x=0;$x<=$this->input->post('table_row')-2;$x++){ 
			$data_item = array(
				"item_g_id"				=> "2",//hard coded    //$this->input->post('group_id'),
				"item_code"				=> $_POST['item_code'][$x],
				"item_name"				=> $_POST['item_name'][$x],
				"item_tag"				=> $_POST['item_tag'][$x],
				"item_category_char"	=> $_POST['category_id'],
				"item_style"			=> $_POST['header_style_name'],
				"item_style_char"		=> $_POST['header_style_code'],
				"item_style_launch"		=> $_POST['style_launch'],
				"item_color"			=> $_POST['item_color'][$x],
				"item_color_char"		=> $_POST['item_color'][$x],
				"item_inventory"		=> 1,
				"item_sales"			=> 1,
				"item_purchase"			=> 1,
				"uom_inventory"			=> 1,
				"item_status"			=> 'ACTIVE'
			);
			$this->inventory_model->table = "t_inventory";
			$last_id = $this->inventory_model->create($data_item);
			$temp_last_id = $last_id;
			
			$temp_whse = $this->location_model->get_site_location();
			for($z = 0; $z<count($temp_whse);$z++){ 
				$data_item_whse = array(
					"item_id"		=> $temp_last_id,
					"location_id"	=> $temp_whse[$z]['location_id']
				);
				$this->inventory_model->table = "t_inventory_warehouse";
				$this->inventory_model->create($data_item_whse);
			}			
		}
		//4.2.RUN TRANS
		
		//4.3.END TRANS	
		$this->sky->trans_end();
	}
	
	public function detail($code){
		$item = $this->inventory_model->get_stock_id($code); 
		$data['item_detail'] = $this->inventory_model->get_asset_info($item);
		$data['item_warehouse'] = $this->inventory_model->get_inventory_warehouse($item);
		//$data['item_transaction'] = $this->inventory_model->get_inventory_transaction('',$item);
		$data['item_transaction'] = $this->inventory_model->querying("
			select `bal_table`.sysdate, `bal_table`.docdate, `bal_table`.doc, `bal_table`.qty, `bal_table`.whse, `bal_table`.trx_status
				,@balance := @balance + `bal_table`.qty as 'buff1'
				,@balance2 := -@balance as 'buff2'
				,@balance3 := @balance2 + `bal_table`.qty as 'balance'
			from 
				(
					select trans_id as 'id', timestamp as 'sysdate', doc_date as 'docdate', doc_num as 'doc', 
                    item_qty as 'qty', location_name as 'whse', t_inventory.qty_onhand as 'onhand', 
					t_inventory_transaction.trx_status as 'trx_status'
					from t_inventory_transaction 
				  	inner join t_inventory on t_inventory.item_id = t_inventory_transaction.item_id 
					inner join t_site_location on t_site_location.location_id = t_inventory_transaction.whse_id 
					where t_inventory_transaction.item_id = (select item_id from t_inventory where item_id = '".$item."') 
					order by timestamp desc
				) as `bal_table` 
				join (select @balance := (select -qty_onhand from t_inventory where item_id = '".$item."')) as varinit1 
				join (select @balance2 := 0) as varinit2
				join (select @balance3 := 0) as varinit3
			 
			order by `bal_table`.id desc
		");
		$this->sky->load_page('detail',$data);
	}
	public function transaction_detail($type,$item_id,$wh_id){
		$data['item_trx'] = $this->inventory_model->get_inventory_transaction($type,$item_id,$wh_id);
		var_dump($data['item_trx']);
	}
	
	/**
	*
	* warehouse functions
	*
	**/
	public function warehouse(){
		$this -> sky -> load_page ('warehouse');
	}

	public function warehouse_detail($whid)
	{
		if($_POST && is_numeric($this->input->post('location_id'))){
			// update data ...
			$whid = $this->input->post('location_id');
			if ($this -> inventory_model -> update_warehouse ($whid,$this -> input -> post(NULL,TRUE))){
				$data['msg'] = 'Successfully Updated Warehouse';
			};
		}
		if(is_numeric($whid)){
			//$this -> load -> model ('inventory_model');
			$data['whid'] = $whid;
			$data['whdata'] = $this -> inventory_model -> get_warehouse($whid);
			$this -> sky -> load_page ('warehouse_detail',$data);
		} else {
			redirect('inventory/warehouse','refresh');
		}


	}


	
	//adjustment
	public function adjustment(){
		$this->db->select("*");
		$this->db->from('t_stock_adjust_header');
		$this->db->join('t_site_location','t_site_location.location_id = t_stock_adjust_header.whse_id','inner');
		$this->db->order_by('doc_id','desc');
		$query=$this->db->get();
		$data['datatable'] = $query->result_array();
		$this -> sky -> load_page ('adjustment',$data);
	}
	public function adjustment_add(){
		$data['ddl_whse'] 			= $this->location_model->get_site_location(); 
		$data['doc_num'] 			= $this->document_model->getNextSeries('ITM_ADJ');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$this -> sky -> load_page ('adjustment_add',$data);
	}
	public function adjustment_edit($code){
		$data['ddl_whse'] 			= $this->location_model->get_site_location(); 
		//$data['doc_num'] 			= $this->document_model->getNextSeries('ITM_ADJ');
		$data['form_header']		= $this->inventory_model->querying("select * from t_stock_adjust_header where doc_num = '".$code."'");
		$data['form_detail']		= $this->inventory_model->querying("
			select * from t_stock_adjust_detail 
			inner join t_inventory on t_stock_adjust_detail.item_id = t_inventory.item_id 
			where doc_id = (select doc_id from t_stock_adjust_header where doc_num = '".$code."')");
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$this -> sky -> load_page ('adjustment_edit',$data);
	}
	public function adjustment_cancel($code){
	}
	public function adjustment_close($code){
	}

	public function adjustment_add_query(){
		//1.INIT
		$this->load->model('inventory_model');
		//2.POST VAR & OTHER VAR
		$document_number = $this->document_model->getNextSeries('ITM_ADJ');
		$data_header = array(
			//"doc_id" 		=> $this->db->insert_id(),//$temp_doc_id,
			"doc_num" 		=> $document_number,
			"doc_dt" 		=> $this->input->post('doc_dt'),
			"doc_ddt" 		=> $this->input->post('doc_dt'),
			"doc_status" 	=> "ACTIVE",
			"whse_id"		=> $this->input->post('whse_id'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"create_by"		=> $this->session->userdata("username")			
		);		
		//3.VALIDATION
		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		$this -> db -> trans_begin();
		//4.2.RUN TRANS
		$this->inventory_model->table = 't_stock_adjust_header';
		$temp_temp_id = $this->inventory_model->create($data_header);
		$temp_id = $temp_temp_id;
		
		for($x=0;$x<=$this->input->post('table_row')-2;$x++){
			$this->inventory_model->table = 't_inventory';
			$item = $this->inventory_model->read('item_code',$_POST['item_code'][$x]);
			$data_detail = array(
				"doc_id" 		=> $temp_id,
				"doc_line"		=> $x+1,
				"item_id"		=> $item['item_id'],
				"item_qty"		=> $_POST['item_qty'][$x]
			);
			$this->inventory_model->table = 't_stock_adjust_detail';
			$this->inventory_model->create($data_detail);
			
			//item transaction			
			$data_transaction_in = array(
				"doc_type"		=> 'TRF',
				"doc_num"		=> $document_number,
				"doc_date"		=> $this->input->post('doc_dt'),
				"item_id"		=> $item['item_id'],
				"item_qty" 		=> $_POST['item_qty'][$x],
				"item_value"	=> 0,
				"total_value"	=> 0,
				"whse_id"		=> $this->input->post('whse_id') //whse transit
			);
			$this->inventory_model->table="t_inventory_transaction";			
			$this->inventory_model->create($data_transaction_in);
		}
		$this->db->query("call procUpdateQtyOnHand()");
		//4.3.END TRANS	
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			return false;
		}else{
			$this->db->trans_commit();
			$this->document_model->useDocSeries('ITM_TRF');
			echo "1"; return true;
		}
	}
	public function adjustment_edit_query(){		
	}
	public function adjustment_cancel_query(){
	}
	public function adjustment_close_query(){		
	}
	

	public function transfer(){
		$this->inventory_model->table = 't_stock_moving_header';
		//$data['datatable'] = $this->inventory_model->readAll();		
		$data['datatable'] = $this->inventory_model->querying("
			select t0.*, t1.location_name as 'whse_from_name', t2.location_name as 'whse_to_name'
			from t_stock_moving_header t0
			inner join t_site_location t1 on t1.location_id = t0.whse_from
			inner join t_site_location t2 on t2.location_id = t0.whse_to
			order by t0.doc_id desc
		");
		$this -> sky -> load_page ('transfer',$data);
	}

	public function transfer_send(){
		$data['ddl_whse'] 			= $this->location_model->get_site_location(); 
		$data['ddl_freight']		= $this->sales_order_model->getFreight();
		if(!$data['ddl_freight']){
			$data['ddl_freight'] = array(array("doc_freight"=>""));
		}
		$data['doc_num'] 			= $this->document_model->getNextSeries('ITM_TRF');
		$data['dialog_inventory'] 	= $this->inventory_model->get_stock_all();
		$this -> sky -> load_page ('transfer_send',$data);
	}
	public function transfer_receipt($code){
		$data['ddl_whse'] 			= $this->location_model->get_site_location(); 
		$this->inventory_model->table = 't_stock_moving_header';		
		$data['form_header']		= $this->inventory_transfer_model->getHeader($code);
		$data['form_detail']		= $this->inventory_transfer_model->getDetail($code);
		
/*		//$data['form_detail']		= $this->inventory_model->readAll('doc_id',$data['form_header']['doc_id']);		
		$data['form_detail']		= $this->inventory_model->querying("
			select * from t_stock_moving_detail t0
			inner join t_inventory t1 on t0.item_id = t1.item_id
			where t0.doc_id = ".$data['form_header']['doc_id']);
			
		$this->location_model->table = 't_site_location';
		$data['whse_from_name']		= $this->location_model->read('location_id',$data['form_header']['whse_from']);
		$data['whse_to_name']		= $this->location_model->read('location_id',$data['form_header']['whse_to']);*/
		$this -> sky -> load_page ('transfer_receipt',$data);
	}
	public function transfer_view($code){
		$data['ddl_whse'] 			= $this->location_model->get_site_location(); 
		//$this->inventory_model->table = 't_stock_moving_header';		
		$data['form_header']		= $this->inventory_transfer_model->getHeader($code);
		$data['form_detail']		= $this->inventory_transfer_model->getDetail($code);
		$this -> sky -> load_page ('transfer_view',$data);
	}
	public function transfer_cancel($code){
		$data['form_header']		= $this->inventory_transfer_model->getHeader($code);
		$data['form_detail']		= $this->inventory_transfer_model->getDetail($code);
		$this -> sky -> load_page ('transfer_cancel',$data);
	}
	public function transfer_close($code){
		$data['form_header']		= $this->inventory_transfer_model->getHeader($code);
		$data['form_detail']		= $this->inventory_transfer_model->getDetail($code);
		$this -> sky -> load_page ('transfer_close',$data);
	}
	public function transfer_print($code){
		$data['title'] 				= 'Stock Transfer';
		$data['subtitle'] 			= $code;
		$data['form_header'] 		= $this->inventory_transfer_model->getHeader($code);
		$data['form_detail'] 		= $this->inventory_transfer_model->getDetail($code);
		$this->sky->load_page('print_stock_transfer',$data,NULL,1);	
	}	
	
	public function transfer_send_query(){
		//DATA
		$document_number = $this->document_model->getNextSeries('ITM_TRF');
		$data_header = array(
			"doc_num" 		=> $document_number,
			"doc_dt" 		=> $this->input->post('doc_dt'),
			"doc_ddt" 		=> $this->input->post('doc_ddt'),
			"doc_status" 	=> "ACTIVE",
			"whse_from"		=> $this->input->post('whse_from'),
			"whse_to"		=> $this->input->post('whse_to'),
			"doc_note" 		=> $this->input->post('doc_note'),
			"create_by"		=> $this->session->userdata("username"),
			"create_dt"		=> date('Y-m-d H:m:s')
		);
		$data_detail = array();
		$data_transaction_in = array();
		$data_transaction_out = array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){				
				$temp_info	= $this->inventory_model->get_asset_info($this->input->post('item_id')[$x]);
				array_push($data_detail, array(
						"doc_line"		=> $_POST['row'][$x],
						"item_id"		=> $this->input->post('item_id')[$x],
						"item_qty"		=> $this->input->post('item_qty')[$x]
					)
				);
			}
		}

		//VALIDATION

		//TRANS
		$this -> db -> trans_begin();		
		$data['t_stock_moving_header'] 	= $data_header;
		$data['t_stock_moving_detail'] 	= $data_detail;		
		$this->inventory_transfer_model->add($data);		
		$this->sky->trans_end();
	}
	public function transfer_receipt_query($code){
		//DATA
		//$document_number = $this->document_model->getNextSeries('ITM_RCP');
		$data_header = array(			
			"doc_id"		=> $this->input->post('doc_id'),
			"doc_num" 		=> $this->input->post('doc_num'),
			"doc_dt"		=> $this->input->post('doc_dt'),
			"doc_status" 	=> "CLOSED",
			"whse_from"		=> $this->input->post('whse_from'),
			"whse_to"		=> $this->input->post('whse_to'),
			"receipt_note" 	=> $this->input->post('receipt_note'),
			"receipt_dt" 	=> $this->input->post('receipt_dt'),
			"update_by"		=> $this->session->userdata("username"),
			"update_dt"		=> date('Y-m-d H:m:s')
		);		
		$data_detail = array();
		foreach($this->input->post('row') as $x=>$row){
			if($_POST['row'][$x] != count($this->input->post('row'))){
				array_push($data_detail,array(
						"doc_line"	=> $_POST['row'][$x],
						"item_id"	=> $this->input->post('item_id')[$x],
						"item_qty_closed"	=> $this->input->post('item_qty_receipt')[$x]
					)
				);
			}
		}
		
		//TRANS
		$this -> db -> trans_begin();
		$data['t_stock_moving_header'] = $data_header;
		$data['t_stock_moving_detail'] = $data_detail;
		$this->inventory_transfer_model->receipt($data);


		//$this->inventory_transfer_model->add($data);		
		$this->sky->trans_end();
	}
	public function transfer_cancel_query($code){
		//DATA
		$data_header = array(
			"doc_status" 	=> "CANCELLED",
			"cancel_by"		=> $this->session->userdata("username"),
			"cancel_dt"		=> date('Y-m-d H:m:s'),
			"cancel_reason" => $this->input->post('reason')
		);

		//TRANS
		$this -> db -> trans_begin();
		$data['t_stock_moving_header'] = $data_header;		
		$this->inventory_transfer_model->edit($data,array('doc_num'=>$code));
		//$this->inventory_transfer_model->change_status($data_header,array('doc_num'=>$code));
		$this->inventory_model->reverse_entry($code);		
		$this->sky->trans_end();
	}
	public function transfer_close_query($code){
		//DATA
		$data_header = array(
			"doc_status" 	=> "CLOSED",
			"update_by"		=> $this->session->userdata("username"),
			"update_dt"		=> date('Y-m-d H:m:s'),
			"cancel_reason" => $this->input->post('reason')
		);

		//TRANS
		$this -> db -> trans_begin();
		$data['t_stock_moving_header'] = $data_header;		
		$this->inventory_transfer_model->edit($data,array('doc_num'=>$code));
		//$this->inventory_model->reverse_entry($code);		
		$this->sky->trans_end();
	}
	

	//dialog
	public function dialog_inventory(){
		$data['datasource'] = $this->inventory_model->get_stock_all();
		$this->sky->load_page('dialog_inventory',$data,NULL,1);	
	}
	
	//ajax for datatables
	public function ajax_inventory_list($return="json"){
		if  ($return == 'json') echo json_encode($this -> inventory_model -> ajax_inventory_list());
	}public function ajax_stock_conv($return="json"){
		if  ($return == 'json') echo json_encode($this -> inventory_model -> ajax_stock_conv());
	}
	public function ajax_style_list($return="json"){
		if  ($return == 'json') echo json_encode($this -> inventory_model -> ajax_style_list());
	}
	public function ajax_warehouse_list($return="json"){
		if  ($return == 'json') echo json_encode($this -> inventory_model -> ajax_warehouse_list());
	}
	public function ajax_inventory_style_list($return="json"){
		if  ($return == 'json') echo json_encode($this -> inventory_model -> ajax_inventory_style_list());
	}
	
	public function ajax_transfer_list($return="json"){
		if  ($return == 'json') echo json_encode($this -> inventory_model -> ajax_transfer_list());
	}
	//public function ajax_inventory_info($id){
	public function ajax_inventory_info(){
		if(isset($_GET['inventory'])){
			echo json_encode($this -> inventory_model -> ajax_inventory_info($_GET['inventory']));
		}else{
			echo json_encode($this -> inventory_model -> ajax_inventory_info());
		}
	}
	public function ajax_style_info(){
		echo json_encode($this -> inventory_model -> ajax_style_info($_GET['style']));
	}
	public function ajax_inventory_get_stock_by_vendor(){
		echo json_encode($this -> inventory_model -> get_stock_by_vendor($_POST['buss_char']));
		//echo $_POST['buss_char'];
	}
	
	//typeahead
	public function ta_get(){
		$data = $this->inventory_model->ta_get();
		if (!$data)$data=FALSE;
		header('Content-type: application/json');
		//echo json_encode($data);exit();	
		echo $data;exit();	
	}
	

	/*==================================================
	=            Inventory Category Section            =
	==================================================*/

	/**
	*
	* Display item category index
	*
	*/
	public function category()
	{
		$this->load->model('inventory_model');
		$data['categories'] = $this -> inventory_model -> get_category_all();
		$this -> sky -> load_page ('category', $data);
	}

	public function ajax_category_list()
	{
		$this->load->model('inventory_model');
		echo json_encode($this -> inventory_model -> get_category_all());
	}
	

	/**
	*
	* Add new category
	*
	*/
	public function category_add()
	{
		$this->load->model('inventory_model');
		$this->load->model('account_model');
		$data['coa'] = $this->account_model->getAll(array(array(0=>'is_postable',1=>'1')));
		$formdata = $this->input->post(NULL);
		if(! empty ($formdata)){
			if($this -> inventory_model -> add_category($this->input->post(NULL))){
				$this->session->set_flashdata('flash_notification', 'New Category Created');
				redirect('inventory/category');
			} else {
				$this->session->set_flashdata('flash_notification', 'Error creating new Category');
			}

		}
		$this -> sky -> load_page ('category_add', $data);

	}

	/**
	*
	* Edit category
	*
	*/
	public function category_edit($cat_id = NULL)
	{
		if(! is_numeric($cat_id)){
			$this->session->set_flashdata('flash_notification', 'Invalid Category ID');
			redirect('inventory/category','refresh');
		}
		
		$this->load->model('inventory_model');
		$this->load->model('account_model');

		$data['coa'] = $this->account_model->getAll(array(array(0=>'is_postable',1=>'1')));
		$data['category'] = $this -> inventory_model -> get_category($cat_id);
		
		$formdata = $this->input->post(NULL);

		if(! empty ($formdata)){
			if($this -> inventory_model -> update_category($cat_id, $formdata)){
				$this->session->set_flashdata('flash_notification', 'Category Updated');
				redirect('inventory/category');
			} else {
				$this->session->set_flashdata('flash_notification', 'Error updating category');
			}

		}
		$this -> sky -> load_page ('category_edit', $data);

	}

	
	
	
	/*-----  End of Inventory Category Section  ------*/
	
	

	/**
	 * REPORT MODEL
	 */
	public function inventory_balance_report($filters){
		if($filters['date_from']){
			//$this->db->where('doc_dt >=', $filters['date_from']);
		}
		if($filters['date_to']){
			//$this->db->where('doc_dt <=', $filters['date_to']);
		}
		$this->db->from('v_itembalance');
		return $this->db->get()->result_array();
	}


	/*==================================================
	=            Stock Conversion			            =
	==================================================*/

	public function conversion(){
		$this -> sky -> load_page ('stock_conv');
	}

	public function stock_conv_add(){
		$data['inventory']			= $this->inventory_model->get_stock_all();
		$data['warehouse']			= $this->inventory_model->get_warehouse_all();
		$this->sky->load_page('stock_conv_add',$data);
	}

	public function stock_conv_add_query(){
		//1.INIT
		//2.POST VAR & OTHER VAR
		$data = array(
			"doc_no"					=> $this->input->post('doc_code'),
			"doc_date"					=> $this->input->post('doc_date')
			// "inventory_in"				=> $this->input->post('inventory_in'),
			// "qty_in"					=> $this->input->post('qty_in'),
			// "warehouse_in"				=> $this->input->post('warehouse_in'),
			// "inventory_out"				=> $this->input->post('inventory_out'),
			// "qty_out"					=> $this->input->post('qty_out'),
			// "warehouse_out"				=> $this->input->post('warehouse_out')
		);
		//3.VALIDATION		
		//4.DB OPERATION
		//4.1.BEGIN TRANS
		
		$this -> db -> trans_begin();
		//4.2.RUN TRANS
		//$this->inventory_model->create_stock($data);
		$this->inventory_model->add_stock_conversion($data);
		//4.3.END TRANS	
		$this->sky->trans_end();
	}

	/*-----  End of  Stock Conversion  ------*/
}