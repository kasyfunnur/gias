<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report extends CI_Controller {	
	public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this->load->helper('url');
		$this->load->helper('swlayout');
		$this->load->helper('approval');
		$this->load->helper('swreport');
		$this->load->model('location_model');		
		$this->load->model('sales_order_model');
		$this->load->model('sales_delivery_model');
		$this->load->model('sales_invoice_model');
		$this->load->model('sales_return_model');
		$this->load->model('document_model');
		$this->load->model('inventory_model');
		$this->load->model('asset_model');
		$this->load->model('business_model');
		$this->load->model('status_model');
		$this->load->model('journal_model');
		$this->load->model('approval_model');
		$this->load->model('account_model');
		$this->load->model('location_model');
		$this->load->model('setting_model');
		$this->load->model('report_model');
	}
	public function filtering($code){
		$data['header'] = $this->report_model->get_report_header($code);
		$data['config'] = $this->report_model->get_report_config($code);		
		$this->sky->load_page("filter",$data);			
	}
	public function filter($type){
		$CI = &get_instance();
		$CI->config->load('sky_report_filter');
		$temp_config = $CI->config->item('report_filter');
		switch($type){
			case 'stock_balance':
				$data['type'] = 'stock_balance';
				$data['name'] = 'Stock Balance';
				$data['config']	= $temp_config['stock_balance'];				
				break;
			case 'stock_transaction':
				$data['type'] = 'stock_transaction';
				$data['name'] = 'Stock Card';
				$data['config']	= $temp_config['stock_transaction'];				
				break;
			case 'account_balance':
				$data['type'] = 'balance';
				$data['name'] = 'Balance Sheet';
				$data['config']	= $temp_config['account_balance'];
				break;
			case 'income':				
				$data['type'] = 'income';
				$data['name'] = 'Income Statement';
				$data['config']	= $temp_config['income'];
				break;
			case 'equity':
				$data['type'] = 'equity';
				$data['name'] = "Statement of Owner's Equity";
				$data['config']	= $temp_config['equity'];
				break;
			case 'cashflow':
				$data['type'] = 'cashflow';
				$data['name'] = "Statement of Cashflow";
				$data['config']	= $temp_config['equity'];
				break;
			case 'vend_aging':
				$data['type'] = 'vend_aging';
				$data['name'] = "Vendor Aging";
				$data['filter_date_range'] = false;
				$data['filter_date_asof'] = true;
				$data['filter_account'] = true;
				$data['filter_business'] = true;
				$data['ddl_business'] = $this->business_model->getVendor();
				break;
			case 'cust_aging':
				$data['type'] = 'cust_aging';
				$data['name'] = "Customer Aging";
				$data['filter_date_range'] = false;
				$data['filter_date_asof'] = true;
				$data['filter_account'] = true;
				$data['filter_business'] = true;
				$data['ddl_business'] = $this->business_model->getCustomer();
				break;
			default:
				break;			
		}		
		$this->sky->load_page("filter",$data);		
	}

	public function query($code){
		$data['setting'] = $this->setting_model->get_company_info();
		$data['header'] = $this->report_model->get_report_header($code);
		$data['config'] = $this->report_model->get_report_config($code);
		$data['results'] = $this->report_model->$code($_GET);
		$data['param'] = $_GET;

		$this->sky->load_page($data['header']['report_view'],$data);
	}

	/*public function query($type){
		switch($type){
			//INVENTORY
			case 'stock_balance':
				$data['path']	= 'inventory/'.$type;
				$data['type']	= 'stock_balance';
				$data['name'] 	= 'Stock Balance';

				$param1			= (is_array($_GET['filter_item']))?implode(',', $_GET['filter_item']):$_GET['filter_item'];
				$param2			= (is_array($_GET['filter_whse']))?implode(',', $_GET['filter_whse']):$_GET['filter_whse'];
				$param3			= $_GET['filter_date_to'];
				$data['param1']	= $param1;
				$data['param2']	= $param2;
				$data['param3']	= $param3;
				$data['date_from'] = "``";
				if(isset($_GET['filter_date_from'])){
					$data['date_from']	= $_GET['filter_date_from'];
				}
				$data['date_to']	= $_GET['filter_date_to'];				
				$query1 		= sprintf("
					select t0.item_id, t1.item_code, t1.item_name, t2.location_name, t0.whse_id, sum(item_qty) as 'balance_qty'
					from t_inventory_transaction t0
					inner join t_inventory t1 on t0.item_id = t1.item_id
					inner join t_site_location t2 on t2.location_id = t0.whse_id
					where t0.item_id in (%s) 
					and t0.whse_id in (%s)					
					and t0.doc_date > %s
					and t0.doc_date < '%s'
						group by t0.item_id, t0.whse_id
					order by t0.timestamp desc
				",$param1,$param2,$data['date_from'] || '',$_GET['filter_date_to']); 
				$data['results']	= $this->db->query($query1)->result_array();
				break;
			case 'stock_transaction':
				$data['path']	= 'inventory/'.$type;
				$data['type']	= 'stock_transaction';
				$data['name'] 	= 'Stock Card';

				//echo "this is get"; var_dump($_POST);

				$param1			= (is_array($_POST['filter_item']))?implode(',', $_POST['filter_item']):$_POST['filter_item'];
				$param2			= (is_array($_POST['filter_whse']))?implode(',', $_POST['filter_whse']):$_POST['filter_whse'];
				$param3			= $_POST['filter_date_to'];
				$data['param1']	= $param1;
				$data['param2']	= $param2;
				$data['param3']	= $param3;
				$data['date_from'] = "``";
				$temp_date_from = $_POST['filter_date_from'] || "";
				//echo $data['data_from'];	
				if(isset($_POST['filter_date_from'])){ 
					$data['date_from'] = $_POST['filter_date_from'];
					$temp_date_from = $_POST['filter_date_from'];
				}
				$data['date_to']	= $_POST['filter_date_to'];
				
				//echo "JMDebug";var_dump($_POST['filter_item']);
				//echo "JMDebug";var_dump($_POST['filter_whse']);
				
				$query1 		= sprintf("
					select * from t_inventory_transaction t0
					inner join t_inventory t1 on t0.item_id = t1.item_id
					inner join t_site_location t2 on t2.location_id = t0.whse_id
					where t0.item_id in (%s) 
					and t0.whse_id in (%s)
					and t0.doc_date > '%s'
					and t0.doc_date < '%s'
					order by t0.timestamp desc
				",$param1,$param2,$temp_date_from,$_POST['filter_date_to']);
				$data['results']	= $this->db->query($query1)->result_array();
				break;
			//FINANCE
			case 'balance':
				$data['path']	= 'finance/'.$type;
				$data['type'] = 'balance';
				$data['name'] = 'Balance Sheet';
				$data['data_ast'] = $this->account_model->getChild('10000');
				$data['data_lia'] = $this->account_model->getChild('20000');
				$data['data_equ'] = $this->account_model->getChild('30000');
				$data['ast'] 	  = $this->account_model->getBalance('10000');
				$data['lia'] 	  = $this->account_model->getBalance('20000');
				$data['equ'] 	  = $this->account_model->getBalance('30000');
				break;
			case 'income':		
				$data['path']	= 'finance/'.$type;
				$data['type'] = 'income';
				$data['name'] = 'Income Statement';
				$data['data_sal'] = $this->account_model->getChild('40000');
				$data['data_exp'] = $this->account_model->getChild('50000');
				$data['sal'] 	  = $this->account_model->getBalance('40000');
				$data['exp'] 	  = $this->account_model->getBalance('50000');				
				break;
			case 'equity':		
				$data['path']		= 'finance/'.$type;
				$data['type'] 		= 'equity';
				$data['name'] 		= "Statement of Owner's Equity";
				$data['data_equ'] 	= $this->account_model->getChild('30000');
				$data['equ'] 	  	= $this->account_model->getBalance('30000');			
				break;
			case 'cashflow':	
				$data['path']		= 'finance/'.$type;
				$data['type'] 		= 'cashflow';
				$data['name'] 		= "Statement of Cashflow";
				$data['data_cash'] 	= $this->account_model->getAll(array(array('is_cash',1)));
				$data['cash'] 	  	= $this->account_model->getBalance('10000');			
				break;
			//BUSINESS
			case 'vend_aging':	
				$data['path'] = 'business/'.$type;			
				$data['type'] = 'aging';
				$data['name'] = "Vendor Aging Statement";
				$data['business']	= $this->business_model->get_business_info(array('buss_id'=>$this->input->get('filter_business')));
				$data['datasource'] = $this->business_model->getAging($this->input->get('filter_business'));		
				break;
			case 'cust_aging':	
				$data['path'] = 'business/'.$type;			
				$data['type'] = 'aging';
				$data['name'] = "Customer Aging Statement";
				$data['business']	= $this->business_model->get_business_info(array('buss_id'=>$this->input->get('filter_business')));
				$data['datasource'] = $this->business_model->getAging($this->input->post('filter_business'));				
				break;
			default:
				break;			
		}
		$this->sky->load_page($data['path'],$data);
	}	*/
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */