<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Asset extends CI_Controller {	
	public function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this -> load -> model ('asset_model');
		$this -> load -> helper('swlayout_helper');
		}
	
	public function asset_list(){
		$this -> sky -> load_page ('asset_list');
		}
		
	public function asset_report($id=NULL){
		$data['asset_history'] = "";
		if($id) $data['asset_history'] = $this->asset_model->asset_track($id);
		$this->sky->load_page('asset_track',$data);	
	}
	
	public function asset_track($id=NULL){
		/*$data['asset_history'] = $this->asset_model->asset_track($id);
		$this -> sky -> load_page ('asset_track',$data);*/
		$data['asset_id']  = "";
		$data['asset_info'] = "";
		if(isset($_POST['search_serial'])){
			$data['asset_id'] = $this->asset_model->get_asset_id($_POST['search_serial']);	
			$data['asset_info'] = $this->asset_model->get_asset_info($data['asset_id']);
		}		
		//if($id) $data['asset_history'] = $this->asset_model->asset_track($id);
		$this->sky->load_page('asset_track',$data);	
	}
		
	
	public function asset_group(){
		$this -> sky -> load_page ('asset_group');
	}
	public function asset_sub_group($id=NULL){
		if($id != NULL){
			$data['data_asset_group'] = $this->asset_model->get_asset_sub_group($id);
			//$data['data_asset_group'] = $this->asset_model->get_asset_by_parent($id);
			$this -> sky -> load_page ('asset_sub_group',$data);
			//$this -> sky -> load_page ('asset_sub_group');
		//}else{
			//$this -> sky -> load_page ('asset_group');
		}
	}
	public function asset_sub_group_edit($id){
		$data['tag'] = $this->asset_model->get_asset_sub_group_tag($id);
		$data['group_info'] = $this->asset_model->get_asset_sub_group($id);
		
		if($_POST){
			$this->asset_model->edit_asset_group($_POST);	
			//var_dump($_POST);
			$this -> form_validation -> reset_post_data();
		}
		
		$this -> sky -> load_page ('asset_sub_group_edit',$data);
	}
	
	
		
	public function usage_list(){
		$this -> sky -> load_page ('usage_list');
		}		
	public function usage_monitor(){
		$this -> sky -> load_page ('usage_monitor');
		}
	
	public function usage_request(){
		$this -> load -> helper('document_helper');
		$location_id = $this -> session -> userdata('user_location_id');
		$data['site_location'] 	= $this -> asset_model -> get_site_location($location_id);
		
		if($_POST){
			$msg = $this -> asset_model -> add_usage_request($_POST);
			$this -> form_validation -> reset_post_data();
			}
		
		$this -> sky -> load_page ('usage_request',$data,(isset($msg) ? $msg : NULL));
		}
			
	public function new_asset(){
		$data['group_array'] 	= $this -> asset_model -> get_asset_group();
		$data['site_location'] 	= $this -> asset_model -> get_site_location();
		
		if($_POST){
			$msg = $this -> asset_model -> add_new_asset($_POST);
			$this -> form_validation -> reset_post_data();
		}
		
		$this -> sky -> load_page ('new_asset', $data, (isset($msg) ? $msg : NULL));
	}
	
	//jm
	/*public function new_group(){
		$data['group_array'] 	= $this -> asset_model -> get_asset_by_parent(0);
		if($_POST){
			if($_POST['radType'] == "0"){
				echo 'a';
				$msg = $this -> asset_model -> add_new_group($_POST);
			}else{
				echo 'b';
				$msg = $this -> asset_model -> add_new_sub_group($_POST);
			}
			$this -> form_validation -> reset_post_data();
		}
		$this -> sky -> load_page ('new_group', $data, (isset($msg) ? $msg : NULL));
	}*/
	public function new_group(){
		$data['group_array'] = $this->asset_model->get_asset_group();
		if($_POST){
			$msg = 	$this -> asset_model -> add_new_group($_POST);
			$this -> form_validation -> reset_post_data();
		}
		$this -> sky -> load_page ('new_group', $data, (isset($msg) ? $msg : NULL));
	}
		
	//ajax for datatables
	public function ajax_asset_list($return="json"){
		if  ($return == 'json') echo json_encode($this -> asset_model -> ajax_asset_list());
	}
	
	public function ajax_asset_history($return="json",$asset_id){
		//if  ($return == 'json') echo json_encode($this -> asset_model -> ajax_asset_history('',$asset_id));
		
		$table = 'datatables_demo';
		$primaryKey = 'id';
		$columns = array(
			array( 'db' => 'first_name', 'dt' => 0 ),
			array( 'db' => 'last_name',  'dt' => 1 ),
			array( 'db' => 'position',   'dt' => 2 ),
			array( 'db' => 'office',     'dt' => 3 ),
			array(
				'db'        => 'start_date',
				'dt'        => 4,
				'formatter' => function( $d, $row ) {
					return date( 'jS M y', strtotime($d));
				}
			),
			array(
				'db'        => 'salary',
				'dt'        => 5,
				'formatter' => function( $d, $row ) {
					return '$'.number_format($d);
				}
			)
		);
		$sql_details = array('user' => 'altus_admin','pass' => 'LyWkFQNjMH3ItafaDFsr','db'   => 'altus_erp','host' => 'localhost');
		require( 'ssp.class.php' );
		 
		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
	}
			
	public function ajax_group_list($return="json"){
		if  ($return == 'json') echo json_encode($this -> asset_model -> ajax_group_list());
	}
	public function ajax_sub_group_list($return="json",$group_id){		
		if 	($return == 'json') echo json_encode($this -> asset_model -> ajax_sub_group_list('',$group_id));		
	}
		
	public function ajax_usage_request($return="json"){
		if  ($return == 'json') echo json_encode($this -> asset_model -> ajax_usage_request());
		}
	public function ajax_usage_list($return="json"){
		if  ($return == 'json') echo json_encode($this -> asset_model -> ajax_usage_list());
		}
	public function ajax_usage_monitor($return="json"){
		if  ($return == 'json') echo json_encode($this -> asset_model -> ajax_usage_list('datatable_array',FALSE));
		}
	

	//empty pages
	public function maintenance_type(){
		$this -> sky -> load_page ('empty');
		}
	public function maintenance_schedule(){
		$this -> sky -> load_page ('empty');
		}
	public function maintenance_order(){
		$this -> sky -> load_page ('empty');
		}
	public function non_financial_tran(){
		$this -> sky -> load_page ('empty');
		}
	public function financial_tran(){
		$this -> sky -> load_page ('empty');
		}		
}