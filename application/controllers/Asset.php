<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Asset extends CI_Controller {	
	public function __construct() {
        parent::__construct();
		if (!$this->ion_auth->logged_in()) redirect('auth/login');
		$this -> load -> model ('asset_model');
		$this -> load -> helper('swlayout_helper');
		}
	
	public function asset_list(){
		$data['site_location'] 	= $this -> asset_model -> get_site_location();
		$this -> sky -> load_page ('asset_list',$data);
	}
	public function asset_group(){
		$data['group_array'] = $this->asset_model->get_asset_group();
		$this -> sky -> load_page ('asset_group',$data);
	}
	public function asset_group_edit($id){
		$data['tag'] 		= $this	->asset_model -> get_asset_tag($id);
		$data['group_info'] = $this	->asset_model -> get_asset_group($id);
		$this->load->model('account_model');
		$data['coa'] = $this->account_model->getAll(array(array(0=>'is_postable',1=>'1')));
		
		if($_POST){
			$this->asset_model->edit_asset_group($_POST);	
			//var_dump($_POST);
			$this -> form_validation -> reset_post_data();
		}
		$this -> sky -> load_page ('asset_group_edit',$data);
	}
	public function asset_track($id=NULL){
		$data['asset_history'] = $this->asset_model->asset_track($id);
		$this -> sky -> load_page ('asset_track',$data);
	}
	public function ajax_get_sub_group($group_id){
		echo json_encode($this->asset_model->ajax_get_sub_group($group_id));		
	}
	public function ajax_get_parent_group(){
		echo json_encode($this->asset_model->ajax_get_parent_group($_GET['ajax_group_id']));
	}
	
	
	public function usage_list(){
		$this -> sky -> load_page ('usage_list');
		}		
	public function usage_monitor(){
		$this -> sky -> load_page ('usage_monitor');
		}
	public function non_financial_trans(){
		$this -> sky -> load_page ('non_financial_trans');
		}		
	public function asset_view($asset_label){
		$this->load->helper('currency_helper');
		$asset_id = $this -> asset_model -> get_asset_id ($asset_label);
		$data['asset_data'] 	= $this -> asset_model -> get_asset_data($asset_id);
		$data['group_data'] 	= $this -> asset_model -> get_asset_group($data['asset_data']['asset_group_id']);
		$data['location_data'] 	= $this -> asset_model -> get_site_location($data['asset_data']['asset_location_id']);
		$data['asset_history'] 	= $this -> asset_model -> get_asset_history($asset_id);		
		$this -> sky -> load_page ('asset_view',$data);
		}	
	public function usage_request(){
		$this -> load -> helper('document_helper');
		$location_id = 1; //$this -> session -> userdata('user_location_id');
		$data['site_location'] 	= $this -> asset_model -> get_site_location($location_id);
		if($_POST){
			$msg = $this -> asset_model -> add_usage_request($_POST);
			$this -> form_validation -> reset_post_data();
			}
		$this -> sky -> load_page ('usage_request',$data,(isset($msg) ? $msg : NULL));
		}
	public function move_request(){
		$this -> load -> helper('document_helper');
		$data['site_location'] 	= $this -> asset_model -> get_site_location();	
			if($_POST){
			$msg = $this -> asset_model -> add_move_request($_POST);
			}
		$this -> sky -> load_page ('move_request',$data,isset($msg) ? $msg : NULL);
		}
	public function move_receipt($document_ref){
		if($_POST){
			$msg = $this -> asset_model -> move_asset($_POST);
			}
		$data ['doc_array'] =	$this -> asset_model -> get_trans_doc($document_ref);	
		$this -> sky -> load_page ('move_receipt',$data,isset($msg) ? $msg : NULL);
		}
	public function disposal_receipt($document_ref){
		if($_POST){
			$msg = $this -> asset_model -> dispose_asset($_POST);
			}
		$data ['doc_array'] =	$this -> asset_model -> get_trans_doc($document_ref);	
		$this -> sky -> load_page ('disposal_receipt',$data,isset($msg) ? $msg : NULL);
		}
	public function disposal_request(){
		$this -> load -> helper('document_helper');
		$data['site_location'] 	= $this -> asset_model -> get_site_location();	
			if($_POST){
			$msg = $this -> asset_model -> add_disposal_request($_POST);
			}
		
		$this -> sky -> load_page ('disposal_request',$data,isset($msg) ? $msg : NULL);
		}

	/*=========================================
	=            Depreciation Part            =
	=========================================*/
	
	public function depreciation(){
		$this -> load -> helper('document_helper');
		$data['site_location'] 	= $this -> asset_model -> get_site_location();
		
		if($_POST){
			$msg = $this -> asset_model -> add_depreciation($_POST);
			}
		
		$this -> sky -> load_page ('depreciation',$data,isset($msg) ? $msg : NULL);
		}

	public function get_asset_depre($depr_date = null)
	{
		if($depr_date == null){
			if($this->input->post('depr_date')){
				$depr_date = date('Y-m-d', strtotime($this->input->post('depr_date')));
			} else { 
				$depr_date = date('Y-m-d', time());
			}
		} else {
			$depr_date = date('Y-m-d', strtotime($depr_date));
		}

		$this->output->set_content_type('application/json');
		echo json_encode(array('data'=>$this -> asset_model -> get_asset_depre($depr_date)));
	
	}

	/*-----  End of Depreciation Part  ------*/



	public function new_asset(){
		$this->load->helper('currency_helper');
		$data['group_array'] 	= $this -> asset_model -> get_asset_group();
		$data['site_location'] 	= $this -> asset_model -> get_site_location();
		
		if($_POST){
			$msg = $this -> asset_model -> add_new_asset($_POST);
			$this -> form_validation -> reset_post_data();
		}
		
		$this -> sky -> load_page ('new_asset', $data, (isset($msg) ? $msg : NULL));
	}
	public function new_group(){
		$data['group_array'] = $this->asset_model->get_asset_group();
		$this->load->model('account_model');
		$data['coa'] = $this->account_model->getAll(array(array(0=>'is_postable',1=>'1')));
		if($_POST){
			$msg = 	$this -> asset_model -> add_new_group($_POST);
			$this -> form_validation -> reset_post_data();
		}
		$this -> sky -> load_page ('new_group', $data, (isset($msg) ? $msg : NULL));
	}
	public function ajax_datatable($view){
		echo json_encode ($this -> asset_model -> ajax_datatable($view));
		}
	
	
	//ajax functions
	public function get_next_label($group_id){
		echo $this -> asset_model -> get_asset_tag ($group_id,"next_label");
		}
		
	public function get_next_tag($parent_id){
		echo $this -> asset_model -> get_next_asset_group_tag ($parent_id);
		}
	
	public function get_maintenance_schedule($schedule_id = NULL,$type="json"){
		$data = $this -> asset_model -> get_maintenance_schedule ($schedule_id,1);
		echo ($type=="json") ? json_encode($data) : $data ; 
		}	
		
	//validation callback
	function is_valid_date($date)
	{
		$this->form_validation->set_message('is_valid_date', "Please enter a valid date.");
		
		$test_arr  = explode('/', $date);
		if (checkdate($test_arr[0], $test_arr[1], $test_arr[2])) {
			return true;
		} else
			return false;
		/* if(preg_match("/^(\d{2})-(\d{2})-(\d{4})$/", $date, $matches))
		{
	
			if(checkdate($matches[2], $matches[1], $matches[3]))
			{
				return true;
			} else return false;
		} else return false; */
	}
	
	function is_valid_location($location_id)
	{
		$this->form_validation->set_message('is_valid_location', "Location doesnt exist, please enter a valid location.");
		
		if($this -> asset_model -> get_site_location($location_id)) return TRUE;
		else return FALSE;
	}
	
	function is_valid_group($group_id)
	{
		$this->form_validation->set_message('is_valid_group', "Group doesnt exist, please enter a valid group.");
		
		if($this -> asset_model -> get_asset_group($group_id)) return TRUE;
		else return FALSE;
	}
	
	function is_valid_label($label)
	{
		$this->form_validation->set_message('is_valid_label', "Label is already used, please enter an unused label or let the system automatically attach a new label to the asset.");
		
		if($this -> asset_model -> get_asset_id($label)) return FALSE;
		else return TRUE;
	}
	//empty pages
	public function financial_trans(){
		//$this -> sky -> load_page ('empty'); // uninstalled module
		$this -> sky -> load_page ('financial_trans');
		}		
		
	public function maintenance_type(){
		$this -> sky -> load_page ('maintenance_type');
		}
		
	public function new_maintenance_type(){
		if($_POST){
			$msg = $this -> asset_model -> new_maintenance_type($_POST);
			if($msg == 'maint_type_added')$this -> form_validation -> reset_post_data();
		}
		$this -> sky -> load_page ('new_maintenance_type', NULL, (isset($msg) ? $msg : NULL));
	}
	
	public function maintenance_schedule(){
		$this -> sky -> load_page ('maintenance_schedule');
		}
		
	public function new_maintenance_schedule(){
		$data['maintenance_type'] 	= $this -> asset_model -> get_maintenance_type();
		$data['users'] 				= $this -> asset_model -> get_user();
		$this->form_validation->set_message('is_natural_no_zero', 'No asset has been added for scheduling, please add an asset to the list');
		if($_POST && $this->form_validation -> run()){
			$msg = $this -> asset_model -> new_maintenance_schedule($_POST);
			if($msg == 'maintenance_schedule_added')$this -> form_validation -> reset_post_data();
		}
		$this -> sky -> load_page ('new_maintenance_schedule', $data, (isset($msg) ? $msg : NULL));
	}
	
	public function maintenance_schedule_view($schedule_id = 0){
		$data['schedule_id'] = $schedule_id;
		$data['schedule_details'] = $this -> asset_model -> get_maintenance_schedule($schedule_id, TRUE);
		$this -> sky -> load_page ('maintenance_schedule_view', $data);
	}
	
	public function maintenance_order(){
		$this -> sky -> load_page ('maintenance_order');
	}
		
	public function new_maintenance_order(){
		$data['maintenance_type'] 		= $this -> asset_model -> get_maintenance_type();
		$data['users'] 					= $this -> asset_model -> get_user();
		$data['schedules'] 				= $this -> asset_model -> get_maintenance_schedule();
		$data['vendors'] 				= $this -> asset_model -> get_vendor();
		$this->form_validation->set_message('is_natural_no_zero', 'No asset has been added for maintenance order, please add an asset to the list');
		if($_POST && $this->form_validation -> run()){
			$msg = $this -> asset_model -> new_maintenance_order($_POST);
			if($msg == 'maintenance_order_added')$this -> form_validation -> reset_post_data();
		}
		$this -> sky -> load_page ('new_maintenance_order', $data, (isset($msg) ? $msg : NULL));
	}
	
	public function maintenance_reminder($group = 'today'){
		$data['users'] 		= $this -> asset_model -> get_user();
		$data['pic_id']		= $this -> session -> userdata('user_id');
		$data['group']		= $group;
		$this -> sky -> load_page ('maintenance_reminder', $data);
	}	
}