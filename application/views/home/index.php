<!-- Content Header (Page header) -->

<section class="content-header">
    <h1> Dashboard </h1>
</section>

<!-- Main content -->
<?php 
/*
	$data['total_assets']  = 10000;
	$data['total_incoming_assets']  = 10000;
	$data['total_breakdown_assets']  = 10000;
	$data['total_pending_approval']  = 10000;
	$data['total_location']  = 10000;
	$data['total_user']  = 10000;
	$data['total_pending_po']  = 10000;
	*/
?>



<!---<div id="swipeBox" ontouchstart="touchStart(event,'swipeBox');" ontouchend="touchEnd(event);" ontouchmove="touchMove(event);" ontouchcancel="touchCancel(event);">
	Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
</div>
<div id="swipeBox2" ontouchstart="touchStart(event,'swipeBox2');" ontouchend="touchEnd(event);" ontouchmove="touchMove(event);" ontouchcancel="touchCancel(event);">
	Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
    Test to swipe and Swipe to test.<br />
</div>--->
<script type="text/javascript">
function processingRoutine() {
	var swipedElement = document.getElementById(triggerElementID);
	if(swipedElement.id == "swipeBox"){
		if ( swipeDirection == 'left' ) {
			swipedElement.style.backgroundColor = 'grey';
		} else if ( swipeDirection == 'right' ) {
			swipedElement.style.backgroundColor = 'white';
		} else if ( swipeDirection == 'up' ) {
			swipedElement.style.backgroundColor = 'red';
		} else if ( swipeDirection == 'down' ) {
			swipedElement.style.backgroundColor = 'yellow';
		}
	}
	if(swipedElement.id == "swipeBox2"){
		if ( swipeDirection == 'left' ) {
			swipedElement.style.backgroundColor = 'red';
		} else if ( swipeDirection == 'right' ) {
			swipedElement.style.backgroundColor = 'blue';
		} else if ( swipeDirection == 'up' ) {
			swipedElement.style.backgroundColor = 'white';
		} else if ( swipeDirection == 'down' ) {
			swipedElement.style.backgroundColor = 'black';
		}
	}	
}
</script>




<section class="content"> 
    <?php if(isset($msg))echo $msg;?>
    <!-- Small boxes (Stat box) -->
    <div class="row"><div class="col-sm-12"><h4><strong>Master Data</strong></h4></div></div>
    <div class="row">
        <div class="col-lg-3 col-xs-6"> 
            <!-- small box -->
            <a href="<?php echo site_url('inventory/stock'); ?>">
            <div class="small-box bg-aqua">                
                <div class="inner"><h3> <?php echo $total_stock_count; ?> </h3><p> Qty On Hand</p></div>
                <div class="icon"> <i class="ion ion-bag"></i> </div>
                <div class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i> </div>                
			</div>
            </a> 
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6"> 
            <!-- small box -->
            <a href="<?php echo site_url('inventory/style'); ?>">            
            <div class="small-box bg-yellow">
                <div class="inner"><h3> <?php echo $total_style; ?> </h3> <p> Style  </p></div>
                <div class="icon"> <i class="ion ion-alert-circled"></i> </div>
                <div class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i></div>
			</div>
			</a>            
        </div>
        <!-- ./col -->        
        <div class="col-lg-3 col-xs-6"> 
            <!-- small box -->
            <a href="<?php echo site_url('business/partner'); ?>">            
            <div class="small-box bg-purple">
                <div class="inner"><h3> <?php echo $total_business; ?> </h3> <p> Customers/Vendors</p></div>
                <div class="icon"> <i class="ion ion-person-stalker"></i> </div>
                <div class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i></div>
			</div>
			</a>            
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6"> 
            <!-- small box -->
            <a href="<?php echo site_url('inventory/warehouse'); ?>">            
            <div class="small-box bg-teal">
                <div class="inner"><h3> <?php echo $total_warehouse; ?> </h3> <p> Warehouses </p></div>
                <div class="icon"> <i class="ion ion-home"></i> </div>
                <div class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i></div>
			</div>
			</a>            
        </div>
        <!-- ./col -->        
    </div>
    <!-- /.row --> 
    
    <div class="row"><div class="col-sm-12"><h4><strong>Transaction</strong></h4></div></div>
    <!-- 2nd row -->
    <div class="row">
        <div class="col-lg-3 col-xs-6"> 
            <!-- small box -->
            <a href="<?php echo site_url('purchase/purchase_order'); ?>">            
            <div class="small-box bg-blue">
                <div class="inner"><h3> <?php echo $total_purchase; ?> </h3> <p> Purchase </p></div>
                <div class="icon"> <i class="ion ion-ios7-cart"></i> </div>
                <div class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i></div>
			</div>
			</a>            
        </div>
        <!-- ./col --> 
        <div class="col-lg-3 col-xs-6"> 
            <!-- small box -->
            <a href="<?php echo site_url('purchase/purchase_invoice'); ?>">            
            <div class="small-box bg-red">
                <div class="inner"><h3> <?php echo $total_collection; ?> </h3> <p> Due Payment </p></div>
                <div class="icon"> <i class="ion ion-clipboard"></i> </div>
                <div class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i></div>
			</div>
			</a>            
        </div> 
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6"> 
            <!-- small box -->
            <a href="<?php echo site_url('sales/sales_order'); ?>">            
            <div class="small-box bg-green">
                <div class="inner"><h3> <?php echo $total_new_order; ?> </h3> <p> Pending Sales </p></div>
                <div class="icon"> <i class="ion ion-ios7-download"></i> </div>
                <div class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i></div>
			</div>
			</a>            
        </div>                
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6"> 
            <!-- small box -->
            <a href="<?php echo site_url('sales/sales_invoice'); ?>">            
            <div class="small-box bg-lime">
                <div class="inner"><h3> <?php echo $total_collection; ?> </h3> <p> Collection </p></div>
                <div class="icon"> <i class="ion ion-cash"></i> </div>
                <div class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i></div>
			</div>
			</a>            
        </div>        
        
    </div>
    <div class="row">
    <!-- ./col -->
    	<div class="col-lg-3 col-xs-6"> 
            <!-- small box -->
            <a href="<?php echo site_url('asset/asset_list'); ?>">            
            <div class="small-box bg-maroon">
                <div class="inner"><h3> <?php echo $total_consignment; ?> </h3> <p> Consignment </p></div>
                <div class="icon"> <i class="ion ion-android-hand"></i> </div>
                <div class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i></div>
			</div>
			</a>            
        </div> 
    </div>


    <div class="row"><div class="col-sm-12"><h4><strong>Financials</strong></h4></div></div>

    <!-- chart -->
    <div class="row">
    <!-- ./col -->
        <div class="col-lg-6 col-xs-6"> 
            <!-- Bar chart -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="fa fa-bar-chart-o"></i>
                  <h3 class="box-title">Sales By Month</h3>
                </div>
                <div class="box-body">
                  <div id="bar-chart" style="height: 300px;"></div>
                </div><!-- /.box-body-->
              </div><!-- /.box -->         
        </div> 
    </div>

    <!-- ./col -->
    </div>
</section>
<!-- /.content -->