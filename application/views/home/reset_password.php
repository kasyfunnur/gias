<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> 
		Reset Password </h1>
</section>

<!-- Main content -->
<section class="content">
	<!---<h3>My Profile</h3>--->
	<?php
 if(isset($msg)){
	 echo $msg;
 }
 ?>
	
			<div class="tab-pane" id="reset_pwd">
				<?php
				$attributes = array(
					'class' 	=> 'form-horizontal',
					'role'		=> 'form',
					'method' 	=> 'post',
					'name'		=> 'form_reset_pwd', 
					'id' 		=> 'form_reset_pwd',
					'action'	=> site_url('home/profile')
					);
				echo form_open('', $attributes);
			?>
				<input type="hidden" name="form_type" value="reset-pwd">
				<input type="hidden" name="hidden_user_id" value="<?php echo $this->session->userdata('user_id'); ?>">
				<div class="form-group">
					<label class="control-label col-md-3" for="cur_pwd"><?php echo lang('current_password'); ?></label>
					<div class="col-md-4">
						<input type="password" name="cur_pwd" id="cur_pwd">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3" for="cur_pwd"><?php echo lang('enter_new_password');?></label>
					<div class="col-md-4">
						<input type="password" name="new_pwd" id="new_pwd">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3" for="cur_pwd"><?php echo lang('retype_new_password');?></label>
					<div class="col-md-4">
						<input type="password" name="new_pwd2" id="new_pwd2">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-6">
						<button class="btn btn-primary btn-flat" type="submit">Update</button>
					</div>
				</div>
				</form>
			</div>
	
</section>
<!-- /.content -->