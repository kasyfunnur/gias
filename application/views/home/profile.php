<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> <a href="<?php echo site_url();?>"> Home </a> &nbsp;<i class="fa fa-angle-right"></i>&nbsp;
		User Profile </h1>
</section>

<!-- Main content -->
<section class="content">
	<h3>My Profile</h3>
	<?php
 if(isset($msg)){
	 echo $msg;
 }
 ?>
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#user_settings" data-toggle="tab"><?php echo lang('user_settings');?></a></li>
			<li><a href="#reset_pwd" data-toggle="tab"><?php echo lang('reset_password');?></a></li>
		</ul>
		<div class="tab-content"> 
			<!-- User Settings Tab -->
			<div class="tab-pane active" id="user_settings">
				<?php
				$attributes = array(
					'class' 	=> 'form-horizontal',
					'role'		=> 'form',
					'method' 	=> 'post',
					'name'		=> 'frm_user_settings', 
					'id' 		=> 'frm_user_settings',
					'action'	=> site_url('home/profile')
					);
				echo form_open('', $attributes);
			?>
				<input type="hidden" name="form_type" value="update-user-settings">
				<input type="hidden" name="hidden_user_id" value="<?php echo $this->session->userdata('user_id'); ?>">
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo lang('full_name');?></label>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="full_name" value="<?php echo htmlspecialchars($this->session->userdata('full_name'))?>">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2"><?php echo lang('default_language'); ?> :</label>
					<div class="col-sm-6">
						<?php $current_lang = $this -> session -> userdata('user_lang');?>
						<select class="form-control" name="sel_lang">
							<option value="english" <?php if($current_lang == 'english') echo 'selected'; ?>>English</option>
							<option value="indonesian" <?php if($current_lang == 'indonesian') echo 'selected'; ?>>Indonesian</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-6">
						<button class="btn btn-primary btn-flat" type="submit">Update</button>
					</div>
				</div>
				</form>
			</div>
			
			<!-- Reset Password Tab -->
			<div class="tab-pane" id="reset_pwd">
				<?php
				$attributes = array(
					'class' 	=> 'form-horizontal',
					'role'		=> 'form',
					'method' 	=> 'post',
					'name'		=> 'form_reset_pwd', 
					'id' 		=> 'form_reset_pwd',
					'action'	=> site_url('home/profile')
					);
				echo form_open('', $attributes);
			?>
				<input type="hidden" name="form_type" value="reset-pwd">
				<input type="hidden" name="hidden_user_id" value="<?php echo $this->session->userdata('user_id'); ?>">
				<div class="form-group">
					<label class="control-label col-md-3" for="cur_pwd"><?php echo lang('current_password'); ?></label>
					<div class="col-md-4">
						<input type="password" name="cur_pwd" id="cur_pwd">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3" for="cur_pwd"><?php echo lang('enter_new_password');?></label>
					<div class="col-md-4">
						<input type="password" name="new_pwd" id="new_pwd">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3" for="cur_pwd"><?php echo lang('retype_new_password');?></label>
					<div class="col-md-4">
						<input type="password" name="new_pwd2" id="new_pwd2">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-6">
						<button class="btn btn-primary btn-flat" type="submit">Update</button>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->