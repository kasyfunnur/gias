<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Sky ERP</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?php echo base_url('favicon.ico') ; ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url('favicon.ico') ; ?>" type="image/x-icon">
    <?php 
      echo '<link href="'.base_url( 'assets/css/bootstrap.css'). '" rel="stylesheet" media="screen">';
      echo '<link href="'.base_url( 'assets/css/font-awesome.min.css'). '" rel="stylesheet" type="text/css">';
      echo '<link href="'.base_url( 'assets/css/ionicons.min.css'). '" rel="stylesheet" type="text/css">';
      echo '<link href="'.base_url( 'assets/css/morris.css').'" rel="stylesheet" type="text/css">';
      echo '<link href="'.base_url( 'assets/css/AdminLTE.css'). '" rel="stylesheet" type="text/css">';
      echo '<link href="'.base_url( 'assets/css/skins/skin-blue.min.css'). '" rel="stylesheet" type="text/css">';
      echo '<link href="'.base_url( 'assets/css/dataTables.bootstrap.css'). '" rel="stylesheet" type="text/css">';
      echo '<link href="'.base_url( 'assets/css/skyware.css'). '" rel="stylesheet" type="text/css">';
      echo '<link href="'.base_url( 'assets/css/typeahead.css'). '" rel="stylesheet" type="text/css">';
      echo '<link href="'.base_url( 'assets/css/bootstrap-flat.min.css'). '" rel="stylesheet" type="text/css">';
      echo '<link href="'.base_url( 'assets/css/bootstrap-flat-extras.min.css'). '" rel="stylesheet" type="text/css">';
    ?>
</head>

<body class="skin-blue">
    <div class="wrapper">
        <!-- header logo: style can be found in header.less -->
        <header class="main-header">
            <a href="<?php echo site_url(); ?>" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <!-- <img class="icon pull-left" src="<?php echo base_url('assets/img/kagumlogo.png'); ?>" height="45" style="margin-top:8px;"> -->
                <i class="fa fa-home"></i> Sky ERP
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation" style="height:50px;">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- support help desk link -->
                        <li class="messages-menu">
                            <a href="http://erphelp.digitalbuana.com" target="_blank" title="Helpdesk">
                                <i class="fa fa-question-circle"></i>
                            </a>
                        </li>
                        <!-- approval drop down -->
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Approvals"> <i class="fa fa-envelope"></i>
                                <?php if (isset($approval_count) && $approval_count> 0 ) { ?>
                                <span class="label label-danger" style="font-size:12px;"> <?php echo $approval_count ; ?></span>
                                <?php }; ?>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">
                                    <h4>You have <strong><?php echo $approval_count ; ?></strong> pending approvals</h4></li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <?php foreach ( $pending_approval as $app ){ echo "<li><a href='" . site_url( "approval/approval_list/$app[appr_id]") . "'>
              <h4><span class='badge'>$app[approval_count]</span>&nbsp;&nbsp;$app[appr_name]</h4></a></li>" ; } ?>
                                    </ul>
                                </li>
                                <li class="footer"><a href="<?php echo site_url('approval/approval_list');?>">See All Messages</a></li>
                            </ul>
                        </li>
                        <!-- end of approval drop down -->
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-user"></i> <span><?php if (isset($user_data['first_name'])) echo $user_data['first_name'] ;?>
          <i class="caret"></i></span> </a>
                            <ul class="dropdown-menu">
                                <li class="user-footer bg-light-blue">
                                    <p>
                                        <?php if (isset($user_data[ 'first_name'])) echo $user_data[ 'first_name'] ; if (isset($user_data[ 'designation']) && $user_data[ 'designation'] !='' ) echo ", {$user_data['designation']}"; ;?> </p>
                                </li>
                                <li class="user-footer text-right">
                                    <span class="pull-left"><a href="<?php echo base_url('home/profile'); ?>" class="btn btn-default btn-flat">Profile</a></span>
                                    <span class="pull-right"><a href="<?php echo base_url('auth/logout'); ?>" class="btn btn-default btn-flat">Sign out</a></span>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu" id="sidebar">
                    <?php echo $side_menu ?>
                </ul>
            </section>
            <br>
        </aside>        
        <aside class="content-wrapper">
