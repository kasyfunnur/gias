</aside><!-- /.right-side -->
</div><!-- ./wrapper -->
<?php 
$controller = $this -> router -> fetch_class();
$method 	= $this -> router -> fetch_method();


echo '<script aysnc="false" src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/bootstrap.min.js').'"></script>';

echo '<script src="'.base_url('assets/js/jquery.filestyle.bootstrap.js').'"></script>';
/*echo '<script src="'.base_url('assets/js/admin_app.js').'"></script>';*/
echo '<script src="'.base_url('assets/js/app.js').'"></script>';
echo '<script src="'.base_url('assets/js/jquery.dataTables.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/dataTables.bootstrap.js').'"></script>';
echo '<script src="'.base_url('assets/js/jquery.inputmask.js').'"></script>';
echo '<script src="'.base_url('assets/js/jquery.inputmask.date.extensions.js').'"></script>';
echo '<script src="'.base_url('assets/js/jquery.inputmask.numeric.extensions.js').'"></script>';
echo '<script src="'.base_url('assets/js/general.js').'"></script>';
echo '<script src="'.base_url('assets/js/colResizable-1.3.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/accounting.min.js').'"></script>';

echo '<script src="'.base_url('assets/js/swipe.js').'"></script>';
echo '<script src="'.base_url('assets/js/timeago.js').'"></script>';
echo '<script src="'.base_url('assets/js/bloodhound.js').'"></script>';
echo '<script src="'.base_url('assets/js/typeahead.bundle.js').'"></script>';

echo '<script src="'.base_url('assets/js/raphael-min.js').'"></script>';
echo '<script src="'.base_url('assets/js/morris.min.js').'"></script>';

// charting
echo '<script src="'.base_url('assets/js/flot/jquery.flot.min.js').'"></script>';
//echo '<script src="'.base_url('assets/js/flot/jquery.flot.resize.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/flot/jquery.flot.categories.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/jquery.bootstrap-growl.min.js').'"></script>';
if (file_exists ("assets/js/page/{$controller}_{$method}.js")){
	echo '<script src="'.base_url("assets/js/page/{$controller}_{$method}.js").'"></script>';
}

/*echo "<script>
		if(!table){
		var table = $('#{$method}').dataTable({'ajax': 'ajax_datatable/{$method}'}); };
	</script>"; //universal datatable line -> make sure method name and table name is the same
*/
if( $this->session->flashdata('flash_notification')){ ?>
<script>
	$(function() {
    	$.bootstrapGrowl("<?php echo $this->session->flashdata('flash_notification') ?>");
    });
</script>
<?php };
?>

<script type="text/javascript">
function validateForm(){
	if(confirm("Are you sure?")==true){		
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}
</script>
</body>
</html>