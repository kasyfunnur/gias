<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<style>
table.printHeader{
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	border: none 1px #FFF;
}
</style>
<?php
/* echo '<script src="'.base_url('assets/js/jquery-1.9.1.min.js').'"></script>'; */
echo '<script src="'.base_url('assets/js/jquery-2.0.3.js').'"></script>';

/* echo '<script src="'.base_url('assets/js/bootstrap.min.js').'"></script>'; */
/* echo '<script src="'.base_url('assets/js/general.js').'"></script>'; */

echo '<link href="'.base_url('assets/css/bootstrap.min.css').'" rel="stylesheet">';
/*echo '<link href="'.base_url('assets/css/general.css').'" rel="stylesheet" media="screen">';*/

// print style
echo '<link href="'.base_url('assets/css/print.css').'" rel="stylesheet">';
echo '<link href="'.base_url('assets/css/bootstrap-flat.min.css').'" rel="stylesheet" type="text/css">';
echo '<link href="'.base_url('assets/css/bootstrap-flat-extras.min.css').'" rel="stylesheet" type="text/css">';
?>
<title>Skyware Solutions&trade; - Print <?php echo $title ?></title>
</head>
<body>
<br>
<!---<div class="no-print printbox">
	<a class="btn" onClick="window.print();"><i class="icon-print"></i>&nbsp;Print</a>
</div>--->
<div class="no-print printbox pull-right">
	<a class="btn" onClick="window.print();"><i class="icon-print"></i>&nbsp;Print</a>
</div>

<div class="container col-sm-6">
<table class="printHeader" width="100%">
	<tr>
		<td valign="center" align="center" width="120">
            <img src="<?php echo base_url("/assets/img/company-logo.png");?>" width="180">
        </td>
		<td valign="top" align="left" width="280"><h3 id="comp_name"><!---FarisFab---></h3>
			<!---<small>            <br>
            Bandung, Jawa Barat<br>
            Indonesia - 41084
            Tlp: +6287 877 234 878
            </small>--->
		</td>
		<td valign="top" align="right"><h3 id="title"><?php echo $title ?></h3><h4 id="subtitle"><?php echo $subtitle ?></h4></td>
	</tr>
</table>
<hr>