<section class="content-header">
  	<h1> Sales Order <a href="<?php echo site_url('sales/sales_order_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<span class="hidden-xs">
        		<i class="fa fa-plus"></i>&nbsp;&nbsp;New Order
            </span>
        	<span class="visible-xs">
        		<i class="fa fa-plus"></i>
            </span>
        </button>
    </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<section class="content">
<div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body table-responsive">
  	<table class="table table-hover">
    	<thead>
        	<tr>
            	<!---<th class="hidden-xs">#</th>--->
                <th style="width:20%">Document</th>
                <th style="width:50%">Note</th>
                <th style="width:30%">Actions</th>
            </tr>
        </thead>
        <tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>
			<?php 
				$approval_status = $this->status_model->status_label('approval',$datatable[$x]['approval_status']);
                $document_status = $this->status_model->status_label('document',$datatable[$x]['doc_status']);
				$logistic_status = $this->status_model->status_label('logistic',$datatable[$x]['logistic_status']);
				$billing_status = $this->status_model->status_label('billing',$datatable[$x]['billing_status']);
				$finance_status = $this->status_model->status_label('financial',$datatable[$x]['financial_status']);
            ?>
            <tr>
            	<!---<td class="hidden-xs"><?php echo $x+1;?>.</td>--->
                <td>
					<span style="font-size:16px;font-weight:lighter !important;color:#888">
                        <a href="<?php echo site_url('sales/sales_order_edit');?>/<?php echo $datatable[$x]['doc_num'];?>">
                            <?php echo $datatable[$x]['doc_num'];?>
                        </a> 
                        - <?php echo $datatable[$x]['buss_name'];?> <?php //echo $document_status;?>                        
                    </span>
                    <br />
                    <span style="font-size:12px;font-weight:lighter !important;color:#888">
                        By <?php echo $datatable[$x]['create_by'];?>
                        - <?php echo date("d M, Y", strtotime($datatable[$x]['doc_dt']));?>
                    </span>
                </td>
                <!---<td><?php echo date("Y-m-d", strtotime($datatable[$x]['doc_dt']));?></td>--->
                <!---<td><?php echo $datatable[$x]['buss_name'];?></td>--->
                <!---<td><?php echo $approval_status;?></td>--->
                <!---<td>
					<?php //if($datatable[$x]['doc_status'] != "ACTIVE") 
						//echo $document_status;						
					?>
                    <?php echo $logistic_status;?>
                </td>--->
                
                <?php 
					$link_receipt = "#"; 
					$link_add = '<span class="btn btn-xs btn-default disabled" style="color:#fcfcfc"><i class="fa fa-plus"></i></span>';;
					$disable = 'disabled';
					$link_cancel = base_url('sales/sales_order_cancel')."/".$datatable[$x]['doc_num'];
					$link_close = base_url('sales/sales_order_close')."/".$datatable[$x]['doc_num'];
					$link_print = base_url('sales/print_so')."/".$datatable[$x]['doc_num'];
					if($datatable[$x]['logistic_status'] != "DELIVERED" 
						&& $datatable[$x]['logistic_status'] != "PARTIAL" 
						&& $datatable[$x]['doc_status'] != "CANCELLED"
						&& $datatable[$x]['doc_status'] != "CLOSED"){						
						$disable = '';
					} 
					
					if($datatable[$x]['logistic_status'] != "DELIVERED"){
						$link_receipt = site_url('sales/sales_delivery_add')."/".$datatable[$x]['doc_num'];
						$link_add = '<span class="btn btn-xs btn-default"><i class="fa fa-plus"></i></span>';
					}
					
					if($datatable[$x]['doc_status'] != "ACTIVE"){
						$link_receipt = '#';
						$link_add = '<span class="btn btn-xs btn-default disabled" style="color:#fcfcfc"><i class="fa fa-plus"></i></span>';;
					}
					
				?>
                <!---<td> 
                	<a href="<?php echo $link_receipt;?>">
                    <?php echo $link_add;?>
                    </a>&nbsp;&nbsp;
                </td>--->
                <!---<td><?php echo $billing_status;?></td>--->
                <td>
                	<?php echo $logistic_status;?>
                	<?php echo $datatable[$x]['doc_note'];?>
                </td>
                <td> 
                	<a href="<?php echo $link_cancel;?>" class="btn btn-danger <?php echo $disable;?>">
                        <i class="fa fa-ban"></i> Cancel</a>&nbsp;&nbsp;
					<a href="<?php echo $link_close;?>" class="btn btn-warning <?php echo $disable;?>">
                        <i class="fa fa-archive"></i> Close</a>&nbsp;&nbsp;
                    <!---<a href="<?php echo $link_print?>" class="btn btn-success" target="_blank">
                        <i class="fa fa-print"></i> Print</a>--->
                    <a href="<?php echo $link_receipt?>" class="btn btn-info <?php echo $disable;?>" target="_blank">
                        <i class="fa fa-plus"></i> Delivery</a>
                	<!---<a href="<?php echo $link_receipt;?>">                    
                    	<span class="btn btn-xs btn-default <?php echo $disable;?>" >
                        <i class="fa fa-plus"></i> Delivery </span></a>&nbsp;&nbsp;--->
                	<!---<a href="<?php echo base_url('sales/sales_order_cancel')."/".$datatable[$x]['doc_num'];?>">                    
                    	<span class="btn btn-xs btn-default <?php echo $disable;?>" >
                        <i class="fa fa-ban"></i> Cancel</span></a>&nbsp;&nbsp;
					<a href="<?php echo base_url('sales/sales_order_close')."/".$datatable[$x]['doc_num'];?>">
                    	<span class="btn btn-xs btn-default <?php echo $disable;?>" >
                        <i class="fa fa-archive"></i> Close</span></a>&nbsp;&nbsp;
                    <a href="<?php echo base_url('sales/print_so')."/".$datatable[$x]['doc_num'];?>">
                    	<span class="btn btn-xs btn-default" >
                        <i class="fa fa-print"></i> Print</span></a>&nbsp;&nbsp;--->
                        
                        
                    <!---<div class="btn-group">
                      	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        	Send To <span class="caret"></span>
                      	</button>
                      	<ul class="dropdown-menu" role="menu">                            
                            <li>
                            	<a href="<?php echo $link_receipt;?>">
                                    <i class="fa fa-plus"></i> Delivery</a>
                            </li>
                      	</ul>
                    </div>
					<div class="btn-group">
                      	<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                        	Document <span class="caret"></span>
                      	</button>
                      	<ul class="dropdown-menu" role="menu">
                            <li class="<?php echo $disable;?>">
                            	<?php if($disable == ''){ ?>
	                            	<a href="<?php echo $link_cancel;?>"><i class="fa fa-ban"></i> Cancel</a>
                                <?php }else{ ?>
    	                            <a><i class="fa fa-ban"></i> Cancel</a>
                                <?php }?>
                        	</li>
                            <li class="<?php echo $disable;?>">
                            	<?php if($disable == ''){ ?>
	                            	<a href="<?php echo $link_close;?>"><i class="fa fa-archive"></i> Close</a>
                                <?php }else{ ?>
    	                            <a><i class="fa fa-archive"></i> Close</a>
                                <?php }?>
                            </li>
                            <li class="divider"></li>
                            <li>
                            	<a href="<?php echo base_url('sales/print_so')."/".$datatable[$x]['doc_num'];?>">
                                    <i class="fa fa-print"></i> Print</a>
                            </li>
                      	</ul>
                    </div>--->
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table></div>
    </div>
</section>
<!-- /.content -->
