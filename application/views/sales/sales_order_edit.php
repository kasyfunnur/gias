<!-- Content Header (Page header) -->
<style>
	.inactive td{background-color:#CCC;opacity: 0.5;}
	thead>tr>th{text-align:center;font-weight:bold}
	#foot_label{text-align:right;}
	/*tbody{padding-left:1px;padding-right:1px;}opacity: 0.5;*/
</style>
<?php // STATUS MODEL
	//$status_attr = $this->purchase_order_model->logistic_status($form_header[0]['logistic_status']);
	$logistic_status = $this->status_model->status_icon('logistic',$form_header[0]['logistic_status']);
	$billing_status = $this->status_model->status_icon('billing',$form_header[0]['billing_status']);
	$finance_status = $this->status_model->status_icon('financial',$form_header[0]['financial_status']);
?>

<section class="content-header">
    <h1><a class="" href="<?php echo site_url('sales/sales_order');?>">Sales Order</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Edit </h1>   
</section>

<!-- Main content -->
<section class="content">	
    <?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
    <div class="button_action pull-right"></div>
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <h4><?php echo $logistic_status;?>
            <?php echo $billing_status;?>
            <?php //echo $finance_status;?></h4>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-6">
        	<!---<div class="form-group">
            	<label class="col-sm-3">Status</label>
                <div class="col-sm-9">
					<?php echo $logistic_status;?>
                    <?php echo $billing_status;?>
                    <?php echo $finance_status;?>
                </div>
            </div>--->
            <div class="form-group">
                <label class="col-sm-3" for="buss_name">
                	Customer                   
                </label>
                <div class="col-sm-9">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" readonly="readonly"		
                        	value="<?php echo $form_header[0]['buss_name'];?>">
                        <input type="hidden" id="buss_id" name="buss_id" value="<?php echo $form_header[0]['buss_id'];?>">                   	
                        <span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_customer" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                    
                </div>
            </div>
            <!---<div class="form-group">
                <label class="col-sm-3" for="doc_ref">Doc Ref</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_ref" name="doc_ref" >
                </div>
            </div>--->
            
            <div class="form-group">
                <label class="col-sm-3" for="cust_phone1">Customer Phone</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" id="cust_phone1" name="cust_phone1" placeholder="Phone 1" 
                    	value="<?php echo $form_header[0]['doc_buss_tlp1'];?>">
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-4">
					<input type="text" class="form-control input-sm" id="cust_phone2" name="cust_phone2" placeholder="Phone 2"
                    	value="<?php echo $form_header[0]['doc_buss_tlp2'];?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="ship_addr">Customer Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr"><?php echo $form_header[0]['ship_addr'];?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"><?php echo $form_header[0]['doc_note'];?></textarea>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $form_header[0]['doc_num'];?>" readonly>
                    <input type="hidden" id="doc_id" name="doc_id" value="<?php echo $form_header[0]['doc_id'];?>" />
                </div>
            </div>	
        	<div class="form-group">
            	<!---<div class="row">--->
                    <label class="col-sm-3" for="doc_dt">Date</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo substr($form_header[0]['doc_dt'],0,10);?>">
                    </div>
				<!---</div>--->
            </div>		
            <div class="form-group">
            	<!---<div class="row">--->
                    <label class="col-sm-3" for="doc_ddt">Date Required</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo substr($form_header[0]['doc_ddt'],0,10);?>">
                    </div>
				<!---</div>--->
            </div>		
			<div class="form-group">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name', $form_header[0]['whse_id']);?>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-12">
	        <div class="table-responsive">
        	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
            <thead>
            <tr>
            	<th style="width:5%">#</th>
                <th style="width:20%">
                	<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                        <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Item Code</span>
						<span class="visible-xs"><i class="fa fa-search"></i></span>
                    </button>
                </th>
                <th style="width:20%">Item Name</th>
                <th style="width:10%">Quantity</th>
                <th style="width:15%">Price</th>
                <th style="width:20%">Total</th>
                <th style="width:10%" class="action">Action</th>            
            </tr>
            </thead>
            <tbody>
            <tr height="25px" id="default">
	            <input type="hidden" name="item_id[]" id="item_id1" class="form-control">
            	<td style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                </td>
                <td>
                	<div class="input-group input-group-xs">                    	
	                    <span class="input-group-btn ">
                            <a href="#" class="btn btn-warning form-control" name="a_ast_code" target="_blank">
                            	<i class="fa fa-external-link"></i>
                            </a>                            
						</span>
                        <input type="text" name="ast_code[]" id="ast_code1" class="form-control " readonly="readonly">
                    </div>
                	<!---<input type="hidden" name="ast_code[]" id="ast_code1" class="form-control" readonly="readonly">
                    <a href="#" target="_blank" class="form-control" style="text-decoration:underline;"> </a>--->
                </td>
                <td>
                	<input type="text" name="ast_name[]" id="ast_name1" class="form-control">
                </td>
                <td>
                	<input type="number" name="ast_qty[]"  id="ast_qty1"  class="form-control" style="text-align:right">
                </td>
                <td>
                	<input type="text" name="ast_price[]"  id="ast_price1"  class="form-control" style="text-align:right">
                </td>
                <td>
                	<input type="text" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right"readonly>
                </td>
                <td>
                	<div class="input-group">
						<div class='input-group-btn'>
                            <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                            	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
						</div>
					</div>
				</td>                
            </tr>            
            </tbody>
            <tfoot>
            	<!---<tr>
                	<td class="hidden-xs col-sm-1" style="text-align:center"></td>
                    <td class="hidden-xs col-sm-2"></td>
                    <td class="col-sm-2 col-xs-3"></td>
                    <td class="col-sm-1 col-xs-1"></td>
                    <td class="input-group col-sm-2 col-xs-2" id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                    <td class="input-group col-sm-2 col-xs-3"><div>
                    	<input type="text" class="form-control" id="doc_total" name="doc_total" value="0" style="text-align:right;" readonly>
                        </div>
                    </td>
                </tr>--->
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><div>
                    <input type="number" class="form-control" id="qty_total" name="qty_total" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
                <td id="foot_label"><strong>Sub Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_subtotal" name="doc_subtotal" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td id="foot_label" style="text-align:right;">
                	<input type="number" class="col-sm-5" id="doc_disc_pct" name="doc_disc_pct"
                    	value="0" style="text-align:right;" />%
                	<strong>Discount:&nbsp;&nbsp;&nbsp;</strong>
                </td>
                <td><div>
                    <input type="text" class="form-control" id="doc_disc" name="doc_disc" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_total" name="doc_total" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            </tfoot>
            </table>
            </div>
    	</div>
    </div>
    <br />
    <div class="button_action pull-right"></div>
    <br /><br /><br />
	</form>	
</section>


<div class="pull-right" id="button_action" style="display:none;">
	<?php $link_print = base_url('sales/print_so')."/".$form_header[0]['doc_num'];?>
    <a href="<?php echo $link_print?>" class="btn btn-flat btn-success" target="_blank">
        <i class="fa fa-print"></i> Print</a>
    &nbsp;
    <button type="submit" class="btn btn-flat btn-info">
        <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
</div>
<!-- /.content -->

<?php //LAYOUT HELPER: MODAL
	echo sw_createModal('modal_customer','Select Customer','dialog_customer_push_data();','business/dialog_customer');
	echo sw_createModal('modal_item','Select Item','dialog_inventory_push_data();','inventory/dialog_inventory');
?>

<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
var source_customer = <?php echo json_encode($dialog_customer);?>;
var source_inventory = <?php echo json_encode($dialog_inventory);?>;

var form_header = <?php echo json_encode($form_header);?>;
var form_detail = <?php echo json_encode($form_detail);?>;

var status_logistic = "<?php echo $form_header[0]['logistic_status'];?>";
var status_financial = "<?php echo $form_header[0]['financial_status'];?>";

var return_url = "<?php echo site_url('sales/sales_order');?>";
var submit_url = "<?php echo site_url('sales/sales_order_edit_query');?>/"+form_header['doc_num'];
var ajax_url_1 = "<?php echo base_url('inventory/ajax_inventory_info');?>";
var url_1 = "<?php echo site_url('inventory/detail');?>";
$(document).ready(function(){	
	//$("#table_detail").colResizable();
});
</script>
