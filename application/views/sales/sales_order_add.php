<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('sales/sales_order');?>">Sales Order</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New 
    </h1>
</section>

<!-- Main content -->
<section class="content">
	<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>	
    <div class="row">
    	<div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-3" for="buss_name">
                	Customer                    
                </label>
                <div class="col-sm-9">
                	<input type="hidden" id="buss_id" name="buss_id" value="">
                    <input type="hidden" id="buss_char" name="buss_char" value="">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="">                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_customer" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                    
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="cust_phone">Customer Phone</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" id="cust_phone1" name="cust_phone1" placeholder="Phone 1">
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-4">
					<input type="text" class="form-control input-sm" id="cust_phone2" name="cust_phone2" placeholder="Phone 2">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="ship_addr">Customer Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                </div>
            </div>	
        	<div class="form-group">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>		
            <div class="form-group">
                <label class="col-sm-3" for="doc_ddt">Date Required</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>
        </div>
    </div>    

    <div class="row">
    	<div class="col-sm-12">
        	<div class="table-responsive"><!--  class="table-responsive" style="overflow:auto;"-->
        	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
            <thead>
            <tr>
            	<th style="width:5%">#</th>
                <th style="width:20%">
                	<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                        <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Item Code</span>
						<span class="visible-xs"><i class="fa fa-search"></i></span>                        
                    </button>
                </th>
                <th style="width:12%">Item Name</th>
                <th style="width:8%">Quantity</th>
                <th style="width:10%">Price</th>
                <th style="width:8%">Disc %</th>
                <th style="width:10%">Tax</th>
                <th style="width:12%">Net Price</th>
                <th style="width:15%">Total</th>
                <th style="width:10%" class="action">Action</th>
            </tr>
            </thead>            
            <tbody>
            <tr id="default">
            	<input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                <td style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>

                    <input type="hidden" name="ast_price_temp[]" id="ast_price_temp1">                    
                    <input type="hidden" name="const_disc[]" id="const_disc1" value="100">
                    <input type="hidden" name="const_after_disc[]" id="const_after_disc1">
                    <input type="hidden" name="ast_disc_factor[]" id="ast_disc_factor1" >
                    <input type="hidden" name="ast_price_after_disc[]" id="ast_price_after_disc1" >
                    <input type="hidden" name="const_tax[]" id="const_tax1" value="100">
                    <input type="hidden" name="const_after_tax[]" id="const_after_tax1">
                    <input type="hidden" name="ast_tax_factor[]" id="ast_tax_factor1" >
                </td>
                <td>
                    <div class="input-group">                    	
	                    <span class="input-group-btn">
                            <a href="#" class="btn btn-warning form-control" name="a_ast_code" target="_blank">
                            	<i class="fa fa-external-link"></i>
                            </a>                            
						</span>
                        <input type="text" name="ast_code[]" id="ast_code1" class="form-control " readonly="readonly">
                    </div>
                </td>
                <td>
                	<input type="text" name="ast_name[]" id="ast_name1" class="form-control">
                </td>
                <td>
                	<input type="number" name="ast_qty[]"  id="ast_qty1"  class="form-control" style="text-align:right">
                </td>
                <td>
                	<input type="text" name="ast_price[]"  id="ast_price1"  class="form-control" style="text-align:right">
                </td>
                <td>
                    <input type="number" name="ast_disc[]" id="ast_disc1" class="form-control" style="text-align:right">
                </td>
                <td>
                    <!-- <input type="text" name="ast_tax[]"  id="ast_tax1"  class="form-control" style="text-align:right"> -->
                    <?php echo sw_CreateDropdown(array('ast_tax[]','ast_tax1'),$ddl_tax,'tax_percent','tax_name');?>
                </td>
                <td>
                    <input type="number" name="ast_netprice[]"  id="ast_netprice1"  class="form-control" style="text-align:right" readonly>
                </td>
                <td>
                	<input type="text" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right"readonly>
                </td>
                <td>
                	<div class="input-group">
						<div class='input-group-btn'>
                            <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                            	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
						</div>
					</div>
				</td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><div>
                    <input type="number" class="form-control" id="qty_total" name="doc_total" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
                <td></td>
                <td></td>
                <td id="foot_label"><strong>Sub Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_subtotal" name="doc_subtotal" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>Disc:&nbsp;&nbsp;&nbsp;</strong></td>
                <td id="foot_label" style="text-align:right;">
                	<input type="number" class="col-sm-8" id="doc_disc_pct" name="doc_disc_pct"
                    	value="0" style="text-align:right;" />%
                	<!-- <strong>Disc:&nbsp;&nbsp;&nbsp;</strong> -->
                </td>                
                <td><div>
                    <input type="text" class="form-control" id="doc_disc" name="doc_disc" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_total" name="doc_total" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            </tfoot>
            </table>
            </div>
    	</div>
    </div>
    <br />
    <button type="submit" class="btn btn-info pull-right">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>    
    <br /><br /><br />
	</form>
</section>
<!-- /.content -->

<?php //LAYOUT HELPER: MODAL
	echo sw_createModal('modal_item','Select Item','dialog_inventory_push_data();','inventory/dialog_inventory');
	echo sw_createModal('modal_customer','Select Customer','dialog_customer_push_data();','business/dialog_customer');	
?>
<script src="<?php echo site_url('assets/js/bloodhound.js');?>"></script>
<script src="<?php echo site_url('assets/js/typeahead.jquery.js');?>"></script>
<script src="<?php echo site_url('assets/js/typeahead.bundle.js');?>"></script>

<!--<script src="<?php echo site_url('assets/js/bootstrap3-typeahead.js');?>"></script>-->
<script> 
var source_location = <?php echo json_encode($ddl_whse);?>;
var source_customer = <?php echo json_encode($dialog_customer);?>;
var source_inventory = <?php echo json_encode($dialog_inventory);?>;
var ta_inventory = <?php echo json_encode($ta_inventory);?>;

var return_url = "<?php echo site_url('sales/sales_order');?>";
var submit_url = "<?php echo site_url('sales/sales_order_add_query');?>";
var ajax_url_1 = "<?php echo base_url('inventory/ajax_inventory_info');?>";
var url_1 = "<?php echo site_url('inventory/detail');?>";




/*var stocksData = source_inventory;
var stocks = new Bloodhound({
    datumTokenizer: function (d) { 
		for (var prop in d) { 
		  	//return Bloodhound.tokenizers.whitespace(d[prop]);
			return Bloodhound.tokenizers.whitespace(d.item_code);
		}        
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    limit: 5,
    local: stocksData,
});
stocks.initialize();
$('.typeahead').typeahead(null, {
    name: 'stocks',
    displayKey: function(stocks) {
        for (var prop in stocks) { 
			return stocks.item_code;
        }
    },
    source: stocks.ttAdapter(),
	updater: function (stocks) {
		console.log(stocks);
		alert('done');
		return stocks;
	}	
}).on('typeahead:selected', function (obj, datum) {
	var var_datum = [];
	var_datum[0] = datum.item_id;
	dialog_inventory_pull_data(var_datum);
});
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*var stocksData = source_inventory;
var stocks = new Bloodhound({
	datumTokenizer: function (d) { 
		for (var prop in d) { return Bloodhound.tokenizers.whitespace(d.item_code);}        
	},
	queryTokenizer: Bloodhound.tokenizers.whitespace,
	//local:  ["(A)labama","Alaska","Arizona","Arkansas","Arkansas2","Barkansas"]
	limit: 5, //#2 added
	local : stocksData
});
stocks.initialize();*/
/*$('.typeahead').typeahead(null,{  // #1 added
	//name : 'stock', //#4 added	
	displayKey: function(stocks) {
        for (var prop in stocks) { 
			return stocks.item_code;
        }
    },
	source:stocks.ttAdapter() ,
	updater: function (stocks) {
		return stocks;
	}
}).on('typeahead:selected', function (obj, datum) {
	var var_datum = [];
	var_datum[0] = datum.item_id;
	dialog_inventory_pull_data(var_datum);
});*/
/*function apply_typeahead(elm){
	$(elm).typeahead(null,{ 
		displayKey: function(stocks) {
			for (var prop in stocks) { 
				return stocks.item_code;
			}
		},
		source:stocks.ttAdapter() ,
		updater: function (stocks) {
			return stocks;
		}
	}).on('typeahead:selected', function (obj, datum) {
		var var_datum = [];
		var_datum[0] = datum.item_id;
		dialog_inventory_pull_data(var_datum);
	});
}
apply_typeahead('input[name^=ast_code].typeahead');*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//$(document).ready(function(){	
	
//});

</script>
