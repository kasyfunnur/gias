<?php
echo '<link href="'.base_url('assets/css/bootstrap.min.css').'" rel="stylesheet" media="screen">';
echo '<link href="'.base_url('assets/css/font-awesome.min.css').'" rel="stylesheet" type="text/css">';
echo '<link href="'.base_url('assets/css/ionicons.min.css').'" rel="stylesheet" type="text/css">';
echo '<link href="'.base_url('assets/css/AdminLTE.css').'" rel="stylesheet" type="text/css">';
?>
<style>
	.selected td{background-color:#48507b;color:#fff}
	tbody{		
		-moz-user-select: -moz-none;
		-khtml-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}
</style><?php //echo sw_createBox("Choose Document:");?>
<?php $column = array('','Asset Code','Asset Name');?>
<div class="row">
	<div class="col-sm-12">
    	<table class="table table-condensed" id="dialog_sales_delivery_list">
        <thead>
            <tr>
                <td width="5%"><input type="checkbox" name="sales_delivery_chk_all" id="sales_delivery_chk_all" />&nbsp;&nbsp;</td>
                <td width="15%">Doc Num</td>
                <td width="15%">SO #</td>
                <td width="20%">Customer</td>
                <td width="30%">Create Date</td>
                <td width="15%">Warehouse</td>
                <td>Status</td>
            </tr>
        </thead>
        <tbody>
                    
        </tbody>
        </table>
    </div>
</div>

<?php 
echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/bootstrap.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/admin_app.js').'"></script>';

echo '<script src="'.base_url('assets/js/comp/status.js').'"></script>';
?>

<script type="text/javascript">
/*---
1.reset_table(table)
2.populate_table(table)
3.dialog_sales_order_push_data
---*/
//if dialog type = table, default row is required
var data_del = '';
var del_default_row = '<tr><td><input type="checkbox" name="chk[]" id="chk"/></td><td><input type="hidden" name="doc_id[]" id="doc_id" /><span></span></td><td><span></span></td><td></td><td></td><td></td></tr>';
function dialog_sales_delivery_reset_table(table){
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(del_default_row);
}
function dialog_sales_delivery_populate_data(table,data){ 
	data_del = data;
	if(data_del){
		//delete first row
		if($('#'+table+' > tbody > tr').length != 1){
			$('#'+table+' > tbody > tr:last').remove();
		}
		for(var p = 0; p<data_del.length; p++){
			var row = clone_row(table);
			row.find('td:eq(1)').find('input[type=hidden]').val(data_del[p]['doc_id']);
			row.find('td:eq(1)').find('span').text(data_del[p]['doc_num']);
			row.find('td:eq(2)').find('span').text(data_del[p]['doc_ref']);
			row.find('td:eq(3)').text(data_del[p]['buss_name']);
			row.find('td:eq(4)').text(data_del[p]['doc_dt']);
			row.find('td:eq(5)').text(data_del[p]['location_name']);
			row.find('td:eq(6)').html(status_label('billing',data_del[p]['billing_status']));
		}
		var row = clone_row(table);
		
		//delete first and last after insert
		$('#'+table+' > tbody > tr:first').remove();
		$('#'+table+' > tbody > tr:last').remove();
	}else{
		console.log('error');	
	}
}
function dialog_sales_delivery_push_data(){
	var result;
	//var selected = $('input[name="doc_id[]"]').val();
	var selected = $('#dialog_sales_delivery_list > tbody > tr').find('td:eq(0) > input[type="checkbox"]:checked').parent().parent().find('td:eq(1) > input[type="hidden"]').val();
	//alert('selectedd : '+ selected);
	//console.log(data_del);
	for( var i = 0; i < data_del.length; i++ ) {
		//alert(source_so[i]['doc_id']); 	
		if( data_del[i]['doc_id'] === selected ) { 
			result = data_del[i];
			//break;
		}
	}
	dialog_sales_delivery_pull_data(result);
}

$(document).ready(function(){		
	$(document).on('click','#dialog_sales_delivery_list tbody > tr',function(event){
		//SINGLE SELECTION
		if(event.target.type !== 'checkbox'){
			$(':checkbox',this).trigger('click');
		}else{
			if($(this).hasClass('selected')){
				$(this).removeClass('selected');
			}else{
				$(this).siblings().removeClass('selected');
				$(this).siblings().find('td > input:checkbox').prop("checked",false);
				$(this).addClass('selected');
			}
		}		
	});
});
</script>