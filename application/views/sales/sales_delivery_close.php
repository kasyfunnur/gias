<!-- Content Header (Page header) -->
<style>
	.inactive td{background-color:#CCC;opacity: 0.5;}
	thead>tr>th{text-align:center;font-weight:bold}
	#foot_label{text-align:right;}
	/*tbody{padding-left:1px;padding-right:1px;}opacity: 0.5;*/
</style>
<?php // STATUS MODEL
	//$status_attr = $this->sales_order_model->logistic_status($form_header[0]['logistic_status']);
	//$logistic_status = $this->status_model->status_icon('logistic',$form_header[0]['logistic_status']);
	$billing_status = $this->status_model->status_icon('billing',$form_header[0]['billing_status']);
	$finance_status = $this->status_model->status_icon('financial',$form_header[0]['financial_status']);
?>

<section class="content-header">
    <h1><a class="" href="<?php echo site_url('sales/sales_delivery');?>">Sales Delivery</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        CLOSING... </h1>   
</section>

<!-- Main content -->
<section class="content">	
    <?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <h4>
            <?php echo $billing_status;?>
            <?php echo $finance_status;?></h4>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-6">
        	<!---<div class="form-group">
            	<label class="col-sm-3">Status</label>
                <div class="col-sm-9">					
                    <?php echo $billing_status;?>
                    <?php echo $finance_status;?>
                </div>
            </div>--->
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="buss_name">
                	Customer                   
                </label>
                <div class="col-sm-9">
                	<?php echo $form_header[0]['buss_name'];?>                                  
                </div>
            </div>   
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="ship_addr">Location</label>
                <div class="col-sm-9">
                    <?php echo $form_header[0]['location_name'];?>
                </div>
            </div>         
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <?php echo $form_header[0]['ship_addr'];?>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <?php echo $form_header[0]['doc_note'];?>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <?php echo $form_header[0]['doc_num'];?>
                </div>
            </div>	
        	<div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <?php echo substr($form_header[0]['doc_dt'],0,10);?>
                </div>
            </div>		
            <div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="doc_ddt">Date Required</label>
                <div class="col-sm-9">
                    <?php echo substr($form_header[0]['doc_ddt'],0,10);?>
                </div>
            </div>		                    
        </div>
    </div>
    <hr />
    
    <div class="row">
    	<div class="col-sm-3">Reason for closing document:</div>
        <div class="col-sm-9">
    	<textarea required name="reason" id="reason" style="width:500px"/></textarea>
        </div>
    </div>
    
    <br />
    <button type="submit" class="btn btn-info pull-right">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
	</form>	
</section>
<!-- /.content -->

<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script>
//var form_header = <?php echo json_encode($form_header);?>;
//var form_detail = <?php echo json_encode($form_detail);?>;

//var status_financial = "<?php echo $form_header[0]['financial_status'];?>";

var return_url = "<?php echo site_url('sales/sales_delivery');?>";
var submit_url = "<?php echo site_url('sales/sales_delivery_close_query');?>/"+'<?php echo $form_header[0]['doc_num'];?>';

function validateForm(){
	if(confirm("Are you sure?")==true){		
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Document has been closed successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}
</script>
