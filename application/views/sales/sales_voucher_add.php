<section class="content-header">
    <h1><a class="" href="<?php echo site_url('sales/vouchers');?>">Voucher List</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New</h1>
</section>
<?php echo $msg?>
<!-- Main content -->
<section class="content">
    <?php $attributes=array( 'class'=> 'form-horizontal', 'role' => 'form', 'method' => 'post', 'name' => 'frm', 'id' => 'frm', 'onsubmit'=>'return FormValidation()' ); echo form_open('', $attributes); ?>
    <div class="box box-info">
        <div class="box-header"></div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="voucher_code">Voucher Code</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="voucher_code" name="voucher_code" value="" placeholder="Voucher Code..." required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="active_date">Active Date</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="active_date" name="active_date" value="<?php echo date('Y-m-d') ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="expiry_date">Expiry Date</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="expiry_date" name="expiry_date" value="<?php echo date('Y-m-d') ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="voucher_count">Voucher Count</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="voucher_count" name="voucher_count" value="" placeholder="Voucher Count..." required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="value">Voucher Value</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="value" name="value" value="" placeholder="Voucher Value (ex: 50,000)..." required>
                        </div>
                    </div>

					<div class="form-group">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-primary">Generate</button>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>
