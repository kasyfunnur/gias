<!-- Content Header (Page header) -->
<style>
	.inactive td{background-color:#CCC;opacity: 0.5;}
	thead>tr>td{text-align:center;font-weight:bold}
	#foot_label{text-align:right;}
	label{		
		-moz-user-select: -moz-none;
		-khtml-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}	
</style>
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('sales/sales_invoice');?>">Sales Down Payment Invoice</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New </h1>
</section>

<!-- Main content -->
<section class="content">
    <?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
    <div class="row">
    	<div class="col-sm-6">
        	<input type="hidden" id="doc_ref_id" name="doc_ref_id" value="" />
            <input type="hidden" id="doc_ref_type" name="doc_ref_type" value="" />
            <div class="form-group">
                <label class="col-sm-3" for="buss_name">
                	Customer                    
                </label>
                <div class="col-sm-9">
                	<input type="hidden" id="buss_id" name="buss_id" value="">
                    <input type="hidden" id="buss_char" name="buss_char" value="">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="" readonly>                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning disabled" data-toggle="modal" href="#modal_customer" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                   
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_ref">
                	Sales Order                    
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="so_doc_ref_id" name="so_doc_ref_id" value="" />
                    <input type="hidden" id="so_doc_ref_type" name="so_doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="so_doc_ref" name="so_doc_ref" value="" readonly>                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning disabled" data-toggle="modal" href="#modal_so" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                                       
                </div>
            </div>
            <!---<div class="form-group">
                <label class="col-sm-3" for="doc_ref">
                	Purchase Receipt                    
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="rcv_doc_ref_id" name="rcv_doc_ref_id" value="" />
                    <input type="hidden" id="rcv_doc_ref_type" name="rcv_doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="rcv_doc_ref" name="rcv_doc_ref" value="" readonly>
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_rcv" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                 
                </div>
            </div>--->
            <div class="form-group">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>
            <!---<div class="form-group">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr" readonly></textarea>
                </div>
            </div> --->           
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                </div>
            </div>	
        	<div class="form-group">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>		
            <div class="form-group">
                <label class="col-sm-3" for="doc_ddt">Due Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-12">
        	<table class="table-bordered" id="table_detail">
            <thead>
            <tr>
            	<td width="5%">#</td>
                <td width="15%">Item Code
                	<!---<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                    	<i class="fa fa-search"></i>&nbsp;&nbsp; Item Code</button>--->
                </td>
                <td width="20%">Item Name</td>
                <!---<td width="20%">Open Qty</td>--->
                <td width="10%">Qty</td>
                <td width="20%">Price</td>
                <td width="25%">Total</td>
                <td width="10%">Action</td>
            </tr>
            </thead>
            <tbody>
            <tr height="30px" id="default">
            	<td style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                    <input type="hidden" name="reff_line[]" id="reff_line1" value="" />
                </td>
                <td>
                	<input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                	<input type="text" name="ast_code[]" id="ast_code1" class="form-control" readonly="readonly">
                </td>
                <td><input type="text" name="ast_name[]" id="ast_name1" class="form-control" readonly="readonly"></td>
                <td>
                	<input type="number" name="ast_qty[]" id="ast_qty1" 
                		class="form-control" style="text-align:right" readonly="readonly">
                   	<input type="hidden" name="ast_qty_open[]" id="ast_qty1" 
                    	class="form-control" readonly="readonly" style="text-align:right">
                    <input type="hidden" name="ast_del_qty[]" id="ast_del_qty1" 
                    	class="form-control" readonly="readonly" style="text-align:right">
				</td>
                <td><input type="number" name="ast_price[]" 
                		id="ast_price1" class="form-control" style="text-align:right" readonly="readonly"></td>
                <td><input type="number" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right"readonly></td>
                
                <td>
                	<div style='text-align:center'>
						<div class='btn-group'>
                            <button type="button" class="btn btn-flat btn-xs btn-danger" id="act_del1" name="act_del">DELETE</button>
						</div>
					</div>
				</td>
            </tr>
            </tbody>
            <tfoot>
                <tr>
                	<td colspan="5" id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                    <td>
                    	<div>
                    	<input type="number" class="form-control" id="amount_total" name="amount_total" value="0" 
                        	style="text-align:right;" readonly>
                        </div>
                   	</td>
                </tr>
                <tr>
                	<td colspan="5" id="foot_label">
                    	<strong class="pull-right">
	                        <input type="number" class="col-sm-5" id="doc_dp_pct" name="doc_dp_pct"
    	                		value="0" style="text-align:right;" />%
        	                DP Payment:&nbsp;&nbsp;&nbsp;
						</strong>
                    </td>
                    <td>
                    	<div>                        
                    	<input type="number" class="form-control" id="amount_due" name="amount_due" value="0" 
                        	style="text-align:right;" readonly>
                        </div>
                   	</td>
                </tr>
            </tfoot>
            </table>
    	</div>
    </div>
    <hr />
    <!---<div class="row">
    	<div class="col-sm-12">
            <table class="table-bordered" id="table_expense">
                <thead>
                <tr>
                    <td width="10%">#</td>
                    <td width="45%">Expense</td>
                    <td width="25%">Amount</td>
                    <td width="20%">Action</td>
                </tr>
                </thead>
                <tbody>
                	<tr height="30px">
                        <td style="text-align:center">
                        	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                        </td>
                        <td><input type="text" name="ast_qty_open[]" id="ast_qty1" 
	                    	class="form-control">
                       	</td>
                        <td><input type="number" name="ast_qty_open[]" id="ast_qty1" 
                    		class="form-control" style="text-align:right">
                        </td>
                        <td>
                        	<div style='text-align:center'>
                                <div class='btn-group'>
                                    <button type="button" class="btn btn-flat btn-xs btn-danger" id="act_del1" name="act_del">DELETE</button>
                                </div>
                            </div>
						</td>
                    </tr>
                </tbody>
			</table>
        </div>
    </div>--->
    <!---<br />--->
    <div class="row">
    	<div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
		</div>
    </div>
    <!---<hr />--->
	</form>	
</section>
<!-- /.content -->

<?php //var_dump($dialog_po);?>
<script>
var source_del = "";
var source_so = "";
</script>
<!---<section class="modal">--->
<div class="modal fade" id="modal_so" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title">Select SO
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" 
                        	onclick="dialog_sales_order_push_data();">Save & Close</button>
                    </div>
                </h4>                
            </div>
            <div class="modal-body">
				<?php 
					$this->load->view('sales/dialog_sales_order');	
				?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" 
                	onclick="dialog_sales_order_push_data();">Save & Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title">Select Delivery
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" 
                        	onclick="dialog_sales_delivery_push_data();">Save & Close</button>
                    </div>
                </h4>                
            </div>
            <div class="modal-body">
				<?php 
					$this->load->view('sales/dialog_sales_delivery');	
				?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" 
                	onclick="dialog_sales_delivery_push_data();">Save & Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_customer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog" >
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title">Select Customer
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" 
                        	onclick="dialog_customer_push_data();">Save & Close</button>  
                    </div>
                </h4>                
            </div>
            <div class="modal-body">
				<?php 					
					$this->load->view('business/dialog_customer');
				?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" 
                	onclick="dialog_customer_push_data();">Save & Close</button>                
            </div>
        </div>
    </div>
</div>
<!---</section>--->
<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
//var source_po = <?php// echo json_encode($dialog_po);?>;
var source_customer = <?php echo json_encode($dialog_customer);?>;

var return_url = "<?php echo site_url('sales/sales_invoice');?>";
var submit_url = "<?php echo site_url('sales/sales_invoice_add_query/DP');?>";
var ajax_url_1 = "<?php echo base_url('sales/ajax_getAll_del_with_customer');?>";
var ajax_url_2 = "<?php echo base_url('sales/ajax_getDetail_del_open');?>";
var ajax_url_3 = "<?php echo base_url('sales/ajax_getHeader_del');?>";
var ajax_url_4 = "<?php echo base_url('sales/ajax_getAll_so_with_vendor');?>";
var ajax_url_5 = "<?php echo base_url('sales/ajax_getDetail_so');?>";
var ajax_url_6 = "<?php echo base_url('sales/ajax_getHeader_so');?>";

var param = "<?php echo $param;?>";

$(document).ready(function(){	
	$("#location_id").on('change',function(){
		var result;
		for( var i = 0; i < source_location.length; i++ ) {
			if( source_location[i]['location_id'] == $(this).val() ) {
				result = source_location[i];
				break;
			}
		}
		$("#ship_addr").val(result['location_address']+', '+result['location_city']+', '+result['location_state']+', '+result['location_country']);
	});
	$("#location_id").val(1).change();
});

</script>
<script type="text/javascript">
function get_del_detail(){
	return $.ajax({
		url:ajax_url_2,
		data:{doc_num:$("#del_doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
        }
	});
}
function get_so_detail(){
	return $.ajax({
		url:ajax_url_5,
		data:{doc_num:$("#so_doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
        }
	});
}
</script>
