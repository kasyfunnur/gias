<section class="content-header">
    <h1><a class="" href="<?php echo site_url('sales/vouchers');?>">Voucher List</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Edit</h1>
</section>
<?php echo $msg?>
<!-- Main content -->
<?php $header = $voucher['header'];
    $detail = $voucher['detail'];
?>
<section class="content">
    <?php $attributes=array( 'class'=> 'form-horizontal', 'role' => 'form', 'method' => 'post', 'name' => 'frm', 'id' => 'frm', 'onsubmit'=>'return FormValidation()' ); echo form_open('', $attributes); ?>
    <input type="hidden" name="id" value="<?php echo $header['id'] ; ?>">
    <div class="box box-info">
        <div class="box-header"></div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="voucher_code">Voucher Code</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="voucher_code" name="voucher_code" placeholder="Voucher Code..." disabled value="<?php echo $header['voucher_code'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="active_date">Active Date</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="active_date" name="active_date" value="<?php echo date('Y-m-d',strtotime($header['active_date'])) ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="expiry_date">Expiry Date</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="expiry_date" name="expiry_date" value="<?php echo date('Y-m-d',strtotime($header['expiry_date'])) ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="voucher_count">Voucher Count</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="voucher_count" name="voucher_count" value="<?php echo $header['voucher_count'] ?>" placeholder="Voucher Count..." disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="value">Voucher Value</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="value" name="value" value="<?php echo $header['value'] ?>" placeholder="Voucher Value (ex: 50,000)..." disabled>
                        </div>
                    </div>

					<div class="form-group">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-danger" onclick="void_voucher()">Void Remaining Vouchers</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- detail vouchers -->
            <div class="row">
                <div class="col-xs-12">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Voucher Number</th>
                            <th>Used</th>
                            <th>Disabled</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $count = 1;
                            foreach ($detail as $v) { ?>
                            <tr>
                                <td><?php echo $count; $count++; ?>
                                <td><?php echo $v['voucher_number'] ?></td>
                                <td><?php echo ($v['used'] ? "Yes" : "No") ?>
                                <td><?php echo ($v['disabled'] ? "Yes" : "No") ?></td>
                            </tr>
                        <?php } ?>
                        <tr></tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</section>