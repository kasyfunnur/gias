<?php $this->view('template/printheader.php');?>

<style type="text/css">
	table.printBody tr td{
		font-family:Verdana, Geneva, sans-serif;
		font-size:12px;
		border: solid 1px #CCC;
	}	
	thead{
		font-weight: bold;
		text-align: center;	
	}	
	td.numeric{
		text-align:right;
	}
	tr.aggr, tr.disc, tr.total{
		font-weight: bold;	
	}
</style>
<!---<pre>--->
<?php 
//	var_dump($form_header);
	//print_r($form_detail);
?>
<!---</pre>--->
<div class="container col-sm-12 col-xs-12">
<div class="row">
	<div class="col-sm-6 col-xs-6">
    	<div class="row">
        	<div class="col-sm-4 col-xs-4">Kepada</div>
            <div class="col-sm-8 col-xs-8">: <?php echo $form_header[0]['buss_name']?></div>
        </div>
        <div class="row">
        	<div class="col-sm-4 col-xs-4">Alamat</div>            
            <div class="col-sm-8 col-xs-8">: <?php echo $form_header[0]['buss_addr']; 
				if($form_header[0]['buss_city']){echo " - ".$form_header[0]['buss_city'];}?></div>
        </div>
        <div class="row">
        	<div class="col-sm-4 col-xs-4">Tlp</div>            
            <div class="col-sm-8 col-xs-8">: <?php echo $form_header[0]['buss_tlp1']; 
				if($form_header[0]['buss_tlp2']){echo " / ".$form_header[0]['buss_tlp2'];}?></div>
        </div>
	</div>
    <div class="col-sm-6 col-xs-6">
    	<div class="row">
        	<div class="col-sm-5 col-xs-5">Tanggal</div>            
            <div class="col-sm-7 col-xs-7">: <?php echo date("d-M-y",strtotime($form_header[0]['doc_dt']));?></div>
        </div>
        <div class="row">
        	<div class="col-sm-5 col-xs-5">Tanggal diperlukan</div>            
            <div class="col-sm-7 col-xs-7">: <?php echo date("d-M-y",strtotime($form_header[0]['doc_ddt']));?></div>
        </div>
        <div class="row">
        	<div class="col-sm-5 col-xs-5">Memo</div>            
            <div class="col-sm-7 col-xs-7">: <?php echo $form_header[0]['doc_note'];?></div>
        </div>
    </div>
</div>
<!---<div class="row">--->
	<!---<table class="table-condensed">
    	<div class="row">
        	<div class="col-sm-1">No</div>
            <div class="col-sm-2">Art.</div>
            <div class="col-sm-2">Model</div>
            <div class="col-sm-1">Qty</div>
            <div class="col-sm-1">UoM</div>
            <div class="col-sm-2">Harga</div>
            <div class="col-sm-3">Total</div>
        </div>
        
    </table>--->
<!---</div>--->

<table width="100%" class="printBody table-condensed">
<thead>
	<td>No</td>
    <td>Art.</td>
    <td>Model</td>
    <td>Qty</td>
    <td>UoM</td>
    <td>Harga</td>
    <!---<td>Disc</td>--->
    <td>Jumlah</td>
</thead>
<tbody>
<?php //AGGR INIT
	$aggr_item_qty = 0;
	$aggr_item_total = 0;
?>
<?php for($x = 0; $x<= count($form_detail)-1; $x++){ ?>	
    <tr>
        <td class="numeric"><?php echo $x+1;?>. </td>
        <td><?php echo $form_detail[$x]['item_code'];?></td>
        <td><?php echo $form_detail[$x]['item_name'];?></td>
        <td class="numeric"><?php echo number_format($form_detail[$x]['item_qty'],2);?></td>
        <td><!---<?php //echo $form_detail[$x]['uom_name'];?>--->Pcs</td>
        <td class="numeric"><?php echo number_format($form_detail[$x]['item_price'],2);?></td>
        <!---<td class="numeric"><?php echo number_format($form_detail[$x]['item_disc_pct1'],2);?></td>--->
        <td class="numeric"><?php echo number_format($form_detail[$x]['item_total'],2);?></td>
    </tr>    
<?php //AGGR OPT
	$aggr_item_qty += $form_detail[$x]['item_qty'];
	$aggr_item_total += $form_detail[$x]['item_total'];
?>
<?php } ?>
	<tr>
    	<td colspan="7">&nbsp;</td>
    </tr>
	<tr class="aggr">
        <td class="numeric"></td>
        <td></td>
        <td></td>        
        <td class="numeric"><?php echo number_format($aggr_item_qty,2);?></td>
        <!---<td></td>--->
        <td class=""></td>
        <td class="numeric">Sub Total</td>
        <td class="numeric"><?php echo number_format($aggr_item_total,2);?></td>
    </tr>
	<tr class="disc">
        <td class="numeric"></td>
        <td></td>
        <td></td>
        <td></td>
        <!---<td class="numeric"></td>--->
        <td class=""></td>
        <td class="numeric">Discount <?php echo $form_header[0]['doc_disc_percent']." %";?></td>
        <td class="numeric"><?php echo number_format($form_header[0]['doc_disc_amount'],2);?></td>
    </tr>     
	<tr class="total">
        <td class="numeric"></td>
        <td></td>
        <td></td>
        <td></td>
        <!---<td class="numeric"></td>--->
        <td class=""></td>
        <td class="numeric">Total</td>
        <td class="numeric"><?php echo number_format($form_header[0]['doc_total'],2);?></td>
    </tr>         
</tbody>
</table>
<br />
<div class="row">
    <div class="col-sm-2 col-xs-2">Send From</div>
    <div class="col-sm-4 col-xs-4">: <?php echo $form_header[0]['location_name'];?></div>    
    <div class="col-sm-2 col-xs-2">Created By</div>
    <div class="col-sm-4 col-xs-4">: <?php echo $form_header[0]['create_by'];?></div>    
</div>
<div class="row">
    <div class="col-sm-2 col-xs-2"><!---Send From---></div>
    <div class="col-sm-4 col-xs-4"><!---: <?php echo $form_header[0]['location_name'];?>---></div>    
    <div class="col-sm-2 col-xs-2">Created Date</div>
    <div class="col-sm-4 col-xs-4">: <?php echo $form_header[0]['create_dt'];?></div>    
</div>

</div>
<?php $this->view('template/printfooter.php');?>