<section class="content-header">
    <h1>Sales <i class="fa fa-angle-right"></i> POS <i class="fa fa-angle-right"></i> Review</h1>
</section>
<?php echo (isset($msg))?$msg: ""?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <form action="" method="POST" id="form_pos_review" class="form-inline" role="form">
                <div class="form-group">
                <label class="control-label">
                    <h4><?php echo $cashier['location_code'] . ' ' . $cashier['cashier_code'] ?></h4>
                </label>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="sales_date">Date</label>

                    <input type="date" class="form-control" id="sales_date" name="sales_date" placeholder="Transaction Date" value="<?php echo $sales_date ;?>">
                </div>

                <button type="submit" class="btn btn-primary"> <i class="fa fa-search"></i> Display</button>
            </form>

            
        </div>
        <div class="box-body">
            <table class="table table-hover table-striped table-condensed">
                <thead>
                    <tr>
                        <th>Outlet</th>
                        <th>Cashier #</th>
                        <th>Input by</th>
                        <th>Date</th>
                        <th>Sales Code</th>
                        <th align="right" class="text-right">Sales Amount</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $total_sales = 0;
                        foreach ($transactions as $trx) {
                            $total_sales += $trx['sales_amount'];
                            ?>
                            <tr>
                                <td><?php echo $trx['location_code']; ?></td>
                                <td><?php echo $trx['cashier_code']; ?></td>
                                <td><?php echo $trx['input_by']; ?></td>
                                <td><?php echo $trx['input_date']; ?></td>
                                <td>
                                    <a href="javascript:print_sales('<?php echo $trx['sales_code'] ?>')" target="_blank">
                                        <?php echo $trx['sales_code']; ?>
                                    </a>
                                </td>
                                <td align="right"><?php echo number_format($trx['sales_amount']); ?></td>
                                <td><?php echo ($trx['posted']) ?
                                    '<label class="label label-success">Posted</label>' :
                                    '<label class="label label-warning">Unposted</label>'; ?>
                                </td>
                            </tr>
                    <?php } ?>
                            <!-- total row -->
                            <tr>
                                <td colspan="5" align="right"><h4>Total &nbsp;</h4></td>
                                <td align="right"><h4 id="total_sales_amount"><?php echo number_format($total_sales); ?></h4></td>
                                <td>&nbsp;</td>
                            </tr>

                            <!-- payments row -->
                            
                            <?php foreach ($payments as $payment) { ?>
                                <tr>
                                    <td colspan="5" align="right"><h4>By <?php echo $payment['method'] ?></h4></td>
                                    <td align="right"><h4><?php echo number_format($payment['amount']); ?></h4></td>
                                    <td>&nbsp;</td>    
                                </tr>
                            <?php } ?>
                        

                            <!-- action button -->
                            <tr>
                                <td colspan="7" class="text-center" align="center">
                                    <a class="btn btn-danger" onclick="pos_close_cashier('<?php echo site_url("sales/pos_close_cashier/{$cashier['cashier_id']}/{$sales_date}") ?>')">CLOSE</a>
                                </td>
                            </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script type="text/javascript">
    var site_url = '<?php echo site_url(); ?>';
</script>