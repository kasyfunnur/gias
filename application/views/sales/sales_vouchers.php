<section class="content-header">	
  <h1>Voucher List
  	<a href="<?php echo site_url('sales/voucher_add');?>" class="btn btn-flat btn-info pull-right">
      	<i class="fa fa-plus"></i>&nbsp;&nbsp;Add New
    </a>
  </h1>
</section>
<section class="content">
<?php echo $msg?>
<div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body">
      <table id="voucher_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Code</th>
                <th>Issued Date</th>
                <th>Active From</th>
                <th>Expiry Date</th>
                <th>Vouchers Count</th>
                <th>Value</th>
            </tr>
        </thead>
    </table>
    </div>
  </div>
</section>
