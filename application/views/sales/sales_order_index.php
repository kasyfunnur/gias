<section class="content-header">
  	<h1> Sales Order <a href="<?php echo site_url('sales/sales_order_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<span class="hidden-xs">
        		<i class="fa fa-plus"></i>&nbsp;&nbsp;New Order
            </span>
        	<span class="visible-xs">
        		<i class="fa fa-plus"></i>
            </span>
        </button>
    </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<section class="content">
<div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body table-responsive">
  	<table class="table table-hover">
    	<thead>
        	<tr>
            	<!--<th class="hidden-xs">#</th>-->
                <th style="width:25%">Document</th>
                <th style="width:35%">Note</th>
                <th style="width:40%">Actions</th>
            </tr>
        </thead>
        <tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>
			<?php 
				$approval_status = $this->status_model->status_label('approval',$datatable[$x]['approval_status']);
                $document_status = $this->status_model->status_label('document',$datatable[$x]['doc_status']);
				$logistic_status = $this->status_model->status_label('logistic',$datatable[$x]['logistic_status']);
				$billing_status = $this->status_model->status_label('billing',$datatable[$x]['billing_status']);
				$finance_status = $this->status_model->status_label('financial',$datatable[$x]['financial_status']);
				$dp_status = $this->status_model->status_label('financial',"DP");
            ?>
            <tr>
                <td>
					<span style="font-size:16px;font-weight:lighter !important;color:#888">
                        <a href="<?php echo site_url('sales/sales_order_edit');?>/<?php echo $datatable[$x]['doc_num'];?>">
                            <?php echo $datatable[$x]['doc_num'];?>
                        </a> 
                        - <?php echo $datatable[$x]['buss_name'];?> <?php //echo $document_status;?>                        
                    </span>
                    <br />
                    <span style="font-size:12px;font-weight:lighter !important;color:#888">
                        By <?php echo $datatable[$x]['create_by'];?>
                        - <?php echo date("d M, Y", strtotime($datatable[$x]['doc_dt']));?>
                        - <strong>[<?php echo $datatable[$x]['doc_status'];?>]</strong>
                    </span>
                </td>
                <?php 
					$disable = 'disabled';					
					$link_receipt = base_url('sales/sales_delivery_add')."/".$datatable[$x]['doc_num']; 					
					$link_cancel = base_url('sales/sales_order_cancel')."/".$datatable[$x]['doc_num'];
					$link_close = base_url('sales/sales_order_close')."/".$datatable[$x]['doc_num'];
					$link_print = base_url('sales/print_so')."/".$datatable[$x]['doc_num'];
					$link_dp = base_url('sales/sales_invoice_dp_add')."/".$datatable[$x]['doc_num'];
					if($datatable[$x]['logistic_status'] != "DELIVERED" 
						&& $datatable[$x]['logistic_status'] != "PARTIAL" 
						&& $datatable[$x]['doc_status'] != "CANCELLED"
						&& $datatable[$x]['doc_status'] != "CLOSED"){						
						$disable = '';
					}
				?>
                <td>
                	[Due : <?php echo date("d M, Y", strtotime($datatable[$x]['doc_ddt']));?>]
                	<?php echo $logistic_status;?> <?php if($datatable[$x]['doc_paid'] > 0) echo $dp_status;?> <br />
                	<?php echo $datatable[$x]['doc_note'];?>
                </td>
                <td> 
                	<a href="<?php echo $link_cancel;?>" class="btn btn-danger <?php echo $disable;?>">
                        <i class="fa fa-ban"></i> Cancel</a>&nbsp;&nbsp;
					<a href="<?php echo $link_close;?>" class="btn btn-warning <?php echo $disable;?>">
                        <i class="fa fa-archive"></i> Close</a>&nbsp;&nbsp;
                    <a href="<?php echo $link_print;?>" class="btn btn-success <?php echo $disable;?>" target="_blank">
                        <i class="fa fa-print"></i> Print</a>&nbsp;&nbsp;
                    <a href="<?php echo $link_receipt;?>" class="btn btn-info <?php echo $disable;?>" target="_blank">
                        <i class="fa fa-plus"></i> Delivery</a>&nbsp;&nbsp;
                    <a href="<?php echo $link_dp;?>" class="btn btn-info <?php echo $disable;?>" target="_blank">
                        <i class="fa fa-dollar"></i> DP</a>&nbsp;&nbsp;
                        
                    <!--<div class="btn-group">
                      	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        	Send To <span class="caret"></span>
                      	</button>
                      	<ul class="dropdown-menu" role="menu">                            
                            <li>
                            	<a href="<?php echo $link_receipt;?>">
                                    <i class="fa fa-plus"></i> Delivery</a>
                            </li>
                      	</ul>
                    </div>
					<div class="btn-group">
                      	<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                        	Document <span class="caret"></span>
                      	</button>
                      	<ul class="dropdown-menu" role="menu">
                            <li class="<?php echo $disable;?>">
                            	<?php if($disable == ''){ ?>
	                            	<a href="<?php echo $link_cancel;?>"><i class="fa fa-ban"></i> Cancel</a>
                                <?php }else{ ?>
    	                            <a><i class="fa fa-ban"></i> Cancel</a>
                                <?php }?>
                        	</li>
                            <li class="<?php echo $disable;?>">
                            	<?php if($disable == ''){ ?>
	                            	<a href="<?php echo $link_close;?>"><i class="fa fa-archive"></i> Close</a>
                                <?php }else{ ?>
    	                            <a><i class="fa fa-archive"></i> Close</a>
                                <?php }?>
                            </li>
                            <li class="divider"></li>
                            <li>
                            	<a href="<?php echo base_url('sales/print_so')."/".$datatable[$x]['doc_num'];?>">
                                    <i class="fa fa-print"></i> Print</a>
                            </li>
                      	</ul>
                    </div>-->
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table></div>
    </div>
</section>
<!-- /.content -->
