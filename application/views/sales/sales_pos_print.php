<style type="text/css" media="screen">
	html, body, table, tr, td, pre{
		font-family: monospace;
	}
</style>
<table>
	<thead>
		<tr>
			<th colspan="10">
<pre>Digital Buana - Retail Store
Jakarta, Indonesia
022 - 1234567
Sales #<?php echo $trx['header']['trx_code'] ?>

<?php echo $trx['header']['trx_datetime'] ?>

------------------------------------</pre>
			</th>
		</tr>
	</thead>
	<tbody>
	<tr>
		<td>Qty</td>
		<td>Item</td>
		<td style="text-align: right">Rp</td>
	</tr>
<?php foreach ($trx['items'] as $item) { ?>
		<tr>
			<td nowrap="" valign="top"><?php echo $item['item_qty'] ?> x</td>
			<td valign="top">
				<?php echo $item['item_code'] ?>
				<br>
				<?php echo $item['item_name'] ?>

			</td>
			<td valign="top" style="text-align: right"><?php echo $item['item_qty'] * $item['item_price'] ?></td>
		</tr>
<?php } ?>
<tr><td colspan="2"><strong>Total</strong></td><td colspan="2" style="text-align: right"><strong><?php echo number_format($trx['header']['trx_sales_amount']) ?></strong></td></tr>
<tr><td colspan="10">------------------------------------</td></tr>

<?php 	$total_pymt = 0;
		$total_change = 0;
?>
<?php foreach ($trx['payments'] as $pymt) { ?>
		<?php
			$total_pymt += $pymt['amount'];
			if(is_numeric($pymt['change_amount'])) $total_change += $pymt['change_amount'];
		?>
		<tr>
			<td nowrap="" valign="top" colspan="2"><?php echo ucfirst($pymt['method']) ?></td>
			<td valign="top" colspan="2" style="text-align: right">
				<?php echo ($pymt['method'] == 'cash') ? 
					number_format(intval($pymt['amount']) + intval($pymt['change_amount'])) :
					number_format(intval($pymt['amount'])) ;?>
			</td>
		</tr>
<?php } ?>
		<tr>
			<td colspan="2">Change</td>
			<td colspan="2" style="text-align: right">
				<?php echo number_format($total_change) ?>
			</td>
		</tr>
	</tbody>
</table>

<pre>
------------------------------------
   Thank You! Please Come Again
</pre>
<script type="text/javascript">
	
	if (opener && opener.pos_reset_form){
		opener.pos_reset_form(false);
	}
</script>