<!-- Content Header (Page header) -->
<style>
	.inactive td{background-color:#CCC;opacity: 0.5;}
	thead>tr>th{text-align:center;font-weight:bold}
	#foot_label{text-align:right;}
	label{		
		-moz-user-select: -moz-none;
		-khtml-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}	
</style>
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('sales/sales_delivery');?>">Sales Delivery</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New </h1>
</section>

<!-- Main content -->
<section class="content">
    <?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
    <div class="row">
    	<div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-3" for="buss_name">
                	Customer                    
                </label>
                <div class="col-sm-9">
                	<input type="hidden" id="buss_id" name="buss_id" value="">
                    <input type="hidden" id="buss_char" name="buss_char" value="">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="" readonly>
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning <?php if(isset($param) && $param)echo 'disabled';?>" 
                            	data-toggle="modal" href="#modal_customer" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                   
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="cust_phone">Customer Phone</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" id="cust_phone1" name="cust_phone1" placeholder="Phone 1">
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-4">
					<input type="text" class="form-control input-sm" id="cust_phone2" name="cust_phone2" placeholder="Phone 2">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="ship_addr">Customer Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_ref">
                	Sales Order                    
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="doc_ref_id" name="doc_ref_id" value="" />
                    <input type="hidden" id="doc_ref_type" name="doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="doc_ref" name="doc_ref" value="">                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning <?php if(isset($param) && $param)echo 'disabled';?>"
                            	data-toggle="modal" href="#modal_so" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                                      
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>           
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                </div>
            </div>	
            <div class="form-group">
                <label class="col-sm-3" for="doc_ext_ref">Freight Doc #</label>
                <div class="col-sm-3">
                	<input class="form-control input-sm" type="text" name="doc_freight_new" id="doc_freight_new" style="display:none;">
                	<?php 
						array_push($ddl_freight,array('freight_code'=>"0",'doc_freight'=>"New..."));
							echo sw_CreateSelect('doc_freight', $ddl_freight, 'freight_code', 'doc_freight', "");
						/*if(count($ddl_freight)==0){							
							echo '<input class="form-control input-sm" type="text" name="doc_freight" id="doc_freight">';
						}else{
							array_push($ddl_freight,array('freight_code'=>"0",'doc_freight'=>"New..."));
							echo sw_CreateSelect('doc_freight', $ddl_freight, 'freight_code', 'doc_freight', "");
                        }*/
					?>
                </div>
                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm" id="doc_ext_ref" name="doc_ext_ref" value="">
                </div>
            </div>
        	<div class="form-group">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>
            <div class="form-group <?php if(isset($warning) && $warning)echo 'has-error';?>">
                <label class="col-sm-3" for="doc_ddt">Date Required</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>" readonly style="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-12">
        	<div class="table-responsive"><!--- style="table-layout:fixed;white-space:nowrap;width:960px"--->
        	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
            <thead>
            <tr>
            	<th width="5%">#</th>
                <th width="15%">Item Code</th>
                <th width="15%">Item Name</th>
                <th width="20%">Open Qty</th>
                <th width="20%">Qty</th>
                <!---<td width="15%">Price</td>
                <td width="15%">Total</td>--->
                <th width="10%" class="action">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr height="25px" id="default">
            	<td style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                    <input type="hidden" name="reff_line[]" id="reff_line1" value="" />
                </td>
                <td>
                	<input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                	<input type="text" name="ast_code[]" id="ast_code1" class="form-control" readonly="readonly">
                </td>
                <td><input type="text" name="ast_name[]" id="ast_name1" class="form-control" readonly="readonly"></td>
                <td>
                	<input type="hidden" name="ast_so_qty[]" id="ast_so_qty1" />
                    <input type="hidden" name="ast_price[]" id="ast_price1" />
                	<input type="number" name="ast_qty_open[]" id="ast_qty1" 
                    	class="form-control" readonly="readonly" style="text-align:right">                
                </td>
                <td>
                	<input type="number" name="ast_qty[]" id="ast_qty1" 
                		class="form-control" style="text-align:right">
                    <input type="hidden" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right"readonly>
				</td>
                <td>
                	<div class="input-group">
						<div class='input-group-btn'>
                            <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                            	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
						</div>
					</div>
				</td>
            </tr>
            </tbody>
            <tfoot>
            	<input type="hidden" id="doc_subtotal" name="doc_subtotal" value="0">
                <input type="hidden" id="doc_disc_pct" name="doc_disc_pct" value="0">
                <input type="hidden" id="doc_disc" name="doc_disc" value="0">
				<input type="hidden" id="doc_total" name="doc_total" value="0">
            	<tr>
                	<td colspan="4" id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                    <td>
                    	<div>
                    	<input type="number" class="form-control" id="qty_total" name="qty_total" value="0" 
                        	style="text-align:right;" readonly>
                        </div>
                   	</td>
                </tr>
                <tr>
                	<td colspan="3" id="foot_label">                        
                        <input type="text" id="freight_accrue_party" name="freight_accrue_party" placeholder="Reimburse to.."/>
                    	<strong>Freight:&nbsp;&nbsp;&nbsp;</strong>
                        <select name="freight_type" id="freight_type">
	                        <option value="cust_accrue" selected="selected">Charge Customer
                        	<option value="own_accrue">Company Expense
                        </select>
                    </td>
                    <td> 
                        <div class="input-group input-group-sm">                                
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success" >
                                    <i class="fa fa-money"></i>&nbsp;&nbsp; IDR</button>                                
                            </span>
                            <input type="number" class="form-control" id="exp_freight" name="exp_freight" value="0" 
                                style="text-align:right;">
                        </div>
                   	</td>
                </tr>
                <tr>
                	<td colspan="3" id="foot_label">                    	
                        <input type="text" id="other_accrue_party" name="other_accrue_party" placeholder="Reimburse to.."/>
                    	<strong>Other Cost:&nbsp;&nbsp;&nbsp;</strong>
                        <select name="other_type" id="other_type">
	                        <option value="cust_accrue" selected="selected">Charge Customer
                        	<option value="own_accrue">Company Expense                        
                        </select>
                    </td>
                    <td> 
                    	<!---<div class="col-sm-12">--->
                        <div class="input-group input-group-sm">                                
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success" >
                                    <i class="fa fa-money"></i>&nbsp;&nbsp; IDR</button>                                
                            </span>
                            <input type="number" class="form-control" id="exp_other" name="exp_other" value="0" 
                                style="text-align:right;">
                            <!---<span class="input-group-btn">
                                <button type="button" class="btn btn-success" >
                                    <i class="fa fa-money"></i>&nbsp;&nbsp; IDR</button>                                
                            </span>--->
                        </div>
                   	</td>
                </tr>
            </tfoot>
            </table>
            </div>
    	</div>
    </div>
    <hr />

    <div class="row">
    	<div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
		</div>
    </div>
    <!---<hr />--->
	</form>	
</section>
<!-- /.content -->

<?php //LAYOUT HELPER: MODAL
	echo sw_createModal('modal_so','Select SO','dialog_sales_order_push_data();','sales/dialog_sales_order');
	echo sw_createModal('modal_customer','Select Customer','dialog_customer_push_data();','business/dialog_customer');
?>
<?php 
	//echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
var source_so = <?php echo json_encode($dialog_so);?>;
//var source_so = <?php echo json_encode($dialog_so);?>;
var source_customer = <?php echo json_encode($dialog_customer);?>;

var return_url = "<?php echo site_url('sales/sales_delivery');?>";
var submit_url = "<?php echo site_url('sales/sales_delivery_add_query');?>";

//var ajax_url_1 = "<?php echo base_url('sales/ajax_getAll_so_with_customer');?>";
var ajax_url_1 = "<?php echo base_url('sales/ajax_getAll_so_where');?>";
var ajax_url_2 = "<?php echo base_url('sales/ajax_getDetail_so_open');?>";
var ajax_url_3 = "<?php echo base_url('sales/ajax_getHeader_so');?>";

var param = "<?php echo $param;?>";

$(document).ready(function(){	
	$("#location_id").on('change',function(){
		var result;
		for( var i = 0; i < source_location.length; i++ ) {
			if( source_location[i]['location_id'] == $(this).val() ) {
				result = source_location[i];
				break;
			}
		}
		$("#ship_addr").val(result['location_address']+', '+result['location_city']+', '+result['location_state']+', '+result['location_country']);
	});
	$("#location_id").val(1).change();
	

	$("#doc_freight").on('change',function(){
		if($(this).val()== 0){ //if new location
			//void window.showModalDialog('<?php echo site_url("purchase/dialog_purchase_order/");?>/vendor/'+document.getElementById('buss_id').value);
			$("#doc_freight_new").show();
			$("#doc_freight").hide();
		}
	});	
});

</script>
<script type="text/javascript">
function get_so_detail(){
	return $.ajax({
		url:ajax_url_2,
		data:{doc_num:$("#doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
        }
	});
}
</script>
