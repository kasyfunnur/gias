<!-- Content Header (Page header) -->
<style>
	.inactive td{background-color:#CCC;opacity: 0.5;}
	thead>tr>td{text-align:center;font-weight:bold}
	#foot_label{text-align:right;}
	label{		
		-moz-user-select: -moz-none;
		-khtml-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}	
</style>
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('sales/sales_invoice');?>">Sales Invoice</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Edit </h1>
</section>

<!-- Main content -->
<section class="content">
    <?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
    <div class="row">
    	<div class="col-sm-6">
        	<input type="hidden" id="doc_ref_id" name="doc_ref_id" value="" />
            <input type="hidden" id="doc_ref_type" name="doc_ref_type" value="" />
            <div class="form-group">
                <label class="col-sm-3" for="buss_name">
                	Customer                    
                </label>
                <div class="col-sm-9">
                	<input type="hidden" id="buss_id" name="buss_id" value="">
                    <input type="hidden" id="buss_char" name="buss_char" value="">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="" readonly>                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_customer" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                   
                </div>
            </div>
            <input type="hidden" id="doc_ref_id" name="doc_ref_id" value="" />
            <input type="hidden" id="doc_ref" name="doc_ref" value="" />            
            <div class="form-group">
                <label class="col-sm-3" for="ship_addr">Customer Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr"></textarea>
                </div>
            </div>
            <!---<div class="form-group">
                <label class="col-sm-3" for="doc_ref">
                	Sales Order                    
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="so_doc_ref_id" name="so_doc_ref_id" value="" />
                    <input type="hidden" id="so_doc_ref_type" name="so_doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="so_doc_ref" name="so_doc_ref" value="" readonly>                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_so" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                                       
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_ref">
                	Sales Delivery                    
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="del_doc_ref_id" name="del_doc_ref_id" value="" />
                    <input type="hidden" id="del_doc_ref_type" name="del_doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="del_doc_ref" name="del_doc_ref" value="" readonly>
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_del" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                 
                </div>
            </div>--->
            <div class="form-group">
                <label class="col-sm-3" for="doc_ref">
                	Ref # (DO)
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="del_doc_ref_id" name="del_doc_ref_id" value="" />
                    <input type="hidden" id="del_doc_ref_type" name="del_doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="del_doc_ref" name="del_doc_ref" value="" readonly>
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_del" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                 
                </div>
            </div>
            <!---<div class="form-group">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>--->
            <!---<div class="form-group">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr" readonly></textarea>
                </div>
            </div> --->          
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                </div>
            </div> 
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $form_header[0]['doc_num'];?>" readonly>
                </div>
            </div>	
        	<div class="form-group">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo substr($form_header[0]['doc_dt'],0,10);?>">
                </div>
            </div>		
            <div class="form-group">
                <label class="col-sm-3" for="doc_ddt">Due Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>
            <!---<div class="form-group">
                <label class="col-sm-3" for="doc_ddt">DateRequired</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>" readonly>
                </div>
            </div>--->
            
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-12">
	        <div class="table-responsive">
        	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
            <thead>
            <tr>
            	<td width="5%">#</td>
                <td width="15%">Item Code
                	<!---<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                    	<i class="fa fa-search"></i>&nbsp;&nbsp; Item Code</button>--->
                </td>
                <td width="20%">Item Name</td>
                <!---<td width="20%">Open Qty</td>--->
                <td width="10%">Qty</td>
                <td width="20%">Price</td>
                <td width="25%">Total</td>
                <td width="10%">Action</td>
            </tr>
            </thead>
            <tbody>
            <tr height="30px" id="default">
            	<td style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                    <input type="hidden" name="reff_line[]" id="reff_line1" value="" />
                </td>
                <td>
                	<input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                	<input type="text" name="ast_code[]" id="ast_code1" class="form-control" readonly="readonly">
                </td>
                <td><input type="text" name="ast_name[]" id="ast_name1" class="form-control" readonly="readonly"></td>
                <td>
                	<input type="number" name="ast_qty[]" id="ast_qty1" 
                		class="form-control" style="text-align:right">
                   	<input type="hidden" name="ast_qty_open[]" id="ast_qty1" 
                    	class="form-control" readonly="readonly" style="text-align:right">
                    <input type="hidden" name="ast_del_qty[]" id="ast_del_qty1" 
                    	class="form-control" readonly="readonly" style="text-align:right">
				</td>
                <td><input type="number" name="ast_price[]"  id="ast_price1"  class="form-control" style="text-align:right"></td>
                <td><input type="number" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right"readonly></td>
                
                <td>
                	<div class="input-group">
						<div class='input-group-btn'>
                            <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                            	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
						</div>
					</div>
				</td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><div>
                    <input type="number" class="form-control" id="qty_total" name="doc_total" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
                <td id="foot_label"><strong>Sub Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_subtotal" name="doc_subtotal" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td id="foot_label" style="text-align:right;">
                	<input type="number" class="col-sm-5" id="doc_disc_pct" name="doc_disc_pct"
                    	value="0" style="text-align:right;" /> %&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                	<strong>Discount:&nbsp;&nbsp;&nbsp;</strong>
                </td>
                <td><div>
                    <input type="text" class="form-control" id="doc_disc" name="doc_disc" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <!---FREIGHT N OTHER COST--->
            <tr>
                <td colspan="5" id="foot_label"><strong>Freight:&nbsp;&nbsp;&nbsp;</strong></td>
                <td> 
                    <div>
                        <input type="text" class="form-control" id="exp_freight" name="exp_freight" value="0" 
                            style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="5" id="foot_label"><strong>Other Cost:&nbsp;&nbsp;&nbsp;</strong></td>
                <td> 
                    <div>
                        <input type="text" class="form-control" id="exp_other" name="exp_other" value="0" 
                            style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            
            <tr>
                <td colspan="5" id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_total" name="doc_total" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            
            
            </tfoot>
            </table>
            </div>
    	</div>
    </div>
    <hr />
    <!---<div class="row">
    	<div class="col-sm-12">
            <table class="table-bordered" id="table_expense">
                <thead>
                <tr>
                    <td width="10%">#</td>
                    <td width="45%">Expense</td>
                    <td width="25%">Amount</td>
                    <td width="20%">Action</td>
                </tr>
                </thead>
                <tbody>
                	<tr height="30px">
                        <td style="text-align:center">
                        	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                        </td>
                        <td><input type="text" name="ast_qty_open[]" id="ast_qty1" 
	                    	class="form-control">
                       	</td>
                        <td><input type="number" name="ast_qty_open[]" id="ast_qty1" 
                    		class="form-control" style="text-align:right">
                        </td>
                        <td>
                        	<div style='text-align:center'>
                                <div class='btn-group'>
                                    <button type="button" class="btn btn-flat btn-xs btn-danger" id="act_del1" name="act_del">DELETE</button>
                                </div>
                            </div>
						</td>
                    </tr>
                </tbody>
			</table>
        </div>
    </div>--->
    <!---<br />--->
    <div class="row">
    	<div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
		</div>
    </div>
    <!---<hr />--->
	</form>	
</section>
<!-- /.content -->

<?php //LAYOUT HELPER: MODAL
	echo sw_createModal('modal_customer','Select Customer','dialog_customer_push_data();','business/dialog_customer');
	echo sw_createModal('modal_so','Select SO','dialog_sales_order_push_data();','sales/dialog_sales_order');
	echo sw_createModal('modal_del','Select Delivery','dialog_sales_delivery_push_data();','sales/dialog_sales_delivery');
?>

<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
//var source_so = <?php //echo json_encode($dialog_so);?>;
var source_del = <?php echo json_encode($dialog_del);?>;
var source_so = <?php echo json_encode($dialog_so);?>;
var source_customer = <?php echo json_encode($dialog_customer);?>;

var form_header = <?php echo json_encode($form_header);?>;
var form_detail = <?php echo json_encode($form_detail);?>;

var status_financial = "<?php echo $form_header[0]['financial_status'];?>";

var return_url = "<?php echo site_url('sales/sales_invoice');?>";
var submit_url = "<?php echo site_url('sales/sales_invoice_add_query');?>";
var ajax_url_1 = "<?php echo base_url('sales/ajax_getAll_del_with_customer');?>";
var ajax_url_2 = "<?php echo base_url('sales/ajax_getDetail_del_open');?>";
var ajax_url_3 = "<?php echo base_url('sales/ajax_getHeader_del');?>";
//var ajax_url_4 = "<?php echo base_url('sales/ajax_getAll_so_with_customer');?>";
var ajax_url_4 = "<?php echo base_url('sales/ajax_getAll_so_where');?>";
var ajax_url_5 = "<?php echo base_url('sales/ajax_getDetail_so');?>";
var ajax_url_6 = "<?php echo base_url('sales/ajax_getHeader_so');?>";

//var param = "<?php //echo $param;?>";

$(document).ready(function(){	
	
});

</script>
<script type="text/javascript">
function get_del_detail(){
	$.ajax({
		url:ajax_url_3,
		data:{doc_num:$("#del_doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response); 
			//table_detail_reset('table_detail');
			form_header_insert(data);			
        }
	});
	$.ajax({
		url:ajax_url_2,
		data:{doc_num:$("#del_doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
        }
	});
	//return true;
}

function get_so_header(){
	$.ajax({
		url:ajax_url_6,
		data:{doc_num:$("#so_doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			form_header_insert(data);
        }
	});
}
function get_so_detail(){ 
	
	$.ajax({
		url:ajax_url_5,
		data:{doc_num:$("#so_doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);
        }
	});
}
</script>
