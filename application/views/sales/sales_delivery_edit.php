<!-- Content Header (Page header) -->
<style>
	.inactive td{background-color:#CCC;opacity: 0.5;}
	thead>tr>td{text-align:center;font-weight:bold}
	#foot_label{text-align:right;}
	label{		
		-moz-user-select: -moz-none;
		-khtml-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}	
</style>
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('sales/sales_delivery');?>">Sales Delivery</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Edit </h1>
</section>

<!-- Main content -->
<section class="content">
	<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
    <div class="row">
    	<div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-3" for="buss_name">
                	Customer                   
                </label>
                <div class="col-sm-9">
	                <input type="hidden" id="buss_id" name="buss_id" value="<?php echo $form_header[0]['buss_id'];?>">
                    <input type="hidden" id="buss_char" name="buss_char" value="<?php echo $form_header[0]['buss_char'];?>">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" readonly
                        	value="<?php echo $form_header[0]['buss_name'];?>">                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_customer" style="width:100%;"disabled="disabled">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>                       
                    </div>                    
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="cust_phone">Customer Phone</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" id="cust_phone1" name="cust_phone1" placeholder="Phone 1">
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-4">
					<input type="text" class="form-control input-sm" id="cust_phone2" name="cust_phone2" placeholder="Phone 2">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="ship_addr">Customer Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_ref">
                	Sales Order                    
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="doc_ref_id" name="doc_ref_id" value="" />
                    <input type="hidden" id="doc_ref_type" name="doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="doc_ref" name="doc_ref" 
                        	value="<?php echo $form_header[0]['doc_ref'];?>" readonly="readonly">                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_so" style="width:100%;" disabled="disabled">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                                      
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name', $form_header[0]['whse_id']);?>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $form_header[0]['doc_num'];?>" readonly>
                </div>
            </div>	
            <div class="form-group">
                <label class="col-sm-3" for="doc_ext_ref">Freight Doc #</label>
                <div class="col-sm-3">
                	<input class="form-control input-sm" type="text" placeholder="New Courier"
                    	name="doc_freight_new" id="doc_freight_new" style="display:none;">
                	<?php array_push($ddl_freight,array('freight_code'=>"0",'doc_freight'=>"New..."));
							echo sw_CreateSelect('doc_freight', $ddl_freight, 'freight_code', 'doc_freight', $form_header[0]['doc_freight']);
							//echo sw_CreateSelect('doc_freight', $ddl_freight, 'doc_freight', 'doc_freight');?>
                </div>
                <div class="col-sm-6">
                    <input type="text" class="form-control input-sm" id="doc_ext_ref" name="doc_ext_ref" value="<?php echo $form_header[0]['doc_ext_ref'];?>">
                </div>
            </div>
        	<div class="form-group">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo substr($form_header[0]['doc_dt'],0,10);?>">
                </div>
            </div>		
            <div class="form-group">
                <label class="col-sm-3" for="doc_ddt">Date Required</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo substr($form_header[0]['doc_ddt'],0,10);?>">
                </div>
            </div>	
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"><?php echo $form_header[0]['doc_note'];?></textarea>
                </div>
            </div>	
                    
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-12">
        	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
            <thead>
            <tr>
            	<td width="5%">#</td>
                <td width="15%">Item Code</td>                
                <td width="15%">Item Name</td>
                <td width="20%">Open Qty</td>
                <td width="20%">Qty</td>
                <!---<td width="15%">Price</td>--->
                <td width="10%">Action</td>
            </tr>
            </thead>
            <!---style="overflow:auto;height:400px;"--->
            <tbody>
            <?php //foreach($form_detail as $detail){?>
            <?php //var_dump($detail);?>
            	<tr height="30px" id="default">                
                <!---<td style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="<?php echo $detail['doc_line'];?>" readonly/>
                    <input type="hidden" name="reff_line[]" id="reff_line1" value="" />
                </td>
                <td>
                	<input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                	<input type="text" name="ast_code[]" id="ast_code1" class="form-control" readonly="readonly" value="<?php echo $detail['item_code'];?>">
                </td>
                <td><input type="text" name="ast_name[]" id="ast_name1" class="form-control" readonly="readonly" value="<?php echo $detail['item_name'];?>"></td>
                <td>
                	<input type="hidden" name="ast_so_qty[]" id="ast_so_qty1" />
                    <input type="hidden" name="ast_price[]" id="ast_price1" />
                	<input type="number" name="ast_qty_open[]" id="ast_qty1" 
                    	class="form-control" readonly="readonly" style="text-align:right" value="<?php echo $detail['item_qty'];?>">                
                </td>
                <td>
                	<input type="number" name="ast_qty[]" id="ast_qty1" 
                		class="form-control" style="text-align:right">
				</td>
                <td>
                	<div style='text-align:center'>
						<div class='btn-group'>
                            <button type="button" class="btn btn-flat btn-xs btn-danger" id="act_del1" name="act_del">DELETE</button>
						</div>
					</div>
				</td>--->
                
                
                
                <td style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="" readonly/>
                    <input type="hidden" name="reff_line[]" id="reff_line1" value="" />
                </td>
                <td>
                	<input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                	<input type="text" name="ast_code[]" id="ast_code1" class="form-control" readonly="readonly" value="">
                </td>
                <td><input type="text" name="ast_name[]" id="ast_name1" class="form-control" readonly="readonly" value=""></td>
                <td>
                	<input type="hidden" name="ast_so_qty[]" id="ast_so_qty1" />
                    <input type="hidden" name="ast_price[]" id="ast_price1" />
                	<input type="number" name="ast_qty_open[]" id="ast_qty1" 
                    	class="form-control" readonly="readonly" style="text-align:right" value="">                
                </td>
                <td>
                	<input type="number" name="ast_qty[]" id="ast_qty1" 
                		class="form-control" style="text-align:right">
				</td>
                <td>
                	<div class="input-group">
						<div class='input-group-btn'>
                            <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                            	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
						</div>
					</div>
				</td>
                
                
                
                
                
                
            	<!---<td><?php //echo $detail['doc_line'];?></td>
                <td>
                	<?php //echo sw_CreateSelect('ast_group1',$ddl_asset_group,'group_id','group_name',$detail['item_group'],array('initialvalue'=>'-1','initialdisplay'=>'Select..'));?>
                </td>
                <td><input type="text" name="ast_name1" id="ast_name1" class="form-control" value="<?php //echo $detail['item_name'];?>"></td>
                <td><input type="number" name="ast_qty1"  id="ast_qty1"  class="form-control" value="<?php //echo $detail['item_qty'];?>"></td>
                <td><input type="text" name="ast_price1"  id="ast_price1"  class="form-control" value="<?php //echo $detail['item_price'];?>"></td>
                <td><input type="text" name="sub_total1"  id="sub_total1"  class="form-control"  value="<?php //echo $detail['item_total'];?>"readonly></td>--->
            </tr>
			<?php //}?>
            <!---<tr height="30px" class="inactive">
            	<td><?php echo count($form_detail)+1;?></td>
                <td>
                	<?php echo sw_CreateSelect('ast_group1',$ddl_asset_group,'group_id','group_name',NULL,array('initialvalue'=>'-1','initialdisplay'=>'Select..'));?>
                </td>
                <td><input type="text" name="ast_name1" id="ast_name1" class="form-control"></td>
                <td><input type="number" name="ast_qty1"  id="ast_qty1"  class="form-control"></td>
                <td><input type="text" name="ast_price1"  id="ast_price1"  class="form-control"></td>
                <td><input type="text" name="sub_total1"  id="sub_total1"  class="form-control" readonly></td>
            </tr>--->
            </tbody>
            <tfoot>
            	<tr>
                	<td colspan="4" id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                    <td>
                    	<div>
                    	<input type="number" class="form-control" id="qty_total" name="qty_total" value="0" 
                        	style="text-align:right;" readonly>
                        </div>
                   	</td>
                </tr>
                <tr>
                	<td colspan="3" id="foot_label">
                    	<input type="text" id="freight_accrue_party" 
                        	name="freight_accrue_party" placeholder="Reimburse to.."
                            value="<?php echo $form_header[0]['doc_cost_freight_party'];?>"/>
                    	<strong>Freight:&nbsp;&nbsp;&nbsp;</strong>
                        <!---<select name="freight_type" id="freight_type">
	                        <option value="cust_accrue" selected="selected">Charge Customer
                        	<option value="own_accrue">Company Expense
                        </select>--->
                        <?php echo form_dropdown('freight_type', $options = array('cust_accrue'=>'Charge Customer','own_accrue'=>'Company Expense'), array($form_header[0]['doc_cost_freight_type']));?>
                    </td>
                    <td> 
                        <div class="input-group input-group-sm">                                
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success" >
                                    <i class="fa fa-money"></i>&nbsp;&nbsp; IDR</button>                                
                            </span>
                            <input type="number" class="form-control" id="exp_freight" name="exp_freight" value="0" 
                                style="text-align:right;">
                        </div>
                   	</td>
                </tr>
                <tr>
                	<td colspan="3" id="foot_label">
                    	<input type="text" id="other_accrue_party" 
                        	name="other_accrue_party" placeholder="Reimburse to.." 
                            value="<?php echo $form_header[0]['doc_cost_other_party'];?>"/>
                    	<strong>Other Cost:&nbsp;&nbsp;&nbsp;</strong>
                        <!---<select name="other_type" id="other_type">
	                        <option value="cust_accrue" selected="selected">Charge Customer
                        	<option value="own_accrue">Company Expense                        
                        </select>--->
                        <?php echo form_dropdown('other_type', $options = array('cust_accrue'=>'Charge Customer','own_accrue'=>'Company Expense'), array($form_header[0]['doc_cost_other_type']));?>
                    </td>
                    <td> 
                    	<!---<div class="col-sm-12">--->
                        <div class="input-group input-group-sm">                                
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success" >
                                    <i class="fa fa-money"></i>&nbsp;&nbsp; IDR</button>                                
                            </span>
                            <input type="number" class="form-control" id="exp_other" name="exp_other" value="0" 
                                style="text-align:right;">
                            <!---<span class="input-group-btn">
                                <button type="button" class="btn btn-success" >
                                    <i class="fa fa-money"></i>&nbsp;&nbsp; IDR</button>                                
                            </span>--->
                        </div>
                   	</td>
                </tr>
            </tfoot>
            </table>
    	</div>
    </div>
    <br />
    <!---<button type="submit" class="btn">Submit</button>--->
    <div class="row">
    	<div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
		</div>
    </div> 
	</form>
	
</section>
<!-- /.content -->

<?php //LAYOUT HELPER: MODAL
	echo sw_createModal('modal_so','Select SO','dialog_sales_order_push_data();','sales/dialog_sales_order');
	echo sw_createModal('modal_customer','Select Customer','dialog_customer_push_data();','business/dialog_customer');
?>

<script type="text/javascript">
var source_location = <?php echo json_encode($ddl_whse);?>;
var source_customer = <?php echo json_encode($dialog_customer);?>;
var source_inventory = <?php echo json_encode($dialog_inventory);?>;

var form_header = <?php echo json_encode($form_header);?>;
var form_detail = <?php echo json_encode($form_detail);?>;

var return_url = "<?php echo site_url('sales/sales_delivery');?>";
var submit_url = "<?php echo site_url('sales/sales_delivery_edit_query');?>/"+form_header[0]['doc_num'];
//var ajax_url_1 = "<?php echo base_url('sales/ajax_getAll_so_with_customer');?>";
var ajax_url_1 = "<?php echo base_url('sales/ajax_getAll_so_where');?>";
var ajax_url_2 = "<?php echo base_url('sales/ajax_getDetail_so_open');?>";
var ajax_url_3 = "<?php echo base_url('sales/ajax_getHeader_so');?>";

$(document).ready(function(){	
	$("#doc_freight").on('change',function(){
		if($(this).val()== 0){ //if new location
			//void window.showModalDialog('<?php echo site_url("purchase/dialog_purchase_order/");?>/vendor/'+document.getElementById('buss_id').value);
			$("#doc_freight_new").show();
			//$("#doc_freight").hide();
		}
	});	
});
</script>
<script type="text/javascript">
function get_so_detail(){
	return $.ajax({
		url:ajax_url_2,
		data:{doc_num:$("#doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
        }
	});
}
</script>
