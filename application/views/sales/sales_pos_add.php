<!-- FULL POS Interface Here -->
<form name="form_pos" id="form_pos" method="post" action="<?php echo site_url('sales/pos_submit') ?>" target="_blank">

<input type="hidden" name="cashier_id" value="<?php echo $cashier['cashier_id'] ?>">
<input type="hidden" name="location_id" value="<?php echo $cashier['location_id'] ?>">

<!-- header and save or cancel button -->
<div class="row">
	<div class="col-xs-5 text-center">
		<h3><?php echo 'Store : ' . $cashier['location_name'] . ' - Cashier : ' . $cashier['cashier_code'] ?></h3>
	</div>
	<div class="col-xs-7">
		<button type="button" class="btn btn-default btn-lg col-xs-3" onclick="pos_reset_form(true)">Reset</button>
		<button type="button" class="btn btn-success btn-lg col-xs-6 col-xs-offset-3" onclick="pos_submit_form()">Save</button>
	</div>
</div>
<div class="row" style="padding-right:5px;">
	<!-- sold item table -->
	<div class="col-md-5" id="pos_item_list" style="background-color: white; height: 450px; overflow-y: auto">
		<div class="col-xs-2" style="font-size:0.95em"><strong>Code</strong></div>
		<div class="col-xs-3" style="font-size:0.95em"><strong>Name</strong></div>
		<div class="col-xs-3 text-right" style="font-size:0.95em"><strong>Price x Qty</strong></div>
		<div class="col-xs-3 text-right" style="font-size:0.95em"><strong>Total Price</strong></div>
		<div class="col-xs-1 text-right">&nbsp;</div>
	</div>

	<!-- barcode input -->
	<div class="col-md-7">
		<!-- Main POS Interface to scan barcode -->
		<div class="input-group">
			<input class="form-control" type="text" placeholder="Barcode" id="input_code">
			<span class="input-group-btn">
				<button type="button" class="btn btn-warning btn-md" onclick="clear_barcode_input()"> X </button>
			</span>
		</div>

		<!-- Main POS Interface to display scanned item -->
		<div class="row" style="margin-top:8px;">
			<div class="col-xs-12">
				<div class="box box-danger">
				  <div class="box-header with-border">
				    <h4 class="box-title" id="barcode_placeholder">...</h4>
				    <div class="box-tools pull-right">
				      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				    </div><!-- /.box-tools -->
				  </div><!-- /.box-header -->
				  <div class="box-body">
				    <div class="row">
				    	<div class="col-xs-4" id="image_placeholder"></div>
						<div class="col-md-8" id="item_placeholder"></div>
				    </div>
				  </div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
			
		</div>


		<!-- Main POS Interface to scan promotional code or voucher -->
		<!-- 
		<div class="row" style="margin-top:8px;">
			<div class="col-md-4"><h4>Promotional Code</h4></div>
			<div class="col-md-8">
				<div class="input-group">
					<input class="form-control" type="text" placeholder="Code" id="input_promo_code">
					<span class="input-group-btn">
						<button type="button" class="btn btn-warning btn-md" onclick="clear_promocode_input()"> X </button>
					</span>
				</div>
			</div>
		</div>
		--> <!-- remark for now -->

		<!-- Main POS Interface to add multiple payment methods -->
		<div class="row">
			<div class="col-xs-12"><h3>Add Payment</h3></div>
			<div class="row">
				<!--
				<div class="col-xs-3"><button type="button" class="btn btn-lg btn-success btn-block">CASH</button></div>
				<div class="col-xs-3"><button type="button" class="btn btn-lg btn-success btn-block">CREDIT CARD</button></div>
				<div class="col-xs-3"><button type="button" class="btn btn-lg btn-success btn-block">DEBIT CARD</button></div>
				<div class="col-xs-3"><button type="button" class="btn btn-lg btn-success btn-block">VOUCHER</button></div>
				-->
				<div class="col-md-12">
					<!-- payment tab panel -->
					<div role="tabpanel" style="margin-left:10px;">

					  <!-- Nav tabs -->
					  <ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active"><a href="#cash" aria-controls="cash" role="tab" data-toggle="tab"><i class="fa fa-money"></i> CASH</a></li>
					    <li role="presentation"><a href="#creditcard" aria-controls="creditcard" role="tab" data-toggle="tab"><i class="fa fa-credit-card"></i> CREDIT CARD</a></li>
					    <li role="presentation"><a href="#debitcard" aria-controls="debitcard" role="tab" data-toggle="tab"><i class="fa fa-credit-card"></i> DEBIT CARD</a></li>
					    <li role="presentation"><a href="#voucher" aria-controls="voucher" role="tab" data-toggle="tab"><i class="fa fa-barcode"></i> VOUCHER</a></li>
					  </ul>

					  <!-- Tab panes -->
					  <div class="tab-content">
					  	<!-- cash payment content -->
					    <div role="tabpanel" class="tab-pane active" id="cash">
					    	<div class="row">
					    		<div class="col-xs-10">
							    	<div class="input-group">
									  <span class="input-group-addon">Cash - Rp</span>
									  <input type="text" class="form-control text-right" placeholder="Cash Nominal" name="input_pay_cash_amount" id="input_pay_cash_amount">

									  <span class="input-group-btn">
									  	<button class="btn btn-primary" type="button" onclick="add_payment('cash')"><i class="fa fa-plus-square"></i> Add</button>
									  </span>
									</div>
								</div>
							</div>
					    </div>

					    <!-- credit card payment content -->
					    <div role="tabpanel" class="tab-pane" id="creditcard">
					    	<div class="row">
					    		<div class="col-xs-10">
							    	<div class="input-group">
									  <span class="input-group-addon">Magnetic Number</span>
									  <input type="text" class="form-control" placeholder="Scan using magnetic reader" name="input_pay_cc_magnet" id="input_pay_cc_magnet">
									</div>
									<div class="input-group">
									  <span class="input-group-addon">Bank</span>
									  <select class="form-control" name="input_pay_cc_bank" id="input_pay_cc_bank">
									  	<option value="bca">BCA</option>
									  	<option value="bca">Mandiri</option>
									  	<option value="bca">BNI</option>
									  	<option value="bca">Bukopin</option>
									  	<option value="bca">Danamon</option>
									  	<option value="bca">BRI</option>
									  </select>
									  
									  <select class="form-control" name="input_pay_cc_card" id="input_pay_cc_card">
									  	<option value="visa">Visa</option>
									  	<option value="Master">Master</option>
									  </select>
									  
									</div>
									<div class="input-group">
									  <span class="input-group-addon">Name</span>
									  <input type="text" class="form-control" placeholder="Name on Card" name="input_pay_cc_name" id="input_pay_cc_name">
									</div>
									<div class="input-group">
									  <span class="input-group-addon">EDC Trx Number</span>
									  <input type="text" class="form-control" placeholder="EDC Transaction or Approval Number" name="input_pay_cc_trx" id="input_pay_cc_trx">
									</div>
									<div class="input-group">
									  <span class="input-group-addon">Amount</span>
									  <input type="text" class="form-control text-right" placeholder="Amount" name="input_pay_cc_amount" id="input_pay_cc_amount">
									</div>
									<button class="btn btn-primary pull-right" type="button"><i class="fa fa-plus-square"></i> Add</button>
								</div>
							</div>
					    </div>

					    <!-- debit card payment content -->
					    <div role="tabpanel" class="tab-pane" id="debitcard">
					    	<div class="row">
					    		<div class="col-xs-10">
							    	<div class="input-group">
									  <span class="input-group-addon">Magnetic Number</span>
									  <input type="text" class="form-control" placeholder="Scan using magnetic reader" name="input_pay_dc_magnet" id="input_pay_dc_magnet">
									</div>
									<div class="input-group">
									  <span class="input-group-addon">Bank</span>
									  <select class="form-control" name="input_pay_dc_bank" id="input_pay_dc_bank">
									  	<option value="bca">BCA</option>
									  	<option value="bca">Mandiri</option>
									  	<option value="bca">BNI</option>
									  	<option value="bca">Bukopin</option>
									  	<option value="bca">Danamon</option>
									  	<option value="bca">BRI</option>
									  </select>
									</div>
									<div class="input-group">
									  <span class="input-group-addon">Name</span>
									  <input type="text" class="form-control" placeholder="Name on Card" name="input_pay_dc_name" id="input_pay_dc_name">
									</div>

									<div class="input-group">
									  <span class="input-group-addon">EDC Trx Number</span>
									  <input type="text" class="form-control" placeholder="EDC Transaction or Approval Number" name="input_pay_dc_trx" id="input_pay_dc_trx">
									</div>
									
									<div class="input-group">
									  <span class="input-group-addon">Amount</span>
									  <input type="text" class="form-control text-right" placeholder="Amount" name="input_pay_dc_amount" id="input_pay_dc_amount">
									</div>

									<button class="btn btn-primary pull-right" type="button"><i class="fa fa-plus-square-o"></i> Add</button>
								</div>
							</div>
					    </div>

					    <!-- voucher code payment content -->
					    <div role="tabpanel" class="tab-pane" id="voucher">
					    	<div class="row">
					    		<div class="col-xs-10">
							    	<div class="input-group" style="max-width:100%">
									  <span class="input-group-addon">Voucher Number</span>
									  <input type="text" class="form-control" placeholder="Voucher Code..." name="input_pay_vc_code" id="input_pay_vc_code">
									  <span class="input-group-btn">
									  	<button class="btn btn-default" type="button"><i class="fa fa-plus-square-o"></i></button>
									  </span>
									</div>
								</div>
							</div>
					    </div>
					  </div>

					</div>

					<!-- end of payment tab panel -->
				</div>
			</div>
		</div>

		<!-- Main POS Interface to hold payments added -->
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-default">
				  <div class="box-header with-border">
				    <h3 class="box-title">Payment List</h3>
				    <div class="box-tools pull-right">
				      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				    </div><!-- /.box-tools -->
				  </div><!-- /.box-header -->

				  <div class="box-body" id="div_payment_list"></div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>

	</div>

</div>

<!-- total row and buttons-->
<div class="row" style="padding-right:5px; position:fixed;bottom:0;right:0;width:100%;z-index:1000;">

<!-- total item and price -->
	<div class="col-md-5" style="background-color: #02005C; color: white; font-size: 2em;">
		<div class="row">
			<div class="col-xs-6 text-center" id="div_item_count"></div>
			<div class="col-xs-6 text-right" id="div_total_sales">Rp 0</div>
		</div>
	</div>

<!-- total payment and change -->
	<div class="col-md-7" style="background-color: #1E6C00; color: white; font-size: 2em;">
		<div class="row">
			<div class="col-xs-6 text-right" id="div_total_payment">...</div>
			<div class="col-xs-6 text-right" id="div_change" style="background-color:#E39E19">...</div>
		</div>
	</div>
</div>

</form>
<script type="text/javascript">
	var get_item_url = '<?php echo site_url('sales/pos_get_item') ?>';
	var get_promo_url = '<?php echo site_url('sales/pos_get_promo') ?>';
	var get_voucher_url = '<?php echo site_url('sales/pos_get_voucher') ?>';
	
	var site_url = '<?php echo site_url(); ?>';
	var base_url = '<?php echo base_url(); ?>';
</script>