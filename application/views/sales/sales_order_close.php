<!-- Content Header (Page header) -->
<style>
	.inactive td{background-color:#CCC;opacity: 0.5;}
	thead>tr>th{text-align:center;font-weight:bold}
	#foot_label{text-align:right;}
	/*tbody{padding-left:1px;padding-right:1px;}opacity: 0.5;*/
</style>
<?php // STATUS MODEL
	//$status_attr = $this->purchase_order_model->logistic_status($form_header[0]['logistic_status']);
	$logistic_status = $this->status_model->status_icon('logistic',$form_header[0]['logistic_status']);
	$billing_status = $this->status_model->status_icon('billing',$form_header[0]['billing_status']);
	$finance_status = $this->status_model->status_icon('financial',$form_header[0]['financial_status']);
?>

<section class="content-header">
    <h1><a class="" href="<?php echo site_url('sales/sales_order');?>">Sales Order</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        CLOSING... </h1>   
</section>

<!-- Main content -->
<section class="content">	
    <?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <h4><?php echo $logistic_status;?>
            <?php echo $billing_status;?>
            <?php //echo $finance_status;?></h4>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-6">
        	<!---<div class="form-group">
            	<label class="col-sm-3">Status</label>
                <div class="col-sm-9">
					<?php echo $logistic_status;?>
                    <?php echo $billing_status;?>
                    <?php echo $finance_status;?>
                </div>
            </div>--->
            <div class="form-group">
                <label class="col-sm-3" for="buss_name">
                	Customer                   
                </label>
                <div class="col-sm-9">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" readonly="readonly"		
                        	value="<?php echo $form_header[0]['buss_name'];?>">
                        <input type="hidden" id="buss_id" name="buss_id" value="<?php echo $form_header[0]['buss_id'];?>">                   	
                        <span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_customer" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                </div>
            </div>            
            </div>
            <!---<div class="form-group">
                <label class="col-sm-3" for="doc_ref">Doc Ref</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_ref" name="doc_ref" >
                </div>
            </div>--->
            
            <div class="form-group">
                <label class="col-sm-3" for="cust_phone">Customer Phone</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" id="cust_phone" name="cust_phone" placeholder="Phone 1" 
                    	value="<?php echo $form_header[0]['buss_tlp1'];?>">
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-4">
					<input type="text" class="form-control input-sm" id="cust_phone2" name="cust_phone2" placeholder="Phone 2"
                    	value="<?php echo $form_header[0]['buss_tlp2'];?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="ship_addr">Customer Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr"><?php echo $form_header[0]['ship_addr'];?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"><?php echo $form_header[0]['doc_note'];?></textarea>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $form_header[0]['doc_num'];?>" readonly>
                    <input type="hidden" id="doc_id" name="doc_id" value="<?php echo $form_header[0]['doc_id'];?>" />
                </div>
            </div>	
        	<div class="form-group">
            	<!---<div class="row">--->
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                        <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo substr($form_header[0]['doc_dt'],0,10);?>">
                    </div>
				<!---</div>--->
            </div>		
            <div class="form-group">
            	<!---<div class="row">--->
                    <label class="col-sm-3" for="doc_ddt">Date Required</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo substr($form_header[0]['doc_ddt'],0,10);?>">
                </div>
				<!---</div>--->
            </div>		
			<div class="form-group">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name', $form_header[0]['whse_id']);?>
                </div>
            </div>
                </div>
            </div>		                    
    
    <div class="row">
    	<div class="col-sm-12">
			<a href="javascript:$('#table_detail').toggle();">Collapse/Expand</a>
        	<table class="table-bordered table-responsive" id="table_detail">
            <thead>
            <tr>
            	<th style="width:5%">#
                	<!---<button type="button" class="btn btn-warning form-control" >#</button> --->               
                </th>
                <th style="width:15%">
                	<!---<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                        <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Item Code</span>
						<span class="visible-xs"><i class="fa fa-search"></i></span>
                    </button>--->
                    Item Code
                </th>
                <th style="width:15%">Item Name</th>
                <th style="width:15%">Quantity</th>
                <th style="width:20%">Price</th>
                <th style="width:20%">Total</th>
                <th style="width:10%" class="action">Action</th>            
            </tr>
            </thead>
            <tbody>
            <tr height="30px">
            	<td class="input-group hidden-xs col-sm-1" style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                </td>
                <td class="input-group col-sm-2 col-xs-3">
                	<input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                	<input type="text" name="ast_code[]" id="ast_code1" class="form-control" readonly="readonly">
                </td>
                <td class="input-group hidden-xs col-sm-2">
                	<input type="text" name="ast_name[]" id="ast_name1" class="form-control">
                </td>
                <td class="input-group col-sm-1 col-xs-3">
                	<input type="number" name="ast_qty[]"  id="ast_qty1"  class="form-control" style="text-align:right">
                </td>
                <td class="input-group col-sm-2 col-xs-2">
                	<input type="text" name="ast_price[]"  id="ast_price1"  class="form-control" style="text-align:right">
                </td>
                <td class="input-group col-sm-2 col-xs-2">
                	<input type="text" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right"readonly>
                </td>
                <td class="input-group col-sm-2 col-xs-1">
                	<div style='text-align:center'>
						<div class='btn-group'>
                            <button type="button" class="btn btn-flat btn-danger" id="act_del1" name="act_del"><!---DELETE--->
                            	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
						</div>
					</div>
				</td>                
            </tr>            
            </tbody>
            <tfoot>
            	<!---<tr>
                	<td class="hidden-xs col-sm-1" style="text-align:center"></td>
                    <td class="hidden-xs col-sm-2"></td>
                    <td class="col-sm-2 col-xs-3"></td>
                    <td class="col-sm-1 col-xs-1"></td>
                    <td class="input-group col-sm-2 col-xs-2" id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                    <td class="input-group col-sm-2 col-xs-3"><div>
                    	<input type="text" class="form-control" id="doc_total" name="doc_total" value="0" style="text-align:right;" readonly>
                        </div>
                    </td>
                </tr>--->
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><div>
                    <input type="number" class="form-control" id="qty_total" name="qty_total" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
                <td id="foot_label"><strong>Sub Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_subtotal" name="doc_subtotal" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td id="foot_label" style="text-align:right;">
                	<input type="number" class="col-sm-5" id="doc_disc_pct" name="doc_disc_pct"
                    	value="0" style="text-align:right;" /> %&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                	<strong>Discount:&nbsp;&nbsp;&nbsp;</strong>
                </td>
                <td><div>
                    <input type="text" class="form-control" id="doc_disc" name="doc_disc" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_total" name="doc_total" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            </tfoot>
            </table>
        </div>
    </div>
    <hr />
    
    <div class="row">
    	<div class="col-sm-3">Reason for closing document:</div>
        <div class="col-sm-9">
    	<textarea required name="reason" id="reason" style="width:500px"/></textarea>
        </div>
    </div>
    
    <br />
    <button type="submit" class="btn btn-info pull-right">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
    <br />
	</form>	
    
</section>
<!-- /.content -->

<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script>
var form_header = <?php echo json_encode($form_header);?>;
var form_detail = <?php echo json_encode($form_detail);?>;

var status_logistic = "<?php echo $form_header[0]['logistic_status'];?>";
var status_financial = "<?php echo $form_header[0]['financial_status'];?>";

var return_url = "<?php echo site_url('sales/sales_order');?>";
var submit_url = "<?php echo site_url('sales/sales_order_close_query');?>/"+'<?php echo $form_header[0]['doc_num'];?>';

function sky_count_doc_summary(){
	var var_subtotal 	= $("#doc_subtotal").val();
	var pct 			= $("#doc_disc_pct").val();
	$("#doc_disc").val((pct/100)*$("#doc_subtotal").val());
	var var_disc 		= $("#doc_disc").val();
	var var_total		= $("#doc_total").val(var_subtotal - var_disc);
}

function validateForm(){
	if(confirm("Are you sure?")==true){		
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Document has been closed successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}

$(document).ready(function(){
//ON EDIT PAGE
	$("#location_id").val(form_header[0]['whse_id']).change();
	$("#buss_id").val(form_header[0]['buss_id']);
	$("#buss_name").val(form_header[0]['buss_name']);
	$("#doc_ref").val(form_header[0]['doc_num']);
	$("#doc_note").val(form_header[0]['doc_note']);
	$("#doc_disc_pct").val(form_header[0]['doc_disc_percent']);
	$("#doc_pct").val(form_header[0]['doc_disc_amount']);
	
	
	if(status_logistic != "PENDING"){
		$('#buss_name,#location_id,#doc_note,#doc_dt,#doc_ddt').css('color','#000');
		$('input,button,select,textarea').attr('disabled',true);
	}
	//CANCEL DOCUMENT	
	$('#buss_name,#location_id,#doc_note,#doc_dt,#doc_ddt').css('color','#000');
	$('input,button,select,textarea').attr('disabled',true);
	$("textarea[name=reason],button[type=submit]").attr('disabled',false);
//	$("input[type=submit]").attr('disabled',false);
	

	if(form_detail){
		if($('#table_detail > tbody > tr').length != 1){
			$('#table_detail > tbody > tr:last').remove();
		}
		for(var p = 0;p<form_detail.length;p++){
			var row = clone_row("table_detail");		
			row.find('td:eq(1)').find('input[name^=ast_code]').val(form_detail[p]['item_code']);
			row.find('td:eq(1)').find('input[type=hidden]').val(form_detail[p]['item_id']);
			//row.find('td:eq(2)').find('input').val(form_detail[p]['item_name']);
			row.find('td:eq(2)').find('input').val(form_detail[p]['item_name_so']);
			row.find('td:eq(3)').find('input').val(form_detail[p]['item_qty']);
			//row.find('td:eq(4)').find('input').val(form_detail[p]['item_sell_price']);
			row.find('td:eq(4)').find('input').val(form_detail[p]['item_price']);
			sky_count_row_total('table_detail',row,'ast_qty','ast_price','sub_total');	
			//var new_row = $('#table_detail > tbody > tr:last').remove();
		}
		var row = clone_row("table_detail");
		if($('#table_detail > tbody > tr:first').find('td:eq(1) > input').val() == ""){			
			$('#table_detail > tbody > tr:first').remove();
		}
		sky_tbl_refresh_row_num("table_detail");
		sky_count_col_total('table_detail','doc_subtotal','sub_total[]');
		sky_count_col_total('table_detail','qty_total','ast_qty[]');
		sky_count_doc_summary();		
	}else{
		console.log('error');	
	}
	$('#table_detail').hide();
});
</script>
