<section class="content-header">
    <h1>Sales > POS</h1>
</section>
<?php echo (isset($msg))?$msg: ""?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header"></div>
        <div class="box-body table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Cashier - Location</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ($cashiers as $cashier) {
                            $closed = ($cashier['is_closed'] == 1) ? "closed" : "open" ;
                            echo '<tr>';
                            echo "<td>{$cashier['location_code']} - {$cashier['cashier_code']}";
                            if ($cashier['is_closed'] == 1){
                                echo "<span class='label label-danger pull-right'>Closed</span>";
                            } else {
                                echo "<span class='label label-success pull-right'>Open</span>";
                            }
                            echo "</td>";
                            echo "<td class='text-center'>
                                    <a href='".site_url('sales/pos_add')."/{$cashier['location_id']}/{$cashier['cashier_id']}'>
                                        <button type='button' class='btn btn-md btn-info' ". ($cashier['is_closed']==1?'disabled':'') .">Start Transaction</button>
                                    </a>
                                    <a href='".site_url('sales/pos_toggle_cashier')."/{$cashier['cashier_id']}'>
                                        <button type='button' class='btn btn-md btn-warning'>Open / Close</button>
                                    </a>
                                    <a href='".site_url('sales/pos_review_cashier')."/{$cashier['cashier_id']}'>
                                        <button type='button' class='btn btn-md btn-warning'>Review Transactions</button>
                                    </a>
                                </td>";
                            echo '</tr>';
                        }
                     ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<!-- /.content -->
