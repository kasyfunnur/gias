<section class="content-header">
  <h1><a class="" href="<?php echo site_url('admin/location');?>">Location</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
    Add Location Form </h1>
</section>
<section class="content"> <?php echo $msg?> <?php echo validation_errors()?>
  <div class="box box-primary">
    <div class="box-header"> </div>
    <div class="box-body">
      <?php
		$attributes = array(
			'class' 	=> 'form-horizontal',
			'role'		=> 'form',
			'method' 	=> 'post'
			);
		echo form_open('', $attributes);
		?>
      <div class="form-group <?php echo (!form_error('name')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Name</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" name="name" value="<?php echo set_value('name')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('address')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Address</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="address" value="<?php echo set_value('address')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('phone')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Phone</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="phone" value="<?php echo set_value('phone')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('city')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">City</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" name="city" value="<?php echo set_value('city')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('state')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">State</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="state" value="<?php echo set_value('state')?>">
        </div>
      </div>
      <div class="form-group<?php echo (!form_error('country')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Country</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="country" value="<?php echo set_value('country')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('description')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Description</label>
        <div class="col-sm-8">
          <textarea class="form-control" type="text" name="description"><?php echo set_value('description')?></textarea>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('user_name')) ? '' : 'has-error' ?>">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary btn-flat pull-right">Add New</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</section>
