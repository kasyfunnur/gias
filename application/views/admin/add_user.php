<section class="content-header">
  <h1><a class="" href="<?php echo site_url('admin/user');?>">User</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
    Add User Form </h1>
</section>
<section class="content"> <?php echo $msg?> <?php echo validation_errors()?>
  <div class="box box-primary">
    <div class="box-header"> </div>
    <div class="box-body">
      <?php
		$attributes = array(
			'class' 	=> 'form-horizontal',
			'role'		=> 'form',
			'method' 	=> 'post'
			);
		echo form_open('', $attributes);
		?>
      <div class="form-group <?php echo (!form_error('user_name')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Username</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="user_name" value="<?php echo set_value('user_name')?>" autofocus>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('full_name')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Full Name</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="full_name" value="<?php echo set_value('full_name')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('user_pass')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Password</label>
        <div class="col-sm-8">
          <input class="form-control" type="password" name="user_pass" value="<?php echo set_value('user_pass')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('user_email')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Email</label>
        <div class="col-sm-8">
          <input class="form-control" type="email" name="user_email" value="<?php echo set_value('user_email')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('location_id')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Location</label>
        <div class="col-sm-8">
          <select class="form-control" id="location_id" name="location_id" >
            <?php foreach($site_location as $location)
			echo "<option value='$location[location_id]' ".set_select('location_id', "$location[location_id]").">$location[location_name]</option>";?>
          </select>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('group_id')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Group</label>
        <div class="col-sm-8">
          <select multiple class="form-control" id="group_id" name="group_id[] ">
            <?php foreach($group_array as $group_row)
			echo "<option value='$group_row[group_id]' ".set_select('group_id', "$group_row[group_id]").">$group_row[group_name]</option>";?>
          </select>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('birthdate')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Birth Date</label>
        <div class="col-sm-8">
          <input type="text" class="form-control date-mask" id="birthdate" name="birthdate" value="<?php echo set_value('birthdate')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('gender')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Gender</label>
        <div class="col-sm-8">
          <select class="form-control" id="gender" name="gender">
            <option value='male' <?php echo set_select('gender','male',TRUE)?>>Male</option>
            <option value='female' <?php echo set_select('gender','female')?>>Female</option>
          </select>
        </div>
      </div>
      
      <!-- feature-position -->
      <div class="form-group <?php echo (!form_error('position')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Position</label>
        <div class="col-sm-8">
          <?php html_select_position(array('name'=>'position','class'=>'form-control')); ?>
        </div>
      </div>
      
      <div class="form-group <?php echo (!form_error('designation')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Designation</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="designation" value="<?php echo set_value('designation')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('department')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Department</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="department" value="<?php echo set_value('department')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('phone_number')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Phone Number</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="phone_number" value="<?php echo set_value('phone_number')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('mobile_number')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Mobile Number</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="mobile_number" value="<?php echo set_value('mobile_number')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('extension_number')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Extension Number</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="extension_number" value="<?php echo set_value('extension_number')?>">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary btn-flat pull-right">Add New</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</section>
