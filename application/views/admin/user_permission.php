<section class="content-header">
  <h1>Location List</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Location</a></li>
    <li class="active">Location List</li>
  </ol>
</section>
<section class="content">
  <div class="box box-info">
    <div class="box-header">
      <a href='./new_location'>
        <button class="btn btn-sm btn-flat btn-info" style="margin-bottom:10px">Add New</button>
        </a>
    </div>
    <div class="box-body">
      <table id="location_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Location ID</th>
            <th>Name</th>
            <th>City</th>
            <th>Phone</th>
            <th>Address</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Location ID</th>
            <th>Name</th>
            <th>City</th>
            <th>Phone</th>
            <th>Address</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</section>