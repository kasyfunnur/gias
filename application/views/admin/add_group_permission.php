<section class="content-header">
  <h1>Add Location Form</h1>
</section>
<section class="content">
<?php echo $msg?>
  <div class="box box-primary">
    <div class="box-body">
      <?php
		$attributes = array(
			'class' 	=> 'form-horizontal',
			'role'		=> 'form',
			'method' 	=> 'post', 
			'name'		=> 'new_location_form', 
			'id' 		=> 'new_location_form'
			);
		echo form_open('', $attributes);
		?>
      <div class="form-group">
        <label class="col-sm-4 control-label">Name</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="name">
		</div>
      </div>
      <div class="form-group">
        <label class="col-sm-4 control-label">Address</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="address">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4 control-label">Phone</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="phone">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4 control-label">City</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" name="city">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4 control-label">State</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="state">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4 control-label">Country</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="country">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4 control-label">Description</label>
        <div class="col-sm-8">
            <textarea class="form-control" type="text" name="description"></textarea>
         </div>
      </div>
      <div class="form-group">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary btn-flat pull-right">Add New</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</section>
