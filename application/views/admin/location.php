<section class="content-header">
  <h1>Location
  <a href="<?php echo site_url('admin/add_location');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<i class="fa fa-plus"></i>&nbsp;&nbsp;Add New
		</button>
    </a>
    </h1>
</section>
<section class="content">
  <div class="box box-info">
    <div class="box-header">
    </div>
    <div class="box-body">
      <table id="location_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Location ID</th>
            <th>Name</th>
            <th>City</th>
            <th>Phone</th>
            <th>Address</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</section>