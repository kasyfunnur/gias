<section class="content-header">
  <h1>Skyware</h1>
</section>
<section class="content">
  <div class="box box-warning">
    <div class="box-header">
  <h3 class="box-title">Uninstalled Module</h3>
  <div class="box-tools pull-right">
        <button class="btn btn-danger btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button class="btn btn-danger btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body">
 <p> This module has not been installed yet, please contact our administrator if you would like to try out this module. </p>
    </div>
  </div>
</section>
