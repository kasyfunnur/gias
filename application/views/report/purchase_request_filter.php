<section class="content-header">
  <h1><?php echo $report_title; ?> Report</h1>
</section>
<section class="content">
  <?php 
$attributes = array(
	'class' 	=> 'form-horizontal',
	'role'		=> 'form',
	'method' 	=> 'post',
	'name'		=> 'form_report', 
	'id' 		=> 'form_report' // do not change id and name, name and id will be used in common report_prepare.js
	);
echo form_open('', $attributes);
?>
  <div class="box box-info">
    <div class="box-header">
      <h4 class="box-title">Filters</h4>
    </div>
    <div class="box-body">
      
      <div class="form-group">
        <label class="col-sm-2 control-label">Location</label>
        <div class="col-sm-5">
          <?php asset_move_select_location(array('name'=>'sel_location[]', 'id'=>'sel_location', 'multiple'=>'true', 'size'=>'4','class'=>'form-control'),0); ?>
        </div>
      </div>
      
      <!-- acquired date range filter -->
      <div class="form-group">
        <label class="col-sm-2 control-label">Acquired Date</label>
        <div class="col-sm-2">
        	<label class="radio-inline">
        	<input type="radio" name="rdo_datefilter" value="0" checked onClick="toggle_date_filter(this)"> All
            </label>
            <label class="radio-inline">
            <input type="radio" name="rdo_datefilter" value="1" onClick="toggle_date_filter(this)"> Range
            </label>
        </div>
        <div id="date_filter_span" style="display:none">
            <?php
                $default_start = '01-01-' . date('Y',time());  // default beginning of the year
                $default_end = date('d-m-Y',time()); // default today
            ?>
          <div class="col-sm-2">
          <input type="text" class="form-control date-mask" id="date_from" name="date_from" value="<?php echo date('d-m-Y',strtotime($default_start))?>"></div>
          <div class="col-sm-1"><p>To</p></div>
          <div class="col-sm-2">
          	<input type="text" class="form-control date-mask" id="date_to" name="date_to" value="<?php echo date('d-m-Y',strtotime($default_end))?>">
          </div>
        </div>
      </div>
      
      <div class="form-group">
      	<div class="col-sm-offset-2 col-sm-5">
        
      	<?php generate_report_buttons($report_url); ?>
        </div>
      </div>
    </div>
  </div>
  
  </form>
</section>

<script type="text/javascript">
	function toggle_date_filter(rdo){
		if(rdo){
			document.getElementById('date_filter_span').style.display = (rdo.value == 0) ? 'none' : 'inline' ;
		}
	}
	
	function toggle_price_filter(rdo){
		if(rdo){
			document.getElementById('price_filter_span').style.display = (rdo.value == 0) ? 'none' : 'inline' ;
		}
	}
</script>