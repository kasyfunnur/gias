<!-- <?php print_r ($filters) ;?> -->

<table width="100%" class="table_data" cellspacing="1" cellpadding="5">
	<thead>
    	<tr>
        	<th>Journal Code</th>
            <th>Posting Date</th>
            <th>Memo</th>
            <th>Document Refference</th>
            <th>Account Number</th>
            <th>Account Name</th>
            <th class="text-right">Currency</th>
            <th class="text-right">Debit</th>
            <th class="text-right">Credit</th>
        </tr>
    </thead>
    <tbody>
    	<?php
            $current_id = 0;
            foreach ($data as $trans) { 
                if($trans['journal_id'] != $current_id){
                    echo str_repeat('<td>&nbsp;</td>',9);
                    $current_id = $trans['journal_id'];
                }
        ?>
    	<tr>
        	<td><?php echo htmlspecialchars($trans['journal_code']) ?></td>
            <td><?php echo date('d-M-Y',strtotime($trans['posting_date'])) ?></td>
            <td><?php echo htmlspecialchars($trans['journal_memo']) ?></td>
            <td><?php echo htmlspecialchars($trans['reff']) ?></td>
            <td><?php echo htmlspecialchars($trans['account_number']) ?></td>
            <td><?php echo htmlspecialchars($trans['account_name']) ?></td>
            <td align="right"><?php echo htmlspecialchars($trans['journal_curr']) ?></td>
            <td align="right"><?php echo number_format($trans['journal_dr']) ?></td>
            <td align="right"><?php echo number_format($trans['journal_cr']) ?></td>
        </tr>
        <?php }; ?>
    </tbody>
</table>