<!-- <pre>
<?php print_r ($filters) ;?></pre>

<pre>
    <?php print_r($data) ; ?>
</pre>
-->
<?php //var_dump($data);?>
<h3><?php echo "From ".date("d M Y",strtotime($filters['date_from']))." - to - ".date("d M Y",strtotime($filters['date_to'])); ?></h3>
<table width="100%" class="table_data" cellspacing="1" cellpadding="5" class="table table-striped">
    <thead>
        <tr>
            <th>Item Group</th>
            <th>Item Code</th>
            <th>Name</th>
            <th align="right" class="text-right">Qty</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $datum) { ?>
        <tr>
            <td><?php echo $datum['category']?></td>
            <td><?php echo $datum['item_code']?></td>
            <td><?php echo $datum['item_name'];?></td>
            <td align="right"><?php echo number_format($datum['item_qty']);?></td>            
        </tr>
        <?php }; ?>
    </tbody>
</table>