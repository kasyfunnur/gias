<!-- <pre>
<?php print_r ($filters) ;?></pre>

<pre>
    <?php print_r($data) ; ?>
</pre>
-->

<h3><?php echo "As of {$filters['seldate']}"; ?></h3>
<table width="100%" class="table_data" cellspacing="1" cellpadding="5" class="table table-striped">
	<thead>
    	<tr>
        	<th>Account Number</th>
            <th>Account Name</th>
            <th align="right" class="text-right">Debit</th>
            <th align="right" class="text-right">Credit</th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($data as $coa) { ?>
    	<tr>
            <?php $styling = ($coa['is_postable'] == 0) ? 'font-weight:bold;' : '' ;?>
        	<td style="<?php echo $styling ;?>"><?php echo str_repeat('&nbsp;&nbsp;', $coa['account_level'] ) . $coa['account_number']?></td>
            <td style="<?php echo $styling ;?>"><?php echo str_repeat('&nbsp;&nbsp;', $coa['account_level'] ) . $coa['account_name']?></td>
            <td align="right"><?php echo number_format($coa['debit'])?></td>
            <td align="right"><?php echo number_format($coa['credit'])?></td>
        </tr>
        <?php }; ?>
    </tbody>
</table>