<!-- <?php print_r ($filters) ;?> -->

<table width="100%" class="table_data" cellspacing="1" cellpadding="5">
	<thead>
    	<tr>
        	<th>Label</th>
            <th>Name</th>
            <th>Group</th>
            <th>Acquired Date</th>
            <th>Asset Cost</th>
            <th>Location</th>
            <th>Condition</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($data as $asset) { ?>
    	<tr>
        	<td><?php echo $asset['asset_label']?></td>
            <td><?php echo $asset['asset_name']?></td>
            <td><?php echo $asset['group_name']?></td>
            <td><?php echo date('d-M-Y',strtotime($asset['date_acquired']))?></td>
            <td><?php echo $asset['asset_cost']?></td>
            <td><?php echo $asset['location_name']?></td>
            <td><?php echo $asset['asset_condition']?></td>
            <td><?php echo ucfirst($asset['asset_status'])?></td>
        </tr>
        <?php }; ?>
    </tbody>
</table>