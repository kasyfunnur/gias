<!-- <pre>
<?php print_r ($filters) ;?></pre>

<pre>
    <?php print_r($data) ; ?>
</pre>
-->

<h3><?php echo "From " .  date('d-M-Y',strtotime($filters['date_from'])) .  " to " . date('d-M-Y', strtotime($filters['date_to'])); ?></h3>
<table width="100%" class="table_data" cellspacing="1" cellpadding="5" class="table table-striped">
    <thead>
        <tr>
            <th>Account Number</th>
            <th>Account Name</th>
            <th align="right" class="text-right">Debit (IDR)</th>
            <th align="right" class="text-right">Credit (IDR)</th>
            <th align="right" class="text-right">Net (IDR)</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $total_debit = 0;
            $total_credit = 0;
            foreach ($data as $coa) { 
                $total_debit += $coa['debit'];
                $total_credit += $coa['debit'];
            ?>
        <tr>
            <?php $styling = ($coa['is_postable'] == 0) ? 'font-weight:bold;' : '' ;?>
            <td style="<?php echo $styling ;?>"><?php echo $coa['account_number']?></td>
            <td style="<?php echo $styling ;?>"><?php echo $coa['account_name']?></td>
            <td align="right"><?php echo currency_format($coa['debit'])?></td>
            <td align="right"><?php echo currency_format($coa['credit'])?></td>
            <td align="right"><?php echo currency_format($coa['debit'] - $coa['credit'])?></td>
        </tr>
        <?php }; ?>
        <tr>
            <td colspan="2" align="right"><b>Total</b></td>
            <td align="right"><strong><?php echo currency_format($total_debit)?></strong></td>
            <td align="right"><strong><?php echo currency_format($total_credit)?></strong></td>
            <td align="right"><strong><?php echo currency_format($total_debit - $total_credit)?></strong></td>
        </tr>
    </tbody>
</table>