<!-- <pre>
<?php print_r ($filters) ;?></pre>

<pre>
    <?php print_r($data) ; ?>
</pre>
-->
<?php //var_dump($data);?>
<h3><?php echo "From ".date("d M Y",strtotime($filters['date_from']))." - to - ".date("d M Y",strtotime($filters['date_to'])); ?></h3>
<table width="100%" class="table_data" cellspacing="1" cellpadding="5" class="table table-striped">
    <thead>
        <tr>
            <th>Date</th>
            <th>Doc#</th>
            <th>Vendor</th>
            <th>Status</th>

            <th>Item</th>
            <th style="text-align:right;">Qty</th>
            <th style="text-align:right;">Price</th>
            <th style="text-align:right;">Open Qty</th>
            <th style="text-align:right;">Line Total</th>

            <th align="right" class="text-right">Total</th>    
            
        </tr>
    </thead>
    <tbody>
        <?php $prev_doc = '';?>
        <?php foreach ($data as $datum) { ?>

        <tr>
            <?php if($prev_doc != $datum['doc_num']){ ?>
                <td><?php echo date("d M Y",strtotime($datum['doc_dt']))?></td>
                <td><?php echo $datum['doc_num']?></td>
                <td><?php echo $datum['buss_name'];?></td>
                <td><?php echo $datum['doc_status'];?></td>
            <?php } else { ?>
                <td colspan="4">&nbsp;</td>
            <?php } ?>

            <td><?php echo $datum['item_name'];?></td>
            <td align="right"><?php echo number_format($datum['item_qty']);?></td>
            <td align="right"><?php echo number_format($datum['item_netprice']);?></td>
            <td align="right"><?php echo number_format($datum['item_qty']-$datum['item_qty_closed']);?></td>
            <td align="right"><?php echo number_format($datum['item_total']);?></td>
            <?php if($prev_doc != $datum['doc_num']){ ?>
                <td align="right"><?php echo number_format($datum['doc_total']);?></td>                
            <?php } else { ?>
                <td colspan="1">&nbsp;</td>
            <?php } ?>
        </tr>

        <?php $prev_doc = $datum['doc_num'];?>
        <?php }; ?>
    </tbody>
</table>