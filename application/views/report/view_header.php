<section class="content-header">
  <h1><a href="<?php echo $filter_url; ?>"><?php echo $report_title; ?> Report</a>
  &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
  View
  </h1>
</section>
<section class="content">
<div class="box box-info">
  <div class="box-header"><h2 class="box-title"><?php echo $report_title; ?></h2></div>
  <div class="box-body table-responsive">