<section class="content-header">
	<h1><?php echo $report_title; ?> Report</h1>
</section>
<section class="content">
	<?php 
		$attributes = array(
			'class' 	=> 'form-horizontal',
			'role'		=> 'form',
			'method' 	=> 'post',
			'name'		=> 'form_report', 
			'id' 		=> 'form_report' // do not change id and name, name and id will be used in common report.js
			);
		echo form_open('', $attributes);
	?>
  	<div class="box box-info">
		<div class="box-header">
			<h4 class="box-title">Filters</h4>
		</div>
		<div class="box-body">

			<div class="form-group">
				<label class="col-sm-2 control-label">Balance Sheet Date</label>
				<div class="col-sm-2">
					<input type="date" class="form-control" id="seldate" name="seldate" value="<?php echo date('Y-m-d'); ?>" placeholder="">
				</div>
			</div>
		  
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-8">
					<?php generate_report_buttons($report_url); ?>
				</div>
			</div>
		</div>
	</div>
  	</form>
</section>

<script type="text/javascript">
	function toggle_date_filter(rdo){
		if(rdo){
			document.getElementById('date_filter_span').style.display = (rdo.value == 0) ? 'none' : 'inline' ;
		}
	}
	
	function toggle_price_filter(rdo){
		if(rdo){
			document.getElementById('price_filter_span').style.display = (rdo.value == 0) ? 'none' : 'inline' ;
		}
	}
</script>