<section class="content-header">
  <h1><?php echo $report_title; ?> Report</h1>
</section>
<section class="content">
  <?php 
$attributes = array(
	'class' 	=> 'form-horizontal',
	'role'		=> 'form',
	'method' 	=> 'post',
	'name'		=> 'form_report', 
	'id' 		=> 'form_report' // do not change id and name, name and id will be used in common report.js
	);
echo form_open('', $attributes);
?>
  <div class="box box-info">
    <div class="box-header">
      <h4 class="box-title">Filters</h4>
    </div>
    <div class="box-body">
      <div class="form-group">
        <label class="col-sm-2 control-label">Asset Group</label>
        <div class="col-sm-6">
          <?php asset_list_select_group(array('name'=>'sel_group[]', 'id'=>'sel_group', 'multiple'=>'true', 'size'=>'8','class'=>'form-control'),0); ?>
        </div>
      </div>
      
      <div class="form-group">
        <label class="col-sm-2 control-label">Asset Location</label>
        <div class="col-sm-6">
          <?php asset_move_select_location(array('name'=>'sel_location[]', 'id'=>'sel_location', 'multiple'=>'true', 'size'=>'4','class'=>'form-control'),0); ?>
        </div>
      </div>
      
      <div class="form-group">
      	<?php
			$default_start = '01-01-' . date('Y',time());  // default beginning of the year
			$default_end = date('d-m-Y',time()); // default today
		?>
        <label class="col-sm-2 control-label">Date Range</label>
        <div class="col-sm-2">
          <input type="text" class="form-control date-mask" id="date_from" name="date_from" value="<?php echo date('d-m-Y',strtotime($default_start))?>">
        </div>
        <label class="col-sm-1 control-label"><div class="text-center">To</div></label>
        <div class="col-sm-2">
          <input type="text" class="form-control date-mask" id="date_to" name="date_to" value="<?php echo date('d-m-Y',strtotime($default_end))?>">
        </div>
      </div>
      
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-8">
          <?php generate_report_buttons($report_url); ?>
        </div>
      </div>
    </div>
  </div>
  </form>
</section>