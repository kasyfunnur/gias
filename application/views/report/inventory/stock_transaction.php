<STYLE>
th {
	font-weight:bold;
	font-size:16px;
}
#result > thead > tr > th {
	font-weight:bold;
}
</STYLE>

<section class="content-header">
    <h1><a class="" href="#">Report</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        <?php echo $header['report_name'];?>
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Result
        &nbsp;&nbsp;&nbsp;
    </h1>   
</section>

<!-- Main content -->
<section class="content">	
	<?php
        $attributes = array(
            'class' 	=> 'form-horizontal',
            'role'		=> 'form',
            'method' 	=> 'post', 
            'name'		=> 'frm', 
            'id' 		=> 'frm',
            'onSubmit'	=> 'return validateForm();'
            );
        echo form_open('', $attributes);
    ?>
	<div class="row">
		<div class="col-sm-9">
        	<div class="text-center"><h3><?php echo $setting['comp_name'];?></h3></div>
        	<div class="text-center"><h4>Stock Card</h4></div>
            <div class="text-center">
            	<h5><?php echo date("d-M-Y",strtotime($param['date'][0]))." --- ".date("d-M-Y",strtotime($param['date'][1]));?></h5>
			</div>
            <table width="100%" id="result" class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th>System Date</th>
                        <th>Type</th>
                        <th>Doc Num</th>
                        <th>Item</th>
                        <th>Qty</th>
                        <th>WH</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				foreach($results as $result){
					$doc_link = "";
					switch($result['doc_type']){
						case 'DEL':
						$doc_link = base_url('sales/sales_delivery_edit')."/";
							break;
						case 'RCV':
						$doc_link = base_url('purchase/purchase_receipt_edit')."/";
							break;
						case 'TRF':
							$doc_link = base_url('inventory/transfer_view')."/";
							break;
						default:
							$doc_link = base_url('inventory/transfer_view')."/";
					}
					/*if($result['doc_type'] == 'DEL'){
						$doc_link = base_url('sales/sales_delivery_edit')."/";
					}elseif($result['doc_type'] == 'RCV'){
						$doc_link = base_url('purchase/purchase_receipt_edit')."/";
					}*/
					
					echo "<tr>";
					echo "<td>".$result['timestamp']."</td>";
					echo "<td>".$result['doc_type']."</td>";
					echo "<td><a href='".$doc_link.$result['doc_num']."' target='_blank'>".$result['doc_num']."</a></td>";
					echo "<td>".$result['item_code']." - ".$result['item_name']."</td>";
					echo "<td style='text-align:right;'>".$result['item_qty']."</td>";
					echo "<td>".$result['location_name']."</td>";
					echo "<td>".$result['trx_status']."</td>";
					echo "</tr>";
				}				
				?>                    
                </tbody>
                <tr></tr>
            </table>
		</div>    	
    </div>

	<br><br>
    <a class="btn btn-info pull-left">
    	<i class="fa fa-print"></i>&nbsp;&nbsp; Print
    </a>&nbsp;&nbsp;&nbsp;
    <a class="btn btn-info">
    	<i class="fa fa-download"></i>&nbsp;&nbsp; Download PDF
    </a>
    <!---<button type="submit" class="btn btn-info pull-left">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>--->
    
	</form>	
</section>