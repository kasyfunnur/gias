<STYLE>
th {
	font-weight:bold;
	font-size:16px;
}
thead > tr > td {
	font-weight:bold;
}
</STYLE>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="#">Report</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        <?php echo $name;?>
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Result
        &nbsp;&nbsp;&nbsp;
    </h1>   
</section>

<!-- Main content -->
<section class="content">	
	<?php
        $attributes = array(
            'class' 	=> 'form-horizontal',
            'role'		=> 'form',
            'method' 	=> 'post', 
            'name'		=> 'frm', 
            'id' 		=> 'frm',
            'onSubmit'	=> 'return validateForm();'
            );
        echo form_open('', $attributes);
		//var_dump($table_left);
    ?>
	<div class="row">
		<div class="col-sm-6">
            <table width="100%" border="solid;1px;">
                <thead>
                    <tr>
                        <th>Account #</th>
                        <th>Name</th>
                        <th>Balance</th>
                    </tr>
                    <tr>
                    	<td>10000</td>
                        <td>ASSET</td>
                        <td class="value"><?php echo number_format($ast).str_repeat('&nbsp;',20);?></td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data_ast as $data){ ?>
                    <?php $sp = str_repeat('&nbsp;',$data['account_level']*5);?>
                    <?php $sp2 = str_repeat('&nbsp;',(5-$data['account_level'])*5);?>
					<?php 
						($data['default'] == "C") ? $mul = -1 : $mul = 1;
						$bal = $mul * $data['account_balance'];						
					?>
                    <tr>
                        <td><?php echo $sp.$data['account_number'];?></td>
                        <td><?php echo $sp.$data['account_name'];?></td>
                        <td class="value"><?php echo number_format($bal).$sp2;?></td>                  
                    </tr>
                    <?php } ?>
                </tbody>
                <tr></tr>
            </table>
		</div>
    	<div class="col-sm-6">
            <table width="100%" border="solid;1px;">
                <thead>
                    <tr>
                        <th>Account #</th>
                        <th>Name</th>
                        <th>Balance</th>
                    </tr>
                    <tr>
                    	<td>20000</td>
                        <td>LIABILITES</td>
                        <td class="value"><?php echo number_format($lia).str_repeat('&nbsp;',20);?></td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data_lia as $data){ ?>
                    <?php $sp = str_repeat('&nbsp;',$data['account_level']*5);?>
                    <?php $sp2 = str_repeat('&nbsp;',(5-$data['account_level'])*5);?>
					<?php 
						($data['default'] == "C") ? $mul = -1 : $mul = 1;
						$bal = $mul * $data['account_balance'];						
					?>
                    <tr>
                        <td><?php echo $sp.$data['account_number'];?></td>
                        <td><?php echo $sp.$data['account_name'];?></td>
                        <td class="value"><?php echo number_format($bal).$sp2;?></td>
                    </tr>
                    <?php } ?>
                </tbody>
				<thead>
                    <tr>
                        <th>Account #</th>
                        <th>Name</th>
                        <th>Balance</th>
                    </tr>
                    <tr>
                    	<td>30000</td>
                        <td>EQUITY</td>
                        <td class="value"><?php echo number_format($equ).str_repeat('&nbsp;',20);?></td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data_equ as $data){ ?>
                    <?php $sp = str_repeat('&nbsp;',$data['account_level']*5);?>
                    <?php $sp2 = str_repeat('&nbsp;',(5-$data['account_level'])*5);?>
                    <?php 
						($data['default'] == "C") ? $mul = -1 : $mul = 1;
						$bal = $mul * $data['account_balance'];						
					?>
                    <tr>
                        <td><?php echo $sp.$data['account_number'];?></td>
                        <td><?php echo $sp.$data['account_name'];?></td>
                        <td class="value"><?php echo number_format($bal).$sp2;?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
		</div>
    </div>

	<br><br>
    <a class="btn btn-info pull-left">
    	<i class="fa fa-print"></i>&nbsp;&nbsp; Print
    </a>&nbsp;&nbsp;&nbsp;
    <a class="btn btn-info">
    	<i class="fa fa-download"></i>&nbsp;&nbsp; Download PDF
    </a>
    <!---<button type="submit" class="btn btn-info pull-left">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>--->
    
	</form>	
</section>

