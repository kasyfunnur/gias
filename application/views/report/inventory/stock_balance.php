<?php 
	//var_dump($param1);
	//var_dump($param2);
	//var_dump($param3);
	//var_dump($date_from);
	//var_dump($date_to);
//	var_dump($results);
?>
<STYLE>
th {
	font-weight:bold;
	font-size:16px;
}
#result > thead > tr > th {
	font-weight:bold;
}
</STYLE>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="#">Report</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        <?php echo $name;?>
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Result
        &nbsp;&nbsp;&nbsp;
    </h1>   
</section>

<!-- Main content -->
<section class="content">	
	<?php
        $attributes = array(
            'class' 	=> 'form-horizontal',
            'role'		=> 'form',
            'method' 	=> 'post', 
            'name'		=> 'frm', 
            'id' 		=> 'frm',
            'onSubmit'	=> 'return validateForm();'
            );
        echo form_open('', $attributes);
		//var_dump($table_left);
		$CI = &get_instance();
		$CI->load->model('setting_model');
		$company = $CI->setting_model->get_company_info();

    ?>
	<div class="row">
		<div class="col-sm-9">
        	<div class="text-center"><h3><?php echo $company['comp_name'];?></h3></div>
        	<div class="text-center"><h4>Stock Balance</h4></div>
			
            <div class="text-center">
            	<h5><?php echo ($date_from != "``")? "Period : ".$date_from." - ".$date_to: "As of : ".$date_to ;?></h5>
			</div>
            <table width="100%" id="result" class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <!---<th>System Date</th>
                        <th>Type</th>
                        <th>Doc Num</th>
                        <th>Item</th>
                        <th>Qty</th>
                        <th>WH</th>
                        <th>Status</th>--->
                        <th>Item</th>
                        <th>Qty</th>
                        <th>WH</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				/*foreach($results as $result){
					$doc_link = "";
					if($result['doc_type'] == 'DEL'){
						$doc_link = base_url('sales/sales_delivery_edit')."/";
					}elseif($result['doc_type'] == 'RCV'){
						$doc_link = base_url('purchase/purchase_receipt_edit')."/";
					}
					
					echo "<tr>";
					echo "<td>".$result['timestamp']."</td>";
					echo "<td>".$result['doc_type']."</td>";
					echo "<td><a href='".$doc_link.$result['doc_num']."' target='_blank'>".$result['doc_num']."</a></td>";
					echo "<td>".$result['item_code']." - ".$result['item_name']."</td>";
					echo "<td style='text-align:right;'>".$result['item_qty']."</td>";
					echo "<td>".$result['location_name']."</td>";
					echo "<td>".$result['trx_status']."</td>";
					echo "</tr>";
				}*/
				foreach($results as $result){
					echo "<tr>";					
					echo "<td>".$result['item_code']." - ".$result['item_name']."</td>";
					echo "<td>".$result['balance_qty']."</td>";
					echo "<td>".$result['location_name']."</td>";
					echo "</tr>";
				}
				?>
                    
                </tbody>
                <tr></tr>
            </table>
		</div>    	
    </div>

	<br><br>
    <a class="btn btn-info pull-left">
    	<i class="fa fa-print"></i>&nbsp;&nbsp; Print
    </a>&nbsp;&nbsp;&nbsp;
    <a class="btn btn-info">
    	<i class="fa fa-download"></i>&nbsp;&nbsp; Download PDF
    </a>
    <!---<button type="submit" class="btn btn-info pull-left">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>--->
    
	</form>	
</section>