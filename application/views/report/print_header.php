<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $report_title ?></title>
<!-- <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" type="text/css"> -->
<!-- cannot use bootstrap for a while, error in pdf library when loading the fonts -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/print.css'); ?>" type="text/css">
</head>
<body>
<table width="100%">
	<tr>
   	  <td width="140" rowspan="2" align="left" valign="top"><img src="<?php echo base_url('assets/img/company-logo.png');?>" width="140"></td>
        <td width="200" rowspan="2" align="left" valign="top">
        <strong>Digital Buana</strong><br>
        Jakarta, Indonesia<br>
        Tel: +62 22 1234 5678
        </td>
        <td align="right" valign="top">
        	<h1><?php echo $report_title; ?></h1>
            <p align="right" class="print_time">&nbsp;</p>
        </td>
    </tr>
	<tr>
	  <td align="right" valign="bottom"><span class="print_time">Printed on: <?php echo date('d M Y - H:i',time());?> </span>
      <div class="no-print"><button type="button" class="btn btn-flat btn-info" onClick="print();"><i class="fa fa-print"></i> Print</button></div>
      </td>
  </tr>
</table>
<hr width="100%">