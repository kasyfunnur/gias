<!-- <pre>
<?php print_r ($filters) ;?></pre>

<pre>
    <?php print_r($data) ; ?>
</pre>
-->
<?php //var_dump($data);?>
<h3><?php echo "From ".date("d M Y",strtotime($filters['date_from']))." - to - ".date("d M Y",strtotime($filters['date_to'])); ?></h3>
<table width="100%" class="table_data" cellspacing="1" cellpadding="5" class="table table-striped">
    <thead>
        <tr>
            <th>Date</th>
            <th>Doc Reff</th>
            <!-- <th>Input By</th> -->
            <th>Item Code</th>
            <th>Name</th>            
            <th align="right" class="text-right">Qty</th>
            <th>&nbsp;&nbsp;&nbsp;Location</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $datum) { ?>
        <tr>
            <td><?php echo date("d M Y",strtotime($datum['doc_date']))?></td>
            <td><?php echo $datum['doc_num'];?></td>
            <td><?php echo $datum['item_code']?></td>
            <td><?php echo $datum['item_name'];?></td>

            <td align="right"><?php echo number_format($datum['item_qty']);?></td>
            <td><?php echo "&nbsp;&nbsp;&nbsp;".$datum['whse_code']." - ".$datum['whse_name'];?></td>
        </tr>
        <?php }; ?>
    </tbody>
</table>