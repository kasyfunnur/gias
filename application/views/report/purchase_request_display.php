<!--- <?php print_r ($filters) ;?> --->

<table width="100%" class="table_data" cellspacing="1" cellpadding="5">
	<thead>
    	<tr>
        	<th>Department</th>
            <th>Document Number</th>
            <th>Request Date</th>
            <th>Request For</th>
            <th>Site</th>
            <th>Currency</th>
            <th class="text-right">Total Amount</th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($data as $asset) { ?>
    	<tr>
        	<td><?php echo $asset['request_dept']?></td>
            <td><?php echo $asset['doc_num']?></td>
            <td><?php echo date('d-M-Y',strtotime($asset['doc_dt']))?></td>
            <td><?php echo $asset['request_for']?></td>
            <td><?php echo $asset['location_name']?></td>
            <td><?php echo $asset['doc_curr']?></td>
            <td align="right"><?php echo number_format($asset['doc_total'],0,'.',',')?></td>
        </tr>
        <?php }; ?>
    </tbody>
</table>