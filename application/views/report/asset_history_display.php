<!-- <?php print_r ($filters) ;?> -->

<table width="100%" class="table_data" cellspacing="1" cellpadding="5">
	<thead>
    	<tr>
        	<th>Asset Label</th>
            <th>Asset Name</th>
        	<th>Transaction Type</th>
            <th>Document Reference</th>
            <th>Transaction Date</th>
			<th>User</th>
            <th>Transaction Detail</th>
            <th>Transaction Memo</th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($data as $trans) { ?>
    	<tr>
        	<td nowrap><?php echo htmlspecialchars($trans['asset_label']) ?></td>
            <td nowrap><?php echo htmlspecialchars($trans['asset_name']) ?></td>
            <td nowrap><?php echo htmlspecialchars($trans['trans_type']) ?></td>
            <td nowrap><?php echo htmlspecialchars($trans['trans_doc']) ?></td>
            <td nowrap><?php echo date('d-M-Y',strtotime($trans['trans_date']))?></td>
            <td><?php echo htmlspecialchars($trans['full_name']) ?></td>
            <td><?php echo htmlspecialchars($trans['trans_detail']) ?></td>
            <td><?php echo htmlspecialchars($trans['trans_memo']) ?></td>
            
        </tr>
        <?php }; ?>
    </tbody>
</table>