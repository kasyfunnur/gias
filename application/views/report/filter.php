<!-- Content Header (Page header) -->
<?php //var_dump($header);var_dump($config);?>
<section class="content-header">
    <h1><a class="" href="#">Report</a> 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        <?php echo $header['report_module'];?>
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        <?php echo $header['report_name'];?>
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Filter
        &nbsp;&nbsp;&nbsp;
    </h1>   
</section>

<!-- Main content -->
<section class="content">   
    <?php
        $attributes = array(
            'class'     => 'form-horizontal',
            'role'      => 'form',
            'method'    => 'get', 
            'name'      => 'frm', 
            'id'        => 'frm',
            'onSubmit'  => 'return validateForm();'
            );
        //echo form_open('', $attributes);
    ?>
    <script src="<?php echo base_url('assets/js/jquery-2.0.3.min.js');?>"></script>
    <form action="<?php echo site_url($header['report_query']);?>" accept-charset="utf-8" class="form-horizontal" role="form" method="get" name="frm" id="frm" onsubmit="return validateForm();">

    <div class="row info">
        <div class="col-sm-9">
            <table width="100%">
                <?php 
                    foreach($config as $conf) { 
                        $the_object = $conf['object_type'];
                        //echo $the_object($conf['label'],$conf['object_name'],$conf['data']);
                        echo $the_object('',$conf['object_name'],$conf['data']);
                    } 
                ?>
            </table>
        </div>
    </div>
    
    <br><br>
    <button type="submit" class="btn btn-info pull-left">
        <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
    </form> 
</section>
<?php //var_dump($config);?>
<script src="<?php echo base_url('assets/js/jquery-2.0.3.min.js');?>"></script>
<script>
//var submit_url = "<?php //echo site_url('report/query/'.$type);?>";

function validateForm(){ // for report validateForm different from other query document.
    return true;
}

$(document).ready(function(){
    $("#item_all").on('click',function(){
        $("#filter_item option").prop('selected', $(this).prop('checked'));
    });
    $("#whse_all").on('click',function(){
        $("#filter_whse option").prop('selected', $(this).prop('checked'));
    });
});
</script>