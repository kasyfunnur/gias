<section class="content-header">
    <h1><?php echo $report_title; ?> Report</h1>
</section>
<section class="content">
    <?php 
        $attributes = array(
        	'class' 	=> 'form-horizontal',
        	'role'		=> 'form',
        	'method' 	=> 'post',
        	'name'		=> 'form_report', 
        	'id' 		=> 'form_report' // do not change id and name, name and id will be used in common report.js
        	);
        echo form_open('', $attributes);
    ?>
    <div class="box box-info">
        <div class="box-header">
            <h4 class="box-title">Filters</h4>
        </div>
        <div class="box-body">
            
            <div class="form-group">
            	<?php
                	$default_start = date('Y',time()) . '-01-01';  // default beginning of the year
                	$default_end = date('Y-m-d',time()); // default today
                ?>
                <label class="col-sm-2 control-label">Date Range</label>
                <div class="col-sm-2">
                    <input type="date" class="form-control" id="date_from" name="date_from" value="<?php echo $default_start ?>">
                </div>

                <label class="col-sm-1 control-label"><div class="text-center">To</div></label>
                <div class="col-sm-2">
                    <input type="date" class="form-control" id="date_to" name="date_to" value="<?php echo $default_end ?>">
                </div>
            </div>
          
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php generate_report_buttons($report_url); ?>
                </div>
            </div>
        </div>
    </div>
    </form>
</section>