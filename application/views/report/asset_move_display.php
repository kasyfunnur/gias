<!-- <?php print_r ($filters) ;?> -->

<table width="100%" class="table_data" cellspacing="1" cellpadding="5">
	<thead>
    	<tr>
        	<th>Document</th>
            <th>Status</th>
            <th>Date</th>
            <th>Asset Label</th>
            <th>Asset Name</th>
            <th>Move From</th>
            <th>Move To</th>
            <th>Requested By</th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($data as $trans) { ?>
    	<tr>
        	<td nowrap><?php echo $trans['document_ref']?></td>
            <td nowrap><?php echo $trans['request_status']?></td>
            <td nowrap><?php echo date('d-M-Y',strtotime($trans['transaction_date']))?></td>
            <td><?php echo $trans['asset_label']?></td>
            <td><?php echo $trans['asset_name']?></td>
            <td><?php echo $trans['from_location']?></td>
            <td><?php echo $trans['to_location']?></td>
            <td><?php echo $trans['full_name']?></td>
        </tr>
        <?php }; ?>
    </tbody>
</table>