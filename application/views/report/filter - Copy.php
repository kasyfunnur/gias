<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="#">Report</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        <?php echo $name;?>
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Filter
        &nbsp;&nbsp;&nbsp;
    </h1>   
</section>

<!-- Main content -->
<section class="content">	
	<?php
        $attributes = array(
            'class' 	=> 'form-horizontal',
            'role'		=> 'form',
            'method' 	=> 'get', 
            'name'		=> 'frm', 
            'id' 		=> 'frm',
            'onSubmit'	=> 'return validateForm();'
            );
        //echo form_open('', $attributes);
    ?>
	<form action="<?php echo site_url('report/query/'.$type);?>" accept-charset="utf-8" class="form-horizontal" role="form" method="get" name="frm" id="frm" onsubmit="return validateForm();">

	<div class="row info">
        <div class="col-sm-9">
            <table width="100%">
            	<?php if(isset($config['date']) && $config['date'] == 1){ ?>
                	<tr class="info">
                    	<td>Date as of</td>
                        <td class="value">
							<input type="date" class="form-control" name="filter_date_to" id="filter_date_to" value="<?php echo date('Y-m-d');?>">
                        </td>  
                    </tr>
                <?php } ?>
                <?php if(isset($config['date']) && $config['date'] == 2){ ?>
                	<tr class="info">
                    	<td>Date From</td>
                        <td class="value">
							<input type="date" class="form-control" name="filter_date_from" id="filter_date_from" value="<?php echo date('Y-m-01');?>">							
                        </td>
                    	<td>Date To</td>
                        <td class="value">
							<input type="date" class="form-control" name="filter_date_to" id="filter_date_to" value="<?php echo date('Y-m-d');?>">
                        </td>                    
					</tr>
                <?php } ?>
                
                <?php if(isset($config['whse'])){ ?>
                	<tr class="info">
                    	<td>Warehouse</td>
                        <td class="value">                        	
                        	<?php 
								$whse_add = array(0=>array('location_id' => -1,'location_name' => '---ALL---'));
								$whse_all = array_merge($whse_add,$config['whse']['data']);
							?>
							<?php if($config['whse']['type'] == 'single') {
									echo sw_createSelect('filter_whse',$whse_all,'location_id','location_name');?>
                            <?php }else{ ?>
                            		<input type="checkbox" id="whse_all" value="ALL"/>
                                    <label for="whse_all">Select All Warehouse</label>
                                   	<select class="form-control" size="3" style="height:150px;"
                                        name="filter_whse[]" id="filter_whse" multiple="multiple">
                                        <?php foreach($config['whse']['data'] as $whse)
                                            {echo '<option value="'.$whse['location_id'].'">'.$whse['location_name'];}
                                        ?>
                                    </select>  
                            <?php } ?>  
                        </td>                    
					</tr>
                <?php } ?>
                
                <?php if(isset($config['item'])){ ?>
                	<tr class="info">
                    	<td>Item(s)</td>
                        <td class="value">                   	
                        	<?php 
								$item_add = array(0=>array('item_id' => -1,'item_code' => '---ALL---'));
								$item_all = array_merge($item_add,$config['item']['data']);
							?>
                            <?php if($config['item']['type'] == 'single'){ 								
                            		echo sw_createSelect('filter_item',$item_all,'item_id','item_code');?>
                            <?php }else{ ?>
                            		<input type="checkbox" id="item_all" value="ALL"/> 
                                    <label for="item_all">Select All Item</label>
                                    <select class="form-control" size="3" style="height:150px;"
                                        name="filter_item[]" id="filter_item" multiple="multiple">
                                        <?php foreach($config['item']['data'] as $item)
                                            {echo '<option value="'.$item['item_id']	.'">'.$item['item_code']." - ".$item['item_name'];}
                                        ?>
                                    </select>  
                            <?php } ?>     
                        </td>                    
					</tr>
                <?php } ?>
                
            	<?php if(isset($filter_date_range) && $filter_date_range == TRUE){?>
	                <!--<tr class="info">
                    	<td>Date From</td>
                        <td class="value">
							<input type="date" name="filter_date_from" id="filter_date_from">
                        </td>                    
                    	<td>Date To</td>
                        <td class="value">
							<input type="date" name="filter_date_to" id="filter_date_to">
                        </td>                    
					</tr>-->
    			<?php } ?>
                
                <?php if(isset($filter_date_asof) && $filter_date_asof == TRUE){?>
	                <!--<tr class="info">
                    	<td>As of</td>
                        <td class="value">
							<input type="date" name="filter_date_asof" id="filter_date_asof" value="<?php echo date('Y-m-d',time());?>">
                        </td>                    
					</tr>-->
                <?php } ?>
                
                <?php if(isset($filter_business) && $filter_business == TRUE){?>
                	<tr class="info">
                    	<td>Business</td>
                        <td class="value">
							<!--<input type="date" name="filter_date_asof" id="filter_date_asof">-->
                            <?php echo sw_createSelect('filter_business',$ddl_business,'buss_id','buss_name');?>
                        </td>                    
					</tr>
                <?php } ?>
    		</table>
		</div>
	</div>
    
	<br><br>
    <button type="submit" class="btn btn-info pull-left">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
	</form>	
</section>
<?php //var_dump($config);?>
<script src="<?php echo base_url('assets/js/jquery-2.0.3.min.js');?>"></script>
<script>
var submit_url = "<?php echo site_url('report/query/'.$type);?>";

function validateForm(){ // for report validateForm different from other query document.
	return true;
}

$(document).ready(function(){
	$("#item_all").on('click',function(){
		$("#filter_item option").prop('selected', $(this).prop('checked'));
	});
	$("#whse_all").on('click',function(){
		$("#filter_whse option").prop('selected', $(this).prop('checked'));
	});
});
</script>