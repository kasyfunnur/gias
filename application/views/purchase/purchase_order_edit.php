<!-- Content Header (Page header) -->
<?php  //var_dump($doc_flow[0][0]);
foreach($doc_flow as $docs){
    //echo $docs[0]['doc_num']."<br>";
}
$list_action = array(
    array('link'=>base_url('/purchase/purchase_order_cancel')."/".$form_header[0]['doc_num'],'icon'=>'ban','text'=>'Cancel'),
    array('link'=>base_url('/purchase/purchase_order_close')."/".$form_header[0]['doc_num'],'icon'=>'minus-circle','text'=>'Close')
);
$list_download = array(
    array('link'=>base_url('/purchase/purchase_order_print')."/".$form_header[0]['doc_num'],'icon'=>'file-excel-o','text'=>'Excel'),
    array('link'=>base_url('/purchase/purchase_order_print')."/".$form_header[0]['doc_num'],'icon'=>'file-pdf-o','text'=>'PDF'),
    array('link'=>base_url('/purchase/purchase_order_print')."/".$form_header[0]['doc_num'],'icon'=>'print','text'=>'PRINT')
);
?>
<?php
    $attributes = array(
    'class'     => 'form-horizontal',
    'role'      => 'form',
    'method'    => 'post', 
    'name'      => 'frm', 
    'id'        => 'frm',
    'onSubmit'  => 'return validateForm();'
    );
    //echo form_open('', $attributes);
    echo form_open_multipart(site_url('purchase/purchase_order_edit_query'), $attributes);
    echo form_text('','hidden','doc_id',$form_header[0]['doc_id']);
    echo form_text('','hidden','save_confirm','save');
    echo form_text('','hidden','return_url',site_url('purchase/purchase_order'));
?>
<section class="content-header">
    <h1>        
        <a class="" href="<?php echo site_url('purchase/purchase_order');?>">Purchase Order</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Edit    
        &nbsp;&nbsp;&nbsp;
        <div class="pull-right button_action" id="button_action"></div>
    </h1>
</section>

<!-- Main content -->
<section class="content">	
    <div class="row">
    	<div class="col-sm-6">
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="buss_name">
                	Vendor                   
                </label>
                <div class="col-sm-9">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" readonly="readonly"		
                        	value="<?php echo $form_header[0]['buss_name'];?>">
                        <input type="hidden" id="buss_id" name="buss_id" value="<?php echo $form_header[0]['buss_id'];?>">                   	
                        <span class="input-group-btn">
							<button type="button" class="control btn btn-warning" data-toggle="modal" href="#modal_vendor" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                    
                </div>
            </div>
            <!--<div class="form-group">
                <label class="col-sm-3" for="doc_ref">Doc Ref</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_ref" name="doc_ref" >
                </div>
            </div>-->
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name', $form_header[0]['whse_id']);?>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr" readonly><?php echo $form_header[0]['ship_addr'];?></textarea>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_note">Document Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"><?php echo $form_header[0]['doc_note'];?></textarea>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $form_header[0]['doc_num'];?>" readonly>
                    <input type="hidden" id="doc_id" name="doc_id" value="<?php echo $form_header[0]['doc_id'];?>" />
                </div>
            </div>	
        	<div class="form-group col-sm-12 col-xs-6">
            	<!--<div class="row">-->
                    <label class="col-sm-3" for="doc_dt">Date</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo substr($form_header[0]['doc_dt'],0,10);?>">
                    </div>
				<!--</div>-->
            </div>		
            <div class="form-group col-sm-12 col-xs-6">
            	<!--<div class="row">-->
                    <label class="col-sm-3" for="doc_ddt">Date Required</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo substr($form_header[0]['doc_ddt'],0,10);?>">
                    </div>
				<!--</div>-->
            </div>		
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_curr">Curr</label>
                <div class="col-sm-5">
                    <?php echo sw_CreateSelect('doc_curr', $ddl_curr, 'curr_code', array('curr_code','curr_name'),$form_header[0]['doc_curr']);?>
                    <?php //echo sw_CreateSelect('doc_curr', $arr_curr_rate, 'value', array('curr_code','curr_name'),1);?>
                    <!-- <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly> -->
                </div>
                <div class="col-sm-4">
                    <input type="number" class="form-control input-sm" id="doc_rate" name="doc_rate" value="<?php echo $form_header[0]['doc_rate']; ?>">
                </div>
            </div> 
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="buss_name">
                    Purchase Contract                    
                </label>
                <div class="col-sm-9">
                    <div class="input-group input-group-sm">
                        <input type="hidden" id="contract_id" name="contract_id" value="<?php echo $form_header[0]['contract_num']; ?>">
                        <input type="text" class="form-control input-sm" id="contract_num" name="contract_num" value="" >
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_contract" style="width:100%;">
                                <i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
                        </span>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    
<!--<a href="http://www.google.com" class="form-control"><span>Linksss</span></a>-->

    <!-- TAB -->
    <div style="border:1px;">
    <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active col-sm-2  well no-padding">
        <a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Details</a>
    </li>
    <li role="presentation" class="col-sm-2 well no-padding">
        <a href="#info" aria-controls="info" role="tab" data-toggle="tab">Info</a>
    </li>
    <li role="presentation" class="col-sm-2 well no-padding">
        <a href="#attachment" aria-controls="attachment" role="tab" data-toggle="tab">Attachment</a>
    </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="detail">
            <div class="row">
            	<div class="col-sm-12">
                	<div class="table-responsive">
                	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
                    <thead>
                    <tr>
                    	<th style="width:5%">#</th>
                        <th style="width:20%">
                        	<button type="button" class="control btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                            	<span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Item Code</span>
        						<span class="visible-xs"><i class="fa fa-search"></i></span>
        					</button>
                        </th>
                        <th style="width:12%">Item Name</th>
                        <th style="width:8%">Quantity</th>
                        <th style="width:10%">Price</th>
                        <th style="width:8%">Disc %</th>
                        <th style="width:10%">Tax</th>
                        <th style="width:12%">Net Price</th>
                        <th style="width:15%">Total</th>
                        <th style="width:10%" class="action">Action</th>            
                    </tr>
                    </thead>
                    <tbody>
                    <tr id="default">
                    	<input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                    	<td style="text-align:center">
                        	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly tabindex="-1"/>
                            <input type="hidden" name="ast_price_temp[]" id="ast_price_temp1">                    
                            <input type="hidden" name="const_disc[]" id="const_disc1" value="100">
                            <input type="hidden" name="const_after_disc[]" id="const_after_disc1">
                            <input type="hidden" name="ast_disc_factor[]" id="ast_disc_factor1" >
                            <input type="hidden" name="ast_price_after_disc[]" id="ast_price_after_disc1" >
                            <input type="hidden" name="const_tax[]" id="const_tax1" value="100">
                            <input type="hidden" name="const_after_tax[]" id="const_after_tax1">
                            <input type="hidden" name="ast_tax_factor[]" id="ast_tax_factor1" >
                        </td>
                        <td>
                        	<div class="input-group">                    	
        	                    <span class="input-group-btn">
                                    <a href="#" class="btn btn-warning form-control" name="a_ast_code" target="_blank" tabindex="-1">
                                    	<i class="fa fa-external-link"></i>
                                    </a>                            
        						</span>
                                <input type="text" name="ast_code[]" id="ast_code1" class="form-control " readonly="readonly" tabindex="-1">
                            </div>
                        </td>
                        <td>
                        	<input type="text" name="ast_name[]" id="ast_name1" class="form-control">
                        </td>
                        <td>
                        	<input type="number" name="ast_qty[]"  id="ast_qty1"  class="form-control" style="text-align:right">
                        </td>
                        <td>
                        	<input type="text" name="ast_price[]"  id="ast_price1"  class="form-control" style="text-align:right">
                        </td>
                        <td>
                            <input type="number" name="ast_disc[]" id="ast_disc1" class="form-control" style="text-align:right">
                        </td>
                        <td>
                            <!-- <input type="text" name="ast_tax[]"  id="ast_tax1"  class="form-control" style="text-align:right"> -->
                            <?php echo sw_CreateDropdown(array('ast_tax[]','ast_tax1'),$ddl_tax,'tax_percent','tax_name');?>
                        </td>
                        <td>
                            <input type="number" name="ast_netprice[]"  id="ast_netprice1"  class="form-control" style="text-align:right" readonly tabindex="-1">
                        </td>
                        <td>
                        	<input type="text" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right" readonly tabindex="-1">
                        </td>
                        <td>
                        	<div class="input-group">
        						<div class='input-group-btn'>
                                    <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                                    	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
        	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                                    </button>
        						</div>
        					</div>
        				</td>                
                    </tr>            
                    </tbody>
                    <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><div>
                            <input type="number" class="form-control" id="qty_total" name="qty_total" 
                            	value="0" style="text-align:right;" readonly>
                            </div>
                        </td>
                        <td></td>
                        <td></td>
                        <td id="foot_label"><strong>Sub Total:&nbsp;&nbsp;&nbsp;</strong></td>
                        <td></td>
                        <td><div>
                            <input type="text" class="form-control" id="doc_subtotal" name="doc_subtotal" 
                            	value="0" style="text-align:right;" readonly>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Disc:&nbsp;&nbsp;&nbsp;</strong></td>
                        <td id="foot_label" style="text-align:right;">
                            <input type="number" class="col-sm-8" id="doc_disc_pct" name="doc_disc_pct"
                                value="0" style="text-align:right;" />%
                            <!-- <strong>Disc:&nbsp;&nbsp;&nbsp;</strong> -->
                        </td>
                        <td><div>
                            <input type="text" class="form-control" id="doc_disc" name="doc_disc" 
                                value="0" style="text-align:right;" readonly>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                        <td></td>
                        <td><div>
                            <input type="text" class="form-control" id="doc_total" name="doc_total" 
                            	value="0" style="text-align:right;" readonly>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                    </table>
                    </div>
            	</div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="info">

        </div>
        </form> 
        <div role="tabpanel" class="tab-pane" id="attachment">
            <?php 
                $attributes = array(
                    'class'     => 'form-horizontal',
                    'role'      => 'form',
                    'method'    => 'post', 
                    'name'      => 'frm', 
                    'id'        => 'frm',
                    //'onSubmit'  => 'return validateForm();'
                );
                echo form_open_multipart(site_url('purchase/purchase_order_attachment_query')."/".$form_header[0]['doc_num'], $attributes);
            ?>
            <div class="row">
                <div class="col-sm-4">
                    <input type="file" name="upload_file[]" multiple class="filestyle" data-buttonText="Find file">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-11">
                    <table class="table table-bordered table-hover" id="table_file_upload">
                    <thead>
                        <tr>
                            <th style="width:40%">Filename</th>
                            <th style="width:40%">Description</th>
                            <th style="width:20%">Upload Date</th>
                            <!-- <th>
                                <a href="javascript:add_file_upload();" class="btn btn-warning"><i class="fa fa-plus"></i></a>
                            </th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($form_attach as $a=>$attach){ ?>
                        <tr id="default_upload">
                            <td>
                                <input type='hidden' name="upload_filename[]" id="upload_filename[]" value="<?php echo $attach['file_name'];?>">
                                <a href="<?php echo base_url('/assets/files')."/".$attach['file_name'];?>"><?php echo $attach['file_desc'];?></a>
                            </td>
                            <td>
                                <input class="form-control" type="text" name="upload_filedesc[]" id="upload_filedesc[]" value="<?php echo $attach['file_desc'];?>">
                            </td>
                            <td>
                                <?php echo date("Y M d - H:i:s",strtotime($attach['upload_dt']));?>
                            </td>                            
                        </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><input type="submit" value="Save Attachment"></td>
                        </tr>
                    </tfoot>
                    </table>
                </div>
            </div>
            <?php //echo form_text('','hidden','return_url',site_url('purchase/purchase_order'));?>
            </form>
        </div>
    </div>
    </div>
    <br />	
</section>



<template class="button_action">
    <a id="new" class="btn btn-info" target="_blank" href="<?php echo base_url('/purchase/purchase_order_add/');?>">
        <i class="fa fa-plus"></i>&nbsp;&nbsp; New
    </a>
    
    <?php //echo form_button_action('View','success',$list_download);?>
    <?php //echo form_button_action('Action','success',$list_action);?>
    <!-- <a id="next" class="btn btn-info" href="<?php echo base_url('/purchase/purchase_receipt_add/')."/".$form_header[0]['doc_num'];?>">
        <i class="fa fa-sign-out"></i>&nbsp;&nbsp; Receipt
    </a> -->
    <?php $disable_edit = ($form_header[0]['doc_status'] == 'DRAFT') ? '' : 'disabled';?>
    <!-- <button type="submit" class="btn btn-info" id="submit" <?php echo $disable_edit;?>>
        <i class="fa fa-check"></i>&nbsp;&nbsp; Update
    </button> -->

    <?php if($form_header[0]['doc_status'] == 'DRAFT'){ ?>        
        <button type="submit" class="btn btn-info" id="confirm">
            <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
        </button>
        <button type="submit" class="btn btn-info" id="save">
            <i class="fa fa-save"></i>&nbsp;&nbsp; Save Draft
        </button>
    <?php } ?>

    <?php if($form_header[0]['doc_status'] == 'AWAITING' || $form_header[0]['doc_status'] == 'IN REVIEW' ){ ?>
        <?php echo form_button_action('View','success',$list_download);?>
        <?php echo form_button_action('Action','success',$list_action);?>
    <?php } else ?>

    <?php if($form_header[0]['doc_status'] == 'ACTIVE'){ ?>
        <?php echo form_button_action('View','success',$list_download);?>
        <?php echo form_button_action('Action','success',$list_action);?>
        <a id="next" class="btn btn-info" href="<?php echo base_url('/purchase/purchase_receipt_add/')."/".$form_header[0]['doc_num'];?>">
            <i class="fa fa-sign-out"></i>&nbsp;&nbsp; Receipt
        </a>
    <?php } ?>
    
    <?php if($form_header[0]['doc_status'] == 'CLOSED'){ ?>
        <?php echo form_button_action('View','success',$list_download);?>
    <?php } ?>

    <?php $status = doc_status($form_header[0]['doc_status']);?>
    <div class="btn btn-<?php echo $status['color'];?>">
        Doc Status : <?php echo $status['status'];?>
    </div>
</template>
<!-- /.content -->

<?php //LAYOUT HELPER: MODAL
	echo sw_createModal('modal_vendor','Select Vendor','dialog_vendor_push_data();','business/dialog_vendor');
	echo sw_createModal('modal_item','Select Item','dialog_inventory_push_data();','inventory/dialog_inventory');
?>

<script>
var source_location = '';
var source_vendor = '';
var source_inventory = '';
<?php if($form_header[0]['doc_status']=="DRAFT"){?>
    var source_location = <?php echo json_encode($ddl_whse);?>;
    var source_vendor = <?php echo json_encode($dialog_vendor);?>;
    var source_inventory = <?php echo json_encode($dialog_inventory);?>;
<?php } ?>
var form_header = <?php echo json_encode($form_header);?>;
var form_detail = <?php echo json_encode($form_detail);?>;

var return_url = "<?php echo site_url('purchase/purchase_order');?>";
var submit_url = "<?php echo site_url('purchase/purchase_order_edit_query');?>/"+form_header[0]['doc_num'];
var ajax_url_1 = "<?php echo base_url('inventory/ajax_inventory_info');?>";
var url_1 = "<?php echo site_url('inventory/detail');?>";
</script>
