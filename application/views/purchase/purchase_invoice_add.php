<!-- Content Header (Page header) -->

<?php $attributes = array(
    'class'     => 'form-horizontal',
    'role'      => 'form',
    'method'    => 'post', 
    'name'      => 'frm', 
    'id'        => 'frm',
    'onSubmit'  => 'return validateForm();'
    );
echo form_open('', $attributes);
echo form_text('','hidden','save_confirm','save');
?>  

<section class="content-header">
    <h1><a class="" href="<?php echo site_url('purchase/purchase_invoice');?>">Purchase Invoice</a> 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New 
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
    	<div class="col-sm-6">
        	<input type="hidden" id="doc_ref_id" name="doc_ref_id" value="" />
            <input type="hidden" id="doc_ref_type" name="doc_ref_type" value="" />
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="buss_name">
                	Vendor                    
                </label>
                <div class="col-sm-9">
                	<input type="hidden" id="buss_id" name="buss_id" value="">
                    <input type="hidden" id="buss_char" name="buss_char" value="">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="" readonly>                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" <?php if(isset($param) && $param)echo 'disabled';?>
                            	data-toggle="modal" href="#modal_vendor" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                   
                </div>
            </div>
            <!--<div class="form-group">
                <label class="col-sm-3" for="doc_ref">
                	Purchase Order                    
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="po_doc_ref_id" name="po_doc_ref_id" value="" />
                    <input type="hidden" id="po_doc_ref_type" name="po_doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="po_doc_ref" name="po_doc_ref" value="" readonly>                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_po" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                                       
                </div>
            </div>-->
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_ref">
                	<!-- Purchase Receipt -->Doc Ref
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="doc_ref_id" name="doc_ref_id" value="" />
                    <input type="hidden" id="doc_ref_type" name="doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="doc_ref" name="doc_ref" value="" readonly>
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" <?php if(isset($param) && $param)echo 'disabled';?>
                            	data-toggle="modal" href="#modal_rcv" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                 
                </div>
            </div>
            <!--<div class="form-group">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>-->
            <!--<div class="form-group">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr" readonly></textarea>
                </div>
            </div> -->      
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                </div>
            </div>     
        </div>
    	<div class="col-sm-6">
        	<div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                </div>
            </div>	
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_ext_ref">Vendor Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_ext_ref" name="doc_ext_ref" value="">
                </div>
            </div>  
        	<div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>		
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_ddt">Due Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>
            
        </div>
    </div>
    <div class="row">
    	<div class="col-sm-12">
        	<div class="table-responsive" style="overflow:auto;">
        	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
            <thead>
            <tr>
            	<th style="width:3%">#</th>
                <th style="width:12%">Item Code</th>
                <th style="width:12%">Item Name</th>
                <th style="width:8%">Quantity</th>
                <th style="width:10%">Price</th>
                <th style="width:8%">Disc %</th>
                <th style="width:10%">Tax</th>
                <th style="width:12%">Net Price</th>
                <th style="width:15%">Total</th>
                <th style="width:10%" class="action">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr id="default">
            	<input type="hidden" name="item_id[]" id="item_id1" class="form-control">
            	<td style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                    <input type="hidden" name="reff_line[]" id="reff_line1" value="" />
                </td>
                <td>
                	<div class="input-group">                    	
	                    <span class="input-group-btn">
                            <a href="#" class="btn btn-warning form-control" name="a_ast_code" target="_blank">
                            	<i class="fa fa-external-link"></i>
                            </a>                            
						</span>
                        <input type="text" name="ast_code[]" id="ast_code1" class="form-control " readonly="readonly">
                    </div>
                </td>
                <td><input type="text" name="ast_name[]" id="ast_name1" class="form-control" readonly="readonly"></td>
                <td>                	
                    <input type="hidden" name="ast_rcv_qty[]" id="ast_rcv_qty1">
                   	<input type="hidden" name="ast_qty_open[]" id="ast_qty1">
                    <input type="number" name="ast_qty[]" id="ast_qty1" readonly="readonly" 
                        class="form-control" style="text-align:right">
				</td>
                <td><input type="number" name="ast_price[]"  id="ast_price1" class="form-control" style="text-align:right"></td>
                <td>
                    <input type="number" name="ast_disc[]" id="ast_disc1" class="form-control" style="text-align:right">
                </td>
                <td>
                    <!-- <input type="text" name="ast_tax[]"  id="ast_tax1"  class="form-control" style="text-align:right"> -->
                    <?php echo sw_CreateDropdown(array('ast_tax[]','ast_tax1'),$ddl_tax,'tax_percent','tax_name');?>
                </td>
                <td>
                    <input type="number" name="ast_netprice[]"  id="ast_netprice1"  class="form-control" style="text-align:right" readonly tabindex="-1">
                </td>
                <td><input type="number" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right"readonly></td>
                
                <td>
                	<div class="input-group">
						<div class='input-group-btn'>
                            <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                            	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
						</div>
					</div>
				</td>
            </tr>
            </tbody>
            <tfoot>
            	<tr>
                <td></td>
                <td></td>
                <td></td>
                <td><div>
                    <input type="number" class="form-control" id="qty_total" name="qty_total" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
                <td></td>
                <td></td>
                <td id="foot_label"><strong>Sub Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_subtotal" name="doc_subtotal" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>Disc:&nbsp;&nbsp;&nbsp;</strong></td>
                <td id="foot_label" style="text-align:right;">
                    <input type="number" class="col-sm-8" id="doc_disc_pct" name="doc_disc_pct"
                        value="0" style="text-align:right;" />%
                    <!-- <strong>Disc:&nbsp;&nbsp;&nbsp;</strong> -->
                </td>
                <td><div>
                    <input type="text" class="form-control" id="doc_disc" name="doc_disc" 
                        value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_total" name="doc_total" 
                        value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>              
            </tfoot>
            </table>
           	</div>
    	</div>
    </div>
    
    <hr />
    <!--<div class="row">
    	<div class="col-sm-12">
            <table class="table-bordered" id="table_expense">
                <thead>
                <tr>
                    <td width="10%">#</td>
                    <td width="45%">Expense</td>
                    <td width="25%">Amount</td>
                    <td width="20%">Action</td>
                </tr>
                </thead>
                <tbody>
                	<tr height="30px">
                        <td style="text-align:center">
                        	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                        </td>
                        <td><input type="text" name="ast_qty_open[]" id="ast_qty1" 
	                    	class="form-control">
                       	</td>
                        <td><input type="number" name="ast_qty_open[]" id="ast_qty1" 
                    		class="form-control" style="text-align:right">
                        </td>
                        <td>
                        	<div style='text-align:center'>
                                <div class='btn-group'>
                                    <button type="button" class="btn btn-flat btn-xs btn-danger" id="act_del1" name="act_del">DELETE</button>
                                </div>
                            </div>
						</td>
                    </tr>
                </tbody>
			</table>
        </div>
    </div>-->
    <!--<br />-->
    <div class="row">
    	<div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
		</div>
    </div>    
	</form>	
</section>
<!-- /.content -->

<?php //LAYOUT HELPER: MODAL
	echo sw_createModal('modal_vendor','Select Vendor','dialog_vendor_push_data();','business/dialog_vendor');
	echo sw_createModal('modal_po','Select PO','dialog_purchase_order_push_data();','purchase/dialog_purchase_order');
	echo sw_createModal('modal_rcv','Select Receipt','dialog_purchase_receipt_push_data();','purchase/dialog_purchase_receipt');
?>
<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
//var source_po = <?php// echo json_encode($dialog_po);?>;
var source_vendor = <?php echo json_encode($dialog_vendor);?>;
var source_rcv = <?php //echo json_encode($dialog_rcv);?>'';
var source_po = <?php //echo json_encode($dialog_po);?>'';

var return_url = "<?php echo site_url('purchase/purchase_invoice');?>";
var submit_url = "<?php echo site_url('purchase/purchase_invoice_add_query');?>";
var ajax_url_1 = "<?php echo base_url('purchase/ajax_getAll_rcv_with_vendor');?>";
var ajax_url_2 = "<?php echo base_url('purchase/ajax_getDetail_rcv_open');?>";
var ajax_url_3 = "<?php echo base_url('purchase/ajax_getHeader_rcv');?>";
var ajax_url_4 = "<?php echo base_url('purchase/ajax_getAll_po_with_vendor');?>";
var ajax_url_5 = "<?php echo base_url('purchase/ajax_getDetail_po');?>";
var ajax_url_6 = "<?php echo base_url('purchase/ajax_getHeader_po');?>";
var url_1 = "<?php echo site_url('inventory/detail');?>";

var param = "<?php echo $param;?>";

</script>
<script type="text/javascript">
function get_rcv_detail(){
	return $.ajax({
		url:ajax_url_2,
		//data:{doc_num:$("#rcv_doc_ref").val()},
		data:{doc_num:$("#doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
        }
	});
}
function get_po_detail(){
	return $.ajax({
		url:ajax_url_5,
		data:{doc_num:$("#po_doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
        }
	});
}
</script>
