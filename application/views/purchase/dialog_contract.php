<?php $column = array('','Asset Code','Asset Name');?>
<div class="row">
	<div class="col-sm-12 col-xs-12">
		Search : <input type="text" id="search_contract" name="search_contract" value="" accesskey="S"/>
        <table class="table table-condensed table-hover table-striped" id="dialog_contract_list">
        <thead>
            <tr>
                <th width="10%">&nbsp;</th>
                <th width="20%">Code</th>
                <th width="30%">Date</th>
                <th width="30%">Contact</th>
            </tr>
        </thead>
        <tbody>
        
        </tbody>
        </table>
    </div>
</div>

<?php 
echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>

<script type="text/javascript">
/*---
1.reset_table(table)
2.populate_table(table)
3.dialog_purchase_order_push_data
---*/
var data_contract = '';
var contract_default_row = '<tr>'+
	'<td><input type="checkbox" name="chk[]" id="chk"/></td>'+
	'<td><input type="hidden" name="doc_id[]" id="doc_id" /><span></span></td>'+
	'<td></td><td></td></tr>';

function dialog_contract_reset_table(table){
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(contract_default_row);
}
function dialog_contract_populate_data(table,data){ 
	data_contract = data; 
	if(data_contract){
		//delete first row
		if($('#'+table+' > tbody > tr').length != 1){
			$('#'+table+' > tbody > tr:last').remove();
		}
		for(var p = 0; p<data_contract.length; p++){
			var row = clone_row(table);
			row.find('td:eq(1)').find('input[type=hidden]').val(data_contract[p]['doc_id']);
			row.find('td:eq(1)').find('span').text(data_contract[p]['doc_num']);
			row.find('td:eq(2)').text(data_contract[p]['doc_dt']);
			row.find('td:eq(3)').text(data_contract[p]['contact_name']);
		}
		var row = clone_row(table);	
		//delete first and last after insert
		$('#'+table+' > tbody > tr:first').remove();
		$('#'+table+' > tbody > tr:last').remove();
	}else{
		console.log('error');	
	}
}

function dialog_contract_push_data(){
	var result;
	//select process
	var selected = $('#dialog_contract_list > tbody').find('input[name="chk[]"]:checked').parent().parent().find('td:eq(1) > input[type=hidden]').val();
	for( var i = 0; i < source_contract.length; i++ ) { 
		if( source_contract[i]['doc_id'] === selected ) { 
			result = source_contract[i];
			break;
		}
	}
	dialog_contract_pull_data(result);
}

$(document).ready(function(){
	$(document).on('click','#dialog_contract_list tbody > tr',function(event){
		//SINGLE SELECTION
		if(event.target.type !== 'checkbox'){
			$(':checkbox',this).trigger('click');
		}else{
			if($(this).hasClass('selected')){
				$(this).removeClass('selected');
			}else{
				$(this).siblings().removeClass('selected');
				$(this).siblings().find('td > input:checkbox').prop("checked",false);
				$(this).addClass('selected');
			}
		}
	});
});
$("#modal_contract").on('keypress', function(event) {
	console.log(event);
	//alert('sefsefe');
    //$(this).find("#search").focus();
});
$("#search_contract").on("keyup", function() {
    var value = $(this).val();
	var cols = $("#dialog_contract_list").find("tr:first th").length;
    $("#dialog_contract_list tbody tr").each(function(index) { 
		var show = 0;
		$row = $(this);
		for(var x = 0;x<=cols;x++){
			if($row.hasClass('selected')){show++; break;}
			if($row.find("td:eq("+x+")").text().toLowerCase().indexOf(value.toLowerCase()) !== -1){// console.log('yes');
				show++; break;
			}
		}
		(show>0) ? $row.show(): $row.hide();		
    });
});
</script>