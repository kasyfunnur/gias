<!-- Content Header (Page header) -->

<?php echo validation_errors(); ?>
<?php $attributes = array(
    'class'     => 'form-horizontal',
    'role'      => 'form',
    'method'    => 'post', 
    'name'      => 'frm', 
    'id'        => 'frm',
    //'onSubmit'  => 'return validateForm();'
    'onSubmit'  => 'return validate_form();'
    );
//echo form_open('', $attributes);
echo form_open_multipart('purchase/purchase_contract_add_query', $attributes);

echo form_text('','hidden','save_confirm','save');
?>  

<section class="content-header">
    <h1><a class="" href="<?php echo site_url('purchase/purchase_contract');?>">Purchase Contract</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New 
    </h1>
</section>

<!-- Main content -->
<section class="content">	
    <div class="row">
    	<div class="col-sm-6">        	
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="buss_name">
                	Vendor                    
                </label>
                <div class="col-sm-9">
                	<input type="hidden" id="buss_id" name="buss_id" value="">
                    <input type="hidden" id="buss_char" name="buss_char" value="">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="" >
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_vendor" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                    
                </div>
            </div>

            <!-- <div class="form-group col-sm-12">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr" readonly></textarea>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_note">Document Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                </div>
            </div> -->
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="contact_name">Contact Person</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="contact_name" name="contact_name" >
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="contact_phone">Contact Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="contact_phone" name="contact_phone" >
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="contact_email">Contact Email</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="contact_email" name="contact_email" >
                </div>
            </div>

        </div>
    	<div class="col-sm-6">
        	<div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                </div>
            </div>	
        	<div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="start_dt">Start Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="start_dt" name="start_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>		
            <div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="end_dt">End Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="end_dt" name="end_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>
            <div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="terminate_dt">Termination</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="terminate_dt" name="terminate_dt" value="" readonly>
                </div>
            </div>      
            <div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="sign_dt">Signed on </label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="sign_dt" name="sign_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    	<div class="col-sm-12">
            <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active col-sm-2 well no-padding">
                    <a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a>
                </li>
                <li role="presentation" class="col-sm-2 well no-padding">
                    <a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Detail</a>
                </li>
                <li role="presentation" class="col-sm-2 well no-padding">
                    <a href="#document" aria-controls="document" role="tab" data-toggle="tab">Document</a>
                </li>
                <li role="presentation" class="col-sm-2 well no-padding">
                    <a href="#attachment" aria-controls="attachment" role="tab" data-toggle="tab">Attachment</a>
                </li>
                
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="general"><br>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group col-sm-12">
                                <!-- <div class="col-sm-3"> -->
                                    <label class="col-sm-3" for="agreement_method">Type of Contract</label>
                                <!-- </div> -->
                                <div class="col-sm-9">
                                    <select class="form-control" name="agreement_method" id="agreement_method">
                                        <option value="GENERAL">General</option>
                                        <option value="SPECIFIC">Specific</option>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-sm-3">
                                    <label>Ignore Price?</label>
                                    
                                </div>
                                <div class="col-sm-9">
                                    <input type="checkbox" name="contract_price" id="contract_price"> <label for="contract_price">Yes</label>
                                </div>
                            </div> -->
                            <div class="form-group col-sm-12">
                                <label class="col-sm-3" for="contract_price">Ignore Price?</label>
                                <div class="col-sm-9">
                                    <input type="checkbox" name="contract_price" id="contract_price"> <label for="contract_price">Yes</label>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-3" for="doc_note">Document Memo</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group col-sm-12">
                                <label class="col-sm-3" for="doc_status">Status</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-sm" id="doc_status" name="doc_status" value="" readonly>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-3" for="payment_term">Payment Term</label>
                                <div class="col-sm-9">
                                    <!-- <select class="form-control" name="contract_type" id="contract_type">
                                        <option value="GENERAL">General</option>
                                        <option value="SPECIFIC">Specific</option>
                                    </select> -->
                                    <input type="text" class="form-control input-sm" id="payment_term" name="payment_term" value="" >
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-3" for="payment_method">Payment Method</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-sm" id="payment_method" name="payment_method" value="" >
                                    <!-- <select class="form-control" name="contract_type" id="contract_type">
                                        <option value="GENERAL">General</option>
                                        <option value="SPECIFIC">Specific</option>
                                    </select> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="detail"><br>
                    <div class="table-responsive" style="overflow:auto;">
                    <table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
                    <thead>
                    <tr>
                        <th style="width:5%">#</th>
                        <th style="width:15%">
                            <button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                                <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Item Code</span>
                                <span class="visible-xs"><i class="fa fa-search"></i></span>
                            </button>
                        </th>
                        <th style="width:15%">Item Name</th>
                        <th style="width:10%">Quantity</th>
                        <th style="width:20%">Price</th>
                        <th style="width:10%">UoM</th>
                        <th style="width:15%">Total</th>
                        <th style="width:10%" class="action">Action</th>
                    </tr>
                    </thead>            
                    <tbody>
                    <tr id="default">
                        <input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                        
                        <td style="text-align:center">
                            <input type="text" name="row[]" id="row1" class="form-control" value="1" readonly tabindex="-1"/>
                        </td>
                        <td>
                            <div class="input-group">                       
                                <span class="input-group-btn">
                                    <a href="#" class="btn btn-warning form-control" name="a_ast_code" target="_blank" tabindex="-1">
                                        <i class="fa fa-external-link"></i>
                                    </a>                            
                                </span>
                                <input type="text" name="ast_code[]" id="ast_code1" class="form-control " readonly="readonly" tabindex="-1">
                            </div>
                        </td>
                        <td>
                            <input type="text" name="ast_name[]" id="ast_name1" class="form-control">
                        </td>
                        <td>
                            <input type="number" name="ast_qty[]"  id="ast_qty1"  class="form-control" style="text-align:right">
                        </td>
                        <td>          
                            <input type="number" name="ast_price[]" id="ast_price1" class="form-control" style="text-align:right">
                        </td>
                        <td>
                            <input type="text" name="ast_uom[]" id="ast_uom1" class="form-control">
                        </td>
                        
                        <td>
                            <input type="text" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right" readonly tabindex="-1">
                        </td>
                        <td>
                            <div class="input-group">
                                <div class='input-group-btn'>
                                    <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                                        <span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
                                        <span class="visible-xs"><i class="fa fa-ban"></i></span>
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                        <td></td>
                        <td><div>
                            <input type="text" class="form-control" id="doc_subtotal" name="doc_subtotal"
                                value="0" style="text-align:right;" readonly>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                    </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="document">
                    <div class="table-responsive" style="overflow:auto;">
                    <table class="table-bordered" id="table_detail_document" style="table-layout:fixed;white-space:nowrap;width:960px">
                    <thead>
                        <tr>
                            <th style="width:10%">#</th>
                            <th style="width:30%">Type</th>
                            <th style="width:30%">Doc Num</th>
                            <th style="width:30%">Total</th>
                        </tr>
                    </thead>            
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Purchase Order</td>
                            <td>PO-01243</td>
                            <td>257,000</td>
                        </tr>
                    </tbody>
                    </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="attachment">
                    <div class="row">
                        <div class="col-sm-4">
                            <input type="file" name="upload_file" multiple class="filestyle" data-buttonText="Find file">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9">
                            <table class="table table-bordered table-hover" id="table_file_upload">
                            <thead>
                                <tr>
                                    <th>Filename</th>
                                    <th>Description</th>
                                    <th>Upload Date</th>
                                    <!-- <th>
                                        <a href="javascript:add_file_upload();" class="btn btn-warning"><i class="fa fa-plus"></i></a>
                                    </th> -->
                                </tr>
                            </thead>
                            <tbody> 
                                <tr id="default_upload">
                                    <td>
                                        <!-- <input type="file" name="uplaod_filename" multiple> -->
                                    </td>
                                    <td>
                                        <!-- <input type="text" name="upload_filedesc[]" id="upload_filedesc[]" value=""> -->
                                    </td>
                                    <td>&nbsp;</td>                            
                                </tr>
                            </tbody>
                            </table>
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                <!-- <div class="col-sm-12">
                                    <input type="file" name="upload" multiple class="filestyle" data-buttonText="Find file">
                                    <a href="#" class="btn btn-warning modal_caller" data-toggle="modal" data-target="#modal_upload">
                                        <span style="text-decoration:underline"><i class="fa fa-cloud-upload"></i> Upload File</span>
                                    </a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
    	</div>
    </div>
    <br />
    <div class="row">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right" id="confirm">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
            <button type="submit" class="btn btn-info pull-right" id="save">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Save Draft
            </button>
        </div>
    </div>
</section>
</form>

<?php //LAYOUT HELPER: MODAL
	echo sw_createModal('modal_vendor','Select Vendor','dialog_vendor_push_data();','business/dialog_vendor');
	echo sw_createModal('modal_item','Select Item','dialog_inventory_push_data();','inventory/dialog_inventory');
?>
<script src="<?php echo site_url('assets/js/bloodhound.js');?>"></script>
<script src="<?php echo site_url('assets/js/typeahead.jquery.js');?>"></script>
<script src="<?php echo site_url('assets/js/typeahead.bundle.js');?>"></script>

<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
var source_vendor = <?php echo json_encode($dialog_vendor);?>;
var source_inventory = <?php echo json_encode($dialog_inventory);?>;

<?php if(isset($form_header)){ ?>
var form_header = <?php echo json_encode($form_header);?>;
<?php }?>
<?php if(isset($form_detail)){ ?>
var form_detail = <?php echo json_encode($form_detail);?>;
<?php }?>

var return_url = "<?php echo site_url('purchase/purchase_contract');?>";
var submit_url = "<?php echo site_url('purchase/purchase_contract_add_query');?>";
var ajax_url_1 = "<?php echo base_url('inventory/ajax_inventory_info');?>";
var url_1 = "<?php echo site_url('inventory/detail');?>";

function validateForm(){
    if(confirm("Are you sure???")==true){     
        $.post(
            submit_url,
            $('#frm').serialize(),
            function(response){                 
                if(response==1){
                    alert("Data updated successfully!");
                    window.location.href=return_url;
                }else{
                    console.log("ERROR: "+response);
                }
            }
        );
    }
    return false;
}
function validate_form(){
    if(confirm("Are you sure?? Purchase Add??")==true){
        return true;
    }
    return false;
}
</script>
