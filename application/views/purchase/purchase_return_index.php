<section class="content-header">
  	<h1> Purchase Return <a href="<?php echo site_url('purchase/purchase_return_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<span class="hidden-xs">
        		<i class="fa fa-plus"></i>&nbsp;&nbsp;New Return
            </span>
        	<span class="visible-xs">
        		<i class="fa fa-plus"></i>
            </span>
		</button>
    </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<section class="content">
<div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body table-responsive">
  	<table class="table table-hover">
    	<thead>
        	<tr>
            	<th style="width:20%">Document</th>
                <th style="width:50%">Note</th>
                <th style="width:30%">Actions</th>
            </tr>
        </thead>
        <tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>
			<?php 
                $document_status = $this->status_model->status_label('document',$datatable[$x]['doc_status']);
				$finance_status = $this->status_model->status_label('financial',$datatable[$x]['financial_status']);
				if($datatable[$x]['doc_status'] == "CANCELLED"){
					$color = '#F00';
				}elseif($datatable[$x]['doc_status'] == "CLOSED"){
					$color = '#F90';
				}else{
					$color = '#09F';
				}
            ?>
            <tr>
                <td>
                	<span style="font-size:16px;font-weight:lighter !important;color:#888">
                        <a href="<?php echo site_url('purchase/purchase_invoice_edit');?>/<?php echo $datatable[$x]['doc_num'];?>">
                            <?php echo $datatable[$x]['doc_num'];?>
                        </a> 
                        - <?php echo $datatable[$x]['buss_name'];?> <?php //echo $document_status;?>                        
                    </span>
                    <br />
                    <span style="font-size:12px;font-weight:lighter !important;color:#888">
                        By <?php echo $datatable[$x]['create_by'];?>
                        - <?php echo date("d M, Y", strtotime($datatable[$x]['doc_dt']));?>
                        <?php //echo $document_status;?>
                        - <strong>[<?php echo $datatable[$x]['doc_status'];?>]</strong>
                    </span>
                </td>
                <?php 
					$link_payment = "#"; 
					$link_add = '<span class="btn btn-xs btn-default disabled" style="color:#fcfcfc"><i class="fa fa-plus"></i></span>';;
					$disable = 'disabled';
					$link_cancel = base_url('purchase/purchase_receipt_cancel')."/".$datatable[$x]['doc_num'];
					$link_close = base_url('purchase/purchase_receipt_close')."/".$datatable[$x]['doc_num'];
					$link_print = base_url('purchase/print_rcv')."/".$datatable[$x]['doc_num'];
					$link_logistic = base_url('sales/sales_delivery')."/".$datatable[$x]['doc_num'];
					if($datatable[$x]['doc_status'] != "CANCELLED"
						&& $datatable[$x]['doc_status'] != "CLOSED"){
						$link_payment = site_url('finance/payment_out_add')."/".$datatable[$x]['doc_num'];
						$link_add = '<span class="btn btn-xs btn-default"><i class="fa fa-plus"></i></span>';
						$disable = '';
					} 
				?>
                <td>
                	[Due : <?php echo date("d M, Y", strtotime($datatable[$x]['doc_ddt']));?>]
					<?php echo $finance_status;?>
                	<?php echo $datatable[$x]['doc_note'];?>
                </td>
                <td>
                    <a href="<?php echo $link_cancel;?>" class="btn btn-danger <?php echo $disable;?>">
                        <i class="fa fa-ban"></i> Cancel</a>&nbsp;&nbsp;
					<a href="<?php echo $link_close;?>" class="btn btn-warning <?php echo $disable;?>">
                        <i class="fa fa-archive"></i> Close</a>&nbsp;&nbsp;
                    <a href="<?php echo $link_logistic;?>" class="btn btn-info <?php echo $disable;?>" target="_blank">
                        <i class="fa fa-plus"></i> Send Back</a>                
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table></div>
    </div>
</section>
<!-- /.content -->