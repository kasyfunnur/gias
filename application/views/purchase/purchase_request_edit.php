<!-- Content Header (Page header) -->
<?php 
$list_action = array(
    array('link'=>base_url('/purchase/purchase_order_duplicate')."/".$form_header[0]['doc_num'],'icon'=>'files-o','text'=>'Duplicate'),    
    array('link'=>base_url('/purchase/purchase_order_cancel')."/".$form_header[0]['doc_num'],'icon'=>'ban','text'=>'Cancel'),
    array('link'=>base_url('/purchase/purchase_order_close')."/".$form_header[0]['doc_num'],'icon'=>'minus-circle','text'=>'Close')
);
$list_download = array(
    array('link'=>base_url('/purchase/purchase_order_print')."/".$form_header[0]['doc_num'],'icon'=>'file-excel-o','text'=>'Excel'),
    array('link'=>base_url('/purchase/purchase_order_print')."/".$form_header[0]['doc_num'],'icon'=>'file-pdf-o','text'=>'PDF'),
    array('link'=>base_url('/purchase/purchase_order_print')."/".$form_header[0]['doc_num'],'icon'=>'print','text'=>'PRINT')
);
?>
<?php 
    echo form_open('', config_form());
    echo form_text('','hidden','doc_id',$form_header[0]['doc_id']);
?>  
<section class="content-header">
    <h1>
        <a class="" href="<?php echo site_url('purchase/purchase_request');?>">Purchase Request</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Edit
        &nbsp;&nbsp;&nbsp;
        <div class="pull-right button_action" id="button_action"></div>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
    	<div class="col-sm-6">        	
            <!-- <div class="form-group col-sm-12">
                <label class="col-sm-3" for="buss_name">
                	Vendor                    
                </label>
                <div class="col-sm-9">
                	<input type="hidden" id="buss_id" name="buss_id" value="">
                    <input type="hidden" id="buss_char" name="buss_char" value="">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="" >
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_vendor" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                    
                </div>
            </div> -->
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <?php echo form_text_ro('Shipping Address','textarea','ship_addr',$form_header[0]['ship_addr']);?>
            </div>
            <div class="form-group col-sm-12">
                <?php echo form_text('Memo','textarea','doc_note',$form_header[0]['doc_note']);?>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group col-sm-12">
                <?php echo form_text_ro('Doc #','text','doc_num',$form_header[0]['doc_num']);?>
            </div>	
        	<div class="form-group col-sm-12 col-xs-6">
                <?php echo form_text('Date','date','doc_dt',substr($form_header[0]['doc_dt'],0,10));?>
            </div>		
            <div class="form-group col-sm-12 col-xs-6">
                <?php echo form_text('Date Required','date','doc_ddt',substr($form_header[0]['doc_ddt'],0,10));?>
            </div>
        </div>
    </div>    

    <div class="row">
    	<div class="col-sm-12">
        	<div class="table-responsive" style="overflow:auto;">
        	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
            <thead>
            <tr>
            	<th style="width:5%">#</th>
                <th style="width:20%">
                	<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                        <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Item Code</span>
						<span class="visible-xs"><i class="fa fa-search"></i></span>
                        <?php //echo sw_altButton('Item_code','fa-search');?>
                    </button>
                </th>
                <th style="width:20%">Item Name</th>
                <th style="width:10%">Quantity</th>
                <!-- <th style="width:15%">Price</th> -->
                <!-- <th style="width:20%">Total</th> -->
                <th style="width:10%" class="action">Action</th>
            </tr>
            </thead>            
            <tbody>
            <tr height="25px" id="default">
	            <input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                <td style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                </td>
                <td>
                	<div class="input-group">                    	
	                    <span class="input-group-btn">
                            <a href="#" class="btn btn-warning form-control" name="a_ast_code" target="_blank">
                            	<i class="fa fa-external-link"></i>
                            </a>                            
						</span>
                        <input type="text" name="ast_code[]" id="ast_code1" class="form-control " readonly="readonly">
                    </div>
                </td>
                <td>
                	<input type="text" name="ast_name[]" id="ast_name1" class="form-control">
                </td>
                <td>
                	<input type="number" name="ast_qty[]"  id="ast_qty1"  class="form-control" style="text-align:right">
                </td>
                <!-- <td>
                	<input type="text" name="ast_price[]"  id="ast_price1"  class="form-control" style="text-align:right">
                </td> -->
                <!-- <td>
                	<input type="text" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right"readonly>
                </td> -->
                <td>
                	<div class="input-group">
						<div class='input-group-btn'>
                            <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                            	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
						</div>
					</div>
				</td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <!-- <td></td> -->
                <td></td>
                <td></td>
                <td id="foot_label"><strong>Sub Total:&nbsp;&nbsp;&nbsp;</strong></td>
                
                <td><div>
                    <input type="number" class="form-control" id="qty_total" name="qty_total" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
                <!-- <td><div>
                    <input type="text" class="form-control" id="doc_subtotal" name="doc_subtotal" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td> -->
            </tr>
            <!-- <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td id="foot_label" style="text-align:right;">
                	<input type="number" class="col-sm-5" id="doc_disc_pct" name="doc_disc_pct"
                    	value="0" style="text-align:right;" />%
                	<strong>Disc:&nbsp;&nbsp;&nbsp;</strong>
                </td>
                <td><div>
                    <input type="text" class="form-control" id="doc_disc" name="doc_disc" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_total" name="doc_total" 
                    	value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr> -->
            </tfoot>
            </table>
            </div>
    	</div>
    </div>  
    
    <br />
    <button type="submit" class="btn btn-info pull-right">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
    <br />
	</form>
</section>
<!-- /.content -->

<?php //LAYOUT HELPER: MODAL
	//echo sw_createModal('modal_vendor','Select Vendor','dialog_vendor_push_data();','business/dialog_vendor');
	echo sw_createModal('modal_item','Select Item','dialog_inventory_push_data();','inventory/dialog_inventory');
?>
<script src="<?php echo site_url('assets/js/bloodhound.js');?>"></script>
<script src="<?php echo site_url('assets/js/typeahead.jquery.js');?>"></script>
<script src="<?php echo site_url('assets/js/typeahead.bundle.js');?>"></script>

<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
//var source_vendor = <?php //echo json_encode($dialog_vendor);?>;
var source_inventory = <?php echo json_encode($dialog_inventory);?>;

var return_url = "<?php echo site_url('purchase/purchase_request');?>";
var submit_url = "<?php echo site_url('purchase/purchase_request_add_query');?>";
var ajax_url_1 = "<?php echo base_url('inventory/ajax_inventory_info');?>";
var url_1 = "<?php echo site_url('inventory/detail');?>";

</script>
