<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('purchase/purchase_receipt');?>">Purchase Receipt</a> 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New 
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <?php echo form_open('', config_form());?>
    
    <div class="container-fluid">
        <div class="row">
        	<div class="col-sm-6">
                <div class="form-group col-sm-12">
                    <label class="col-sm-3" for="buss_name">
                    	Vendor                    
                    </label>
                    <div class="col-sm-9">
                    	<input type="hidden" id="buss_id" name="buss_id" value="">
                        <input type="hidden" id="buss_char" name="buss_char" value="">
                    	<div class="input-group input-group-sm">
                            <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="" readonly />
                        	<span class="input-group-btn">
    							<button type="button" class="btn btn-warning" <?php if(isset($param) && $param)echo 'disabled';?>
                                	data-toggle="modal" href="#modal_vendor" style="width:100%;">
                                	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
    						</span>
                        </div>                   
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label class="col-sm-3" for="doc_ref">
                    	Purchase Order                    
                    </label>
                    <div class="col-sm-9">
                        <input type="hidden" id="doc_ref_id" name="doc_ref_id" value="" />
                        <input type="hidden" id="doc_ref_type" name="doc_ref_type" value="" />
                    	<div class="input-group input-group-sm">
                            <input type="text" class="form-control input-sm" id="doc_ref" name="doc_ref" value="">
                        	<span class="input-group-btn">
    							<button type="button" class="btn btn-warning" <?php if(isset($param) && $param)echo 'disabled';?>
                                	data-toggle="modal" href="#modal_po" style="width:100%;">
                                	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
    						</span>
                        </div>                                      
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label class="col-sm-3" for="location_id">Location</label>
                    <div class="col-sm-9">
                        <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name');?>
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                    <div class="col-sm-9">
                        <textarea class="form-control input-sm" id="ship_addr" name="ship_addr" readonly></textarea>
                    </div>
                </div>            
            </div>
        	<div class="col-sm-6">
            	<div class="form-group col-sm-12">
                    <label class="col-sm-3" for="doc_num">Doc #</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                    </div>
                </div>	
                <div class="form-group col-sm-12">
                    <label class="col-sm-3" for="doc_ext_ref">Doc Ext #</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control input-sm" id="doc_ext_ref" name="doc_ext_ref" value="">
                    </div>
                </div>
            	<div class="form-group col-sm-12">
                    <label class="col-sm-3" for="doc_dt">Date</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo date('Y-m-d',time());?>">
                    </div>
                </div>		
                <div class="form-group col-sm-12">
                    <label class="col-sm-3" for="doc_ddt">Date Required</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>" readonly>
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label class="col-sm-3" for="doc_note">Memo</label>
                    <div class="col-sm-9">
                        <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                    </div>
                </div>
            </div>
        </div>
    
    
        <div class="row">
        	<div class="col-sm-12">
            	<div class="table-responsive" style="overflow:auto;">
            	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
                <thead>
                <tr>
                	<th style="width:5%">#</td>
                    <th style="width:15%">Item Code</td>
                    <th style="width:15%">Item Name</td>
                    <th style="width:20%">Open Qty</td>
                    <th style="width:20%">Qty</td>
                    <th style="width:10%">Action</td>
                </tr>
                </thead>
                <tbody>
                <tr id="default">
                	<input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                	
                    <td style="text-align:center">
                    	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                        <input type="hidden" name="reff_line[]" id="reff_line1" value="" />

                        <input type="hidden" name="ast_price_temp[]" id="ast_price_temp1">                    
                        <input type="hidden" name="const_disc[]" id="const_disc1" value="100">
                        <input type="hidden" name="const_after_disc[]" id="const_after_disc1">
                        <input type="hidden" name="ast_disc_factor[]" id="ast_disc_factor1" >
                        <input type="hidden" name="ast_price_after_disc[]" id="ast_price_after_disc1" >
                        <input type="hidden" name="const_tax[]" id="const_tax1" value="100">
                        <input type="hidden" name="const_after_tax[]" id="const_after_tax1">
                        <input type="hidden" name="ast_tax_factor[]" id="ast_tax_factor1" >
                    </td>
                    <td>
                    	<div class="input-group">                    	
    	                    <span class="input-group-btn">
                                <a href="#" class="btn btn-warning form-control" name="a_ast_code" target="_blank">
                                	<i class="fa fa-external-link"></i>
                                </a>                            
    						</span>
                            <input type="text" name="ast_code[]" id="ast_code1" class="form-control " readonly="readonly">
                        </div>
                    </td>
                    <td><input type="text" name="ast_name[]" id="ast_name1" class="form-control" readonly="readonly"></td>
                    <td>
                    	<input type="hidden" name="ast_po_qty[]" id="ast_po_qty1" />
                        <input type="hidden" name="ast_price[]" id="ast_price1" />
                    	<input type="hidden" name="ast_disc[]" id="ast_disc1" />
                        <input type="hidden" name="ast_tax[]" id="ast_tax1" />
                        <input type="hidden" name="ast_netprice[]" id="ast_netprice1" />
                        <input type="hidden" name="sub_total[]" id="sub_total1" />
                        <input type="number" name="ast_qty_open[]" id="ast_qty1" 
                        	class="form-control" readonly="readonly" style="text-align:right">                
                    </td>
                    <td>
                    	<input type="number" name="ast_qty[]" id="ast_qty1" 
                    		class="form-control" style="text-align:right">
    				</td>
                    <td>
                    	<div class="input-group">
    						<div class='input-group-btn'>
                                <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                                	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
    	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                                </button>
    						</div>
    					</div>
    				</td>
                </tr>
                </tbody>
                <tfoot>
                    <input type="hidden" name="doc_subtotal" id="doc_subtotal">
                    <input type="hidden" name="doc_disc_pct" id="doc_disc_pct">
                    <input type="hidden" name="doc_disc" id="doc_disc">
                    <input type="hidden" name="doc_total" id="doc_total">
                	<tr>
                    	<td colspan="4" id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                        <td>
                        	<div>
                        	<input type="number" class="form-control" id="qty_total" name="qty_total" value="0" 
                            	style="text-align:right;" readonly>
                            </div>
                       	</td>
                    </tr>
                    <tr>
                    	<td colspan="3" id="foot_label"><strong>Freight:&nbsp;&nbsp;&nbsp;</strong></td>
                        <td>
                            <div class="input-group input-group-sm">                                
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success" >
                                        <i class="fa fa-money"></i>&nbsp;&nbsp; IDR</button>                                
                                </span>
                                <input type="number" class="form-control" id="exp_freight" name="exp_freight" value="0" 
                                    style="text-align:right;">
                            </div>
                       	</td>
                    </tr>
                    <tr>
                    	<td colspan="3" id="foot_label"><strong>Other Cost:&nbsp;&nbsp;&nbsp;</strong></td>
                        <td>
                            <div class="input-group input-group-sm">                                
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success" >
                                        <i class="fa fa-money"></i>&nbsp;&nbsp; IDR</button>                                
                                </span>
                                <input type="number" class="form-control" id="exp_other" name="exp_other" value="0" 
                                    style="text-align:right;">
                                <!--<span class="input-group-btn">
                                    <button type="button" class="btn btn-success" >
                                        <i class="fa fa-money"></i>&nbsp;&nbsp; IDR</button>                                
                                </span>-->
                            </div>
                       	</td>
                    </tr>
                </tfoot>
                </table>
                </div>
        	</div>
        </div>
    </div>
    <br />
    <div class="row">
    	<div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
		</div>
    </div>    
	</form>	
</section>
<!-- /.content -->

<?php //LAYOUT HELPER: MODAL
	echo sw_createModal('modal_po','Select PO','dialog_purchase_order_push_data();','purchase/dialog_purchase_order');
	echo sw_createModal('modal_vendor','Select Vendor','dialog_vendor_push_data();','business/dialog_vendor');
?>
<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
var source_po 		= <?php echo json_encode($dialog_po);?>;
var source_vendor 	= <?php echo json_encode($dialog_vendor);?>;

var return_url = "<?php echo site_url('purchase/purchase_receipt');?>";
var submit_url = "<?php echo site_url('purchase/purchase_receipt_add_query');?>";

var ajax_url_1 = "<?php echo base_url('purchase/ajax_getAll_po_with_vendor');?>";
var ajax_url_2 = "<?php echo base_url('purchase/ajax_getDetail_po_open');?>";
var ajax_url_3 = "<?php echo base_url('purchase/ajax_getHeader_po');?>";
var url_1 = "<?php echo site_url('inventory/detail');?>";

var param = "<?php echo $param;?>";

$(document).ready(function(){	
	$("#location_id").on('change',function(){
		var result;
		for( var i = 0; i < source_location.length; i++ ) {
			if( source_location[i]['location_id'] == $(this).val() ) {
				result = source_location[i];
				break;
			}
		}
		$("#ship_addr").val(result['location_address']+', '+result['location_city']+', '+result['location_state']+', '+result['location_country']);
	});
	$("#location_id").val(1).change();
});

</script>
<script type="text/javascript">
function get_po_detail(){
	return $.ajax({
		url:ajax_url_2,
		data:{doc_num:$("#doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_insert(data);			
        }
	});
}
</script>
