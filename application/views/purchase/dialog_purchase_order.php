<?php
/*echo '<link href="'.base_url('assets/css/bootstrap.min.css').'" rel="stylesheet" media="screen">';
echo '<link href="'.base_url('assets/css/font-awesome.min.css').'" rel="stylesheet" type="text/css">';
echo '<link href="'.base_url('assets/css/ionicons.min.css').'" rel="stylesheet" type="text/css">';
echo '<link href="'.base_url('assets/css/AdminLTE.css').'" rel="stylesheet" type="text/css">';*/
?>
<style>
	/*.selected td{background-color:#48507b;color:#fff}
	tbody{		
		-moz-user-select: -moz-none;
		-khtml-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}*/
</style><?php //echo sw_createBox("Choose Document:");?>
<?php $column = array('','Asset Code','Asset Name');?>
<div class="row">
	<div class="col-sm-12">
    	<table class="table table-condensed" id="dialog_purchase_order_list">
        <thead>
            <tr>
                <td width="5%"><input type="checkbox" name="purchase_order_chk_all" id="purchase_order_chk_all" />&nbsp;&nbsp;</td>
                <td width="15%">Doc Num</td>
                <td width="35%">Vendor</td>
                <td width="30%">Create Date</td>
                <td width="15%">Warehouse</td>
                <td>Status</td>
            </tr>
        </thead>
        <tbody>
                    
        </tbody>
        </table>
    </div>
</div>

<?php 
echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>

<script type="text/javascript">
/*---
1.reset_table(table)
2.populate_table(table)
3.dialog_purchase_order_push_data
---*/
//if dialog type = table, default row is required
var data_po = '';
//var data_po = source_po;
var po_default_row = '<tr>
	<td><input type="checkbox" name="chk[]" id="chk"/></td>
	<td><input type="hidden" name="doc_id[]" id="doc_id" /><span></span></td>
	<td></td><td></td><td></td><td></td></tr>';
function dialog_purchase_order_reset_table(table){
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(po_default_row);
}
function dialog_purchase_order_populate_data(table,data){ 
	data_po = data;
	if(data_po){
		//delete first row
		if($('#'+table+' > tbody > tr').length != 1){
			$('#'+table+' > tbody > tr:last').remove();
		}
		for(var p = 0; p<data_po.length; p++){
			var row = clone_row(table);
			row.find('td:eq(1)').find('input[type=hidden]').val(data_po[p]['doc_id']);
			row.find('td:eq(1)').find('span').text(data_po[p]['doc_num']);
			row.find('td:eq(2)').text(data_po[p]['buss_name']);
			row.find('td:eq(3)').text(data_po[p]['doc_dt']);
			row.find('td:eq(4)').text(data_po[p]['location_name']);
			//row.find('td:eq(5)').html(status_label('logistic',data_po[p]['logistic_status']));
			row.find('td:eq(5)').html(data_po[p]['document_status']);
		}
		var row = clone_row(table);	
		//delete first and last after insert
		$('#'+table+' > tbody > tr:first').remove();
		$('#'+table+' > tbody > tr:last').remove();
	}else{
		console.log('error');	
	}
}
function dialog_purchase_order_push_data(){
	var result;
	//var selected = $('input[name="doc_id[]"]').val();
	var selected = $('#dialog_purchase_order_list > tbody > tr').find('td:eq(0) > input[type="checkbox"]:checked').parent().parent().find('td:eq(1) > input[type="hidden"]').val();
	for( var i = 0; i < data_po.length; i++ ) {
		if( data_po[i]['doc_id'] === selected ) { 
			result = data_po[i];
			//break;
		}
	}
	dialog_purchase_order_pull_data(result);
}

$(document).ready(function(){		
	$(document).on('click','#dialog_purchase_order_list tbody > tr',function(event){
		//SINGLE SELECTION
		if(event.target.type !== 'checkbox'){
			$(':checkbox',this).trigger('click');
		}else{
			if($(this).hasClass('selected')){
				$(this).removeClass('selected');
			}else{
				$(this).siblings().removeClass('selected');
				$(this).siblings().find('td > input:checkbox').prop("checked",false);
				$(this).addClass('selected');
			}
		}		
	});
	
	/*var click = new Date();
	var lastClick = new Date();
	$("#docslist").on("click", function (event) {
		click = new Date();
		if(click - lastClick < 400){
			$("#btn_ok").click();			
		//}else{				
		}
		lastClick = new Date();		
	});
	$("#docslist").on('dblclick',function(){
		//alert('dbl clik');
	});*/
	
});
</script>