<?php
echo '<link href="'.base_url('assets/css/bootstrap.min.css').'" rel="stylesheet" media="screen">';
echo '<link href="'.base_url('assets/css/font-awesome.min.css').'" rel="stylesheet" type="text/css">';
echo '<link href="'.base_url('assets/css/ionicons.min.css').'" rel="stylesheet" type="text/css">';
echo '<link href="'.base_url('assets/css/AdminLTE.css').'" rel="stylesheet" type="text/css">';
?>
<style>
	.selected td{background-color:#48507b;color:#fff}
</style>

<?php //echo sw_createBox("Choose Asset:");?>

<?php $column = array('','Asset Code','Asset Name');?>
<!---<div class="row">
	<div class="col-sm-12">
		<button class="btn" id="btn_ok" onClick="push_data();">Select</button>
	</div>
</div>--->
<div class="row">
	<div class="col-sm-12">
        <table class="table table-condensed" id="assetlist">
        <thead>
            <tr>
                <td>&nbsp;</td>
                <td>Asset Tracking ID</td>
                <td>Asset Name</td>
                <td>Current Location</td>
            </tr>
        </thead>
        <tbody>
            <!---<div class="form-group">--->
            <?php for ($x=0;$x<count($assets);$x++){ ?>
            <tr>
                <td>
                <!---<div class="checkbox">
                	<label class>
                    <div class="icheckbox_minimal">
                    <input type="checkbox" name="chk[]" id="<?php echo $assets[$x]['asset_id'];?>" value="<?php echo $assets[$x]['asset_id'];?>" disabled/>
					</div>
					</label>
				</div>--->
                	<input type="checkbox" name="chk[]" id="<?php echo $assets[$x]['asset_id'];?>" value="<?php echo $assets[$x]['asset_id'];?>" />
                </td>
                <td><?php echo $assets[$x]['asset_label'];?></td>
                <td><?php echo $assets[$x]['asset_name'];?></td>
                <td><?php echo $assets[$x]['location_name'];?></td>
            </tr>
            <?php } ?>
            <!---</div>--->
        </tbody>
        </table>
    </div>
</div>
<!---</div>--->
<?php 
echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/bootstrap.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/admin_app.js').'"></script>';
?>
<script type="text/javascript">
var selected = [];
function push_data(){
	window.opener.pull_data(selected);
}
function addSelected(value){
	selected.push(value);
	console.log(selected);
}
function delSelected(value){
	for (var i = 0; i < selected.length; i++) {
        if (selected[i] === value) {
            selected.splice(i, 1);
            i--;
        }
    }
	console.log(selected);
}

$(document).ready(function(){	
	//if use iCheck	
	/*$('#assetlist tbody > tr')
		.filter(':has(:checkbox:checked)')
		.addClass('selected')
		.end()
		.click(function(event) {
		$(this).toggleClass('selected');
		if (event.target.type !== 'checkbox') {
			if($(':checkbox',this).parent().hasClass('checked')){
				$(':checkbox',this).parent().removeClass('checked');
				delSelected($(':checkbox',this).val());
			}else{
				$(':checkbox',this).parent().addClass('checked');
				addSelected($(':checkbox',this).val());
			}
		}
	});	*/
	
	$('#assetlist tbody > tr').click(function(event){
		if(event.target.type !== 'checkbox'){			
			$(':checkbox',this).trigger('click');
		}else{
			$(this).toggleClass('selected');
		}
	});
});
</script>