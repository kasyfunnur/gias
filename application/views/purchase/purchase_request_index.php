<section class="content-header">
  	<h1> Purchase Request <a href="<?php echo site_url('purchase/purchase_request_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<span class="hidden-xs">
        		<i class="fa fa-plus"></i>&nbsp;&nbsp;New Request
            </span>
        	<span class="visible-xs">
        		<i class="fa fa-plus"></i>
            </span>
        </button>
    </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg:"";?>
<!-- Main content -->
<section class="content">
<div class="box box-info">
    <div class="box-body table-responsive">
  	<table class="table table-hover table-condensed" id="purchase_request_index">
    	<thead>
            <tr class="filter">
                <th style="width:25%;text-align:left">
                    <!--Showing <?php //echo count($datatable);?> Docs (Total <?php //echo count($datatableraw);?>)-->
                    Showing #(Total <?php //echo count($datatableraw);?>)
                </th>
                <th style="width:35%;text-align:left">                  
                    <!--Filter : <select name="sel_year"><option value="2015">2015</option><option value="2014">2014</option></select>  -->
                </th>
                <th style="width:40%;text-align:left">
                    
                </th>
            </tr>
        </thead>
        <thead>
            <tr>                
                <th style="width:25%;text-align:left">
                    <div class="input-group input-group-sm">
                        <span class="input-group-btn">                            
                            <button type="submit" class="btn btn-info" name="btn_filter" value="" />ALL</button>
                            <button type="submit" class="btn btn-warning" name="btn_filter" value="ACTIVE"/>OPEN</button>
                            <button type="submit" class="btn btn-success" name="btn_filter" value="CLOSED"/>CLOSED</button>
                            <button type="submit" class="btn btn-danger" name="btn_filter" value="CANCELLED"/>CANCELLED</button>
                        </span>
                    </div>
                </th>
                <th style="width:35%;text-align:left">
                    <div class="input-group input-group-sm">
                        <?php echo sw_textButton('search_field','Search Here..','search_button','Search','search');?>                                            </div>
                </th>
                <th style="width:40%;text-align:left">                    
                    <?php 
                    $config['base_url'] = $this->uri->config->config['base_url']."sales/sales_order";
                    $config['total_rows'] = count($datatableraw);
                    $config['per_page'] = 10;
                    $config['num_tag_open'] = '<li>';
                    $config['num_tag_close'] = '</li>';
                    $config['cur_tag_open'] = '<li class="active"><a>';
                    $config['cur_tag_close'] = '</a></li>';
                    $config['prev_tag_open'] = '<li>';
                    $config['prev_tag_close'] = '</li>';
                    $config['next_tag_open'] = '<li>';
                    $config['next_tag_close'] = '</li>';
                    $config['last_tag_open'] = '<li>';
                    $config['last_tag_close'] = '</li>';
                    $config['first_tag_open'] = '<li>';
                    $config['first_tag_close'] = '</li>';                    
                    $this->pagination->initialize($config); 
                    ?>
                    <div class="input-group input-group-sm">                        
                        <ul class="pagination">
                            <?php echo $this->pagination->create_links();?>                            
                        </ul>
                    </div>                    
                </th>
            </tr>
        </thead>
        <tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>
			<?php 
				//$approval_status = $this->status_model->status_label('approval',$datatable[$x]['approval_status']);
                $approval_status = 'approved'; // ind, temporary fix approval status
                $document_status = $this->status_model->status_label('document',$datatable[$x]['doc_status']);
				//$logistic_status = $this->status_model->status_label('logistic',$datatable[$x]['logistic_status']);
				//$billing_status = $this->status_model->status_label('billing',$datatable[$x]['billing_status']);
				//$finance_status = $this->status_model->status_label('financial',$datatable[$x]['financial_status']);
				//$dp_status = $this->status_model->status_label('financial',"DP");
            ?>
            <tr>
                <td>
                    <span class="searchable" style="font-size:16px;font-weight:lighter !important;color:#888">
                        <a href="<?php echo site_url('purchase/purchase_request_edit');?>/<?php echo $datatable[$x]['doc_num'];?>">
                            <?php echo $datatable[$x]['doc_num'];?>
                        </a> 
                        <!-- - --> <?php //echo $datatable[$x]['buss_name'];?> <?php //echo $document_status;?>
                    </span>
                    
                    <br />
                    <span style="font-size:12px;font-weight:lighter !important;color:#888">
                        By <?php echo $datatable[$x]['create_by'];?>
                        - <?php echo date("d M, Y", strtotime($datatable[$x]['doc_dt']));?>                        
                        <!--<button class="btn btn-info btn-xs comment" name="<?php echo $datatable[$x]['doc_num'];?>">
	                        <i class="fa fa-comments"></i>
                        </button>-->
                    </span>
                    <span class="pull-right">
                        <!-- <strong>[<?php //echo $datatable[$x]['doc_status'];?>]</strong> -->
                        <strong><?php echo $document_status;?></strong>
                    </span>
                </td>
                <?php 					
					$disable = 'disabled';
					$link_receipt = base_url('purchase/purchase_receipt_add')."/".$datatable[$x]['doc_num'];
					$link_cancel = base_url('purchase/purchase_order_cancel')."/".$datatable[$x]['doc_num'];
					$link_close = base_url('purchase/purchase_order_close')."/".$datatable[$x]['doc_num'];
					$link_print = base_url('purchase/print_po')."/".$datatable[$x]['doc_num'];
					$link_dp = base_url('purchase/purchase_invoice_dp_add')."/".$datatable[$x]['doc_num'];
					/*if($datatable[$x]['logistic_status'] != "RECEIVED" 
						&& $datatable[$x]['logistic_status'] != "PARTIAL" 
						&& $datatable[$x]['doc_status'] != "CANCELLED"
						&& $datatable[$x]['doc_status'] != "CLOSED"){
						$disable = '';
					} 					*/
				?>
                <td>
                	[Due : <?php echo date("d M, Y", strtotime($datatable[$x]['doc_ddt']));?>]
                    <?php //echo $logistic_status;?> <?php //if($datatable[$x]['doc_paid'] > 0) echo $dp_status;?> 
                	<br />
                	<span class="searchable"><?php echo $datatable[$x]['doc_note'];?></span>
                </td>              
                <td> 
                	<a href="<?php echo $link_cancel;?>" class="btn btn-sm btn-danger <?php echo $disable;?>">
                        <i class="fa fa-ban"></i> Cancel</a>&nbsp;&nbsp;
					<a href="<?php echo $link_close;?>" class="btn btn-sm btn-warning <?php echo $disable;?>">
                        <i class="fa fa-archive"></i> Close</a>&nbsp;&nbsp;
                    <a href="<?php echo $link_print;?>" class="btn btn-sm btn-success <?php echo $disable;?>" target="_blank">
                        <i class="fa fa-print"></i> Print</a>&nbsp;&nbsp;
					<a href="<?php echo $link_receipt;?>" class="btn btn-sm btn-info <?php echo $disable;?>" target="_blank">
                        <i class="fa fa-plus"></i> Receipt</a>&nbsp;&nbsp;
					<a href="<?php echo $link_dp;?>" class="btn btn-sm btn-info <?php echo $disable;?>" target="_blank">
                        <i class="fa fa-dollar"></i> DP</a>&nbsp;&nbsp;
                </td>
            </tr>
            <?php }?>
        </tbody>
    </table></div>
    </div>
</section>
<!-- /.content -->

<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>

<script>
$(document).ready(function(){
	$(".pagination").click(function(e){
        $("input[name='btn_filter']").val('ACTIVE');
    });
});
</script>