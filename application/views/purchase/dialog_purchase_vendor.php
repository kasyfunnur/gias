<?php
echo '<link href="'.base_url('assets/css/bootstrap.min.css').'" rel="stylesheet" media="screen">';
echo '<link href="'.base_url('assets/css/font-awesome.min.css').'" rel="stylesheet" type="text/css">';
echo '<link href="'.base_url('assets/css/ionicons.min.css').'" rel="stylesheet" type="text/css">';
echo '<link href="'.base_url('assets/css/AdminLTE.css').'" rel="stylesheet" type="text/css">';
?>
<style>
	.selected td{background-color:#48507b;color:#fff}
</style>

<?php echo sw_createBox("Choose Vendor:");?>

<?php $column = array('','Asset Code','Asset Name');?>
<div class="row">
	<div class="col-sm-12">
		<button class="btn" id="btn_ok" onClick="push_data();">Select</button>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
        <table class="table table-condensed" id="vendorlist">
        <thead>
            <tr>
                <td>&nbsp;</td>
                <td>Vendor Code</td>
                <td>Name</td>
            </tr>
        </thead>
        <tbody>
            <!---<div class="form-group">--->
            <?php for ($x=0;$x<count($vendors);$x++){ ?>
            <tr>
                <td>
                    <input type="checkbox" name="chk[]" id="<?php echo $vendors[$x]['buss_id'];?>" value="<?php echo $vendors[$x]['buss_id'];?>" disabled/>
                </td>
                <td><?php echo $vendors[$x]['buss_code'];?></td>
                <td><?php echo $vendors[$x]['buss_name'];?></td>
            </tr>
            <?php } ?>
            <!---</div>--->
        </tbody>
        </table>
    </div>
</div>
</div>
<?php 
echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/bootstrap.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/admin_app.js').'"></script>';
?>
<script type="text/javascript">
var source = <?php echo json_encode($vendors);?>;

var selected = [];
function push_data(){
	for( var i = 0; i < source.length; i++ ) {
		if( source[i]['buss_id'] === selected ) {
			result = source[i];
			break;
		}
	}
	window.opener.pull_data_vendor(result);
	window.close();
}
function setSelected(value){
	selected = value;
}
function unsetSelected(){
	selected = [];
}
function addSelected(value){
	selected.push(value);
}
function delSelected(value){
	for (var i = 0; i < selected.length; i++) {
        if (selected[i] === value) {
            selected.splice(i, 1);
            i--;
        }
    }
}

$(document).ready(function(){	
	var click = new Date();
	var lastClick = new Date();
	$("#vendorlist").on("click", function (event) {
		click = new Date();
		if(click - lastClick < 400){
			$("#btn_ok").click();			
		//}else{				
		}
		lastClick = new Date();		
	});
	$("#vendorlist").on('dblclick',function(){
		//alert('dbl clik');
	});
	$('#vendorlist tbody > tr')		
		.filter(':has(:checkbox)')
		.end()
		.click(function(event) {
		if (event.target.type !== 'checkbox') {
			if($(':checkbox',this).parent().hasClass('checked')){
				$(':checkbox',this).parent().removeClass('checked');
				$(this).removeClass('selected');
				//delSelected($(':checkbox',this).val());	
				unsetSelected();				
			}else{						
				$('input').iCheck('check');
				$('input').iCheck('uncheck');
				$(this).siblings().removeClass('selected');
				$(':checkbox',this).parent().addClass('checked');
				$(this).addClass('selected');
				//addSelected($(':checkbox',this).val());
				setSelected($(':checkbox',this).val());
			}
		}
	});
});
</script>