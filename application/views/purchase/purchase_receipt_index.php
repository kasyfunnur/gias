<section class="content-header">
  	<h1> Purchase Receipt <a href="<?php echo site_url('purchase/purchase_receipt_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<span class="hidden-xs">
        		<i class="fa fa-plus"></i>&nbsp;&nbsp;New Receipt
            </span>
        	<span class="visible-xs">
        		<i class="fa fa-plus"></i>
            </span>
		</button>
    </a>
  	</h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<?php echo form_open('', config_form('#'));?>
<section class="content">   
<div class="box box-info">    
    <div class="box-body table-responsive">
  	<table class="table table-hover table-condensed" id="purchase_receipt_index">
    	<thead>
            <tr class="filter">
                <th style="width:25%;text-align:left">
                    Total = <?php echo $rows;?> document(s).
                </th>
                <th style="width:35%;text-align:left">                  
                    <!--Filter : <select name="sel_year"><option value="2015">2015</option><option value="2014">2014</option></select>  -->
                </th>
                <th style="width:40%;text-align:left">
                    
                </th>
            </tr>
            <tr>                
                <th style="width:25%;text-align:left">
                    <div class="input-group input-group-sm">
                        <span class="input-group-btn">                            
                            <button type="submit" class="btn btn-info" name="btn_filter" value="" />ALL</button>
                            <button type="submit" class="btn btn-warning" name="btn_filter" value="ACTIVE"/>ACTIVE</button>
                            <button type="submit" class="btn btn-danger" name="btn_filter" value="CANCELLED"/>CANCELLED</button>
                            <button type="submit" class="btn btn-success" name="btn_filter" value="CLOSED"/>CLOSED</button>
                        </span>
                    </div>
                </th>
                <th style="width:35%;text-align:left">
                    <div class="input-group input-group-sm">
                        <?php echo sw_textButton('search_field','Search Here..','search_button','Search','search');?>                                            </div>
                </th>
                <th style="width:40%;text-align:left">                    
                    <?php 
                    $page_config['base_url'] = $this->uri->config->config['base_url']."purchase/purchase_receipt";
                    $page_config['total_rows'] = $rows;
                    $page_config = array_merge($page_config,$this->config->item('pagination','pagination'));
                    $this->pagination->initialize($page_config); 
                    ?>
                    <div class="input-group input-group-sm">                        
                        <ul class="pagination">
                            <?php echo $this->pagination->create_links();?>                            
                        </ul>
                    </div>                    
                </th>
            </tr>
        	<tr class="hide">
            	<th style="width:25%">Document</th>
                <th style="width:35%">Note</th>
                <th style="width:40%">Actions</th>
            </tr>
        </thead>
        <tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>
            <?php 
				$document_status = $this->status_model->status_label('document',$datatable[$x]['doc_status']);
				
				if($datatable[$x]['doc_status'] == "CANCELLED"){
					$color = '#F00';
				}elseif($datatable[$x]['doc_status'] == "CLOSED"){
					$color = '#F90';
				}else{
					$color = '#09F';
				}
                $status = doc_status($datatable[$x]['doc_status']);
            ?>
            <tr>            	
                <td>
                	<span class="searchable" style="font-size:16px;font-weight:lighter !important;color:#888">
                        <a href="<?php echo site_url('purchase/purchase_receipt_edit');?>/<?php echo $datatable[$x]['doc_num'];?>">
                            <?php echo $datatable[$x]['doc_num'];?>
                        </a> 
                        - <?php echo $datatable[$x]['buss_name'];?>
                    </span>
                    <br />
                    <span style="font-size:12px;font-weight:lighter !important;color:#888">
                        By <?php echo $datatable[$x]['create_by'];?>
                        - <?php echo date("d M, Y", strtotime($datatable[$x]['doc_dt']));?>
                    </span>
                    <span class="pull-right">                        
                        <strong>
                            <span class="label label-xs label-<?php echo $status['color'];?>" style="margin:2px">
                            <i class="fa"></i> <?php echo $status['status'];?></span>
                        </strong>
                    </span>
                </td>				
                <?php                   
                    $disable_cancel = '';
                    $disable_close = '';
                    $disable_print = '';
                    $disable_invoice = '';
                    switch($datatable[$x]['doc_status']){
                        case "CANCELLED":
                            $disable_cancel = 'disabled';$disable_close = 'disabled';
                            $disable_print = 'disabled';$disable_invoice = 'disabled';
                            break;
                        case "CLOSED":
                            $disable_cancel = 'disabled';$disable_close = 'disabled';
                            $disable_print = '';$disable_invoice = 'disabled';
                            break;
                        case "APPROVED":
                            $disable_cancel = '';$disable_close = '';
                            $disable_print = '';$disable_invoice = '';
                            break;
                        case "DRAFT":
                            $disable_cancel = '';$disable_close = 'disabled';
                            $disable_print = 'disabled';$disable_invoice = 'disabled';
                            break;
                        default:
                            $disable_cancel = '';$disable_close = '';
                            $disable_print = '';$disable_invoice = '';
                    } 
                     
                ?>
                <td>
					[Due : <?php echo date("d M, Y", strtotime($datatable[$x]['doc_ddt']));?>]
                    <?php //echo $logistic_status;?> <?php //if($datatable[$x]['doc_paid'] > 0) echo $dp_status;?> 
                    <br />
                    <span class="searchable"><?php echo $datatable[$x]['doc_note'];?></span>
                </td>
                <td>
                    <?php
                        $link_invoice = base_url('purchase/purchase_invoice_add')."/".$datatable[$x]['doc_num'];
                        $link_cancel = base_url('purchase/purchase_receipt_cancel')."/".$datatable[$x]['doc_num'];
                        $link_close = base_url('purchase/purchase_receipt_close')."/".$datatable[$x]['doc_num'];
                        $link_print = base_url('purchase/purchase_receipt_print')."/".$datatable[$x]['doc_num'];                    
                    ?>
                    <a href="<?php echo $link_cancel;?>" class="btn btn-sm btn-danger <?php echo $disable_cancel;?>">
                        <i class="fa fa-ban"></i> Cancel</a>&nbsp;&nbsp;
					<a href="<?php echo $link_close;?>" class="btn btn-sm btn-warning <?php echo $disable_close;?>">
                        <i class="fa fa-archive"></i> Close</a>&nbsp;&nbsp;
                    <a href="<?php echo $link_print;?>" class="btn btn-sm btn-success <?php echo $disable_print;?>" target="_blank">
                        <i class="fa fa-print"></i> Print</a>&nbsp;&nbsp;
                    <a href="<?php echo $link_invoice;?>" class="btn btn-sm btn-info <?php echo $disable_invoice;?>" target="_blank">
                        <i class="fa fa-plus"></i> Invoice</a>                
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
	</div>
    </div>
</section>
</form>
<!-- /.content -->

<?php 
    echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>

<script>
$(document).ready(function(){
    /*$(".pagination").click(function(e){
        $("input[name='btn_filter']").val('ACTIVE');
    });*/
});
</script>