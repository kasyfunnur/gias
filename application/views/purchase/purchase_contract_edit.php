<!-- Content Header (Page header) -->
<?php 
$list_action = array(
    array('link'=>base_url('/purchase/purchase_contract_cancel')."/".$form_header[0]['doc_num'],'icon'=>'ban','text'=>'Cancel'),
    array('link'=>base_url('/purchase/purchase_contract_close')."/".$form_header[0]['doc_num'],'icon'=>'minus-circle','text'=>'Close')
);
$list_download = array(
    array('link'=>base_url('/purchase/purchase_contract_print')."/".$form_header[0]['doc_num'],'icon'=>'file-excel-o','text'=>'Excel'),
    array('link'=>base_url('/purchase/purchase_contract_print')."/".$form_header[0]['doc_num'],'icon'=>'file-pdf-o','text'=>'PDF'),
    array('link'=>base_url('/purchase/purchase_contract_print')."/".$form_header[0]['doc_num'],'icon'=>'print','text'=>'PRINT')
);?>
<?php echo validation_errors(); ?>
<?php $attributes = array(
    'class'     => 'form-horizontal',
    'role'      => 'form',
    'method'    => 'post', 
    'name'      => 'frm', 
    'id'        => 'frm',
    'onSubmit'  => 'return validateForm();'
    );
echo form_open('', $attributes);
echo form_text('','hidden','doc_id',$form_header[0]['doc_id']);
echo form_text('','hidden','save_confirm','save');
?>  

<section class="content-header">
    <h1><a class="" href="<?php echo site_url('purchase/purchase_contract');?>">Purchase Contract</a> 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Edit 
        &nbsp;&nbsp;&nbsp;
        <div class="pull-right button_action" id="button_action"></div>
    </h1>
</section>

<!-- Main content -->
<section class="content">   
    <div class="row">
        <div class="col-sm-6">          
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="buss_name">
                    Vendor                    
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="buss_id" name="buss_id" value="<?php echo $form_header[0]['buss_id'];?>">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="<?php echo $form_header[0]['buss_name'];?>" >
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_vendor" style="width:100%;">
                                <i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
                        </span>
                    </div>                    
                </div>
            </div>

            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="contact_name">Contact Person</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="contact_name" name="contact_name" value="<?php echo $form_header[0]['contact_name']; ?>">
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="contact_phone">Contact Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="contact_phone" name="contact_phone" value="<?php echo $form_header[0]['contact_phone']; ?>">
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="contact_email">Contact Email</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="contact_email" name="contact_email" value="<?php echo $form_header[0]['contact_email']; ?>">
                </div>
            </div>

        </div>
        <div class="col-sm-6">
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $form_header[0]['doc_num'];?>" readonly>
                </div>
            </div>  
            <div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="start_dt">Start Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="start_dt" name="start_dt" value="<?php echo substr($form_header[0]['start_dt'],0,10);?>">
                </div>
            </div>      
            <div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="end_dt">End Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="end_dt" name="end_dt" value="<?php echo substr($form_header[0]['end_dt'],0,10);?>">
                </div>
            </div>
            <div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="terminate_dt">Termination</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="terminate_dt" name="terminate_dt" value="<?php echo substr($form_header[0]['terminate_dt'],0,10);?>" readonly>
                </div>
            </div>      
            <div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="sign_dt">Signed on </label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="sign_dt" name="sign_dt" value="<?php echo substr($form_header[0]['sign_dt'],0,10);?>">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
                <li role="presentation"><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Detail</a></li>
                <li role="presentation"><a href="#document" aria-controls="document" role="tab" data-toggle="tab">Document</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="general"><br>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group col-sm-12">
                                <label class="col-sm-3" for="agreement_method">Type of Contract</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="agreement_method" id="agreement_method">
                                        <option value="GENERAL">General</option>
                                        <option value="SPECIFIC">Specific</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-3" for="contract_price">Ignore Price?</label>
                                <div class="col-sm-9">
                                    <input type="checkbox" name="contract_price" id="contract_price"> <label for="contract_price">Yes</label>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-3" for="doc_note">Document Memo</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"><?php echo $form_header[0]['doc_note'];?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group col-sm-12">
                                <label class="col-sm-3" for="doc_status">Status</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-sm" id="doc_status" name="doc_status" value="ACTIVE" readonly>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-3" for="payment_term">Payment Term</label>
                                <div class="col-sm-9">
                                    <!-- <select class="form-control" name="contract_type" id="contract_type">
                                        <option value="GENERAL">General</option>
                                        <option value="SPECIFIC">Specific</option>
                                    </select> -->
                                    <input type="text" class="form-control input-sm" id="payment_term" name="payment_term" value="" >
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-3" for="payment_method">Payment Method</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-sm" id="payment_method" name="payment_method" value="" >
                                    <!-- <select class="form-control" name="contract_type" id="contract_type">
                                        <option value="GENERAL">General</option>
                                        <option value="SPECIFIC">Specific</option>
                                    </select> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="detail"><br>
                    <div class="table-responsive" style="overflow:auto;">
                    <table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
                    <thead>
                    <tr>
                        <th style="width:5%">#</th>
                        <th style="width:15%">
                            <button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                                <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Item Code</span>
                                <span class="visible-xs"><i class="fa fa-search"></i></span>
                            </button>
                        </th>
                        <th style="width:15%">Item Name</th>
                        <th style="width:10%">Quantity</th>
                        <th style="width:20%">Price</th>
                        <th style="width:10%">UoM</th>
                        <th style="width:15%">Total</th>
                        <th style="width:10%" class="action">Action</th>
                    </tr>
                    </thead>            
                    <tbody>
                    <tr id="default">
                        <input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                        
                        <td style="text-align:center">
                            <input type="text" name="row[]" id="row1" class="form-control" value="1" readonly tabindex="-1"/>
                        </td>
                        <td>
                            <div class="input-group">                       
                                <span class="input-group-btn">
                                    <a href="#" class="btn btn-warning form-control" name="a_ast_code" target="_blank" tabindex="-1">
                                        <i class="fa fa-external-link"></i>
                                    </a>                            
                                </span>
                                <input type="text" name="ast_code[]" id="ast_code1" class="form-control " readonly="readonly" tabindex="-1">
                            </div>
                        </td>
                        <td>
                            <input type="text" name="ast_name[]" id="ast_name1" class="form-control">
                        </td>
                        <td>
                            <input type="number" name="ast_qty[]"  id="ast_qty1"  class="form-control" style="text-align:right">
                        </td>
                        <td>          
                            <input type="number" name="ast_price[]" id="ast_price1" class="form-control" style="text-align:right">
                        </td>
                        <td>
                            <input type="text" name="ast_uom[]" id="ast_uom1" class="form-control">
                        </td>
                        <td>
                            <input type="text" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right" readonly tabindex="-1">
                        </td>
                        <td>
                            <div class="input-group">
                                <div class='input-group-btn'>
                                    <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                                        <span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
                                        <span class="visible-xs"><i class="fa fa-ban"></i></span>
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                        <td></td>
                        <td><div>
                            <input type="text" class="form-control" id="doc_subtotal" name="doc_subtotal"
                                value="0" style="text-align:right;" readonly>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                    </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="document">
                    <div class="table-responsive" style="overflow:auto;">
                    <table class="table-bordered" id="table_detail_document" style="table-layout:fixed;white-space:nowrap;width:960px">
                    <thead>
                        <tr>
                            <th style="width:10%">#</th>
                            <th style="width:30%">Type</th>
                            <th style="width:30%">Doc Num</th>
                            <th style="width:30%">Total</th>
                        </tr>
                    </thead>            
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Purchase Order</td>
                            <td>PO-01243</td>
                            <td>257,000</td>
                        </tr>
                    </tbody>
                    </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right" id="confirm">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
            <button type="submit" class="btn btn-info pull-right" id="save">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Save Draft
            </button>
        </div>
    </div>
</section>
</form>

<template class="button_action">
    <a id="new" class="btn btn-info" target="_blank" href="<?php echo base_url('/purchase/purchase_contract_add/');?>">
        <i class="fa fa-plus"></i>&nbsp;&nbsp; New
    </a>

    <?php $disable_edit = ($form_header[0]['doc_status'] == 'DRAFT') ? '' : 'disabled';?>
    <!-- <button type="submit" class="btn btn-info" id="submit" <?php echo $disable_edit;?>>
        <i class="fa fa-check"></i>&nbsp;&nbsp; Update
    </button> -->

    <?php if($form_header[0]['doc_status'] == 'DRAFT'){ ?>        
        <button type="submit" class="btn btn-info" id="confirm">
            <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
        </button>
        <button type="submit" class="btn btn-info" id="save">
            <i class="fa fa-save"></i>&nbsp;&nbsp; Save Draft
        </button>
    <?php } ?>

    <?php if($form_header[0]['doc_status'] == 'AWAITING' || $form_header[0]['doc_status'] == 'IN REVIEW' ){ ?>
        <?php echo form_button_action('View','success',$list_download);?>
        <?php echo form_button_action('Action','success',$list_action);?>
    <?php } else ?>

    <?php if($form_header[0]['doc_status'] == 'ACTIVE'){ ?>
        <?php echo form_button_action('View','success',$list_download);?>
        <?php echo form_button_action('Action','success',$list_action);?>
        <!-- <a id="next" class="btn btn-info" href="<?php echo base_url('/purchase/purchase_receipt_add/')."/".$form_header[0]['doc_num'];?>">
            <i class="fa fa-sign-out"></i>&nbsp;&nbsp; Receipt
        </a> -->
    <?php } ?>
    
    <?php if($form_header[0]['doc_status'] == 'CLOSED'){ ?>
        <?php echo form_button_action('View','success',$list_download);?>
    <?php } ?>

    <?php $status = doc_status($form_header[0]['doc_status']);?>
    <div class="btn btn-<?php echo $status['color'];?>">
        Doc Status : <?php echo $status['status'];?>
    </div>
</template>

<?php //LAYOUT HELPER: MODAL
    echo sw_createModal('modal_vendor','Select Vendor','dialog_vendor_push_data();','business/dialog_vendor');
    echo sw_createModal('modal_item','Select Item','dialog_inventory_push_data();','inventory/dialog_inventory');
?>
<script src="<?php echo site_url('assets/js/bloodhound.js');?>"></script>
<script src="<?php echo site_url('assets/js/typeahead.jquery.js');?>"></script>
<script src="<?php echo site_url('assets/js/typeahead.bundle.js');?>"></script>

<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
var source_vendor = <?php echo json_encode($dialog_vendor);?>;
var source_inventory = <?php echo json_encode($dialog_inventory);?>;

<?php if(isset($form_header)){ ?>
var form_header = <?php echo json_encode($form_header);?>;
<?php }?>
<?php if(isset($form_detail)){ ?>
var form_detail = <?php echo json_encode($form_detail);?>;
<?php }?>

var return_url = "<?php echo site_url('purchase/purchase_contract');?>";
var submit_url = "<?php echo site_url('purchase/purchase_contract_edit_query');?>";
var ajax_url_1 = "<?php echo base_url('inventory/ajax_inventory_info');?>";
var url_1 = "<?php echo site_url('inventory/detail');?>";

function validateForm(){
    if(confirm("Are you sure???")==true){     
        $.post(
            submit_url,
            $('#frm').serialize(),
            function(response){                 
                if(response==1){
                    alert("Data updated successfully!");
                    window.location.href=return_url;
                }else{
                    console.log("ERROR: "+response);
                }
            }
        );
    }
    return false;
}
</script>
