<!-- Content Header (Page header) -->
<style>
	.inactive td{background-color:#CCC;opacity: 0.5;}
</style>

<section class="content-header">
  <h1> Purchase Request <small>Add </small> </h1>
</section>

<!-- Main content -->
<section class="content">
<?php
$attributes = array(
	'class' 	=> 'form-horizontal',
	'role'		=> 'form',
	'method' 	=> 'post', 
	'name'		=> 'frm', 
	'id' 		=> 'frm',
	'onSubmit'	=> 'return validateForm();'
	);
echo form_open('', $attributes);
?>
    <div class="row">
    	<div class="col-sm-6">
            <!---<div class="form-group">
                <label class="col-sm-3" for="buss_name">
                	Vendor                    
                </label>
                <div class="col-sm-9">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="">
                        <input type="hidden" id="buss_id" name="buss_id" value="">
                    	<span class="input-group-btn">
                        	<button type="button" class="btn" id="btn_search_vendor" 
                            	onclick="javascript:void window.showModalDialog('<?php echo site_url("purchase/dialog_purchase_vendor");?>','Select Vendor','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Search</button>
						</span>                        
                    </div>                    
                </div>
            </div>--->
            <!---<div class="form-group">
                <label class="col-sm-3" for="doc_ref">Doc Ref</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_ref" name="doc_ref" >
                </div>
            </div>--->            
            <!---<div class="form-group">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr" readonly></textarea>
                </div>
            </div>--->
            <div class="form-group">
                <label class="col-sm-3" for="request_for">Request For</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="request_for" name="request_for" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="request_dept">Request Dept</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="request_dept" name="request_dept" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="location_id">For Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>		
            <div class="form-group">
                <label class="col-sm-3" for="doc_ddt">DateRequired</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>            	
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-12">
        	<table class="table-bordered" id="table_detail">
            <thead>
            <tr>
            	<td width="5%">#</td>
                <td width="20%">
                	<!---<a href="#" class="btn btn-warning" role="button" onclick="javascript:void window.showModalDialog('<?php echo site_url("purchase/purchase_dialog_asset");?>','1403157670582','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Asset Code</a>--->
                    Group
                </td>
                <td width="35%">Asset Name</td>
                <td width="10%">Qty</td>
                <td width="15%">Price</td>
                <td width="15%">Total</td>
            </tr>
            </thead>
            <!---style="overflow:auto;height:400px;"--->
            <tbody>
            <tr height="30px">
            	<td>1</td>
                <td><!---<input type="text" name="ast_code[]" id="ast_code" class="form-control">--->
                	<?php echo sw_CreateSelect('ast_group1',$ddl_asset_group,'group_id','group_name',NULL,array('initialvalue'=>'-1','initialdisplay'=>'Select..'));?>
                </td>
                <td><input type="text" name="ast_name1" id="ast_name1" class="form-control"></td>
                <td><input type="number" name="ast_qty1"  id="ast_qty1"  class="form-control"></td>
                <td><input type="text" name="ast_price1"  id="ast_price1"  class="form-control"></td>
                <td><input type="text" name="sub_total1"  id="sub_total1"  class="form-control" readonly></td>
            </tr>
            </tbody>
            </table>
    	</div>
    </div>
    <br />
    <button type="submit" class="btn btn-info">Submit</button>
	</form>
	
</section>
<!-- /.content -->
<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
$(document).ready(function(){	
	
	$(document).on('change','.selection',function(){
		var tbl_cnt = $('#table_detail tr').length;
		var name_str = $(this).attr('name');	
		var name_len = name_str.length;
		var name_cnt = name_str.substring(name_len-1,name_len);		
		if(name_cnt==tbl_cnt-1){
			addRow("table_detail");
		}
	});

	/*$("#location_id").on('change',function(){
		var result;
		for( var i = 0; i < source_location.length; i++ ) {
			if( source_location[i]['whse_id'] == $(this).val() ) {
				result = source_location[i];
				break;
			}
		}
		$("#ship_addr").val(result['whse_address']+', '+result['whse_city']+', '+result['whse_state']+', '+result['whse_country']);
	});
	$("#location_id").val(1).change();*/
});

</script>
<script type="text/javascript">

function validateForm(){
	if(confirm("Are you sure?")==true){
		//FORM VALIDATION
		/*if($("#buss_code").val() == "" ){
			alert('Please input Vendor.');
			return false;
		}*/	
		var tbl_row = document.getElementById("table_detail").rows.length;
		$('<input>').attr({
			type: 'hidden',
			id: 'table_row',
			name: 'table_row',
			value: tbl_row
		}).appendTo('form');
		//sw_submit("frm","<?php //echo base_url($submit_path);?>","<?php //echo base_url($return_path,'refresh');?>","");
		$.post(
			"<?php echo site_url('purchase/purchase_request_add_query');?>",
			$('#frm').serialize(),
			function(response){	
				/*if(error_msg == ""){
					error_msg = response;
				}*/
				if(response==1){
					alert("Data updated successfully!");
					window.location.href="<?php echo site_url('purchase/purchase_request');?>";
				}else{
					alert("ERROR: "+response);
				}
			}
		);
	}
	return false;
}

function addRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);
	
	var cell0 = row.insertCell(0);
	cell0.innerHTML = rowCount;
	
	var cell1 = row.insertCell(1);	
	var buffer = '<?php echo sw_CreateSelect('ast_group',$ddl_asset_group,'group_id','group_name',NULL,array('initialvalue'=>'-1','initialdisplay'=>'Select..'));?>';
	var bufferopt = $(buffer);
	bufferopt.attr('name','ast_group'+rowCount);
	bufferopt.attr('id','ast_group'+rowCount);
	cell1.innerHTML = bufferopt.get(0).outerHTML;

	var cell2 = row.insertCell(2);
	cell2.innerHTML = '<input type="text" name="ast_name'+rowCount+'" id="ast_name'+rowCount+'" class="form-control">';
	
	var cell3 = row.insertCell(3);
	cell3.innerHTML = '<input type="number" name="ast_qty'+rowCount+'"  id="ast_qty'+rowCount+'"  class="form-control numeric" >';
	
	var cell4 = row.insertCell(4);
	cell4.innerHTML = '<input type="text" name="ast_price'+rowCount+'" id="ast_price'+rowCount+'" class="form-control">';
	
	var cell5 = row.insertCell(5);
	cell5.innerHTML = '<input type="text" name="sub_total'+rowCount+'" id="sub_total'+rowCount+'" class="form-control" readonly>';
}
function deleteRow(tableID) {
try {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;

	for(var i=0; i<rowCount; i++) {
		var row = table.rows[i];
		var chkbox = row.cells[0].childNodes[0];
		if(null != chkbox && true == chkbox.checked) {
			table.deleteRow(i);
			rowCount--;
			i--;
		}
	}
	}catch(e) {
		alert(e);
	}
}
function pull_data(value){
	//alert(value.length);
	console.log(value);
	for(var x=0;x<value.length;x++){		
		//addRow("table_detail");
	}
}
function pull_data_vendor(data_vendor){	
	/*$("#buss_name").val(data_vendor['buss_name']);
	$("#buss_id").val(data_vendor['buss_id']);
	$("#bill_addr").val(data_vendor['buss_addr']+' '+data_vendor['buss_city']+' '+data_vendor['buss_country']);	*/
}

</script>
