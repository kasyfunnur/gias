<!-- Content Header (Page header) -->
<?php //var_dump($arr_curr_rate); ?>
<?php echo validation_errors(); ?>
<?php $attributes = array(
    'class'     => 'form-horizontal',
    'role'      => 'form',
    'method'    => 'post', 
    'name'      => 'frm', 
    'id'        => 'frm',
    //'onSubmit'  => 'return validateForm();'
    'onSubmit'  => 'return validate_form();'
    );
//echo form_open('', $attributes);
echo form_open_multipart(site_url('purchase/purchase_quotation_add_query'), $attributes);
echo form_text('','hidden','save_confirm','save');
?>  

<section class="content-header">
    <h1><a class="" href="<?php echo site_url('purchase/purchase_quotation');?>">Purchase Quotation</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New 
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
    	<div class="col-sm-6">        	
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="buss_name">
                	Vendor                    
                </label>
                <div class="col-sm-9">
                	<input type="hidden" id="buss_id" name="buss_id" value="">
                    <input type="hidden" id="buss_char" name="buss_char" value="">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="" >
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_vendor" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                    
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr" readonly></textarea>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_note">Document Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                </div>
            </div>	
        	<div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>		
            <div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="doc_ddt">Date Required</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>

            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_curr">Curr</label>
                <div class="col-sm-5">
                    <?php echo sw_CreateSelect('doc_curr', $ddl_curr, 'curr_code', array('curr_code','curr_name'),'IDR');?>
                    <?php //echo sw_CreateSelect('doc_curr', $arr_curr_rate, 'value', array('curr_code','curr_name'),1);?>
                    <!-- <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly> -->
                </div>
                <div class="col-sm-4">
                    <input type="number" class="form-control input-sm" id="doc_rate" name="doc_rate" value="1" step="any">
                </div>
            </div>  
        </div>
    </div>    


    <div class="row">
        <div class="col-sm-12">
    <!-- TAB --> <!-- style="border:1px;" -->
    <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active col-sm-2  well no-padding">
            <a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Details</a>
        </li>
        <li role="presentation" class="col-sm-2 well no-padding">
            <a href="#info" aria-controls="info" role="tab" data-toggle="tab">Info</a>
        </li>
        <li role="presentation" class="col-sm-2 well no-padding">
            <a href="#attachment" aria-controls="attachment" role="tab" data-toggle="tab">Attachment</a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="detail">

            <div class="row">
            	<div class="col-sm-12">
                	<div class="table-responsive" style="overflow:auto;">
                	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
                    <thead>
                    <tr>
                    	<th style="width:3%">#</th>
                        <th style="width:12%">
                        	<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                                <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Item Code</span>
        						<span class="visible-xs"><i class="fa fa-search"></i></span>
                            </button>
                        </th>
                        <th style="width:12%">Item Name</th>
                        <th style="width:8%">Quantity</th>
                        <th style="width:10%">Price (<span class="label_curr">IDR</span>)</th>
                        <th style="width:8%">Disc %</th>
                        <th style="width:10%">Tax</th>
                        <th style="width:12%">Net Price</th>
                        <th style="width:15%">Total</th>
                        <th style="width:10%" class="action">Action</th>
                    </tr>
                    </thead>            
                    <tbody>
                    <tr id="default">
        	            <input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                        
                        <td style="text-align:center">
                        	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly tabindex="-1"/>
                            <input type="hidden" name="ast_price_temp[]" id="ast_price_temp1">                    
                            <input type="hidden" name="const_disc[]" id="const_disc1" value="100">
                            <input type="hidden" name="const_after_disc[]" id="const_after_disc1">
                            <input type="hidden" name="ast_disc_factor[]" id="ast_disc_factor1" >
                            <input type="hidden" name="ast_price_after_disc[]" id="ast_price_after_disc1" >
                            <input type="hidden" name="const_tax[]" id="const_tax1" value="100">
                            <input type="hidden" name="const_after_tax[]" id="const_after_tax1">
                            <input type="hidden" name="ast_tax_factor[]" id="ast_tax_factor1" >
                        </td>
                        <td>
                        	<div class="input-group">                    	
        	                    <span class="input-group-btn">
                                    <a href="#" class="btn btn-warning form-control" name="a_ast_code" target="_blank" tabindex="-1">
                                    	<i class="fa fa-external-link"></i>
                                    </a>                            
        						</span>
                                <input type="text" name="ast_code[]" id="ast_code1" class="form-control " readonly="readonly" tabindex="-1">
                            </div>
                        </td>
                        <td>
                        	<input type="text" name="ast_name[]" id="ast_name1" class="form-control">
                        </td>
                        <td>
                        	<input type="number" name="ast_qty[]"  id="ast_qty1"  class="form-control" style="text-align:right" step="any">
                        </td>
                        <td>          
                        	<input type="number" name="ast_price[]" id="ast_price1" class="form-control" style="text-align:right" step="any">
                        </td>
                        <td>
                            <input type="number" name="ast_disc[]" id="ast_disc1" class="form-control" style="text-align:right" step="any">
                        </td>
                        <td>
                            <!-- <input type="text" name="ast_tax[]"  id="ast_tax1"  class="form-control" style="text-align:right"> -->
                            <?php echo sw_CreateDropdown(array('ast_tax[]','ast_tax1'),$ddl_tax,'tax_percent','tax_name');?>
                        </td>
                        <td>
                            <input type="number" name="ast_netprice[]"  id="ast_netprice1"  class="form-control" style="text-align:right" readonly tabindex="-1" step="any">
                        </td>
                        <td>
                        	<input type="text" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right" readonly tabindex="-1">
                        </td>
                        <td>
                        	<div class="input-group">
        						<div class='input-group-btn'>
                                    <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                                    	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
        	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                                    </button>
        						</div>
        					</div>
        				</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><div>
                            <input type="number" class="form-control" id="qty_total" name="qty_total" 
                            	value="0" style="text-align:right;" readonly>
                            </div>
                        </td>
                        <td></td>
                        <td></td>
                        <td id="foot_label"><strong>Sub Total:&nbsp;&nbsp;&nbsp;</strong></td>
                        <td><span class="label_curr">IDR</span></td>
                        <td><div>
                            <input type="text" class="form-control" id="doc_subtotal" name="doc_subtotal" 
                            	value="0" style="text-align:right;" readonly>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Disc:&nbsp;&nbsp;&nbsp;</strong></td>
                        <td id="foot_label" style="text-align:right;">
                        	<input type="number" class="col-sm-8" id="doc_disc_pct" name="doc_disc_pct"
                            	value="0" style="text-align:right;" />%
                        	<!-- <strong>Disc:&nbsp;&nbsp;&nbsp;</strong> -->
                        </td>
                        <td><div>
                            <input type="text" class="form-control" id="doc_disc" name="doc_disc" 
                            	value="0" style="text-align:right;" readonly>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                        <td><span class="label_curr">IDR</span></td>
                        <td><div>
                            <input type="text" class="form-control" id="doc_total" name="doc_total" 
                            	value="0" style="text-align:right;" readonly>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                    </table>
                    </div>
            	</div>
            </div>
        </div>
    <div role="tabpanel" class="tab-pane" id="info">
    
    </div>
        <div role="tabpanel" class="tab-pane" id="attachment">
            <div class="row">
                <div class="col-sm-4">
                    <input type="file" name="upload_file" multiple class="filestyle" data-buttonText="Find file">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9">
                    <table class="table table-bordered table-hover" id="table_file_upload">
                    <thead>
                        <tr>
                            <th>Filename</th>
                            <th>Description</th>
                            <th>Upload Date</th>
                            <!-- <th>
                                <a href="javascript:add_file_upload();" class="btn btn-warning"><i class="fa fa-plus"></i></a>
                            </th> -->
                        </tr>
                    </thead>
                    <tbody> 
                        <tr id="default_upload">
                            <td>
                                <!-- <input type="file" name="uplaod_filename" multiple> -->
                            </td>
                            <td>
                                <!-- <input type="text" name="upload_filedesc[]" id="upload_filedesc[]" value=""> -->
                            </td>
                            <td>&nbsp;</td>                            
                        </tr>
                    </tbody>
                    </table>
                </div>
                <div class="col-sm-3">
                    <div class="row">
                        <!-- <div class="col-sm-12">
                            <input type="file" name="upload" multiple class="filestyle" data-buttonText="Find file">
                            <a href="#" class="btn btn-warning modal_caller" data-toggle="modal" data-target="#modal_upload">
                                <span style="text-decoration:underline"><i class="fa fa-cloud-upload"></i> Upload File</span>
                            </a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right" id="confirm">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
            <button type="submit" class="btn btn-info pull-right" id="save">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Save Draft
            </button>
        </div>
    </div>
</section>
</form>

<?php //LAYOUT HELPER: MODAL
	echo sw_createModal('modal_vendor','Select Vendor','dialog_vendor_push_data();','business/dialog_vendor');
	echo sw_createModal('modal_item','Select Item','dialog_inventory_push_data();','inventory/dialog_inventory');
?>
<script src="<?php echo site_url('assets/js/bloodhound.js');?>"></script>
<script src="<?php echo site_url('assets/js/typeahead.jquery.js');?>"></script>
<script src="<?php echo site_url('assets/js/typeahead.bundle.js');?>"></script>

<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
var source_vendor = <?php echo json_encode($dialog_vendor);?>;
var source_inventory = <?php echo json_encode($dialog_inventory);?>;

<?php if(isset($form_header)){ ?>
var form_header = <?php echo json_encode($form_header);?>;
<?php }?>
<?php if(isset($form_detail)){ ?>
var form_detail = <?php echo json_encode($form_detail);?>;
<?php }?>

var return_url = "<?php echo site_url('purchase/purchase_quotation');?>";
var submit_url = "<?php echo site_url('purchase/purchase_quotation_add_query');?>";
var ajax_url_1 = "<?php echo base_url('inventory/ajax_inventory_info');?>";
var ajax_url_2 = "<?php echo base_url('currency/ajax_get_rate');?>";
var ajax_url_3 = "<?php echo base_url('purchase/ajax_getAll_pct_with_vendor');?>";
var ajax_url_4 = "<?php echo base_url('purchase/ajax_getDetail_pct_open');?>";
var url_1 = "<?php echo site_url('inventory/detail');?>";

function validateForm(){
    if(confirm("Are you sure???")==true){     
        $.post(
            submit_url,
            $('#frm').serialize(),
            function(response){                 
                if(response==1){
                    alert("Data updated successfully!");
                    window.location.href=return_url;
                }else{
                    console.log("ERROR: "+response);
                }
            }
        );
    }
    return false;
}
function get_pct_detail(){
    return $.ajax({
        url:ajax_url_4,
        data:{doc_num:$("#contract_num").val()},
        success: function(response) {
            data = JSON.parse(response);
            table_detail_insert(data);          
        }
    });
}
function validate_form(){
    if(confirm("Are you sure?? Purchase Add??")==true){
        return true;
    }
    return false;
}
</script>
