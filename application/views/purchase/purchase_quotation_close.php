<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('purchase/purchase_quotation');?>">Purchase Quotation</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        CLOSING... 
    </h1>   
</section>

<!-- Main content -->
<section class="content">
    <?php   echo form_open('', config_form());
            echo form_text('','hidden','doc_id',$form_header[0]['doc_id']);
    ?> 

    <div class="row">
    	<div class="col-sm-6">        	
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="buss_name">
                	Vendor                   
                </label>
                <div class="col-sm-9">
                	<?php echo $form_header[0]['buss_name'];?>                                  
                </div>
            </div>            
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <?php echo $form_header[0]['ship_addr'];?>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <?php echo $form_header[0]['doc_note'];?>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <?php echo $form_header[0]['doc_num'];?>
                </div>
            </div>	
        	<div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <?php echo substr($form_header[0]['doc_dt'],0,10);?>
                </div>
            </div>		
            <div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="doc_ddt">Date Req</label>
                <div class="col-sm-9">
                    <?php echo substr($form_header[0]['doc_ddt'],0,10);?>
                </div>
            </div>		                    
        </div>
    </div>
    <hr />
    
    <div class="row">
        <div class="col-sm-3">Reason for closing document:</div>
        <div class="col-sm-6">
            <textarea required name="reason" id="reason" class="col-sm-12"/></textarea>
        </div>
        <div class="col-sm-3">
            <button type="submit" class="btn btn-info btn-lg pull-left">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
        </div>
    </div>  
	</form>	
</section>
<!-- /.content -->

<?php 
	//echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script>
var return_url = "<?php echo site_url('purchase/purchase_quotation');?>";
var submit_url = "<?php echo site_url('purchase/purchase_quotation_close_query');?>/"+'<?php echo $form_header[0]['doc_num'];?>';

/*function validateForm(){
	if(confirm("Are you sure?")==true){		
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Document has been closed successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}*/
</script>
