<!-- Content Header (Page header) -->
<?php 
$list_action = array(
    //array('link'=>base_url('/purchase/purchase_order_print')."/".$form_header[0]['doc_num'],'icon'=>'print','text'=>'Print'),
    array('link'=>base_url('/purchase/purchase_invoice_cancel')."/".$form_header[0]['doc_num'],'icon'=>'ban','text'=>'Cancel'),
    array('link'=>base_url('/purchase/purchase_invoice_close')."/".$form_header[0]['doc_num'],'icon'=>'minus-circle','text'=>'Close')
);
$list_download = array(
    array('link'=>base_url('/purchase/purchase_invoice_print')."/".$form_header[0]['doc_num'],'icon'=>'file-excel-o','text'=>'Excel'),
    array('link'=>base_url('/purchase/purchase_invoice_print')."/".$form_header[0]['doc_num'],'icon'=>'file-pdf-o','text'=>'PDF'),
    array('link'=>base_url('/purchase/purchase_invoice_print')."/".$form_header[0]['doc_num'],'icon'=>'print','text'=>'PRINT')
);
?>
<?php
   echo form_open('', config_form());
   echo form_text('','hidden','doc_id',$form_header[0]['doc_id']);
?>
<section class="content-header">
    <h1>
        <a class="" href="<?php echo site_url('purchase/purchase_invoice');?>">Purchase Invoice</a> 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Edit 
        &nbsp;&nbsp;&nbsp;
        <div class="pull-right button_action" id="button_action"></div>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
    	<div class="col-sm-6">
        	<div class="form-group col-sm-12">
                <label class="col-sm-3" for="buss_name">
                	Vendor                    
                </label>
                <div class="col-sm-9">
                	<input type="hidden" id="buss_id" name="buss_id" value="">
                    <input type="hidden" id="buss_char" name="buss_char" value="">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="" readonly>                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_vendor" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                   
                </div>
            </div>
            <!--<div class="form-group">
                <label class="col-sm-3" for="doc_ref">
                	Purchase Order                    
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="po_doc_ref_id" name="po_doc_ref_id" value="" />
                    <input type="hidden" id="po_doc_ref_type" name="po_doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="po_doc_ref" name="po_doc_ref" value="" readonly>                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_po" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                                       
                </div>
            </div>-->
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_ref">
                	Doc Ref                   
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="doc_ref_id" name="doc_ref_id" value="" />
                    <input type="hidden" id="doc_ref_type" name="doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="doc_ref" name="doc_ref" value="<?php echo $form_header[0]['doc_ref'];?>" readonly>
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" 
                            	data-toggle="modal" href="#modal_rcv" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                 
                </div>
            </div>
            <!--<div class="form-group">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>-->
            <!--<div class="form-group">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr" readonly></textarea>
                </div>
            </div> -->           
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"><?php echo $form_header[0]['doc_note'];?></textarea>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                </div>
            </div>	
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_ext_ref">Vendor Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_ext_ref" name="doc_ext_ref" value="<?php echo $form_header[0]['doc_ext_ref'];?>">
                </div>
            </div>  
        	<div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>		
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_ddt">Due Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-12">
        	<div class="table-responsive">
        	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
            <thead>
            <tr>
            	<th style="width:3%">#</th>
                <th style="width:12%">Item Code</th>
                <th style="width:12%">Item Name</th>
                <th style="width:8%">Quantity</th>
                <th style="width:10%">Price</th>
                <th style="width:8%">Disc %</th>
                <th style="width:10%">Tax</th>
                <th style="width:12%">Net Price</th>
                <th style="width:15%">Total</th>
                <th style="width:10%" class="action">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr id="default">
            	<input type="hidden" name="item_id[]" id="item_id1" class="form-control">
            	<td style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                    <input type="hidden" name="reff_line[]" id="reff_line1" value="" />
                </td>
                <td>
                	<div class="input-group">                    	
	                    <span class="input-group-btn">
                            <a href="#" class="btn btn-warning form-control" name="a_ast_code" target="_blank">
                            	<i class="fa fa-external-link"></i>
                            </a>                            
						</span>
                        <input type="text" name="ast_code[]" id="ast_code1" class="form-control " readonly="readonly">
                    </div>
                </td>
                <td><input type="text" name="ast_name[]" id="ast_name1" class="form-control" readonly="readonly"></td>
                <td>                	
                   	<input type="hidden" name="ast_qty_open[]" id="ast_qty1" >
                    <input type="hidden" name="ast_rcv_qty[]" id="ast_rcv_qty1">
                    <input type="number" name="ast_qty[]" id="ast_qty1" 
                        class="form-control" style="text-align:right">
				</td>
                <td><input type="number" name="ast_price[]"  id="ast_price1"  class="form-control" style="text-align:right"></td>
                <td><input type="number" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right"readonly></td>
                
                <td>
                	<div class="input-group">
						<div class='input-group-btn'>
                            <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                            	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
						</div>
					</div>
				</td>
            </tr>
            </tbody>
            <tfoot>
            	<tr>
                	<td colspan="5" id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                    <td>
                    	<div>
                    	<input type="number" class="form-control" id="amount_total" name="amount_total" value="0" 
                        	style="text-align:right;" readonly>
                        </div>
                   	</td>
                </tr>                
            </tfoot>
            </table>
            </div>
    	</div>
    </div>
    <br />		
</section>
</form>
<template class="button_action">
    <a id="new" class="btn btn-info" target="_blank" href="<?php echo base_url('/purchase/purchase_invoice_add/');?>">
        <i class="fa fa-plus"></i>&nbsp;&nbsp; New
    </a>
    <?php $status = doc_status($form_header[0]['doc_status']);?>
    <div class="btn btn-<?php echo $status['color'];?>">
        Doc Status : <?php echo $status['status'];?>
    </div>

    <?php if($form_header[0]['doc_status'] == 'DRAFT'){ ?>
        <button type="submit" class="btn btn-info" id="confirm">
            <i class="fa fa-check"></i>&nbsp;&nbsp; Confirm
        </button>
        <button type="submit" class="btn btn-info" id="save">
            <i class="fa fa-check"></i>&nbsp;&nbsp; Update Draft
        </button>
    <?php } ?>

    <?php if($form_header[0]['doc_status'] == 'AWAITING' || $form_header[0]['doc_status'] == 'IN REVIEW' ){ ?>
        <?php echo form_button_action('View','success',$list_download);?>
        <?php echo form_button_action('Action','success',$list_action);?>
    <?php } else ?>

    <?php if($form_header[0]['doc_status'] == 'CANCELLED'){ ?>

    <?php } ?>

    <?php if($form_header[0]['doc_status'] == 'UNPAID'){ ?>
        <?php echo form_button_action('View','success',$list_download);?>
    <?php } ?>

    <?php if($form_header[0]['doc_status'] == 'PAID'){ ?>
        <?php echo form_button_action('View','success',$list_download);?>
    <?php } ?>

    <?php //echo form_button_action('Action','success',$list_action);?>
    <!-- <a class="btn btn-info" href="<?php echo base_url('/purchase/purchase_invoice_add/')."/".$form_header[0]['doc_num'];?>">
        <i class="fa fa-sign-out"></i>&nbsp;&nbsp; Invoice
    </a>
    <button type="submit" class="btn btn-info">
        <i class="fa fa-check"></i>&nbsp;&nbsp; Update
    </button> -->
</template>
<!-- /.content -->

<?php //LAYOUT HELPER: MODAL
	echo sw_createModal('modal_vendor','Select Vendor','dialog_vendor_push_data();','business/dialog_vendor');
	echo sw_createModal('modal_po','Select PO','dialog_purchase_order_push_data();','purchase/dialog_purchase_order');
	echo sw_createModal('modal_rcv','Select Receipt','dialog_purchase_receipt_push_data();','purchase/dialog_purchase_receipt');
?>
<?php //var_dump($form_detail);?>
<script>
var source_location = '<?php //echo json_encode($ddl_whse);?>';
//var source_po = <?php// echo json_encode($dialog_po);?>;
var source_vendor = <?php echo json_encode($dialog_vendor);?>;
var source_rcv = '<?php //echo json_encode($dialog_rcv);?>';
var source_po = '<?php //echo json_encode($dialog_po);?>';

var form_header = <?php echo json_encode($form_header);?>;
var form_detail = <?php echo json_encode($form_detail);?>;

var return_url = "<?php echo site_url('purchase/purchase_invoice');?>";
var submit_url = "<?php echo site_url('purchase/purchase_invoice_edit_query');?>/"+form_header[0]['doc_num'];
var ajax_url_1 = "<?php echo base_url('purchase/ajax_getAll_rcv_with_vendor');?>";
var ajax_url_2 = "<?php echo base_url('purchase/ajax_getDetail_rcv_open');?>";
var ajax_url_3 = "<?php echo base_url('purchase/ajax_getHeader_rcv');?>";
var ajax_url_4 = "<?php echo base_url('purchase/ajax_getAll_po_with_vendor');?>";
var ajax_url_5 = "<?php echo base_url('purchase/ajax_getDetail_po');?>";
var ajax_url_6 = "<?php echo base_url('purchase/ajax_getHeader_po');?>";

var url_1 = "<?php echo site_url('inventory/detail');?>";

var param = "<?php echo $param;?>";

$(document).ready(function(){	
	$("#location_id").on('change',function(){
		var result;
		for( var i = 0; i < source_location.length; i++ ) {
			if( source_location[i]['location_id'] == $(this).val() ) {
				result = source_location[i];
				break;
			}
		}
		$("#ship_addr").val(result['location_address']+', '+result['location_city']+', '+result['location_state']+', '+result['location_country']);
	});
	$("#location_id").val(1).change();
});

</script>
<script type="text/javascript">
function get_rcv_detail(){
	return $.ajax({
		url:ajax_url_2,
		data:{doc_num:$("#rcv_doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
        }
	});
}
function get_po_detail(){
	return $.ajax({
		url:ajax_url_5,
		data:{doc_num:$("#po_doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
        }
	});
}
</script>
