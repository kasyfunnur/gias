<!-- Content Header (Page header) -->
<style>
	.inactive td{background-color:#CCC;opacity: 0.5;}
	thead>tr>td{text-align:center;font-weight:bold}
	#foot_label{text-align:right;}
	label{		
		-moz-user-select: -moz-none;
		-khtml-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}	
</style>
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('purchase/purchase_return');?>">Purchase Return</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New </h1>
</section>

<!-- Main content -->
<section class="content">
    <?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
    <div class="row">
    	<div class="col-sm-6">
        	<input type="hidden" id="doc_ref_id" name="doc_ref_id" value="" />
            <input type="hidden" id="doc_ref_type" name="doc_ref_type" value="" />
            <div class="form-group">
                <label class="col-sm-3" for="buss_name">
                	Vendor                    
                </label>
                <div class="col-sm-9">
                	<input type="hidden" id="buss_id" name="buss_id" value="">
                    <input type="hidden" id="buss_char" name="buss_char" value="">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="" readonly>                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" <?php if(isset($param) && $param)echo 'disabled';?>
                            	data-toggle="modal" href="#modal_vendor" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                   
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_ref">
                	Purchase Receipt                    
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="doc_ref_id" name="doc_ref_id" value="" />
                    <input type="hidden" id="doc_ref_type" name="doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="doc_ref" name="doc_ref" value="" readonly>
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" <?php if(isset($param) && $param)echo 'disabled';?>
                            	data-toggle="modal" href="#modal_rcv" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                 
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_ref">
                	Purchase Invoice                    
                </label>
                <div class="col-sm-9">
                    <input type="hidden" id="doc_ref_id" name="doc_ref_id" value="" />
                    <input type="hidden" id="doc_ref_type" name="doc_ref_type" value="" />
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="doc_ref" name="doc_ref" value="" readonly>
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" <?php if(isset($param) && $param)echo 'disabled';?>
                            	data-toggle="modal" href="#modal_pinv" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Pick</button>                                
						</span>
                    </div>                 
                </div>
            </div>
            <!---<div class="form-group">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>--->
            <!---<div class="form-group">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr" readonly></textarea>
                </div>
            </div> --->      
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                </div>
            </div>     
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                </div>
            </div>	
        	<div class="form-group">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>		
            <div class="form-group">
                <label class="col-sm-3" for="doc_ddt">Due Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-12">
        	<table class="table-bordered" id="table_detail">
            <thead>
            <tr>
            	<td width="5%">#</td>
                <td width="25%">Item Code</td>
                <td width="30%">Item Name</td>
                <td width="40%">Qty</td>
                <!---<td width="20%">Price</td>
                <td width="25%">Total</td>--->
                <td width="10%">Action</td>
            </tr>
            </thead>
            <tbody>
            <tr height="30px" id="default">
            	<td style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                    <input type="hidden" name="reff_line[]" id="reff_line1" value="" />
                </td>
                <td>
                	<input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                	<input type="text" name="ast_code[]" id="ast_code1" class="form-control" readonly="readonly">
                </td>
                <td><input type="text" name="ast_name[]" id="ast_name1" class="form-control" readonly="readonly"></td>
                <td>
                	<input type="number" name="ast_qty[]" id="ast_qty1" 
                		class="form-control" style="text-align:right">
                   	<input type="hidden" name="ast_qty_open[]" id="ast_qty1" 
                    	class="form-control" readonly="readonly" style="text-align:right">
                    <input type="hidden" name="ast_rcv_qty[]" id="ast_rcv_qty1" 
                    	class="form-control" readonly="readonly" style="text-align:right">
				</td>
                <!---<td><input type="number" name="ast_price[]"  id="ast_price1"  
                	class="form-control" style="text-align:right" readonly="readonly"></td>
                <td><input type="number" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right"readonly></td>--->
                
                <td>
                	<div class="input-group">
						<div class='input-group-btn'>
                            <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                            	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
						</div>
					</div>
				</td>
            </tr>
            </tbody>
            <tfoot>
            	<tr>
                	<td colspan="3" id="foot_label"><strong>Total Qty:&nbsp;&nbsp;&nbsp;</strong></td>
                    <td>
                    	<div>
                    	<input type="number" class="form-control" id="qty_total" name="qty_total" value="0" 
                        	style="text-align:right;" readonly>
                        </div>
                   	</td>
                </tr>                
            </tfoot>
            </table>
    	</div>
    </div>
    <hr />
    <!---<div class="row">
    	<div class="col-sm-12">
            <table class="table-bordered" id="table_expense">
                <thead>
                <tr>
                    <td width="10%">#</td>
                    <td width="45%">Expense</td>
                    <td width="25%">Amount</td>
                    <td width="20%">Action</td>
                </tr>
                </thead>
                <tbody>
                	<tr height="30px">
                        <td style="text-align:center">
                        	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                        </td>
                        <td><input type="text" name="ast_qty_open[]" id="ast_qty1" 
	                    	class="form-control">
                       	</td>
                        <td><input type="number" name="ast_qty_open[]" id="ast_qty1" 
                    		class="form-control" style="text-align:right">
                        </td>
                        <td>
                        	<div style='text-align:center'>
                                <div class='btn-group'>
                                    <button type="button" class="btn btn-flat btn-xs btn-danger" id="act_del1" name="act_del">DELETE</button>
                                </div>
                            </div>
						</td>
                    </tr>
                </tbody>
			</table>
        </div>
    </div>--->
    <!---<br />--->
    <div class="row">
    	<div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
		</div>
    </div>    
	</form>	
</section>
<!-- /.content -->

<?php //var_dump($dialog_po);?>
<script>
var source_rcv = <?php //echo json_encode($dialog_rcv);?>'';
var source_po = <?php //echo json_encode($dialog_po);?>'';
</script>
<!---<section class="modal">--->
<?php 
	echo sw_createModal('modal_pinv','Select Purchase Invoice','dialog_purchase_invoice_push_data();','purchase/dialog_purchase_invoice');
	echo sw_createModal('modal_rcv','Select Receipt','dialog_purchase_receipt_push_data();','purchase/dialog_purchase_receipt');
	echo sw_createModal('modal_vendor','Select Vendor','dialog_vendor_push_data();','business/dialog_vendor');
?>
<!---</section>--->
<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
//var source_po = <?php// echo json_encode($dialog_po);?>;
var source_vendor = <?php echo json_encode($dialog_vendor);?>;

var return_url = "<?php echo site_url('purchase/purchase_return');?>";
var submit_url = "<?php echo site_url('purchase/purchase_return_add_query');?>";
var ajax_url_1 = "<?php echo base_url('purchase/ajax_getAll_rcv_with_vendor');?>";
var ajax_url_2 = "<?php echo base_url('purchase/ajax_getDetail_rcv_open');?>";
var ajax_url_3 = "<?php echo base_url('purchase/ajax_getHeader_rcv');?>";
var ajax_url_4 = "<?php echo base_url('purchase/ajax_getAll_po_with_vendor');?>";
var ajax_url_5 = "<?php echo base_url('purchase/ajax_getDetail_po');?>";
var ajax_url_6 = "<?php echo base_url('purchase/ajax_getHeader_po');?>";

var param = "<?php echo $param;?>";

$(document).ready(function(){	

});

</script>
<script type="text/javascript">
function get_rcv_detail(){
	return $.ajax({
		url:ajax_url_2,
		//data:{doc_num:$("#rcv_doc_ref").val()},
		data:{doc_num:$("#doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
        }
	});
}
function get_pinv_detail(){
	return $.ajax({
		url:ajax_url_5,
		data:{doc_num:$("#po_doc_ref").val()},
		success: function(response) {
			data = JSON.parse(response);
			table_detail_reset('table_detail');
			table_detail_insert(data);			
        }
	});
}
</script>
