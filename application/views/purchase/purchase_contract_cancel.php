<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('purchase/purchase_order');?>">Purchase Order</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        CANCELLING... 
    </h1>   
</section>

<?php   echo form_open('', config_form());
        echo form_text('','hidden','doc_id',$form_header[0]['doc_id']);
?>   
<!-- Main content -->
<section class="content">   
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="buss_name">
                    Vendor                   
                </label>
                <div class="col-sm-9">
                    <div class="input-group input-group-sm">
                        <input disabled type="text" class="form-control input-sm" id="buss_name" name="buss_name" readonly="readonly"        
                            value="<?php echo $form_header[0]['buss_name'];?>">
                        <input type="hidden" id="buss_id" name="buss_id" value="<?php echo $form_header[0]['buss_id'];?>">                      
                        <span class="input-group-btn">
                            <button disabled type="button" class="control btn btn-warning" data-toggle="modal" href="#modal_vendor" style="width:100%;">
                                <i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
                        </span>
                    </div>                    
                </div>
            </div>
            <!--<div class="form-group">
                <label class="col-sm-3" for="doc_ref">Doc Ref</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_ref" name="doc_ref" >
                </div>
            </div>-->
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php //echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name', $form_header[0]['whse_id']);?>
                    <input disabled type="text" class="form-control input-sm" id="location_id" name="location_id" value="<?php echo $form_header[0]['location_name'];?>" disabled>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <textarea disabled class="form-control input-sm" id="ship_addr" name="ship_addr" readonly><?php echo $form_header[0]['ship_addr'];?></textarea>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea disabled class="form-control input-sm" id="doc_note" name="doc_note"><?php echo $form_header[0]['doc_note'];?></textarea>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $form_header[0]['doc_num'];?>" readonly>
                    <input type="hidden" id="doc_id" name="doc_id" value="<?php echo $form_header[0]['doc_id'];?>" />
                </div>
            </div>  
            <div class="form-group col-sm-12 col-xs-6">
                <!--<div class="row">-->
                    <label class="col-sm-3" for="doc_dt">Date</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo substr($form_header[0]['doc_dt'],0,10);?>" disabled>
                    </div>
                <!--</div>-->
            </div>      
            <div class="form-group col-sm-12 col-xs-6">
                <!--<div class="row">-->
                    <label class="col-sm-3" for="doc_ddt">Date Req</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo substr($form_header[0]['doc_ddt'],0,10);?>" disabled>
                    </div>
                <!--</div>-->
            </div>      
                    
        </div>
    </div>
    
<!--<a href="http://www.google.com" class="form-control"><span>Linksss</span></a>-->

    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
            <table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
            <thead>
            <tr>
                <th style="width:5%">#</th>
                <th style="width:20%">
                    <button disabled type="button" class="control btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                        <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Item Code</span>
                        <span class="visible-xs"><i class="fa fa-search"></i></span>
                    </button>
                </th>
                <th style="width:20%">Item Name</th>
                <th style="width:10%">Quantity</th>
                <th style="width:15%">Price</th>
                <th style="width:20%">Total</th>
                <th style="width:10%" class="action">Action</th>              
            </tr>
            </thead>
            <tbody>
            <tr id="default">
                <input type="hidden" name="item_id[]" id="item_id1" class="form-control">
                <td style="text-align:center">
                    <input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                </td>
                <td>
                    <div class="input-group">                       
                        <span class="input-group-btn">
                            <a href="#" class="btn btn-warning form-control" name="a_ast_code" target="_blank">
                                <i class="fa fa-external-link"></i>
                            </a>                            
                        </span>
                        <input type="text" name="ast_code[]" id="ast_code1" class="form-control " readonly="readonly">
                    </div>
                </td>
                <td>
                    <input type="text" name="ast_name[]" id="ast_name1" class="form-control">
                </td>
                <td>
                    <input type="number" name="ast_qty[]"  id="ast_qty1"  class="form-control" style="text-align:right">
                </td>
                <td>
                    <input type="text" name="ast_price[]"  id="ast_price1"  class="form-control" style="text-align:right">
                </td>
                <td>
                    <input type="text" name="sub_total[]"  id="sub_total1"  class="form-control"  style="text-align:right"readonly>
                </td>
                <td>
                    <div class="input-group">
                        <div class='input-group-btn'>
                            <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                                <span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
                                <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
                        </div>
                    </div>
                </td>                
            </tr>            
            </tbody>
            <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><div>
                    <input type="number" class="form-control" id="qty_total" name="qty_total" 
                        value="0" style="text-align:right;" readonly>
                    </div>
                </td>
                <td id="foot_label"><strong>Sub Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_subtotal" name="doc_subtotal" 
                        value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td id="foot_label" style="text-align:right;">
                    <input type="number" class="control col-sm-5" id="doc_disc_pct" name="doc_disc_pct"
                        value="0" style="text-align:right;" />%
                    <strong>Disc:&nbsp;&nbsp;&nbsp;</strong>
                </td>
                <td><div>
                    <input type="text" class="form-control" id="doc_disc" name="doc_disc" 
                        value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_total" name="doc_total" 
                        value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            </tfoot>
            </table>
            </div>
        </div>
    </div>
    <hr />  
    <div class="row">
        <div class="col-sm-3">Reason for cancelling document:</div>
        <div class="col-sm-6">
            <textarea required name="reason" id="reason" class="col-sm-12"/></textarea>
        </div>
        <div class="col-sm-3">
            <button type="submit" class="btn btn-info btn-lg pull-left">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
        </div>
    </div>    
    <br />
    
</section>
</form> 
<!-- /.content -->

<script>
var form_header = <?php echo json_encode($form_header);?>;
var form_detail = <?php echo json_encode($form_detail);?>;

var return_url = "<?php echo site_url('purchase/purchase_order');?>";
var submit_url = "<?php echo site_url('purchase/purchase_order_cancel_query');?>/"+'<?php echo $form_header[0]['doc_num'];?>';
var ajax_url_1 = "<?php echo base_url('inventory/ajax_inventory_info');?>";
var url_1 = "<?php echo site_url('inventory/detail');?>";
/*function validateForm(){
	if(confirm("Are you sure?")==true){		
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Document has been cancelled successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}*/
</script>
