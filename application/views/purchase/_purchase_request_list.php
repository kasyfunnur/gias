<section class="content-header">
  <h1> Purchase Request
  <a href="<?php echo site_url('purchase/purchase_request_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<i class="fa fa-plus"></i>&nbsp;&nbsp;New Request
		</button>
    </a>
  </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<section class="content">
<div class="box box-info">
    <div class="box-header">
    </div>
    <div class="box-body table-responsive">
  	<table class="table table-hover">
    	<thead>
        	<tr>
            	<th>#</th>
                <th>Doc Num</th>
                <th>Date</th>
                <th>For</th>
                <th>Dept</th>
                <th>Location</th>
                <th>Required</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>
            <tr>
            	<td><?php echo $x+1;?>.</td>
                <td><a href="<?php echo site_url('purchase/purchase_request_edit');?>/<?php echo $datatable[$x]['doc_num'];?>"><?php echo $datatable[$x]['doc_num'];?></a></td>
                <td><?php echo $datatable[$x]['doc_dt'];?></td>
                <td><?php echo $datatable[$x]['request_for'];?></td>
                <td><?php echo $datatable[$x]['request_dept'];?></td>
                <td><?php echo $datatable[$x]['location_name'];?></td>
                <td><?php echo $datatable[$x]['doc_ddt'];?></td>
                <td><span class="label label-info"><?php echo $datatable[$x]['doc_status'];?></span></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    </div>
    </div>
</section>
<!-- /.content -->