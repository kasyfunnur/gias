<!-- Content Header (Page header) -->
<section class="content-header">
  	<h1> Purchase Order 
    <a href="<?php echo site_url('purchase/purchase_order_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<span class="hidden-xs">
        		<i class="fa fa-plus"></i>&nbsp;&nbsp;New Order
            </span>
        	<span class="visible-xs">
        		<i class="fa fa-plus"></i>
            </span>
        </button>
    </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg:"";?>
<!-- Main content -->
<?php $attributes = array(
    'class'     => 'form-horizontal',
    'role'      => 'form',
    'method'    => 'post', 
    'name'      => 'frm', 
    'id'        => 'frm',
    'onSubmit'  => 'return validateForm();'
    );
    echo form_open('', $attributes);?>
<section class="content">
<div class="box box-info">
    <div class="box-body table-responsive">
  	<table class="table table-hover table-condensed" id="purchase_order_index">
    	<thead>
            <tr class="filter">
                <th style="width:25%;text-align:left">
                    Total = <?php echo $rows;?> document(s).
                </th>
                <th style="width:35%;text-align:left">                  
                    <!--Filter : <select name="sel_year"><option value="2015">2015</option><option value="2014">2014</option></select>  -->
                </th>
                <th style="width:40%;text-align:left">
                    
                </th>
            </tr>
            <tr>                
                <th style="width:25%;text-align:left">
                    <div class="input-group input-group-sm">
                        <span class="input-group-btn">                            
                            <button type="submit" class="btn btn-info" name="btn_filter" value="" />ALL</button>
                            <button type="submit" class="btn btn-default" name="btn_filter" value="DRAFT" />DRAFT</button>
                            <button type="submit" class="btn btn-info" name="btn_filter" value="AWAITING"/>AWAITING</button>
                            <button type="submit" class="btn btn-warning" name="btn_filter" value="ACTIVE"/>ACTIVE</button>
                            <button type="submit" class="btn btn-danger" name="btn_filter" value="CANCELLED"/>CANCELLED</button>
                            <button type="submit" class="btn btn-success" name="btn_filter" value="CLOSED"/>CLOSED</button>
                        </span>
                    </div>
                </th>
                <th style="width:35%;text-align:left">
                    <div class="input-group input-group-sm">
                        <?php echo sw_textButton('search_field','Search Here..','search_button','Search','search');?>
                    </div>
                </th>
                <th style="width:40%;text-align:left">                    
                    <?php 
                    $page_config['base_url'] = $this->uri->config->config['base_url']."purchase/purchase_order";
                    $page_config['total_rows'] = $rows;
                    $page_config = array_merge($page_config,$this->config->item('pagination','pagination'));
                    $this->pagination->initialize($page_config); 
                    ?>
                    <div class="input-group input-group-sm">                        
                        <ul class="pagination">
                            <?php echo $this->pagination->create_links();?>                            
                        </ul>
                    </div>                    
                </th>
            </tr>
        </thead>
        <tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>
			<?php $status = doc_status($datatable[$x]['doc_status']);?>
            <tr>
                <td>
                    <span class="searchable" style="font-size:16px;font-weight:lighter !important;color:#888">
                        <a href="<?php echo site_url('purchase/purchase_order_edit');?>/<?php echo $datatable[$x]['doc_num'];?>">
                            <?php echo $datatable[$x]['doc_num'];?>
                        </a> 
                        - <?php echo $datatable[$x]['buss_name'];?>
                    </span>
                    
                    <br />
                    <span style="font-size:12px;font-weight:lighter !important;color:#888">
                        By <?php echo $datatable[$x]['create_by'];?>
                        - <?php echo date("d M, Y", strtotime($datatable[$x]['doc_dt']));?>                        
                        <!--<button class="btn btn-info btn-xs comment" name="<?php echo $datatable[$x]['doc_num'];?>">
	                        <i class="fa fa-comments"></i>
                        </button>-->
                    </span>
                    <span class="pull-right">                        
                        <strong>
                            <span class="label label-xs label-<?php echo $status['color'];?>" style="margin:2px">
                            <i class="fa"></i> <?php echo $status['status'];?></span>
                        </strong>
                    </span>
                </td>
                <?php 					
                    $disable_cancel = '';
                    $disable_close = '';
                    $disable_print = '';
                    $disable_receipt = '';
                    switch($datatable[$x]['doc_status']){
                        
                        case "DRAFT":
                            $disable_cancel = '';$disable_close = 'disabled';
                            $disable_print = 'disabled';$disable_receipt = 'disabled';
                            break;
                        case "AWAITING":
                            $disable_cancel = '';$disable_close = 'disabled';
                            $disable_print = '';$disable_receipt = 'disabled';
                            break;
                        case "IN REVIEW":
                            $disable_cancel = '';$disable_close = 'disabled';
                            $disable_print = '';$disable_receipt = 'disabled';
                            break;
                        case "ACTIVE":
                            $disable_cancel = '';$disable_close = '';
                            $disable_print = '';$disable_receipt = '';
                            break;
                        case "CANCELLED":
                            $disable_cancel = 'disabled';$disable_close = 'disabled';
                            $disable_print = 'disabled';$disable_receipt = 'disabled';
                            break;
                        case "CLOSED":
                            $disable_cancel = 'disabled';$disable_close = 'disabled';
                            $disable_print = '';$disable_receipt = 'disabled';
                            break;
                        default:
                            $disable_cancel = '';$disable_close = '';
                            $disable_print = '';$disable_receipt = '';
                    } 
                     
				?>
                <td>
                	[Due : <?php echo date("d M, Y", strtotime($datatable[$x]['doc_ddt']));?>]
                    <?php //echo $logistic_status;?> <?php //if($datatable[$x]['doc_paid'] > 0) echo $dp_status;?> 
                	<br />
                	<span class="searchable"><?php echo $datatable[$x]['doc_note'];?></span>
                </td>              
                <td> 
                    <?php
                        $link_receipt = base_url('purchase/purchase_receipt_add')."/".$datatable[$x]['doc_num'];
                        $link_cancel = base_url('purchase/purchase_order_cancel')."/".$datatable[$x]['doc_num'];
                        $link_close = base_url('purchase/purchase_order_close')."/".$datatable[$x]['doc_num'];
                        $link_print = base_url('purchase/purchase_order_print')."/".$datatable[$x]['doc_num'];
                        $link_dp = base_url('purchase/purchase_invoice_dp_add')."/".$datatable[$x]['doc_num'];
                    ?>
                	<a href="<?php echo $link_cancel;?>" class="btn btn-sm btn-danger <?php echo $disable_cancel;?>">
                        <i class="fa fa-ban"></i> Cancel</a>&nbsp;&nbsp;
					<a href="<?php echo $link_close;?>" class="btn btn-sm btn-warning <?php echo $disable_close;?>">
                        <i class="fa fa-archive"></i> Close</a>&nbsp;&nbsp;
                    <a href="<?php echo $link_print;?>" class="btn btn-sm btn-success <?php echo $disable_print;?>" target="_blank">
                        <i class="fa fa-print"></i> Print</a>&nbsp;&nbsp;
					<a href="<?php echo $link_receipt;?>" class="btn btn-sm btn-info <?php echo $disable_receipt;?>" target="_blank">
                        <i class="fa fa-plus"></i> Receipt</a>&nbsp;&nbsp;
					<!-- <a href="<?php //echo $link_dp;?>" class="btn btn-sm btn-info <?php // echo $disable;?>" target="_blank">
                        <i class="fa fa-dollar"></i> DP</a>&nbsp;&nbsp; -->
                </td>
            </tr>
            <?php }?>
        </tbody>
    </table>
    </div>
</div>
</section>
</form>
<!-- /.content -->

<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>

<script>
$(document).ready(function(){
	/*$(".pagination").click(function(e){
        $("input[name='btn_filter']").val('ACTIVE');
    });*/
});
</script>