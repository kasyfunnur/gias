<style type="text/css">
	table.printBody tr td{
		font-family:Verdana, Geneva, sans-serif;
		font-size:12px;
		border: solid 1px #CCC;
	}	
	thead{
		font-weight: bold;
		text-align: center;	
	}	
	td.numeric{
		text-align:right;
	}
	tr.aggr, tr.disc, tr.total{
		font-weight: bold;	
	}
</style>
<!---<pre>--->
<?php 
	//print_r($form_header);
	//print_r($form_detail);
?>
<!---</pre>--->
<table width="99%" class="info">
<tr>
	<td width="50%"> <!---Business Info--->
	    <table width="100%">
        <tr>
            <td>Kepada </td><td>: <?php echo $form_header[0]['buss_name']?></td>        
        </tr>
        <tr>
            <td>Alamat </td><td>: <?php echo $form_header[0]['buss_addr']." - ". $form_header[0]['buss_city'];?></td>        
        </tr>        
        </table>
    </td>
    <td width="50%"> <!---Document Info--->
	    <table width="100%">
        <tr>
            <td>Tanggal </td><td>: <?php echo date("d-M-y",strtotime($form_header[0]['doc_dt']));?></td>        
        </tr>
        <tr>
            <td>Jatuh Tempo (<?php //echo $form_header[0]['pymt_name'];?>) </td><td>: <?php echo date("d-M-y",strtotime($form_header[0]['doc_ddt']));//." ( ". $form_header[0]['pymt_name']. " )";?></td>        
        </tr>        
        </table>
    </td>
	 
</tr>
	
</table>

<table width="100%" class="printBody">
<thead>
	<td>No</td>
    <td>Art.</td>
    <td>Model</td>
    <td>Qty</td>
    <td>UoM</td>
    
    <td>Harga</td>
    <td>Jumlah</td>    
</thead>
<tbody>
<?php //AGGR INIT
	$aggr_item_qty = 0;
	$aggr_item_total = 0;
?>
<?php for($x = 0; $x<= count($form_detail)-1; $x++){ ?>	
    <tr>
        <td class="numeric"><?php echo $x+1;?>. </td>
        <td><?php echo $form_detail[$x]['item_code'];?></td>
        <td><?php echo $form_detail[$x]['item_name'];?></td>
        <td class="numeric"><?php echo number_format($form_detail[$x]['item_qty'],2);?></td>
        <td class="">Pcs<?php //echo $form_detail[$x]['uom_name'];?></td>
        
        <td class="numeric"><?php echo number_format($form_detail[$x]['item_netprice'],2);?></td>
        <td class="numeric"><?php echo number_format($form_detail[$x]['item_total'],2);?></td>        
    </tr>    
<?php //AGGR OPT
	$aggr_item_qty += $form_detail[$x]['item_qty'];
	$aggr_item_total += $form_detail[$x]['item_total'];
?>
<?php } ?>
	<tr>
    	<td colspan="7">&nbsp;</td>
    </tr>
	<tr class="aggr">
        <td class="numeric"></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="numeric"><?php echo number_format($aggr_item_qty,2);?></td>
        
        <td class="numeric">Sub Total</td>
        <td class="numeric"><?php echo number_format($aggr_item_total,2);?></td>
    </tr>
	<tr class="disc">
        <td class="numeric"></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="numeric"></td>
        
        <td class="numeric">Discount</td>
        <td class="numeric"><?php echo number_format($form_header[0]['doc_disc_amount'],2);?></td>
    </tr>     
	<tr class="total">
        <td class="numeric"></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="numeric"></td>
        
        <td class="numeric">Total</td>
        <td class="numeric"><?php echo number_format($form_header[0]['doc_total'],2);?></td>
    </tr>         
</tbody>
</table>

<table id="footer" width="100%">
<!---<tr>
	<td colspan="6">
    	Harap melakukan pembayaran ke: <br />
		<?php //echo $banks[0]['bank_name']." - ".$banks[0]['bank_branch']." : ".$banks[0]['bank_acct'];?> 
        A/N: <?php //echo $banks[0]['bank_behalf'];?>
    </td>
</tr>--->
<tr><td colspan="6">&nbsp;</td></tr>
<tr>
	<td colspan="2" align="center">Dibuat oleh</td>
    <td colspan="2" align="center">Disetujui oleh</td>
    <td colspan="2" align="center">Diterima oleh</td>    
</tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr>
	<td align="center">(</td><td align="center">)</td>
    <td align="center">(</td><td align="center">)</td>
    <td align="center">(</td><td align="center">)</td>
</tr>
</table>