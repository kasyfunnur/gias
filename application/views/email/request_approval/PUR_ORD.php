<table>
  <tr>
    <td>Document Number</td>
    <td>:</td>
    <td><strong style="color:#0070F5"><?php echo $document['header']['doc_num'] ; ?></strong></td>
  </tr>
  <tr>
    <td>Vendor</td>
    <td>:</td>
    <td><strong style="color:#0070F5"><?php echo $document['header']['buss_name'] ; ?></strong></td>
  </tr>
  <tr>
    <td>Document Date</td>
    <td>:</td>
    <td><strong style="color:#0070F5"><?php echo date('l, jS M Y',strtotime($document['header']['doc_dt'])) ; ?></strong></td>
  </tr>
  <tr>
    <td>Document Amount</td>
    <td>:</td>
    <td align="right"><strong style="color:#0070F5">Rp <?php echo number_format($document['header']['doc_total']) ; ?></strong></td>
  </tr>
  <tr>
    <td valign="top">Item to be Purchased</td>
    <td valign="top">:</td>
    <td valign="top"><strong style="color:#0070F5">
      <?php foreach( $document['detail'] as $item ) {
                       echo $item['item_code'].' - '.$item['item_name'].' - '. $item['item_qty'] . '<br>';
                   }; ?>
      </strong>
    </td>
  </tr>
</table>