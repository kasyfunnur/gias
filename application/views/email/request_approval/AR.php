<div>
	<h1 style="font-family:Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, sans-serif; color:#058DBD; font-weight:100;">
    	New Request for Approval
    </h1>
    <p>
    Dear <?php $approver_name; ?><br>
    the Foloowing Request has been created for your review and action:<br>
    <table>
    	<tr>
        	<td>Document Number</td>
            <td>:</td>
            <td><strong style="color:#0070F5"><?php echo $doc_number ; ?></strong></td>
        </tr>
        <tr>
        	<td>Requestor</td>
            <td>:</td>
            <td><strong style="color:#0070F5"><?php echo $requestor_name ; ?></strong></td>
        </tr>
        <tr>
        	<td>Date Requested</td>
            <td>:</td>
            <td><strong style="color:#0070F5"><?php echo $request_date ; ?></strong></td>
        </tr>
        <tr>
        	<td>Asset Requested</td>
            <td>:</td>
            <td>
            	<strong style="color:#0070F5">
					<?php foreach( $asset_list as $asset ) {
                        echo $asset['asset_label'].' - '.$asset['asset_name'] . '<br>';
                    }; ?>
				</strong>
            </td>
        </tr>
    </table>
    </p>
    <p>
    	<a style="margin: 10px; padding: 8px; background-color:#5cb85c; color:#FFFFFF" href="<?php echo base_url("approval/approve_document/$key/$doc_id/3")?>">Approve</a>
        <a style="margin: 10px; padding: 8px; background-color:#f0ad4e; color:#FFFFFF" href="<?php echo base_url("approval/approve_document/$key/$doc_id/4")?>">Revise</a>
        <a style="margin: 10px; padding: 8px; background-color:#d9534f; color:#FFFFFF" href="<?php echo base_url("approval/approve_document/$key/$doc_id/5")?>">Reject</a>
    </p>
</div>