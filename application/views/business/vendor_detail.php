<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('business/vendor_list');?>">Bussiness</a> 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Vendor 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Edit
        &nbsp;&nbsp;&nbsp;
    </h1>
</section>

<?php //echo (isset($msg))?$msg:""?>
<!-- Main content -->
<section class="content">
    <?php
    $attributes = array(
        'class'     => 'form-horizontal',
        'role'      => 'form',
        'method'    => 'post', 
        'name'      => 'frm', 
        'id'        => 'frm',
        'onSubmit'  => 'return validateForm();'
        );
    echo form_open('', $attributes);
    ?>
    <div class="container">
        <div class="row info">
            <div class="col-sm-4">
                <!-- <table width="100%"> -->
                    <?php echo form_text_ro('Code','text','buss_code',$business[0]['buss_code']);?>
                    <?php echo form_text('Name','text','buss_name',$business[0]['buss_name'],'Name');?> 
                    <?php echo form_text('Business Address','text','buss_addr',$business[0]['buss_addr'],'Street../Address..');?>                
                    <?php echo form_text('City','text','buss_city',$business[0]['buss_city'],'City');?>
                    <?php echo form_text('State','text','buss_state',$business[0]['buss_state'],'State');?> 
                    <?php echo form_text('Country','text','buss_country',$business[0]['buss_country'],'Country');?>
                <!-- </table> -->
            </div>        
            <div class="col-sm-4">
                <!-- <table width="100%"> -->
                                   
                    <?php echo form_text('Tlp 1','text','buss_tlp1',$business[0]['buss_tlp1'],'Telephone 1');?>
                    <?php echo form_text('Tlp 2','text','buss_tlp2',$business[0]['buss_tlp2'],'Telephone 2');?>
                    <?php echo form_text('Email','text','buss_email',$business[0]['buss_email'],'Email');?>
                    <?php echo form_text('Website','text','buss_website',$business[0]['buss_website'],'Website');?>
                <!-- </table> -->
            </div>
        </div>
        <br>
        <div class="row info">        
            <div class="col-sm-4">
                <!-- <table width="100%"> -->                
                    <?php echo form_text('Default Curr','text','buss_curr',$business[0]['buss_curr'],'Curr');?>
                    <?php echo form_text('Limit','number','limit_local',$business[0]['limit_local'],'Trx Limit');?>                    
                <!-- </table> -->
            </div>
        </div>
    </div>
    <br />
    <button type="submit" class="btn btn-info pull-right">
        <i class="fa fa-check"></i>&nbsp;&nbsp; Update
    </button>
    <br />
    </form>
</section>
<!-- /.content -->

<script>
var return_url = "<?php echo site_url('business/vendor_list');?>";
var submit_url = "<?php echo site_url('business/vendor_edit_query');?>";
</script>
