<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('business/vendor_list');?>">Bussiness</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Vendor 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Add
        &nbsp;&nbsp;&nbsp;
    </h1>
</section>

<?php //echo (isset($msg))?$msg:""?>
<!-- Main content -->
<section class="content">
	<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
    <div class="container">
        <div class="row info">
            <div class="col-sm-4">
                <!-- <table width="100%"> -->
                    <?php echo form_text_ro('Code','text','buss_code',$doc_num);?>
                    <?php echo form_text('Name','text','buss_name',NULL,'Name');?> 
                    <?php echo form_text('Business Address','text','buss_addr',NULL,'Street../Address..');?>                
                    <?php echo form_text('City','text','buss_city',NULL,'City');?>
                    <?php echo form_text('State','text','buss_state',NULL,'State');?> 
                    <?php echo form_text('Country','text','buss_country',NULL,'Country');?>
                <!-- </table> -->
            </div>        
            <div class="col-sm-4">
                <!-- <table width="100%"> -->
                                   
                    <?php echo form_text('Tlp 1','text','buss_tlp1',NULL,'Telephone 1');?>
                    <?php echo form_text('Tlp 2','text','buss_tlp2',NULL,'Telephone 2');?>
                    <?php echo form_text('Email','text','buss_email',NULL,'Email');?>
                    <?php echo form_text('Website','text','buss_website',NULL,'Website');?>
                <!-- </table> -->
            </div>
        </div>
        <br>
        <div class="row info">        
            <div class="col-sm-4">
                <!-- <table width="100%"> -->                
                    <?php //echo form_text('Default Curr','text','buss_curr',NULL,'Curr');?>
                    <?php //echo form_text('Limit','number','limit_local',NULL,'Trx Limit');?>                    
                <!-- </table> -->
            </div>
    	</div>
    </div>
    <br />
    <button type="submit" class="btn btn-info pull-right">
        <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
    <br />
    </form>
</section>
<!-- /.content -->

<script>
var return_url = "<?php echo site_url('business/vendor_list');?>";
var submit_url = "<?php echo site_url('business/vendor_add_query');?>";
</script>
