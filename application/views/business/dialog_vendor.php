
<?php $column = array('','Asset Code','Asset Name');?>
<div class="row">
	<div class="col-sm-12 col-xs-12">
		Search : <input type="text" id="search_vend" name="search_vend" value="" accesskey="S"/>
        <table class="table table-condensed table-hover table-striped" id="dialog_vendor_list">
        <thead>
            <tr>
                <th width="10%">&nbsp;</th>
                <th width="20%">Code</th>
                <th width="30%">Name</th>
                <th width="30%">Location</th>
            </tr>
        </thead>
        <tbody>
        
        </tbody>
        </table>
    </div>
</div>

<?php 
echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
//echo '<script src="'.base_url('assets/js/bootstrap.min.js').'"></script>';
//echo '<script src="'.base_url('assets/js/admin_app.js').'"></script>';
?>

<script type="text/javascript">
var data_vendor = '';
var vendor_default_row = '<tr><td><input type="checkbox" name="chk[]" id="chk"/></td><td><input type="hidden" name="buss_id[]" id="buss_id" /><span></span></td><td></td><td></td><td></td></tr>';

function dialog_vendor_reset_table(table){
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(vendor_default_row);
}
function dialog_vendor_populate_data(table,data){ 
	data_vendor = data; 
	if(data_vendor){
		//delete first row
		if($('#'+table+' > tbody > tr').length != 1){
			$('#'+table+' > tbody > tr:last').remove();
		}
		for(var p = 0; p<data_vendor.length; p++){
			var row = clone_row(table);
			row.find('td:eq(1)').find('input[type=hidden]').val(data_vendor[p]['buss_id']);
			row.find('td:eq(1)').find('span').text(data_vendor[p]['buss_code']);
			row.find('td:eq(2)').text(data_vendor[p]['buss_name']);
			row.find('td:eq(3)').text(data_vendor[p]['buss_addr']);
		}
		var row = clone_row(table);		
		//delete first and last after insert
		$('#'+table+' > tbody > tr:first').remove();
		$('#'+table+' > tbody > tr:last').remove();
	}else{
		console.log('error');	
	}
}

function dialog_vendor_push_data(){
	var result;
	//select process
	var selected = $('#dialog_vendor_list > tbody').find('input[name="chk[]"]:checked').parent().parent().find('td:eq(1) > input[type=hidden]').val();
	for( var i = 0; i < source_vendor.length; i++ ) { 
		if( source_vendor[i]['buss_id'] === selected ) { 
			result = source_vendor[i];
			break;
		}
	}
	dialog_vendor_pull_data(result);
}

$(document).ready(function(){
	$(document).on('click','#dialog_vendor_list tbody > tr',function(event){
		//SINGLE SELECTION
		if(event.target.type !== 'checkbox'){
			$(':checkbox',this).trigger('click');
		}else{
			if($(this).hasClass('selected')){
				$(this).removeClass('selected');
			}else{
				$(this).siblings().removeClass('selected');
				$(this).siblings().find('td > input:checkbox').prop("checked",false);
				$(this).addClass('selected');
			}
		}
	});

	
});
$("#modal_vendor").on('keypress', function(event) {
	console.log(event);
	//alert('sefsefe');
    //$(this).find("#search").focus();
});
$("#search_vend").on("keyup", function() {
    var value = $(this).val();
	var cols = $("#dialog_vendor_list").find("tr:first th").length;
    $("#dialog_vendor_list tbody tr").each(function(index) { 
		var show = 0;
		$row = $(this);
		for(var x = 0;x<=cols;x++){
			if($row.hasClass('selected')){show++; break;}
			if($row.find("td:eq("+x+")").text().toLowerCase().indexOf(value.toLowerCase()) !== -1){// console.log('yes');
				show++; break;
			}
		}
		(show>0) ? $row.show(): $row.hide();		
    });
});
</script>