<div class="row">
	<div class="col-sm-12 col-xs-12">
        <table class="table table-condensed" id="dialog_customer_list">
        <thead>
            <tr>
                <td width="10%">&nbsp;</td>
                <td width="20%">Code</td>
                <td width="30%">Name</td>
                <td width="30%">Location</td>
            </tr>
        </thead>
        <tbody>
        
        </tbody>
        </table>
    </div>
</div>
<?php 
echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/bootstrap.min.js').'"></script>';
?>
<script type="text/javascript">
var data_customer = '';
var customer_default_row = '<tr><td><input type="checkbox" name="chk[]" id="chk"/></td><td><input type="hidden" name="buss_id[]" id="buss_id" /><span></span></td><td></td><td></td><td></td></tr>';

function dialog_customer_reset_table(table){
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(customer_default_row);
}
function dialog_customer_populate_data(table,data){ 
	data_customer = data; 
	if(data_customer){
		//delete first row
		if($('#'+table+' > tbody > tr').length != 1){
			$('#'+table+' > tbody > tr:last').remove();
		}
		for(var p = 0; p<data_customer.length; p++){
			var row = clone_row(table);
			row.find('td:eq(1)').find('input[type=hidden]').val(data_customer[p]['buss_id']);
			row.find('td:eq(1)').find('span').text(data_customer[p]['buss_code']);
			row.find('td:eq(2)').text(data_customer[p]['buss_name']);
			row.find('td:eq(3)').text(data_customer[p]['buss_addr']);
		}
		var row = clone_row(table);		
		//delete first and last after insert
		$('#'+table+' > tbody > tr:first').remove();
		$('#'+table+' > tbody > tr:last').remove();
	}else{
		console.log('error');	
	}
}
function dialog_customer_push_data(){
	var result;
	//select process
	var selected = $('#dialog_customer_list > tbody').find('input[name="chk[]"]:checked').parent().parent().find('td:eq(1) > input[type=hidden]').val();
	for( var i = 0; i < source_customer.length; i++ ) { 
		if( source_customer[i]['buss_id'] === selected ) { 
			result = source_customer[i];
			break;
		}
	}
	dialog_customer_pull_data(result);
}

$(document).ready(function(){	
	$(document).on('click','#dialog_customer_list tbody > tr',function(event){
		//SINGLE SELECTION
		if(event.target.type !== 'checkbox'){
			$(':checkbox',this).trigger('click');
		}else{
			if($(this).hasClass('selected')){
				$(this).removeClass('selected');
			}else{
				$(this).siblings().removeClass('selected');
				$(this).siblings().find('td > input:checkbox').prop("checked",false);
				$(this).addClass('selected');
			}
		}		
	});
});
</script>	