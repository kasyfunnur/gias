<section class="content-header">
  	<h1> 
       	<a class="" href="<?php echo site_url('business/partner');?>">Business Partner</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
		Customer List            
    	<a href="<?php echo site_url('business/customer_add');?>">
            <button class="btn btn-flat btn-info pull-right">
                <span class="hidden-xs">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;New Customer
                </span>
                <span class="visible-xs">
                    <i class="fa fa-plus"></i>
                </span>
            </button>
        </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<section class="content">
<div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body table-responsive">
  	<table class="table table-hover table-condensed">
    	<thead>
        	<tr>
                <th>Code</th>
                <th>Name</th>
                <th>Address</th>
                <th  style="text-align:right">Balance</th>
                <th class="hidden-xs">Action</th>
            </tr>
        </thead>
        <tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>

            <tr>
                <td>
                	<a href="<?php echo site_url('business/customer');?>/<?php echo $datatable[$x]['buss_code'];?>">
						<?php echo $datatable[$x]['buss_code'];?>
                    </a>
                </td>
                <td><?php echo $datatable[$x]['buss_name'];?></td>
                <td><?php echo $datatable[$x]['buss_addr']."<br>".$datatable[$x]['buss_city']." ". $datatable[$x]['buss_state'];?>
                </td>                
                <td style="text-align:right">$ xxx </td>           
                <td> 
                	<a href="<?php echo base_url('sales/sales_order_cancel')."/".$datatable[$x]['buss_code'];?>">                    
                    	<span class="btn btn-xs btn-default <?php //echo $disable;?>" >
                        <i class="fa fa-ban"></i> Cancel</span></a>&nbsp;&nbsp;
					<a href="<?php echo base_url('sales/sales_order_close')."/".$datatable[$x]['buss_code'];?>">
                    	<span class="btn btn-xs btn-default <?php //echo $disable;?>" >
                        <i class="fa fa-archive"></i> Close</span></a>&nbsp;&nbsp;
                    <a href="<?php echo base_url('sales/print_so')."/".$datatable[$x]['buss_code'];?>">
                    	<span class="btn btn-xs btn-default" >
                        <i class="fa fa-print"></i> Print</span></a>&nbsp;&nbsp;
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table></div>
    </div>
</section>
<!-- /.content -->