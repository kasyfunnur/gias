<section class="content-header">
  	<h1> Business Partner 
        <button class="btn btn-flat btn-info pull-right">
        	<span class="hidden-xs">
        		<i class="fa fa-plus"></i>&nbsp;&nbsp;New Group
            </span>
        	<span class="visible-xs">
        		<i class="fa fa-plus"></i>
            </span>
        </button>
    </h1>
</section>

<section class="content"> 
<div class="row">
	<!---<div class="col-sm-6">
    	<a href="<?php echo site_url('/business/vendor_list');?>" class="btn btn-danger col-sm-12">Vendor List</a>
    </div>--->
    <div class="col-lg-3 col-xs-6"> 
        <!-- small box -->
        <a href="<?php echo site_url('business/vendor_list'); ?>">
        <div class="small-box bg-red">                
            <div class="inner"><h3> <?php echo $total_vendor; ?> </h3><p> Vendors</p></div>
            <div class="icon"> <i class="ion ion-ios7-people"></i> </div>
            <div class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i> </div>                
        </div>
        </a> 
    </div>
	<!---<div class="col-sm-6">
	    <a href="<?php echo site_url('/business/customer_list');?>" class="btn btn-success col-sm-12">Customer List</a>
    </div>--->
    <div class="col-lg-3 col-xs-6"> 
        <!-- small box -->
        <a href="<?php echo site_url('business/customer_list'); ?>">
        <div class="small-box bg-green">                
            <div class="inner"><h3> <?php echo $total_customer; ?> </h3><p> Customers</p></div>
            <div class="icon"> <i class="ion ion-ios7-people"></i> </div>
            <div class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i> </div>                
        </div>
        </a> 
    </div>
</div>
</section>