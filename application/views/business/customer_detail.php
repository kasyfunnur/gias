<style>
.key{
	vertical-align:top;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
<h1><a class="" href="<?php echo site_url('business/customer_list');?>">Bussiness</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Customer </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<section class="content">
	<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
    <div class="row info">
        <div class="col-sm-6">
            <div class="form-group">
            	<label class="col-sm-3">Customer Info</label>
                <div class="col-sm-4">                
                    <input type="text" class="form-control"
                        name="buss_code" id="buss_code" value="<?php echo $business[0]['buss_code'];?>" readonly="readonly"/>
				</div>
				<div class="col-sm-5">
                    <input type="text" class="form-control"
                        name="buss_name" id="buss_name" value="<?php echo $business[0]['buss_name'];?>"/>
				</div>
			</div>
            <div class="form-group">
            	<label class="col-sm-3">Bussiness Address</label>
                <div class="col-sm-9">
                    <textarea name="buss_addr" class="form-control" id="buss_addr" placeholder="Address.. / Street.."
                        style="width:100%;"><?php echo $business[0]['buss_addr'];?></textarea>
				</div><br>
                <div class="col-sm-3 col-sm-offset-3">
	                <input type="text" class="form-control" name="buss_city" id="buss_city" 
                    	placeholder="City.." value="<?php echo $business[0]['buss_city'];?>">
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="buss_state" id="buss_state" 
                    	placeholder="State.." value="<?php echo $business[0]['buss_state'];?>">
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="buss_country" id="buss_country" 
                    	placeholder="Country.." value="<?php echo $business[0]['buss_country'];?>">
				</div>                
			</div>
            <div class="form-group">
            	<label class="col-sm-3">Bussiness Contact</label>
                <div class="col-sm-4">	                
	                <input type="text" class="form-control" name="buss_tlp1" id="buss_tlp1" 
                    	placeholder="Phone 1" value="<?php echo $business[0]['buss_tlp1'];?>">
                </div>
                <div class="col-sm-5">	                
                    <input type="text" class="form-control" name="buss_tlp2" id="buss_tlp2" 
                    	placeholder="Phone 2" value="<?php echo $business[0]['buss_tlp2'];?>">
                </div>
                <br>
                <div class="col-sm-4 ">
	                <input type="text" class="form-control" name="buss_website" id="buss_website" 
                    	placeholder="Website.." value="<?php echo $business[0]['buss_website'];?>"> 
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="buss_email" id="buss_email" 
                    	placeholder="Email.." value="<?php echo $business[0]['buss_email'];?>">
                </div>
			</div>
            <div class="form-group">
            	<label class="col-sm-3">Balance</label>
                <div class="col-sm-9">
	                <input type="text" class="form-control" name="buss_balance" id="buss_balance" value="0" readonly>
                </div>
			</div>
        </div>
	</div>
    <br />
    <button type="submit" class="btn btn-info pull-right">
        <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
    <br />
    </form>
</section>
<!-- /.content -->
<script>
var return_url = "<?php echo site_url('business/customer_list');?>";
var submit_url = "<?php echo site_url('business/update_query');?>/"+"<?php echo $business[0]['buss_code'];?>";

function validateForm(){
	if(confirm("Are you sure?")==true){
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;	
}
</script>
