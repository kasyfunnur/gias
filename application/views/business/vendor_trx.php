<section class="content-header">
  	<h1> 
	    <a class="" href="<?php echo site_url('business/partner');?>">Business Partner</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
		Vendor Transaction History    
    	<!-- <a href="<?php echo site_url('business/vendor_add');?>">
            <button class="btn btn-flat btn-info pull-right">
                <span class="hidden-xs">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;New Vendor
                </span>
                <span class="visible-xs">
                    <i class="fa fa-plus"></i>
                </span>
            </button>
        </a> -->
    </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<?php //var_dump($datatable);?>
<!-- Main content -->
<section class="content">
<div class="box box-info">
    <div class="box-header">
        <h3><?php echo $buss_info[0]['buss_code']." - ".$buss_info[0]['buss_name']; ?></h3>
    </div>
    <div class="box-body table-responsive">
  	<table class="table table-hover table-condensed">
    	<thead>
        	<tr>
                <th>Doc#</th>
                <th>Date</th>
                <th>Status</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
        	<?php foreach($datatable as $x=>$data){ ?>
            <tr>
                <td><?php echo $data['doc_num'] ?></td>
                <td><?php echo $data['doc_dt'] ?></td>
                <td><?php echo $data['doc_status'] ?></td>
                <td><?php echo $data['doc_total'] ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table></div>
    </div>
</section>
<!-- /.content -->