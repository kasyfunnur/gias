<section class="content-header">
    <h1> 
        <a class="" href="<?php echo site_url('business/partner');?>">Business Partner</a> 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Customer List            
        <a href="<?php echo site_url('business/customer_add');?>">
            <button class="btn btn-flat btn-info pull-right">
                <span class="hidden-xs">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;New Customer
                </span>
                <span class="visible-xs">
                    <i class="fa fa-plus"></i>
                </span>
            </button>
        </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg: ""?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header"></div>
        <div class="box-body table-responsive">
            <table class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th class="hidden-xs">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($x=0;$x<count($datatable);$x++){?>
                    <tr>
                        <td>
                            <a href="<?php echo site_url('business/customer');?>/<?php echo $datatable[$x]['buss_code'];?>">
                                <?php echo $datatable[$x][ 'buss_code'];?>
                            </a>
                        </td>
                        <td>
                            <?php echo $datatable[$x][ 'buss_name'];?>
                        </td>
                        <td>
                            <?php echo $datatable[$x][ 'buss_addr']. "<br>".$datatable[$x][ 'buss_city']. " ". $datatable[$x][ 'buss_state'];?>
                        </td>
                        <td>
                            <?php $link_trx=base_url('business/customer_trx'). "/".$datatable[$x][ 'buss_code']; ?>
                            <a href="<?php echo $link_trx;?>" class="btn btn-sm btn-danger">
                                <i class="fa fa-archive"></i> Trx History</a>&nbsp;&nbsp;
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<!-- /.content -->
