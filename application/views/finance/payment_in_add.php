<!-- Content Header (Page header) -->
<style>
	.inactive td{background-color:#CCC;opacity: 0.5;}
	thead>tr>td{text-align:center;font-weight:bold;}
	/*thead>tr>td>input{text-overflow: ellipsis; white-space: nowrap; overflow: hidden;}*/
	/*thead>tr>td{ text-align:right; }*/
	#foot_label{text-align:right;}
	label{		
		-moz-user-select: -moz-none;
		-khtml-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}	
</style>
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('finance/payment_in');?>">Payment In</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New </h1>
</section>

<!-- Main content -->
<section class="content">
	<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>	    
    <div class="row">
    	<div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-3" for="buss_name">
                	Payment From                    
                </label>
                <div class="col-sm-9">
                	<input type="hidden" id="buss_id" name="buss_id" value="">
                    <input type="hidden" id="buss_char" name="buss_char" value="">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="" readonly>                        
                    	<span class="input-group-btn">
							<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_vendor" style="width:100%;">
                            	<i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
						</span>
                    </div>                    
                </div>
            </div>
            <!---<div class="form-group">
                <label class="col-sm-3" for="location_id">Location</label>
                <div class="col-sm-9">
                    <?php //echo sw_CreateSelect('location_id', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>--->
            
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                </div>
            </div>	
        	<div class="form-group">
                <label class="col-sm-3" for="head_doc_dt">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="head_doc_dt" name="head_doc_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>		
            <div class="form-group">
                <label class="col-sm-3" for="head_doc_ddt">Posting Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="head_doc_ddt" name="head_doc_ddt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-sm-12">
        	<table class="table-bordered" id="table_detail">
            <thead>
            <tr>
            	<td width="4%">#</td>
                <td width="10%">Doc#</td>
                <td width="10%">Date</td>
                <td width="8%">Curr</td>
                <td width="15%">Sub Total</td>
                <td width="15%">Total</td>
                <td width="15%">Paid Amount</td>
                <td width="20%">To Pay</td>
                <td width="3%">-</td>
            </tr>
            </thead>
            <tbody>
            <tr height="30px" id="default">
            	<td style="text-align:center"><input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/></td>
                <td>
                	<input type="hidden" name="doc_type[]" id="doc_type1" class="form-control">
                	<input type="hidden" name="doc_id[]" id="doc_id1" class="form-control">
                	<input type="text" name="doc_num[]" id="doc_num1" class="form-control" readonly="readonly">
                </td>
                <td><input type="text" name="doc_dt[]" id="doc_dt1" class="form-control" readonly="readonly"></td>
                <td><input type="text" name="doc_curr[]" id="doc_curr1" class="form-control" readonly="readonly" /></td>
                <td>
                	<input type="number" name="doc_subtotal[]" id="doc_subtotal1" class="form-control" style="text-align:right" readonly="readonly">
				</td>
                <td>
                	<input type="number" name="doc_total[]" id="doc_total1" class="form-control" style="text-align:right" readonly="readonly">
				</td>
                <td><input type="number" name="doc_paid[]" id="doc_paid1" class="form-control" style="text-align:right" readonly="readonly"></td>
                <td><input type="number" name="to_pay[]" id="to_pay1" class="form-control" style="text-align:right"></td>
                <td>
                	<div style='text-align:center'>
						<div class='btn-group'>
                            <button type="button" class="btn btn-flat btn-xs btn-danger" id="act_full1" name="act_full">FULL</button>
						</div>
					</div>
				</td>
            </tr>
            </tbody>
            <tfoot>
            	<tr>
                	<td colspan="7" style="text-align:right;"> Total : </td>
                    <td><input type="number" name="total_payment" id="total_payment" class="form-control" style="text-align:right" /></td>
                </tr>
                <tr>
                	<td colspan="7" style="text-align:right;"> To Receive : </td>
                    <td>
                    <div class="input-group">                                
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success" >
                                    <i class="fa fa-money"></i>&nbsp;&nbsp; IDR</button>                                
                            </span>
                            <input type="number" class="form-control" id="payment" name="payment" value="0" readonly="readonly"
                                style="text-align:right;">
                            <!---<span class="input-group-btn">
                                <button type="button" class="btn btn-success" >
                                    <i class="fa fa-money"></i>&nbsp;&nbsp; IDR</button>                                
                            </span>--->
                        </div>
                    </td>
                </tr>
            </tfoot>
            </table>
    	</div>
    </div>
    <hr />    
    <br />
    <div class="row">
        <div class="col-sm-3 col-sm-offset-9">
        <button type="button" class="btn btn-info" data-toggle="modal" href="#modal_pay" >
            <i class="fa fa-dollar"></i>&nbsp;&nbsp; Payment Detail
        </button>&nbsp;
        <button type="submit" class="btn btn-info">
            <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
        </button> 
        </div>
        <input type="hidden" name="head_bank_amt" id="head_bank_amt" />
        <input type="hidden" name="head_cash_amt" id="head_cash_amt" />
    </div>
	</form>
</section>
<div class="modal fade" id="modal_vendor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog" >
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title">Select Vendor
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" 
                        	onclick="dialog_customer_push_data();">Save & Close</button>  
                    </div>
                </h4>                
            </div>
            <div class="modal-body">
				<?php 					
					//$this->load->view('business/dialog_vendor',$dialog_vendor);
					$this->load->view('business/dialog_customer');
				?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" 
                	onclick="dialog_customer_push_data();">Save & Close</button>                
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_pay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title">Make Payment
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" 
                        	onclick="dialog_make_payment_push_data();">Save & Close</button>
                    </div>
                </h4>                
            </div>
            <div class="modal-body">
				<?php 
					//$data_dialog_po['dialog_po'] = $dialog_po;
					//$this->load->view('purchase/dialog_purchase_order',$data_dialog_po);	
					$this->load->view('finance/dialog_make_payment');	
				?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" 
                	onclick="dialog_make_payment_push_data();">Save & Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /.content -->
<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
var source_customer = <?php echo json_encode($dialog_customer);?>;

var return_url = "<?php echo site_url('finance/payment_in');?>";
var submit_url = "<?php echo site_url('finance/payment_in_add_query');?>";
var ajax_url_1 = "<?php echo base_url('finance/ajax_get_ar_with_customer');?>";
var ajax_url_2 = "<?php echo base_url('sales/ajax_getDetail_so_open');?>";
var ajax_url_3 = "<?php echo base_url('sales/ajax_getHeader_so');?>";

var param = "<?php echo $param;?>";
var buss = "<?php echo $buss;?>";
var buss_name = "<?php echo $buss_name;?>";
</script>