<?php 
echo '<script src="'.base_url('assets/js/comp/status.js').'"></script>';
?>
<div class="row">
	<div class="col-sm-12 tabbable" id="myTab">
    	<ul class="nav nav-tabs nav-justified">
            <li class="active">
            	<a href="#pane1">Bank Transfer</a></li>
  			<li><a href="#pane2">Cash</a></li>
        </ul>
        <div class="tab-content">
	      	<div class="tab-pane active" id="pane1"><br />
                <div class="row">
	                <div class="col-sm-6 col-sm-offset-3">
	                <table style="width:90%;">
	                	<tr>
	                    	<td>Bank</td>
	                        <td>&nbsp;:&nbsp;</td>
	                        <td><input class="form-control" type="text" name="bank_code" id="bank_code" /></td>	                        
	                    </tr>
	                	<tr>
	                    	<td>Amount</td>
	                        <td>&nbsp;:&nbsp;</td>
	                        <td><input class="form-control" type="number" name="bank_amt" id="bank_amt" value="0" class="form-control" style="text-align:right;"/></td>
	                        <td><input class="form-control btn btn-warning" type="button" name="btn_copy" id="btn_copy" value="Copy"/></td>
	                    </tr>
	                    <tr>
	                    	<td>Ref</td>
	                        <td>&nbsp;:&nbsp;</td>
	                        <td><input class="form-control" type="number" name="bank_ref" id="bank_ref"/></td>
	                    </tr>
	                </table>
	                </div>
                </div>
        	</div>
            <div class="tab-pane" id="pane2"><br />
	        	<div class="row">
	                <div class="col-sm-6 col-sm-offset-3">
	                <table style="width:90%;">
	                	<tr>
	                    	<td>Cash</td>
	                        <td>&nbsp;:&nbsp;</td>
	                        <td><input class="form-control" type="text" name="cash_code" /></td>	                        
	                    </tr>
	                	<tr>
	                    	<td>Amount</td>
	                        <td>&nbsp;:&nbsp;</td>
	                        <td><input class="form-control" type="number" name="cash_amt" id="cash_amt" value="0" class="form-control" style="text-align:right;"/></td>
	                        <td><input class="form-control btn btn-warning" type="button" name="btn_copy" id="btn_copy" value="Copy"/></td>
	                    </tr>
	                </table>
	                </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<br />
<hr />
<br />
<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <table style="width:90%;">
            <tr>
                <td>Total</td>
                <td>&nbsp;:&nbsp;</td>
                <td>
                	<input type="hidden" name="amt_total" id="amt_total" />
                	<div id="amt_total" class="form-control" style="text-align:right;background-color:#eee;" />
                </td>
            </tr>
            <tr>
                <td>Payment</td>
                <td>&nbsp;:&nbsp;</td>
                <td><input type="number" name="amt_payment" id="amt_payment" class="form-control" style="text-align:right;" readonly="readonly"/></td>
            </tr>
            <tr>
                <td>Balance</td>
                <td>&nbsp;:&nbsp;</td>
                <td><input type="number" name="amt_balance" id="amt_balance" class="form-control" style="text-align:right;" readonly="readonly"/></td>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
/*---
1.reset_table(table)
2.populate_table(table)
3.dialog_purchase_order_push_data
---*/
//if dialog type = table, default row is required
/*var data_po = '';
var po_default_row = '<tr><td><input type="checkbox" name="chk[]" id="chk"/></td><td><input type="hidden" name="doc_id[]" id="doc_id" /><span></span></td><td></td><td></td><td></td><td></td></tr>';
function dialog_purchase_order_reset_table(table){
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(po_default_row);
}
function dialog_purchase_order_populate_data(table,data){ 
	data_po = data;
	if(data_po){
		//delete first row
		if($('#'+table+' > tbody > tr').length != 1){
			$('#'+table+' > tbody > tr:last').remove();
		}
		for(var p = 0; p<data_po.length; p++){
			var row = clone_row(table);
			row.find('td:eq(1)').find('input[type=hidden]').val(data_po[p]['doc_id']);
			row.find('td:eq(1)').find('span').text(data_po[p]['doc_num']);
			row.find('td:eq(2)').text(data_po[p]['buss_name']);
			row.find('td:eq(3)').text(data_po[p]['doc_dt']);
			row.find('td:eq(4)').text(data_po[p]['location_name']);
			row.find('td:eq(5)').html(status_label('logistic',data_po[p]['logistic_status']));
		}
		var row = clone_row(table);
		
		//delete first and last after insert
		$('#'+table+' > tbody > tr:first').remove();
		$('#'+table+' > tbody > tr:last').remove();
	}else{
		console.log('error');	
	}
}*/
function dialog_make_payment_push_data(){
	var result = {};
	result.total_payment = $("#amt_payment").val();
	result.bank_amt = $("#bank_amt").val();
	result.cash_amt = $("#cash_amt").val();
	dialog_make_payment_pull_data(result);
}


$(document).ready(function(){		
	/*$(document).on('click','#dialog_purchase_order_list tbody > tr',function(event){
		//SINGLE SELECTION
		if(event.target.type !== 'checkbox'){
			$(':checkbox',this).trigger('click');
		}else{
			if($(this).hasClass('selected')){
				$(this).removeClass('selected');
			}else{
				$(this).siblings().removeClass('selected');
				$(this).siblings().find('td > input:checkbox').prop("checked",false);
				$(this).addClass('selected');
			}
		}		
	});*/
	$("#bank_amt").change();
	
	//FUNCTION TO CALCULATE TOTAL - PAYMENT = BALANCE AND 
	//	CALCULATE TOTAL PAYMENT FROM BANK AND CASH ACCOUNT
	$("#bank_amt,#cash_amt").on('change',function(event){		
		if(event.target.value == "")$(this).val(0);
		$("#amt_payment").val(parseInt($("#bank_amt").val())+parseInt($("#cash_amt").val()));
		$("#amt_balance").val(parseInt($("#amt_total").val())-parseInt($("#amt_payment").val()));
		if($("#amt_payment").val() == "")$("#amt_payment").val(0);
		if($("#amt_balance").val() == "")$("#amt_balance").val(0);
	});
	$("#btn_copy").on('click',function(){
		$("#bank_amt").val($("#amt_balance").val()).change();
	});
	
});
</script>