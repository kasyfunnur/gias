<!-- Content Header (Page header) -->
<style>
	/*thead>tr>th{text-align:center;font-weight:bold}*/
</style>
<section class="content-header">
  <h1> Outgoing Payment List<a href="<?php echo site_url('finance/payment_out_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<i class="fa fa-plus"></i>&nbsp;&nbsp;New Payment
		</button>
    </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<section class="content">
<div class="box box-info">
    <div class="box-header">
    </div>
    <div class="box-body table-responsive">
  	<table class="table table-hover">
    	<thead>
        	<tr>
            	<th>#</th>
                <th>Doc Num</th>
                <!---<th>Doc Ref</th>	--->
                <th>Date</th>
                <th>Payer/Payee</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>
			<?php 
                //$logistic_status = $this->status_model->status_label('logistic',$datatable[$x]['logistic_status']);
				//$finance_status = $this->status_model->status_label('financial',$datatable[$x]['financial_status']);
            ?>
            <tr>
            	<td><?php echo $x+1;?>.</td>
                <td>
                	<a href="<?php echo site_url('finance/payment_out_edit');?>/<?php echo $datatable[$x]['doc_num'];?>">
						<?php echo $datatable[$x]['doc_num'];?>
                    </a>
                </td>
                <!---<td><?php echo $datatable[$x]['doc_ref'];?></td>--->
                <td><?php echo $datatable[$x]['doc_dt'];?></td>
                <td><?php echo $datatable[$x]['buss_name'];?></td>
                <!---<td><?php echo $finance_status;?></td>--->
                <td>
                	<a href="#">
                    	<span class="btn btn-xs btn-default " >
                        <i class="fa fa-ban"></i> Cancel</span></a>&nbsp;&nbsp;
					<!---<a href="#">
                    	<span class="btn btn-xs btn-default " >
                        <i class="fa fa-archive"></i> Close</span></a>&nbsp;&nbsp;--->
                    <a href="#">
                    	<span class="btn btn-xs btn-default" >
                        <i class="fa fa-print"></i> Print</span></a>&nbsp;&nbsp;
                    
                	<!---<a href="<?php //echo $link_payment;?>">
                    	<span class="btn btn-xs btn-default">Payment</span></a>&nbsp;&nbsp;--->
                    <!---<a href="<?php echo site_url('finance/payment_out_add')."/".$datatable[$x]['doc_num'];?>">
                    	<span class="btn btn-xs btn-default">Payment</span></a>--->
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table></div>
    </div>
</section>
<!-- /.content -->