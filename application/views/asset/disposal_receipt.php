<section class="content-header">
<h1><a class="" href="<?php echo site_url('asset/non_financial_trans');?>">Non Financial Transaction</a>
  &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
  Receipt
  </h1>
</section>
<?php
		$attributes = array(
			'class' 	=> 'form-horizontal',
			'role'		=> 'form',
			'method' 	=> 'post'
			);
		echo form_open('', $attributes);
		?>
<section class="content"><?php echo $msg?>
  <div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body table-responsive">
      <dl class="dl-horizontal">
        <dt>Document Reference</dt>
        <dd><?php echo isset($doc_array[0]['document_ref'])?$doc_array[0]['document_ref']: '-'?></dd>
        <dt>Transaction Type</dt>
        <dd>Moving</dd>
        <dt>Requested By</dt>
        <dd><?php echo (isset($doc_array[0]['user_name'])?$doc_array[0]['user_name']:'-').
		(isset($doc_array[0]['full_name'])?" (".$doc_array[0]['full_name'].")":' - ')?></dd>
        <dt>Document Date</dt>
        <dd><?php echo (isset($doc_array[0]['timestamp']))? date('d/m/y h:i:s',strtotime($doc_array[0]['timestamp'])):'-'?></dd>
        <dt>Approved By</dt>
        <dd><?php echo (isset($doc_array[0]['user_name'])?$doc_array[0]['user_name']:'-').
		(isset($doc_array[0]['full_name'])?" (".$doc_array[0]['full_name'].")":'-')?></dd>
      </dl>
      <table class="table table-hover" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th><input type="checkbox" onClick="toggle(this)" /></th>
            <th>Label</th>
            <th>Name</th>
            <th>Status</th>
          </tr>
          <?php foreach ($doc_array as $row) {
			  if($row['status'] == 'in transit')$status = '<span class="label label-warning">IN TRANSIT</span>';
			  else if($row['status'] == 'received')$status = '<span class="label label-success">RECEIVED</span>';
			  else if($row['status'] == 'disposed')$status = '<span class="label label-danger">DISPOSED</span>';
			  else if($row['status'] == 'pending')$status = '<span class="label label-warning">PENDING</span>';
			  ?>
          <tr>
            <td><input type='checkbox' name='assets_received[]' value="<?php echo $row['asset_id']?>" <?php echo ($row['status']=='disposed') ? "disabled>" : ">" ?></td>
            <td><?php echo "<a href='../asset_view/$row[asset_label]'>$row[asset_label]</a>" ?>
              </th>
            <td><?php echo $row['asset_name']?>
              </th>
            <td><?php echo isset($status)?$status:" - "?> 
              </th>
          </tr>
          <?php } ?>
        <input type="hidden" name="request_id" value="<?php echo $doc_array[0]['request_id'] ?>">
          </thead>
      </table>
      <div class="row">
        <div class="col-sm-3 pull-right" style="padding-top:20px">
          <div class="input-group">
            <input type="text" class="form-control date-mask" name="date" value"<?php echo set_value('date',date('d/m/Y'))?>">
            <span class="input-group-btn">
            <button class="btn btn-primary btn-flat pull-right" type="submit">Disposed</button>
            </span> </div>
          <!-- /input-group --> 
        </div>
        <!-- /.col-lg-6 --> 
      </div>
    </div>
  </div>
</section>
</form>
<script>
function toggle(source) {
  checkboxes = document.getElementsByName('assets_received[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>