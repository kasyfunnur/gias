<section class="content-header">	
  <h1>Asset Tracking
  	<a href="<?php echo site_url('asset/new_asset');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<i class="fa fa-plus"></i>&nbsp;&nbsp;Advance Search
		</button>
    </a>
  </h1>
</section>
<section class="content">
<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
?>
<?php echo $msg?>
<div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body table-responsive">
      <!---<table id="asset_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Serial Number</th>
                <th>Name</th>
                <th>Group</th>
                <th>Office</th>
                <th>Value</th>
                <th>Status</th>
            </tr>
        </thead>
      </table>--->
      	<div class="row">
        	<div class="col-sm-2">
	      		Serial Number : <!---<input type="text" id="search_serial" />--->
      		</div>
            <div class="col-sm-4">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control input-sm" id="search_serial" name="search_serial" value="">
                    <span class="input-group-btn">
                        <!---<button type="button" class="btn" id="btn_search_serial" 
                            onclick="javascript:alert('tess dulu');">Search</button>--->
						<button type="submit" class="btn">Search</button>
                    </span>                        
                </div> 
			</div>
		</div>	
        <?php 
			//var_dump($asset_info);
			//var_dump(isset($_POST['search_serial'])?$asset_id:"");
		?>
        <div class="row">
        	<div class="col-sm-12">
            	<h3><?php echo $asset_info['asset_serial'];?></h3>
            </div>
		</div>
        <div class="row">
        	<div class="col-sm-12">
            	<table id="asset_history_table" class="table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Group</th>
                            <th>Name</th>
                            <th>Location</th>
                            <th>Status</th>
                            <th>User</th>                     
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Date</th>
                            <th>Group</th>
                            <th>Name</th>
                            <th>Location</th>
                            <th>Status</th>
                            <th>User</th>                     
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
  </div>
</section>


<script src="<?php echo base_url('assets/js/jquery-2.0.3.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/admin_app.js');?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.js');?>"></script>
<script src="<?php echo base_url('assets/js/dataTables.bootstrap.js');?>"></script>
<script>
function format (d) {
    return 'data: s '+d;
}
(function($){
$(document).ready(function() {
    var dt = $('#asset_history_table').dataTable({
        'processing': true,
        'serverSide': true,
		//'ajax': '<?php echo base_url('asset/ajax_asset_history');?>/json/<?php echo $asset_id;?>'
		'ajax' : 'asset/asset_sample.php'
    });
});
}(jQuery));
</script>