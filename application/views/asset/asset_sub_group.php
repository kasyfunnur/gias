<section class="content-header">
  <h1><a href="<?php echo site_url('asset/asset_group');?>">Asset Group</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp; <?php echo $data_asset_group['group_name']." [".$data_asset_group['group_tag']."]"?>
  	<a href="<?php echo site_url('asset/new_group');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<i class="fa fa-plus"></i>&nbsp;&nbsp;Add New
		</button>
    </a>
  </h1>
</section>
<style>
td.details-control {
    background: url('../resources/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.details td.details-control {
    background: url('../resources/details_close.png') no-repeat center center;
}
</style>
<section class="content">
<?php echo $msg?>
<form name="frm">
<div class="box box-info">
    <div class="box-header">
    </div>
    <div class="box-body table-responsive">
      <table id="sub_group_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Group ID</th>
                <th>Name</th>
                <th>Tag Code</th>
                <th>Description</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Group ID</th>
                <th>Name</th>
                <th>Tag Code</th>
                <th>Description</th>
            </tr>
        </tfoot>
    </table>
    </div>
  </div>
</form>
</section>

<script src="<?php echo base_url('assets/js/jquery-2.0.3.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/admin_app.js');?>"></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.js');?>"></script>
<script src="<?php echo base_url('assets/js/dataTables.bootstrap.js');?>"></script>

<script>
/*function format (d) {
    return 'data: s '+d;
}*/
var postData = {
	'group_id' : '<?php echo $data_asset_group["group_id"];?>'
};

(function($){
$(document).ready(function() {	
    var dt = $('#sub_group_list').DataTable({
		'bProcessing': true,
        'bServerSide': true,
        'processing': true,
        'serverSide': true,
        'ajax': '<?php echo base_url('asset/ajax_sub_group_list');?>/json/<?php echo $data_asset_group["group_id"];?>',
		//'data': postData
		'fnRowCallback': function( nRow, aData, iDisplayIndex ) {
		  	$('td:eq(1)', nRow).html('<a href="<?php echo base_url('asset/asset_sub_group_edit');?>/'+aData[0]+'">'+aData[1]+'</a>');
		  	return nRow;
		}
    });
	
    // Array to track the ids of the details displayed rows
    var detailRows = [];
	/*$('#sub_group_list tbody').on( 'click', 'tr', function () {
		var tr = $(this);
        var row = dt.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
 		tr.addClass( 'details' );
        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide(); 			
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show(); 
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    });*/
});
}(jQuery));
</script>