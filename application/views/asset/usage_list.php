<section class="content-header">	
  <h1>Asset Usage Request
  	<a href="<?php echo site_url('asset/usage_request');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<i class="fa fa-plus"></i>&nbsp;&nbsp;Add New
		</button>
    </a>
  </h1>
</section>
<section class="content">
<?php echo $msg?>
<div class="box box-info">
    <div class="box-header">
    </div>
    <div class="box-body table-responsive">
   <div class="row" style="padding-bottom:10px">
        <div class="col-xs-3">
          <select class="form-control" name="show" id="show" >
            <option value="0" selected>Everyone</option>
            <option value="1">Mine</option>
          </select>
       </div>
       </div>
      <table id="usage_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Reference</th>
                <th>By</th>
                <th>Date</th>
                <th>Asset Name</th>
                <th>Asset Serial</th>
                <th>Note</th>
                <th>Status</th>
            </tr>
        </thead>
    </table>
    </div>
  </div>
</section>