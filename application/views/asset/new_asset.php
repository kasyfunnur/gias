<section class="content-header">
  <h1><a class="" href="<?php echo site_url('asset/asset_list');?>">Asset List</a>
  &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
  New Asset
  </h1>
</section>
<section class="content">
<?php echo $msg?>
<?php echo validation_errors()?>
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">New Asset Form</h3>
    </div>
    <div class="box-body">
      <?php
		$attributes = array(
			'class' 	=> 'form-horizontal',
			'role'		=> 'form',
			'method' 	=> 'post', 
			'name'		=> 'new_asset_form', 
			'id' 		=> 'new_asset_form'
			);
		echo form_open('', $attributes);
		?>
      <div class="form-group <?php echo (!form_error('location_id')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Location</label>
        <div class="col-sm-8">
          <select class="form-control" id="location_id" name="location_id" >
            <?php foreach($site_location as $location)
			echo "<option value='$location[location_id]' ".set_select('location_id', "$location[location_id]").">$location[location_name]</option>";?>
          </select>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('group_id')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Group</label>
        <div class="col-sm-8">
          <select class="form-control" id="group_id" name="group_id">
            <?php foreach($group_array as $group_row)
			echo "<option value='$group_row[group_id]' ".set_select('group_id', "$group_row[group_id]").">$group_row[group_name]</option>";?>
          </select>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('serial')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Serial Label</label>
        <div class="col-sm-8">
          <div class="input-group">
            <input type="text" class="form-control" name="serial" value="<?php echo set_value('serial')?>">
            <span class="input-group-btn"><a class="btn btn-warning btn-flat disabled" type="button" href="#">Generate</a></span></div>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('name')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Name</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="name" value="<?php echo set_value('name')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('description')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Description</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="description" value="<?php echo set_value('description')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('date_acquired')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Acquisition Date</label>
        <div class="col-sm-8">
          <input type="date" class="form-control" id="date_acquired" name="date_acquired" value="<?php echo ( isset($date_acquired) ? set_value('date_acquired') : date('Y-m-d') ) ;?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('note')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Note</label>
        <div class="col-sm-8">
          <textarea class="form-control" type="text" name="note"><?php echo set_value('note')?></textarea>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('value')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Value</label>
        <div class="col-sm-8">
          <div class="input-group">
              <span class="input-group-btn">
                <?php echo currency_select('IDR',array('name'=>'asset_curr','class'=>'btn btn-default')); ?>
              </span>
              <input class="form-control" type="text" name="value" value="<?php echo set_value('value')?>">
          </div>
        </div>
      </div>

      <div class="form-group <?php echo (!form_error('value')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Lifetime</label>
        <div class="col-sm-8">
          <div class="input-group"> <span class="input-group-addon">Months</span>
            <input class="form-control" type="text" name="lifetime" value="<?php echo set_value('lifetime')?>">
          </div>
        </div>
      </div>

      <div class="form-group <?php echo (!form_error('value')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Salvage Value</label>
        <div class="col-sm-8">
          <div class="input-group"> <span class="input-group-addon">Rp.</span>
            <input class="form-control" type="text" name="salvage_value" value="<?php echo set_value('value')?>">
          </div>
        </div>
      </div>

      
      <?php $group_id=1;
	  foreach ($group_array as $row){
		  ?>
      <div id="custom_group_<?php echo $group_id?>" style="display:none">
        <div class="box-header">
          <h4 class="box-title"><?php echo $row ['group_name']?></h4>
        </div>
        <?php 
		  $no_field = 1; //flag to show there was no field found in this group
		  for ($i=1; $i<=10; $i++){
			  if($row["custom_field_$i"]!=NULL){
				  $no_field = 0;
				  ?>
        <div class="form-group <?php echo (!form_error('custom_field[$group_id][$i]')) ? '' : 'has-error' ?>">
          <label class="col-sm-4 control-label"><?php echo $row["custom_field_$i"]?></label>
          <div class="col-sm-8">
            <input class="form-control" type="text" name="custom_field[<?php echo $group_id?>][<?php echo $i?>]" 
            value="<?php echo set_value("custom_field[$group_id][$i]")?>">
          </div>
        </div>
        <?php
				  }
			  }
			if($no_field == 1){ ?>
        <div class="form-group">
          <label class="col-sm-4 control-label"><i class="pull">No Custom Fields Found</i></label>
        </div>
        <?php
    		}
		  ?>
      </div>
      <?php 
		 $group_id++;
		 } ?>
      <div class="form-group">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary btn-flat pull-right">Add New</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</section>
