<section class="content-header">
  <h1> <a href="<?php echo site_url('asset/asset_group');?>">Asset Group</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i> &nbsp;&nbsp;<?php echo $group_info['group_name']?> </h1>
</section>
<section class="content"> <?php echo $msg?> <?php echo validation_errors()?>
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Asset Group Form</h3>
    </div>
    <div class="box-body">
      <?php
		$attributes = array(
			'class' 	=> 'form-horizontal',
			'role'		=> 'form',
			'method' 	=> 'post', 
			'name'		=> 'new_group_form', 
			'id' 		=> 'new_group_form'
			);
		echo form_open('', $attributes);
		?>
      <input type="hidden" name="group_id" id="group_id" value="<?php echo $group_info['group_id'];?>" />
      <div class="form-group <?php echo (!form_error('name')) ? '' : 'has-error' ?>">
        <label class="col-sm-2 control-label" for="name">Group Name</label>
        <div class="col-sm-10">
          <input class="form-control" type="text" name="name" id="name" maxlength="255" value="<?php echo set_value('name',$group_info['group_name'])?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('description')) ? '' : 'has-error' ?>">
        <label class="col-sm-2 control-label" for="description">Description</label>
        <div class="col-sm-10">
          <textarea class="form-control" type="text" maxlength="1000" name="description" id="description"><?php echo set_value('description',$group_info['group_description'])?></textarea>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('tag')) ? '' : 'has-error' ?>">
        <label class="col-sm-2 control-label" for="tag">Tag</label>
        <div class="col-sm-10">
          <input class="form-control" type="text" maxlength="1000" name="group_tag" id="group_tag" value="<?php echo set_value('group_tag',$tag)?>" />
        </div>
      </div>
      <?php for ($i=1;$i<=10;$i++){ ?>
      <div class="form-group <?php echo (!form_error("custom_field[$i]")) ? '' : 'has-error' ?>">
        <label class="col-sm-2 control-label">Custom Field <?php echo $i?></label>
        <div class="col-sm-4">
          <input class="form-control" type="text" max="255" value="<?php echo set_value("custom_field[$i]",$group_info['custom_field_'.$i]);?>" name="custom_field[<?php echo $i?>]">
        </div>
        <label class="col-sm-2 control-label" for="custom_field_type[<?php echo $i?>]">Type</label>
        <?php //echo $group_info['custom_type_'.$i];?>
        <div class="col-sm-4">
          <select class="form-control" type="text" name="custom_field_type[<?php echo $i?>]" id="custom_field_type[<?php echo $i?>]">
            <option value="1" <?php set_select("custom_field_type[$i]","1")?><?php if ($group_info['custom_type_'.$i] == 'varchar'){echo "selected";} ?>>Varchar</option>
            <option value="2" <?php set_select("custom_field_type[$i]","2")?><?php if ($group_info['custom_type_'.$i] == 'datetime'){echo "selected";} ?>>Date</option>
            <option value="3" <?php set_select("custom_field_type[$i]","3")?><?php if ($group_info['custom_type_'.$i] == 'boolean'){echo "selected";} ?>>True/False</option>
            <option value="4" <?php set_select("custom_field_type[$i]","4")?><?php if ($group_info['custom_type_'.$i] == 'decimal'){echo "selected";} ?>>Decimal</option>
          </select>
        </div>
      </div>
      <?php } ?>

      <!-- link account setting for this group -->
      <div class="form-group">
          <label class="col-sm-4 control-label" for="description">Asset Cost Account</label>
          <div class="col-sm-8">
              <select name="link_acc_cost" class="form-control">
                <?php foreach ($coa as $c) {
                  $selected = $c['account_id'] == $group_info['acc_id_cost'] ? 'selected' : '';
                  echo '<option value="' . $c['account_id'] . '" '.$selected.'>' . $c['account_number'] . ' - ' . $c['account_name'].'</option>';
                } ?>
              </select>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 control-label" for="description">Asset Accumulated Depreciation Account</label>
          <div class="col-sm-8">
              <select name="link_acc_accdepre" class="form-control">
                <?php foreach ($coa as $c) {
                  $selected = $c['account_id'] == $group_info['acc_id_accdepre'] ? 'selected' : '';
                  echo '<option value="' . $c['account_id'] . '" '.$selected.'>' . $c['account_number'] . ' - ' . $c['account_name'].'</option>';
                } ?>
              </select>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 control-label" for="description">Asset Depreciation Expense Account</label>
          <div class="col-sm-8">
              <select name="link_acc_depre" class="form-control">
                <?php foreach ($coa as $c) {
                  $selected = $c['account_id'] == $group_info['acc_id_depre'] ? 'selected' : '';
                  echo '<option value="' . $c['account_id'] . '" '.$selected.'>' . $c['account_number'] . ' - ' . $c['account_name'].'</option>';
                } ?>
              </select>
          </div>
      </div>


      <div class="form-group">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary btn-flat pull-right">Save</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</section>
