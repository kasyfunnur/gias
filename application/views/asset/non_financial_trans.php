<section class="content-header">
  <h1>Non Financial Transaction
  <div class="btn-group pull-right">
  <a href="<?php echo site_url('asset/disposal_request');?>" class="btn btn-flat btn-danger pull-right">
        	<i class="fa fa-plus"></i>&nbsp;&nbsp;Disposal Request
    </a>
    <a href="<?php echo site_url('asset/move_request');?>" class="btn btn-flat btn-info pull-right">
        	<i class="fa fa-plus"></i>&nbsp;&nbsp;Move Request
    </a>
    </div>
    </h1>
</section>
<section class="content"><?php echo $msg?>
  <div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body table-responsive">
      <table id="non_financial_trans" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Document Ref</th>
            <th>Requested By</th>
            <th>Type</th>
            <th>Date</th>
            <th>From</th>
            <th>To</th>
            <th>Status</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</section>