<section class="content-header">	
  <h1>Asset Usage Monitoring
  </h1>
</section>
<section class="content">
<?php echo $msg?>
<div class="box box-info">
    <div class="box-header">
    </div>
    <div class="box-body table-responsive">
      <table id="usage_monitor" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Reference</th>
                <th>By</th>
                <th>Date</th>
                <th>Asset Name</th>
                <th>Asset Serial</th>
                <th>Note</th>
                <th>Status</th>
            </tr>
        </thead>
    </table>
    </div>
  </div>
</section>