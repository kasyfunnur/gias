<?php 
$ref_no = get_ref_number('asset_request');
$full_name = trim($this -> session -> userdata('full_name'));
$display_name = ($full_name != '' || $full_name) ? "(".$full_name.")" :'';
$today_date = date('d/m/Y');
?>
<section class="content-header">
  <h1><a class="" href="<?php echo site_url('asset/usage_list');?>">Asset Usage Request</a>
  &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
  New Request
  </h1>
</section>
<section class="content"> <?php echo $msg?> <?php echo validation_errors()?>
  <div class="box box-primary">
    <div class="box-header">
    <span class="pull-right" style="padding-right:10px">
    Location : <?php echo $site_location['location_name']?><span style="padding-right:10px"></span>
    REF NO. <?php echo substr($ref_no,0,2).' / '.substr($ref_no,2,6).' / '.substr($ref_no,8,14)?></span>
    </div>
    <div class="box-body">
      <?php
		$attributes = array(
			'class' 	=> 'form-horizontal',
			'role'		=> 'form',
			'method' 	=> 'post', 
			'name'		=> 'usage_request_form', 
			'id' 		=> 'usage_request_form'
			);
		echo form_open('', $attributes);
		?>
      <div class="form-group col-sm-6">
        <label class="col-sm-4 control-label">Requested By</label>
        <div class="col-sm-8">
          <p class="form-control-static"><?php echo ucfirst($this -> session ->userdata ('user_name')) . ' '.$display_name?></p>
        </div>
      </div>
      <div class="form-group col-sm-6">
        <label class="col-sm-4 control-label">For</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" name="for" value="<?php echo set_value('for','Self')?>">
        </div>
      </div>
      <div class="form-group col-sm-6 <?php echo (!form_error('date_from')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Date From</label>
        <div class="col-sm-8">
          <input type="text" class="form-control date-mask" id="date_from" name="date_from" value="<?php echo set_value('date_from',$today_date)?>">
        </div>
      </div>
      <div class="form-group col-sm-6 <?php echo (!form_error('date_to')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Date To</label>
        <div class="col-sm-8">
          <input type="text" class="form-control date-mask" id="date_to" name="date_to" value="<?php echo set_value('date_to',$today_date)?>">
        </div>
      </div>
      <div class="form-group col-sm-6">
        <label class="col-sm-4 control-label">Note</label>
        <div class="col-sm-8">
          <textarea type="text" class="form-control" name="note"><?php echo set_value('note')?></textarea>
        </div>
      </div>
       <div class="form-group col-sm-6">
        <label class="col-sm-4 control-label">Asset(s) Requested</label>
        <div class="col-sm-8">
          <textarea type="text" id="asset_requested" class="form-control" name="asset_requested"><?php echo set_value('asset_requested')?></textarea>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-11">
          <button type="submit" class="btn btn-primary btn-flat pull-right">Submit Form</button>
        </div>
      </div>
      </form>
    </div>
  </div>
  
  <div class="box box-info">
    <div class="box-header">
		<h3 class="box-title">Asset List</h3>
    </div>
    <div class="box-body">
      <table id="usage_request" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Label</th>
                <th>Name</th>
                <th>Status</th>
                <th>Add</th>
            </tr>
        </thead>
    </table>
    </div>
  </div>
</section>
