<section class="content-header">
<h1><a class="" href="<?php echo site_url('asset/financial_trans');?>">Financial Transaction</a>
  &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
  Depreciation
  </h1>
</section>
<?php
		$attributes = array(
			'class' 	=> 'form-horizontal',
			'role'		=> 'form',
			'method' 	=> 'post'
			);
		echo form_open('asset/depreciation', $attributes);
		?>
<section class="content"><?php echo $msg?>
  <div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body">
      <div class="form-group">
        <label class="col-sm-4 control-label">Location</label>
        <div class="col-sm-8">
          <select class="form-control" name="location_id" id="location_id" >
            <option value="0" selected>All</option>
            <?php foreach ($site_location as $location)
		echo "<option value='$location[location_id]' ".set_select('location_id', "$location[location_id]").">$location[location_name]</option>";
			 ?>
          </select>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('depreciation_date')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Depreciation Date</label>
        <div class="col-sm-8">
          <input class="form-control" type="date" id="depreciation_date" name="depreciation_date" value="<?php echo set_value('depreciation_date',date('Y-m-d'))?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('note')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Note</label>
        <div class="col-sm-8">
          <input class="form-control" name="note" value="<?php echo set_value('note')?>">
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-8 col-sm-offset-4">
          <button type="button" class="btn btn-info" onclick="refresh_asset_data()">List Assets</button>
        </div>
      </div>

      <input type="hidden" name="assets_checked" id="assets_checked">
      <div class="form-group">
        <div class="col-sm-6">
          <input type="checkbox" onClick="toggle(this);populate()" />
          Toggle All </div>
        <div class="col-sm-6">
          <button class="btn btn-danger btn-flat pull-right" type="submit">Depreciate</button>
        </div>
      </div>
      <table id="depreciation" class="table table-bordered table-striped table-condensed" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Label</th>
            <th>Name</th>
            <th>Group</th>
            <th>Office</th>
            <th>Currency</th>
            <th>Current Value</th>
            <th>Depreciation Amount</th>
            <th>Value after depreciation</th>
          </tr>
        </thead>
        <tbody id="tbody_depreciation">
        </tbody>
      </table>
    </div>
  </div>
</section>
</form>
<script>
function toggle(source) {
  checkboxes = document.getElementsByName('asset_requested[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>