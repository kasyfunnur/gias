<section class="content-header">
  <h1><a class="" href="<?php echo site_url('asset/asset_group');?>">Asset Group</a>
  &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
  Edit Group
  </h1>
</section>
<section class="content">
<?php echo $msg?>
<?php echo validation_errors()?>
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Asset Group Form</h3>
    </div>
    <div class="box-body">
      <?php
		$attributes = array(
			'class' 	=> 'form-horizontal',
			'role'		=> 'form',
			'method' 	=> 'post', 
			'name'		=> 'new_group_form', 
			'id' 		=> 'new_group_form'
			);
		echo form_open('', $attributes);
		?>
        
		<?php //var_dump($data_asset_group);?>
      <div class="form-group <?php echo (!form_error('name')) ? '' : 'has-error' ?>">
        <label class="col-sm-2 control-label">Group Name</label>
        <div class="col-sm-10">
          <input class="form-control" type="text" name="name" maxlength="255" value="<?php echo set_value('name',$data_asset_group['group_name'])?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('description')) ? '' : 'has-error' ?>">
        <label class="col-sm-2 control-label">Description</label>
        <div class="col-sm-10">
          <textarea class="form-control" type="text" maxlength="1000" name="description"><?php echo set_value('description',$data_asset_group['group_description'])?></textarea>
        </div>
      </div>
      
      <!---JM--->
      <div class="form-group">
		<div class="col-sm-12">
        	<table id="asset_sub_group_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Group ID</th>
                        <th>Name</th>
                        <th>Label Tag</th>
                        <th>Description</th>
                    </tr>
                </thead>
            </table>
        </div>        
      </div>
      <!---JM--->
      <?php for ($i=1;$i<=10;$i++){ ?>
          <div class="form-group <?php echo (!form_error("custom_field[$i]")) ? '' : 'has-error' ?>">
        <label class="col-sm-2 control-label">Custom Field <?php echo $i?></label>
        <div class="col-sm-4">
          <input class="form-control" type="text" max="255" value="<?php echo set_value("custom_field[$i]")?>" name="custom_field[<?php echo $i?>]">
        </div>
         <label class="col-sm-2 control-label">Type</label>
        <div class="col-sm-4">
          <select class="form-control" type="text" name="custom_field_type[<?php echo $i?>]">
          	<option value="1" <?php set_select("custom_field_type[$i]","1")?>>Varchar</option>
            <option value="2" <?php set_select("custom_field_type[$i]","2")?>>Date</option>
            <option value="3" <?php set_select("custom_field_type[$i]","3")?>>True/False</option>
            <option value="4" <?php set_select("custom_field_type[$i]","4")?>>Decimal</option>
          </select>
        </div>
      </div>
		 <?php } ?>
      <div class="form-group">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary btn-flat pull-right">Add New</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</section>
