<section class="content-header">
    <h1>Asset Group
    <a href="<?php echo site_url('asset/new_group')?>">
        <button class="btn btn-flat btn-info pull-right">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;New Group
        </button>
    </a>
  </h1>
</section>
<section class="content">
    <?php echo $msg?>
    <div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
            <div class="row" style="padding-bottom:10px">
                <div class="col-xs-3">
                    <select class="form-control" name="parent" id="parent">
                        <option value="0" selected>Main</option>
                        <?php foreach($group_array as $group_row) if($group_row[ 'group_parent_id']=='0' ) echo "<option value='$group_row[group_id]' ".set_select( 'group_id', "$group_row[group_id]"). ">$group_row[group_name]</option>";?>
                    </select>
                </div>
            </div>
            <table id="asset_group" class="table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Tag Code</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</section>
