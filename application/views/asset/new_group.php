<section class="content-header">
    <h1><a class="" href="<?php echo site_url('asset/asset_group');?>">Asset Group</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
    New Group </h1>
</section>
<section class="content">
    <?php echo $msg?>
    <?php echo validation_errors()?>
    <?php $attributes=array( 'class'=> 'form-horizontal', 'role' => 'form', 'method' => 'post', 'name' => 'new_group_form', 'id' => 'new_group_form' ); echo form_open('', $attributes); ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">New Asset Group</h3>
                </div>
                <div class="box-body">
                    <div class="form-group" id="category_form">
                        <label class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="parent" id="parent">
                                <option value="0" selected>Main</option>
                                <?php foreach($group_array as $group_row) if($group_row[ 'group_parent_id']=='0' ) echo "<option value='$group_row[group_id]' ".set_select( 'group_id', "$group_row[group_id]"). ">$group_row[group_name]</option>";?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group <?php echo (!form_error('name')) ? '' : 'has-error' ?>">
                        <label class="col-sm-2 control-label" for="name">Name</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" name="name" id="name" maxlength="255" value="<?php echo set_value('name')?>">
                        </div>
                    </div>
                    <div class="form-group <?php echo (!form_error('tag')) ? '' : 'has-error' ?>">
                        <label class="col-sm-2 control-label" for="description">Tag</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" maxlength="25" name="tag" id="tag" value="<?php echo set_value('tag')?>">
                        </div>
                    </div>
                    <div class="form-group <?php echo (!form_error('description')) ? '' : 'has-error' ?>">
                        <label class="col-sm-2 control-label" for="description">Description</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" type="text" maxlength="1000" name="description" id="description">
                                <?php echo set_value( 'description')?>
                            </textarea>
                        </div>
                    </div>
                    <?php for ($i=1;$i<=10;$i++){ ?>
                    <div class="form-group <?php echo (!form_error(" custom_field[$i] ")) ? '' : 'has-error' ?>">
                        <label class="col-sm-2 control-label">Custom Field
                            <?php echo $i?>
                        </label>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" max="255" value="<?php echo set_value(" custom_field[$i] ")?>" name="custom_field[<?php echo $i?>]">
                        </div>
                        <label class="col-sm-2 control-label" for="custom_field_type[<?php echo $i?>]">Type</label>
                        <div class="col-sm-4">
                            <select class="form-control" type="text" name="custom_field_type[<?php echo $i?>]" id="custom_field_type[<?php echo $i?>]">
                                <option value="1" <?php set_select( "custom_field_type[$i]", "1")?>>Varchar</option>
                                <option value="2" <?php set_select( "custom_field_type[$i]", "2")?>>Date</option>
                                <option value="3" <?php set_select( "custom_field_type[$i]", "3")?>>True/False</option>
                                <option value="4" <?php set_select( "custom_field_type[$i]", "4")?>>Decimal</option>
                            </select>
                        </div>
                    </div>
                    <?php } ?>
                    <!-- link account setting for this group -->
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="description">Asset Cost Account</label>
                        <div class="col-sm-8">
                            <select name="link_acc_cost" class="form-control">
                              <?php foreach ($coa as $c) {
                                echo '<option value="' . $c['account_id'] . '">' . $c['account_number'] . ' - ' . $c['account_name'].'</option>';
                              } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="description">Asset Accumulated Depreciation Account</label>
                        <div class="col-sm-8">
                            <select name="link_acc_accdepre" class="form-control">
                              <?php foreach ($coa as $c) {
                                echo '<option value="' . $c['account_id'] . '">' . $c['account_number'] . ' - ' . $c['account_name'].'</option>';
                              } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="description">Asset Depreciation Expense Account</label>
                        <div class="col-sm-8">
                            <select name="link_acc_depre" class="form-control">
                              <?php foreach ($coa as $c) {
                                echo '<option value="' . $c['account_id'] . '">' . $c['account_number'] . ' - ' . $c['account_name'].'</option>';
                              } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary btn-flat pull-right">Add New</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
