<?php 
array_walk_recursive($asset_data, 'replacer');
array_walk_recursive($group_data, 'replacer');
array_walk_recursive($location_data, 'replacer');
function replacer(& $item, $key) {
    if ($item == '' || $item == NULL) {
        $item = '-';
    }
}
?>
<section class="content-header">
  <h1><a class="" href="<?php echo site_url('asset/asset_list');?>">Asset List</a>
  &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
  Asset View
  </h1>
</section>
<section class="content">
<?php echo $msg?>
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title"><?php echo $asset_data['asset_label']?></h3>
    </div>
    <div class="row">
    <div class="col-md-7">
    <div class="box-body">
      <dl class="dl-horizontal">
        <dt>Label</dt><dd><?php echo $asset_data['asset_label'] ?></dd>
        <dt>Name</dt><dd><?php echo $asset_data['asset_name'] ?></dd>
        <dt>Group</dt><dd><?php echo $group_data['group_name']?></dd>
        <dt>Condition</dt><dd><?php echo $asset_data['asset_condition']?></dd>
        <dt>Description</dt><dd><?php echo $asset_data['asset_description']?></dd>
        <dt>Acquisition Date</dt><dd><?php echo $asset_data['date_acquired']?></dd>
        <dt>Current Location</dt><dd><?php echo $location_data['location_name']?></dd>
        <dt>Asset Cost</dt><dd><?php echo currency_format($asset_data['asset_currency'],$asset_data['asset_cost']);?></dd>
        <dt>Asset Value</dt><dd><?php echo currency_format($asset_data['asset_currency'], $asset_data['asset_value']);?></dd>
        <dt>Added on</dt><dd><?php echo $asset_data['timestamp']?></dd>
      <?php for ($i=1;$i<=10;$i++){
		  if ($group_data["custom_field_$i"]!='-'){
			  echo "<dt>".$group_data["custom_field_$i"]."</dt><dd>".$asset_data["custom_field_$i"]."</dd>"; 
			  }
		  }?>
      </dl>
      </div>
      </div>
      <div class="col-md-5">
        <a href="http://flickholdr.iwerk.org/300/300" class="thumbnail">
      		<img src="http://dummyimage.com/300x300&text=Image has not been uploaded">
    	</a>
      </div>
    </div>
  </div>
  <div class="box box-info">
  <div class="box-header">
      <h3 class="box-title" style="padding-left: 60px;">Asset History</h3>
    </div>
    <div class="box-body">
        
        <ul class="timeline">

            <!-- timeline time label -->
            <?php foreach ( $asset_history as $h ) { ?>
            <li class="time-label">
                <span class="bg-red">
                    <?php echo date('l, jS M Y', strtotime($h['transaction_date']));?>
                </span>
            </li>
            <!-- /.timeline-label -->
        
            <!-- timeline item -->
            <li>
                <!-- timeline icon -->
                <i class="fa fa-envelope bg-blue"></i>
                <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> <?php echo date('g:i A', strtotime($h['timestamp']));?></span>
        
                    <h3 class="timeline-header">
                        <!-- <a href="#"> </a> -->
                            <strong><?php echo ucfirst($h['transaction_type']); ?></strong>
                            by
                            <strong><?php echo $h['full_name']; ?></strong>
                            <?php /* echo $this->sky->label_box($h['document_status']); */ ?>
                            &nbsp;&nbsp;&nbsp;
                            <span class="label label-default"><?php echo $h['document_status']; ?></span>
                       
                    </h3>
        
                    <div class="timeline-body">
                       <?php echo htmlspecialchars($h['memo'])?>
                    </div>
        
                    <!-- <div class='timeline-footer'>
                        <a class="btn btn-primary btn-xs">...</a>
                    </div>
                    -->
                </div>
            </li>
            <!-- END timeline item -->
        
			<?php } ?>
        
        </ul>
        
        <!-- end timeline -->
        
    </div>
  </div>
</section>
