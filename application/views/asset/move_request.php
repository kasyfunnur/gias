<section class="content-header">
<h1><a class="" href="<?php echo site_url('asset/non_financial_trans');?>">Non Financial Transaction</a>
  &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
  Move Request
  </h1>
</section>
<?php
		$attributes = array(
			'class' 	=> 'form-horizontal',
			'role'		=> 'form',
			'method' 	=> 'post'
			);
		echo form_open('asset/move_request', $attributes);
		?>
<section class="content"><?php echo $msg?>
  <div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body table-responsive">
      <div class="form-group">
        <label class="col-sm-4 control-label">Location</label>
        <div class="col-sm-8">
          <select class="form-control" name="location_id" id="location_id" >
            <?php foreach ($site_location as $location)
		echo "<option value='$location[location_id]' ".set_select('location_id', "$location[location_id]").">$location[location_name]</option>";
			 ?>
          </select>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('new_location_id')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">New Location</label>
        <div class="col-sm-8">
          <select class="form-control" name="new_location_id" >
            <?php foreach ($site_location as $location)
		echo "<option value='$location[location_id]' ".set_select('new_location_id', "$location[location_id]").">$location[location_name]</option>";
			 ?>
          </select>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('moving_date')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Moving Date</label>
        <div class="col-sm-8">
          <input class="form-control date-mask" name="moving_date" value="<?php echo set_value('moving_date',date('d/m/Y'))?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('note')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Note</label>
        <div class="col-sm-8">
          <textarea class="form-control" name="note" ><?php echo set_value('note')?></textarea>
        </div>
      </div>
      <input type="hidden" name="assets_checked" id="assets_checked">
      <div class="form-group">
        <div class="col-sm-6">
          <input type="checkbox" onClick="toggle(this);populate()" />
          Toggle All </div>
        <div class="col-sm-6">
          <button class="btn btn-primary btn-flat pull-right" type="submit">Move</button>
        </div>
      </div>
      <table id="move_request" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>#</th>
            <th>Label</th>
            <th>Name</th>
            <th>Group</th>
            <th>Office</th>
            <th>Status</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</section>
</form>
<script>
function toggle(source) {
  checkboxes = document.getElementsByName('asset_requested[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>