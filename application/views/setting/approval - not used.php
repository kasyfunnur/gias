<section class="content-header">
  	<h1> 
	    Settings &gt; Approval
    </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<section class="content">
<div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body table-responsive">
    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th>Document</th>
                <th>Skip Approval ?</th>
                <th>Email Notification ?</th>                
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($datatable as $x=>$data){?>
            <tr>
                <td>
                    <a href="<?php echo site_url('setting/approval_edit');?>/<?php echo $datatable[$x]['appr_id'];?>">
                        <?php echo $data['appr_code'];?>
                    </a>
                </td>
                <td><?php echo $data['appr_name'];?></td>
                <td><?php echo $data['appr_email'];?>
                </td>                
                <td style="text-align:right">$ xxx </td>           
                
            </tr>
            <?php } ?>
        </tbody>
    </table></div>
    </div>
</section>
<!-- /.content -->