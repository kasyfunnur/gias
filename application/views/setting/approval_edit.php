<section class="content-header">
    <h1> 
        <a class="" href="<?php echo site_url('setting/approval');?>">Approval</a> 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Edit
        &nbsp;
        <div class="pull-right button_action" id="button_action"></div>
    </h1>
</section>
<?php $attributes = array(
    'class'=> 'form-horizontal',
    'role' => 'form',
    'method' => 'post',
    'name' => 'frm',
    'id' => 'frm',
    'onSubmit' => 'return FormValidation();'
);
    echo form_open('', $attributes); echo form_text('','hidden','appr_id',$approval_header['appr_id']); ?>
<?php echo (isset($msg))?$msg: ""?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group col-sm-12">
                        <label class="col-sm-3" for="appr_code">Approval Code</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" id="appr_code" name="appr_code" value="<?php echo $approval_header['appr_code'];?>" readonly>
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <label class="col-sm-3" for="appr_name">Approval Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" id="appr_name" name="appr_name" value="<?php echo $approval_header['appr_name'];?>">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group col-sm-12">
                        <?php echo form_checkbox( array( 'name'=>'appr_email', 'id'=>'appr_email', 'value'=>1, 'checked'=>$approval_header['appr_email'] ) ); ?>
                        <label for="appr_email">Email Notification</label>
                    </div>
                </div>
            </div>
            <hr>
            <!-- detail approval part -->
            <div class="row">
                <div class="col-md-12">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Request By &nbsp;&nbsp;
                            <a class="btn btn-info btn-xs" onclick="add_detail(0,0,0,'')"><i class="fa fa-plus-square"></i> Add</a>
                            </th>
                            <th>Auto Approve</th>
                            <th>Step Based</th>
                            <th>Approver Matrix</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="approve_detail_tbody">
                    </tbody>
                </table>
                </div>
            </div>
            
            <!-- end of detail approval part -->

            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <br>
</section>
</form>
<script>
var return_url = "<?php echo site_url('setting/approval');?>";
var submit_url = "<?php echo site_url('setting/approval_edit_query');?>";

// approval detail object;
var approval_detail = <?php echo json_encode($approval) ?>;
var positions = <?php echo json_encode($positions) ?>;
</script>
