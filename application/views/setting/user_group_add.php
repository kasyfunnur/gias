<section class="content-header">
  <h1><a class="" href="<?php echo site_url('admin/user_group');?>">User Group</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
    Add User Group Form </h1>
</section>
<section class="content"> <?php echo $msg?> <?php echo validation_errors()?>
  <div class="box box-primary">
    <div class="box-header"> </div>
    <div class="box-body">
      <?php
    $attributes = array(
      'class'   => 'form-horizontal',
      'role'    => 'form',
      'method'  => 'post'
      );
    echo form_open('', $attributes);
    ?>
      <div class="form-group <?php echo (!form_error('group_name')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Group Name</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="group_name" value="<?php echo set_value('group_name')?>" autofocus>
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('group_desc')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Description</label>
        <div class="col-sm-8">
          <textarea class="form-control" type="text" name="group_desc"><?php echo set_value('group_desc')?></textarea>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary btn-flat pull-right">Add New</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</section>
