<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Shift Settings
    <small>Control Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-gear"></i> Setting</a></li>
    <li><a href="#">HR</a></li>
    <li class="active">Add Shift</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      &nbsp;
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Add Shift</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo site_url('setting/store'); ?>" method="post">
          <div class="box-body">
            <div class="form-group">
              <label for="shiftCode" class="col-sm-1 control-label">Shift Code</label>
              <div class="col-sm-2">
                <input class="form-control" id="shiftCode" name="shiftCode" placeholder="Shift Code" type="text" required="autofocus">
              </div>
            </div>
            &nbsp;
            <?php
              $date = date_create(now());
              date_time_set($date, 00, 00);
              $startTime = date_format($date, 'H:i');
              date_time_set($date, 23, 59);
              $endTime = date_format($date, 'H:i');
            ?>
            <div class="form-group">
              <label for="startTime" class="col-sm-1 control-label">Start Time</label>
              <div class="col-sm-2">
                <input class="form-control" id="startTime" name="startTime" placeholder="Start Time" type="text" value="<?php echo $startTime; ?>">
              </div>
              <!-- Batas -->
              <label for="breakStart" class="col-sm-1 control-label">Break Start</label>
              <div class="col-sm-2">
                <input class="form-control" id="breakStart" name="breakStart" placeholder="Break Start" type="text" value="<?php echo $startTime; ?>">
              </div>
            </div>
            &nbsp;
            <div class="form-group">
              <label for="endTime" class="col-sm-1 control-label">End Time</label>
              <div class="col-sm-2">
                <input class="form-control" id="endTime" name="endTime" placeholder="End Time" type="text" value="<?php echo $endTime; ?>">
              </div>
              <!-- Batas -->
              <label for="breakEnd" class="col-sm-1 control-label">Break End</label>
              <div class="col-sm-2">
                <input class="form-control" id="breakEnd" name="breakEnd" placeholder="Break Time" type="text" value="<?php echo $endTime; ?>">
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right" onclick="return confirm('Are you sure want to add a new shift ?')">Save</button>
            <a href="<?php echo site_url('setting/shift'); ?>">
              <button type="button" class="btn btn-default pull-right">Cancel</button>
            </a>
          </div>
          &nbsp;
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Save Shift</h4>
      </div>
      <div class="modal-body">
        Are you sure want to add a new shift ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>
