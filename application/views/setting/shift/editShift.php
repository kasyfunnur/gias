<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Shift Settings
    <small>Control Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-gear"></i> Setting</a></li>
    <li><a href="#">HR</a></li>
    <li class="active">Edit Shift</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      &nbsp;
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Add Shift</h3>
        </div>
        <!-- /.box-header -->
        <?php
          $date = new DateTime($e['startTime']);
          $dates = new DateTime($e['endTime']);
          $sB = new DateTime($e['breakStart']);
          $eB = new DateTime($e['breakEnd']);
        ?>
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo site_url('setting/updateShift/'. $e['idShift']); ?>" method="post">
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-1 control-label">Shift Code</label>
              <div class="col-sm-2">
                <input class="form-control" id="shiftCode" name="shiftCode" placeholder="Shift Code" type="text" value="<?php echo $e['shiftCode']; ?>">
              </div>
            </div>
            &nbsp;
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-1 control-label">Start Time</label>
              <div class="col-sm-2">
                <input class="form-control" id="startTime" name="startTime" placeholder="Start Time" type="text" value="<?php echo $date->format('H:i'); ?>">
              </div>
              <!-- Batas -->
              <label for="inputEmail3" class="col-sm-1 control-label">Break Start</label>
              <div class="col-sm-2">
                <input class="form-control" id="breakStart" name="breakStart" placeholder="Break Start" type="text" value="<?php echo $sB->format('H:i'); ?>">
              </div>
            </div>
            &nbsp;
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-1 control-label">End Time</label>
              <div class="col-sm-2">
                <input class="form-control" id="endTime" name="endTime" placeholder="End Time" type="text" value="<?php echo $dates->format('H:i'); ?>">
              </div>
              <!-- Batas -->
              <label for="inputEmail3" class="col-sm-1 control-label">Break End</label>
              <div class="col-sm-2">
                <input class="form-control" id="breakEnd" name="breakEnd" placeholder="Break Time" type="text" value="<?php echo $eB->format('H:i'); ?>">
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right" onclick="return confirm('Are you sure want to edit this shift ?')">Save</button>
            <a href="<?php echo site_url('setting/shift'); ?>">
              <button type="submit" class="btn btn-default pull-right">Cancel</button>
            </a>
          </div>
          &nbsp;
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Save Shift</h4>
      </div>
      <div class="modal-body">
        Are you sure want to add a new shift ?
      </div>
      <div class="modal-footer">
        <form action="<?php echo site_url('setting/updateShift/'. $e['id']);?>" method="post">
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <button type="submit" class="btn btn-primary">Yes</button>
        </form>
      </div>
    </div>
  </div>
</div>
