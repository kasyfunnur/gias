<section class="content-header">
  <h1>
    Shift Settings
    <small>Control Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-gear"></i> Setting</a></li>
    <li><a href="#">HR</a></li>
    <li class="active">Shift</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <a href="<?php echo site_url('setting/addShift');?>">
    <button class="btn btn-flat btn-primary ">
      <i class="fa fa-plus"></i>&nbsp;&nbsp;Add New mployee
    </button>
  </a>
  <div class="row">
    <div class="col-xs-12">
      &nbsp;
      <?php echo $this->session->flashdata('msg'); ?>
      &nbsp;
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Data Shift</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Shift Code</th>
                <th>Shift Name</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Break Start</th>
                <th>Break End</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; foreach($shift as $e):?>
              <?php
                $date = new DateTime($e['startTime']);
                $dates = new DateTime($e['endTime']);
                $sB = new DateTime($e['breakStart']);
                $eB = new DateTime($e['breakEnd']);
              ?>
              <tr>
                <td><?php echo $no++; ?></td>
                <td><a href="<?php echo site_url('setting/editShift/'.$e['idShift']);?>"><?php echo $e['shiftCode']; ?></td></a>
                <td><?php echo $e['shiftName']; ?></td>
                <td><?php echo $date->format('H:i'); ?></td>
                <td><?php echo $dates->format('H:i'); ?></td>
                <td><?php echo $sB->format('H:i');?></td>
                <td><?php echo $eB->format('H:i');?></td>
              </tr>
              <?php endforeach;?>
            </tfoot>
          </table>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content-wrapper -->
