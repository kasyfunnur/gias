<section class="content-header">
  <h1>Users
  <a href="<?php echo site_url('setting/add_user');?>">
        <button class="btn btn-flat btn-info pull-right">
          <i class="fa fa-plus"></i>&nbsp;&nbsp;Add New
    </button>
    </a>
  </h1>
</section>
<section class="content">
  <div class="box box-info">
    <div class="box-header">
    </div>
    <div class="box-body">
      <table id="user_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Name</th>
            <th>Location</th>
            <th>Position</th>
            <th>Email</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</section>