<?php $attributes = array(
    'class'     => 'form-horizontal',
    'role'      => 'form',
    'method'    => 'post', 
    'name'      => 'frm', 
    'id'        => 'frm',
    'onSubmit'  => 'return validateForm();'
    );
echo form_open('', $attributes);
?>  
<section class="content-header">
	<h1>
		<span>Setting</span>
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
    	<span>Master Data</span> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
    	<span>Finance</span> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        <span>Currency</span> 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        <span>Add</span> 

        <!-- <a href="<?php echo site_url('setting/master_fin_curr_add');?>">
            <button class="btn btn-flat btn-info pull-right">
            	<span class="hidden-xs">
            		<i class="fa fa-plus"></i>&nbsp;&nbsp;New Currency
                </span>
            	<span class="visible-xs">
            		<i class="fa fa-plus"></i>
                </span>
            </button>
        </a> -->
	</h1>
</section>

<section class="content">
<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <label class="col-sm-4" for="curr_name">Currency Code</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="curr_code" id="curr_code">
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4" for="curr_name">Currency Name</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="curr_name" id="curr_name">
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4" for="curr_iso">Currency ISO</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="curr_iso" id="curr_iso">
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4" for="curr_decimal">Currency Decimal</label>
            <div class="col-sm-8">
                <input class="form-control" type="number" name="curr_decimal" id="curr_decimal">
            </div>
        </div>
        <div class="row">
            <label class="col-sm-4" for="curr_symbol">Currency Symbol</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="curr_symbol" id="curr_symbol">
            </div>
        </div>
    </div>
</div>
</section>
<div class="row">
    <div class="col-sm-6 text-right">
        <button type="submit" class="btn btn-info"><i class="fa fa-plus"></i> Add</button>
    </div>
</div>


<script>
var submit_url = "<?php echo site_url('setting/master_fin_curr_add');?>";
var return_url = "<?php echo site_url('setting/master_fin_curr');?>";
function validateForm(){
    if(confirm("Are you sure???")==true){     
        $.post(
            submit_url,
            $('#frm').serialize(),
            function(response){                 
                if(response==1){
                    alert("Data updated successfully!");
                    window.location.href=return_url;
                }else{
                    console.log("ERROR: "+response);
                }
            }
        );
    }
    return false;
}
</script>