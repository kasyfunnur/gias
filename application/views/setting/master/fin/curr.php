<section class="content-header">
	<h1>
		<span>Setting</span>
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
    	<span>Master Data</span> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
    	<span>Finance</span> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        <span>Currency</span> 

        <a href="<?php echo site_url('setting/master_fin_curr_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<span class="hidden-xs">
        		<i class="fa fa-plus"></i>&nbsp;&nbsp;New Currency
            </span>
        	<span class="visible-xs">
        		<i class="fa fa-plus"></i>
            </span>
        </button>
    </a>
	</h1>
</section>

<section class="content">
<div class="row">
	<div class="col-sm-12">
		<table class="table table-border table-hover" id="table_detail">
			<thead>
			<tr>
				<th>Code</th>
				<th>Currency</th>
				<!-- <th>ISO Code</th> -->
				<th>Decimal</th>
				<th>Symbol</th>
				<th>Action</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($list_currency as $x=>$currency): ?>
			<tr>
				<td><?php echo $currency['curr_code'];?></td>
				<td><?php echo $currency['curr_name'];?></td>
				<!-- <td><?php echo $currency['curr_iso'];?></td> -->
				<td><?php echo $currency['curr_decimal'];?></td>
				<td><?php echo $currency['curr_symbol'];?></td>
				<td>
					<a href="<?php echo base_url('setting/master_fin_curr_edit')."/".$currency['curr_code'];?>" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
				</td>
			</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>

<?php //var_dump($list_currency); ?>