<div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#general" aria-controls="home" role="tab" data-toggle="tab">General</a></li>
        <li role="presentation"><a href="#sales" aria-controls="profile" role="tab" data-toggle="tab">Sales</a></li>
        <li role="presentation"><a href="#inventory" aria-controls="messages" role="tab" data-toggle="tab">Invenvory</a></li>
        <li role="presentation"><a href="#purchasing" aria-controls="settings" role="tab" data-toggle="tab">Purchasing</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="general">
            <div class="container">
                <div class="row">   
                    <div class="col-sm-8">
                        <div class="table table-condensed table-hover">
                            <table class="table-bordered" id="table_general" style="table-layout:fixed;white-space:nowrap;width:960px">
                            <thead>
                            <tr>
                                <th style="width:40%">Description</th>
                                <th style="width:60%">Link Account</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Bank Charges Account</td>
                                <td><?php echo sw_CreateSelect('la_bank_charge', $list_account, 'account_id', array('account_number','account_name'));?></td>
                            </tr> 
                            <tr>
                                <td>Exch. Diff Loss</td>
                                <td></td>
                            </tr> 
                            <tr>
                                <td>Exch. Diff Gain</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Period-end Closing</td>
                                <td></td>
                            </tr> 
                            <tr>
                                <td>Roudning Account</td>
                                <td></td>
                            </tr>
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="sales">

        </div>
        <div role="tabpanel" class="tab-pane" id="inventory">

        </div>
        <div role="tabpanel" class="tab-pane" id="purchasing">

        </div>
    </div>

</div>