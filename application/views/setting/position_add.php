<section class="content-header">
  <h1><a class="" href="<?php echo site_url('setting/position');?>">Organization Structure</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
    Add New Position </h1>
</section>
<section class="content"> <?php echo $msg?> <?php echo validation_errors()?>
  <div class="box box-primary">
    <div class="box-header"> </div>
    <div class="box-body">
      <?php
		$attributes = array(
			'class' 	=> 'form-horizontal',
			'role'		=> 'form',
			'method' 	=> 'post',
			'name'		=> 'frmposition',
			'id'		=> 'frmposition'
			);
		echo form_open('', $attributes);
		?>
        
      <div class="form-group">
        <label class="col-sm-4 control-label">Type</label>
        <div class="col-sm-8">
        	<label class="radio-inline">
         	<input type="radio" id="position_type" name="position_type" value="position" onClick="change_position_type(this)" <?php echo set_radio('position_type', 'position', TRUE); ?>> Position</label>
            <label class="radio-inline">
            <input type="radio" id="position_type" name="position_type" value="division" onClick="change_position_type(this)" <?php echo set_radio('position_type', 'division'); ?>> Division</label>
        </div>
      </div>
      
      <div class="form-group <?php echo (!form_error('position')) ? '' : 'has-error' ?>" id="div_division" style="display:block">
        <label class="col-sm-4 control-label">Parent Division</label>
        <div class="col-sm-8">
         <?php html_select_division(array('name'=>'division_id','class'=>'form-control')); ?>
        </div>
      </div>
      
      
      <div class="form-group <?php echo (!form_error('position_code')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Position Code</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="position_code" value="<?php echo set_value('position_code')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('position_name')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Position Name</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="position_name" value="<?php echo set_value('position_name')?>">
        </div>
      </div>
      <div class="form-group <?php echo (!form_error('position_desc')) ? '' : 'has-error' ?>">
        <label class="col-sm-4 control-label">Position Description</label>
        <div class="col-sm-8">
          <input class="form-control" type="text" name="position_desc" value="<?php echo set_value('position_desc')?>">
        </div>
      </div>
      
      
      <div class="form-group">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary btn-flat pull-right">Add New</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</section>
<script>
	function change_position_type(radio_element)
	{
		if(radio_element){
			console.log(radio_element);
			if (radio_element.value == 'position')
				$('#div_division').slideDown(200);
			else 
				$('#div_division').slideUp(200);
		}
	}
</script>