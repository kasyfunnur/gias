<!-- Content Header (Page header) -->
<?php $attributes = array(
    'class'     => 'form-horizontal',
    'role'      => 'form',
    'method'    => 'post', 
    'name'      => 'frm', 
    'id'        => 'frm',
    'onSubmit'  => 'return validateForm();'
    );
echo form_open('', $attributes);
?>  

<section class="content-header">
	<h1><a class="" href="<?php echo site_url('');?>">Setting</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Document Numbering
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Add
    </h1>
</section>

<!-- $this->input->post('doc_code'),
$this->input->post('doc_name'),
$this->input->post('doc_prefix'),
$this->input->post('doc_suffix'),
$this->input->post('num_next'),
$this->input->post('num_last') -->

<!-- Main content -->
<section class="content">	
    <div class="row">
    	<div class="col-sm-4">
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_code">Document Code</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_code" name="doc_code" required>
                </div>
            </div>
            
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_name">Document Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_name" name="doc_name" required>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_prefix">Document Prefix</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_prefix" name="doc_prefix"
                    placeholder="(ex:PO-123) = type 'PO'" required>
                </div>
            </div>

        	<div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_suffix">Document Suffix</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_suffix" name="doc_suffix" value="">
                </div>
            </div>	
		</div>
    	<div class="col-sm-4">        			
            <div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="num_next">Next Num</label>
                <div class="col-sm-9">
                    <input type="number" class="form-control input-sm" id="num_next" name="num_next" value="1">
                </div>
            </div>
            <div class="form-group col-sm-12 col-xs-6">
                <label class="col-sm-3" for="num_last">Last Num</label>
                <div class="col-sm-9">
                    <input type="number" class="form-control input-sm" id="num_last" name="num_last" value="9999">
                </div>
            </div>
        </div>
    </div>   
</section>

</form>