<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Employee Settings
    <small>Control Panel</small>
  </h1>
  &nbsp;
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-gear"></i> Setting</a></li>
    <li><a href="#">HR</a></li>
    <li>Employee</li>
    <li class="active"><?php echo $em['idEmp']; ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <label class="col-sm-2 control-label">Emp ID</label>
  <p class="form-control-static"><?php echo $em['idEmp']; ?></p>
  <label class="col-sm-2 control-label">Name</label>
  <p class="form-control-static"><?php echo $em['first_name']; echo '&nbsp'.$em['last_name']; ?></p>
  <label class="col-sm-2 control-label">Position</label>
  <p class="form-control-static"><?php echo $em['position_id']; ?></p>
  <div class="row">
    <div class="col-xs-12">
      &nbsp;
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Attendance Log</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Date</th>
                <th>Shift Code</th>
                <th>Shift Start</th>
                <th>Actual Start</th>
                <th>Shift End</th>
                <th>Actual End</th>
                <th>Status</th>
                <th>Overtime (minutes)</th>
              </tr>
            </thead>
            <tbody>
            <?php $no = 1; foreach($es as $e): ?>
              <?php
                $date = new DateTime($e['startTime']);
                $dates = new DateTime($e['endTime']);
                $sB = new DateTime($e['breakStart']);
                $eB = new DateTime($e['breakEnd']);
                $aS = new DateTime($e['actualStart']);
                $aE = new DateTime($e['actualEnd']);
              ?>
              <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $e['tanggal']; ?></td>
                <td><?php echo $e['shiftCode']; ?></td>
                <td><?php echo $date->format('H:i'); ?></td>
                <td><input type="text" name="actualStart" value="<?php echo $aS->format('H:i'); ?>" disabled></td>
                <td><?php echo $dates->format('H:i'); ?></td>
                <td><input type="text" name="actualEnd" value="<?php echo $aE->format('H:i'); ?>" disabled></td>
                <td><?php echo $e['shiftCode']; ?></td>
                <td><input type="text" name="overtime" value="<?php echo $e['overtime']; ?>"></td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content-wrapper -->
