<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Employee Settings
    <small>Control Panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-gear"></i> Setting</a></li>
    <li><a href="#">HR</a></li>
    <li class="active">Employee</li>
  </ol>
</section>
<?php echo (isset($msg))?$msg: ""?>
<!-- Main content -->
<section class="content">
  <a href="<?php echo site_url('setting/addShift');?>">
    <button class="btn btn-flat btn-primary ">
      <i class="fa fa-plus"></i>&nbsp;&nbsp;Add New mployee
    </button>
  </a>
  <div class="row">
    <div class="col-xs-12">
      &nbsp;
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Employee</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="employee_list" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Emp ID</th>
                <th>Name</th>
                <th>Position</th>
                <th>Location</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; foreach($employee as $e):?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $e['emp_no']; ?></td>
                  <td><?php echo $e['full_name']; ?></td>
                  <td><?php echo $e['position_name']; ?></td>
                  <td><?php echo $e['location_name']; ?></td>
                  <td><?php echo $e['email']; ?></td>
                  <td>
                    <a href="<?php echo site_url('setting/showEmployee/'.$e['emp_no']); ?>"><i class="fa fa-eye" title="See Detail"></i></a>
                    &nbsp;
                    <a href="#"><i class="fa fa-trash" title="Delete Data"></i></a>
                  </td>
                </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content-wrapper -->
