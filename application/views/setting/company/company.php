<section class="content-header">
  <h1>Settings
  <i class="fa fa-angle-right"></i>
  Companies
  <a href="<?php echo site_url('setting/company_add');?>">
        <button class="btn btn-flat btn-info pull-right">
          <i class="fa fa-plus"></i>&nbsp;&nbsp;Add New Company
    </button>
    </a>
  </h1>
</section>
<section class="content">
  <div class="box box-info">
    <div class="box-header">
    </div>
    <div class="box-body">
      <table id="user_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Company</th>
            <th>Address</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($companies as $comp) { ?>
            <tr>
              <td>
                <a href="<?php echo site_url('setting/company_edit') . '/' . $comp['comp_id'] ?>" title="">
                  <?php echo $comp['comp_name'] ?>
                </a>
              </td>
              <td><?php echo $comp['comp_addr'] ?></td>
              <td>Holding</td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</section>