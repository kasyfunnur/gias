<section class="content-header">
    <h1>Settings
  &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
  <a href="<?php echo site_url('setting/company') ?>" title="">Companies</a>
  &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
  New Company
  </h1>
</section>
<section class="content">
    <div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
            <?php $attributes=array( 'class'=> 'form-horizontal', 'role' => 'form', 'method' => 'post', 'onsubmit' => 'return FormValidation()' ); echo form_open('', $attributes); ?>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="comp_name">Company Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="comp_name" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Company Address</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="comp_addr" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Company City</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="comp_city">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Company State</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="comp_state">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Company Country</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="comp_country">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Zip Code</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="comp_zipcode">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Company Website</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="comp_website">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="comp_tlp1">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Fax</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="comp_fax1">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-9">
                    <input class="form-control" type="email" name="comp_email" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Company Tax Number</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="comp_tax_num">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</section>