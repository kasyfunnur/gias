<section class="content-header">
  <h1>User Group
  <a href="<?php echo site_url('setting/add_user_group');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<i class="fa fa-plus"></i>&nbsp;&nbsp;Add New
		</button>
    </a>
  </h1>
</section>
<section class="content">
  <div class="box box-info">
    <div class="box-header">
    </div>
    <div class="box-body">
      <table id="user_group_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>
          	<th>Group ID</th>
            <th>Group Name</th>
            <th>Group Description</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</section>