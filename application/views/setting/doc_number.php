<!-- Content Header (Page header) -->
<?php $attributes = array(
    'class'     => 'form-horizontal',
    'role'      => 'form',
    'method'    => 'post', 
    'name'      => 'frm', 
    'id'        => 'frm',
    'onSubmit'  => 'return validateForm();'
    );
echo form_open('setting/doc_number_add', $attributes);
//echo form_text('','hidden','save_confirm','save');
?>  

<section class="content-header">
    <h1><a class="" href="<?php echo site_url('');?>">Setting</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Document Numbering 
		
		<a href="<?php echo site_url('setting/doc_number_add');?>">
	        <button class="btn btn-flat btn-info pull-right">
	        	<span class="hidden-xs">
	        		<i class="fa fa-plus"></i>&nbsp;&nbsp;New Doc
	            </span>
	        	<span class="visible-xs">
	        		<i class="fa fa-plus"></i>
	            </span>
	        </button>
	    </a>
    </h1>
</section>

<!-- Main content -->
<section class="content">	
<div class="box box-info">
    <div class="box-body table-responsive">
  	<table class="table table-hover table-condensed" id="purchase_order_index">
    	<thead>
            <tr>                
                <th style="width:25%;text-align:left">
                    <!-- <div class="input-group input-group-sm">
                        <span class="input-group-btn">                            
                            <button type="submit" class="btn btn-info" name="btn_filter" value="" />ALL</button>
                        </span>
                    </div> -->
                    Document Code
                </th>
                <th style="width:35%;text-align:left">
                    <!-- <div class="input-group input-group-sm">
                        <?php echo sw_textButton('search_field','Search Here..','search_button','Search','search');?>
                    </div> -->
                    Prefix Code
                </th>
                <th style="width:40%;text-align:left">
					<div class="row">
                		<div class="col-sm-4">
	                    	First Number
	                    </div>
	                    <div class="col-sm-4">
							Next Number
						</div>
						<div class="col-sm-4">
							Last Number
						</div>
					</div>
                </th>
            </tr>
        </thead>
        <tbody>
        	<?php foreach ($documents as $x=>$document){?>
            <tr>
                <td>
                	<?php echo $document['document_code']; ?><br>
					<?php echo $document['document_name']; ?>
                </td>
                <td>
					<?php echo $document['series_prefix']; ?>
				</td>          
                <td> 
                	<div class="row">
                		<div class="col-sm-4">
	                    	<?php echo $document['first_num']; ?>
	                    </div>
	                    <div class="col-sm-4">
							<input class="form-control" type="text" name="" id="" value="<?php echo $document['next_num']; ?>">
						</div>
						<div class="col-sm-4">
							<input class="form-control" type="text" name="" id="" value="<?php echo $document['last_num']; ?>">
						</div>
					</div>
                </td>
            </tr>
            <?php }?>
        </tbody>
    </table>
    </div>
</div>
	

</section>
</form>

<script type="text/javascript">
function validateForm(){
	if(confirm("Are you sure?")==true){		
		alert('test');
	}
	return false;
}
</script>