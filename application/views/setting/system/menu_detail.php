<?php $attributes = array(
        'class'     => 'form-horizontal',
        'role'      => 'form',
        'method'    => 'post', 
        'name'      => 'frm', 
        'id'        => 'frm',
        'onSubmit'  => 'return FormValidation();'
        );
    echo form_open('', $attributes);
    ?>

    <input type="hidden" name="menu_id" value="<?php echo $menu['menu_id'] ?>">

    <div class="form-group">
      <label for="menu_name" class="col-sm-3 control-label">Menu Name</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="menu_name" name="menu_name" placeholder="Menu Name" value="<?php echo $menu['menu_name'] ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="last_node" class="col-sm-3 control-label">Menu Type</label>
      <div class="col-sm-9">
        <select name="last_node" class="form-control">
          <option value="0" <?php echo $menu['last_node'] == 0 ? 'selected' : '' ?>>Menu Group</option>
          <option value="1" <?php echo $menu['last_node'] == 1 ? 'selected' : '' ?>>Menu Link</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="menu_link" class="col-sm-3 control-label">Menu Link</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="menu_link" name="menu_link" placeholder="Menu Link" value="<?php echo $menu['menu_link'] ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="menu_description" class="col-sm-3 control-label">Menu Description</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="menu_description" name="menu_description" placeholder="Menu Description" value="<?php echo $menu['menu_description'] ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="icon" class="col-sm-3 control-label">Menu Icon</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="icon" name="icon" placeholder="Menu Icon" value="<?php echo $menu['icon'] ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="order_no" class="col-sm-3 control-label">Order</label>
      <div class="col-sm-9">
        <input type="number" class="form-control" id="order_no" name="order_no" placeholder="Order No" value="<?php echo $menu['order_no'] ?>" required>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-9 col-sm-offset-3">
        <div class="checkbox">
          <label>
            <input type="checkbox" name="active" value="1" <?php echo ($menu['active'] ? 'checked' : '') ;?>>
          Active ?
          </label>
        </div>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-9 col-sm-offset-3">
        <button class="btn btn-primary" type="button" onclick="update_menu()">Update</button>
      </div>
      
    </div>


</form>
<!-- 
[menu_id] => 80
[parent_id] => 0
[menu_name] => Fixed Asset
[menu_link] => 
[menu_description] => 
[name_prefix] => 
[name_postfix] => 
[last_node] => 0
[order_no] => 3
[icon] => fa fa-building-o
-->