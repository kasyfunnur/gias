<link rel="stylesheet" href="<?php echo base_url('assets/js/jstree/themes/default/style.min.css') ?>">

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Setting <i class="fa fa-angle-right"></i> System <i class="fa fa-angle-right"></i>
  Menu Management
  </h1>
</section>

<!-- Main content -->
<section class="content"> 
	<div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
          <h4>Menu</h4>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
                    <div id="menutree"></div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-7 col-lg-8">
                    <div id="menu_data"></div>
                </div>
            </div>
        </div>
      </div>
    </section>
</section>
<script type="text/javascript">
  var json_menu_url = '<?php echo site_url('setting/ajax_menu_tree') ?>';
  var detail_menu_url = '<?php echo site_url('setting/ajax_menu_detail') ?>';
</script>