<section class="content-header">
  <h1>Organization Structure
  <a href="<?php echo site_url('setting/position_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<i class="fa fa-plus"></i>&nbsp;&nbsp;Add New Position / Division
		</button>
    </a>
  </h1>
</section>
<section class="content">
  <div class="box box-info">
    <div class="box-header">
    </div>
    <div class="box-body">
      <table id="position" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Code</th>
            <th>Name</th>
            <th>Description</th>
            <th>Type</th>
            <th>User Count</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</section>