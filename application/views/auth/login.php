<!DOCTYPE HTML>

    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Sky ERP</title>
    <meta name="viewport" content="width=device-width">
    <meta name="robots" content="noindex, nofollow">
    <link rel="shortcut icon" href="<?php echo base_url('favicon.ico') ; ?>" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url('favicon.ico') ; ?>" type="image/x-icon">
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" media="screen">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/signin.css'); ?>" rel="stylesheet" media="screen">
    <style type="text/css">
      .btn.btn-flat {
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      border-radius: 0;
      -webkit-box-shadow: none;
      -moz-box-shadow: none;
      box-shadow: none;
      border-width: 1px;
    }
    </style>

    <!-- HTML5 shim for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
    <![endif]-->
    <script>
        document.write('<style type="text/css">body {display:none;}<\/style>');
    </script>
    <!-- Anti ClickJacking Protection! -->
    <style id="antiClickjack">
body {
  display: none !important;
}
</style>
    <script>
        if (self === top) {
            var antiClickjack = document.getElementById("antiClickjack");
            antiClickjack.parentNode.removeChild(antiClickjack);
        } else {
            top.location = self.location;
        }
    </script>
    </head>
    <body>
<div class="container">
      <div class="form-signin text-center"><img src="<?php echo base_url('assets/img/company-logo.png'); ?>" alt="Logo" width="300"></div>
      
      <!-- Forgot Password Form -->
      <?php
    $attributes = array(
      'class'   => 'form-signin', 
      'method'  => 'post', 
      'name'    => 'forgetpass_form', 
      'id'    => 'forgetpass_form', 
      'style'   => 'display:none',
      'role'    => 'form'
      );
    echo form_open('login/forgetPassword', $attributes);
    ?>
      <?php echo validation_errors('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>','</div>')?>
      <input type="text" class="form-control" id="forgetpass_username" name="forgetpass_username" placeholder="Username" value="<?php echo set_value('forgetpass_username')?>" required>
      <button type="submit" class="btn btn-flat btn-primary btn-lg btn-block" name="sublogin2" value="LogIn">Submit</button>
      <div class="login-footer"> <a href="javascript:void(0);" id="backtologin">Back To Login</a> </div>
      </form>
      <!-- End Forgot Password Form --> 
      
      <!-- Login Form -->
      <?php
    $attributes = array(
      'class'   => 'form-signin', 
      'method'  => 'post', 
      'name'    => 'login_form', 
      'id'    => 'login_form',
      'role'    => 'form'
      );
    echo form_open('auth/login', $attributes);
    ?>
      <?php echo validation_errors("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>{$message}<br>",'</div>')?>
      <?php if(isset($message) && trim($message) !== ''){
          echo "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>{$message}</div>";
        }
      ?>
      <input type="text" class="form-control" id="login_username" name="identity" placeholder="Username" value="<?php echo set_value('login_username')?>" required>
      <input type="password" class="form-control" id="login_password" name="password" placeholder="Password"  value="<?php echo set_value('login_password')?>" required>
    <button type="submit" class="btn btn-flat btn-primary btn-lg btn-block" name="sublogin2" value="LogIn">Login</button>
    <div class="login-footer"> <a href="javascript:void(0);" id="forgotpw">Forgot Password?</a> </div>
  </div>
      
     <?php echo form_close();?>
      <!-- End Login Form --> 
    </div>
<!--/panel content-->
</div>
<p class="form-signin text-center">Powered By: <a href="http://www.digitalbuana.com/" target="_new">Digital Buana</a></p>
</div>
<script src="<?php echo base_url('assets/js/jquery.min.js');?>" ></script> 
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>" ></script> 
<script>
  $('#forgotpw').click(function() {
    $('#login_form').slideUp('slow', function() {
      $('#forgetpass_form').slideDown("slow");
    });
  });
  $('#backtologin').click(function() {
    $('#forgetpass_form').slideUp('slow', function() {
      $('#login_form').slideDown("slow");
    });
  });
  
  // Page Fade In After Load    
  $(window).load(function() {
    $("body").fadeIn();
  });
</script>
</body>
</html>

<!-- 

<h1><?php echo lang('login_heading');?></h1>
<p><?php echo lang('login_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/login");?>

  <p>
    <?php echo lang('login_identity_label', 'identity');?>
    <?php echo form_input($identity);?>
  </p>

  <p>
    <?php echo lang('login_password_label', 'password');?>
    <?php echo form_input($password);?>
  </p>

  <p>
    <?php echo lang('login_remember_label', 'remember');?>
    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
  </p>


  <p><?php echo form_submit('submit', lang('login_submit_btn'));?></p>

<?php echo form_close();?>

<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>
-->