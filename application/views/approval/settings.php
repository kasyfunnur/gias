<!-- Content Header (Page header) -->

<section class="content-header">
  <h1>Approval Settings</h1>
</section>

<!-- Main content -->
<section class="content"> 
	<div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
          <table id="asset_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Document Code</th>
                    <th>Document Name</th>
                    <th>Email Enabled</th>
                </tr>
            </thead>
            <tbody>
            	<?php foreach ($approval_documents as $appr) { ?>
                	<tr>
                    	<td width="150"><a href="<?php echo site_url("approval/settings_detail/$appr[appr_id]")?>"><?php echo $appr['appr_code']; ?></a></td>
                        <td><?php echo $appr['appr_name']; ?></td>
                        <td><?php echo ($appr['appr_email'] == 1 ? 'Yes' : 'No') ; ?></td>
                    </tr>
                <?php }; ?>
            </tbody>
        </table>
        </div>
      </div>
    </section>
</section>