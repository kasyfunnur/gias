<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Approval Status</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>"
</head>

<body>
<div class="container">
	<div class="row">
    	<div class="col-xs-12">
    	<?php echo (isset($msg) ? ucfirst($msg) : '<em>No approval message...</em>'); ?>
        </div>
    </div>
</div>
</body>
</html>