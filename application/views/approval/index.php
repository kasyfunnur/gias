<!-- Content Header (Page header) -->

<section class="content-header">
  <h1>My Approval</h1>
</section>

<!-- Main content -->
<section class="content"> 
	<?php if(isset($msg)) echo $msg ;?>
	<div class="box box-info">
        <div class="box-header">
        	<h3 class="box-title">
        	<?php
				$attributes = array(
					'class' 	=> 'form-inline',
					'role'		=> 'form',
					'method' 	=> 'post', 
					'name'		=> 'approval_list_form', 
					'id' 		=> 'approval_list_form'
					);
				echo form_open('', $attributes);
			?>
            	<div class="form-group">
            	<label class="control-label" for="sel_doc">Document</label>
            	<select name="sel_doc" id="sel_doc" class="form-control" onChange="filterdoc(this);">
                	<option value="0" <?php if ($selected_doc == 0) echo 'selected'; ?>>All Documents (<?php echo $total_pending ; ?>)</option>
                    <?php 
						foreach ($document_count as $doc){
							$selected = ($selected_doc == $doc['appr_id']) ? 'selected' : '';
							echo "<option value='$doc[appr_id]' $selected>$doc[appr_name] ($doc[approval_count])</option>";	
						}
					?>
                </select>
                </div>
            </form>
           </h3>
        </div>
        <div class="box-body">
          <table id="asset_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Document</th>
                    <th>Document Number</th>
                    <th>Requestor</th>
                    <th>Request Date</th>
                    <th>Approval Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php //var_dump($approval_list);?>
            	<?php foreach ($approval_list as $appr) {?>
                	<tr>
                    
                    	<td><?php echo $appr['appr_name']; ?></td>
                        <td>							
                            <?php if($appr['appr_name'] == 'Purchase Order'){ ?>
                                <a href="<?php echo site_url('purchase/purchase_order_inbox');?>/<?php echo $appr['doc_number'];?>">
                                    <?php echo $appr['doc_number'];?>
                                </a>
                            <?php } else {?>
                            	<?php echo $appr['doc_number']; ?>
                            <?php } ?>
                        </td>
                        <td><?php echo $appr['requestor_full_name']; ?></td>
                        <td><?php echo $appr['request_datetime']; ?></td>
                        <td><?php 
								$disable_action = FALSE;
								
								switch($appr['approver_action']){
									case 1:
										echo 'Awaiting';
										break;
									case 2:
										echo 'Partially Approved';
										break;
									case 3:
										echo 'Approved';
										$disable_action = TRUE;
										break;
									case 4:
										echo 'Revised';
										$disable_action = TRUE;
										break;
									case 5:
										echo 'Rejected';
										$disable_action = TRUE;
										break;
									default :
										echo 'Awaiting';
										break;
								}; ?></td>
                        <td>
                        	<?php if (!$disable_action) { ?>
                        	<a class="btn btn-flat btn-success" href="<?php echo site_url("approval/approve_document/$appr[unique_key]/$appr[doc_approval_id]/3")?>">Approve</a>
                            
                            <!-- <a class="btn btn-flat btn-warning" href="<?php echo site_url("approval/approve_document/$appr[unique_key]/$appr[doc_approval_id]/4")?>">Revise</a> -->
                            
                            <a class="btn btn-flat btn-danger" href="<?php echo site_url("approval/approve_document/$appr[unique_key]/$appr[doc_approval_id]/5")?>">Reject</a>
                            
                            <?php }; ?>
                        </td>
                    </tr>
                <?php }; ?>
            </tbody>
        </table>
        </div>
      </div>
    </section>
</section>
<!-- /.content -->

<script>
	function filterdoc(selobject)
	{
		window.location = '<?php echo site_url('approval/approval_list'); ?>' + '/' + selobject.value;
	}
</script>