<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><a class="" href="<?php echo site_url('approval/settings');?>">Approval Settings</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
    <?php echo $settings_detail[0]['appr_name'] . " ({$settings_detail[0]['appr_code']})" ?></h1>
</section>

<!-- Main content -->
<section class="content"> 
	<div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
          <table id="asset_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Settings Detail ID</th>
                    <th>Request by</th>
                    <th>Approval matrix</th>
                    <th>Step based ?</th>
                    <th>Auto approve ?</th>
                </tr>
            </thead>
            <tbody>
            	<?php foreach ($settings_detail as $appr) { ?>
                	<tr>
                    	<td><?php echo $appr['appr_detail_id']; ?></td>
                        <td><?php echo htmlspecialchars($appr['position_name']) ; ?></td>
                        <td><?php echo $appr['approval_matrix']; ?></td>
                        <td><?php echo $appr['step_based']; ?></td>
                        <td><?php echo $appr['auto_approve']; ?></td>
                    </tr>
                <?php }; ?>
            </tbody>
        </table>
        </div>
      </div>
    </section>
</section>