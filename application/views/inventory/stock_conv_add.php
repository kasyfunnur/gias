<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('inventory/conversion');?>">Stock Conversion</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New 
    </h1>
</section>

<!-- Main content -->
<section class="content">
	<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>	
    <div class="row">
    	<div class="col-sm-6">
        	
            <div class="form-group">
                <label class="col-sm-3" for="doc_code">
                    Document Code
                </label>
                <div class="col-sm-9">  
                    <input type="text" class="form-control input-sm" id="doc_code" name="doc_code" value="" >
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3" for="doc_date">
					Document Date
                </label>
                <div class="col-sm-9">  
					<input type="date" class="form-control input-sm" id="doc_date" name="doc_date" value="" >
                </div>
            </div>

            <div class="form-group">
               <div class="col-sm-9">
                    Outgoing
                </div> 
            </div>

            <div class="form-group">
                <label class="col-sm-3" for="inventory_out">Inventory</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('inventory_out', $inventory, 'item_code', 'item_name');?>
                </div>
            </div> 

             <div class="form-group">
                <label class="col-sm-3" for="qty_out">Qty</label>
                <div class="col-sm-9">
                   <input type="text" class="form-control input-sm" id="qty_out" name="qty_outgoing" value="" >
                </div>
            </div> 

            <div class="form-group">
                <label class="col-sm-3" for="warehouse_out">Warehouse</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('warehouse_out', $warehouse, 'location_code', 'location_name');?>
                </div>
            </div>  

            <div class="form-group">
               <div class="col-sm-9">
                    Incoming
                </div> 
            </div>

            <div class="form-group">
                <label class="col-sm-3" for="inventory_in">Inventory</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('inventory_in', $inventory, 'item_code', 'item_name');?>
                </div>
            </div> 

             <div class="form-group">
                <label class="col-sm-3" for="qty_in">Qty</label>
                <div class="col-sm-9">
                   <input type="text" class="form-control input-sm" id="qty_in" name="qty_incoming" value="" >
                </div>
            </div> 

            <div class="form-group">
                <label class="col-sm-3" for="warehouse_in">Warehouse</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('warehouse_in', $warehouse, 'location_code', 'location_name');?>
                </div>
            </div>  
           
        </div> 

         
    </div>   

    <br />
    <button type="submit" class="btn btn-info pull-right">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
    <br />
	</form>
</section>

<?php 
    echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script>
/*var source_location = <?php //echo json_encode($ddl_whse);?>;
var source_vendor = <?php //echo json_encode($dialog_vendor);?>;
var source_inventory = <?php //echo json_encode($dialog_inventory);?>;*/

var return_url = "<?php echo site_url('inventory/conversion');?>";
var submit_url = "<?php echo site_url('inventory/stock_conv_add_query');?>";

function validateForm(){
    if(confirm("Are you sure?")==true){    
        $.post(
            submit_url,
            $('#frm').serialize(),
            function(response){                 
                if(response==1){
                    alert("Data updated successfully!");
                    window.location.href=return_url;
                }else{
                    console.log("ERROR: "+response);
                }
            }
        );
    }
    return false;
}
</script>
<!-- /.content -->

<!--<section class="modal">-->

