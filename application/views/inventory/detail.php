<section class="content-header">
    <h1><a class="" href="<?php echo site_url('inventory/stock');?>">Inventory</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        <?php echo $item_detail['item_code'];?> </h1>
</section>

<section class="content">
<?php echo $msg?>
<!--<section class="content">-->
	<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>	
    <div class="row">
    	<div class="col-sm-5">
            <div class="form-group">
            	<div class="thumbnail" style="display:none">
	                <img src="<?php echo site_url($item_detail['item_image']);?>" width="500" height="500"/>
                </div>                
            </div> 
			<!--<div class="form-group">
            	<div class="col-sm-6 col-xs-6"><button class="btn btn-info col-sm-12 col-xs-12" value="Prev">Prev</button></div>
                <div class="col-sm-6 col-xs-6"><button class="btn btn-info col-sm-12 col-xs-12 pull-right" value="Prev">Next</button></div>
            </div>-->
            
            <!-- <a href="#" class="btn btn-default"><i class="fa fa-download"></i> Download PDF</a> -->
            <div style="overflow:auto;height:200px;border:solid;border-width:thin;border-color:#999">
                <em>Historical Transaction</em>
            	<table class="table table-condensed">
                <thead>
                	<tr>
                    	<th>Date</th>
                        <th>Doc</th>
                        <th style="text-align:right">Qty</th>
                        <th style="text-align:right">Balance</th>
                    </tr>
                </thead>
               	<tbody>
                <?php //echo var_dump($item_transaction);?>
				<?php foreach($item_transaction as $transaction) { ?>
                	<tr>
						<td><?php echo date("d M Y",strtotime($transaction['sysdate']));?></td>
                        <td>
							<?php echo($transaction['trx_status'] == "REVERSE") ? $transaction['doc']."(".$transaction['trx_status'].")" : $transaction['doc'];?>                        
                        </td>
                        <td style="text-align:right"><?php echo $transaction['qty'];?></td>
                        <td style="text-align:right">
							<?php 
								/*if(isset($transaction['buss_name1'])){
									echo $transaction['buss_name1'];
								}elseif(isset($transaction['buss_name2'])){
									echo $transaction['buss_name2'];
								}else{
									echo $transaction['location_name'];
								}*/	
							?>
                            <?php echo $transaction['balance'];?>
                        </td>
                <?php } ?>
                </tbody>
                </table>
            </div>
        </div>
    	<div class="col-sm-5 col-sm-offset-1">
        	<div class="form-group" style="color: #878787 !important;font-size: medium !important;font-weight: lighter !important;">
                <div style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%">  
                    <div><h3><?php echo $item_detail['item_code'];?></h3></div>  
                    <div><h4><?php echo $item_detail['item_name'];?></h4></div>
                </div>  
                <table style="margin:2px 0;text-align:left;width:100%"> 
                    <tbody>
                        <tr> 
                            <td style="width:55%">Category</td> 
                            <td style="width:25%">UoM</td> 
                        </tr> 
                        <tr style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%"> 
                            <td><h3><?php echo $item_detail['item_category'];?></h3></td> 
                            <td><h3><?php echo $item_detail['uom'];?></h3></td> 
                        </tr> 
                        <tr> 
                            <td style="width:55%">Sell Price</td> 
                            <td style="text-align:right">Status</td> 
                        </tr>
                        <tr style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%"> 
                            <td><h3><?php echo $item_detail['item_sell_price'];?></h3></td> 
                            <td style="text-align:right"><h3><?php echo $item_detail['item_status'];?></h3></td> 
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="form-group">
            	<div style="color: #878787 !important;font-size: medium !important;font-weight: lighter !important;">
                    <table style="margin:2px 0;text-align:left;width:100%" id="item_warehouse">                    
                    <thead>
                    <tr style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%">
                    	<th>WH</td>
                        <th style="text-align:right;width:20%">Bal</td>
                        <th style="text-align:right;width:20%">In</td>
                        <th style="text-align:right;width:20%">Out</td>
                        <th style="text-align:right;width:20%">Avail</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php for($zz = 0; $zz < count($item_warehouse);$zz++){	
						$onhand = $item_warehouse[$zz]['qty_onhand'];
						$tocome = $item_warehouse[$zz]['qty_tocome'];
						$togo   = $item_warehouse[$zz]['qty_togo'];
						$avail  = $onhand + $tocome - $togo;
					?>
                    <tr>
                    	<td><h3><?php echo $item_warehouse[$zz]['location_name'];?></h3></td>
                        <td style="text-align:right"><a href="#"><h3><?php echo $onhand;?></h3></a></td>
                        <td style="text-align:right"><a href="#"><h3><?php echo $tocome;?></h3></a></td>
                        <td style="text-align:right"><a href="#"><h3><?php echo $togo;?></h3></a></td>
                        <td style="text-align:right"><h3><?php echo $avail;?></h3></td>
                    </tr>
                    <?php }?>                            
                    </tbody>
                    <tfoot>
                    <?php 
                    	$all_onhand = $item_detail['qty_onhand'];
						$all_tocome = $item_detail['qty_tocome'];
						$all_togo   = $item_detail['qty_togo'];
						$all_avail  = $all_onhand + $all_tocome - $all_togo;
					?>
                    <tr>
                    	<td><h3>Total</h3></td>
                        <td style="text-align:right"><a href="#"><h3><?php echo $all_onhand;?></h3></a></td>
                        <td style="text-align:right"><a href="#"><h3><?php echo $all_tocome;?></h3></a></td>
                        <td style="text-align:right"><a href="#"><h3><?php echo $all_togo;?></h3></a></td>
                        <td style="text-align:right"><h3><?php echo $all_avail;?></h3></td>
                    </tr>
                    </tfoot>
                    </table>
				</div>
            </div>
        </div>
    </div>    
    <br />
    <button type="submit" class="btn btn-info pull-right">
    	<i class="fa fa-edit"></i>&nbsp;&nbsp; Edit
    </button><br /><br />
	</form>
</section>
<!-- /.content -->