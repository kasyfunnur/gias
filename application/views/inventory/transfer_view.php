<!-- Content Header (Page header) -->
<?php //BUTTON GROUP
$list_action = array(
    array('link'=>base_url('/inventory/transfer_duplicate')."/".$form_header[0]['doc_num'],'icon'=>'files-o','text'=>'Duplicate'),    
    array('link'=>base_url('/inventory/transfer_cancel')."/".$form_header[0]['doc_num'],'icon'=>'ban','text'=>'Cancel'),
    array('link'=>base_url('/inventory/transfer_receipt')."/".$form_header[0]['doc_num'],'icon'=>'minus-circle','text'=>'Close')
);
?>
<?php
    $attributes = array(
        'class'     => 'form-horizontal',
        'role'      => 'form',
        'method'    => 'post', 
        'name'      => 'frm', 
        'id'        => 'frm',
        'onSubmit'  => 'return validateForm();'
        );
    echo form_open('', $attributes);
    echo form_text('','hidden','doc_id',$form_header[0]['doc_id']);
    echo form_text('','hidden','whse_from',$form_header[0]['whse_from']);
    echo form_text('','hidden','whse_to',$form_header[0]['whse_to']);
?>
<section class="content-header">
    <h1>
    	<a class="" href="<?php echo site_url('inventory/transfer');?>">Stock Transfer</a> 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        View 
        &nbsp;&nbsp;&nbsp;
        <div class="pull-right button_action" id="button_action"></div>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
    	<div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-3" for="whse_from">From Location</label>
                <div class="col-sm-9">
                    <?php //echo sw_CreateSelect('whse_from', $ddl_whse, 'location_id', 'location_name');?>
                    <input type="text" class="form-control input-sm" name="whse_from_name" id="whse_from_name" 
                    	value="<?php echo $form_header[0]['whse_from_name'];?>" readonly/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="whse_to">To Location</label>
                <div class="col-sm-9">
                    <?php //echo sw_CreateSelect('whse_to', $ddl_whse, 'location_id', 'location_name');?>
                    <input type="text" class="form-control input-sm" name="whse_to_name" id="whse_to_name"
                    	value="<?php echo $form_header[0]['whse_to_name'];?>" readonly/>
                    <input type="hidden" name="whse_to" id="whse_to" value="<?php //echo $form_header['whse_to'];?>" />
                </div>
            </div>            
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note" readonly="readonly"><?php echo $form_header[0]['doc_note'];?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="receipt_note">Receipt Note</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="receipt_note" name="receipt_note">
                    </textarea>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $form_header[0]['doc_num'];?>" readonly>
                </div>
            </div>
        	<div class="form-group">
                <label class="col-sm-3" for="doc_dt">Send Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" 
                    	value="<?php echo substr($form_header[0]['doc_dt'],0,10);?>" readonly="readonly">
                </div>
            </div>		
            <div class="form-group col-sm-12">
                <label class="col-sm-3" for="doc_ddt">Estimate Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-3" for="receipt_dt">Receipt Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="receipt_dt" name="receipt_dt" 
                    	value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>		
        </div>
    </div>    
        
    <div class="row">
    	<div class="col-sm-12">
        	<table class="table-bordered" id="table_detail">
            <thead style="text-align:center">
            <tr>
            	<th width="10%">#</th>
                <th width="20%">
                	<!--<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                        <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Item Code</span>
						<span class="visible-xs"><i class="fa fa-search"></i></span>
                        <?php //echo sw_altButton('Item_code','fa-search');?>
                    </button>-->Item Code
                </th>
                <th width="20%">Item Name</th>
                <th width="25%">Qty</th>
                <!--<th width="25%">Qty Receipt</th>-->
                <!--<th width="20%">Action</th>-->
                <!--<td width="15%">Price</td>
                <td width="15%">Total</td>-->
            </tr>
            </thead>
            <tbody>
            <tr height="30px" id="default">
            	<td style="text-align:center"><input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/></td>
                <td><input type="text" name="item_code[]" id="item_code1" class="form-control" readonly></td>
                <td><input type="text" name="item_name[]" id="item_name1" class="form-control" readonly></td>
                <td><input type="number" name="item_qty[]"  id="item_qty1"  class="form-control" readonly></td>
                <!--<td><input type="number" name="item_qty_receipt[]"  id="item_qty_receipt1"  class="form-control"></td>-->
                <!--<td><input type="text" name="ast_price1"  id="ast_price1"  class="form-control"></td>
                <td><input type="text" name="sub_total1"  id="sub_total1"  class="form-control" readonly></td>-->
            </tr>
            </tbody>
            </table>
    	</div>
    </div>    
    <br />	
</section>
</form>

<template class="button_action">
    <?php $status = doc_status($form_header[0]['doc_status']);?>
    <div class="btn btn-<?php echo $status['color'];?>">
        Doc Status : <?php echo $status['status'];?>
    </div>
    <?php //echo form_button_action('View','success',$list_download);?>
    <?php echo form_button_action('Action','success',$list_action);?>
    <a id="next" class="btn btn-info" href="<?php echo base_url('/purchase/purchase_receipt_add/')."/".$form_header[0]['doc_num'];?>">
        <i class="fa fa-sign-out"></i>&nbsp;&nbsp; Acceptance
    </a>
    <?php $disable_edit = ($form_header[0]['doc_status'] == 'DRAFT') ? '' : 'disabled';?>
    <button type="submit" class="btn btn-info" id="submit" <?php echo $disable_edit;?>>
        <i class="fa fa-check"></i>&nbsp;&nbsp; Update
    </button>
</template>
<!-- /.content -->


<?php 
	//echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
	//var_dump($form_header);
	//var_dump($form_detail);
?>
<script>
var source_location = <?php echo json_encode($ddl_whse);?>;

var form_header = <?php echo json_encode($form_header);?>;
var form_detail = <?php echo json_encode($form_detail);?>;

var return_url = "<?php echo site_url('inventory/transfer');?>";
var submit_url = "<?php echo site_url('inventory/transfer_receipt_query');?>/"+form_header['doc_num'];
var ajax_url_1 = "<?php echo base_url('inventory/ajax_inventory_info');?>";

</script>
