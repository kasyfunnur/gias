<section class="content-header">	
  <h1>Style List
  	<a href="<?php echo site_url('inventory/style_add');?>" class="btn btn-flat btn-info pull-right">
      	<i class="fa fa-plus"></i>&nbsp;&nbsp;Add New
    </a>
  </h1>
</section>
<section class="content">
<?php echo $msg?>
<div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body table-responsive">
      <table id="inventory_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
            	<!---<th>#</th>--->
                <th>Style Code</th>
                <th>Name</th>
                <th>Date Launch</th>
                <!---<th>Color Variance</th>--->
            </tr>
        </thead>
        <!---<tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>
            <tr>
            	<td><?php echo $x+1;?></td>
	            <td><?php echo $datatable[$x]['item_style_char'];?></td>
                <td><?php echo $datatable[$x]['item_style'];?></td>
                <td><?php echo $datatable[$x]['count'];?></td>
                <td><?php echo $datatable[$x]['color'];?></td>
            </tr>
            <?php } ?>
        </tbody>--->
    </table>
    </div>
  </div>
</section>
