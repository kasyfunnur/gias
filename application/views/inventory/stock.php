<section class="content-header">	
  <h1>Inventory List
  	<a href="<?php echo site_url('inventory/stock_add');?>" class="btn btn-flat btn-info pull-right">
      	<i class="fa fa-plus"></i>&nbsp;&nbsp;Add New
    </a>
  </h1>
</section>
<section class="content">
<?php echo $msg?>
<div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body table-responsive">
      <table id="inventory_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Code</th>
                <th>Name</th>
                <th>On Hand + </th>
                <th>Incoming - </th>
                <th>Outgoing = </th>
                <th>Available</th>
                <th>Status</th>
            </tr>
        </thead>
    </table>
    </div>
  </div>
</section>
