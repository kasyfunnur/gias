<style>
h4 {
	line-height:0.2em; 
	margin:0px 0px 0px 0px;
}
</style>
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('inventory/stock');?>">Inventory</a>
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Style 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
		<?php echo $form_header[0]['style_name'];?> 
    </h1>
</section>
<?php //var_dump($form_header);?>


<!---<div id="swp_item" ontouchstart="touchStart(event,'swp_item');" ontouchend="touchEnd(event);" ontouchmove="touchMove(event);" ontouchcancel="touchCancel(event);">
This item can be swipe..<br />
This item can be swipe..<br />
This item can be swipe..<br />
This item can be swipe..<br />
This item can be swipe..<br />
This item can be swipe..<br />
This item can be swipe..<br />
This item can be swipe..<br />
This item can be swipe..<br />
</div>--->

<section class="content">
<?php echo $msg?>
<!---<section class="content">--->
	<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>	
    <div class="row">
    	<div class="col-sm-4">
            <div class="form-group">
            	<div class="thumbnail">
	                <img src="<?php echo site_url($form_header[0]['style_default_image']);?>" width="500" height="500"/>
                </div>                
            </div> 
			<div class="form-group">
            	<div class="col-sm-6 col-xs-6"><button class="btn btn-info col-sm-12 col-xs-12" value="Prev">Prev</button></div>
                <div class="col-sm-6 col-xs-6"><button class="btn btn-info col-sm-12 col-xs-12 pull-right" value="Prev">Next</button></div>
            </div>
        </div>
    	<div class="col-sm-offset-1 col-sm-6">
        	<div class="form-group" style="color: #878787 !important;font-size: medium !important;font-weight: lighter !important;">
                <!---<div style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%">  
                    <div style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%">Code<h3><?php echo $form_header[0]['style_code'];?></h3></div>  
                    <div style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%">Name<h4><?php echo $form_header[0]['style_name'];?></h4></div>
                </div>  
                <br /><br />--->
                <div style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%">
					Code<div class="pull-right"><?php echo $form_header[0]['style_code'];?></div>
                </div>
                <div style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%">
					Name<div class="pull-right"><?php echo $form_header[0]['style_name'];?></div>
                </div>
                <br /><br />
                <table style="margin:2px 0;text-align:left;width:100%"> 
                    <tbody>
                        <tr style="border-top:1px solid #ddd;padding:0 0 3px 0;width:100%"> 
                            <td style="width:35%">Date Launch</td> 
                            <td style="width:35%">Category</td> 
                            <td style="text-align:right">Selling Price</td> 
                        </tr> 
                        <tr style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%"> 
                            <td><h3><?php echo date("M-Y",strtotime($form_header[0]['style_date']));?></h3></td> 
                            <td><h3><?php echo $form_header[0]['category_name'];?></h3></td> 
                            <td style="text-align:right">
                            	<h3>IDR <?php echo number_format($form_detail[0]['item_sell_price'],0);?></h3>
                            </td> 
                        </tr> 
                        <!---<tr> 
                            <td style="width:55%">Sell Price</td> 
                            <td style="width:25%">MF Code</td> 
                            <td style="text-align:right">Status</td> 
                        </tr> 
                        <tr style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%"> 
                            <td><h3><?php echo $form_header[0]['item_sell_price'];?></h3></td> 
                            <td><h3><?php echo $form_header[0]['item_manufacturer_char'];?></h3></td> 
                            <td style="text-align:right"><h3><?php echo $form_header['item_status'];?></h3></td> 
                        </tr>--->
                    </tbody>
                </table>
            <!---</div>        	   
            <div>--->
            	<br />
            	<table class="table table-condensed">
                	<thead>
                    <tr style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%">
                    	<!---<th style="text-align:left;width:26%">Code</th>--->
                        <th style="text-align:left;width:26%">Name</th>
                        <!---<th>Color</th>--->
                        <th style="text-align:right;width:12%">Bal</th>
                        <th style="text-align:right;width:12%">In</th>
                        <th style="text-align:right;width:12%">Out</th>
                        <th style="text-align:right;width:12%">Avail</th>
					</tr>
                    </thead>
                    <tbody>
                    <?php for($zz = 0; $zz < count($form_detail);$zz++){	
						$onhand = $form_detail[$zz]['qty_onhand'];
						$tocome = $form_detail[$zz]['qty_tocome'];
						$togo   = $form_detail[$zz]['qty_togo'];
						$avail  = $onhand + $tocome - $togo;
					?>
                    <tr style="border-bottom:1px solid #ddd;padding:0 0 3px 0;width:100%">
                    	<td>
							<?php echo $form_detail[$zz]['item_name'];?><br />
							<strong><?php echo $form_detail[$zz]['item_code'];?></strong>
                        </td>
                        <!---<td><?php //echo $form_detail[$zz]['item_name'];?>&nbsp;</td>--->
                        <!---<td><?php echo $form_detail[$zz]['item_color'];?></td>--->
                        <td style="text-align:right"><a href="#"><?php echo $onhand;?></a></td>
                        <td style="text-align:right"><a href="#"><?php echo $tocome;?></a></td>
                        <td style="text-align:right"><a href="#"><?php echo $togo;?></a></td>
                        <td style="text-align:right"><?php echo $avail;?></td>
                    </tr>
                    <?php }?> 
                    </tbody>
                </table>
			</div>   
        </div>
    </div>    

    
    <br />
    <button type="submit" class="btn btn-info pull-right">
    	<i class="fa fa-edit"></i>&nbsp;&nbsp; Edit
    </button>
	</form>
</section>
<!-- /.content -->