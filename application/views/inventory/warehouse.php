<section class="content-header">
    <h1>Warehouse List
    <a href="#" class="btn btn-flat btn-info pull-right" title="Not Yet Applied">
        <i class="fa fa-plus"></i>&nbsp;&nbsp;Add New
    </a>
  </h1>
</section>
<section class="content">
    <?php echo $msg?>
    <div class="box box-info">
        <div class="box-header"></div>
        <div class="box-body">
            <table id="inventory_list" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Address</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</section>
