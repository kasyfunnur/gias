<section class="content-header">	
  <h1>Inventory Category
  	<a href="<?php echo site_url('inventory/category_add');?>" class="btn btn-flat btn-info pull-right">
      	<i class="fa fa-plus"></i>&nbsp;&nbsp;Add New
    </a>
  </h1>
</section>
<section class="content">
<div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body">
      <table id="category_list" class="table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Code</th>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
        	<?php foreach ($categories as $c) { ?>
        		<tr>
        			<td><a href="<?php echo site_url('inventory/category_edit') . '/' . $c['category_id'] ?>" title=""><?php echo $c['category_code'] ?></a>
        			</td>
        			<td><?php echo $c['category_name'] ?></td>
        		</tr>
        	<?php } ?>
        </tbody>
    </table>
    </div>
  </div>
</section>