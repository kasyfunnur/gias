<section class="content-header">
    <h1><a class="" href="<?php echo site_url('inventory/warehouse');?>">Warehouse</a> &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
    Edit Warehouse </h1>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
        <?php if(isset($msg)) echo $msg ?>
        </div>
        <div class="box-body">
            <?php $attributes=array( 'class'=> 'form-horizontal', 'role' => 'form', 'method' => 'post' ); echo form_open('', $attributes); ?>
            <input type="hidden" name="location_id" value="<?php echo $whid ?>">
            <div class="form-group">
                <label for="location_code" class="col-sm-2 control-label">Location Code</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="location_code" id="location_code" placeholder="Location Code" value="<?php echo $whdata['location_code'] ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="location_name" class="col-sm-2 control-label">Location Name</label>
                <div class="col-sm-10">
                    <input type="text" name="location_name" id="location_name" class="form-control" value="<?php echo $whdata['location_name'] ?>" required="required">
                </div>
            </div>
            <div class="form-group">
                <label for="location_address" class="col-sm-2 control-label">Location Address</label>
                <div class="col-sm-10">
                    <input type="text" name="location_address" id="location_address" class="form-control" value="<?php echo $whdata['location_address'] ?>" required="required">
                </div>
            </div>
            <div class="form-group">
                <label for="location_phone" class="col-sm-2 control-label">Location Phone</label>
                <div class="col-sm-10">
                    <input type="text" name="location_phone" id="location_phone" class="form-control" value="<?php echo $whdata['location_phone'] ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="location_city" class="col-sm-2 control-label">Location City</label>
                <div class="col-sm-10">
                    <input type="text" name="location_city" id="location_city" class="form-control" value="<?php echo $whdata['location_city'] ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="location_state" class="col-sm-2 control-label">Location State</label>
                <div class="col-sm-10">
                    <input type="text" name="location_state" id="location_state" class="form-control" value="<?php echo $whdata['location_state'] ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="location_country" class="col-sm-2 control-label">Location Country</label>
                <div class="col-sm-10">
                    <input type="text" name="location_country" id="location_country" class="form-control" value="<?php echo $whdata['location_country'] ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="location_description" class="col-sm-2 control-label">Location Description</label>
                <div class="col-sm-10">
                    <input type="text" name="location_description" id="location_description" class="form-control" value="<?php echo $whdata['location_description'] ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_active" <?php echo ($whdata[ 'is_active']) ? 'checked' : '' ?> value="1"> Active ?
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_sales" <?php echo ($whdata[ 'is_sales']) ? 'checked' : '' ?> value="1"> Sales Outlet ?
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-info">Update</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>
