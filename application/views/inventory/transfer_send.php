<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    	<a class="" href="<?php echo site_url('inventory/transfer');?>">Stock Transfer</a> 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Add 
    </h1>
</section>

<!-- Main content -->
<section class="content">
	<?php $attributes = array(
        'class'     => 'form-horizontal',
        'role'      => 'form',
        'method'    => 'post', 
        'name'      => 'frm', 
        'id'        => 'frm',
        'onSubmit'  => 'return validateForm();'
        );
        echo form_open('', $attributes);?> 
    <div class="container-fluid">
        <div class="row">
        	<div class="col-sm-6">
                <div class="form-group col-sm-12">
                    <label class="col-sm-3" for="whse_from">From Location</label>
                    <div class="col-sm-9">
                        <?php echo sw_CreateSelect('whse_from', $ddl_whse, 'location_id', 'location_name');?>
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label class="col-sm-3" for="whse_to">To Location</label>
                    <div class="col-sm-9">
                        <?php echo sw_CreateSelect('whse_to', $ddl_whse, 'location_id', 'location_name');?>
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label class="col-sm-3" for="doc_note">Memo</label>
                    <div class="col-sm-9">
                        <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                    </div>
                </div>
            </div>
        	<div class="col-sm-6">
            	<div class="form-group col-sm-12">
                    <label class="col-sm-3" for="doc_num">Doc #</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                    </div>
                </div>	
                <div class="form-group col-sm-12">
                    <label class="col-sm-3" for="doc_ext_ref">Freight Doc #</label>
                    <div class="col-sm-3">
                    	<?php 
    						if(count($ddl_freight)==0){
    							$ddl_freight = array(0=>array('doc_freight'=>"",'doc_freight'=>""));
    							echo '<input class="form-control input-sm" type="text" name="doc_freight" id="doc_freight">';
    						}else{
    							echo sw_CreateSelect('doc_freight', $ddl_freight, 'doc_freight', 'doc_freight', "");
                            }
    					?>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm" id="doc_ext_ref" name="doc_ext_ref" value="">
                    </div>
                </div>
            	<div class="form-group col-sm-12">
                    <label class="col-sm-3" for="doc_dt">Date</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo date('Y-m-d',time());?>">
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label class="col-sm-3" for="doc_ddt">Estimate Date</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>">
                    </div>
                </div>
            </div>
        </div>    
            
        <div class="row">
        	<div class="col-sm-12">
                <div class="table-responsive" style="overflow:auto;">
                	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
                    <thead>
                    <tr>
                    	<th style="width:10%">#</th>
                        <th style="width:30%">                        	
                        	<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                                <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Item Code</span>
        						<span class="visible-xs"><i class="fa fa-search"></i></span>
                            </button>
                        </th>
                        <th style="width:30%">Item Name</th>
                        <th style="width:30%">Qty</th>
                        <th style="width:10%" class="action">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr id="default">
                    	<td style="text-align:center">
                            <input type="hidden" name="item_id[]" id="item_id1" class="form-control" value="">
                            <input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                        </td>
                        <!-- <td><input type="text" name="item_code[]" id="item_code1" class="form-control" readonly></td> -->
                        <td>
                            <div class="input-group">                       
                                <span class="input-group-btn">
                                    <a href="#" class="btn btn-warning form-control" name="a_ast_code" target="_blank">
                                        <i class="fa fa-external-link"></i>
                                    </a>                            
                                </span>
                                <input type="text" name="ast_code[]" id="ast_code1" class="form-control " readonly="readonly">
                            </div>
                        </td>
                        <td>
                            <input type="text" name="item_name[]" id="item_name1" class="form-control" readonly>
                        </td>
                        <td>
                            <input type="number" name="item_qty[]"  id="item_qty1"  class="form-control">
                        </td>
                        <!--<td><input type="text" name="ast_price1"  id="ast_price1"  class="form-control"></td>
                        <td><input type="text" name="sub_total1"  id="sub_total1"  class="form-control" readonly></td>-->
                        <td>
                        	<div class="input-group">
        						<div class='input-group-btn'>
                                    <button type="button" class="btn btn-flat btn-danger form-control" id="act_del1" name="act_del">
                                    	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
        	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                                    </button>
        						</div>
        					</div>
        				</td>
                    </tr>
                    </tbody>
                    </table>
                </div>
        	</div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right">
            	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
            </button>
        </div>
    </div>
	</form>
</section>
<!-- /.content -->

<?php //LAYOUT HELPER: MODAL
    //echo sw_createModal('modal_vendor','Select Vendor','dialog_vendor_push_data();','business/dialog_vendor');
    echo sw_createModal('modal_item','Select Item','dialog_inventory_push_data();','inventory/dialog_inventory');
?>

<?php 
	//echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>

<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
var source_inventory = <?php echo json_encode($dialog_inventory);?>;

var return_url = "<?php echo site_url('inventory/transfer');?>";
var submit_url = "<?php echo site_url('inventory/transfer_send_query');?>";
var ajax_url_1 = "<?php echo base_url('inventory/ajax_inventory_info');?>";

</script>
