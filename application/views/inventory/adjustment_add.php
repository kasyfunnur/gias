<!-- Content Header (Page header) -->
<style>
	.inactive td{background-color:#CCC;opacity: 0.5;}
	thead>tr>th{text-align:center;font-weight:bold;}
	#foot_label{text-align:right;}
</style>
<section class="content-header">
    <h1>
    	<a class="" href="<?php echo site_url('inventory/transfer');?>">Stock Adjustment</a> &nbsp;&nbsp;
    	<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Add </h1>
</section>


<!-- Main content -->
<section class="content">
	<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
	
    <div class="row">
    	<div class="col-sm-6">
            <!---<div class="form-group">
                <label class="col-sm-3" for="buss_name">
                	Vendor                    
                </label>
                <div class="col-sm-9"><input type="hidden" id="buss_id" name="buss_id" value="">
                	<div class="input-group input-group-sm">
                        <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="">                        
                    	<span class="input-group-btn">
                        	<button type="button" class="btn" id="btn_search_vendor" 
                            	onclick="javascript:void window.showModalDialog('<?php echo site_url("purchase/dialog_purchase_vendor");?>','Select Vendor','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;">Search</button>
						</span>                        
                    </div>                    
                </div>
            </div>--->
            <div class="form-group">
                <label class="col-sm-3" for="whse_id">Warehouse</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('whse_id', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>
            <!---<div class="form-group">
                <label class="col-sm-3" for="whse_to">To Location</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('whse_to', $ddl_whse, 'location_id', 'location_name');?>
                </div>
            </div>      --->      
            <!---<div class="form-group">
                <label class="col-sm-3" for="ship_addr">Shipping Address</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="ship_addr" name="ship_addr" readonly></textarea>
                </div>
            </div>--->
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note"></textarea>
                </div>
            </div>
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $doc_num;?>" readonly>
                </div>
            </div>	
        	<div class="form-group">
                <label class="col-sm-3" for="doc_dt">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>		
            <!---<div class="form-group">
                <label class="col-sm-3" for="doc_ddt">Date Required</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_ddt" name="doc_ddt" value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>--->
        </div>
    </div>    
        
    <div class="row">
    	<div class="col-sm-12">
        	<table class="table-bordered" id="table_detail">
            <thead style="text-align:center">
            <tr>
            	<th width="10%">#</th>
                <th width="30%">
                	<!---<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">Item</button>--->
                	<button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_item" style="width:100%;">
                        <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Item Code</span>
						<span class="visible-xs"><i class="fa fa-search"></i></span>
                        <?php //echo sw_altButton('Item_code','fa-search');?>
                    </button>
                </th>
                <th width="30%">Item Name</th>
                <th width="30%">Qty</th>
                <th style="width:10%" class="action">Action</th>
                <!---<td width="15%">Price</td>
                <td width="15%">Total</td>--->
            </tr>
            </thead>
            <tbody>
            <tr height="30px" id="default">
            	<td style="text-align:center"><input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/></td>
                <td><input type="text" name="item_code[]" id="item_code1" class="form-control" readonly></td>
                <td><input type="text" name="item_name[]" id="item_name1" class="form-control" readonly></td>
                <td><input type="number" name="item_qty[]"  id="item_qty1"  class="form-control"></td>
                <!---<td><input type="text" name="ast_price1"  id="ast_price1"  class="form-control"></td>
                <td><input type="text" name="sub_total1"  id="sub_total1"  class="form-control" readonly></td>--->
                <td>
                	<div style='text-align:center'>
						<div class='btn-group'>
                            <button type="button" class="btn btn-flat btn-danger" id="act_del1" name="act_del"><!---DELETE--->
                            	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
						</div>
					</div>
				</td>
            </tr>
            </tbody>
            </table>
    	</div>
    </div>
    <br />
    <button type="submit" class="btn btn-info pull-right">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
    <br />
	</form>
</section>
<!-- /.content -->

<!---<section class="modal">--->
<div class="modal fade" id="modal_item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title">Select Item
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <span class="hidden-xs"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</span>
                            <span class="visible-xs"><i class="fa fa-arrow-left"></i></span>
                        </button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" 
                        	onclick="dialog_inventory_push_data();"><!---Save & Close--->
                            <span class="hidden-xs"><i class="fa fa-save"></i>&nbsp;&nbsp;Save & Close</span>
                            <span class="visible-xs"><i class="fa fa-save"></i></span>
                        </button>
                    </div>
                </h4>                
            </div>
            <div class="modal-body">
				<?php 					
					$this->load->view('inventory/dialog_inventory');
				;?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <span class="hidden-xs"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</span>
                    <span class="visible-xs"><i class="fa fa-arrow-left"></i></span>
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" 
                    onclick="dialog_inventory_push_data();"><!---Save & Close--->
                    <span class="hidden-xs"><i class="fa fa-save"></i>&nbsp;&nbsp;Save & Close</span>
                    <span class="visible-xs"><i class="fa fa-save"></i></span>
                </button>
            </div>
        </div>
    </div>
</div>
<!---<div class="modal fade" id="modal_item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog" >
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title">Select Item
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="dialog_inventory_push_data();">Save & Close</button>
                    </div>
                </h4>                
            </div>
            <div class="modal-body">
				<?php 
					$data['datasource'] = $this->inventory_model->get_stock_all();
					$this->load->view('inventory/dialog_inventory',$data);	
				;?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="dialog_inventory_push_data();">Save & Close</button>
            </div>
        </div>
    </div>
</div>--->
<!---</section>--->
<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script>
var source_location = <?php echo json_encode($ddl_whse);?>;
var source_inventory = <?php echo json_encode($dialog_inventory);?>;

var return_url = "<?php echo site_url('inventory/adjustment');?>";
var submit_url = "<?php echo site_url('inventory/adjustment_add_query');?>";
var ajax_url_1 = "<?php echo base_url('inventory/ajax_inventory_info');?>";
$(document).ready(function(){	
	$("#location_id").on('change',function(){
		var result;
		for( var i = 0; i < source_location.length; i++ ) {
			if( source_location[i]['location_id'] == $(this).val() ) {
				result = source_location[i];
				break;
			}
		}
		$("#ship_addr").val(result['location_address']+', '+result['location_city']+', '+result['location_state']+', '+result['location_country']);
	});
	$("#location_id").val(1).change();	
	//$("#table_detail").colResizable();	
});
</script>
