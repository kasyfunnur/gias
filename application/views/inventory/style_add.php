<!-- Content Header (Page header) -->
<style>
	.inactive td{background-color:#CCC;opacity: 0.5;}
	thead>tr>th{text-align:center;font-weight:bold;}
	#foot_label{text-align:right;}
	label{		
		-moz-user-select: -moz-none;
		-khtml-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}	
</style>
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('inventory/style');?>">Style</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New </h1>
</section>

<!-- Main content -->
<section class="content">
	<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>	
    <div class="row">
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="group_id">Group</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('group_id', $ddl_group, 'item_g_id', 'group_label');?>
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-3" for="header_style_code">
                	Style Code                    
                </label>
                <div class="col-sm-9">
					<input type="text" class="form-control input-sm" id="header_style_code" name="header_style_code" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="header_style_name">
                	Style Name                    
                </label>
                <div class="col-sm-9">
					<input type="text" class="form-control input-sm" id="header_style_name" name="header_style_name" value="">
                </div>
            </div>            
            <div class="form-group">
                <label class="col-sm-3" for="ship_addr">Category</label>
                <div class="col-sm-9">
                    <?php echo sw_CreateSelect('category_id', $ddl_category, 'category_code', 'category_name');?>
                </div>
            </div>            
        </div>
    	<div class="col-sm-6">
        	<div class="form-group">
                <label class="col-sm-3" for="style_launch">Date Launch</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="style_launch" name="style_launch" 
                    	value="<?php echo date('Y-m-d',time());?>">
                </div>
            </div>
            <div class="form-group">
            	<label class="col-sm-3" for="colors">Pick a color</label>
                <div class="col-sm-9">
					<select size="4" name="issue_ref" multiple="multiple" style="height:120px;width:200px;">
                        <option style="color:red;"> Red 
                        <option> Green
                        <option> Blue
                        <option> Black
                        <option> Brown
                        <option> Beige
                        <option> Yellow
                        <option> Pink
                        <option> Orange                        
                    </select> 
                    <button id="pick_color">Choose</button>
                </div>
            </div>
        </div>
    </div>    

    <div class="row">
    	<div class="col-sm-12">
        	<div class="table-responsive" style="overflow:auto;">
        	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
            <thead>
            <tr>
            	<th style="width:5%">#</th>
                <th style="width:20%">Style Code</th>
                <th style="width:20%">Style Name</th>
                <th style="width:20%">Item Code</th>
                <th style="width:20%">Item Name</th>
                <th style="width:20%">Color</th>
                <th style="width:20%">Item Tag Name</th>
                <th style="width:15%" class="action">Action</th>
            </tr>
            </thead>            
            <tbody>
            <tr height="30px">
                <td style="text-align:center">
                	<input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/>
                </td>
                <td>
                	<input type="text" name="style_code[]" id="style_code1" class="form-control" readonly="readonly">
                </td>
                <td>
                	<input type="text" name="style_name[]" id="style_name1" class="form-control" readonly="readonly">
                </td>
                <td>
                	<input type="text" name="item_code[]" id="item_code1" class="form-control">
                </td>                
                <td>
                	<input type="text" name="item_name[]"  id="item_name1"  class="form-control">
                </td>
                <td>
                	<input type="text" name="item_color[]" id="item_color1" class="form-control">
                </td>
                <td>
                	<input type="text" name="item_tag[]"  id="item_tag1"  class="form-control">
                </td>
                <td>
                	<div style='text-align:center'>
						<div class='btn-group'>
                            <button type="button" class="btn btn-flat btn-danger" id="act_del1" name="act_del"><!---DELETE--->
                            	<span class="hidden-xs">&nbsp;&nbsp;DELETE</span>
	                            <span class="visible-xs"><i class="fa fa-ban"></i></span>
                            </button>
						</div>
					</div>
				</td>
            </tr>
            </tbody>
            <!---<tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                <td><div>
                    <input type="text" class="form-control" id="doc_total" name="doc_total" value="0" style="text-align:right;" readonly>
                    </div>
                </td>
            </tr>
            </tfoot>--->
            </table>
            </div>
    	</div>
    </div>
    <br />
    <button type="submit" class="btn btn-info pull-right">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
    <br />
	</form>
</section>
<!-- /.content -->

<!---<section class="modal">--->
<div class="modal fade" id="modal_item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title">Select Item
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <span class="hidden-xs"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</span>
                            <span class="visible-xs"><i class="fa fa-arrow-left"></i></span>
                        </button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" 
                        	onclick="dialog_inventory_push_data();"><!---Save & Close--->
                            <span class="hidden-xs"><i class="fa fa-save"></i>&nbsp;&nbsp;Save & Close</span>
                            <span class="visible-xs"><i class="fa fa-save"></i></span>
                        </button>
                    </div>
                </h4>                
            </div>
            <div class="modal-body">
				<?php 					
					$this->load->view('inventory/dialog_inventory');
				;?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <span class="hidden-xs"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</span>
                    <span class="visible-xs"><i class="fa fa-arrow-left"></i></span>
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" 
                    onclick="dialog_inventory_push_data();"><!---Save & Close--->
                    <span class="hidden-xs"><i class="fa fa-save"></i>&nbsp;&nbsp;Save & Close</span>
                    <span class="visible-xs"><i class="fa fa-save"></i></span>
                </button>
            </div>
        </div>
    </div>
</div>
<?php //echo sw_createModal('modal_vendor','Select Vendor','dialog_vendor_push_data();','business/dialog_vendor');?>
<div class="modal fade" id="modal_vendor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog" >
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title">Select Vendor
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" 
                        	onclick="dialog_vendor_push_data();">Save & Close</button>  
                    </div>
                </h4>                
            </div>
            <div class="modal-body">
				<?php 					
					$this->load->view('business/dialog_vendor');
				?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" 
                	onclick="dialog_vendor_push_data();">Save & Close</button>                
            </div>
        </div>
    </div>
</div>
<!---</section>--->
<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script>
/*var source_location = <?php //echo json_encode($ddl_whse);?>;
var source_vendor = <?php //echo json_encode($dialog_vendor);?>;
var source_inventory = <?php //echo json_encode($dialog_inventory);?>;*/

var return_url = "<?php echo site_url('inventory/style');?>";
var submit_url = "<?php echo site_url('inventory/style_add_query');?>";
var rowCount = 1;
function clone_row(table){
	rowCount++;
	var row = $("#"+table+" > tbody > tr:first").clone().find("input,select").each(function(){		
		var temp_name = $(this).attr('name');
		$(this).attr('id',function(_,id){return temp_name.substring(0,temp_name.length-2)+rowCount;});
		$(this).attr('name',function(_,name){return name;});
		$(this).val('');
	}).end().appendTo("#"+table+" > tbody");
	sky_tbl_refresh_row_num(table);
	return row;
}
function sky_tbl_refresh_row_num(tableid){
	$('#'+tableid+' > tbody > tr').each(function(){
		var t_idx = $(this).index()+1;
		$(this).find('td:eq(0) > input[name="row[]"]').val(t_idx);
	});	
}
function new_item_style_map(){
	$("#table_detail > tbody > tr").find('input[name^=style_code]').val($("#header_style_code").val());
	$("#table_detail > tbody > tr").find('input[name^=style_name]').val($("#header_style_name").val());
	$("#table_detail > tbody > tr").find('input[name^=item_color]',this).each(function(){	
		//if(! $(this).parent().parent().is(":last-child") || $(this).parent().parent().is(":first-child")){
			var clr = $(this).val().toUpperCase();;	
			var clr_str = clr.toLowerCase().replace(/\b[a-z]/g, function(letter){return letter.toUpperCase();});
			$(this).parent().parent().find('input[name^=item_name]').val($("#header_style_name").val() + " " + clr_str);
			$(this).parent().parent().find('input[name^=item_code]').val($("#style_launch").val().substring(2,4)+$("#category_id").val()+$("#header_style_code").val()+"_"+clr);
		//}
	});
	
}
$(document).ready(function(){	
	$("#table_detail").colResizable();	
	$("#group_id").attr('disabled','disabled');
	$(document).on('change','#table_detail > tbody > tr:last',function(){
		clone_row('table_detail');
		sky_tbl_refresh_row_num('table_detail');
	});
	
	$("#header_style_code, #header_style_name, #category_id, #table_detail").on('change',function(){
		new_item_style_map();
	});

});

function validateForm(){
	if(confirm("Are you sure?")==true){	
		var tbl_row = $("#table_detail > tbody > tr").length;
		$('<input>').attr({
			type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
		}).appendTo('form');	
		$.post(
			submit_url,
			$('#frm').serialize(),
			function(response){					
				if(response==1){
					alert("Data updated successfully!");
					window.location.href=return_url;
				}else{
					console.log("ERROR: "+response);
				}
			}
		);
	}
	return false;
}
</script>
