<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('inventory/transfer');?>">Stock Transfer</a> 
        &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        CANCELLING... 
    </h1>   
</section>

<!-- Main content -->
<?php $attributes = array(
    'class'     => 'form-horizontal',
    'role'      => 'form',
    'method'    => 'post', 
    'name'      => 'frm', 
    'id'        => 'frm',
    'onSubmit'  => 'return validateForm();'
    );
    echo form_open('', $attributes);?>
<section class="content">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-3" for="whse_from">From Location</label>
                <div class="col-sm-9">
                    <?php //echo sw_CreateSelect('whse_from', $ddl_whse, 'location_id', 'location_name');?>
                    <input type="text" class="form-control input-sm" name="whse_from_name" id="whse_from_name" 
                        value="<?php echo $form_header[0]['whse_from_name'];?>" readonly/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="whse_to">To Location</label>
                <div class="col-sm-9">
                    <?php //echo sw_CreateSelect('whse_to', $ddl_whse, 'location_id', 'location_name');?>
                    <input type="text" class="form-control input-sm" name="whse_to_name" id="whse_to_name"
                        value="<?php echo $form_header[0]['whse_to_name'];?>" readonly/>
                    <input type="hidden" name="whse_to" id="whse_to" value="<?php //echo $form_header['whse_to'];?>" />
                </div>
            </div>            
            <div class="form-group">
                <label class="col-sm-3" for="doc_note">Memo</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="doc_note" name="doc_note" readonly="readonly"><?php echo $form_header[0]['doc_note'];?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="receipt_note">Receipt Note</label>
                <div class="col-sm-9">
                    <textarea class="form-control input-sm" id="receipt_note" name="receipt_note" readonly>
                    </textarea>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="col-sm-3" for="doc_num">Doc #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control input-sm" id="doc_num" name="doc_num" value="<?php echo $form_header[0]['doc_num'];?>" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="doc_dt">Send Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="doc_dt" name="doc_dt" 
                        value="<?php echo substr($form_header[0]['doc_dt'],0,10);?>" readonly="readonly">
                </div>
            </div>      
            <div class="form-group">
                <label class="col-sm-3" for="receipt_dt">Receipt Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control input-sm" id="receipt_dt" name="receipt_dt" readonly
                        value="">
                </div>
            </div>      
        </div>
    </div>    
    <hr>
    <div class="row">
        <div class="col-sm-3">Reason for cancelling document:</div>
        <div class="col-sm-9">
        <textarea required name="reason" id="reason" style="width:500px"/></textarea>
        </div>
    </div>

    <!-- <div class="row">
        <div class="col-sm-12">
            <table class="table-bordered" id="table_detail">
            <thead style="text-align:center">
            <tr>
                <th width="10%">#</th>
                <th width="20%">
                    Item Code
                </th>
                <th width="20%">Item Name</th>
                <th width="25%">Qty</th>
            </tr>
            </thead>
            <tbody>
            <tr height="30px" id="default">
                <td style="text-align:center"><input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/></td>
                <td><input type="text" name="item_code[]" id="item_code1" class="form-control" readonly></td>
                <td><input type="text" name="item_name[]" id="item_name1" class="form-control" readonly></td>
                <td><input type="number" name="item_qty[]"  id="item_qty1"  class="form-control" readonly></td>                
            </tr>
            </tbody>
            </table>
        </div>
    </div>     -->
    <br />  
    <button type="submit" class="btn btn-info pull-right">
        <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
</section>
</form>


<!-- /.content -->


<?php 
    //echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
    //var_dump($form_header);
    //var_dump($form_detail);
?>
<script>
var return_url = "<?php echo site_url('inventory/transfer');?>";
var submit_url = "<?php echo site_url('inventory/transfer_cancel_query');?>/"+'<?php echo $form_header[0]['doc_num'];?>'; //form_header['doc_num'];
</script>