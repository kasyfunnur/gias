<section class="content-header">	
  <h1> Stock Adjustment List
	<a href="<?php echo site_url('inventory/adjustment_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<span class="hidden-xs">
        		<i class="fa fa-plus"></i>&nbsp;&nbsp;New Adjustment
            </span>
        	<span class="visible-xs">
        		<i class="fa fa-plus"></i>
            </span>
        </button>
    </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg:""?>

<section class="content">
<div class="box box-info">
    <div class="box-header"></div>
    <div class="box-body table-responsive">
	<table class="table table-hover">
        <thead>
            <tr>
            	<th style="width:6%">#</th>
                <th style="width:8%">Doc #</th>
                <th style="width:8%">Date</th>
                <th style="width:8%">Status</th>
                <th style="width:10%">Warehouse</th>
                <th style="width:10%">Note</th>
                <th style="width:30%">Action</th>
            </tr>
        </thead>
        <tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>
			<?php 
				$approval_status = $this->status_model->status_label('approval',$datatable[$x]['approval_status']);
                $document_status = $this->status_model->status_label('document',$datatable[$x]['doc_status']);
			?>
            <tr>
            	<td><?php echo $x+1;?>.</td> <!---class="hidden-xs"--->
                <td>
                	<a href="<?php echo site_url('inventory/adjustment_edit');?>/<?php echo $datatable[$x]['doc_num'];?>">
						<?php echo $datatable[$x]['doc_num'];?>
                    </a>
                </td>
                
                <td><?php echo date("Y-m-d", strtotime($datatable[$x]['doc_dt']));?></td>
                <td><?php echo $datatable[$x]['doc_status'];?></td>
                <!---<td><?php echo $approval_status;?></td>--->
                <td><?php echo $datatable[$x]['location_name'];?></td>
                <td><?php echo $datatable[$x]['doc_note'];?></td>
				<td> 
                	<?php 
						$disable = 'disabled';
						if($datatable[$x]['doc_status'] != "CANCELLED" && $datatable[$x]['doc_status'] != "CLOSED"){
							$disable = '';
						} 
					?>
                	<a href="<?php echo base_url('inventory/transfer_cancel')."/".$datatable[$x]['doc_num'];?>">                    
                    	<span class="btn btn-xs btn-default <?php echo $disable;?>" >
                        <i class="fa fa-ban"></i> Cancel</span></a>&nbsp;&nbsp;
					<a href="<?php echo base_url('inventory/transfer_close')."/".$datatable[$x]['doc_num'];?>">
                    	<span class="btn btn-xs btn-default <?php echo $disable;?>" >
                        <i class="fa fa-archive"></i> Close</span></a>&nbsp;&nbsp;
                    <a href="<?php echo base_url('inventory/transfer_print')."/".$datatable[$x]['doc_num'];?>">
                    	<span class="btn btn-xs btn-default" >
                        <i class="fa fa-print"></i> Print</span></a>&nbsp;&nbsp;
                    <a href="<?php echo base_url('inventory/transfer_receipt')."/".$datatable[$x]['doc_num'];?>">
                    	<span class="btn btn-xs btn-info <?php echo $disable;?>" >
                        <i class="fa fa-plus"></i> Acceptance</span></a>&nbsp;&nbsp;
                </td>
			</tr>
            <?php } ?>
		</tbody>
    </table>
    </div>
  </div>
</section>
