<style>
.modal-dialog {z-index:50000}
</style>
<div class="row">
	<div class="col-sm-12 col-xs-12">
    	Search : <input type="text" id="search_inv" name="search_inv" value=""/>
        <table class="table table-condensed table-hover table-striped" id="dialog_inventory_list">
        <thead>
            <tr>
                <th width="5%"><!--<input type="checkbox" name="inventory_chk_all" id="inventory_chk_all" />-->&nbsp;</th>
                <th class="hidden-xs" width="10%"><strong>SKU</strong></th>
                <th width="35%"><strong>Name</strong></th>
                <th class="hidden-xs" width="20%"><strong></strong></th>
                <!--<td class="hidden-xs" width="10%"><strong>Style</strong></td>
                <td class="hidden-xs" width="15%"><strong>Color</strong></td>-->
                <th width="30%" style="text-align:right;"><strong>On Hand</strong></th>
                <!-- <th width="30%" style="text-align:right;"><strong>Qty</strong></th> -->
            </tr>
        </thead>
        <tbody>   
        	<tr id="inventory_default_row">
            	<td width="5%"><input type="checkbox" name="chk[]" id="chk"/></td>
                <td class="hidden-xs" width="10%"><input type="hidden" name="item_id[]" id="item_id" /><span></span></td>
                <td width="35%"></td>
                <td class="hidden-xs" width="20%"></td>
                <td width="30%" style="text-align:right;"></td>
                <!-- <td width="30%" style="text-align:right;">
                	<input type="number" name="pick_qty[]" id="pick_qty" value="">
                </td> -->
            </tr>         
        </tbody>
        </table>
    </div>
</div>
<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script type="text/javascript">
var data_inventory = '';
//var inventory_default_row = '<tr><td><input type="checkbox" name="chk[]" id="chk"/></td><td class="hidden-xs"><input type="hidden" name="item_id[]" id="item_id" /><span></span></td><td></td><td class="hidden-xs"></td><td class="hidden-xs"></td><td class="hidden-xs"></td><td></td><td></td></tr>';
var inventory_default_row = $("#inventory_default_row");

function dialog_inventory_reset_table(table){
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(inventory_default_row);
}
function dialog_inventory_populate_data(table,data){
	data_inventory = data;
	if(data_inventory){
		//delete first row
		if($('#'+table+' > tbody > tr').length != 1){
			$('#'+table+' > tbody > tr:last').remove();
		}
		for(var p = 0; p<data_inventory.length; p++){
			var row = clone_row(table);
			row.find('td:eq(0)').find('input[name="chk[]"]').val(data_inventory[p]['item_id']);
			row.find('td:eq(1)').find('input[type=hidden]').val(data_inventory[p]['item_id']);
			row.find('td:eq(1)').find('span').text(data_inventory[p]['item_code']);
			row.find('td:eq(2)').text(data_inventory[p]['item_name']);
			row.find('td:eq(3)').text(data_inventory[p]['item_category']);
			//row.find('td:eq(4)').text(data_inventory[p]['item_style']);
			//row.find('td:eq(5)').text(data_inventory[p]['item_color']);
			row.find('td:eq(4)').text(data_inventory[p]['qty_onhand']);
		}
		var row = clone_row(table);
		//delete first and last after insert
		$('#'+table+' > tbody > tr:first').remove();
		$('#'+table+' > tbody > tr:last').remove();
	}else{
		console.log('error');	
	}
}
function dialog_inventory_push_data(){ //default function for every dialog, ex.dialog_inventory -> dialog_inventory_push_data();
	var result = [];
	$('#dialog_inventory_list > tbody').find('input[name^="chk"]:checked').each(function(){ //alert($(this).val());
		result.push($(this).val());
	});
	dialog_inventory_pull_data(result);
	$('#dialog_inventory_list > tbody').find('input[name^="chk"]:checked').each(function(){
		$(this).parent().parent().toggleClass('selected');
		$(this).removeAttr('checked');
	});
}


$(document).ready(function(){
	table_sel.init('dialog_inventory_list','multiple');
	var last_selrow = 0;
	var trs = document.getElementById('dialog_inventory_list').tBodies[0].getElementsByTagName('tr');
	$('#dialog_inventory_list').disableSelection();	

	$("#dialog_inventory_list > tbody > tr").on('click',function(){
		alert('aaaa');
	});
	$("#dialog_inventory_list > tbody > tr#inventory_default_row").on('click',function(){
		alert('bbb');
	});
	
});

$(".modal").on('shown', function() { 
	//alert('sefsefe');
    //$(this).find("#search").focus();
});

$("#search_inv").on("keyup", function() {
    var value = $(this).val();
	var cols = $("#dialog_inventory_list").find("tr:first th").length;
    $("#dialog_inventory_list tbody tr").each(function(index) { 
		var show = 0;
		$row = $(this);
		for(var x = 0;x<=cols;x++){
			if($row.hasClass('selected')){show++; break;}
			if($row.find("td:eq("+x+")").text().toLowerCase().indexOf(value.toLowerCase()) !== -1){// console.log('yes');
				show++; break;
			}
		}
		(show>0) ? $row.show(): $row.hide();		
    });
});
</script>