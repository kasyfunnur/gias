<?php $this->view('template/printheader.php');?>

<style type="text/css">
	table.printBody tr td{
		font-family:Verdana, Geneva, sans-serif;
		font-size:12px;
		border: solid 1px #CCC;
	}	
	thead{
		font-weight: bold;
		text-align: center;	
	}	
	td.numeric{
		text-align:right;
	}
	tr.aggr, tr.disc, tr.total{
		font-weight: bold;	
	}
</style>
<!--<pre>-->
<?php 
//	var_dump($form_header);
	//var_dump($form_detail);
?>
<!--</pre>-->
<table width="99%" class="info">
<tr>
	<td width="50%" style="vertical-align:top;"> <!--Business Info-->
	    <table width="100%">
        <tr style="vertical-align:top;">
            <td>From </td><td>:</td><td> <?php echo $form_header[0]['whse_from_name']?></td>        
        </tr>
        <tr style="vertical-align:top;">
            <td>To </td><td>:</td><td> <?php echo $form_header[0]['whse_to_name'];?></td>        
        </tr>        
        </table>
    </td>
	<td width="4%"></td>
    <td width="46%" style="vertical-align:top;"> <!--Document Info-->
	    <table width="100%">
        <tr style="vertical-align:top;">
            <td>Send Date</td><td>: <?php echo date("d-M-y",strtotime($form_header[0]['doc_dt']));?></td> 
        </tr>
        <tr style="vertical-align:top;">
            <td>Receipt Date </td><td>: <?php echo date("d-M-y",strtotime($form_header[0]['receipt_dt']));?></td> 
        </tr>        
        </table>
    </td>	 
</tr>	
</table>
<br>
<table width="100%" class="printBody">
<thead>
	<td>No</td>
    <td>Art.</td>
    <td>Model</td>
    <td>Qty</td>
    <td>UoM</td>
    <!--<td>Harga</td>
    <td>Disc</td>
    <td>Jumlah</td>-->
</thead>
<tbody>
<?php //AGGR INIT
	$aggr_item_qty = 0;
	$aggr_item_total = 0;
?>
<?php //var_dump($aggr_item_total);?>
<?php for($x = 0; $x<= count($form_detail)-1; $x++){ ?>	
    <tr>
        <td class="numeric"><?php echo $x+1;?>. </td>
        <td><?php echo $form_detail[$x]['item_code'];?></td>
        <td><?php echo $form_detail[$x]['item_name'];?></td>
        <td class="numeric"><?php echo number_format($form_detail[$x]['item_qty'],2);?></td>
        <td class=""><!--<?php //echo $form_detail[$x]['uom_name'];?>-->Pcs</td>
        <!--<td class="numeric"><?php echo number_format($form_detail[$x]['item_price'],2);?></td>
        <td class="numeric"><?php echo number_format($form_detail[$x]['item_disc_pct1'],2);?></td>
        <td class="numeric"><?php echo number_format($form_detail[$x]['item_total'],2);?></td>-->
    </tr>    
<?php //AGGR OPT
	$aggr_item_qty += $form_detail[$x]['item_qty'];
	//$aggr_item_total += $form_detail[$x]['item_total'];
?>
<?php } ?>
	<tr>
    	<td colspan="7">&nbsp;</td>
    </tr>
	<tr class="aggr">
        <td class="numeric"></td>
        <td></td>
        <td></td>        
        <td class="numeric"><?php echo number_format($aggr_item_qty,2);?></td>
        <!--<td></td>-->
        <td class=""></td>
        <!-- <td class="numeric">Sub Total</td>
        <td class="numeric"><?php //echo number_format($aggr_item_total,2);?></td> -->
    </tr>	     
</tbody>
</table>

<table id="footer" width="100%">
<tr><td colspan="6">&nbsp;</td></tr>
<tr>
	<td colspan="2" align="center">Tanda Terima</td>
    <td colspan="2" align="center">Cek Barang</td>
    <td colspan="2" align="center">Hormat Kami</td>
</tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td colspan="6">&nbsp;</td></tr>
<tr>
	<td align="center">(</td><td align="center">)</td>
    <td align="center">(</td><td align="center">)</td>
    <td align="center">(</td><td align="center">)</td>
</tr>
</table>
<?php $this->view('template/printfooter.php');?>