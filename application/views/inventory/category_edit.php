<section class="content-header">
    <h1>
    <a href="<?php echo site_url('inventory/category') ?>" title="">Inventory Category</a>
    &nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
    New Category
  </h1>
</section>
<section class="content">
    <div class="box box-info">
        <div class="box-header"></div>
        <div class="box-body">
            <?php $attributes=array( 'class'=> 'form-horizontal', 'role' => 'form', 'method' => 'post', 'name' => 'frm', 'id' => 'frm', 'onSubmit' => 'return FormValidation();' ); echo form_open('', $attributes); ?>
            <input type="hidden" name="category_id" value="<?php echo $category['category_id']; ?>">
            <div class="form-group">
                <label for="category_code" class="col-sm-2">Category Code</label>
                <div class="col-sm-10">
                    <input type="text" name="category_code" id="category_code" class="form-control col-sm-10" value="<?php echo $category['category_code'] ?>" required="required" placeholder="Category Code">
                </div>
            </div>
            <div class="form-group">
                <label for="category_name" class="col-sm-2">Category Name</label>
                <div class="col-sm-10">
                    <input type="text" name="category_name" id="category_name" class="form-control col-sm-10" value="<?php echo $category['category_name'] ?>" required="required" placeholder="Category Name">
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#accounting" aria-controls="accounting" role="tab" data-toggle="tab">Accounting</a>
                            </li>
                            <!-- <li role="presentation">
                                <a href="#misc" aria-controls="misc" role="tab" data-toggle="tab">Misc</a>
                            </li> -->
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="accounting">
                                <!-- Link Account Settings -->

                                <!-- 1. inventory account -->
                                <div class="form-group">
                                    <label for="category_name" class="col-sm-2">Inventory Account</label>
                                    <div class="col-sm-10">
                                        <?php 
                                        echo sw_CreateDropdown('acc_id_inventory',$coa, 'account_id', array('account_number','account_name'), $category['acc_id_inventory'], array('initialvalue'=>0, 'initialdisplay'=>'-- Please Select --'));
                                         ?>
                                    </div>
                                </div>

                            <!-- 2. expense account -->
                                <div class="form-group">
                                    <label for="category_name" class="col-sm-2">Expense Account</label>
                                    <div class="col-sm-10">
                                        <?php 
                                        echo sw_CreateDropdown('acc_id_expense',$coa, 'account_id', array('account_number','account_name'), $category['acc_id_expense'], array('initialvalue'=>0, 'initialdisplay'=>'-- Please Select --'));
                                         ?>
                                    </div>
                                </div>

                            <!-- 3.  -->
                                <div class="form-group">
                                    <label for="category_name" class="col-sm-2">Sales Account</label>
                                    <div class="col-sm-10">
                                        <?php 
                                        echo sw_CreateDropdown('acc_id_sales',$coa, 'account_id', array('account_number','account_name'), $category['acc_id_sales'], array('initialvalue'=>0, 'initialdisplay'=>'-- Please Select --'));
                                         ?>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- <div role="tabpanel" class="tab-pane" id="misc">
                                <h4>Misc Settings for this category</h4>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>
