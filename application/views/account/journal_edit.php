<!-- Content Header (Page header) -->
<?php $list_action = array(array('link'=>'#','icon'=>'print','text'=>'Print'));?>
<?php
    echo form_open('', config_form());
    echo form_text('','hidden','journal_id',$form_header[0]['journal_id']);
?>
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('account/journal');?>">Journal Transaction</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Edit 
        &nbsp;&nbsp;&nbsp;
        <div class="pull-right button_action" id="button_action">
            <?php echo form_button_action('Action','success',$list_action);?>
            <button type="submit" class="btn btn-info">
                <i class="fa fa-check"></i>&nbsp;&nbsp; Update
            </button>
        </div>
    </h1>
</section>

<?php //var_dump($form_detail);?>
<!-- Main content -->
<section class="content">
    <div class="row">
    	<div class="col-sm-4">
            <?php echo form_text_ro('Trans No.','text','jrnl_code',$form_header[0]['journal_code']);?>
            <?php echo form_text_ro('Posting Date','date','post_date',substr($form_header[0]['posting_date'],0,10));?>
            <?php echo form_text('Document Date','date','doc_date',substr($form_header[0]['document_date'],0,10));?>
        </div>
    	<div class="col-sm-4">
            <div class="col-xs-5">Trans Code.</div>
            <div class="col-xs-7">
                <?php echo sw_createSelect("trans_code",$ddl_trans,'trans_code',array('trans_code','trans_name'));?>
            </div>
            <?php echo form_text('Remark','text','doc_remark',$form_header[0]['journal_memo']);?>
        </div>
        <div class="col-sm-4">
            <?php echo form_text('Ref 1','text','ref1',$form_header[0]['journal_ref1']);?>
            <?php echo form_text('Ref 2','text','ref2',$form_header[0]['journal_ref2']);?>
            <?php echo form_text('Ref 3','text','ref3',$form_header[0]['journal_ref3']);?>
        </div>
    </div>
    <br>
    <div class="row">
    	<div class="col-sm-12">
            <div class="table-responsive">
        	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
            <thead>
            <tr>
            	<th style="width:5%">#</th>
                <th style="width:15%">
                    <?php if(isset($limit) && $limit){ ?>
                    Account Code
                    <?php }else{ ?>
                    <button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_account" style="width:100%;">
                        <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Account Code</span>
                        <span class="visible-xs"><i class="fa fa-search"></i></span>
                        <?php //echo sw_altButton('Item_code','fa-search');?>
                    </button>
                    <?php }?>
                </th>
                <th style="width:15%">Account Name</th>
                <th style="width:5%">Curr</th>
                <th style="width:15%">Dr</th>
                <th style="width:15%">Cr</th>
                <th style="width:15%">Cost Center</th>
                <!-- <th style="width:5%">Action</th> -->
            </tr>
            </thead>
            <tbody>
            <tr id="default">
            	<td style="text-align:center"><input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/></td>
                <td>
                	<input type="hidden" name="account_id[]" id="account_id1" class="form-control">
                	<input type="text" name="account_number[]" id="account_number1" class="form-control" readonly="readonly">
                </td>
                <td><input type="text" name="account_name[]" id="account_name1" class="form-control"readonly></td>
                <td><input type="text" name="curr[]"  id="curr1"  class="form-control" style="text-align:right"readonly></td>
                <td><input type="text" name="dr[]"  id="dr1"  class="form-control" style="text-align:right"readonly></td>
                <td><input type="text" name="cr[]"  id="cr1"  class="form-control"  style="text-align:right"readonly></td>
                <td><?php echo sw_CreateDropdown(array('journal_cc[]','journal_cc1'),$ddl_cc,'id',array('cc_code','cc_name'));?></td>
                <!--<td>                	
                    &nbsp;
                    <a href="javascript:table_this_row_delete();">[x]</a>
                </td>-->
            </tr>            
            </tbody>
            <tfoot>
            	<tr>
                	<td colspan="3" style="text-align:right;font-weight:bold;"><span id="journal_balance"></span>&nbsp;&nbsp;</td>
                    <td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                    <td><div>
                    	<input type="text" class="form-control" id="total_dr" name="total_dr" value="0" style="text-align:right;" readonly>
                        </div></td>
                    <td><div>
                    	<input type="text" class="form-control" id="total_cr" name="total_cr" value="0" style="text-align:right;" readonly>
                        </div></td>
                </tr>
            </tfoot>
            </table>
            </div>
    	</div>
    </div>
    <br />
<!-- </form>	 -->
</section>

<section class="content-header">
    <h1>
    <div class="pull-right" id="button_action">
        <?php echo form_button_action('Action','success',$list_action);?>
        <button type="submit" class="btn btn-info">
            <i class="fa fa-check"></i>&nbsp;&nbsp; Update
        </button>
    </div>
    </h1>
</section>
</form>

<!--MODAL-->
<?php echo sw_createModal('modal_account','Select Account','dialog_account_push_data();','account/dialog_account');?>

<?php 
	//echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script src="<?php //echo site_url('assets/js/bloodhound.js');?>"></script>
<script src="<?php //echo site_url('assets/js/typeahead.jquery.js');?>"></script>
<script src="<?php //echo site_url('assets/js/typeahead.bundle.js');?>"></script>
<script>
var form_header = <?php echo json_encode($form_header);?>;
var form_detail = <?php echo json_encode($form_detail);?>;

var source_account = <?php echo json_encode($dialog_account);?>;

var return_url = "<?php echo site_url('account/journal');?>";
var submit_url = "<?php echo site_url('account/journal_edit_query');?>/"+form_header['doc_num'];
var ajax_url_1 = "<?php echo base_url('account/ajax_account_info');?>";
var ajax_url_2 = "<?php echo base_url('account/ajax_trans_info');?>";

</script>
