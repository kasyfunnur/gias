<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
<?php echo $name;?> filter - <?php echo $type;?>
<br>
As of : <input type="date" name="filter_date_asof" id="filter_date_asof">

<br><br>
<button type="submit">Submit</button>

<script>
var submit_url = "<?php echo site_url('account/report_query/balance');?>";
var return_url = "<?php echo site_url('report/finance/balance');?>";
function validateForm(){
	
	//location.href = submit_url;
	//window.location = submit_url;
	$.post(
		submit_url,
		$('#frm').serialize(),
		function(response){					
			if(response==1){
				alert("Data updated successfully!");
				//window.location.href=return_url;
				//window.location.href=submit_url;
			}else{
				console.log("ERROR: "+response);
			}
		}
	);
	return false;
}

</script>