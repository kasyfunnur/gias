<!-- Content Header (Page header) -->
<section class="content-header">
    <h1> Journal Template <a href="<?php echo site_url('account/journal_template_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<span class="hidden-xs">
                <i class="fa fa-plus"></i>&nbsp;&nbsp;New Template
            </span>
            <span class="visible-xs">
                <i class="fa fa-plus"></i>
            </span>
		</button>
    </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<?php $attributes = array(
    'class'     => 'form-horizontal',
    'role'      => 'form',
    'method'    => 'post', 
    'name'      => 'frm', 
    'id'        => 'frm',
    'onSubmit'  => 'return validateForm();'
    );
    echo form_open('', $attributes);?>
<section class="content">
<div class="box box-info">
    <div class="box-body table-responsive">
  	 <table class="table table-hover table-condensed" id="journal_index">
    	<thead>
        	<tr class="filter">
                <th style="width:25%;text-align:left">
                    Total = <?php echo $rows;?> document(s).
                </th>
                <th style="width:35%;text-align:left">                  
                    <!--Filter : <select name="sel_year"><option value="2015">2015</option><option value="2014">2014</option></select>  -->
                </th>
                <th style="width:40%;text-align:left">
                    
                </th>
            </tr>
        	<tr>                
                <th style="width:25%;text-align:left">
                    <div class="input-group input-group-sm"> 
                    </div>
                </th>
                <th style="width:35%;text-align:left">
                    <div class="input-group input-group-sm">
                        <?php echo sw_textButton('search_field','Search Here..','search_button','Search','search');?>
                    </div>
                </th>
                <th style="width:40%;text-align:left">                    
                    <?php 
                    $page_config['base_url'] = $this->uri->config->config['base_url']."account/journal";
                    $page_config['total_rows'] = $rows;
                    $page_config = array_merge($page_config,$this->config->item('pagination','pagination'));
                    $this->pagination->initialize($page_config); 
                    ?>
                    <div class="input-group input-group-sm">                        
                        <ul class="pagination">
                            <?php echo $this->pagination->create_links();?>                            
                        </ul>
                    </div>                    
                </th>
            </tr>
        </thead>
        <tbody>
            <?php //var_dump($datatable_detail);?>
        	<?php //for ($x=0;$x<count($datatable);$x++){?>		 
            <?php foreach($datatable as $x=>$data){?> 
            <?php 
                //$status = doc_status($datatable[$x]['doc_status']);
            ?>           
            <tr>
                <td>
                    <span class="searchable" style="font-size:16px;font-weight:lighter !important;color:#888">
                        <a href="<?php echo site_url('account/journal_template_edit');?>/<?php echo $data['code'];?>">
                            <?php echo $data['code'];?>
                        </a> 
                    </span>                    
                    
                    <span class="pull-right">                        
                    </span>
                </td>            	
                <td>
                    <?php echo $data['name'];?>
                </td>                
                <td>
                    <?php
                        foreach($datatable_detail as $z=>$data_detail){
                            if($data_detail['id'] == $data['id']){
                                if($data_detail['journal_cr'] == 0){
                                    echo $data_detail['account_name']."   :   ".$data_detail['journal_dr']."<br>";
                                }elseif($data_detail['journal_dr']==0){
                                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data_detail['account_name']."   :   ".$data_detail['journal_cr']."<br>";
                                }                        
                            }
                        }
                    ?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table></div>
    </div>
</section>
<!-- /.content -->
