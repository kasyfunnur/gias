<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('account/journal');?>">Journal Template</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Add 
        &nbsp;&nbsp;&nbsp;
    </h1>   
</section>

<?php //var_dump($dialog_account);?>
<!--Main Content-->
<section class="content">	
    <?php echo form_open('', config_form());?>
    <div class="container-fluid">
        <div class="row info">
            <div class="col-sm-4">
                <!-- <table width="100%"> -->
                    <?php echo form_text_ro('Trans No.','text','jrnl_code',$doc_num);?>                    
                    <?php //echo form_text('Posting Date','date','post_date',date('Y-m-d',time()));?>
                    <?php //echo form_text('Document Date','date','doc_date',date('Y-m-d',time()));?>
    			<!-- </table> -->
    		</div>
            <div class="col-sm-4">
                <!-- <table width="100%"> -->
                    <!-- <tr class="info">
                        <td>Trans Code.</td>
                        <td class="value">
                        	<?php //if(isset($limit) && $limit){ ?>
                        	<input class="form-control input-sm" type="text" 
                            	name="trans_code" id="trans_code" value="<?php if(isset($trans_type))echo $trans_type;?>" readonly="readonly"/>
                            <?php //}else{ ?>
    	                    <?php //echo sw_createSelect("trans_code",$ddl_trans,'trans_code','trans_name');?>
                            <?php //} ?>
                        </td>
                    </tr> -->
                    <!-- <div class="col-xs-5 text-nowrap">Trans Code.</div>
                    <div class="col-xs-7 text-nowrap">
                        <?php //echo sw_createSelect("trans_code",$ddl_trans,'trans_code',array('trans_code','trans_name'));?>
                    </div> -->
                    <?php echo form_text('Trans Name','text','jrnl_name');?>
                    <?php //echo form_text('Remark','text','doc_remark');?>
    			<!-- </table> -->
    		</div>
            <div class="col-sm-4">
                <!-- <table width="100%"> -->
                    <?php //echo form_text('Ref 1','text','ref1');?>
                    <?php //echo form_text('Ref 2','text','ref2');?>
                    <?php //echo form_text('Ref 3','text','ref3');?>
    			<!-- </table> -->
    		</div>
    	</div>
        <br>
        <div class="row">
        	<div class="col-sm-12">
                <div class="table-responsive">
            	<table class="table-bordered" id="table_detail" style="table-layout:fixed;white-space:nowrap;width:960px">
                <thead>
                <tr>
                	<th width="5%">#</th>
                    <th width="15%">
                    	<?php if(isset($limit) && $limit){ ?>
    	                Account Code
                        <?php }else{ ?>
                        <button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_account" style="width:100%;">
                            <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Account Code</span>
    						<span class="visible-xs"><i class="fa fa-search"></i></span>
                            <?php //echo sw_altButton('Item_code','fa-search');?>
                        </button>
                        <?php }?>
                    </th>
                    <th width="15%">Account Name</th>
                    <th width="5%">Curr</th>
                    <th width="15%">Dr</th>
                    <th width="15%">Cr</th>
                    <th width="5%">Action</th>
                </tr>
                </thead>
                <tbody>
                <tr id="default">
                	<td style="text-align:center">
                        <input type="text" name="row[]" id="row1" class="form-control" value="1" readonly tabindex="-1"/>
                    </td>
                    <td>
                    	<input type="hidden" name="account_id[]" id="account_id1" class="form-control">
                    	<input type="text" name="account_number[]" id="account_number1" class="form-control" tabindex="-1">
                    </td>
                    <td><input type="text" name="account_name[]" id="account_name1" class="form-control" tabindex="-1"></td>
                    <!-- <td><input type="text" name="curr[]"  id="curr1"  class="form-control" style="text-align:right"readonly tabindex="-1"></td> -->
                    <td><input type="text" name="dr[]"  id="dr1"  class="form-control" style="text-align:right"></td>
                    <td><input type="text" name="cr[]"  id="cr1"  class="form-control"  style="text-align:right"></td>
                    <td>
                    	<!--<div style='text-align:center'>
    						<div class='btn-group'>
                                <button type="button" class="btn btn-flat btn-xs btn-danger" id="act_del1" name="act_del">DELETE</button>
    						</div>
    					</div>-->
                        &nbsp;
                        <a href="javascript:table_this_row_delete();">[x]</a>
    				</td>
                </tr>            
                </tbody>
                <tfoot>
                	<tr>
                    	<td colspan="3" style="text-align:right;font-weight:bold;"><span id="journal_balance"></span>&nbsp;&nbsp;</td>
                    	<td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                        <td><div>
                        	<input type="text" class="form-control" id="total_dr" name="total_dr" value="0" style="text-align:right;" readonly>
                            </div></td>
                        <td><div>
                        	<input type="text" class="form-control" id="total_cr" name="total_cr" value="0" style="text-align:right;" readonly>
                            </div></td>
                    </tr>
                </tfoot>
                </table>
                </div>
        	</div>
        </div>
    </div>
    <br />
    <button type="submit" class="btn btn-info pull-right">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
	</form>	
</section>

<!--MODAL-->
<?php echo sw_createModal('modal_account','Select Account','dialog_account_push_data();','account/dialog_account');?>

<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script src="<?php echo site_url('assets/js/bloodhound.js');?>"></script>
<script src="<?php echo site_url('assets/js/typeahead.jquery.js');?>"></script>
<script src="<?php echo site_url('assets/js/typeahead.bundle.js');?>"></script>
<script>
//var form_header = <?php //echo json_encode($form_header);?>;
var form_detail = "";
<?php if(isset($form_detail)) { ?>
var form_detail = <?php echo json_encode($form_detail);?>;
<?php } ?>

var source_account = <?php echo json_encode($dialog_account);?>;

var return_url = "<?php echo site_url('account/journal_template');?>";
var submit_url = "<?php echo site_url('account/journal_template_add_query');?>";
var ajax_url_1 = "<?php echo base_url('account/ajax_account_info');?>";
var ajax_url_2 = "<?php echo base_url('account/ajax_trans_info');?>";

</script>
<script>
var default_row = $("#default");

var accounts = new Bloodhound({
    datumTokenizer: function(d){
        for(var prop in d) { return Bloodhound.tokenizers.whitespace(d.account_name);}
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    limit: 5,
    local: source_account
});
accounts.initialize();

function apply_typeahead(elm){
    var dtm = $(elm).typeahead(null,{
        displayKey: function(accounts){
            for(var prop in accounts){
                return accounts.account_name;
            }
        },
        source:accounts.ttAdapter(),
        updater: function(accounts){
            return accounts;
        }
    }).on('typeahead:selected',function(obj,datum){
        var var_datum = [];
        var_datum['account_id'] = datum.account_id;
        var_datum['account_number'] = datum.account_number;
        var_datum['account_name'] = datum.account_name;
        var_datum['account_currency'] = datum.account_currency;
        dialog_account_pull_data(datum.account_id);
    });    
}
apply_typeahead('input[name^=account_name]');


function validateForm(){
    if(confirm("Are you sure?")==true){
        var tbl_row = $("#table_detail > tbody > tr").length;
        $('<input>').attr({
            type: 'hidden', id: 'table_row', name: 'table_row', value: tbl_row
        }).appendTo('form');
        $.post(
            submit_url,
            $('#frm').serialize(),
            function(response){                 
                if(response==1){
                    alert("Data updated successfully!");
                    window.location.href=return_url;
                }else{
                    console.log("ERROR: "+response);
                    $("input").remove(".hidden_post");
                }
            }
        );
    }
    return false;
}

$(document).ready(function(){
    pop_account();
    table_doc.init('table_detail',default_row);    
    table_doc.init_col('total_dr','dr[]');
    table_doc.init_col('total_cr','cr[]');
    table_doc.row_index();  
});
function table_detail_insert(data){ 
    if(data){
        table_doc.row_del_last();
        for(var p = 0;p<data.length;p++){
            var row = table_doc.clone();
            row.find('input[name^=account_id]').val(data[p]['account_id']);
            row.find('input[name^=account_number]').val(data[p]['account_number']);
            row.find('input[name^=account_name]').val(data[p]['account_name']);
            row.find('input[name^=curr]').val(data[p]['account_currency']);            
        }
        table_doc.row();
        table_doc.clone();
        table_doc.row_del_first('account_id');
        table_doc.row_index();
        table_doc.col();
        table_doc.extra();
        table_doc.sum();
    }else{
        console.log('error');   
    }   
}
function table_this_row_delete(){
    var xx = $(this).parent().parent().parent().parent(); xx.remove();
}
function pop_account(){
    dialog_account_reset_table('dialog_account_list');
    dialog_account_populate_data('dialog_account_list',source_account);
}
function dialog_account_pull_data(data_account){
    var data_account_param = $.map(data_account, function(value, index) {
        return [value];
    });
    var data_ajax_raw = $.ajax({
        url:ajax_url_1,
        data: {account_number:data_account_param},
        dataType: "json",
        success: function(result){
            table_detail_insert(result);            
        }
    }); 
}
</script>