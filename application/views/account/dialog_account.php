<?php $column = array('','Asset Code','Asset Name');?>
<div class="row">
	<div class="col-sm-12">
    	<table class="table table-condensed" id="dialog_account_list">
        <thead>
            <tr>
                <th width="5%"><input type="checkbox" name="account_chk_all" id="account_chk_all" />&nbsp;</td>
                <th width="15%">Code</td>
                <th width="35%">Name</td>
                <th width="30%">Balance</td>
            </tr>
        </thead>
        <tbody>
        	<tr id="account_default_row" style="height:20px;">
            	<td><input type="checkbox" name="chk[]" id="chk"></td>
                <td><input type="hidden" name="account_id[]" id="account_id"><span></span></td>
                <td></td>
                <td style="text-align:right;"></td>
            </tr>                    
        </tbody>
        </table>
    </div>
</div>

<?php 
echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
/*echo '<script src="'.base_url('assets/js/bootstrap.min.js').'"></script>';
echo '<script src="'.base_url('assets/js/admin_app.js').'"></script>';
echo '<script src="'.base_url('assets/js/comp/status.js').'"></script>';*/
?>

<script type="text/javascript">
//var rowCount = 1;
var data_account = '';
var account_default_row = $("#account_default_row");
function dialog_account_reset_table(table){
	$('#'+table+' > tbody > tr').remove();
	$('#'+table+' > tbody').append(account_default_row);
}
function dialog_account_populate_data(table,data){
	data_account = data;
	if(data_account){
		if($('#'+table+' > tbody > tr').length != 1){
			$('#'+table+' > tbody > tr:last').remove();
		}
		for(var p = 0; p<data_account.length; p++){
			var row = clone_row(table);
			row.find('input[name="chk[]"]').val(data_account[p]['account_id']);
			row.find('input[name="account_id[]"]').val(data_account[p]['account_id']);
			row.find('td:eq(1)').find('span').text(data_account[p]['account_number']);
			row.find('td:eq(2)').text(data_account[p]['account_name']);
			row.find('td:eq(3)').text(data_account[p]['account_balance']);
		}
		var row = clone_row(table);
		//delete first and last after insert
		$('#'+table+' > tbody > tr:first').remove();
		$('#'+table+' > tbody > tr:last').remove();
	}else{
		console.log('error');
	}
}
function dialog_account_push_data(){
	var result = [];
	//MULTIPLE SELECTION
	$('#dialog_account_list > tbody').find('input[name^="chk"]:checked').each(function(){
		result.push($(this).val());
	});
	//console.log(result);
	dialog_account_pull_data(result);	
	$('#dialog_account_list > tbody').find('input[name^="chk"]:checked').each(function(){
		$(this).parent().parent().toggleClass('selected');
		$(this).removeAttr('checked');
	});
}
$(document).ready(function(){
	table_sel.init('dialog_account_list','multiple');
	$('#dialog_account_list').disableSelection();	
});
</script>