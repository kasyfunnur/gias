<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><a class="" href="<?php echo site_url('account/journal');?>">Journal Transaction</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        Add Payable
        &nbsp;&nbsp;&nbsp;
    </h1>   
</section>

<?php //var_dump($form_header);?>
<!-- Main content -->
<section class="content">	
    <?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm', 
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
    <div class="row info">
        <div class="col-sm-4">
            <table width="100%">
                <tr class="info">
                    <td>Trans No.</td>
                    <td class="value"><input class="form-control input-sm" type="text" 
                    	name="jrnl_code" id="jrnl_code" value="<?php echo $doc_num;?>" readonly/></td>
                </tr>
                <tr class="info">
                    <td>Posting Date</td>
                    <td class="value">
                    	<input class="form-control input-sm" type="date" 
                        	name="post_date" id="post_date" value="<?php echo date('Y-m-d',time());?>"/></td>
                </tr>
                <tr class="info">
                    <td>Document Date</td>
                    <td class="value">
                    	<input class="form-control input-sm" type="date" 
                        	name="doc_date" id="doc_date" value="<?php echo date('Y-m-d',time());?>"/>                            
					</td>
                </tr>
                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
			</table>
		</div>
        <div class="col-sm-4">
            <table width="100%">
            	<tr class="info">
                    <td>Buss :</td>
                    <td class="value">
                    	<!---<input class="form-control input-sm" type="text" 
                        	name="trans_code" id="trans_code" value="<?php if(isset($trans_type))echo $trans_type;?>"/>--->
                        <div class="input-group input-group-sm">
                        	<input type="hidden" id="buss_id" name="buss_id" value="">
                            <input type="text" class="form-control input-sm" id="buss_name" name="buss_name" value="">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-warning" data-toggle="modal" 
                                	href="#modal_customer" ><!---style="width:100%;"--->
                                    <i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
                            </span>
                        </div>
                    </td>
                </tr>
                <tr class="info">
                    <td>Trans :</td>
                    <td class="value">
                    	<?php if(isset($limit) && $limit){ ?>
                    	<input class="form-control input-sm" type="text" 
                        	name="trans_code" id="trans_code" value="<?php if(isset($trans_type))echo $trans_type;?>" readonly="readonly"/>
                        <?php }else{ ?>
	                    <?php echo sw_createSelect("trans_code",$ddl_trans,'trans_code','trans_name',"",array("initialdisplay"=>"","initialvalue"=>""));?>
                        <?php } ?>
                    </td>
                </tr>
                <tr class="info">
                    <td>Remark</td>
                    <td class="value">
                    	<input class="form-control input-sm" type="text" 
                        	name="doc_remark" id="doc_remark" value=""/></td>
                </tr>                
                <!---<tr><td>&nbsp;</td><td>&nbsp;</td></tr>--->
			</table>
		</div>
        <div class="col-sm-4">
            <table width="100%">
                <tr class="info">
                    <td>Ref 1</td>
                    <td class="value"><input class="form-control input-sm" type="text" name="ref1" id="ref1" /></td>
                </tr>
                <tr class="info">
                    <td>Ref 2</td>
                    <td class="value"><input class="form-control input-sm" type="text" name="ref2" id="ref2" /></td>
                </tr>
                <tr class="info">
                    <td>Ref 3</td>
                    <td class="value"><input class="form-control input-sm" type="text" name="ref3" id="ref3" /></td>
                </tr>
                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
			</table>
		</div>
	</div>
    
    
    <div class="row">
    	<div class="col-sm-12">
        	<table class="table-bordered" id="table_detail">
            <thead>
            <tr>
            	<th width="5%">#</th>
                <th width="15%"><!---Account Code--->
	                <button type="button" class="btn btn-warning" data-toggle="modal" href="#modal_account" style="width:100%;">
                        <span class="hidden-xs"><i class="fa fa-search"></i>&nbsp;&nbsp;Account Code</span>
						<span class="visible-xs"><i class="fa fa-search"></i></span>
                        <?php //echo sw_altButton('Item_code','fa-search');?>
                    </button>
                </th>
                <th width="15%">Account Name</th>
                <th width="5%">Curr</th>
                <th width="15%">Dr</th>
                <th width="15%">Cr</th>
               <th width="10%" class="action">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr height="30px" id="default">
            	<td style="text-align:center"><input type="text" name="row[]" id="row1" class="form-control" value="1" readonly/></td>
                <td>
                	<input type="hidden" name="account_id[]" id="account_id1" class="form-control">
                	<input type="text" name="account_number[]" id="account_number1" class="form-control" readonly="readonly">
                </td>
                <td><input type="text" name="account_name[]" id="account_name1" class="form-control"readonly></td>
                <td><input type="text" name="curr[]"  id="curr1"  class="form-control" style="text-align:right"readonly></td>
                <td><input type="text" name="dr[]"  id="dr1"  class="form-control" style="text-align:right"></td>
                <td><input type="text" name="cr[]"  id="cr1"  class="form-control"  style="text-align:right"></td>
                <td>
                	<div style='text-align:center'>
						<div class='btn-group'>
                            <button type="button" class="btn btn-flat btn-xs btn-danger" id="act_del1" name="act_del">DELETE</button>
						</div>
					</div>
				</td>
            </tr>            
            </tbody>
            <tfoot>
            	<tr>
                	<td colspan="3" style="text-align:right;font-weight:bold;"><span id="journal_balance"></span>&nbsp;&nbsp;</td>
                	<td id="foot_label"><strong>Total:&nbsp;&nbsp;&nbsp;</strong></td>
                    <td><div>
                    	<input type="text" class="form-control" id="total_dr" name="total_dr" value="0" style="text-align:right;" readonly>
                        </div></td>
                    <td><div>
                    	<input type="text" class="form-control" id="total_cr" name="total_cr" value="0" style="text-align:right;" readonly>
                        </div></td>
                </tr>
            </tfoot>
            </table>
    	</div>
    </div>
    <br />
    <button type="submit" class="btn btn-info pull-right">
    	<i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
	</form>	
</section>
<!-- /.content -->

<!---<section class="modal">--->
<div class="modal fade" id="modal_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title">Select Account
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <span class="hidden-xs"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</span>
                            <span class="visible-xs"><i class="fa fa-arrow-left"></i></span>
                        </button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" 
                        	onclick="dialog_account_push_data();"><!---Save & Close--->
                            <span class="hidden-xs"><i class="fa fa-save"></i>&nbsp;&nbsp;Save & Close</span>
                            <span class="visible-xs"><i class="fa fa-save"></i></span>
                        </button>
                    </div>
                </h4>                
            </div>
            <div class="modal-body">
				<?php 					
					$this->load->view('account/dialog_account');
				?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <span class="hidden-xs"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</span>
                    <span class="visible-xs"><i class="fa fa-arrow-left"></i></span>
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" 
                    onclick="dialog_account_push_data();"><!---Save & Close--->
                    <span class="hidden-xs"><i class="fa fa-save"></i>&nbsp;&nbsp;Save & Close</span>
                    <span class="visible-xs"><i class="fa fa-save"></i></span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_customer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog" >
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title">Select Vendor
                    <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" 
                        	onclick="dialog_vendor_push_data();">Save & Close</button>  
                    </div>
                </h4>                
            </div>
            <div class="modal-body">
				<?php 					
					$this->load->view('business/dialog_vendor');
				?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" 
                	onclick="dialog_vendor_push_data();">Save & Close</button>                
            </div>
        </div>
    </div>
</div>
<!---</section>--->

<?php 
	echo '<script src="'.base_url('assets/js/jquery-2.0.3.min.js').'"></script>';
?>
<script>
//var form_header = <?php //echo json_encode($form_header);?>;
<?php if(isset($form_detail)) { ?>
var form_detail = <?php echo json_encode($form_detail);?>;
<?php } ?>

var source_account = <?php echo json_encode($dialog_account);?>;
var source_vendor = <?php echo json_encode($dialog_vendor);?>;

var return_url = "<?php echo site_url('account/journal');?>";
var submit_url = "<?php echo site_url('account/journal_add_query');?>";
var ajax_url_1 = "<?php echo base_url('account/ajax_account_info');?>";
//$(document).ready(function(){	
	//$("#table_detail").colResizable();
//});

</script>
