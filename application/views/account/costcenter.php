<section class="content-header">
    <h1> Cost Center <a href="<?php echo site_url('account/costcenter_add');?>">
        <button class="btn btn-flat btn-info pull-right">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Cost Center
        </button>
    </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg: ""?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover table-condensed table-striped table-bordered" id="costcenter_list">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($costcenter as $cc) { ?>
                        <tr>
                            <td><?php echo $cc['id']; ?></td>
                            <td><a href="<?php echo site_url("account/costcenter_edit/{$cc['id']}") ?>"><?php echo $cc['cc_code']; ?></a></td>
                            <td><?php echo $cc['cc_name']; ?></td>
                            <td><?php echo $cc['cc_description']; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<!-- /.content -->
