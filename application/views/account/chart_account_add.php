<!-- Content Header (Page header) -->
<section class="content-header">
<h1><a class="" href="<?php echo site_url('account/coa');?>">Chart of Account</a> 
    	&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
        New </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<section class="content">
	<?php
	$attributes = array(
		'class' 	=> 'form-horizontal',
		'role'		=> 'form',
		'method' 	=> 'post', 
		'name'		=> 'frm',     
		'id' 		=> 'frm',
		'onSubmit'	=> 'return validateForm();'
		);
	echo form_open('', $attributes);
	?>
    <div class="row info">
        <div class="col-sm-4">
            <table width="100%">
                <tr class="info">
                    <td>GL Account</td>
                    <td class="value" id="gl_code"><input type="text" name="gl_code" id="gl_code" /></td>
                </tr>
                <tr class="info">
                    <td>GL Name</td>
                    <td class="value" id="gl_name"><input type="text" name="gl_name" id="gl_name" /></td>
                </tr>
                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>                    
                <tr class="info">
                    <td>Account Type</td>
                    <td class="value" id="gl_post">
                        <input type="radio" name="gl_post" id="gl_post1" value="1" checked/> <label for="gl_type1">Postable</label>
                        <input type="radio" name="gl_post" id="gl_post0" value="0"/> <label for="gl_type0">Title</label>
                    </td>
                </tr>
                <tr class="info">
                    <td>Account Usage</td>
                    <td class="value" id="gl_use">
                        <input type="radio" name="gl_use" id="gl_use0" value="O" checked/> <label for="gl_use0">None</label>
                        <input type="radio" name="gl_use" id="gl_use1" value="S"/> <label for="gl_use1">Sales</label>
                        <input type="radio" name="gl_use" id="gl_use2" value="E"/> <label for="gl_use2">Expense</label>
                    </td>
                </tr>
                
                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                <tr class="info">
                    <td>Account Curr</td>
                    <td class="value" id="gl_curr">
                        <select name="gl_curr">
                        <option value="IDR">IDR</option>
                        <option value="USD">USD</option>
                        <option value="SGD">SGD</option>
                        <option value="AUD">AUD</option>
                        </select>
                    </td>
                </tr>
                <tr class="info">
                    <td>Balance</td>
                    <td class="value" id="gl_bal">0</td>
                </tr>
                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>                    
                <tr class="info">
                    <td>Cash Account</td>
                    <td class="value">                        	
                        <input type="checkbox" name="gl_cash" id="gl_cash" value="1"/> 
                        <label for="gl_cash">Yes</label>
                    </td>
                </tr>
                <tr class="info">
                    <td>Bank Account</td>
                    <td class="value">
                        <input type="checkbox" name="gl_bank" id="gl_bank" value="1"/> 
                        <label for="gl_bank">Yes</label>
                    </td>
                </tr>
                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                <tr class="info">
                    <td>Parent Account</td>
                    <td class="value">                        	
                        <select name="gl_parent" id="gl_parent"/> 
                            <?php foreach($gl_parent as $gl){?>
                                <option value="<?php echo $gl['account_number'];?>">
                                    <?php echo $gl['account_number']." - ".$gl['account_name'];?>
                                </option>
                            <?php }?>								
                        </select>
                    </td>
                </tr>
                    <td>Allowed Cost Center</td>
                    <td class="value">                          
                        <select name="gl_cc" id="gl_cc" multiple/> 
                            <?php foreach($costcenter as $cc){?>
                                <option value="<?php echo $cc['cc_code'];?>">
                                    <?php echo $cc['cc_code']." - ".$cc['cc_name'];?>
                                </option>
                            <?php }?>                               
                        </select>
                    </td>
                <tr>

                </tr>
            </table>
            
        </div>
        
        <?php //var_dump($datatable);?>
        <div class="col-sm-8">
            <!--<input type="text" id="search_gl" />-->
            <!--<div class="input-group input-group-sm">
                <input type="text" class="form-control input-sm" id="search_gl" name="search_gl" value="">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-warning" data-toggle="modal" style="width:100%;">
                        <i class="fa fa-search"></i>&nbsp;&nbsp; Search</button>                                
                </span>
            </div>
            <br /><br />-->
            <div class="input-group">
                <select size="18" style="width:100%">
                    <?php foreach($datatable as $data){?>
                    <option value='<?php echo json_encode($data);?>'>
                        <?php echo str_repeat("&nbsp;",$data['account_level']*5).$data['account_number']." - ".$data['account_name'];?>
                    </option>
                    <?php }?>
                </select>
            </div>
        </div>
	</div>
    <br />
    <button type="submit" class="btn btn-info pull-right">
        <i class="fa fa-check"></i>&nbsp;&nbsp; Submit
    </button>
    <br />
</section>
<!-- /.content -->
<script>
var return_url = "<?php echo site_url('account/coa');?>";
var submit_url = "<?php echo site_url('account/coa_add_query');?>";
</script>
