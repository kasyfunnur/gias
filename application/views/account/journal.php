<!-- Content Header (Page header) -->
<section class="content-header">
    <h1> Journal Transaction <a href="<?php echo site_url('account/journal_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<span class="hidden-xs">
                <i class="fa fa-plus"></i>&nbsp;&nbsp;New Journal
            </span>
            <span class="visible-xs">
                <i class="fa fa-plus"></i>
            </span>
		</button>
    </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<?php $attributes = array(
    'class'     => 'form-horizontal',
    'role'      => 'form',
    'method'    => 'post', 
    'name'      => 'frm', 
    'id'        => 'frm',
    'onSubmit'  => 'return validateForm();'
    );
    echo form_open('', $attributes);?>
<section class="content">
<div class="box box-info">
    <div class="box-body table-responsive">
  	 <table class="table table-hover table-condensed" id="journal_index">
    	<thead>
        	<tr class="filter">
                <th style="width:25%;text-align:left">
                    Total = <?php echo $rows;?> document(s).
                </th>
                <th style="width:35%;text-align:left">                  
                    <!--Filter : <select name="sel_year"><option value="2015">2015</option><option value="2014">2014</option></select>  -->
                </th>
                <th style="width:40%;text-align:left">
                    
                </th>

            	<!-- <th>#</th>
                <th>Trx #</th>
                <th>Date</th>
                <th>Memo</th>
                <th>Ref 1</th>
                <th>Ref 2</th>
                <th>Ref 3</th> -->
                <!--<th colspan="2">Balance</th>-->
            </tr>
        	<tr>                
                <th style="width:25%;text-align:left">
                    <div class="input-group input-group-sm">
                        <!-- <span class="input-group-btn">                            
                            <button type="submit" class="btn btn-info" name="btn_filter" value="" />ALL</button>
                            <button type="submit" class="btn btn-default" name="btn_filter" value="DRAFT" />DRAFT</button>
                            <button type="submit" class="btn btn-warning" name="btn_filter" value="APPROVED"/>APPROVED</button>
                            <button type="submit" class="btn btn-success" name="btn_filter" value="CLOSED"/>CLOSED</button>
                            <button type="submit" class="btn btn-danger" name="btn_filter" value="CANCELLED"/>CANCELLED</button>
                        </span> -->
                    </div>
                </th>
                <th style="width:35%;text-align:left">
                    <div class="input-group input-group-sm">
                        <?php echo sw_textButton('search_field','Search Here..','search_button','Search','search');?>
                    </div>
                </th>
                <th style="width:40%;text-align:left">                    
                    <?php 
                    $page_config['base_url'] = $this->uri->config->config['base_url']."account/journal";
                    $page_config['total_rows'] = $rows;
                    $page_config = array_merge($page_config,$this->config->item('pagination','pagination'));
                    $this->pagination->initialize($page_config); 
                    ?>
                    <div class="input-group input-group-sm">                        
                        <ul class="pagination">
                            <?php echo $this->pagination->create_links();?>                            
                        </ul>
                    </div>                    
                </th>
            </tr>
        </thead>
        <tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>		 
            <?php 
                //$status = doc_status($datatable[$x]['doc_status']);
            ?>           
            <tr>
                <td>
                    <span class="searchable" style="font-size:16px;font-weight:lighter !important;color:#888">
                        <a href="<?php echo site_url('account/journal_edit');?>/<?php echo $datatable[$x]['journal_code'];?>">
                            <?php echo $datatable[$x]['journal_code'];?>
                        </a> 
                    </span>                    
                    <br />
                    <span style="font-size:12px;font-weight:lighter !important;color:#888">
                        By <?php echo $datatable[$x]['create_by'];?>
                        - <?php echo date("d M, Y", strtotime($datatable[$x]['posting_date']));?>
                    </span>
                    <span class="pull-right">                        
                        <strong>
                            <?php if($datatable[$x]['journal_ref1']){ ?>
                            <span class="label label-xs label-info" style="margin:2px">
                                <i class="fa"></i> <?php echo $datatable[$x]['journal_ref1'];?>
                            </span>
                            <?php } ?>
                            <?php if($datatable[$x]['journal_ref2']){ ?>
                            <span class="label label-xs label-info" style="margin:2px">
                                <i class="fa"></i> <?php echo $datatable[$x]['journal_ref2'];?>
                            </span>
                            <?php } ?>
                            <?php if($datatable[$x]['journal_ref3']){ ?>
                            <span class="label label-xs label-info" style="margin:2px">
                                <i class="fa"></i> <?php echo $datatable[$x]['journal_ref3'];?>
                            </span>
                            <?php } ?>
                        </strong>
                    </span>
                </td>            	
                <td>
                    <?php echo $datatable[$x]['journal_memo'];?>
                </td>                
                <td>
                    <!--<a href="#">
                    	<span class="btn btn-xs btn-default" >
                        <i class="fa fa-plusrint"></i> History</span>
                    </a>&nbsp;&nbsp;
                    -->
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table></div>
    </div>
</section>
<!-- /.content -->
