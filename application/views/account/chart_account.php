<!-- Content Header (Page header) -->
<style>
	/*thead>tr>th{text-align:center;font-weight:bold}*/
</style>
<section class="content-header">
  <h1> Chart of Account <a href="<?php echo site_url('account/chart_account_add');?>">
        <button class="btn btn-flat btn-info pull-right">
        	<i class="fa fa-plus"></i>&nbsp;&nbsp;Add Account
		</button>
    </a>
    </h1>
</section>
<?php echo (isset($msg))?$msg:""?>
<!-- Main content -->
<section class="content">
<div class="box box-info">
    <div class="box-header">
    </div>
    <div class="box-body table-responsive">
  	<table class="table table-hover table-condensed">
    	<thead>
        	<tr>
            	<!---<th>#</th>--->
                <th>Acc#</th>
                <th>Name</th>
                <th>Curr</th>
                <th>Dr</th>
                <th>Cr</th>
                <th>Action</th>
                <!---<th colspan="2">Balance</th>--->
            </tr>
        	<!---<tr>
                <th>Dr</th>
                <th>Cr</th>
            </tr>--->
        </thead>
        <tbody>
        	<?php for ($x=0;$x<count($datatable);$x++){?>		
            <?php if($datatable[$x]['is_postable'] == 1){$cls = 'post';}else{$cls = '';}?>	
            <tr class="<?php echo $datatable[$x]['account_parent_number'];?>" id="<?php echo $datatable[$x]['account_number'];?>" status="e">
            	<!---<td><?php //echo $x+1;?>.</td>--->
                <td>
                    <!---<a href="<?php //echo site_url('account/chart_account_edit');?>/<?php //echo $datatable[$x]['account_number'];?>">
						<?php //echo str_repeat("&nbsp;",9*$datatable[$x]['account_level']). $datatable[$x]['account_number'];?>
                    </a>--->
                    <?php echo str_repeat("&nbsp;",9*$datatable[$x]['account_level']). $datatable[$x]['account_number'];?>
                </td>
                <td><?php echo str_repeat("&nbsp;",9*$datatable[$x]['account_level']).$datatable[$x]['account_name'];?></td>
                <td><?php echo $datatable[$x]['account_currency'];?></td>
                
                <?php 
					if($datatable[$x]['default'] == "D") {
						$d = $datatable[$x]['account_balance'];
						$c = number_format(0,2);
					}else{
						$d = number_format(0,2);
						$c = $datatable[$x]['account_balance'];
					}
				?>
                <td><?php echo $d;?></td>
                <td><?php echo $c;?></td>
                <td>
                    <!---<a href="#">
                    	<span class="btn btn-xs btn-default" >
                        <i class="fa fa-print"></i> History</span></a>&nbsp;&nbsp;--->
                    <a href="<?php echo site_url('account/chart_account_edit')."/".$datatable[$x]['account_number'];?>">
                    	Edit</a>
				</td>                
            </tr>
            <?php } ?>
        </tbody>
    </table></div>
    </div>
</section>
<!-- /.content -->
