<section class="content-header">
    <h1><a href="<?php echo site_url('account/costcenter');?>">Cost Center</a>
        &nbsp;<i class="fa fa-angle-right"></i>&nbsp;
        Add New
    </h1>
</section>
<?php echo (isset($msg))?$msg: ""?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
        </div>
        <div class="box-body">
            <form method="post" accept-charset="utf-8" class="form-horizontal">
                <div class="form-group">
                    <label for="cc_code" class="col-sm-2 control-label">Cost Center code</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="cc_code" id="cc_code" placeholder="Code" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cc_name" class="col-sm-2 control-label">Cost Center Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="cc_name" id="cc_name" placeholder="Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cc_description" class="col-sm-2 control-label">Cost Center Description</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="cc_description" id="cc_description" placeholder="Description">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- /.content -->
