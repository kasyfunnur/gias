<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Msg Class - Indonesia
 */
class Email_lib{
	protected $CI;
	public $from_email 	= 'admin@erp.digitalbuana.com';	
	public $from_name 	= 'ERP System';		
	public $to_email	= '';			
	public $reply_to	= ''; 			
	public $subject		= '';			
	public $message		= '';
	
	public function __construct(){
		log_message('debug', "Msg class initiated");
		$this -> CI = & get_instance();
		}

	public function send_email($queue = FALSE){
		if($queue){
			$data = array(
			'from_email' 		=> $this -> from_email ,
			'from_name' 		=> $this -> from_name ,
			'to_email' 			=> $this -> to_email ,
			'reply_to' 			=> $this -> reply_to ,
			'subject' 			=> $this -> subject ,
			'message' 			=> $this -> message 
			);
			return $this -> CI -> db -> insert('t_email_queue', $data) ? TRUE : FALSE;
		}
		else{
		 	$headers 	= "From: $this->from_name <$this->from_email>". "\r\n";
			$headers 	.='MIME-Version: 1.0'. "\r\n";
			$headers	.="Content-type: text/html; charset=iso-8859-1". "\r\n";
			$headers	.="Reply-To: $this->reply_to". "\r\n";
			$headers	.="X-Mailer: PHP/" . phpversion(). "\r\n";
		 	return @mail($this->to_email, $this->subject, $this->message, $headers) ? TRUE : FALSE;
		}
	}
	
	function template($type = NULL, $appr_code, $data = NULL){
	
		$view_temp  = $this -> CI -> load -> view("email/$type/header", $data, TRUE);
		$view_temp .= $this -> CI -> load -> view("email/$type/$appr_code", $data, TRUE);
		$view_temp .= $this -> CI -> load -> view("email/$type/footer", $data, TRUE);
		// $this -> message = $this -> CI -> load -> view("email/$type/$appr_code", $data, TRUE);
		$this -> message = $view_temp;
		$this -> subject = $data['subject'];
		
		}
}