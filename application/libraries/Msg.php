<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Msg Class - Indonesia
 */
class Msg{

	public function __construct(){
		log_message('debug', "Msg class initiated");
		}

	public function getMessage($msg){
		switch ($msg) {
		//controller -> asset
		case "test":
		return $this->template('test');
		
		case "asset_added":
		return $this->template('New asset has been added, this new addition has been logged into system logs');
		
		case "asset_add_failed":
		return $this->template('Sorry, adding new asset has failed, this could be because of mismatch of field types, please revise your input and try again or contact your administrator',"danger");
		
		case "asset_group_added":
		return $this->template('New asset group has been added, this new addition has been logged into system logs');
		
		case "asset_group_add_failed":
		return $this->template('Sorry, adding new asset group has failed, this could be because of mismatch of field types, please revise your input and try again or contact your administrator',"danger");
		
		case "asset_group_updated":
		return $this->template('Asset group has been updated');
		
		case "asset_group_updated_failed":
		return $this->template('Sorry, updating asset group has failed, this could be because of mismatch of field types, please revise your input and try again or contact your administrator',"danger");
		
		case "asset_request_added":
		return $this->template('New asset request has been sent, this request has been logged into system logs');
		
		case "asset_request_failed":
		return $this->template('Sorry, adding new asset request has failed, this could be because of mismatch of field types, please revise your input and try again or contact your administrator',"danger");
		
		//controller -> admin
		case "location_added":
		return $this->template('New location has been added, this new addition has been logged into system logs');
		
		case "location_add_failed":
		return $this->template('Sorry, adding new location has failed, this could be because of mismatch of field types, please revise your input and try again or contact your administrator',"danger");
		
		case "user_added":
		return $this->template('New user has been added, this new addition has been logged into system logs');
		
		case "user_add_failed":
		return $this->template('Sorry, adding new user has failed, this could be because of mismatch of field types, please revise your input and try again or contact your administrator',"danger");
		
		case "user_group_added":
		return $this->template('New user has been added, this new addition has been logged into system logs');
		
		case "user_group_add_failed":
		return $this->template('Sorry, adding new user has failed, this could be because of mismatch of field types, please revise your input and try again or contact your administrator',"danger");
		
		case "user_password_reset_failed":
		return $this->template('Sorry, changing password has failed, please revise your input of current password, new password and confirm password.',"danger");
		
		case "user_password_reset_failed":
		return $this->template('Sorry, changing password has failed, please revise your input of current password, new password and confirm password.',"danger");
		
		case "user_password_reset":
		return $this->template('Password has been changed.',"success");
		
		}
	}
	public function template($string,$type="success"){	
			return '<div class="alert alert-'.$type.' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$string.'</div>';	
	}
}