<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Flow Class - Indonesia
 */
class Flow{

	public function __construct(){
		log_message('debug', "Msg class initiated");
		$this -> CI = & get_instance();
		$this->CI->load->model('Flow_model');
	}

	/**
	 * Get Flow
	 */
	public function get_flow($module,$doc,$doc_id)
	{
		//$var_flow = $this->CI->Flow_model->get_list_flow($module);
		//$flow_position = $this->CI->Flow_model->get_doc_position($module,$doc);
		
		$flow_before = $this->CI->Flow_model->get_flow_before($module,$doc);
		$flow_after = $this->CI->Flow_model->get_flow_after($module,$doc);
		
		/**
		 * Get the Prev proces (BEFORE)
		 */
		$next_doc_id = $doc_id;
		$temp_before = array();		
		for($z = 0;$z < count($flow_before); $z++){ 
			$temp_curr_before = $this->CI->Flow_model->get_data_before($flow_before[$z]['table_reff'],$next_doc_id);
			if(count($temp_curr_before)){
				array_push($temp_before,$temp_curr_before);
				$next_doc_id = $temp_curr_before[0]['reff_id'];
			}
		}		
		//var_dump($temp_before);

		/**
		 * Get the Next process (AFTER)
		 */
		$next_reff_id = $doc_id;
		$temp_after = array();
		for($zz = 0; $zz < count($flow_after); $zz++){ 
			$temp_curr_after = $this->CI->Flow_model->get_data_after($flow_after[$zz]['doc_type'],$flow_after[$zz]['table_reff'],$next_reff_id);
			if(count($temp_curr_after)){
				array_push($temp_after,$temp_curr_after);
				$next_reff_id = $temp_curr_after[0]['doc_id'];
			}
		}
		//var_dump($temp_after);

		$doc_before = array();
		foreach($temp_before as $before){
			foreach($before as $_before){
				if($_before['reff_type']){
					$table_now = $this->CI->Flow_model->get_table_info($_before['reff_type']);
					$curr_doc = $this->CI->Flow_model->get_doc($table_now[0]['table_info'],$_before['reff_id']);
					array_push($doc_before,$curr_doc);
				}				
			}
		}

		$doc_after = array();
		foreach($temp_after as $after){
			foreach($after as $_after){
				if($_after['reff_type']){
					$table_now = $this->CI->Flow_model->get_table_info($_after['reff_type']);
					$curr_doc = $this->CI->Flow_model->get_doc($table_now[0]['table_info'],$_after['doc_id']);
					array_push($doc_after,$curr_doc);
				}				
			}
		}

		$all_doc = array();
		$all_doc = array_merge($doc_before,$doc_after);
		return $all_doc;
	}
}