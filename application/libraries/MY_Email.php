<?php

class MY_Email extends CI_Email {
	
	private $CI;
	private $_queue_from_name	= "";
	private $_queue_from_email	= "";
	private $_queue_to_email	= "";
	private $_queue_message		= "";
	private $_queue_subject		= "";

	function __construct ($config = array()) {
		$this -> CI = & get_instance();
		parent::__construct($config);
	}
	
	function from ($from_email = "system@altus.skyware.systems", $from_name = "Altus Ltd.", $return_path = NULL) {
		$this->_queue_from_name = $from_name;
		$this->_queue_from_email = $from_email;
		
		parent::from($from_email, $from_name);
	}
	
	function to($to){
		$this->_queue_to_email = $to;
		
		parent::to($to);
	}
	
	function subject ($subject) {
		$this->_queue_subject = $subject;
		
		parent::subject($subject);
	}
	
	function message ($message) {
		$this->_queue_message = $message;
		
		parent::message($message);
	}
	
	public function send($queue=FALSE)
	{	
		if(!isset($this->_headers['From'])) $this -> from();
		if(!$queue){
			if ($this->_replyto_flag == FALSE)
			{
				$this->reply_to($this->_headers['From']);
			}
	
			if (( ! isset($this->_recipients) AND ! isset($this->_headers['To']))  AND
				( ! isset($this->_bcc_array) AND ! isset($this->_headers['Bcc'])) AND
				( ! isset($this->_headers['Cc'])))
			{
				$this->_set_error_message('lang:email_no_recipients');
				return FALSE;
			}
	
			$this->_build_headers();
	
			if ($this->bcc_batch_mode  AND  count($this->_bcc_array) > 0)
			{
				if (count($this->_bcc_array) > $this->bcc_batch_size)
					return $this->batch_bcc_send();
			}
	
			$this->_build_message();
	
			if ( ! $this->_spool_email())
			{
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	else {
		$data = array(
  		'from_email' 		=> $this -> _queue_from_email ,
		'from_name' 		=> $this -> _queue_from_name ,
  		'to_email' 			=> $this -> _queue_to_email ,
		'subject' 			=> $this -> _queue_subject ,
		'message' 			=> $this -> _queue_message ,
		'alt_message' 		=> $this -> alt_message 
		);
		return $this -> CI -> db -> insert('email_queue', $data) ? TRUE : FALSE ;
		}
	}
}