<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Journal class function and methods
 * this class is a place to put common function and methods to skyware app
 **/

/**
 * Example of usage:
 *
 * $this->journal->start('JRNL');
 * $this->journal->dr('inventory.asset',200000);
 * $this->journal->cr('purchase.accrue',200000);
 * $this->journal->end();
 * 
 */
  
class Journal{
	//protected $CI;
	private $jrnl_id;
	private $total_dr;
	private $total_cr;
	private $line;
	private $curr_type;
	public function __construct(){
		$this -> CI = & get_instance();
		$this->CI->load->model('document_model');
		$this->CI->load->model('journal_model');
		$this->CI->load->model('link_account_model');
		$this->jrnl_id = "";
		$this->line = 0;
		$this->total_dr = 0;
		$this->total_cr = 0;
	}

	public function start($type,$data)
	{
		
		$this->curr_type = $type;
		$curr_jur_num = $this->CI->document_model->getNextSeries($this->curr_type);
		if(!$curr_jur_num){echo "Document Type Series not exists."; return false;}
		if(!isset($data['doc_ref2'])){$data['doc_ref2']="";}
		if(!isset($data['doc_ref3'])){$data['doc_ref3']="";}
		$data_journal_header = array(
			"journal_code"		=> $curr_jur_num,
			"posting_date"		=> $data['doc_dt'],
			"document_date"		=> $data['doc_dt'],
			"journal_memo"		=> $data['doc_note'],
			"journal_type"		=> $type,
			"journal_ref1"		=> $data['doc_ref1'],
			"journal_ref2"		=> $data['doc_ref2'],
			"journal_ref3"		=> $data['doc_ref3'],
			"create_by"			=> $this->CI->session->userdata("username")
		);
		$this->CI->journal_model->table = 't_account_journal_header';
		$this->jrnl_id = $this->CI->journal_model->add_header($data_journal_header);	
	}

	public function add($dc,$acc,$num,$cc=0)
	{
		$link_acc = $this -> CI -> link_account_model -> get_account($acc);
        if(!$link_acc){
            show_error('Unable to get Link Account for code: `inventory.asset`');
            exit();
        }
        $this->line++;
        
		$data_journal_detail = array(
			"journal_id"		=> $this->jrnl_id,
			"journal_line"		=> $this->line,
			"account_id"		=> $link_acc,			
			"journal_cc"		=> $cc,
			"journal_curr"		=> 'IDR'
		);
		if($dc == "D"){
			$this->total_dr += $num;
			$data_journal_detail['journal_dr'] = $num;
			$data_journal_detail['journal_cr'] = 0;
		}else{
			$this->total_cr += $num;
			$data_journal_detail['journal_dr'] = 0;
			$data_journal_detail['journal_cr'] = $num;
		}
		$this->CI->journal_model->add_detail($data_journal_detail);
	}

	public function end($value='')
	{
		if($this->total_dr <> $this->total_cr){
			show_error("Unbalanced transaction");
			exit();
		}
		$this->CI->document_model->useDocSeries($this->curr_type);
	}
}