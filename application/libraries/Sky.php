<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Skyware class function and methods
 * this class is a place to put common function and methods to skyware app
 **/
  
class Sky{
	
	protected $CI;
	
	public function __construct(){
		$this -> CI = & get_instance();
	}

	public function trans_end($success_path,$failed_path=NULL){
		if ($this->CI->db->trans_status() === FALSE){
			$this->CI->db->trans_rollback();
			echo $this->CI->db->_error_message();
			return false;
		}else{
			$this->CI->db->trans_commit();			
			//echo "1"; return true;
			redirect($success_path);
		}
	}

	/*public function trans_end(){
		if ($this->CI->db->trans_status() === FALSE){
			$this->CI->db->trans_rollback();
			echo $this->CI->db->_error_message();
			return false;
		}else{
			$this->CI->db->trans_commit();			
			echo "1"; return true;
		}
	}*/

	public function load_page($page="index", $data = NULL, $msg = NULL, $cleanpage = NULL, $report = FALSE)
	{	
		$this -> CI -> load -> library('msg');
		$data['msg'] 		= ($msg) ? $this -> CI -> msg -> getMessage($msg) : (isset($data['msg']) ? $data['msg'] : NULL ) ;
		$controller 		= $this -> CI -> router -> fetch_class();
		if ($cleanpage) $this -> CI -> load -> view("$controller/$page",$data); //no header , side bar , footer
		else {
			$header['side_menu'] 	= $this -> display_children();
			// if ($this -> CI -> session -> userdata('user_id') !== FALSE){
			if ($this-> CI -> ion_auth->logged_in() && is_numeric($this-> CI ->ion_auth->user()->row()->id)){
				$uid = $this-> CI -> ion_auth ->user()->row()->id;
				$this->CI->load->model('approval_model');
				$this -> CI -> load -> model('user_model');

				$header['approval_count'] = $this -> CI -> approval_model -> get_approval_count ($uid) ;
				// $header['user_data'] = $this -> CI -> session ->all_userdata();
				// $header['user_data'] = $this->ion_auth->user()->row();
				$header['user_data'] = $this-> CI -> user_model->get_user_data($uid);
				$header['pending_approval'] = $this -> CI -> approval_model -> get_document_approval_count ( $this -> CI -> session -> userdata('user_id') );
				
				// asset daily reminder
				//$this -> CI -> load -> model ('asset_model');
				//$header['asset_reminder_count'] = $this -> CI -> asset_model -> get_daily_reminder_count ( $this -> CI -> session -> userdata('user_id') ) ;
				//$header['asset_reminders'] = $this -> CI -> asset_model -> get_daily_reminders ( $this -> CI -> session -> userdata('user_id') ) ;
			}
			else {
				redirect('auth/logout');
			}
			
			
			$this -> CI -> load -> view("template/header",$header);
			if ( $report ) $this -> CI -> load -> view ('report/view_header', $data);
			$this -> CI -> load -> view("$controller/$page", $data);
			if ( $report ) $this -> CI -> load -> view ('report/view_footer', $data);
			$this -> CI -> load -> view("template/footer");
			}
	}
	
	
	public function display_children($parent = 0) {
		
		$amstring = ($this->CI->input->cookie('_am',TRUE) == TRUE ? $this->CI->input->cookie('_am',TRUE) : "");
		$amarray = explode(',',$amstring);
		$amlastnode = $this->CI->input->cookie('_amlastnode',TRUE);
		
    	/*$query = $this -> CI -> db -> query ("SELECT *, Deriv.Count FROM `t_side_menu` a  LEFT OUTER JOIN (SELECT parent_id, COUNT(1) AS Count FROM `t_side_menu` WHERE active = 1 GROUP BY parent_id) Deriv ON Deriv.parent_id = a.menu_id WHERE a.active = 1 AND a.parent_id=" . $parent . " order by order_no ");
    	$result_array 	= $query -> result_array();*/

    	$this->CI->load->model('Menu_model');
    	$result_array 	= $this->CI->Menu_model->get_menu_child($parent);
		$string 		= '';
		$group_hidden   = $this -> CI -> session -> userdata('menu_hidden');
		
		//$active_menu = $this->get_active_menu();
		if($parent == 0){
			//var_dump($result_array);
			//var_dump($group_hidden);
		}
		
    	foreach ($result_array as $row) { 
    		if(!empty($group_hidden)){
    			if (in_array($row['menu_id'], $group_hidden)) continue;
    		}
    		

				$disabled = ($group_hidden) ? ((in_array($row['menu_id'],$group_hidden)) ? "style='display:none'" : ""):"";
				// $active = ( in_array($row['menu_id'],$active_menu) ? 'active' : '' ); // old method
				$active = ( (in_array($row['menu_id'],$amarray)) ? 'active' : '' ); // new method
				
				$string    .= ($row === reset($result_array) && $parent != 0) ? "<ul class='treeview-menu $active'>" : "";
				$class_name = (!$row['last_node']) 		? "class='treeview $active'" : "class='$active'";
				$link		= ($row['menu_link']!='') 	? base_url($row['menu_link']) : "#";
				if ($row['Count'] > 0) {
					$string .= 
						"<li $class_name $disabled data-menuid='$row[menu_id]'><a href='$link'>".
						$row['name_prefix'].
						"<span>" . $row['menu_name']. "</span>" .
						//$row['name_postfix'].
						'<i class="fa pull-right fa-angle-left"></i>' .
						"</a>";
					$string .= $this -> display_children($row['menu_id']);
					$string .= "</li>";
					} 
				elseif ($row['Count'] == 0) {
					$bold = ( $row['menu_id'] == $amlastnode ) ? 'style="font-weight:bold;color:white;"' : '';
					$string .= 
						"<li $class_name $disabled data-menuid='".$row['menu_id']."' data-lastnode='1'><a href='".$link."' $bold>".
						//$row['name_prefix'].
						"<i class='".$row['icon']." fa-fw'></i>".
						$row['menu_name'].
						//$row['name_postfix'].
						"</a>";
					$string .= "</li>";
				}
				if ($row === end($result_array) && $parent != 0)$string .= "</ul>";
		}
	return $string;
	}
	
	public function get_user_group($user_id){

		$this -> CI -> db -> select ('group_id');
		//$query  = $this -> CI -> db -> get_where('t_user_group_junc', array('user_id' => $user_id));
		$query  = $this -> CI -> db -> get_where('t_ion_users_groups', array('user_id' => $user_id));
		$result = $query -> result_array();
		foreach ($result as $row){
			$group_array [] = $row['group_id'];
			}
		return $group_array;	
	}
	
	public function get_menu_hide($user_group){
		foreach($user_group as $group_id){
			
			$this -> CI -> db -> select ('menu_id');
			$query  = $this -> CI -> db -> get_where('t_group_menu_hide', array('group_id' => $group_id));
			$result = $query -> result_array();
			}
		foreach ($result as $row){
			$menu_array [] = $row['menu_id'];
			}
		return (isset($menu_array))?$menu_array:NULL;
	}
	
	// IP, get parent list from active menu (TODO: this should be in menu models instead later)
	/* public function get_active_menu()
	{
		$menus = array();
		
		$current_link = $this -> CI -> router->fetch_class() . '/' . $this->CI->router->fetch_method();
		$current_menu = $this -> CI -> db -> select('menu_id')->get_where ('t_side_menu', array('menu_link' => $current_link))->row_array() ;
		if (count($current_menu) > 0){
			$current_menu_id = $current_menu['menu_id'];
			array_push($menus,$current_menu_id); // push self active menu as active
			do {
				$current_parent = $this->get_parent_id($current_menu_id);
				array_push($menus,$current_parent);
				$current_menu_id = $current_parent;
			} while ($current_parent > 0);
		}
		return $menus;
	} */
	
	// IP, get parent id from menu id (used in get_active_menu() above)
	/* public function get_parent_id($menu_id = 0){
		$m = $this -> CI -> db -> select('parent_id') -> get_where('t_side_menu', array('menu_id' => $menu_id)) -> row_array();
		return $m['parent_id'];
	} */
	
	public function get_random_key($l=32,$c='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890',$u = TRUE){ 
 		if (!$u) for ($s = '', $i = 0, $z = strlen($c)-1; $i < $l; $x = rand(0,$z), $s .= $c{$x}, $i++); 
 		else for ($i = 0, $z = strlen($c)-1, $s = $c{rand(0,$z)}, $i = 1; $i != $l; $x = rand(0,$z), $s .= $c{$x}, $s = ($s{$i} == $s{$i-1} ? substr($s,0,-1) : $s), $i=strlen($s)); 
 	return $s; 
	}
	
	public function label_box($string="",$label="primary"){ //label -> success, warning, primary, danger, info
		if(!strcasecmp($string,'active')||!strcasecmp($string,'active off')||!strcasecmp($string,'active'))$label='success';
		else if(!strcasecmp($string,'sold')||!strcasecmp($string,'written off')||!strcasecmp($string,'disposed')||!strcasecmp($string,'rejected'))$label='danger';
		else if(!strcasecmp($string,'pending')||!strcasecmp($string,'in transit'))$label='warning';
		else if(!strcasecmp($string,'incoming'))$label='info';
		return "<div style='text-align:center'><span class='label label-$label'>$string</span></div>";
		
		}

}