<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {


	
	protected $_error_prefix		= '<div class="callout callout-danger">';
	protected $_error_suffix		= '</div>';
	
	public function reset_post_data(){
    	$obj =& _get_validation_object();
    	foreach($obj->_field_data as $key){
    		$this->_field_data[$key['field']]['postdata'] = NULL;
    	}
    	return true;
    }
    
    public function error_array(){
        return $this->_error_array;
    }
	
	public function error_string($prefix = '', $suffix = '')
	{
		// No errrors, validation passes!
		if (count($this->_error_array) === 0)
		{
			return '';
		}

		if ($prefix == '')
		{
			//$prefix = $this->_error_prefix;
		}

		if ($suffix == '')
		{
			//$suffix = $this->_error_suffix;
		}

		// Generate the error string
		$str = $prefix;
		foreach ($this->_error_array as $val)
		{
			if ($val != '')
			{
				//$str .="<h5>".$val."</h5>";
				$str .= "\n".$val;
			}
		}
		$str .= $suffix;
		return $str;
	}
}