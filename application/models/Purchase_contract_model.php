<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class purchase_contract_model extends MY_Model {    
	public $table;
    function __construct(){
        // Call the Model constructor
        parent::__construct();
		$this->table = '';
		$this->load->model('document_model');		
    }
	
	public function list_contract($filters=NULL)
	{
		$this->db->select("*");
		$this->db->from("t_purchase_contract_header");
		if($filters)$this->db->where($filters);
		
		return $this->db->get()->result_array();
	}
	//AVER
	public function add($data){	
		$this->db->insert('t_purchase_contract_header',$data['t_purchase_contract_header']);
		$temp_temp_id = $this->db->insert_id();
		$temp_id = $temp_temp_id;
		foreach($data['t_purchase_contract_detail'] as $detail){
			$detail['doc_id'] = $temp_id;
			$this->db->insert('t_purchase_contract_detail',$detail);
		}
		$this->document_model->useDocSeries('PUR_CON');
	}
	public function view($where=NULL,$order=NULL){
		$this->db->select("*");
		$this->db->from('t_purchase_contract_header');
		$this->db->join('t_purchase_contract_detail','t_purchase_contract_header.doc_id = t_purchase_contract_detail.doc_id','inner');
		if($where)$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function edit($data,$where){
		$temp_where = $where;
		$temp_where['t_purchase_contract_header.doc_id'] = $where['doc_id'];
		unset($temp_where['doc_id']);
		$check = $this->view($temp_where);
		if($check[0]['doc_status'] != "DRAFT"){
			echo "Document not in edit mode.";
			return FALSE;
		}
		$this->db->update('t_purchase_contract_header',$data['t_purchase_contract_header'],$where);

		if(isset($data['t_purchase_contract_detail'])){
			foreach($data['t_purchase_contract_detail'] as $detail){
				$where['doc_line'] = $detail['doc_line'];
				$this->db->update('t_purchase_contract_detail',$detail,$where);
			}
		}
	}
	public function remove($where){		
	}

	public function count_doc(){
		return $this->db->count_all('t_purchase_contract_header');
	}

	//APPROVAL
	public function approve_purchase_contract($docnumber, $approval_status){
		$this -> db -> trans_begin();				
		$this -> db -> update ('t_purchase_contract_header', array("approval_status" => $approval_status), array('doc_num' => $docnumber));		
		$msg = '';
		switch ($approval_status){
			case '3':
				$this -> db -> update ('t_purchase_contract_header', array("doc_status" => "ACTIVE"), array('doc_num' => $docnumber));
				$msg = 'Purchase Contract has been approved.';
				break;
			case '4':
				$this -> db -> update ('t_purchase_contract_header', array("doc_status" => "DRAFT"), array('doc_num' => $docnumber));
				$msg = 'Purchase Contract has been revised.';
				break;
			case '5':
				$this -> db -> update ('t_purchase_contract_header', array("doc_status" => "CANCELLED"), array('doc_num' => $docnumber));
				$msg = 'Purchase Contract has been rejected.';
				break;
			default :
				$msg = '';
		}
		
		/*if($approval_status == '3' || $approval_status == '2'){
			if($approval_status == '3'){
				$this -> db -> update ('t_purchase_contract_header', array("doc_status" => "ACTIVE"), array('doc_num' => $docnumber));
			}
			$msg = 'Purchase Order has been approved.';
		}elseif ($approval_status == '5'){ 
			if($approval_status == '5'){
				$this -> db -> update ('t_purchase_contract_header', array("doc_status" => "CANCELLED"), array('doc_num' => $docnumber));
			}
			$msg = 'Purchase Order has been rejected.';	
		}*/
		$this -> db -> query("call procUpdateQtyOnHand()");		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return array('success'=>0,'msg'=>'Failed to approve the document ');
		}else{
			$this->db->trans_commit();			
			return array('success'=>1,'msg'=>$msg);
		}

	}
	public function review_purchase_contract($docnumber){
		$doc = array(
			'header' => $this -> db -> get_where('v_purchase_contract', array('doc_num'=>$docnumber))->row_array(),
			'detail' => $this -> db -> get_where('v_purchase_contract_detail', array('doc_num'=>$docnumber))->result_array(),
			);
		//return $this -> db -> get_where('t_purchase_contract_header', array('doc_num'=>$docnumber))->result_array();
		return $doc;
	}
	
	

	public function getAll($filter=NULL,$page=0){
		$this->db->select('*');
		$this->db->from('t_purchase_contract_header');
		$this->db->join('t_business','t_purchase_contract_header.buss_id = t_business.buss_id','inner');
		//$this->db->join('t_site_location','t_purchase_contract_header.whse_id = t_site_location.location_id','inner');
		if($filter)$this->db->where('t_purchase_contract_header.doc_status',$filter);
		$this->db->order_by('doc_id','desc');
		$query = $this->db->get('',10,$page);
		return $query -> result_array();
	}
	
	public function getAll_detail(){
		$query = $this -> db -> query ("
			select t_purchase_contract_detail.*, t_asset_group.group_name from t_purchase_contract_header 
			inner join t_purchase_contract_detail on t_purchase_contract_header.doc_id = t_purchase_contract_detail.doc_id
			inner join t_business on t_purchase_contract_header.buss_id = t_business.buss_id
			inner join t_site_location on t_site_location.location_id = t_purchase_contract_header.whse_id
			inner join t_asset_group on t_asset_group.group_id = t_purchase_contract_detail.item_group
			where doc_status = 'ACTIVE'
		");
		return $query -> result_array();
	}
	public function getAll_with_vendor($id){
		$this->db->select('*');
		$this->db->from('t_purchase_contract_header');
		$this->db->join('t_business','t_purchase_contract_header.buss_id = t_business.buss_id','inner');
		//$this->db->join('t_site_location','t_site_location.location_id = t_purchase_contract_header.whse_id','inner');
		
		$this->db->where('doc_status','ACTIVE');
		
		if($id != NULL){
			$this->db->where('t_business.buss_id',$id);
		}
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	
	
	public function set_item_close($code){	
		$this->db->select('*');
		$this->db->from('t_purchase_contract_header');
		$this->db->join('t_purchase_contract_detail','t_purchase_contract_header.doc_id = t_purchase_contract_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		foreach($query->result_array() as $row){
			if($row['item_qty_closed'] >= $row['item_qty']){
				$data = array("line_close" => 1);
			}else{
				$data = array("line_close" => 0);
			}
			$this->db->where('doc_id',$row['doc_id']);
			$this->db->where('doc_line',$row['doc_line']);				
			$this->db->update('t_purchase_contract_detail', $data);	
		}
	}	
	public function set_doc_close($code){
		$this->db->select('line_close');
		$this->db->distinct('line_close');
		$this->db->from('t_purchase_contract_header');
		$this->db->join('t_purchase_contract_detail','t_purchase_contract_header.doc_id = t_purchase_contract_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		$temp = $query->result_array();		
		$doc_status = "ACTIVE";
		$log_status = "PENDING";
		if(count($temp) > 1){
			$log_status = "PARTIAL";
		}else{
			if($temp[0]['line_close'] == 1){
				$doc_status = "CLOSED";
				$log_status = "RECEIVED";				
			}
		}
		$data = array("doc_status"=>$doc_status);
			$this->db->update('t_purchase_contract_header', $data, array('doc_num' => $code));
	}	
	
	public function get_qty_close($code){
		$this->db->select('*');
		$this->db->from('t_purchase_contract_header');
		$this->db->join('t_purchase_contract_detail','t_purchase_contract_header.doc_id = t_purchase_contract_detail.doc_id','inner');
		$this->db->where('line_close',0);
		$this->db->where('item_qty_closed > 0');
		$query = $this->db->get();
		return $query -> result_array();	
	}
	public function get_close_line($code){
		$this->db->select('*');
		$this->db->from('t_purchase_contract_header');
		$this->db->join('t_purchase_contract_detail','t_purchase_contract_header.doc_id = t_purchase_contract_detail.doc_id','inner');
		$this->db->where('line_close',1);
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();		
	}
	public function get_open_line($code){
		$this->db->select('*');
		$this->db->from('t_purchase_contract_header');
		$this->db->join('t_purchase_contract_detail','t_purchase_contract_header.doc_id = t_purchase_contract_detail.doc_id','inner');
		$this->db->where('line_close',0);
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();		
	}
	
	public function insert_header($data){
		$this->db->insert('t_purchase_contract_header',$data);
		return $this->db->insert_id();
	}
	public function insert_detail($data){
		$this->db->insert('t_purchase_contract_detail',$data);	
		return $this->db->insert_id();
	}
	
	public function update_header($data,$where){
		$this->db->where($where);		
		return $this->db->update('t_purchase_contract_header', $data);	
	}
	
	public function delete_detail($code){
		$this->db->where('doc_id', $code);
		$this->db->delete('t_purchase_contract_detail');
		return $this->db->affected_rows();
	}
	
	public function getHeader($doc){
		$this->db->select("*");
		$this->db->from("t_purchase_contract_header");
		$this->db->join("t_business","t_business.buss_id = t_purchase_contract_header.buss_id");
		$this->db->where("doc_num",$doc);
		$query = $this->db->get();
		return $query -> result_array();
	}
	public function getDetail($doc){
		$this->db->select("*,t_purchase_contract_detail.item_name as 'item_name_po'");
		$this->db->from('t_purchase_contract_detail');
		$this->db->join('t_purchase_contract_header','t_purchase_contract_header.doc_id = t_purchase_contract_detail.doc_id','inner');
		$this->db->join('t_inventory','t_inventory.item_id = t_purchase_contract_detail.item_id','inner');
		$this->db->where('t_purchase_contract_header.doc_num',$doc);
		$query = $this->db->get();
		return $query -> result_array();
	}
	public function getDetail_pct_open($doc){
		$query = $this -> db -> query ("
			select *, t_purchase_contract_detail.item_name as 'item_name_po' from t_purchase_contract_detail
			inner join t_inventory on t_purchase_contract_detail.item_id = t_inventory.item_id
			where doc_id = (select doc_id from t_purchase_contract_header where doc_num = '".$doc."')
			order by doc_line
		");
		return $query -> result_array();
	}
	public function getStatus($doc){
		$this->db->select("doc_status");
		$this->db->from("t_purchase_contract_header");
		$this->db->where('doc_num',$doc);
		
		return $this->db->get()->result_array();
	}


	/**
	 * REPORT MODEL
	 */
	public function purchase_contract_report($filters){
		if($filters['date_from']){
			$this->db->where('doc_dt >=', $filters['date_from']);
		}
		if($filters['date_to']){
			$this->db->where('doc_dt <=', $filters['date_to']);
		}
		if($filters['doc_status']){
			$this->db->where('doc_status',$filters['doc_status']);
		}
		$this->db->from('v_purchase_contract');
		return $this->db->get()->result_array();
	}

	public function purchase_contract_detail_report($filters){
		if($filters['date_from']){
			$this->db->where('doc_dt >=', $filters['date_from']);
		}
		if($filters['date_to']){
			$this->db->where('doc_dt <=', $filters['date_to']);
		}
		$this->db->from('v_purchase_contract_detail');
		return $this->db->get()->result_array();
	}
}