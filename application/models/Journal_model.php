<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Journal_model extends MY_Model{
	public $table;
	public function __construct(){
		parent::__construct();
		log_message("debug", "Asset helper model class initiated");
	}
	public function add_header($data)
	{
		$this->db->insert('t_account_journal_header',$data);
		return $this->db->insert_id();
	}
	public function add_detail($data)
	{
		$this->db->insert('t_account_journal_detail',$data);
	}
	
	public function add($data){
		$this->db->insert('t_account_journal_header',$data['t_account_journal_header']);
		$temp_temp_id = $this->db->insert_id();
		$temp_id = $temp_temp_id;
		foreach($data['t_account_journal_detail'] as $detail){			
			$detail['journal_id']=$temp_id;
			$this->db->insert('t_account_journal_detail',$detail);
		}
		$this->document_model->useDocSeries('JRNL');
	}
	public function view($where=NULL){
		$this->db->select('*');
		$this->db->from('t_account_journal_header');
		if($where) $this->db->where($where);
		$this->db->order_by('journal_id','desc');
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	public function edit($data,$where){
		$this->db->update('t_account_journal_header',$data['t_account_journal_header'],$where);
		/*foreach($data['t_account_journal_detail'] as $detail){
			$this->db->update('t_account_journal_detail',$detail,
				array('journal_id'=>$data['t_account_journal_header']['journal_id'],
					'journal_line'=>$detail['journal_line']
				)
			);
		}*/
	}
	public function count_doc(){		
		return $this->db->count_all('t_account_journal_header');
	}

	public function getAll($filter=NULL,$page=0){
		$this->db->select('*');
		$this->db->from('t_account_journal_header');
		if($filter)$this->db->where('t_account_journal_header.journal_status',$filter);
		$this->db->order_by('journal_id','desc');
		$query = $this->db->get('',10,$page);
		return $query->result_array();
	}

	public function getHeader($code){
		$this->db->select('t_account_journal_header.*');
		$this->db->from('t_account_journal_header');
		$this->db->where('journal_code',$code);
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	public function getDetail($code){
		$this->db->select('*');
		$this->db->from('t_account_journal_header');
		$this->db->join('t_account_journal_detail','t_account_journal_header.journal_id = t_account_journal_detail.journal_id','inner');
		$this->db->join('t_account','t_account.account_id = t_account_journal_detail.account_id','inner');
		$this->db->where('journal_code',$code);
		$this->db->order_by('journal_line','asc');
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}

	public function insert_debet($id,$account,$amount){
		/*$temp_data['journal_id'] = $id;
		$temp_data['journal_code'] = $account;
		$temp_data['journal_']
		$this->db->insert('')*/
	}
	
}