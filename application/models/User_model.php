<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	/**
	*
	* sql_details contains database credentials
	* to be used for datatables SSP module
	*
	**/
	
	var $sql_details;

	public function __construct()
	{
        parent::__construct();
        $this->sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);
    }

   public function _get_id($username)
   {
      $this->db->select("id");
      $this->db->from("t_ion_users");
      $this->db->where("username",$username);
      $query = $this->db->get()->row_array();
      return $query['id'];
   }
   	public function get_user_data($user_id = 0)
   	{
   		$this->db->where('user_id', $user_id);
   		return $this -> db ->get('v_users', 1) -> row_array();
   	}

   	public function get_all()
   	{
   		return $this -> db -> get('v_users') -> result_array();
   	}

   public function get_user_groups()
   {
      return $this->db->get('t_ion_groups')->result_array();
   }
   	/**
   	*
   	* add_user = register new user for ion_auth::register()
   	* 	$username = 'benedmunds';
	*	$password = '12345678';
	*	$email 	  = 'ben.edmunds@gmail.com';
	*	$additional_data = array(
	*			 'first_name' => 'Ben',
	*			 'last_name' => 'Edmunds'
	*			);
	*	$this->ion_auth->register($username, $password, $email, $additional_data, $group)
   	*
   	**/
   	
   	public function add_user($data)
   	{

   		if($data){
   			$username = $data['user_name'];
   			$password = $data['user_pass'];
   			$email = $data['user_email'];
   			$additional_data = array(
   				'first_name' => $data['first_name'],
   				'last_name' => $data['last_name']
   				);
   			$group = $data['group_id'];
   			$id = $this->ion_auth->register($username, $password, $email, $additional_data, $group);
   			if($id && is_numeric($id))
   				return $id;
   			else
   				return 'error';
   		} else
   			return false;
   	}

   	/**
   	*
   	* functions to be returned specially for datatables
   	*
   	**/
   	
   	public function datatable_settings_users($return = "datatable_array"){
		$table 		= 'v_users';
		$primaryKey = 'user_id';
		
		$columns = array(
			array( 'db' => 'full_name'		,'dt' => 0 ),
			array( 'db' => 'location_name'	,'dt' => 1 ),
			array( 'db' => 'position_name'	,'dt' => 2 ),
			array( 'db' => 'email'			,'dt' => 3 ),
         array( 'db' => 'user_id'         ,'dt' => 4 , 'formatter' => function ($d, $row){return "<a href='". site_url('setting/edit_user')."/$d'><i class='fa fa-pencil'></i> Edit</a>";}),
			);
		$this -> load -> library('ssp');
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns);
		}

      /**
      *
      * Employee add function
      *
      **/
      public function add_employee($data)
      {
         return $this->db->insert('t_employee', $data);
      }

}

/* End of file User_model.php */
/* Location: ./application/models/user_model.php */