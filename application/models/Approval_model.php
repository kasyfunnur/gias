<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Approval_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	function getAll()
	{
		return $this->db->get('t_approval')->result_array();
	}
	
	function getApprovalById($appr_id = NULL)
	{
		return $this->db->get_where('t_approval', array('appr_id'=>$appr_id))->row_array();
	}
	
	function getApprovalDetail($appr_id = NULL)
	{
		if ($appr_id == NULL) return false;
		
		return $this -> db -> get_where('v_approval_detail', array('appr_id'=>$appr_id)) -> result_array();
	}
	
	function getApprovalSetting($appr_code,$requestor_user_id)
	{
		return $this->db->get('v_approval_setting')->result_array();
	}
	
	/**
	 * Request Approval for Any Document
	 * Usage Example from a Controller:
	 * 	$this->load->model('approval_model');
	 * 	$approval_status = $this->Approval_model->requestApproval(1,'pr','pr-00012';
	 * 	
	 * 	if ( $approval_status === FALSE ){
	 * 		// no approval settings that match for user id 1 and 'pr' document
	 * 		echo 'not success';
	 * 		
	 * 	} else {
	 * 		// successfully requested
	 * 		echo "the approval status is $approval_status"; // (see Document Approval Status below)			
	 * 	}
	 * 
	 */
	
	/**
	 * 	Document Approval Status :
	 * 	1 - Pending 			: No one approved yet
	 * 	2 - Parital Approval 	: Partially approved
	 * 	3 - Approved 			: Fully Approved
	 * 	4 - Revise 				: Anyone of the approver ask for revision
	 * 	5 - Rejected 			: Anyone of the approver reject the request
	 */
	
	/** NEW APPROVAL BASED ON POSITION **/
	function request_approval($user_id, $appr_code, $document_number)
	{
		$approval_settings = $this->check_user_approval($user_id, $appr_code);
		if ($approval_settings === FALSE || $document_number == "" || $document_number == NULL) {
			return 1;
		} else {
			$this -> load -> helper ('approval');			
			$notify = array();			
			/* check whether this approval is step based or not
				step based means we go through each approval step first before
				notifying the next set of users for approval
			*/			
			$step_based = ( $approval_settings['step_based'] == 0 ? FALSE : TRUE );			
			$approvals = expand_matrix($approval_settings['approval_matrix']);			
			for ( $a = 0 ; $a < count($approvals) ; $a++ )
			{
				if ( is_numeric($approvals[$a]['position']) ){
					$ukey = $this->sky->get_random_key();
					$data = array(
						'appr_detail_id' 		=> $approval_settings['appr_detail_id'],
						'doc_number' 			=> $document_number,
						'requestor_position_id'	=> $approval_settings['requestor_position_id'],
						'requestor_user_id' 	=> $user_id,
						'request_datetime' 		=> date('Y-m-d H:i:s',time()),
						'approver_step'			=> $approvals[$a]['step'],
						'approver_position_id'	=> $approvals[$a]['position'],
						// 'approver_user_id'		=> $approval_settings["approver_user_id{$a}"], 
						'approver_action' 		=> 1, // see document approval status above
						'unique_key' 			=> $ukey
					);					
					$this -> db -> insert('t_document_approval', $data);
					$doc_id = $this -> db -> insert_id();
					$users = $this -> get_users_in_position($approvals[$a]['position']);					
					if ( $approval_settings['appr_email'] == 1 ) {
						foreach ($users as $usr){
							if (trim($usr['user_email']) != ''){
								array_push( $notify , array(
									'user_name' => $usr["full_name"],
									'user_email'=> $usr["user_email"],
									'user_step' => $approvals[$a]['step'],
									'user_unique_key' => $ukey,
									'user_doc_id' => $doc_id
									)
								);
							}
						}
					}
				}
			}
			
			/** EMAIL NOTIFICATION **/
			
			if (count($notify)){
				$this -> load -> library ('email_lib');
				$this -> load -> model ( $approval_settings['appr_model'], 'doc_model' );
				
				for ( $i = 0 ; $i < count($notify) ; $i++ ){
					
					if (!$step_based || ($step_based && $notify[$i]['user_step'] == 1)) {
					
						$temp['approver_name'] 	= $notify[$i]['user_name'];
						$temp['subject'] 		= "New Request: $approval_settings[appr_name] from $approval_settings[position_name]";
						$temp['document']		= $this -> doc_model -> {$approval_settings['review_function']} ($document_number);
						$temp['key']			= $notify[$i]['user_unique_key']; //$data['unique_key'];
						$temp['doc_id']			= $notify[$i]['user_doc_id']; //$doc_id;
						
						$this -> email_lib -> to_email = $notify[$i]['user_email'];
						$this -> email_lib -> template("request_approval", $appr_code ,$temp);
					
						try {
							$success = $this -> email_lib -> send_email (FALSE); // FALSE indicates no email queue
						} catch (Exception $e) {
							log_message('info','unable to send email : ' . $e->getMessage());
						}
					}
				}
			}			
			return ($approval_settings['auto_approve'] == 1) ? 3 : 1;   // bypass the approval and auto approve the document			
		}		
	}
	
	
	/**
	 * Request Approval Checking before submitting a document
	 * Usage example :
	 * 	$this->load->model('approval_model');
	 * 	if ( $this->Approval_model->checkRequestApproval(1,'pr') ){
	 * 		// successfully requested
	 * 		echo '<button value="Confirm Document">';
	 * 	} else {
	 * 		// no approval settings that match for user id 1 and 'pr' document
	 * 		echo '<button disabled value="Confirm Document">;
	 * 	}
	 */
	
	/** new checking function based on position **/
	function check_user_approval($user_id, $appr_code){		
		$requestor_position_id = $this->get_user_position($user_id);		
		$approval_settings = $this->db->get_where('v_approval_detail', array('requestor_position_id'=> $requestor_position_id, 'appr_code'=>$appr_code))->row_array();
		if (count($approval_settings) > 0)
			return $approval_settings;
		else
			return FALSE;
	}
	
	function get_user_position($user_id = NULL){
		if ($user_id == NULL || $user_id == 0) return 0;
		//$pos = $this -> db -> select ('position_id') -> get_where ('t_user', array('user_id'=>$user_id)) -> row_array();
		// ind fix for ion auth
		$pos = $this -> db -> select ('position_id') -> get_where ('t_employee', array('user_id'=>$user_id)) -> row_array();
		return ( count($pos) ? $pos['position_id'] : 0 );
	}
	
	function get_users_in_position($position_id = NULL){
		if ($position_id == NULL) return FALSE;
		
		//return $this -> db -> select('user_id, user_name, full_name, user_email') -> get_where ('t_user', array('position_id' => $position_id))->result_array();
		// ind fix for ion auth
		return $this -> db -> select('user_id, username as user_name, full_name, email as user_email') -> get_where ('v_users', array('position_id' => $position_id))->result_array();
	}
	
	
	/**
	 * 	Get Pending Approval Count for the logged in User (NEW BASED ON POSITION)
	*/
	function get_approval_count($user_id = NULL){
		if ($user_id == NULL){
			// try to obtain session data
			//if ( $this->session->userdata('user_id') !== NULL && is_numeric($this->session->userdata('user_id')))
			if ($this->ion_auth->logged_in() && is_numeric($this->ion_auth->user()->row()->id))
			{
				$user_id = $this->ion_auth->user()->row()->id;
			} else {
				return 0 ;
			}
		}
	
			$approver_position_id = $this->get_user_position($user_id);
			
			$this->db->where("(v.approver_position_id = $approver_position_id AND v.approver_action = 1 AND v.step_based = 0) OR (v.approver_position_id = $approver_position_id AND v.approver_action = 1 AND v.step_based = 1 AND v.approver_step = 1) OR (v.approver_position_id = $approver_position_id AND v.approver_action = 1 AND v.step_based = 1 AND v.approver_step = 1 AND IFNULL((select count(1) from t_document_approval t where t.approver_action != 3 and t.approver_step < v.approver_step and t.doc_number = v.doc_number),0) = 0)", NULL, FALSE);
			$this->db->from('v_document_approval v');
			
			return $this->db->count_all_results();
		
	}
	
	
	function notify_next_step($doc_approval_id)
	{
		$this -> db -> where ('doc_approval_id', $doc_approval_id);
		$current_approval = $this -> db -> get ('v_document_approval') -> row_array();
		
		if ($current_approval['step_based'] == 0)
			return ;
		
		$current_step = $current_approval['approver_step'];
		
		$this -> db -> where ( array (
			'doc_number' => $current_approval['doc_number'],
			'approver_step' => $current_step + 1
			) ) ;
		$next_approval = $this -> db -> get ('v_document_approval') -> result_array();
		
		if (count($next_approval))
			{
			
			$notify = array();
			
			foreach ($next_approval as $appr){
				
				$users = $this -> get_users_in_position($appr['approver_position_id']);
				
				if ( $appr['appr_email'] == 1 ) {
					foreach ($users as $usr){
						if (trim($usr['user_email']) != ''){
							array_push( $notify , array(
								'user_name' => $usr["full_name"],
								'user_email'=> $usr["user_email"],
								'user_unique_key' => $appr['unique_key'],
								'user_doc_id' => $appr['doc_approval_id']
								)
							);
						}
					}
			}
		}
		
			if (count($notify)){
				$this -> load -> library ('email_lib');
				$this -> load -> model ( $next_approval[0]['appr_model'], 'doc_model' );
				
				for ( $i = 0 ; $i < count($notify) ; $i++ ){
					
					//if (!$step_based || ($step_based && $notify[$i]['user_step'] == 1)) {
					
						$temp['approver_name'] 	= $notify[$i]['user_name'];
						$temp['subject'] 		= "New Request: {$next_approval[0]['appr_name']} from {$next_approval[0]['requestor_full_name']}";
						$temp['document']		= $this -> doc_model -> {$next_approval[0]['review_function']} ($current_approval['doc_number']);
						$temp['key']			= $notify[$i]['user_unique_key']; //$data['unique_key'];
						$temp['doc_id']			= $notify[$i]['user_doc_id']; //$doc_id;
		
						$this -> email_lib -> to_email = $notify[$i]['user_email'];
						$this -> email_lib -> template("request_approval", $next_approval[0]['appr_code'] ,$temp);
		
						try {
							$success = $this -> email_lib -> send_email (FALSE); // FALSE indicates no email queue
						} catch (Exception $e) {
							log_message('info','unable to send email : ' . $e->getMessage());
						}
					//}
				}
			 }
		}
	}
	
	/**
	 * 	Get Pending Approval Count Per Document for the logged in User (NEW BASED ON POSITION)
	*/
	function get_document_approval_count($user_id = NULL, $document_code = NULL){
		if ($user_id == NULL){
			// try to obtain session data
			if ( $this->session->userdata('user_id') !== FALSE && is_numeric($this->session->userdata('user_id')))
			{
				$user_id = $this->session->userdata('user_id');
			} else {
				return 0 ;
			}
		}
		
		$approver_position_id = $this->get_user_position($user_id);
		
		$this->db->select('appr_id, appr_code, appr_name, count(1) as approval_count');
		//$this->db->where( array('approver_position_id' => $approver_position_id, 'approver_action' => 1));
		$this->db->where("(v.approver_position_id = $approver_position_id AND v.approver_action = 1 AND v.step_based = 0) OR (v.approver_position_id = $approver_position_id AND v.approver_action = 1 AND v.step_based = 1 AND v.approver_step = 1) OR (v.approver_position_id = $approver_position_id AND v.approver_action = 1 AND v.step_based = 1 AND v.approver_step = 1 AND IFNULL((select count(1) from t_document_approval t where t.approver_action != 3 and t.approver_step < v.approver_step and t.doc_number = v.doc_number),0) = 0)",NULL,FALSE);
		if ($document_code != NULL || $document_code != "")
			$this->db->where( array('appr_code' => $document_code) );
		$this->db->group_by(array("appr_id", "appr_code", "appr_name")); 
		$this->db->order_by("appr_name", "asc");
		return $this->db->get('v_document_approval v')->result_array();
			
		
	}


	/**
	 * 	Get Pending Approvals
	**/
	public function get_pending_doc($doc_num)
	{
		if($doc_num != NULL){
			$this->db->where('doc_number',$doc_num);
			return $this->db->get('v_document_approval')->result_array();
		}
	}

	function get_pending_approvals($user_id = NULL, $appr_id = NULL)
	{
		if ($user_id != NULL and is_numeric($user_id))
		{
			$approver_position_id = $this->get_user_position($user_id);
			//$this->db->where( array('approver_position_id' => $approver_position_id, 'approver_action' => 1));
			$this->db->where("
				(v.approver_position_id = $approver_position_id AND v.approver_action = 1 AND v.step_based = 0) OR 
				(v.approver_position_id = $approver_position_id AND v.approver_action = 1 AND v.step_based = 1 AND 
					v.approver_step = 1) OR 
				(v.approver_position_id = $approver_position_id AND v.approver_action = 1 AND v.step_based = 1 AND 
					v.approver_step = 1 AND 
						IFNULL((select count(1) from t_document_approval t 
							where t.approver_action != 3 and 
							t.approver_step < v.approver_step and 
							t.doc_number = v.doc_number),0) = 0)
			");
			if (is_numeric($appr_id) && $appr_id > 0)
				$this->db->where( array('appr_id' => $appr_id) );
			return $this->db->get('v_document_approval v')->result_array();
		}
		
		return false;
	}
	
	function get_line_approval($doc_approval_id, $unique_key)
	{
		return $this->db->get_where('v_document_approval',
			array('doc_approval_id'=>$doc_approval_id, 'unique_key'=>$unique_key))->row_array();
	}
	
	function update_approval($doc_approval_id, $approval_action)
	{
		$this->db->where('doc_approval_id',$doc_approval_id);
		return $this->db->update('t_document_approval', array('approver_action'=>$approval_action));
	}
	
	function check_previous_step($doc_number,$current_step){
		$this->db->where('doc_number',$doc_number);
		$this->db->where('approver_step < ',$current_step);
		$this->db->where('approver_action !=',3);
		$this->db->from('v_document_approval');
		$result_count = $this->db->count_all_results();
		return ($result_count == 0 ? TRUE : FALSE);
	}
	
	function check_complete_approval($doc_approval_id)
	{
		$this->db->where('doc_approval_id',$doc_approval_id);
		$approval_row = $this->db->get('t_document_approval')->row_array();
		$doc_number = $approval_row['doc_number'];
		
		if($doc_number == "")
			return false;
		
		$this->db->where('doc_number',$doc_number);
		$this->db->from('t_document_approval');
		$total_doc = $this->db->count_all_results();
		if ($total_doc == 1 )
			return true;
			
		if ($total_doc == 0 )
			return false;
		
		$this->db->where('doc_number',$doc_number);
		$this->db->where('doc_approval_id !=',$doc_approval_id);
		$this->db->where('approver_action',3);
		$this->db->from('t_document_approval');
		$approved_other_line = $this->db->count_all_results();
		
		if ($approved_other_line == $total_doc - 1)
			return true;
		else 
			return false;
			
	}


	/**
	*
	* Update approval setting (header and detail)
	*
	**/
	
	public function update_approval_setting($approval_id, $data)
	{
		// $data is expected to be ['header'] & ['detail']

		$this -> db -> trans_start();


		// update header
			$this->db->update('t_approval', $data['header'], array('appr_id'=>$approval_id));

		// clear detail
			$this->db->where('appr_id', $approval_id);
			$this->db->delete('t_approval_detail');

		// insert detail
			$this->db->insert_batch('t_approval_detail', $data['detail']);

		$this-> db ->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return false;
		} else {
			return true;
		}

		//$this->db->update('t_approval',$data,$where);
	}
}