<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  /**
   *
   */
  class Employee_model extends CI_Model
  {

    function __construct()
    {
      // code...
    }

    public function ambil($id = FALSE)
    {
      if($id)
  			$this->db->where('idEmp', $id);

  		return ($id) ? $this->db->get('t_shift')->row_array() : $this->db->get('v_employees')->result_array();
    }

    public function ambilId($id)
    {
      $query = $this->db->get_where('v_shift', array('idEmp' => $id));
      return $query->row_array();

      // $query = $this->db->query("SELECT * FROM t_shift WHERE idEmp = '.$id.' ");
      // $query = $this->db->query('SELECT * FROM `t_shift` WHERE idEmp = ".$id." ');
      // return $query->result();
    }

    public function ambilkId($id)
    {
      $query = $this->db->get_where('v_shift', array('idEmp' => $id));
      return $query->result_array();
    }
  }
