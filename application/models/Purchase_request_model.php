<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class purchase_request_model extends MY_Model {        
    function __construct()
    {
        parent::__construct();
        $this->load->model('document_model');
    }
	
	//AVER
	public function add($data){
		$this->db->insert('t_purchase_request_header',$data['t_purchase_request_header']);
		$temp_temp_id = $this->db->insert_id();
		$temp_id = $temp_temp_id;
		foreach($data['t_purchase_request_detail'] as $detail){
			$detail['doc_id'] = $temp_id;
			$this->db->insert('t_purchase_request_detail',$detail);
		}
		$this->document_model->useDocSeries('PUR_REQ');
	}
	


	public function getAll($where=NULL){
		$this->db->select('*');
		$this->db->from('t_purchase_request_header');
		$this->db->join('t_site_location','t_purchase_request_header.whse_id = t_site_location.location_id','inner');
		if($where)$this->db->where($where);
		$query = $this->db->get();		
		return $query -> result_array();
	}
	public function getHeader($code){
		$this->db->select('*');
		$this->db->from('t_purchase_request_header');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function getDetail($code){
		$this->db->select('*');
		$this->db->from('t_purchase_request_header');
		$this->db->join('t_purchase_request_detail','t_purchase_request_header.doc_id=t_purchase_request_detail.doc_id','inner');
		$this->db->where('t_purchase_request_header.doc_num',$code);
		$query = $this->db->get();
		return $query->result_array();
	}
}