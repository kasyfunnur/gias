<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class sales_invoice_model extends MY_Model {
    var $details;
	public $table;
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->table = '';
    }	
	
	public function getAll(){
		$query = $this -> db -> query ("
			select * from t_sales_invoice_header 
			inner join t_business on t_sales_invoice_header.buss_id = t_business.buss_id
			left join t_site_location on t_site_location.location_id = t_sales_invoice_header.whse_id			
			order by doc_id desc
		");
		return $query -> result_array();
	}
	public function getAll_detail(){
		$query = $this -> db -> query ("
			select t_sales_invoice_detail.*, t_asset_group.group_name from t_sales_invoice_header 
			inner join t_sales_invoice_detail on t_sales_invoice_header.doc_id = t_sales_invoice_detail.doc_id
			inner join t_business on t_sales_invoice_header.buss_id = t_business.buss_id
			inner join t_site_location on t_site_location.location_id = t_sales_invoice_header.whse_id
			inner join t_asset_group on t_asset_group.group_id = t_sales_invoice_detail.item_group
			where doc_status = 'ACTIVE'
		");
		return $query -> result_array();
	}
	public function getAll_with_customer($id){
		$this->db->select('*');
		$this->db->from('t_sales_invoice_header');
		$this->db->join('t_business','t_sales_invoice_header.buss_id = t_business.buss_id','inner');
		$this->db->join('t_site_location','t_site_location.location_id = t_sales_invoice_header.whse_id','inner');
		$this->db->where('doc_status','ACTIVE');
		$this->db->where_not_in('logistic_status','RECEIVED');
		if($id != NULL){
			$this->db->where('t_business.buss_id',$id);
		}
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	
	public function get_ref($code){
		$this->db->select('reff_id');
		$this->db->distinct('reff_id');
		$this->db->from('t_sales_invoice_header');
		$this->db->join('t_sales_invoice_detail','t_sales_invoice_header.doc_id = t_sales_invoice_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();
	}
	
	
	public function get_qty_close($code){
		$this->db->select('*');
		$this->db->from('t_sales_invoice_header');
		$this->db->join('t_sales_invoice_detail','t_sales_invoice_header.doc_id = t_sales_invoice_detail.doc_id','inner');
		$this->db->where('line_close',0);
		$this->db->where('item_qty_closed > 0');
		$query = $this->db->get();
		return $query -> result_array();	
	}
	public function get_close_line($code){
		$this->db->select('*');
		$this->db->from('t_sales_invoice_header');
		$this->db->join('t_sales_invoice_detail','t_sales_invoice_header.doc_id = t_sales_invoice_detail.doc_id','inner');
		$this->db->where('line_close',1);
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();		
	}
	public function get_open_line($code){
		$this->db->select('*');
		$this->db->from('t_sales_invoice_header');
		$this->db->join('t_sales_invoice_detail','t_sales_invoice_header.doc_id = t_sales_invoice_detail.doc_id','inner');
		$this->db->where('line_close',0);
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();		
	}
	
	public function get_del_inv_status_pending($del){
		$this->db->select('*');
		$this->db->from('t_sales_invoice_header');
		$this->db->where("(financial_status = 'PARTIAL' OR financial_status = 'PAID')");
		$this->db->where("doc_ref",$del);
		$query = $this->db->get();
		return $query -> result_array();
	}
	public function get_del_inv_status_done($del){
		$this->db->select('*');
		$this->db->from('t_sales_invoice_header');
		$this->db->where("(financial_status = 'PARTIAL' OR financial_status = 'UNPAID')");
		$this->db->where("doc_ref",$del);
		$query = $this->db->get();
		return $query -> result_array();
	}
	
	public function insert_header($data){
		$this->db->insert('t_sales_invoice_header',$data);
		return $this->db->insert_id();
	}
	public function insert_detail($data){
		$this->db->insert('t_sales_invoice_detail',$data);	
		return $this->db->insert_id();
	}
	
	public function update_header($data,$where){
		if(count($where)<=1){
			$this->db->where($where[0]['field'],$where[0]['value']);
		}else{
			for($j = 0 ; $j< count($where) ; $j++){
				$this->db->where($where[$j]['field'],$where[$j]['value']);
			}
		}
		return $this->db->update('t_sales_invoice_header', $data);	
	}
	
	public function delete_detail($code){
		$this->db->where('doc_id', $code);
		$this->db->delete('t_sales_invoice_detail');
		return $this->db->affected_rows();
	}
	
	public function getHeader($doc){
		$query = $this -> db -> query ("
			select * from t_sales_invoice_header 
			inner join t_business on t_sales_invoice_header.buss_id = t_business.buss_id 
			where doc_num = '".$doc."'
		");
		return $query -> result_array();
	}
	public function getDetail($doc){
		$query = $this -> db -> query ("
			select *, t_sales_invoice_detail.item_name as 'item_name_so' from t_sales_invoice_detail
			inner join t_inventory on t_sales_invoice_detail.item_id = t_inventory.item_id
			where doc_id = (select doc_id from t_sales_invoice_header where doc_num = '".$doc."')
			order by doc_line
		");
		return $query -> result_array();
	}
	public function getDetail_so_open($doc){
		$query = $this -> db -> query ("
			select *, t_sales_invoice_detail.item_name as 'item_name_so' from t_sales_invoice_detail
			inner join t_inventory on t_sales_invoice_detail.item_id = t_inventory.item_id
			where doc_id = (select doc_id from t_sales_invoice_header where doc_num = '".$doc."')
			and t_sales_invoice_detail.item_qty_closed < t_sales_invoice_detail.item_qty 
			order by doc_line
		");
		return $query -> result_array();
	}
}