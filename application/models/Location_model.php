<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location_model extends MY_Model{
	
	public function __construct(){
		parent::__construct();
		log_message("debug", "Location Model class initiated");
		}
		
	public function getAll($where=NULL){
		$this->db->select('*');
		$this->db->from('t_site_location');
		if(!empty($where)){
			$this->db->where($where);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function add_new_location($data){
		$query_data = array(
			'location_code'			=> $data['code'],
			'location_name' 		=> $data['name'],
			'location_address'		=> $data['address'],
			'location_phone'		=> $data['phone'],
			'location_city'			=> $data['city'],
			'location_state'		=> $data['state'],
			'location_country'		=> $data['country'],
			'location_description'	=> $data['description'],		
			'is_active'				=> 1,
			'is_warehouse'			=> $data['warehouse'],
			'is_production'			=> $data['production'],
			'is_virtual'			=> $data['virtual']
		);
		return ($this -> db -> insert('t_site_location', $query_data)) ? "location_added" : "location_add_failed" ;
		}
	
	public function ajax_location_list($return = "datatable_array"){
		$table 		= 't_site_location';
		$primaryKey = 'location_id';
		
		$columns = array(
			array( 'db' => 'location_id'		,'dt' => 0 ),
			array( 'db' => 'location_name'		,'dt' => 1 ),
			array( 'db' => 'location_city'		,'dt' => 2 ),
			array( 'db' => 'location_phone'		,'dt' => 3 ),
			array( 'db' => 'location_address'	,'dt' => 4 )			
			);
		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);
		$this -> load -> library('ssp');
 
		return SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns);
		
		}
			
	function get_site_location($location_id = FALSE){
		if ($location_id) $this -> db -> where ('location_id',$location_id);
		$query = $this -> db -> get('t_site_location');
		return  ($location_id) ? $query -> row_array() : $query ->result_array();
	}
	
	function get_site_by_type($type='warehouse'){
		switch ($type){
			case 'warehouse':
				$this->db->where('is_warehouse',1);
				break;
			case 'virtual':
				$this->db->where('is_virtual',1);
				break;
			case 'production':
				$this->db->where('is_production',1);
				break;
			default:
				$this->db->where('is_active',1);
		}
		$query = $this->db->get('t_site_location');
		return $query->result_array();			
	}
}