<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account_model extends MY_Model{
	public $table;
	protected $CI;
	public function __construct(){
		parent::__construct();
		$this->load->model('document_model');
		$this -> CI = & get_instance();
	}
	
	public function add($data){
		$this->table = 't_account';
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}
	public function view($where=NULL,$select=NULL,$orders=NULL){
		if($select)$this->db->select($select);
		$this->db->from('t_account');
		if($where)$this->db->where($where);	
		if(!is_null($orders)){	
			if (is_array($orders)){ 
				foreach ($orders as $key => $order){
					$this->db->order_by($key,$order);
				}
			}
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function edit($data,$where){
		$this->db->update('t_account',$data,$where);
	}
	
	public function getAll($where=NULL){
		$this->db->select('*');
		$this->db->from('t_account');
		if($where!=NULL){
			foreach($where as $whr){
				$this->db->where($whr[0],$whr[1]);
			}
		}
		$this->db->order_by('account_number','asc');
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	public function getLevel($code){
		$this->db->select("account_level");
		$this->db->from('t_account');
		$this->db->where('account_number',$code);
		$query = $this->db->get();		
		return $query->result_array();
	}
	
	public function getBalance($code){
		$this->db->select("account_balance, default");
		$this->db->from("t_account");
		$this->db->where("account_number",$code);
		$query = $this->db->get();
		$temp = $query->row_array();		
		if($temp['default'] == "C"){
			$val = $temp['account_balance'] * -1;
		}else{
			$val = $temp['account_balance'];
		}
		return $val;
	}
	public function getChild($code){
		$final_result = array(); //init array		
		//get child
		$query_temp = $this -> CI -> db -> query ("select * from t_account where account_parent_number = '".$code."'");
		
		foreach($query_temp->result_array() as $row){
			array_push($final_result,$row);
			if($row['is_postable'] == 0){
				$temp_loops = $this->getChild($row['account_number']); 
				foreach($temp_loops as $loops){
					array_push($final_result,$loops);
				}
			}			
		}
		return $final_result;		
	}

	public function getTransCode(){
		$this->db->select("*");
		$this->db->from('t_account_trans');
		return $this->db->get()->result_array();
	}
	
	public function entry_header($data_header){
		// checking completeness of data
		if(!array_key_exists("journal_type",$data_header)){
			echo "Journal Type must be defined."; return false;
		}
		
		// additional journal header data
		$jrnl_code = $this->document_model->getNextSeries("JRNL"); 
		$data_header["journal_code"] 	= $jrnl_code;
		$data_header["create_by"]		= $this->session->userdata("username");
		$data_header["create_dt"]		= date('Y-m-d H:m:s');
		
		// additional header data by defult
		if(!array_key_exists("journal_date",$data_header)){
			$data_header["journal_date"] = date('Y-m-d H:m:s');
		}
		if(!array_key_exists("document_date",$data_header)){
			$data_header["document_date"] = date('Y-m-d H:m:s');
		}

		$this->document_model->useDocSeries("JRNL"); 		
		$this->db->insert('t_account_journal_header',$data_header);		
		return $this->db->insert_id();
	}
	public function entry_detail($data,$id){				
		foreach($data as $x => $datum){
			// checking completeness of data
			if(!array_key_exists('account_id',$datum)){
				echo "Journal Type must be defined."; return false;
			}
			if(!array_key_exists('journal_dr',$datum) && !array_key_exists('journal_cr',$datum)){
				echo "Either Debit / Credit must have a value."; return false;
			}
			
			// additional journal detail data
			$datum["journal_id"]	= $id;
			$datum["journal_line"]	= $x+1;
				
			$this->db->insert('t_account_journal_detail',$datum);		
			/*if($this->db->insert('t_account_journal_detail',$datum)){
				echo "ya";
			}else{
				echo "no";
			}*/
		}
	}
	
	public function getCash(){
		$this->db->select("*")->from('t_account')->where('is_cash',1)->order_by('account_number','asc');
		$query = $this->db->get();
		return $query->result_array();
	}
	public function getBank(){
		$this->db->select("*")->from('t_account')->where('is_bank',1)->order_by('account_number','asc');
		$query = $this->db->get();
		return $query->result_array();
	}
	public function getCashAndBank(){
		$this->db->select("*")->from('t_account')->where('is_cash',1)->or_where('is_bank',1)->order_by('account_number','asc');
		$query = $this->db->get();
		return $query->result_array();
	}
	public function getControlAccount(){
		$this->db->select("*");
		$this->db->select("CONCAT(account_number, ' - ', account_name) AS account_name2", FALSE); 
		$this->db->from('t_account')->where('is_control',1)->order_by('account_number','asc');
		$query = $this->db->get();
		return $query->result_array();	
	}
	
	public function getTransByAccount($code){
		$this->db->select("*")->from("t_account_journal_detail");
		$this->db->join('t_account_journal_header','t_account_journal_detail.journal_id = t_account_journal_header.journal_id','inner');
		$this->db->where('account_number',$code);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function getTransByBusiness($code){
		$this->db->select("*")->from("t_account_journal_detail");
		$this->db->join('t_account_journal_header','t_account_journal_detail.journal_id = t_account_journal_header.journal_id','inner');
		$this->db->where('journal_buss',$code);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function getTransByDate($date_from,$date_to=NULL){
		$this->db->select("*")->from("t_account_journal_detail");
		$this->db->join('t_account_journal_header','t_account_journal_detail.journal_id = t_account_journal_header.journal_id','inner');
		$this->db->where('journal_date >= $date_from');
		if($date_to != NULL){
			$this->db->where('journal_date <= $date_to');
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	/*public function countCYE(){
		$this->db->select("account_balance");
		$this->db->from('t_account');
		$this->db->where('account_number','40000');
		$query = $this->db->get();
		
		$this->db->select("account_balance");
		$this->db->from('t_account');
		$this->db->where('account_number','50000');
		$query2 = $this->db->get();
		
		return ($query->row_array()-$query2->row_array());
	}*/
	
	//ajax
	public function ajax_account_info($id){
		$this->db->where_in('account_id',$id);
		$query = $this->db->get('t_account');
		$result = $query -> result_array();
		return $result;
	}

	/**
	*
	* Balance Sheet report
	*
	**/
	public function balance_sheet_report($filters)
	{
		$this->db->select('coa.account_number, coa.account_name, coa.account_level, coa.is_postable, sum(ifnull(j.journal_dr,0)) as debit, sum(ifnull(j.journal_cr,0)) as credit', false);
		$this->db->from('t_account coa');
		$this->db->join('t_account_journal_detail j', 'j.account_id = coa.account_id', 'left');
		$this->db->join('t_account_journal_header jh', 'jh.journal_id = j.journal_id', 'left');
		if($filters['seldate'] === NULL){
			$filters['seldate'] = date('Y-m-d', strtotime("+1 day"));
		}

		$filters['seldate'] = date('Y-m-d', strtotime($filters['seldate'] . " +1 days"));

		$this->db->where('jh.posting_date IS NULL');
		$this->db->or_where('jh.posting_date <', $filters['seldate']);

		$this->db->group_by('coa.account_number, coa.account_name, coa.account_level, coa.is_postable');
		
		$this->db->order_by('coa.account_number','asc');

		return $this->db->get()->result_array();
	}

	/**
	*
	* Profit Loss report
	*
	**/
	public function profit_loss_report($filters)
	{
		$this->db->select('coa.account_number, coa.account_name, coa.account_level, coa.is_postable, sum(ifnull(j.journal_dr,0)) as debit, sum(ifnull(j.journal_cr,0)) as credit', false);
		$this->db->from('t_account coa');
		$this->db->join('t_account_journal_detail j', 'j.account_id = coa.account_id', 'left');
		$this->db->join('t_account_journal_header jh', 'jh.journal_id = j.journal_id', 'left');
		if($filters['seldate'] === NULL){
			$filters['seldate'] = date('Y-m-d', strtotime("+1 day"));
		}

		$filters['seldate'] = date('Y-m-d', strtotime($filters['seldate'] . " +1 days"));

		$this->db->where('jh.posting_date IS NULL');
		$this->db->or_where('jh.posting_date <', $filters['seldate']);

		$this->db->group_by('coa.account_number, coa.account_name, coa.account_level, coa.is_postable');
		
		$this->db->order_by('coa.account_number','asc');

		return $this->db->get()->result_array();
	}


	/**
	*
	* acc_ledger_report
	*
	**/
	public function acc_ledger_report($filters)
	{
		if($filters['date_from']){
			$this->db->where('posting_date >=', $filters['date_from']);
		}
		if($filters['date_to']){
			$this->db->where('posting_date <=', $filters['date_to']);
		}
		$this->db->from('v_journals');
		return $this->db->get()->result_array();
	}

	/**
	*
	* trial_balance_report
	*
	**/
	public function trial_balance_report($filters)
	{
		if($filters['date_from'] && $filters['date_to']){

			$this->db->where("IFNULL(jh.posting_date,'2000-01-01') >=", $filters['date_from']);
			$this->db->where("IFNULL(jh.posting_date,'2020-01-01') <=", $filters['date_to']);
		}

		$this->db->where('coa.is_postable', 1);

		$this->db->select('coa.account_number, coa.account_name, coa.account_level, coa.is_postable, sum(ifnull(j.journal_dr,0)) as debit, sum(ifnull(j.journal_cr,0)) as credit', false);
		$this->db->from('t_account coa');
		$this->db->join('t_account_journal_detail j', 'j.account_id = coa.account_id', 'left');
		$this->db->join('t_account_journal_header jh', 'jh.journal_id = j.journal_id', 'left');

		// $this->db->where('jh.posting_date IS NULL');
		

		$this->db->group_by('coa.account_number, coa.account_name, coa.account_level, coa.is_postable');
		
		$this->db->order_by('coa.account_number','asc');

		return $this->db->get()->result_array();

	}

}