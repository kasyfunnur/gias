<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class inventory_transfer_model extends MY_Model {    
	public $table;
    function __construct(){
        // Call the Model constructor
        parent::__construct();
		$this->table = '';
		$this->load->model('document_model');
		$this->load->model('inventory_model');
    }
	
	//AVER
	public function add($data){	
		$this->db->insert('t_stock_moving_header',$data['t_stock_moving_header']);
		$temp_temp_id = $this->db->insert_id();
		$temp_id = $temp_temp_id;
		$data_transaction_in = array();
		$data_transaction_out = array();
		foreach($data['t_stock_moving_detail'] as $detail){
			$detail['doc_id'] = $temp_id;
			$this->db->insert('t_stock_moving_detail',$detail);
			array_push($data_transaction_out,array(
					"doc_type"		=> 'TRF',
					"doc_num"		=> $data['t_stock_moving_header']['doc_num'],
					"doc_date"		=> $data['t_stock_moving_header']['doc_dt'],
					"item_id"		=> $detail['item_id'],
					"item_qty" 		=> -$detail['item_qty'],
					"item_value"	=> 0,
					"total_value"	=> 0,
					"whse_id"		=> $data['t_stock_moving_header']['whse_from'],
					"create_by"		=> $this->session->userdata("username")
				)
			);
			array_push($data_transaction_in,array(
					"doc_type"		=> 'TRF',
					"doc_num"		=> $data['t_stock_moving_header']['doc_num'],
					"doc_date"		=> $data['t_stock_moving_header']['doc_dt'],
					"item_id"		=> $detail['item_id'],
					"item_qty" 		=> $detail['item_qty'],
					"item_value"	=> 0,
					"total_value"	=> 0,
					"whse_id"		=> 5, //whse transit
					"create_by"		=> $this->session->userdata("username")
				)
			);
		}		
		$this->inventory_model->add_trx($data_transaction_out);
		$this->inventory_model->add_trx($data_transaction_in);
		$this->document_model->useDocSeries('ITM_TRF');
		$this->inventory_model->recalculate();
	}
	public function view($where=NULL,$order=NULL){
		$this->db->select("*");
		$this->db->from('t_stock_moving_header');
		$this->db->join('t_stock_moving_detail','t_stock_moving_detail.doc_id = t_stock_moving_header.doc_id','inner');
		if($where)$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function edit($data,$where){
		$check = $this->view($where);
		/*if($check[0]['doc_status'] != "DRAFT"){ 
			echo "Document not in edit mode.";
			return FALSE;
		}*/
		$this->db->update('t_stock_moving_header',$data['t_stock_moving_header'],$where);
		if(isset($data['t_stock_moving_detail'])){			
			foreach($data['t_stock_moving_detail'] as $detail){
				$this->db->update('t_stock_moving_detail',$detail,
					array(
						$where[0],
						'doc_line'=>$detail['doc_line']
					)
				);
			}
		}
	}
	public function remove($where){	
	}

	public function count_doc(){
		return $this->db->count_all('t_stock_moving_header');
	}
	public function change_status($status,$doc_num){
		$chg_data = array();
		$chg_data['doc_status'] = $status;
		if($status == 'CANCELLED'){
			$chg_data['cancel_by']		= $this->session->userdata("username");
			$chg_data['cancel_dt']		= date('Y-m-d H:m:s');
			$chg_data['cancel_reason']  = $this->input->post('reason');
		}else{
			$chg_data['update_by']		= $this->session->userdata("username");
			$chg_data['update_dt']		= date('Y-m-d H:m:s');
		}		
		$this->db->update('t_stock_moving_header',array('doc_status'=>$status),array('doc_num'=>$doc_num));	
	}

	//SPECIFIC
	public function receipt($data){
		$this->db->update('t_stock_moving_header',$data['t_stock_moving_header'],array('doc_id'=>$data['t_stock_moving_header']['doc_id']));
		$data_transaction_in = array();
		$data_transaction_out = array();
		foreach($data['t_stock_moving_detail'] as $detail){
			$this->db->update('t_stock_moving_detail',$detail,
				array(
					'doc_id'	=>$data['t_stock_moving_header']['doc_id'],
					'doc_line'	=>$detail['doc_line'],
					'item_id'	=>$detail['item_id']
				)
			);
			array_push($data_transaction_out,array(
					"doc_type"		=> 'TRF',
					"doc_num"		=> $data['t_stock_moving_header']['doc_num'],
					"doc_date"		=> $data['t_stock_moving_header']['receipt_dt'],
					"item_id"		=> $detail['item_id'],
					"item_qty" 		=> -$detail['item_qty_closed'],
					"item_value"	=> 0,
					"total_value"	=> 0,
					"whse_id"		=> 5, //whse transit
					"create_by"		=> $this->session->userdata("username")
				)
			);
			array_push($data_transaction_in,array(
					"doc_type"		=> 'TRF',
					"doc_num"		=> $data['t_stock_moving_header']['doc_num'],
					"doc_date"		=> $data['t_stock_moving_header']['receipt_dt'],
					"item_id"		=> $detail['item_id'],
					"item_qty" 		=> $detail['item_qty_closed'],
					"item_value"	=> 0,
					"total_value"	=> 0,
					"whse_id"		=> $data['t_stock_moving_header']['whse_to'],
					"create_by"		=> $this->session->userdata("username")
				)
			);
		}
		$this->inventory_model->add_trx($data_transaction_out);
		$this->inventory_model->add_trx($data_transaction_in);
		$this->inventory_model->recalculate();
	}

	public function getHeader($code){
		$this->db->select("t0.*, t1.location_name as `whse_from_name`, t2.location_name as `whse_to_name`");
		$this->db->from("t_stock_moving_header t0");
		$this->db->join('t_site_location t1','t1.location_id = t0.whse_from');
		$this->db->join('t_site_location t2','t2.location_id = t0.whse_to');
		$this->db->where('t0.doc_num',$code);
		return $this->db->get()->result_array();
	}
	public function getDetail($code){
		$this->db->select("*");
		$this->db->from('t_stock_moving_header');
		$this->db->join('t_stock_moving_detail','t_stock_moving_header.doc_id = t_stock_moving_detail.doc_id');
		$this->db->join('t_inventory','t_inventory.item_id = t_stock_moving_detail.item_id');
		$this->db->where('t_stock_moving_header.doc_num',$code)	;
		return $this->db->get()->result_array();
	}

	//APPROVAL
	public function approve_inventory_transfer($docnumber, $approval_status){
	}
	public function review_inventory_transfer($docnumber){
		return $this -> db -> get_where('t_stock_moving_header', array('doc_num'=>$docnumber))->result_array();
	}
	

	public function set_item_close($code){	
		$this->db->select('*');
		$this->db->from('t_purchase_order_header');
		$this->db->join('t_purchase_order_detail','t_purchase_order_header.doc_id = t_purchase_order_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		foreach($query->result_array() as $row){
			if($row['item_qty_closed'] >= $row['item_qty']){
				$data = array("line_close" => 1);
			}else{
				$data = array("line_close" => 0);
			}
			$this->db->where('doc_id',$row['doc_id']);
			$this->db->where('doc_line',$row['doc_line']);				
			$this->db->update('t_purchase_order_detail', $data);	
		}
	}	
	public function set_doc_close($code){
		$this->db->select('line_close');
		$this->db->distinct('line_close');
		$this->db->from('t_purchase_order_header');
		$this->db->join('t_purchase_order_detail','t_purchase_order_header.doc_id = t_purchase_order_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		$temp = $query->result_array();		
		$doc_status = "ACTIVE";
		$log_status = "PENDING";
		if(count($temp) > 1){
			$log_status = "PARTIAL";
		}else{
			if($temp[0]['line_close'] == 1){
				$doc_status = "CLOSED";
				$log_status = "RECEIVED";				
			}
		}
		$data = array("doc_status"=>$doc_status,"logistic_status"=>$log_status);
			$this->db->update('t_purchase_order_header', $data, array('doc_num' => $code));
	}	

}