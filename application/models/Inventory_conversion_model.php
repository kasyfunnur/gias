<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory_conversion_model extends MY_Model {    
	public $table;
    function __construct(){
        parent::__construct();
		$this->table = '';
		$this->load->model('document_model');
		$this->load->model('inventory_model');
    }
	
    public function execute_conversion($outs,$ins)
    {
    	if(empty($outs)) return false;
    	if(empty($ins)) return false;
    	$total_out = 0;
    	$total_in = 0;
    	foreach($outs as $o=>$out){
    		$total_out += $this->outgoing($out['item_id'],$out['qty']);
    	}
    	foreach ($ins as $i => $in) {
    		$total_in += $this->incoming($in['item_id'],$in['qty']);
    	}

    	if ($total_out != $total_in) return false;

    	//if success and correct.
    	return true;
    }


    public function outgoing($doc,$id,$qty)
    {
    	$cr_acct = $this->Inventory_model->link_account($id,'acc_inventory');
    	$value = $this->Inventory_model->get_value($id);
    	return $this->Inventory_model->add_transaction($doc,$id,-1*$qty,$value,$whse=null);
    }

    public function incoming($doc,$id,$qty)
    {
    	$dr_acct = $this->Inventory_model->link_account($id,'acc_inventory');
    	$value = $this->Inventory_model->get_value($id);
    	return $this->Inventory_model->add_transaction($doc,$id,$qty,$value,$whse=null);
    }
}