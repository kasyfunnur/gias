<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model{
	var $sql_details ;
		
	public function __construct(){
		parent::__construct();
		log_message("debug", "Location Model class initiated");
		
		$this->sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);
		
		}
		
		//location
	public function add_location($data){
		$query_data = array(
  		'location_name' 		=> $data['name'],
		'location_address'		=> $data['address'],
		'location_phone'		=> $data['phone'],
		'location_city'			=> $data['city'],
		'location_state'		=> $data['state'],
		'location_country'		=> $data['country'],
		'location_description'	=> $data['description'],
		
		);
		return ($this -> db -> insert('t_site_location', $query_data)) ? "location_added" : "location_add_failed" ;
		}
	
	public function ajax_location_list($return = "datatable_array"){
		$table 		= 't_site_location';
		$primaryKey = 'location_id';
		
		$columns = array(
			array( 'db' => 'location_id'		,'dt' => 0 ),
			array( 'db' => 'location_name'		,'dt' => 1 ),
			array( 'db' => 'location_city'		,'dt' => 2 ),
			array( 'db' => 'location_phone'		,'dt' => 3 ),
			array( 'db' => 'location_address'	,'dt' => 4 )			
			);
		
		$this -> load -> library('ssp');
 
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns);
		
		}
			
	function get_site_location($location_id = FALSE){
		if ($location_id) $this -> db -> where ('location_id',$location_id);
		$query = $this -> db -> get('t_site_location');
		return  ($location_id) ? $query -> row_array() : $query ->result_array();
		}
		
		//user
	public function add_user($data){
		require_once('passwordhash.php');
		defined('PHPASS_HASH_STRENGTH') or define('PHPASS_HASH_STRENGTH', 8);
		defined('PHPASS_HASH_PORTABLE') or define('PHPASS_HASH_PORTABLE', FALSE);

		
		$hasher = new passwordhash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
		$user_pass_hashed = $hasher->HashPassword($data['user_pass']);
		
		$query_data = array(
  		'user_name' 		=> $data['user_name'],
		'full_name'			=> $data['full_name'],
		'user_pass'			=> $user_pass_hashed,
		'user_email'		=> $data['user_email'],
		
		'user_location_id'	=> $data['location_id'],
		
		// 'birthdate'			=> $data['birthdate'],
		'birthdate'			=> date("Y-m-d", strtotime($data['birthdate'])),
		'gender'			=> $data['gender'],
		
		'department'		=> $data['department'],
		'designation'		=> $data['designation'],
		
		'phone_number'		=> $data['phone_number'],
		'mobile_number'		=> $data['mobile_number'],
		'extension_number'	=> $data['extension_number'],
		
		'user_date' 		=> date('Y-m-d H:i:s'),
		'user_modified' 	=> date('Y-m-d H:i:s'),
		
		'position_id'		=> $data['position']
		);
		
		$this -> db -> trans_begin();
		
		$this -> db -> insert('t_user', $query_data);
		$user_id = $this -> db -> insert_id();
		
		foreach ($data['group_id'] as $group_id){
			$this -> db -> insert ('t_user_group_junc', array('user_id' => $user_id,'group_id' => $group_id));
			}
		
		if ($this->db->trans_status() === FALSE){
    		$this->db->trans_rollback();
			return "user_add_failed";
			}
		else{
    		$this->db->trans_commit();
			return "user_added";
			}	
		}
		
	public function ajax_user_list($return = "datatable_array"){
		$table 		= 'v_user_datatable';
		$primaryKey = 'user_id';
		
		$columns = array(
			array( 'db' => 'user_name'			,'dt' => 0 ),
			array( 'db' => 'full_name'			,'dt' => 1 ),
			array( 'db' => 'position_name'		,'dt' => 2 ),
			array( 'db' => 'user_email'			,'dt' => 3 ),
			array( 'db' => 'user_last_login'	,'dt' => 4 ),
			array( 'db' => 'department'			,'dt' => 5 ),
			array( 'db' => 'designation'		,'dt' => 6 ),
			array( 'db' => 'location_name'		,'dt' => 7 )			
			);
		$this -> load -> library('ssp');
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns);
		}
	
	function get_users($user_id = FALSE){
		if ($user_id) $this -> db -> where ('t_user',$user_id);
		$query = $this -> db -> get('t_user');
		return  ($user_id) ? $query -> row_array() : $query ->result_array();
		}
	
		//user group
	public function ajax_user_group_list($return = "datatable_array"){
		$table 		= 't_user_group';
		$primaryKey = 'group_id';
		
		$columns = array(
			array( 'db' => 'group_id'			,'dt' => 0 ),
			array( 'db' => 'group_name'			,'dt' => 1 ),
			array( 'db' => 'group_desc'			,'dt' => 2 )
			);
		$this -> load -> library('ssp');
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns);
		}
	
	public function add_user_group($data){
		
		$query_data = array(
  		'group_name' 	=> $data['group_name'],
		'group_desc'	=> $data['group_desc'],
		);
		
		return ($this -> db -> insert('t_user_group', $query_data)) ? "user_group_added" : "user_group_add_failed";
		}
		
	function get_user_group($group_id = FALSE){
		if ($group_id) $this -> db -> where ('t_user_group',$group_id);
		$query = $this -> db -> get('t_user_group');
		return  ($group_id) ? $query -> row_array() : $query ->result_array();
		}		
}