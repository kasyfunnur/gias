<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tax_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get_all(){
		$this->db->select("*");
		$this->db->from("t_tax");
		return $this->db->get()->result_array();
	}
	
}