<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class purchase_invoice_model extends MY_Model {
	public $table;
    function __construct(){
        // Call the Model constructor
        parent::__construct();
		$this->table = '';
    }	
	
	//AVER
	public function add($data){
		$data['t_purchase_invoice_header']['doc_num'] = $this->document_model->getNextSeries('PUR_INV');
		// temp fixing
		$data['t_purchase_invoice_header']['whse_id'] = 1;
		$this->db->insert('t_purchase_invoice_header',$data['t_purchase_invoice_header']);
		$temp_id = $this->db->insert_id();
		$temp_ref = $this->purchase_receipt_model->view(array("doc_num"=>$data['t_purchase_invoice_header']['doc_ref']),NULL);		
		foreach($data['t_purchase_invoice_detail'] as $x=>$detail){
			$detail['doc_id'] = $temp_id;			
			$detail['reff_id'] = $temp_ref[0]['doc_id'];
			$this->db->insert('t_purchase_invoice_detail',$detail);
			$data_rcv_detail = array(
				"item_qty_closed" 	=> ($data['other'][$x]['ast_rcv_qty'] - $data['other'][$x]['ast_qty_open']) + $detail['item_qty']
			);
			$this->db->update('t_purchase_receipt_detail',$data_rcv_detail,
				array(	'doc_id'=>$temp_ref[0]['doc_id'],
						'doc_line'=>$detail['reff_line'])
			);
		}		
		$this->document_model->useDocSeries('PUR_INV');
	}
	public function view($where=NULL,$order=NULL){
		$this->db->select("*");
		$this->db->from('t_purchase_invoice_header');
		$this->db->join('t_purchase_invoice_detail','t_purchase_invoice_header.doc_id = t_purchase_invoice_detail.doc_id','inner');
		if($where)$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function edit($data,$where){
		$this->db->update('t_purchase_invoice_header',$data['t_purchase_invoice_detail'],$where);
		if(isset($data['t_purchase_invoice_detail'])){
			foreach($data['t_purchase_invoice_detail'] as $detail){
				$this->db->update('t_purchase_invoice_detail',$detail,
					array(
						$where[0],
						'doc_line'=>$detail['doc_line']
					)
				);
			}
		}
	}
	public function remove($where){

	}

	public function count_doc(){
		return $this->db->count_all('t_purchase_invoice_header');
	}



	public function getAll($filter=NULL,$page=0){	
		$this->db->select('*');
		$this->db->from('t_purchase_invoice_header');
		$this->db->join('t_business','t_purchase_invoice_header.buss_id = t_business.buss_id','inner');
		//$this->db->join('t_site_location','t_purchase_invoice_header.whse_id = t_site_location.location_id','inner');
		if($filter)$this->db->where('t_purchase_invoice_header.doc_status',$filter);
		$this->db->order_by('doc_id','desc');
		$query = $this->db->get('',10,$page);		
		return $query -> result_array();	
		$this->db->select('*');
		$this->db->from('t_purchase_invoice_header');
		$this->db->join('t_business','t_purchase_invoice_header.buss_id = t_business.buss_id','inner');
		//$this->db->join('t_site_location','t_purchase_invoice_header.whse_id = t_site_location.location_id','inner');
		if($filter)$this->db->where('t_purchase_invoice_header.doc_status',$filter);
		$this->db->order_by('doc_id','desc');
		$query = $this->db->get('',10,$page);		
		return $query -> result_array();
	}


	public function getAll_detail(){
		$query = $this -> db -> query ("
			select t_purchase_invoice_detail.*, t_asset_group.group_name from t_purchase_invoice_header 
			inner join t_purchase_invoice_detail on t_purchase_invoice_header.doc_id = t_purchase_invoice_detail.doc_id
			inner join t_business on t_purchase_invoice_header.buss_id = t_business.buss_id
			inner join t_site_location on t_site_location.location_id = t_purchase_invoice_header.whse_id
			inner join t_asset_group on t_asset_group.group_id = t_purchase_invoice_detail.item_group
			where doc_status = 'ACTIVE'
		");
		return $query -> result_array();
	}
	public function getAll_with_vendor($id){
		$this->db->select('*');
		$this->db->from('t_purchase_invoice_header');
		$this->db->join('t_business','t_purchase_invoice_header.buss_id = t_business.buss_id','inner');
		$this->db->join('t_site_location','t_site_location.location_id = t_purchase_invoice_header.whse_id','inner');
		$this->db->where('doc_status','ACTIVE');
		
		//$this->db->where_not_in('logistic_status','RECEIVED');
		if($id != NULL){
			$this->db->where('t_business.buss_id',$id);
		}
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	
	public function get_ref($code){
		$this->db->select('reff_id');
		$this->db->distinct('reff_id');
		$this->db->from('t_purchase_invoice_header');
		$this->db->join('t_purchase_invoice_detail','t_purchase_invoice_header.doc_id = t_purchase_invoice_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();
	}
	
	
	public function get_qty_close($code){
		$this->db->select('*');
		$this->db->from('t_purchase_invoice_header');
		$this->db->join('t_purchase_invoice_detail','t_purchase_invoice_header.doc_id = t_purchase_invoice_detail.doc_id','inner');
		$this->db->where('line_close',0);
		$this->db->where('item_qty_closed > 0');
		$query = $this->db->get();
		return $query -> result_array();	
	}
	public function get_close_line($code){
		$this->db->select('*');
		$this->db->from('t_purchase_invoice_header');
		$this->db->join('t_purchase_invoice_detail','t_purchase_invoice_header.doc_id = t_purchase_invoice_detail.doc_id','inner');
		$this->db->where('line_close',1);
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();		
	}
	public function get_open_line($code){
		$this->db->select('*');
		$this->db->from('t_purchase_invoice_header');
		$this->db->join('t_purchase_invoice_detail','t_purchase_invoice_header.doc_id = t_purchase_invoice_detail.doc_id','inner');
		$this->db->where('line_close',0);
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();		
	}
	
	public function get_rcv_inv_status_pending($rcv){
		$this->db->select('*');
		$this->db->from('t_purchase_invoice_header');
		$this->db->where("(financial_status = 'PARTIAL' OR financial_status = 'PAID')");
		$this->db->where("doc_ref",$rcv);
		$query = $this->db->get();
		return $query -> result_array();
	}
	public function get_rcv_inv_status_done($rcv){
		$this->db->select('*');
		$this->db->from('t_purchase_invoice_header');
		$this->db->where("(financial_status = 'PARTIAL' OR financial_status = 'UNPAID')");
		$this->db->where("doc_ref",$rcv);
		$query = $this->db->get();
		return $query -> result_array();
	}
	
	public function insert_header($data){
		$this->db->insert('t_purchase_invoice_header',$data);
		return $this->db->insert_id();
	}
	public function insert_detail($data){
		$this->db->insert('t_purchase_invoice_detail',$data);	
		return $this->db->insert_id();
	}
	
	public function update_header($data,$where){
		if(count($where)<=1){
			$this->db->where($where[0]['field'],$where[0]['value']);
		}else{
			for($j = 0 ; $j< count($where) ; $j++){
				$this->db->where($where[$j]['field'],$where[$j]['value']);
			}
		}
		return $this->db->update('t_purchase_invoice_header', $data);	
	}
	
	public function delete_detail($code){
		$this->db->where('doc_id', $code);
		$this->db->delete('t_purchase_invoice_detail');
		return $this->db->affected_rows();
	}
	
	public function getHeader($doc){
		$this->db->select("*");
		$this->db->from('t_purchase_invoice_header');
		$this->db->join('t_business','t_purchase_invoice_header.buss_id = t_business.buss_id');
		$this->db->where('doc_num',$doc);
		$query=$this->db->get();
		return $query -> result_array();
	}
	public function getDetail($doc){
		$this->db->select("*, t_purchase_invoice_detail.item_name as `item_name_po`");
		$this->db->from('t_purchase_invoice_detail');
		$this->db->join('t_purchase_invoice_header','t_purchase_invoice_header.doc_id = t_purchase_invoice_detail.doc_id');
		$this->db->join('t_inventory','t_inventory.item_id = t_purchase_invoice_detail.item_id');
		$this->db->where('t_purchase_invoice_header.doc_num',$doc);
		$this->db->order_by('t_purchase_invoice_detail.doc_line');
		$query = $this->db->get();		
		return $query -> result_array();
	}
	public function getDetail_po_open($doc){
		$query = $this -> db -> query ("
			select *, t_purchase_invoice_detail.item_name as 'item_name_po' from t_purchase_invoice_detail
			inner join t_inventory on t_purchase_invoice_detail.item_id = t_inventory.item_id
			where doc_id = (select doc_id from t_purchase_invoice_header where doc_num = '".$doc."')
			and t_purchase_invoice_detail.item_qty_closed < t_purchase_invoice_detail.item_qty 
			order by doc_line
		");
		return $query -> result_array();
	}
}