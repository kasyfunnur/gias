<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Payment_model extends MY_Model {
    var $details;
	public $table;
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->table = '';
    }	
	
	public function getAll($type){
		$query = $this -> db -> query ("
			select * from t_payment_header 
			inner join t_business on t_payment_header.buss_id = t_business.buss_id
			where doc_status = 'ACTIVE'
			and doc_type = '".$type."'
			order by doc_id desc
		");
		return $query -> result_array();
	}
	public function getAll_detail(){
		$query = $this -> db -> query ("
			select t_payment_detail.*, t_asset_group.group_name from t_payment_header 
			inner join t_payment_detail on t_payment_header.doc_id = t_payment_detail.doc_id
			inner join t_business on t_payment_header.buss_id = t_business.buss_id
			where doc_status = 'ACTIVE'
		");
		return $query -> result_array();
	}
	public function getAll_with_vendor($id){
		$this->db->select('*');
		$this->db->from('t_payment_header');
		$this->db->join('t_business','t_payment_header.buss_id = t_business.buss_id','inner');
		$this->db->join('t_site_location','t_site_location.location_id = t_payment_header.whse_id','inner');
		$this->db->where('doc_status','ACTIVE');
		$this->db->where_not_in('logistic_status','RECEIVED');
		if($id != NULL){
			$this->db->where('t_business.buss_id',$id);
		}
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	
	
	public function insert_header($data){
		$this->db->insert('t_payment_header',$data);
		return $this->db->insert_id();
	}
	public function insert_detail($data){
		$this->db->insert('t_payment_detail',$data);	
		return $this->db->insert_id();
	}
	
	public function update_header($data,$where){
		if(count($where)<=1){
			$this->db->where($where[0]['field'],$where[0]['value']);
		}else{
			for($j = 0 ; $j< count($where) ; $j++){
				$this->db->where($where[$j]['field'],$where[$j]['value']);
			}
		}
		return $this->db->update('t_payment_header', $data);	
	}
	
	public function delete_detail($code){
		$this->db->where('doc_id', $code);
		$this->db->delete('t_payment_detail');
		return $this->db->affected_rows();
	}
	
	public function getHeader($doc){
		$query = $this -> db -> query ("
			select * from t_payment_header 
			inner join t_business on t_payment_header.buss_id = t_business.buss_id 
			where doc_num = '".$doc."'
		");
		return $query -> result_array();
	}
	public function getDetail($doc){
		$query = $this -> db -> query ("
			select * from t_payment_detail
			inner join t_payment_header on t_payment_detail.doc_id = t_payment_header.doc_id
			where t_payment_header.doc_id = (select doc_id from t_payment_header where doc_num = '".$doc."')
			order by doc_line
		");
		return $query -> result_array();
	}	
}