<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Journal_template_model extends MY_Model{
	public $table;
	public function __construct(){
		parent::__construct();
		log_message("debug", "Asset helper model class initiated");
	}
	public function add($data){
		$this->db->insert('t_account_journal_template_header',$data['t_account_journal_template_header']);
		$temp_temp_id = $this->db->insert_id();
		$temp_id = $temp_temp_id;
		foreach($data['t_account_journal_template_detail'] as $detail){			
			$detail['id']=$temp_id;
			$this->db->insert('t_account_journal_template_detail',$detail);
		}		
	}
	public function view($where=NULL){
		$this->db->select('*');
		$this->db->from('t_account_journal_template_header');
		if($where) $this->db->where($where);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	public function edit($data,$where){
		$this->db->update('t_account_journal_template_header',$data['t_account_journal_template_header'],$where);
		foreach($data['t_account_journal_template_detail'] as $detail){
			$this->db->update('t_account_journal_template_detail',$detail,
				array('id'=>$data['t_account_journal_template_header']['id'],
					'line'=>$detail['line']
				)
			);
		}
	}
	public function count_doc(){		
		return $this->db->count_all('t_account_journal_template_header');
	}

	public function getAll($filter=NULL,$page=0){
		$this->db->select('*');
		$this->db->from('t_account_journal_template_header');
		$this->db->order_by('id','desc');
		$query = $this->db->get('',10,$page);
		return $query->result_array();
	}

	public function getHeader($code){
		$this->db->select('t_account_journal_template_header.*');
		$this->db->from('t_account_journal_template_header');
		$this->db->where('code',$code);
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	public function getDetail($code){
		$this->db->select('*');
		$this->db->from('t_account_journal_template_header');
		$this->db->join('t_account_journal_template_detail','t_account_journal_template_header.id = t_account_journal_template_detail.id','inner');
		$this->db->join('t_account','t_account.account_id = t_account_journal_template_detail.account_id','inner');
		$this->db->where('code',$code);
		$this->db->order_by('line','asc');
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}

	public function list_detail(){
		$this->db->select('*');
		$this->db->from('t_account_journal_template_detail');
		$this->db->join('t_account','t_account.account_id = t_account_journal_template_detail.account_id');
		return $this->db->get()->result_array();
	}

	public function insert_debet($id,$account,$amount){
		/*$temp_data['journal_id'] = $id;
		$temp_data['journal_code'] = $account;
		$temp_data['journal_']
		$this->db->insert('')*/
	}
	
}