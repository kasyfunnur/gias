<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asset_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		log_message("debug", "Asset helper model class initiated");

		}
		
	public function add_new_asset($data)
	{
		$group_id = $data['group_id'];
		$query_data = array(
  		'asset_label' 		=> (isset($data['serial']))?($data['serial']):uniqid(),
		'asset_name' 		=> (isset($data['name']))?($data['name']):'',
  		'asset_group_id' 	=> (isset($data['group_id']))?($data['group_id']):'',
		'asset_description' => (isset($data['description']))?($data['description']):'',
		'date_acquired' 	=> (isset($data['date_acquired']))?date('Y-m-d', strtotime(str_replace('/','-',$data['date_acquired'])))
		:date('Y-m-d')
		,
		'warranty_due_date'	=> (isset($data['warranty_due_date']))?date('Y-m-d', strtotime(str_replace('/','-',$data['warranty_due_date'])))
		:date('Y-m-d')
		,
		'asset_status' 		=> (isset($data['status']))?($data['status']):'2',
		'asset_currency' 	=> (isset($data['asset_curr']))?($data['asset_curr']):'',
		'asset_location_id' => (isset($data['location_id']))?($data['location_id']):'',
		'asset_note'		=> (isset($data['note']))?($data['note']):'',
		'asset_cost'		=> (isset($data['value']))?($data['value']):'0',
		'asset_value'		=> (isset($data['value']))?($data['value']):'0',
		'asset_lifetime'	=> (isset($data['lifetime']))?($data['lifetime']):'0',
		'asset_salvage_value'=>(isset($data['salvage_value']))?($data['salvage_value']):'0',
		'custom_field_1'	=> (isset($data['custom_field'][$group_id][1]))?$data['custom_field'][$group_id][1]:NULL,
		'custom_field_2'	=> (isset($data['custom_field'][$group_id][2]))?$data['custom_field'][$group_id][2]:NULL,
		'custom_field_3'	=> (isset($data['custom_field'][$group_id][3]))?$data['custom_field'][$group_id][3]:NULL,
		'custom_field_4'	=> (isset($data['custom_field'][$group_id][4]))?$data['custom_field'][$group_id][4]:NULL,
		'custom_field_5'	=> (isset($data['custom_field'][$group_id][5]))?$data['custom_field'][$group_id][5]:NULL,
		'custom_field_6'	=> (isset($data['custom_field'][$group_id][6]))?$data['custom_field'][$group_id][6]:NULL,
		'custom_field_7'	=> (isset($data['custom_field'][$group_id][7]))?$data['custom_field'][$group_id][7]:NULL,
		'custom_field_8'	=> (isset($data['custom_field'][$group_id][8]))?$data['custom_field'][$group_id][8]:NULL,
		'custom_field_9'	=> (isset($data['custom_field'][$group_id][9]))?$data['custom_field'][$group_id][9]:NULL,
		'custom_field_10'	=> (isset($data['custom_field'][$group_id][10]))?$data['custom_field'][$group_id][10]:NULL
		);
		return ($this -> db -> insert('t_asset', $query_data)) ? "asset_added" : "asset_add_failed" ;
		}
	
	public function new_maintenance_type($data)
	{
		$query_data = array(
  		'maint_type_code' 			=> (isset($data['maint_type_code']))		?($data['maint_type_code'])			:'',
		'maint_type_name' 			=> (isset($data['maint_type_name']))		?($data['maint_type_name'])			:'',
  		'maint_type_desc' 			=> (isset($data['maint_type_desc']))	    ?($data['maint_type_desc'])			:'',
		'maint_type_colour_code' 	=> (isset($data['maint_type_colour_code'])) ?($data['maint_type_colour_code'])	:'',
		);
		return ($this -> db -> insert('t_maint_type', $query_data)) ? "maint_type_added" : "maint_type_failed" ;
		}	
	
	public function add_new_group($data){
		$query_data = array(
			'group_name' 		=> $data['name'],
			'group_description'	=> $data['description'],
			'group_tag'			=> $data['tag'],
			'custom_field_1'	=> (isset($data['custom_field'][1]))?$data['custom_field'][1]:NULL,
			'custom_field_2'	=> (isset($data['custom_field'][2]))?$data['custom_field'][2]:NULL,
			'custom_field_3'	=> (isset($data['custom_field'][3]))?$data['custom_field'][3]:NULL,
			'custom_field_4'	=> (isset($data['custom_field'][4]))?$data['custom_field'][4]:NULL,
			'custom_field_5'	=> (isset($data['custom_field'][5]))?$data['custom_field'][5]:NULL,
			'custom_field_6'	=> (isset($data['custom_field'][6]))?$data['custom_field'][6]:NULL,
			'custom_field_7'	=> (isset($data['custom_field'][7]))?$data['custom_field'][7]:NULL,
			'custom_field_8'	=> (isset($data['custom_field'][8]))?$data['custom_field'][8]:NULL,
			'custom_field_9'	=> (isset($data['custom_field'][9]))?$data['custom_field'][9]:NULL,
			'custom_field_10'	=> (isset($data['custom_field'][10]))?$data['custom_field'][10]:NULL,
			'custom_type_1'		=> (isset($data['custom_type'][1]))?$data['custom_type'][1]:NULL,
			'custom_type_2'		=> (isset($data['custom_type'][2]))?$data['custom_type'][2]:NULL,
			'custom_type_3'		=> (isset($data['custom_type'][3]))?$data['custom_type'][3]:NULL,
			'custom_type_4'		=> (isset($data['custom_type'][4]))?$data['custom_type'][4]:NULL,
			'custom_type_5'		=> (isset($data['custom_type'][5]))?$data['custom_type'][5]:NULL,
			'custom_type_6'		=> (isset($data['custom_type'][6]))?$data['custom_type'][6]:NULL,
			'custom_type_7'		=> (isset($data['custom_type'][7]))?$data['custom_type'][7]:NULL,
			'custom_type_8'		=> (isset($data['custom_type'][8]))?$data['custom_type'][8]:NULL,
			'custom_type_9'		=> (isset($data['custom_type'][9]))?$data['custom_type'][9]:NULL,
			'custom_type_10'	=> (isset($data['custom_type'][10]))?$data['custom_type'][10]:NULL,
			// link account
			'acc_id_cost'		=> (isset($data['link_acc_cost']))?($data['link_acc_cost']):NULL,
			'acc_id_accdepre'	=> (isset($data['link_acc_accdepre']))?($data['link_acc_accdepre']):NULL,
			'acc_id_depre'		=> (isset($data['link_acc_depre']))?($data['link_acc_depre']):NULL
		);
		return ($this -> db -> insert('t_asset_group', $query_data)) ? "asset_group_added" : "asset_group_add_failed" ;
	}
	
	public function edit_asset_group($data){
		$query_data = array(
			'group_name'		=> $data['name'],
			'group_description'	=> $data['description'],
			'group_tag'			=> $data['group_tag'],
			'custom_field_1'	=> (isset($data['custom_field'][1]))?$data['custom_field'][1]:NULL,
			'custom_field_2'	=> (isset($data['custom_field'][2]))?$data['custom_field'][2]:NULL,
			'custom_field_3'	=> (isset($data['custom_field'][3]))?$data['custom_field'][3]:NULL,
			'custom_field_4'	=> (isset($data['custom_field'][4]))?$data['custom_field'][4]:NULL,
			'custom_field_5'	=> (isset($data['custom_field'][5]))?$data['custom_field'][5]:NULL,
			'custom_field_6'	=> (isset($data['custom_field'][6]))?$data['custom_field'][6]:NULL,
			'custom_field_7'	=> (isset($data['custom_field'][7]))?$data['custom_field'][7]:NULL,
			'custom_field_8'	=> (isset($data['custom_field'][8]))?$data['custom_field'][8]:NULL,
			'custom_field_9'	=> (isset($data['custom_field'][9]))?$data['custom_field'][9]:NULL,
			'custom_field_10'	=> (isset($data['custom_field'][10]))?$data['custom_field'][10]:NULL,
			'custom_type_1'		=> (isset($data['custom_field_type'][1]))?$data['custom_field_type'][1]:NULL,
			'custom_type_2'		=> (isset($data['custom_field_type'][2]))?$data['custom_field_type'][2]:NULL,
			'custom_type_3'		=> (isset($data['custom_field_type'][3]))?$data['custom_field_type'][3]:NULL,
			'custom_type_4'		=> (isset($data['custom_field_type'][4]))?$data['custom_field_type'][4]:NULL,
			'custom_type_5'		=> (isset($data['custom_field_type'][5]))?$data['custom_field_type'][5]:NULL,
			'custom_type_6'		=> (isset($data['custom_field_type'][6]))?$data['custom_field_type'][6]:NULL,
			'custom_type_7'		=> (isset($data['custom_field_type'][7]))?$data['custom_field_type'][7]:NULL,
			'custom_type_8'		=> (isset($data['custom_field_type'][8]))?$data['custom_field_type'][8]:NULL,
			'custom_type_9'		=> (isset($data['custom_field_type'][9]))?$data['custom_field_type'][9]:NULL,
			'custom_type_10'	=> (isset($data['custom_field_type'][10]))?$data['custom_field_type'][10]:NULL,
			// link account
			'acc_id_cost'		=> (isset($data['link_acc_cost']))?($data['link_acc_cost']):NULL,
			'acc_id_accdepre'	=> (isset($data['link_acc_accdepre']))?($data['link_acc_accdepre']):NULL,
			'acc_id_depre'		=> (isset($data['link_acc_depre']))?($data['link_acc_depre']):NULL
		);
		$this->db->where('group_id',$data['group_id']);
		return ($this->db->update('t_asset_group',$query_data)) ? "asset_group_updated" : "asset_group_updated_failed";
	}
	
	public function add_usage_request($data){
		$document_number = get_ref_number('asset_request');
		
		$this -> load-> helper ('approval');
		$query_data = array(
  		'document_ref' 		=> $document_number,
		'location_id'		=> 1, //$this -> session -> userdata ('user_location_id'), // IND, hardcode for now
		'requested_by'		=> $this->ion_auth->user()->row()->id, //$this -> session -> userdata ('user_id'),
		'requested_for'		=> $data['for'],
		'date_from'			=> date('Y-m-d', strtotime(str_replace('/','-',$data['date_from']))),
		'date_to'			=> date('Y-m-d', strtotime(str_replace('/','-',$data['date_to']))),
		'note'				=> $data['note'],
		'request_status'	=> 1 
		);
		
		if(!$data['asset_requested'])return "asset_request_failed";
		
		$asset_requested = array_filter(array_map('trim',explode(";",$data['asset_requested'])));
		
		if(!$asset_requested)return "asset_request_failed";
		
		$this -> db -> trans_start();
		$this -> db -> insert('t_asset_request_header', $query_data);
		$insert_id = $this -> db -> insert_id();
		foreach ($asset_requested as $asset)
		{
			$asset_data = array('request_id' => $insert_id, 'asset_id' =>$this -> get_asset_id($asset));
			$this -> db -> insert ('t_asset_request_detail',$asset_data);
			}
		
		$approval_status = request_approval($this->session->userdata('user_id'), 'AUR', $document_number);	
		$this -> db -> update ('t_asset_request_header', array("request_status" => $approval_status), array ('document_ref' => $document_number));
		
		$this -> db -> trans_complete();
		
		if ($this->db->trans_status() === FALSE)return "asset_request_failed";
		else return "asset_request_added";
	}
		
	public function ajax_datatable($view = 'asset_list' , $action = FALSE){
  		$this -> load -> library('ssp');
  		$sql_details  = array(
   			'user' => $this->db->username,
   			'pass' => $this->db->password,
   			'db'   => $this->db->database,
   			'host' => $this->db->hostname
  		);
  		$join_query  = '';
  		$where_query = '';
  		$or_query    = '';
  
  		if($view == 'asset_list'){
   			$table   = 'v_asset_datatable';
			$where_query = (isset($_GET['status'])&&$_GET['status']!='all') ? "`asset_status` = '".$_GET['status']."'" : "";
			if((isset($_GET['location'])&&$_GET['location']!='all')){
				$where_query .= (($where_query == '') ? '' : 'AND')."`asset_location_id` = ".$_GET['location'];
				}
   			$primaryKey = 'asset_id';
   			$columns = array(
    		array( 'db' => 'asset_label' ,'dt' => 0,'formatter' => function ($d, $row){return "<a href='./asset_view/$d'>$d</a>";}),
			array( 'db' => 'asset_name'  	,'dt' => 1 ),
			array( 'db' => 'group_name'  	,'dt' => 2 ),
			array( 'db' => 'location_name' 	,'dt' => 3 ),
			array( 'db' => 'asset_cost'  	,'dt' => 4 ),
			array( 'db' => 'asset_status' 	,'dt' => 5 ,'formatter' => function ($d,$row){return $this -> sky -> label_box(strtoupper($d));}),
    		array( 'db' => 'asset_id'       ,'dt' => 6 ,'formatter' => function ($d, $row){
     			return "<div style='text-align:center'>
				<div class='btn-group'><a href='./asset_edit/$d' class='btn btn-flat btn-xs btn-info disabled'>Edit</a>
     			<a href='./asset_delete/$d' class='btn btn-flat btn-danger btn-xs disabled'>Delete</a></div></div>";
     			})
    		);
   		}
		else if($view == 'asset_group'){
			$table  		= 't_asset_group';
			$where_query = (isset($_GET['parent'])) ? "`group_parent_id` =".$_GET['parent'] : "`group_parent_id` = 0";
			$primaryKey 	= 'group_id';
			$columns 	= array(
				array( 'db' => 'group_tag'   		,'dt' => 0 ),
				array( 'db' => 'group_name'   		,'dt' => 1 ),
				array( 'db' => 'group_description' 	,'dt' => 2 ),
				array( 'db' => 'group_id'   		,'dt' => 3,'formatter' => function ($d, $row){ 
				
					return "<div style='text-align:center'>
						<div class='btn-group'><a href='./asset_group_edit/$d' class='btn btn-flat btn-xs btn-info'>Edit</a>
					<a href='./asset_group_delete/$d' class='btn btn-flat btn-danger btn-xs'>Delete</a></div></div>";
				}),
			);
		} 
  		else if($view == 'usage_request'){
			$table    			= 'v_asset_datatable';
			$primaryKey  		= 'asset_id';
			$user_location_id 	= 1; //$this -> session -> userdata ('user_location_id'); // hardcode for now
			$where_query 		= "`location_id` =".$user_location_id;
			$columns 			= array(
				array( 'db' => 'asset_label' 	,'dt' => 0 ,   'formatter' => function ($d, $row){return "<a href='./asset_view/$d'>$d</a>";}),
				array( 'db' => 'asset_name'  	,'dt' => 1 ),
				array( 'db' => 'asset_status' 	,'dt' => 2 ,   'formatter' => function ($d,$row) {return $this -> sky -> label_box(strtoupper($d));}),
				array( 'db' => 'asset_label' 	,'dt' => 3,    'formatter' => function ($d, $row){return "<div style='text-align:center'><a href='javascript:void(0)'><button value='$d' onClick='add_asset(this.value)' class='btn btn-flat btn-info btn-xs'>Add</button></a></div>";}));
   		}
   
   
  else if ($view == 'usage_list'){
   $table    		= 'v_asset_request';
   $primaryKey  	= 'request_id';
   $user_id  		= $this -> session -> userdata ('user_id');
   $where_query 	= (isset($_GET['show']) && $_GET['show']==1) ? "`requested_by` =".$user_id : ''; 
   $columns 		= array(
    array('db' => 'document_ref' 	,'dt' => 0),
    array('db' => 'user_name'  		,'dt' => 1),
    array('db' => 'date_from'  		,'dt' => 2),
    array('db' => 'asset_name'  	,'dt' => 3),
    array('db' => 'asset_label'  	,'dt' => 4),
    array('db' => 'note'   			,'dt' => 5),
    			array('db' => 'request_status' 	,'dt' => 6,'formatter' => function ($d, $row){return $this -> sky -> label_box(strtoupper($d));})
			);
   }
   
  else if ($view == 'move_request' || $view == 'disposal_request' || $view == 'depreciation'){
   $table  			= 'v_asset_datatable';
   $primaryKey 		= 'asset_id';
   $where_query 	= (isset($_GET['location_id'])&&$_GET["location_id"]!=0)?"`asset_location_id` =".$_GET["location_id"]." AND `asset_status` = 'active' " : " `asset_status` = 'active'";
   
   if (isset($_GET['assets_checked']) && $_GET['assets_checked']!=''){
    $assets_checked = explode(',',$_GET['assets_checked']);
    foreach ($assets_checked as $asset_id){
     if($asset_id === reset($assets_checked))$or_query = " `asset_id` = '$asset_id' ";
     else $or_query .= " OR `asset_id` = '$asset_id' ";
     }
    }
   $columns = array(
    array( 'db' => 'asset_label' 	,'dt' => 1, 'formatter' => function ($d, $row){return "<a href='./asset_view/$d'>$d</a>";}),
    array( 'db' => 'asset_name'  	,'dt' => 2 ),
    array( 'db' => 'group_name'  	,'dt' => 3 ),
    array( 'db' => 'location_name' 	,'dt' => 4 ),
    array( 'db' => 'asset_status' 	,'dt' => 5 ,'formatter' => function ($d, $row){return $this -> sky -> label_box(strtoupper($d));}),
    array( 'db' => 'asset_id'  		,'dt' => 0 ,
     'formatter' => function ($d, $row){
     $assets_checked = isset($_GET['assets_checked']) ? explode(',',$_GET['assets_checked']): NULL;
     $checked = (($assets_checked) && in_array($d,$assets_checked))?'checked':'';
     return "<input type='checkbox' class='chk' name='asset_requested[]' value='$d' onChange='javascript:populate()' $checked>";
		   })
    );
   }
   
  else if    ($view == 'non_financial_trans' || $view == 'financial_trans'){
   $table  			= 'v_asset_trans_datatable';
   $primaryKey 		= 'request_id';
   $columns 		= array(
    array( 'db' => 'document_ref'  ,'dt' => 0,
     'formatter' => function ($d, $row){  
     if($row['request_status']=='approved'){
      if($row['transaction_type']=='disposal')return "<a href='./disposal_receipt/$d'>$d</a>";
      elseif($row['transaction_type']=='move') return "<a href='./move_receipt/$d'>$d</a>";
      else return "$d";
      }
     else return "$d";
     }
     ),
    array( 'db' => 'user_name'  ,'dt' => 1 ),
    array( 'db' => 'transaction_type' ,'dt' => 2 ),
    array( 'db' => 'transaction_date' ,'dt' => 3 ),
    array( 'db' => 'location_name'  ,'dt' => 4 ),
    array( 'db' => 'new_location_name'  ,'dt' => 5 ),
			array( 'db' => 'request_status'  ,'dt' => 6, 'formatter' => function ($d, $row){return $this -> sky -> label_box(strtoupper($d));}));
   }
		   
  		
		else if ($view == 'maintenance_type'){
   			$table   = 't_maint_type';
   			$primaryKey = 'maint_type_id';
   			$columns = array(
			array( 'db' => 'maint_type_code'  			,'dt' => 0 ),
			array( 'db' => 'maint_type_name'  			,'dt' => 1 ),
			array( 'db' => 'maint_type_desc' 			,'dt' => 2 ),
			array( 'db' => 'maint_type_colour_code'  	,'dt' => 3 ),
    		array( 'db' => 'maint_type_id'       		,'dt' => 4 ,'formatter' => function ($d, $row){
     			return "<div style='text-align:center'>
				<div class='btn-group'><a href='./maintenance_type_edit/$d' class='btn btn-flat btn-xs btn-info disabled'>Edit</a>
     			<a href='./maintenance_type_edit/$d' class='btn btn-flat btn-danger btn-xs disabled'>Delete</a></div></div>";
     			})
    		);
   		}
		
		else if ($view == 'maintenance_schedule_view'){ //if required to show more detailic asset schedule
			$table  			= 't_maint_schedule';
		 	$primaryKey 		= 'schedule_id';
			$filters			= $this -> input -> get_post(NULL, TRUE);
			if (array_key_exists('schedule_id',$filters)){
				$where_query .= "a.schedule_id = {$filters['schedule_id']}";
			}
		 	$columns 		= array(
		  		//array( 'db' => 'a.schedule_code'  	,'dt' => 0	,'field' => 'schedule_code'),
		  		array( 'db' => 'c.asset_label'  	,'dt' => 0  ,'field' => 'asset_label'),
		  		array( 'db' => 'c.asset_name' 		,'dt' => 1  ,'field' => 'asset_name'),
				array( 'db' => 'f.location_name'	,'dt' => 2  ,'field' => 'asset_name'),
				array( 'db' => 'd.maint_type_name' 	,'dt' => 3  ,'field' => 'maint_type_name'),
		  		array( 'db' => 'b.scheduled_date' 	,'dt' => 4  ,'field' => 'scheduled_date'
				,'formatter' => function ($d){ return date('d-M-Y',strtotime($d)); }),
		  		array( 'db' => 'e.user_name'  		,'dt' => 5  ,'field' => 'user_name'),
		  		array( 'db' => 'b.maint_note'  		,'dt' => 6  ,'field' => 'maint_note'),
		  		array( 'db' => 'b.maint_status'  	,'dt' => 7  ,'field' => 'maint_status'
				,'formatter' => function ($d){return $this -> sky -> label_box(strtoupper($d));})
		  );
		 
		 $join_query = "FROM `t_maint_schedule` `a` 
			LEFT JOIN `t_maint_schedule_detail` `b` 	ON (`a`.`schedule_id` 	= `b`.`schedule_id`) 
			LEFT JOIN `t_asset` `c` 					ON (`c`.`asset_id` 		= `b`.`asset_id`) 
			LEFT JOIN `t_maint_type` `d`				ON (`d`.`maint_type_id` = `b`.`maint_type_id`) 
			LEFT JOIN `t_user` `e` 						ON (`e`.`user_id` 		= `a`.`schedule_pic`)
			LEFT JOIN `t_site_location` `f` 			ON (`f`.`location_id` 	= `c`.`asset_location_id`)
			";
		 
		 }
		 
		else if ($view == 'maintenance_schedule'){ 
			$table  			= 't_maint_schedule';
		 	$primaryKey 		= 'schedule_id';
		 	$columns 		= array(
		  		array( 'db' => 'a.schedule_code'  	,'dt' => 0	,'field' => 'schedule_code',
				'formatter' => function ($d, $row){return "<a href='./maintenance_schedule_view/$row[schedule_id]'>$d</a>";}),
		  		array( 'db' => 'b.user_name'  		,'dt' => 1  ,'field' => 'user_name'),
				array( 'db' => 'a.schedule_id'  	,'dt' => 2  ,'field' => 'schedule_id','formatter' => function ($d, $row){
     				return "<div style='text-align:center'>
					<div class='btn-group'><a href='./maintenance_schedule_edit/$d' class='btn btn-flat btn-xs btn-info disabled'>Edit</a>
     				<a href='./maintenance_schedule_delete/$d' class='btn btn-flat btn-danger btn-xs disabled'>Delete</a></div></div>";
     			})
    		);
		 $join_query = "FROM `t_maint_schedule` `a` LEFT JOIN `t_user` `b` ON (`b`.`user_id`= `a`.`schedule_pic`)";
		 }
		  
		else if ($view == 'new_maintenance_schedule' || $view == 'new_maintenance_order'){
   			$table   = 'v_asset_datatable';
   			$primaryKey = 'asset_id';
			$where_query = "asset_status = 'active'";
   			$columns = array(
    		array( 'db' => 'asset_label' ,'dt' => 0,'formatter' => function ($d, $row){return "<a href='./asset_view/$d'>$d</a>";}),
			array( 'db' => 'asset_name'  	,'dt' => 1 ),
			array( 'db' => 'location_name' 	,'dt' => 2 ),
			array( 'db' => 'asset_status' 	,'dt' => 3 ,'formatter' => function ($d,$row){return $this -> sky -> label_box(strtoupper($d));}),
    		array( 'db' => 'asset_id'       ,'dt' => 4 ,'formatter' => function ($d, $row){
     			return "<div style='text-align:center'>
				<button onClick='addAsset($d,\"$row[asset_label]\");return false;' class='btn btn-flat btn-xs btn-info add-button'>Add</a>
     			</div>";
     			})
    		);
   		}
		
		else if ($view == 'maintenance_order'){
			$table  			= 't_maint_schedule';
		 	$primaryKey 		= 'schedule_id';
		 	$columns 		= array(
		  		array( 'db' => 'a.maint_order_date'  	,'dt' => 0	,'field' => 'maint_order_date'),
		  		array( 'db' => 'e.user_name'  	  		,'dt' => 1  ,'field' => 'user_name'),
		  		array( 'db' => 'd.schedule_code' 		,'dt' => 2  ,'field' => 'schedule_code',
				'formatter' => function ($d){return  $d ? $d  : "Manual";}
				),
				array( 'db' => 'c.buss_name' 			,'dt' => 3  ,'field' => 'buss_name' 
				,'formatter' => function ($d){return  $d ? $d  : "Internal";}
				),
		  		array( 'db' => 'a.maint_order_cost' 	,'dt' => 4  ,'field' => 'maint_order_cost')
		  	
		  );
		 
		 $join_query = "FROM `t_maint_order` `a` 
			LEFT JOIN `t_maint_order_detail` `b` 	ON (`a`.`maint_order_id` 	= `b`.`maint_order_id`) 
			LEFT JOIN `t_business` `c` 				ON (`c`.`buss_id` 			= `a`.`maint_order_vendor_id`) 
			LEFT JOIN `t_maint_schedule` `d`		ON (`d`.`schedule_id` 		= `a`.`maint_order_schedule_id`) 
			LEFT JOIN `t_user` `e`					ON (`a`.`maint_order_pic` 	= `e`.`user_id`) 
			";
		 
		 }
		 
		 else if ($view == 'maintenance_reminder'){
			$table  			= 't_maint_schedule';
		 	$primaryKey 		= 'schedule_id';
			$filters			= $this -> input -> get_post(NULL, TRUE);
			switch ($filters['maint_group']){
				case 'overdue':
					$where_query = 'b.scheduled_date < CURDATE()';
					break;
				case 'this_month':
					$where_query = 'MONTH(b.scheduled_date) = MONTH(CURDATE())';
					break;
				case 'today':
					$where_query = 'b.scheduled_date = CURDATE()';
					break;
				case '7days':
					$where_query = 'b.scheduled_date >= curdate() AND b.scheduled_date <= date_add(curdate(), INTERVAL 7 DAY)';
					break;
				case '14days':
					$where_query = 'b.scheduled_date >= curdate() AND b.scheduled_date <= date_add(curdate(), INTERVAL 14 DAY)';
					break;
				case '30days':
					$where_query = 'b.scheduled_date >= curdate() AND b.scheduled_date <= date_add(curdate(), INTERVAL 30 DAY)';
					break;
				default: // all schedule
					$where_query = '';
					break;	
			}
			
			if (array_key_exists('pic_id',$filters)){
				$where_query .= " AND e.user_id = {$filters['pic_id']}";
			}
			
		 	$columns 		= array(
		  		array( 'db' => 'a.schedule_code'  	,'dt' => 0	,'field' => 'schedule_code'),
				array( 'db' => 'd.maint_type_name' 	,'dt' => 1  ,'field' => 'maint_type_name'),
		  		array( 'db' => 'c.asset_label'  	,'dt' => 2  ,'field' => 'asset_label'),
		  		array( 'db' => 'c.asset_name' 		,'dt' => 3  ,'field' => 'asset_name'),
		  		array( 'db' => 'b.scheduled_date' 	,'dt' => 4  ,'field' => 'scheduled_date'),
		  		array( 'db' => 'e.user_name'  		,'dt' => 5  ,'field' => 'user_name'),
		  		array( 'db' => 'b.maint_note'  		,'dt' => 6  ,'field' => 'maint_note'),
		  		array( 'db' => 'b.maint_status'  	,'dt' => 7  ,'field' => 'maint_status'  
				,'formatter' => function ($d){return $this -> sky -> label_box(strtoupper($d));})
		  );
		 
		 $join_query = "FROM `t_maint_schedule` `a` 
			LEFT JOIN `t_maint_schedule_detail` `b` 	ON (`a`.`schedule_id` 	= `b`.`schedule_id`) 
			LEFT JOIN `t_asset` `c` 					ON (`c`.`asset_id` 		= `b`.`asset_id`) 
			LEFT JOIN `t_maint_type` `d`				ON (`d`.`maint_type_id` = `b`.`maint_type_id`) 
			LEFT JOIN `t_user` `e` 						ON (`e`.`user_id` 		= `a`.`schedule_pic`)
			";
		 
		 }
		 
  return SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $join_query, $where_query, $or_query);
  }
	
	public function get_asset_group($group_id = FALSE, $parent_id = FALSE){
		if ($group_id)  $this -> db -> where ('group_id',$group_id);
		if ($parent_id) $this -> db -> where ('group_id',$parent_id);
		$query = $this -> db -> get('t_asset_group');
		if (!$query -> num_rows()) return FALSE;
		return  ($group_id) ? $query -> row_array() : $query ->result_array();
	}
	

	public function ajax_get_sub_group($group_id){
		$this -> db -> select('group_id,group_name');
		$this -> db -> where ('group_parent_id',$group_id);		
		$query = $this -> db -> get('t_asset_group');
		return  $query ->result_array();
	}
	
	public function ajax_get_parent_group($group_id){
		$this -> db -> select('group_parent_id');
		$this -> db -> where ('group_id',$group_id);		
		$query = $this -> db -> get('t_asset_group');
		return  $query ->result_array();
	}
	
	public function get_asset_tag($group_id,$return="tag"){
		$query = $this -> db -> query ("select sg.group_id, sg.group_parent_id, sg.group_name, sg.group_description,concat(ifnull(g.group_tag,''),sg.group_tag) as Tag from t_asset_group sg left join t_asset_group g on g.group_id = sg.group_parent_id where sg.group_id = '".$group_id."'");
		if (!$query -> num_rows()) return FALSE;
		
		$result = $query -> row_array();
		$tag    = $result["Tag"];
		
			if		($return == "tag") 			return $tag;		 		//return only the tag
			else if ($return == "row") 			return $result;			 	//return corresponding tag row
			else if ($return == "next_label"){ 								//return the next empty label available
			
				$this -> db -> select ('asset_label');
				$this -> db -> like   ('asset_label', $tag, 'after'); 
				$query  = $this  -> db -> get    ('t_asset');
				$result = $query -> result_array();
				
				if (!$query -> num_rows())return $tag.sprintf("%04d",1);
				
				for ($i = 1 ; $i <= 9999 ; $i++){
					$label = $tag.sprintf("%04d",$i);
					if(!in_array($label,$result))return $label;
					}
				}
		return FALSE;
		}
		
	public function get_next_asset_group_tag($parent_id){
		$this -> db -> select ('group_tag');
		$query = $this -> db -> get_where ("t_asset_group", array('group_parent_id' => $parent_id));
		if (!$query -> num_rows()) return FALSE;
		
		$result = $query -> result_array();	
		$rows   = array();
		foreach ($result as $row) $rows[] = $row['group_tag'];						
		
		if($parent_id != 0){
			for ($i = 1 ; $i <= 99 ; $i++){
				$label = sprintf("%02d",$i);
				if(!in_array($label,$rows))return $label;
				}
			}
		else if($parent_id == 0){
			for ($i = 'A' ; $i <= 'Z' ; $i++){
				if(!in_array($i,$rows))return $i;
				}
			}	
		return FALSE;
		}	
	
	public function get_asset_by_parent($parent_id){
		$this->db->where('group_parent_id',$parent_id);
		$query = $this->db->get('t_asset_group');		
		return $query->result_array();
		}
	
	public function get_site_location($location_id = FALSE){
		if ($location_id) $this -> db -> where ('location_id',$location_id);
		$query = $this -> db -> get('t_site_location');
		if (!$query -> num_rows()) return FALSE;
		return  ($location_id) ? $query -> row_array() : $query ->result_array();
		}

	public function get_asset_id($asset_label){
		$this   -> db -> select ('asset_id');
		$query  = $this -> db -> get_where ('t_asset',array('asset_label' => $asset_label));
		if (!$query -> num_rows()) return FALSE;
		$result = $query -> row_array();
		return $result['asset_id'];
		}

	public function get_asset_data($asset_id){
		$query  = $this -> db -> get_where ('t_asset',array('asset_id' => $asset_id));
		$result = $query -> row_array();
		return $result;
		}

	public function get_asset_history($asset_id){
		$query  = $this -> db -> order_by ('transaction_date','DESC') -> get_where ('v_asset_history',array('asset_id' => $asset_id));
		$result = $query -> result_array();
		return $result;
		}

	public function get_trans_doc($doc_ref){
		$query  = $this -> db -> get_where ('v_asset_trans',array('document_ref' => $doc_ref));
		$result = $query -> result_array();
		return $result;
		}
	
	public function approve_usage_request($docnumber, $approval_status){	
		$this -> db -> trans_start();
		
		$this -> db -> update ('t_asset_request_header', array("request_status" => $approval_status), array ('document_ref' => $docnumber));
		$msg = '';
		if($approval_status == '3' || $approval_status == '2'){
			// TODO: update the t_asset_request_header request status to approved (3)
			$msg = 'Successfully approved Usage Request.';
		}
		
		if($approval_status == '5'){
			// TODO: update the t_asset_request_header request status to approved (3)
			$msg = 'Successfully rejected Usage Request.';
		}
		
		$this-> db -> trans_complete();
		if ($this->db->trans_status() === FALSE){
			return array('success'=>0,'msg'=>'Failed to approve the document ');
		} else {
			return array('success'=>1,'msg'=>$msg);
		}
	}
	
	public function review_usage_request($docnumber){
		return $this -> db -> get_where('v_asset_request', array('document_ref'=>$docnumber))->result_array();
	}
	
	public function move_asset($data){
		if(!isset($data['assets_received']))return "no_asset_selected";
		
		$this -> db -> trans_begin();
		foreach ($data['assets_received'] as $asset_id){
			$update_data = array(
  				"status" 			=> 'received',
				'transaction_date'	=> date('Y-m-d', strtotime(str_replace('/','-',$data['date'])))
				);
			$where_data = array(
  				'asset_id' 			=> $asset_id,
				'request_id'		=> $data['request_id']
				);
			$this -> db -> update ('t_asset_trans_detail', $update_data, $where_data);	
			
			$this -> db -> update ('t_asset', array('asset_status'=>'active','asset_location_id'=>$data['new_location_id']), array('asset_id' => $asset_id));
		}
				
		if ($this->db->trans_status() === FALSE){
    		$this->db->trans_rollback();
			return "asset_move_failed";
			}
		else{
    		$this->db->trans_commit();
			return "asset_moved";
			}
	}
	
	public function add_move_request($data){
		if(!isset($data['asset_requested']))return "no_asset_selected";
		$document_number = get_ref_number('move_request');
		
		$this -> load-> helper ('approval');
		
		$query_data = array(
  		'document_ref' 		=> $document_number,
		'location_id'		=> $data['location_id'],
		'requested_by'		=> $this -> session -> userdata ('user_id'),
		'transaction_date'	=> date('Y-m-d', strtotime(str_replace('/','-',$data['moving_date']))),
		'new_location_id'	=> $data['new_location_id'],
		'note'				=> $data['note']
		);
		$this -> db -> trans_start();
		$this -> db -> insert('t_asset_trans', $query_data);
		$insert_id = $this -> db -> insert_id();
		
		foreach ($data['asset_requested'] as $asset){
			$asset_data = array('request_id' => $insert_id, 'asset_id' => $asset);
			$this -> db -> insert ('t_asset_trans_detail',$asset_data);
			}
		// $approval_status = requestApproval($this->session->userdata('user_id'), 'MR', $document_number);	
		$approval_status = request_approval($this->session->userdata('user_id'), 'AMR', $document_number);	
		$this -> db -> update ('t_asset_trans', array("request_status" => $approval_status), array ('document_ref' => $document_number));
		$this -> db -> trans_complete();
		if ($this->db->trans_status() === FALSE) return "asset_move_request_failed";
		else return "asset_move_request_added";
			}

	public function approve_move_request($docnumber, $approval_status){	
		/*
		 	Approval Status:
				1 (pending approval)
				2 (partially approved)
				3 (fully approved)
				4 (revise)
				5 (reject)
		*/
			$this -> db -> trans_start();
			$this -> db -> update ('t_asset_trans', array("request_status" => $approval_status), array ('document_ref' => $docnumber));
			$msg = '';
		if($approval_status == '3'){
			$data = $this->get_trans_doc($docnumber);
			foreach ($data as $row)
				$this -> db -> update ('t_asset', array("asset_status" => 'in transit'), array ('asset_id' => $row['asset_id']));
			$msg = 'Asset move request has been accepted, asset can now be moved safely';
		}
		elseif ($approval_status == '5')$msg = 'Asset move request has been rejected';
			$this-> db -> trans_complete();
		if ($this->db->trans_status() === FALSE) return array('success'=>0,'msg'=>'Failed to approve the usage request');
		else return array('success'=>1,'msg'=>$msg);
			}
	
	public function dispose_asset($data){
		if(!isset($data['assets_received']))return "no_asset_selected";
		$this -> db -> trans_start();
		foreach ($data['assets_received'] as $asset_id){
			$update_data = array(
  				"status" 			=> 'disposed',
				'transaction_date'	=> date('Y-m-d', strtotime(str_replace('/','-',$data['date'])))
				);
			$where_data = array(
  				'asset_id' 			=> $asset_id,
				'request_id'		=> $data['request_id']
				);
			$this -> db -> update ('t_asset_trans_detail', $update_data, $where_data);	
			$this -> db -> update ('t_asset', array('asset_status'=>'disposed'), array('asset_id' => $asset_id));
			}
		$this -> db -> trans_complete();
		if ($this->db->trans_status() === FALSE) return "asset_disposal_failed";
		else return "asset_disposed";
			}
	
	public function add_disposal_request($data){
		if(!isset($data['asset_requested']))return "no_asset_selected";
		$document_number = get_ref_number('disposal_request');
		$this -> load-> helper ('approval');
		
		$this -> db -> trans_start();
		
		$query_data = array(
  		'document_ref' 		=> $document_number,
		'location_id'		=> $this -> session -> userdata ('user_location_id'),
		'requested_by'		=> $this -> session -> userdata ('user_id'),
		'transaction_date'	=> date('Y-m-d', strtotime(str_replace('/','-',$data['disposal_date']))),
		'note'				=> $data['note'],
		'transaction_type'	=> 'disposal',
		);
		
		$this -> db -> insert('t_asset_trans', $query_data);
		$insert_id = $this -> db -> insert_id();
		
		foreach ($data['asset_requested'] as $asset){
			$asset_data = array('request_id' => $insert_id, 'asset_id' => $asset);
			$this -> db -> insert ('t_asset_trans_detail',$asset_data);
			}
		// $approval_status = requestApproval($this->session->userdata('user_id'), 'MR', $document_number);	
		$approval_status = request_approval($this->session->userdata('user_id'), 'ADR', $document_number);	
		$this -> db -> update ('t_asset_trans', array("request_status" => $approval_status), array ('document_ref' => $document_number));
		$this -> db -> trans_complete();

		if ($this->db->trans_status() === FALSE) return "asset_disposal_request_failed";
		else return "asset_disposal_request_added";
			}

	/*=========================================
	=            Depreciation Part            =
	=========================================*/

	/**
	*
	* Get list of assets to be depreciated
	* From a given depreciation date
	*
	**/
	

	public function get_asset_depre($depr_date)
	{
		$this->db->from('v_asset_to_depre');
		$this->db->where('date_acquired <=', $depr_date);
		return $this->db->get()->result_array();
	}
	
	public function add_depreciation($data){
		if(!isset($data['asset_requested']))return "no_asset_selected";
		$document_number = get_ref_number('depreciation');
		$this -> load-> helper ('approval');
		
		$this -> db -> trans_start();
		
		$query_data = array(
  		'document_ref' 		=> $document_number,
		'location_id'		=> $data['location_id'],
		'requested_by'		=> $this->ion_auth->user()->row()->id,
		'transaction_date'	=> date('Y-m-d', strtotime(str_replace('/','-',$data['depreciation_date']))),
		'note'				=> $data['note'],
		'transaction_type'	=> 'depreciation',
		);
		
		$this -> db -> insert('t_asset_trans', $query_data);
		$insert_id = $this -> db -> insert_id();
		
		foreach ($data['asset_requested'] as $asset){
			$asset_data = array('request_id' => $insert_id, 'asset_id' => $asset);
			$this -> db -> insert ('t_asset_trans_detail',$asset_data);
			}

		// *SAMPLE* Journal Only
			$this->load->model('journal_model');
            $this->load->model('document_model');
            $this->load->model('link_account_model');

            $journal['code'] = $this->document_model->useDocSeries('JRNL');

            // Header
            // journal_id, journal_code, posting_date, document_date, journal_memo, journal_type, journal_ref1, journal_ref2, journal_ref3, create_by, create_dt
            

            $journal['header'] = array(
                   'journal_code'   => $journal['code'],
                   'posting_date'   => date('Y-m-d', strtotime(str_replace('/','-',$data['depreciation_date']))),
                   'document_date'  => date('Y-m-d', strtotime(str_replace('/','-',$data['depreciation_date']))),
                   'journal_memo'   => 'Depreciation of Assets',
                   'journal_type'   => 'AST',
                   'journal_ref1'   => $document_number,
                   'create_by'      => $this->ion_auth->user()->row()->id,
                   'create_dt'      => date('Y-m-d H:i:s')
                );

            // Detail
            //journal_id, journal_line, account_id, journal_dr, journal_cr, journal_cc, journal_curr
            $journal['detail'] = array();

            $journal['detail'][0] = array(
                        'journal_line'  => 1,
                        'account_id'    => 73, //Depreciation Expenses
                        'journal_dr'    => 20833,
                        'journal_cr'    => 0,
                        'journal_cc'    => 0,
                        'journal_curr'  => 'IDR'
                    );

             $journal['detail'][1] = array(
                        'journal_line'  => 2,
                        'account_id'    => 71, //Machine Accm Dep
                        'journal_dr'    => 0,
                        'journal_cr'    => 20833,
                        'journal_cc'    => 0,
                        'journal_curr'  => 'IDR'
                    );

            // INSERT ALL JOURNAL
            $data = array(
                't_account_journal_header' => $journal['header'],
                't_account_journal_detail' => $journal['detail'],
                );
            $this -> journal_model -> add($data);

		// END SAMPLE Journal

		// $approval_status = requestApproval($this->session->userdata('user_id'), 'MR', $document_number);	
		$approval_status = request_approval($this->ion_auth->user()->row()->id, 'AST_DEP', $document_number);	
		$this -> db -> update ('t_asset_trans', array("request_status" => $approval_status), array ('document_ref' => $document_number));
		$this -> db -> trans_complete();

		if ($this->db->trans_status() === FALSE) return "asset_depreciation_failed";
		else return "asset depreciated";
	}



	/*-----  End of Depreciation Part  ------*/
	
	public function approve_disposal_request($docnumber, $approval_status){	
		$this -> db -> trans_start();
		$this -> db -> update ('t_asset_trans', array("request_status" => $approval_status), array ('document_ref' => $docnumber));
		$msg = '';
		if($approval_status == '3'){
			$data = $this->get_trans_doc($docnumber);
			foreach ($data as $row)
				//$this -> db -> update ('t_asset', array("asset_status" => 'disposed'), array ('asset_id' => $row['asset_id']));
			$msg = 'asset disposal request has been accepted, asset can now be disposed safely';
		} 
		elseif ($approval_status == '5'){
			  $msg = 'asset disposal request has been rejected'	;
		}
			$this-> db -> trans_complete();
		if ($this->db->trans_status() === FALSE)    		
			return array('success'=>0,'msg'=>'Sorry, this document cannot be approved, please login to Skyware System to view the approval history.');
		else return array('success'=>1,'msg'=>$msg);
			}

	public function asset_list_report($filters){
		if(count($filters['sel_group']) && is_array($filters['sel_group']) && !in_array(0,$filters['sel_group'])){
			$group_ids = implode(',',$filters['sel_group']);
			//$this -> db -> where_in ('t_asset.asset_group_id', $filters['sel_group']);
			$this -> db -> where ("( t_asset.asset_group_id IN ($group_ids) OR t_asset_group.group_parent_id IN ($group_ids) )");
		}
		
		if(count($filters['sel_asset_status']) && is_array($filters['sel_asset_status']) && !in_array(0,$filters['sel_asset_status'])){
			$this -> db -> where_in ('t_asset.asset_status', $filters['sel_asset_status']);
		}
		
		if(count($filters['sel_location']) && is_array($filters['sel_location']) && !in_array(0,$filters['sel_location'])){
			$this -> db -> where_in ('t_asset.asset_location_id', $filters['sel_location']);
		}
		
		/* date range filter */
		if(	array_key_exists('rdo_datefilter',$filters) &&	
			$filters['rdo_datefilter'] == 1 && $filters['date_from'] != '' && $filters['date_to'] != ''
		  ){
			$this -> db -> where ('date_acquired >=', date('Y-m-d',strtotime(str_replace('/','-',$filters['date_from']))));
			$this -> db -> where ('date_acquired <=', date('Y-m-d',strtotime(str_replace('/','-',$filters['date_to'])))); 
		}
		
		/* price range filter */
		if(	array_key_exists('rdo_pricefilter',$filters) &&	
			$filters['rdo_pricefilter'] == 1 && $filters['price_from'] != '' && $filters['price_to'] != ''
		  ){
			$this -> db -> where ('asset_cost >=', min($this->toInt($filters['price_from']),$this->toInt($filters['price_to'])) );
			$this -> db -> where ('asset_cost <=', max($this->toInt($filters['price_to']),$this->toInt($filters['price_from'])) );
		}
		
		$this -> db -> select ('t_asset.*, t_asset_group.group_name, t_site_location.location_name');
		$this -> db -> join ('t_asset_group','t_asset_group.group_id = t_asset.asset_group_id','left');
		$this -> db -> join ('t_site_location','t_site_location.location_id = t_asset.asset_location_id','left');
		return $this -> db -> get('t_asset') -> result_array();
	}
	
	public function asset_move_report($filters){
		if(count($filters['sel_group']) && is_array($filters['sel_group']) && !in_array(0,$filters['sel_group'])){
			$group_ids = implode(',',$filters['sel_group']);
			//$this -> db -> where_in ('t_asset.asset_group_id', $filters['sel_group']);
			$this -> db -> where ("( a.asset_group_id IN ($group_ids) OR g.group_parent_id IN ($group_ids) )");
		}
		
		if(count($filters['sel_location']) && is_array($filters['sel_location']) && !in_array(0,$filters['sel_location'])){
			$location_ids = implode(',',$filters['sel_location']);
			//$this -> db -> where_in ('t_asset.asset_group_id', $filters['sel_group']);
			$this -> db -> where ("loc_from.location_id IN ($location_ids)");
		}
		
		if(count($filters['sel_location_to']) && is_array($filters['sel_location_to']) && !in_array(0,$filters['sel_location_to'])){
			$to_location_ids = implode(',',$filters['sel_location_to']);
			//$this -> db -> where_in ('t_asset.asset_group_id', $filters['sel_group']);
			$this -> db -> where ("loc_to.location_id IN ($to_location_ids)");
		}
		
		$this -> db -> select (
		   't.document_ref, t.transaction_date, u.full_name, t.note, t.request_status,
			a.asset_label, a.asset_name, g.group_name,
			loc_from.location_name as from_location, loc_to.location_name as to_location'
		);
		$this -> db -> from ('t_asset_trans t');
		$this -> db -> join ('t_asset_trans_detail td','td.request_id = t.request_id','inner');
		$this -> db -> join ('t_asset a','a.asset_id = td.asset_id','inner');
		$this -> db -> join ('t_asset_group g','g.group_id = a.asset_group_id','left');
		$this -> db -> join ('t_user u','u.user_id = t.requested_by','inner');
		$this -> db -> join ('t_site_location loc_from','loc_from.location_id = t.location_id','left');
		$this -> db -> join ('t_site_location loc_to','loc_to.location_id = t.new_location_id','left');
		$this -> db -> where ('t.transaction_type','move');
		$this -> db -> order_by ('t.transaction_date', 'DESC');
		
		return $this -> db -> get() -> result_array();
	}

	public function get_maintenance_type($type_id = FALSE){
		if ($type_id) $this -> db -> where ('maint_type_id',$type_id);
		$query = $this -> db -> get('t_maint_type');
		if (!$query -> num_rows()) return FALSE;
		return  ($type_id) ? $query -> row_array() : $query ->result_array();
	}
	
	public function get_maintenance_schedule($schedule_id = FALSE,$detail = NULL){
		if ($schedule_id) $this -> db -> where ('t_maint_schedule.schedule_id',$schedule_id);
		if ($detail) 	  {
			$this->db->select ('t_maint_schedule.*, t_maint_schedule_detail.*, t_asset.*, t_maint_type.*, t_user.user_id, t_user.user_name, t_user.full_name, t_site_location.location_name');
			$this->db->from  ('t_maint_schedule');
			$this->db->join  ('t_maint_schedule_detail', 't_maint_schedule.schedule_id = t_maint_schedule_detail.schedule_id', 'left');
			$this->db->join  ('t_asset', 't_asset.asset_id = t_maint_schedule_detail.asset_id', 'left');
			$this->db->join  ('t_maint_type', 't_maint_type.maint_type_id= t_maint_schedule_detail.maint_type_id', 'left');
			$this->db->join  ('t_user', 't_user.user_id= t_maint_schedule.schedule_pic', 'left');
			$this->db->join  ('t_site_location', 't_site_location.location_id = t_asset.asset_location_id', 'left');
			$query = $this->db->get();
			}
		else $query = $this -> db -> get('t_maint_schedule');
		if (!$query -> num_rows()) return FALSE;
		return  ($schedule_id && !$detail) ? $query -> row_array() : $query ->result_array();
	}
	
	public function get_user($user_id = FALSE){
		if ($user_id) $this -> db -> where ('t_user',$user_id);
		$query = $this -> db -> get('t_user');
		if (!$query -> num_rows()) return FALSE;
		return  ($user_id) ? $query -> row_array() : $query ->result_array();
	}
	
	public function get_vendor($vendor_id = FALSE){
		if ($vendor_id) $this -> db -> where ('buss_id',$vendor_id);
		$this -> db -> where ('is_vendor',1);
		$query = $this -> db -> get('t_business');
		if (!$query -> num_rows()) return FALSE;
		return  ($vendor_id) ? $query -> row_array() : $query ->result_array();
	}
	
	public function new_maintenance_schedule($data){
		$this->db->trans_start();
		$query_data = array(
  		'schedule_code' 		=> $data['schedule_code'],
		'schedule_pic'			=> $data['pic_id']
		);
		
		$this -> db -> insert('t_maint_schedule', $query_data);
		$insert_id = $this -> db -> insert_id();
		
		for ($i=0; $i<$data['added_row'];$i++){
			$asset_data = array(
			'schedule_id'	 => $insert_id,
			'asset_id' 		 => $data['asset_id'][$i],
			'maint_type_id'  => $data['maint_type_id'][$i],
			'scheduled_date' => date('Y-m-d', strtotime(str_replace('/','-',$data['date_schedule'][$i]))),
			'maint_note' 	 => $data['note'][$i]
			);
			$this -> db -> insert ('t_maint_schedule_detail',$asset_data);
			}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) return "maintenance_schedule_add_failed";
		else return "maintenance_schedule_added";
		}
		
	public function new_maintenance_order($data){
		$this->db->trans_start();
		$query_data = array(
		'maint_order_pic'			=> $data['pic_id'],
		'maint_order_date'			=> date('Y-m-d', strtotime(str_replace('/','-',$data['order_date']))),
		'maint_order_schedule_id'	=> $data['schedule_id'],
		'maint_order_vendor_id'		=> $data['vendor_id'],
		'maint_order_cost'			=> $data['maint_cost'],
		);
		
		$this -> db -> insert('t_maint_order', $query_data);
		$insert_id = $this -> db -> insert_id();
		
		for ($i=0; $i<$data['added_row'];$i++){
			$asset_data = array(
			'maint_order_id'	 => $insert_id,
			'asset_id' 		 => $data['asset_id'][$i],
			'maint_type_id'  => $data['maint_type_id'][$i],
			'maint_note' 	 => $data['note'][$i]
			);
			$this -> db -> insert ('t_maint_order_detail',$asset_data);
			}
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE) return "maintenance_order_add_failed";
		else return "maintenance_order_added";
		}	
		
	public function asset_history_report($filters){
		if(count($filters['sel_group']) && is_array($filters['sel_group']) && !in_array(0,$filters['sel_group'])){
			$group_ids = implode(',',$filters['sel_group']);
			//$this -> db -> where_in ('t_asset.asset_group_id', $filters['sel_group']);
			$this -> db -> where ("( v.group_id IN ($group_ids) OR v.group_parent_id IN ($group_ids) )");
		}
		
		if(count($filters['sel_location']) && is_array($filters['sel_location']) && !in_array(0,$filters['sel_location'])){
			$location_ids = implode(',',$filters['sel_location']);
			//$this -> db -> where_in ('t_asset.asset_group_id', $filters['sel_group']);
			$this -> db -> where ("v.asset_id IN ( select asset_id from t_asset where asset_location_id in ($location_ids) )");
		}
		
		/* date range filter */
		if(	$filters['date_from'] != '' && $filters['date_to'] != ''
		  ){
			$this -> db -> where ('v.trans_date >=', date('Y-m-d',strtotime(str_replace('/','-',$filters['date_from']))));
			$this -> db -> where ('v.trans_date <=', date('Y-m-d',strtotime(str_replace('/','-',$filters['date_to'])))); 
		}
		
		$this -> db -> select ('v.*, parent_group.group_name as parent_group_name, usr.full_name');
		$this -> db -> from ('v_asset_trans_history v');
		$this -> db -> join ('t_user usr','usr.user_id = v.trans_user','left');
		$this -> db -> join ('t_asset_group parent_group','parent_group.group_id = v.group_parent_id','left');
		$this -> db -> order_by ('v.asset_id', 'ASC');
		$this -> db -> order_by ('v.trans_date', 'ASC');
		
		return $this -> db -> get() -> result_array();
	}
	
	// get daily reminder (currently asset warranty expiration and maintenance due today for the current user)
	public function get_daily_reminder_count($user_id = NULL){
		if ($user_id == NULL){
			if ( $this->session->userdata('user_id') !== FALSE && is_numeric($this->session->userdata('user_id')))
			{
				$user_id = $this->session->userdata('user_id');
			} else {
				return 0 ;
			}
		}
		return $this->db->where( "user_id in (0,{$user_id})" )->count_all_results('v_daily_asset_reminder');
	}
	
	public function get_daily_reminders($user_id = NULL){
		if ($user_id == NULL){
			if ( $this->session->userdata('user_id') !== FALSE && is_numeric($this->session->userdata('user_id')))
			{
				$user_id = $this->session->userdata('user_id');
			} else {
				return 0 ;
			}
		}
		return $this->db->where( "user_id in (0,{$user_id})" )->get('v_daily_asset_reminder')->result_array();
	}
	
	private function toInt($str){
		return preg_replace("/([^0-9\\.])/i", "", $str);
	}	
		
	
}