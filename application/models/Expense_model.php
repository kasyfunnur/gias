<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Expense_model extends MY_Model{
	public $table;
	public function __construct(){
		parent::__construct();
		log_message("debug", "Asset helper model class initiated");
	}
	
	public function add($doc,$desc,$curr,$amt,$accrue_to=NULL,$date=NULL){
		$temp_accrue = (isset($accrue_to)) ? 1 : 0;
		$temp_accrue_to = (isset($accrue_to)) ? $accrue_to : '';
		$temp_date = (isset($date)) ? $date : date('Y-m-d H:i:s');;
		$query_data = array(
			'exp_dt' 		=> $temp_date,
			'exp_doc'		=> $doc,
			'exp_desc'		=> $desc,
			'exp_curr'		=> $curr,
			'exp_amount'	=> $amt,
			'is_accrue'		=> $temp_accrue,
			'accrue_to'		=> $temp_accrue_to
		);
		$this->db->insert('t_expense', $query_data);
	}
	
	
	
}