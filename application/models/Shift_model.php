<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  /**
   *
   */
  class Shift_model extends CI_Model
  {

    function __construct()
    {
      // code...
    }

    public function ambil($id = FALSE)
    {
      if($id)
  			$this->db->where('idShift', $id);

  		return ($id) ? $this->db->get('t_shift')->row_array() : $this->db->get('t_shift')->result_array();
    }

    public function store($data)
    {
      return $this->db->insert('t_shift', $data);
    }

    public function update($id, $data)
    {
      $this->db->where('idShift', $id);
		  $this->db->update('t_shift', $data);
      return TRUE;
    }
  }
