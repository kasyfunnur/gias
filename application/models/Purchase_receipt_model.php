<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class purchase_receipt_model extends MY_Model {
    var $details;
    function __construct(){
        // Call the Model constructor
        parent::__construct();
        $this->load->model('document_model');
        $this->load->model('purchase_order_model');
    }
	
	//AVER
	public function add($data){
		$data['t_purchase_receipt_header']['doc_num'] = $this->document_model->getNextSeries('PUR_RCV');
		$this->db->insert('t_purchase_receipt_header',$data['t_purchase_receipt_header']);
		$temp_id = $this->db->insert_id();
		$temp_ref = $this->purchase_order_model->view(array("doc_num"=>$data['t_purchase_receipt_header']['doc_ref']),NULL);
		
		$temp_subtotal = 0;
		$temp_disc = 0;
		$temp_total = 0;

		foreach($data['t_purchase_receipt_detail'] as $x=>$detail){
			$detail['doc_id'] = $temp_id;
			//$detail['doc_id'] = $this->db->insert_id();
			$detail['reff_id'] = $temp_ref[0]['doc_id'];
			$this->db->insert('t_purchase_receipt_detail',$detail);
			$data_item = array(
				"doc_type"		=> "RCV",
				"doc_num"		=> $data['t_purchase_receipt_header']['doc_num'],
				"doc_date"		=> $data['t_purchase_receipt_header']['doc_dt'],				
				"item_id"		=> $detail['item_id'],
				"item_qty"		=> $detail['item_qty'],
				"item_value"	=> $detail['item_netprice'],
				"total_value"	=> $detail['item_total'],
				"whse_id"		=> $data['t_purchase_receipt_header']['whse_id'],
				"trx_status"	=> "ORIGINAL",
				"create_by"		=> $this->session->userdata("username")
			);
			$this->db->insert('t_inventory_transaction',$data_item);
			$data_po_detail = array(
				"item_qty_closed" 	=> ($data['other'][$x]['ast_po_qty'] - $data['other'][$x]['ast_qty_open']) + $detail['item_qty']
			);
			$this->db->update('t_purchase_order_detail',
				$data_po_detail,
				array(	'doc_id'=>$temp_ref[0]['doc_id'],
						'doc_line'=>$detail['reff_line'])
			);

			//$temp_subtotal	+= $detail['item_total'];
		}
		/*$this->db->update('t_purchase_receipt_header',
			array('doc_subtotal'=>$temp_total),
			array('doc_id'=>$temp_ref[0]['doc_id']));*/
		$this->document_model->useDocSeries('PUR_RCV');
	}
	public function view($where=NULL,$order=NULL){
		$this->db->select("*");
		$this->db->from('t_purchase_receipt_header');
		$this->db->join('t_purchase_receipt_detail','t_purchase_receipt_header.doc_id = t_purchase_receipt_detail.doc_id','inner');
		if($where)$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function edit($data,$where){
		$this->db->update('t_purchase_receipt_header',$data['t_purchase_receipt_header'],$where);
		if(isset($data['t_purchase_receipt_detail'])){
			foreach($data['t_purchase_receipt_detail'] as $detail){
				$this->db->update('t_purchase_receipt_detail',$detail,
					array(
						$where[0],
						'doc_line'=>$detail['doc_line']
					)
				);
			}
		}		
	}
	public function remove($where){		
	}

	public function count_doc(){		
		return $this->db->count_all('t_purchase_receipt_header');
	}

	public function getAll($filter=NULL,$page=0){
		$this->db->select('*');
		$this->db->from('t_purchase_receipt_header');
		$this->db->join('t_business','t_purchase_receipt_header.buss_id = t_business.buss_id','inner');
		$this->db->join('t_site_location','t_purchase_receipt_header.whse_id = t_site_location.location_id','inner');
		if($filter)$this->db->where('t_purchase_receipt_header.doc_status',$filter);
		$this->db->order_by('doc_id','desc');
		$query = $this->db->get('',10,$page);		
		return $query -> result_array();
	}







	
	public function getAll_with_vendor($id,$status=NULL){
		$this->db->select('*');
		$this->db->from('t_purchase_receipt_header');
		$this->db->join('t_business','t_purchase_receipt_header.buss_id = t_business.buss_id','inner');
		$this->db->join('t_site_location','t_site_location.location_id = t_purchase_receipt_header.whse_id','inner');
		/*if($status != NULL){
			$this->db->where('billing_status',$status);
		}else{
			$this->db->where_not_in('billing_status','BILLED');
		}*/
		$this->db->where('doc_status','ACTIVE');

		if($id != NULL){
			$this->db->where('t_business.buss_id',$id);
		}
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	
	public function get_ref($code){
		$this->db->select('reff_id');
		$this->db->distinct('reff_id');
		$this->db->from('t_purchase_receipt_header');
		$this->db->join('t_purchase_receipt_detail','t_purchase_receipt_header.doc_id = t_purchase_receipt_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();
	}
	
	public function set_item_close($code){
		$this->db->select('*');
		$this->db->from('t_purchase_receipt_header');
		$this->db->join('t_purchase_receipt_detail','t_purchase_receipt_header.doc_id = t_purchase_receipt_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		foreach($query->result_array() as $row){
			if($row['item_qty_closed'] >= $row['item_qty']){
				$data = array("line_close" => 1);
			}else{
				$data = array("line_close" => 0);
			}
			$this->db->where('doc_id',$row['doc_id']);
			$this->db->where('doc_line',$row['doc_line']);				
			$this->db->update('t_purchase_receipt_detail', $data);	
		}
	}	
	public function set_doc_close($code){
		$this->db->select('line_close');
		$this->db->distinct('line_close');
		$this->db->from('t_purchase_receipt_header');
		$this->db->join('t_purchase_receipt_detail','t_purchase_receipt_header.doc_id = t_purchase_receipt_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		$temp = $query->result_array();		
		$doc_status = "ACTIVE";
		$log_status = "PENDING";
		if(count($temp) > 1){
			$log_status = "PARTIAL";
		}else{
			if($temp[0]['line_close'] == 1){
				$doc_status = "CLOSED";
				$log_status = "RECEIVED";				
			}
		}
		$data = array("doc_status"=>$doc_status);
		$this->db->update('t_purchase_receipt_header', $data, array('doc_num' => $code));
	}	

	public function get_qty_close($code){
		$this->db->select('*');
		$this->db->from('t_purchase_receipt_header');
		$this->db->join('t_purchase_receipt_detail','t_purchase_receipt_header.doc_id = t_purchase_receipt_detail.doc_id','inner');
		$this->db->where('line_close',0);
		$this->db->where('item_qty_closed > 0');
		$query = $this->db->get();
		return $query -> result_array();	
	}
	public function get_close_line($code){
		$this->db->select('*');
		$this->db->from('t_purchase_receipt_header');
		$this->db->join('t_purchase_receipt_detail','t_purchase_receipt_header.doc_id = t_purchase_receipt_detail.doc_id','inner');
		$this->db->where('line_close',1);
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();		
	}
	public function get_open_line($code){
		$this->db->select('*');
		$this->db->from('t_purchase_receipt_header');
		$this->db->join('t_purchase_receipt_detail','t_purchase_receipt_header.doc_id = t_purchase_receipt_detail.doc_id','inner');
		$this->db->where('line_close',0);
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();		
	}
	
	public function get_po_rcv_status_pending($po){
		$this->db->select('*');
		$this->db->from('t_purchase_receipt_header');
		//$this->db->where("(billing_status = 'PARTIAL' OR billing_status = 'BILLED')");
		$this->db->where("doc_ref",$po);
		$query = $this->db->get();
		return $query -> result_array();
	}
	public function get_po_rcv_status_done($po){
		$this->db->select('*');
		$this->db->from('t_purchase_receipt_header');
		//$this->db->where("(billing_status = 'PARTIAL' OR billing_status = 'UNBILLED')");
		$this->db->where("doc_ref",$po);
		$query = $this->db->get();
		return $query -> result_array();
	}
	
	
	public function insert_header($data){
		$this->db->insert('t_purchase_receipt_header',$data);
		return $this->db->insert_id();
	}
	public function insert_detail($data){
		$this->db->insert('t_purchase_receipt_detail',$data);	
		return $this->db->insert_id();
	}
	
	public function update_header($data,$where){
		if(count($where)<=1){
			$this->db->where($where[0]['field'],$where[0]['value']);
		}else{
			for($j = 0 ; $j< count($where) ; $j++){
				$this->db->where($where[$j]['field'],$where[$j]['value']);
			}
		}
		return $this->db->update('t_purchase_receipt_header', $data);	
	}
	
	public function getHeader($doc){
		$query = $this -> db -> query ("
			select * from t_purchase_receipt_header 
			inner join t_site_location on t_purchase_receipt_header.whse_id = t_site_location.location_id 
			inner join t_business on t_business.buss_id = t_purchase_receipt_header.buss_id 
			where doc_num = '".$doc."'
		");
		return $query -> result_array();
	}
	public function getDetail($doc){
		$query = $this -> db -> query ("
			select * from t_purchase_receipt_detail
			inner join t_inventory on t_purchase_receipt_detail.item_id = t_inventory.item_id
			where doc_id = (select doc_id from t_purchase_receipt_header where doc_num = '".$doc."')
			order by doc_line
		");
		return $query -> result_array();
	}
	public function getDetail_rcv_open($doc){
		$query = $this -> db -> query ("
			select *, t_purchase_receipt_detail.item_name as 'item_name_po' from t_purchase_receipt_detail
			inner join t_inventory on t_purchase_receipt_detail.item_id = t_inventory.item_id
			where doc_id = (select doc_id from t_purchase_receipt_header where doc_num = '".$doc."')
			and t_purchase_receipt_detail.item_qty_closed < t_purchase_receipt_detail.item_qty 
			order by doc_line
		");
		return $query -> result_array();
	}
}