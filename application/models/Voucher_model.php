<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);
	}

	public function add_voucher($data)
	{
		$this->db->trans_start();

		$this->db->insert('t_credit_voucher', $data);

		$trx_id = $this->db->insert_id();

		// generate the vouchers
		$vouchers = $this -> generate_vouchers($trx_id, $data['voucher_code'],$data['voucher_count']);

		$this->db->insert_batch('t_credit_voucher_detail', $vouchers);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return false;
		} else {
			return true;
		}
	}

	private function generate_vouchers($id, $code,$count){
		// contoh voucher  : ABC1514xxxxxxxxx (x 9 digit untuk random addition)
		$prepend = $code . date('jny');
		$vouchers = array();
		$rand_add = rand(1000,99999);
		for ($i=0; $i < $count; $i++) {
			array_push($vouchers, array(
				'voucher_id'		=> $id,
				'voucher_code' 		=> $code,
				'voucher_number' 	=> $prepend . sprintf('%09d',$rand_add),
				'used' 				=> false,
				'disabled' 			=> false
				));
			$rand_add += rand(1000,99999);
		}

		return $vouchers;
	}

	public function ajax_voucher_list($voucher_code = null)
	{

		$this->load->library('ssp');
		$join_query  = '';
		$where_query = '';
		$table 		= 't_credit_voucher';
		$primaryKey = 'id';
		$columns = array(
			array( 'db' => 'id' ,'dt' => 0),
			array( 'db' => 'voucher_code' ,'dt' => 1),
			array( 'db' => 'issued_date'	,'dt' => 2, 'formatter' => function ($d, $row){ return date('d-M-Y',strtotime($d));} ),
			array( 'db' => 'active_date'	,'dt' => 3, 'formatter' => function ($d, $row){ return date('d-M-Y',strtotime($d));} ),
			array( 'db' => 'expiry_date'	,'dt' => 4, 'formatter' => function ($d, $row){ return date('d-M-Y',strtotime($d));} ),
			array( 'db' => 'voucher_count'	,'dt' => 5 ),
			array( 'db' => 'value'			,'dt' => 6, 'formatter' => function ($d, $row){return number_format($d);} )
		);
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns, $join_query, $where_query);
	}

	public function get_voucher($voucher_id = null)
	{
		if(is_numeric($voucher_id)){
			$this->db->where('id', $voucher_id);
		}

		$voucher['header'] = is_numeric($voucher_id) ? 
			$this->db->get('t_credit_voucher')->row_array() :
			$this->db->get('t_credit_voucher')->result_array() ;
		
		if(is_numeric($voucher_id)){
			$this->db->where('voucher_id', $voucher_id);
		}

		$voucher['detail'] = $this->db->get('t_credit_voucher_detail')->result_array() ;

		return $voucher;
	}

	public function update_voucher($voucher_id, $data)
	{
		$this->db->where('id', $voucher_id);
		return $this->db->update('t_credit_voucher', $data);
	}

	public function void_remaining_voucher($voucher_id)
	{
		$this->db->where('voucher_id', $voucher_id);
		$this->db->where('used', false);
		$this->db->set('disabled',true);
		return $this->db->update('t_credit_voucher_detail');
	}

	public function scan_voucher($voucher_number)
	{
		$this->db->where('voucher_number', $voucher_number);
		return $this->db->get('t_credit_voucher_detail')->row_array();
	}

}

/* End of file Voucher_model.php */
/* Location: ./application/models/Voucher_model.php */