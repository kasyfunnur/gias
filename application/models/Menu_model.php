<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	public function add_menu($data)	
	{
		// menu_id, parent_id, menu_name, menu_link, menu_description, name_prefix, name_postfix, last_node, order_no
		if($this->db->insert_batch('t_side_menu', $data))
			return $this->db->insert_id();
		else
			return false;
	}

	public function get_menu($menu_id = null)
	{
		if(is_numeric($menu_id))
		{
			$this->db->where('menu_id', $menu_id);
		}

		return is_numeric($menu_id) ?
			$this->db->get('t_side_menu')->row_array() :
			$this->db->get('t_side_menu')->result_array() ;
	}
	public function get_menu_hide()
	{
		return $this->db->get('t_group_menu_hide')->result_array();
	}
	public function set_array_menu_hide($group,$menu_array)
	{
		foreach($menu_array as $a=>$menu){
			$this->set_menu_hide($group,$menu);
		}
	}
	public function set_menu_hide($group,$menu)
	{
		$this->db->set('group_id',$group);
		$this->db->set('menu_id',$menu);
		return $this->db->insert('t_group_menu_hide');
		//$this->db->insert_id();
	}
	public function get_menu_child($parent=0)
	{
		$query = $this -> db -> query ("SELECT *, Deriv.Count FROM `t_side_menu` a  LEFT OUTER JOIN (SELECT parent_id, COUNT(1) AS Count FROM `t_side_menu` WHERE active = 1 GROUP BY parent_id) Deriv ON Deriv.parent_id = a.menu_id WHERE a.active = 1 AND a.parent_id=" . $parent . " order by order_no ");
    	return $query -> result_array();
	}
	public function get_menu_active()
	{
		$this->db->where('active',1);
		return $this->db->get('t_side_menu')->result_array();
	}

	public function get_parents()
	{
		$this->db->where('last_node', 0);
		$this->db->order_by('order_no', 'asc');
		return $this->db->get('t_side_menu')->result_array();
	}

	public function delete_hide_group($group)
	{
		if(!$group)return false;
		$this->db->where('group_id',$group);
		$this->db->delete('t_group_menu_hide'); echo '1';
	}
	public function apply_hide($group)
	{
		$list_all_menu = $this->get_menu_active();
		$list_menu_id = array();
		//echo '2';
		foreach($list_all_menu as $m=>$mn){
			array_push($list_menu_id,$mn['menu_id']);
			//$this->set_array_menu_hide($group,$menu_array);
		}
		//var_dump($list_menu_id);
		//var_dump($group);
		$this->set_array_menu_hide($group,$list_menu_id);
	}
	public function delete_array_hide_menu($group,$menu_array)
	{
		foreach($menu_array as $a=>$menu){
			$this->delete_hide_menu($group,$menu);
		}
	}
	public function delete_hide_menu($group,$menu)
	{
		$this->db->where('group_id',$group);
		$this->db->where('menu_id',$menu);
		return $this->db->delete('t_group_menu_hide');
	}

	public function get_level($menu_id)
	{
		//$menu = $menu_id;
		$level = 0;
		$parent = '';
		do{
			$tmp = $this->db->where("menu_id",$menu_id)->get('t_side_menu')->row_array();
			$parent = $tmp['parent_id'];
			$menu = $parent;
			$level++;
		}while($parent == 0);
		return $level;
	}

	public function get_menu_tree($parent_menu_id = null)
	{
		if(is_numeric($parent_menu_id))
		{
			$this->db->where('parent_id', $parent_menu_id);
		}

		$this->db->select("menu_id as id,
			(case when parent_id = 0 then '#' else parent_id end) as parent, menu_name as text, menu_description as description, icon, last_node");

		$this->db->order_by('order_no', 'asc');

		return $this->db->get('t_side_menu')->result();
	}

	// from asset model tree
	function get_all_tree_format($node = null){
		if($node != null && is_numeric($node))
			$this->db->where('group_parent_id',$node);

		return 
			$this->db->select(
			"group_id as id,
			(case when group_parent_id = 0 then '#' else group_parent_id end) as parent,
		    group_name as `text`,
		    group_description as description"
		    )->from('t_asset_group')->get()->result();

		    // jstree requires root node to have parent as
		    // '#'

	}

	/**
	*
	* update menu
	*
	**/
	function update_menu($menu_id, $newdata)
	{
		if(!is_numeric($menu_id))
			return false;
		$this->db->where('menu_id', $menu_id);
		if(!array_key_exists('active',$newdata))
				$newdata['active'] = false;

		return $this->db->update('t_side_menu', $newdata);

	}
	

}

/* End of file Menu_model.php */
/* Location: ./application/models/Menu_model.php */