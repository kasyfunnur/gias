<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Position_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		}
	
	public function get_structure($division_id = NULL){
		if($division_id != NULL && is_numeric($division_id) && $division_id > 0)
			$this->db->where('parent_id',$division_id);
		$this->db->order_by('order_no','ASC');
		return $this->db->get('t_position')->result_array();
		}
	
	public function get_position($division_id = NULL){
		$this->db->where('position_type','position');
		if($division_id != NULL && is_numeric($division_id) && $division_id > 0)
			$this->db->where('parent_id',$division_id);
		$this->db->order_by('order_no','ASC');
		return $this->db->get('t_position')->result_array();
		}

	public function get_positions()
	{
		// position_id, position_code, position_name, position_desc, position_type, parent_id, last_node, order_no, division_id, division_code, division_name, child_count, user_count
		$this->db->where('position_type','position');
		$this->db->order_by('division_name', 'asc');
		$this->db->order_by('position_name', 'asc');
		return $this->db->get('v_position')->result_array();
	}
		
	public function get_division(){
		$this->db->where('position_type','division');
		$this->db->order_by('order_no','ASC');
		return $this->db->get('t_position')->result_array();
		}
	
	public function ajax_position_list($return = "datatable_array"){
		$table 		= 'v_position';
		$primaryKey = 'position_id';
		
		$columns = array(
			array( 'db' => 'position_code'		,'dt' => 0 ),
			array( 'db' => 'position_name'		,'dt' => 1 ),
			array( 'db' => 'position_desc'		,'dt' => 2 ),
			array( 'db' => 'position_type'		,'dt' => 3 ),
			array( 'db' => 'user_count'			,'dt' => 4 )
			);
		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);
		$this -> load -> library('ssp');
 
		return SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns);
		
		}
			
	public function add_position($data){
		$query_data = array(
			'position_code' 		=> $data['position_code'],
			'position_name'			=> $data['position_name'],
			'position_desc'			=> $data['position_desc'],
			'position_type'			=> $data['position_type'],
			'parent_id'				=> $data['position_type'] == 'division' ? 0 : $data['division_id'],
			'last_node'				=> $data['position_type'] == 'division' ? 0 : 1,
			'order_no'				=> -1 // -1 means it has to be updated later using auto assign order function
			);
		
		$this -> db -> trans_start();
		
		$this -> db -> insert ('t_position', $query_data);
		$last_id = $this -> db -> insert_id();
		
		$this -> auto_order() ;
		
		$this -> db -> trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			echo 'unable to create position';
		}
		
		}
		
	public function auto_order ()
	{
		/** WORK IN PROGRESS, STILL NEED ONE MORE QUERY TO RE-ORDER */
		$this -> db -> query (
		   "update t_position
			set order_no =
				CASE WHEN position_type = 'position'
					 THEN 
					IF((select max(a.order_no) + 1 from v_position a where a.division_id = t_position.parent_id) = 0,
						(select b.order_no + 1 from v_position b where b.position_id = t_position.parent_id),
						(select max(a.order_no) + 1 from v_position a where a.division_id = t_position.parent_id)
					  )
				else 
					(select max(a.order_no) + 1 from v_position a)
				END
			where
				t_position.order_no = -1;"
		);
	}
}