<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class sales_delivery_model extends MY_Model {
    var $details;
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	public function getAll(){
		$query = $this -> db -> query ("
			select * from t_sales_delivery_header 
			inner join t_site_location on t_sales_delivery_header.whse_id = t_site_location.location_id 
			inner join t_business on t_business.buss_id = t_sales_delivery_header.buss_id
			
			order by doc_id desc
		"); //where doc_status = 'ACTIVE'
		return $query -> result_array();
	}
	public function getAll_with_customer($id,$status=NULL){
		$this->db->select('*');
		$this->db->from('t_sales_delivery_header');
		$this->db->join('t_business','t_sales_delivery_header.buss_id = t_business.buss_id','inner');
		$this->db->join('t_site_location','t_site_location.location_id = t_sales_delivery_header.whse_id','inner');
		if($status != NULL){
			$this->db->where('billing_status',$status);
		}else{
			$this->db->where_not_in('billing_status','BILLED');
		}
		$this->db->where('doc_status','ACTIVE');

		if($id != NULL){
			$this->db->where('t_business.buss_id',$id);
		}
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	
	public function get_ref($code){
		$this->db->select('reff_id');
		$this->db->distinct('reff_id');
		$this->db->from('t_sales_delivery_header');
		$this->db->join('t_sales_delivery_detail','t_sales_delivery_header.doc_id = t_sales_delivery_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();
	}
	
	public function set_item_close($code){
		$this->db->select('*');
		$this->db->from('t_sales_delivery_header');
		$this->db->join('t_sales_delivery_detail','t_sales_delivery_header.doc_id = t_sales_delivery_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		foreach($query->result_array() as $row){
			if($row['item_qty_closed'] >= $row['item_qty']){
				$data = array("line_close" => 1);
			}else{
				$data = array("line_close" => 0);
			}
			$this->db->where('doc_id',$row['doc_id']);
			$this->db->where('doc_line',$row['doc_line']);				
			$this->db->update('t_sales_delivery_detail', $data);	
		}
	}
	public function set_doc_close($code){ //echo $code;
		$this->db->select('line_close');
		$this->db->distinct('line_close');
		$this->db->from('t_sales_delivery_header');
		$this->db->join('t_sales_delivery_detail','t_sales_delivery_header.doc_id = t_sales_delivery_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		$temp = $query->result_array();
		//var_dump($temp);
		$doc_status = "ACTIVE";
		$log_status = "UNBILLED";
		if(count($temp) > 1){
			$log_status = "PARTIAL";
		}else{
			if($temp[0]['line_close'] == 1){
				$doc_status = "CLOSED";
				$log_status = "BILLED";				
			}
		}
		$data = array("doc_status"=>$doc_status,"billing_status"=>$log_status);
		$this->db->where('doc_num',$code);
		$this->db->update('t_sales_delivery_header', $data);
	}
	
	public function get_qty_close($code){
		$this->db->select('*');
		$this->db->from('t_sales_delivery_header');
		$this->db->join('t_sales_delivery_detail','t_sales_delivery_header.doc_id = t_sales_delivery_detail.doc_id','inner');
		$this->db->where('line_close',0);
		$this->db->where('item_qty_closed > 0');
		$query = $this->db->get();
		return $query -> result_array();	
	}
	public function get_close_line($code){
		$this->db->select('*');
		$this->db->from('t_sales_delivery_header');
		$this->db->join('t_sales_delivery_detail','t_sales_delivery_header.doc_id = t_sales_delivery_detail.doc_id','inner');
		$this->db->where('line_close',1);
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();		
	}
	public function get_open_line($code){
		$this->db->select('*');
		$this->db->from('t_sales_delivery_header');
		$this->db->join('t_sales_delivery_detail','t_sales_delivery_header.doc_id = t_sales_delivery_detail.doc_id','inner');
		$this->db->where('line_close',0);
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();		
	}
	
	public function get_so_del_status_pending($po){
		$this->db->select('*');
		$this->db->from('t_sales_delivery_header');
		$this->db->where("(billing_status = 'PARTIAL' OR billing_status = 'BILLED')");
		$this->db->where("doc_ref",$po);
		$query = $this->db->get();
		return $query -> result_array();
	}
	public function get_so_del_status_done($po){
		$this->db->select('*');
		$this->db->from('t_sales_delivery_header');
		$this->db->where("(billing_status = 'PARTIAL' OR billing_status = 'UNBILLED')");
		$this->db->where("doc_ref",$po);
		$query = $this->db->get();
		return $query -> result_array();
	}
	
	
	public function insert_header($data){
		$this->db->insert('t_sales_delivery_header',$data);
		return $this->db->insert_id();
	}
	public function insert_detail($data){
		$this->db->insert('t_sales_delivery_detail',$data);	
		return $this->db->insert_id();
	}
	
	public function update_header($data,$where){
		if(count($where)<=1){
			$this->db->where($where[0]['field'],$where[0]['value']);
		}else{
			for($j = 0 ; $j< count($where) ; $j++){
				$this->db->where($where[$j]['field'],$where[$j]['value']);
			}
		}
		return $this->db->update('t_sales_delivery_header', $data);	
	}
	
	public function getHeader($doc){
		$query = $this -> db -> query ("
			select t_sales_delivery_header.*, t_site_location.*, t_business.*, 
				t_sales_order_header.doc_num as 'so_doc'
			from t_sales_delivery_header 
			inner join t_site_location on t_sales_delivery_header.whse_id = t_site_location.location_id 
			inner join t_business on t_business.buss_id = t_sales_delivery_header.buss_id 
			left join t_sales_order_header on t_sales_delivery_header.doc_ref = t_sales_order_header.doc_num
			where t_sales_delivery_header.doc_num = '".$doc."'
		");
		return $query -> result_array();
	}
	public function getDetail($doc){
		$query = $this -> db -> query ("
			select * from t_sales_delivery_detail
			inner join t_inventory on t_sales_delivery_detail.item_id = t_inventory.item_id
			where doc_id = (select doc_id from t_sales_delivery_header where doc_num = '".$doc."')
			order by doc_line
		");
		return $query -> result_array();
	}
	public function getDetail_del_open($doc){
		$query = $this -> db -> query ("
			select *, t_sales_delivery_detail.item_name as 'item_name_so' from t_sales_delivery_detail
			inner join t_inventory on t_sales_delivery_detail.item_id = t_inventory.item_id
			where doc_id = (select doc_id from t_sales_delivery_header where doc_num = '".$doc."')
			and t_sales_delivery_detail.item_qty_closed < t_sales_delivery_detail.item_qty 
			order by doc_line
		");
		return $query -> result_array();
	}
}