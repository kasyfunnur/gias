<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Document_model extends MY_Model {	
	function __construct()
    {
        parent::__construct();
    }
	
	public function createDocSeries($doc,$name="",$prefix="",$suffix="",$next,$last){
		if($name=="")$name=$doc;		
		$data_doc = array(
			"document_code" => $doc,
			"document_name" => $name,
			"document_series" => 'Default'
		);
		$this->db->insert('t_document',$data_doc);
		$new_id = $this->db->insert_id();
		$data_doc_series = array(
			"document_id" => $new_id,
			"series_name" => 'Default',
			"series_prefix" => $prefix,
			"first_num" => 1,
			"next_num" => $next,
			"last_num" => $last,
			"series_suffix" => $suffix,
			"is_active" => 1
		);
		$this->db->insert('t_document_series',$data_doc_series);
	}

	public function getNextSeries($code,$series=NULL) {
		if($series != NULL || $series != ""){
			$query = $this->db->query('select t0.series_prefix, t0.next_num, t0.last_num 
							from t_document_series t0 
							inner join t_document t1 on t0.document_id = t1.document_id
							where t1.document_code = \''.$code.'\'
							and t0.series_name = \''.$series.'\'
							and t0.is_active = 1');
		}else{
			$query = $this->db->query("select t0.series_prefix, t0.next_num, t0.last_num  
							from t_document_series t0 
							inner join t_document t1 on t0.document_id = t1.document_id 
							where t1.document_code = '".$code."' 
							and t0.series_id = (select document_series from t_document where t_document.document_code = '".$code."') 
							and t0.is_active = 1");
		}
		$a = $query->result_array();
		//var_dump($a);
		return $a[0]['series_prefix']."-".sprintf('%04d', $a[0]['next_num']);
	}
	public function useDocSeries($code,$series=NULL){
		$next_num = $this->getNextSeries($code,$series=NULL);
		if($series != NULL || $series != ""){
			$this->db->query('update t_document_series set next_num = next_num + 1 
			where document_id  = (select document_id from t_document where document_code = \''.$code.'\') 
			and series_name = \''.$series.'\'
			and t0.series_name = \''.$series.'\'
			and is_active = 1');
		}else{
			$this->db->query('update t_document_series set next_num = next_num + 1 
			where document_id  = (select document_id from t_document where document_code = \''.$code.'\') 
			and series_id = (select document_series from t_document where t_document.document_code = \''.$code.'\') 
			and is_active = 1');
		}
		return $next_num;
	}

	/**
	 * List of Document
	 */
	public function get_all_docs()
	{
		$this->db->select("*");
		$this->db->from('t_document');
		$this->db->join('t_document_series','t_document.document_id = t_document_series.document_id');
		return $this->db->get()->result_array();
	}

	public function add_attachment($doc_num,$file_name,$file_desc=NULL)
	{
		$this->db->set("doc_num",$doc_num);
		$this->db->set("file_name",$file_name);
		if($file_desc){
			$this->db->set("file_desc",$file_desc);
		}else{
			$this->db->set("file_desc",substr($file_name,14, strlen($file_name)));	
		}
		
		$this->db->insert('t_attachment');
		return $this->db->insert_id();
	}

	public function del_attachment($doc_num)
	{
		$this->db->where("doc_num",$doc_num);
		return $this->db->delete("t_attachment");
	}
}