<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class warehouse_model extends MY_Model {    
	public $table;
    function __construct(){
        // Call the Model constructor
        parent::__construct();
		$this->table = '';
		$this->load->model('document_model');		
    }
	
	//AVER WH
	public function add($data){	
		$this->db->insert('t_site_location',$data);		
	}
	public function view($where=NULL,$order=NULL){
		$this->db->select("*");
		$this->db->from('t_site_location');
		if($where)$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function edit($data,$where){
		$this->db->update('t_site_location',$data,$where);
	}
	public function remove($where){		
	}


	//AVER INV WH
	public function add_inv_wh($data){
		$this->db->insert('t_inventory_warehouse',$data);
	}
	public function edit_inv_wh($data,$where){
		$this->db->update('t_inventory_warehouse',$data,$where);
	}
}