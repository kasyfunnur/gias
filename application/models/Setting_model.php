<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting_model extends CI_Model{		
	public function __construct(){
		parent::__construct();
		log_message('debug', "User helper model class initiated");
		}
	
	public function get_company_info(){
		$this->db->select("*");
		$query = $this->db->get('t_company');
		return $query->row_array();
	}
}