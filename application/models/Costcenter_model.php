<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Costcenter_model extends CI_Model {

	/**
	*
	* get()
	* Get costcenter based on ID if give, or all if not
	*
	*/
	public function get($id = FALSE){
		if($id)
			$this->db->where('id', $id);

		return ($id) ? $this->db->get('t_costcenter')->row_array() : $this->db->get('t_costcenter')->result_array();
	}

	/**
	*
	* costcenter_add($data)
	* Add new costcenter
	*
	*/
	public function costcenter_add($data)
	{
		if(!count($data))
			return false;

		if ($this->db->insert('t_costcenter', $data)){
			return $this->db->insert_id();
		} else {
			return false;
		}

	}

	/**
	*
	* costcenter_add($data)
	* Add new costcenter
	*
	*/
	public function costcenter_edit($id,$data)
	{
		if(!count($data))
			return false;

		$this->db->where('id', $id);

		return $this->db->update('t_costcenter', $data);

	}
	

}

/* End of file Costcenter_model.php */
/* Location: ./application/models/Costcenter_model.php */