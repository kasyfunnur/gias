<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model{
	
	/*
	$data['total_assets']  = 10000;
	$data['total_incoming_assets']  = 10000;
	$data['total_inactive_assets']  = 10000;
	$data['total_pending_approval']  = 10000;
	$data['total_location']  = 10000;
	$data['total_user']  = 10000;
	$data['total_pending_po']  = 10000;
	*/
	
	public function total_stock_count(){
		$this->db->select("SUM(qty_onhand) AS qty_onhand");
		//$this->db->where('item_status','ACTIVE');
		$query = $this->db->get('t_inventory');
		$result = $query->row_array();
		return intval($result['qty_onhand']);
	}	
	public function total_style(){
		$this->db->select('style_code');
		$this->db->distinct('style_code');
		$query = $this->db->get('t_inventory_style');
		return $query->num_rows();
	}	
	public function total_new_order(){
		$this->db->select('*');
		$this->db->where('doc_status','ACTIVE');
		$query = $this->db->get('t_sales_order_header');
		return $query->num_rows();
	}	
	public function total_collection(){
		$this->db->select('*');
		$this->db->where('doc_status','ACTIVE');
		//$this->db->where_not_in('financial_status','PAID');
		$query = $this->db->get('t_sales_invoice_header');
		return $query->num_rows();
	}
	
	public function total_purchase(){
		$this->db->select('*');
		$this->db->where('doc_status','ACTIVE');
		$query = $this->db->get('t_purchase_order_header');
		return $query->num_rows();
	}	
	public function total_business(){
		$this->db->select('*');
		$this->db->where('doc_status','ACTIVE');
		$query = $this->db->get('t_sales_order_header');
		return $query->num_rows();
	}	
	public function total_warehouse(){
		$this->db->select('*');
		$this->db->where('is_warehouse','1');
		$query = $this->db->get('t_site_location');
		return $query->num_rows();
	}	
	public function total_consignment(){
		return $this -> db -> count_all('t_business');
	}
	
	
	
	/////////////////////
	
	public function total_vendor(){
		$this->db->select('*');
		$this->db->where('is_vendor','1');
		$query = $this->db->get('t_business');
		return $query->num_rows();
	}
	public function total_customer(){
		$this->db->select('*');
		$this->db->where('is_customer','1');
		$query = $this->db->get('t_business');
		return $query->num_rows();		
	}
	
}