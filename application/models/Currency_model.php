<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Currency_model extends CI_Model {

	/**
	*
	* get()
	* Get currency based on ID if give, or all if not
	*
	*/
	public function get($code = FALSE){
		if($code) $this->db->where('curr_code', $code);
		return ($code) ? $this->db->get('t_currency')->row_array() : $this->db->get('t_currency')->result_array();
	}

	/**
	*
	* currency_add($data)
	* Add new currency
	*
	*/
	public function currency_add($data)
	{
		if(!count($data))return false;

		if ($this->db->insert('t_currency', $data)){
			return $this->db->insert_id();
		} else {
			return false;
		}

	}

	/**
	*
	* currency_edit($data)
	* Edit currency
	*
	*/
	public function currency_edit($code,$data)
	{
		if(!count($data))return false;
		$this->db->where('curr_code', $code);
		return $this->db->update('t_currency_rate', $data);
	}

	/**
	 * rate_add($data)
	 */
	public function rate_add($data)
	{
		if(!count($data))return false;
		if($this->db->insert("t_currency_rate",$data)){
			return $this->db->insert_id();			
		}else{
			return false;
		}
	}

	public function get_rate($from,$to,$date=NULL)
	{
		$this->db->select("value");
		$this->db->from("t_currency_rate");
		$this->db->where("curr_from",$from);
		$this->db->where("curr_to",$to);
		$this->db->order_by("effective_date","desc");
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}

	public function get_rate_from($curr)
	{
		$this->db->select("*");
		$this->db->from("t_currency_rate");
		$this->db->join("t_currency","t_currency.curr_code = t_currency_rate.curr_from");
		$this->db->where("t_currency_rate.curr_from",$curr);
		return $this->db->get()->result_array();
	}

	public function get_rate_to($curr)
	{
		$this->db->select("*");
		$this->db->from("t_currency_rate");
		$this->db->join("t_currency","t_currency.curr_code = t_currency_rate.curr_from");
		$this->db->where("t_currency_rate.curr_to",$curr);
		return $this->db->get()->result_array();
	}
}
/* End of file Currency_model.php */
/* Location: ./application/models/Currency_model.php */