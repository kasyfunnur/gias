<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
*
* POS Model to separate all calls related to POS
*
**/

class Pos_model extends CI_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    /**
    *
    * Get cashier & site information
    *
    **/
    public function get_cashier_info($cashier_id = NULL){
    	$this->db->select('c.cashier_id, c.cashier_code, c.is_closed, l.*');
    	$this->db->from('t_site_cashier c ');
    	$this->db->join('t_site_location l', 'l.location_id = c.location_id');
    	if(is_numeric($cashier_id)) {
    		$this->db->where('c.cashier_id', $cashier_id);
    	}

    	return (is_numeric($cashier_id)) ? $this->db->get()->row_array() : $this->db->get()->result_array();
    }

    /**
    *
    * toggle open close cashier
    *
    **/
    public function toggle_cashier_status($cashier_id = NULL)
    {
    	if(is_numeric($cashier_id)) {
    		$this->db->where('cashier_id', $cashier_id);
    	}
    	$this -> db -> set('is_closed','Case when is_closed = 1 then 0 else 1 end', false);
    	return $this->db->update('t_site_cashier');

    }
    

    /**
    *
    * create_transaction()
    * create new POS transaction
    *
    **/
    public function create_transaction($cashier, $items, $payments)
    {
    	$this->db->trans_start();

    		// insert header
			$this->db->insert('t_sales_pos', $cashier);


			// get trx_id from the insert header
			$trx_id = $this->db->insert_id();

			// update trx_id in items array
			for ($i=0; $i < count($items); $i++) {
				$items[$i]['trx_id'] = $trx_id;
			};

			// insert items detail
			$this->db->insert_batch('t_sales_pos_item', $items);

			// update trx_id in payments array
			for ($i=0; $i < count($payments); $i++) {
				$payments[$i]['trx_id'] = $trx_id;

                // if voucher, invalidate it
                if($payments[$i]['method'] == 'voucher'){
                    $this -> use_voucher($payments[$i]['identifier_code']);
                }
			};

			// insert payments
			$this->db->insert_batch('t_sales_pos_payment', $payments);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return false;
		} else
			return $trx_id;
    }

    private function use_voucher($voucher)
    {
        $this->db->where('voucher_number', $voucher);
        $updatedata = array('used' => true);
        $this->db->update('t_credit_voucher_detail', $updatedata);
    }
    /**
    *
    * get_cashier_transactions()
    * return all the cashier sales given the cashier id
    */
    public function get_cashier_transactions($cashier_id = NULL, $filters = NULL)
    {
        if(is_numeric($cashier_id)){
            $this->db->where('cashier_id', $cashier_id);
        }

        if(count($filters)){
            // filters data example : array("posted = 0","trx_datetime < '2015-01-01'")
            foreach ($filters as $option) {
                $this->db->where($option, NULL, FALSE);
            }
        }
        $this->db->from('v_pos_transactions');
        // location_id, location_code, location_name, cashier_id, cashier_code, emp_no, input_by, input_date, sales_code, sales_amount, posted

        return $this -> db -> get() -> result_array();
    }

    /**
    *
    * get_cashier_transactions()
    * return all the cashier sales given the cashier id
    */
    public function get_cashier_transactions_items($cashier_id = NULL, $date = NULL)
    {
        if(is_numeric($cashier_id)){
            $this->db->where('cashier_id', $cashier_id);
        }

        if ($date == NULL){
            $date = date('Y-m-d');
        }

        $this -> db -> where ("pos_date = '{$date}'", NULL, FALSE);
        
        $this->db->from('v_pos_transactions_items');
        // id, trx_id, trx_code, input_datetime, pos_date, cashier_id, cashier_code, location_id, location_code, location_name, line_id, item_id, item_code, item_name, item_qty, move_item_qty, item_price, item_total_price, include_tax, tax_value, sales_value

        return $this -> db -> get() -> result_array();
    }

    /**
    *
    * close_cashier($cashier_id)
    * close and post (journal + inventory) for the given cashier number
    *
    **/
    public function close_cashier($cashier_id, $date = NULL)
    {
        if (is_numeric($cashier_id)){
            // check cashier info for the current status

            // reject close process if cashier is closed already
                $cashier = $this -> get_cashier_info($cashier_id);
                if ($cashier['is_closed'] == 1){
                    $result = array(
                        'success'   => false,
                        'message'   => 'Cashier is already closed'
                    );

                    return $result;
                }


            // validate the date argument
                if($date == NULL){
                    $date = date('Y-m-d');
                }

            // start transaction
                $this->db->trans_start(); // set trans_start(TRUE) for test mode

            // update the close flag of the cashier
                $this -> toggle_cashier_status($cashier_id);

            // update posted flag in sales for that day
                $updatedata = array('posted'=>true);
                $this->db->where("DATE(trx_datetime) = '{$date}' ");
                $this->db->update('t_sales_pos',$updatedata);

            // clear query builder, for consecutive queries
                //$this->db->reset_query();

            // calculate the total sales amount
                $total_sales = $this->db->select_sum('trx_sales_amount','sales_amount')->where("DATE(trx_datetime) = '{$date}' ")->get('t_sales_pos')->row_array();
                $total_sales = $total_sales['sales_amount'];

            // Inventory Transaction
                $this->load->model('inventory_model');
                $items = $this->get_cashier_transactions_items($cashier_id, $date);
                // id, trx_id, trx_code, input_datetime, pos_date, cashier_id, cashier_code, location_id, location_code, location_name, line_id, item_id, item_code, item_name, item_qty, move_item_qty, item_price, item_total_price, include_tax, tax_value, sales_value
                $item_array = array();
                
                $docnum = 'POS' . $cashier['location_id'] . $cashier['cashier_id'] . date('Ymd',strtotime($date));
                
                $total_cogs = 0;
                $total_sales = 0;
                $total_tax_payables = 0;

                foreach ($items as $item) {
                    // doc_type, doc_num, doc_date, item_id, item_qty, item_value, total_value, whse_id, trx_status

                    $item_cost = $this->inventory_model->get_cost($item['item_id'], $item['location_id'], $item['move_item_qty']);

                    $total_cogs += $item_cost;
                    $total_sales += $item['sales_value'];
                    $total_tax_payables += $item['tax_value'];

                    array_push($item_array, array(
                            'doc_type'      => 'SAL',
                            'doc_num'       => $docnum,
                            'doc_date'      => $date,
                            'item_id'       => $item['item_id'],
                            'item_qty'      => $item['move_item_qty'],
                            'item_value'    => $item_cost,
                            'total_value'   => $item_cost * $item['move_item_qty'],
                            'whse_id'       => $item['location_id'],
                            'trx_status'    => 'ORIGINAL'
                        ));
                }
                $this -> inventory_model -> add_trx($item_array);

            // Journal
                /* something like this :
                COGS    275,000
                Inventory   275,000

                CASH    500,000
                    Sales   400,000
                    VAT OUT 100,000
                */
                
                $this->load->model('journal_model');
                $this->load->model('document_model');
                $this->load->model('link_account_model');

                $journal['code'] = $this->document_model->useDocSeries('JRNL');

                // Header
                // journal_id, journal_code, posting_date, document_date, journal_memo, journal_type, journal_ref1, journal_ref2, journal_ref3, create_by, create_dt
                

                $journal['header'] = array(
                       'journal_code'   => $journal['code'],
                       'posting_date'   => $date,
                       'document_date'  => $date,
                       'journal_memo'   => 'Sales by POS',
                       'journal_type'   => 'SAL',
                       'journal_ref1'   => $docnum,
                       'create_by'      => $this->ion_auth->user()->row()->id,
                       'create_dt'      => date('Y-m-d H:i:s')
                    );

                // Detail
                //journal_id, journal_line, account_id, journal_dr, journal_cr, journal_cc, journal_curr
                $journal['detail'] = array();

                // Debit COGS
                $cogs_acc = $this -> link_account_model -> get_account('sales.cogs');
                if(!$cogs_acc){
                    show_error('Unable to get Link Account for COGS (code: `sales.cogs`)');
                    exit();
                }
                
                $journal['detail'][0] = array(
                        'journal_line'  => 1,
                        'account_id'    => $cogs_acc,
                        'journal_dr'    => $total_cogs,
                        'journal_cr'    => 0,
                        'journal_cc'    => 0,
                        'journal_curr'  => 'IDR'
                    );

                // Credit Inventory
                $inventory_acc = $this -> link_account_model -> get_account('inventory.asset');
                if(!$inventory_acc){
                    show_error('Unable to get Link Account for Inventory Asset (code: `inventory.asset`)');
                    exit();
                }
                
                $journal['detail'][1] = array(
                        'journal_line'  => 2,
                        'account_id'    => $inventory_acc,
                        'journal_dr'    => 0,
                        'journal_cr'    => $total_cogs,
                        'journal_cc'    => 0,
                        'journal_curr'  => 'IDR'
                    );

                // Debit Cash at Site Location (+ EDC Bank if any)
                $cash_acc = $this -> link_account_model -> get_account('sales.cash');
                if(!$cash_acc){
                    show_error('Unable to get Link Account for Cash Account (code: `sales.cash`)');
                    exit();
                }
                
                $journal['detail'][2] = array(
                        'journal_line'  => 3,
                        'account_id'    => $cash_acc,
                        'journal_dr'    => $total_sales + $total_tax_payables, // untuk sementara tambahkan dari sales + vat out, belum check ke cash payment dan credit card, dll.
                        'journal_cr'    => 0,
                        'journal_cc'    => 0,
                        'journal_curr'  => 'IDR'
                    );

                // Credit Sales
                $sales_rev_acc = $this -> link_account_model -> get_account('sales.revenue');
                if(!$sales_rev_acc){
                    show_error('Unable to get Link Account for Revenue (code: `sales.revenue`)');
                    exit();
                }
                
                $journal['detail'][3] = array(
                        'journal_line'  => 4,
                        'account_id'    => $sales_rev_acc,
                        'journal_dr'    => 0,
                        'journal_cr'    => $total_sales,
                        'journal_cc'    => 0,
                        'journal_curr'  => 'IDR'
                    );

                // Credit VAT OUT
                $vat_out_acc = $this -> link_account_model -> get_account('sales.vat_out');
                if(!$vat_out_acc){
                    show_error('Unable to get Link Account for Revenue (code: `sales.vat_out`)');
                    exit();
                }
                
                $journal['detail'][4] = array(
                        'journal_line'  => 5,
                        'account_id'    => $vat_out_acc,
                        'journal_dr'    => 0,
                        'journal_cr'    => $total_tax_payables,
                        'journal_cc'    => 0,
                        'journal_curr'  => 'IDR'
                    );

                // INSERT ALL JOURNAL
                $data = array(
                    't_account_journal_header' => $journal['header'],
                    't_account_journal_detail' => $journal['detail'],
                    );
                $this -> journal_model -> add($data);



            // complete transaction
                $this->db->trans_complete();


            // error handling for transaction
                if ($this->db->trans_status() === FALSE)
                {
                    $result = array(
                        'success'   => false,
                        'message'   => 'Error: unable to close cashier'
                    );
                } else {
                    $result = array(
                        'success'   => true,
                        'message'   => 'Cashier is closed'
                    );
                }

            // return the result
                return $result;

        } else {
            $result = array(
                        'success'   => false,
                        'message'   => 'Error: unable to determine cashier to be closed',
                        'data'      => null
                    );

            return $result;
        }
    }
    
    /**
    *
    * Get payment details in a day
    *
    **/
    public function get_payment_summary($date = null){
        // validate the date argument
        if($date == NULL){
            $date = date('Y-m-d');
        }
        $this->db->select('p.method, sum(p.amount) as amount');
        $this->db->from('t_sales_pos s');
        $this->db->join('t_sales_pos_payment p', 'p.trx_id = s.trx_id', 'left');
        $this->db->where("DATE(s.trx_datetime) = '{$date}' ");
        $this->db->group_by('p.method');

        return $this->db->get()->result_array();
    }

    /**
    *
    * get_pos_data($trx_id)
    * get saved pos data (for printing)
    */
    public function get_pos_data($trx_id)
    {
    	$data['header'] = $this->db->get_where('t_sales_pos', array('trx_id'=>$trx_id),1)->row_array();
    	$data['items'] = $this->db->select('pos_item.*, item.item_code, item.item_name')->join('t_inventory item','item.item_id = pos_item.item_id','left')->get_where('t_sales_pos_item pos_item', array('pos_item.trx_id'=>$trx_id))->result_array();
    	$data['payments'] = $this->db->get_where('t_sales_pos_payment', array('trx_id'=>$trx_id))->result_array();

    	return $data;
    }
    
    

    /**
    *
    * get_item
    * return item data required for POS module
    *
    **/
    public function get_item($itemcode)
    {
    	if(trim($itemcode) != ''){
    		$this -> db -> select ('item_id, item_code, item_name, item_sell_price');
    		$this -> db -> from   ('t_inventory');
    		$this -> db -> where  ('item_code', $itemcode);
    		return $this -> db -> get() -> row_array();
    	} else {
    		return false;
    	}
    }

    /**
    *
    * get voucher
    *
    **/
    public function get_voucher($code)
    {
        if(trim($code) != ''){
            $this -> db -> select ('d.*, v.value, v.active_date, v.expiry_date');
            $this -> db -> from   ('t_credit_voucher_detail d');
            $this->db->join('t_credit_voucher v', 'v.id = d.voucher_id', 'left');
            $this -> db -> where  ('d.voucher_number', $code);
            return $this -> db -> get() -> row_array();
        } else {
            return false;
        }
    }
    

    /**
    *
    * get_promo
    * return promotion data required for POS module
    *
    **/
    public function get_promo($promocode)
    {
    	if(trim($promocode) != ''){
    		// temporarily create hard coded promotional code

    		switch ($promocode) {
    			case 'DSC10':
    				$promo = array ('type'=>'percent', 'value' => 10);
    				break;

    			case 'DSC20':
    				$promo = array ('type'=>'percent', 'value' => 20);
    				break;

    			case 'DSC50':
    				$promo = array ('type'=>'percent', 'value' => 50);
    				break;

    			case 'VAL10K':
    				$promo = array ('type'=>'value', 'value' => 10000);
    				break;

    			case 'VAL100K':
    				$promo = array ('type'=>'value', 'value' => 100000);
    				break;

    			case 'VAL250K':
    				$promo = array ('type'=>'value', 'value' => 250000);
    				break;

    			default:
    				$promo = array();
    				break;
    		}

    		return $promo;
    	} else {
    		return false;
    	}
    }
}

/* End of file Pos_model.php */
/* Location: ./application/models/Pos_model.php */