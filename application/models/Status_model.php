<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class status_model extends MY_Model {
    var $details;
	public $table;
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->table = '';
    }
	public function status_label($group,$status){
		$temp = $this->status_parsing($group,$status);
		return '<span class="label '.$temp->label.'">'.$temp->text.'</span>';
	}
	public function status_icon($group,$status){
		$temp = $this->status_parsing($group,$status);
		return 
			'<span class="label label-xs '.$temp->label.'" style="margin:2px">
        		<i class="fa '.$temp->fa.'"></i> '.$temp->text.'</span>';
	}
	public function status_parsing($group,$status){ //(logistic,received,label/icon)
		$status_attr = new STDClass();
		switch($group){
			case "logistic":
				$status_attr->fa	= 'fa-truck';
				break;
			case "billing":
				$status_attr->fa	= 'fa-envelope';
				break;
			case "financial":
				$status_attr->fa	= 'fa-dollar';
				break;
			default:
				$status_attr->fa	= 'fa-file-text';				
		}
		
		$status_attr->text	= $status;
		switch($status){
			//approval
			case "partially approved":
				$status_attr->label = 'label-warning';
				break;
			case "approved":
				$status_attr->label = 'label-success';
				break;
			case "rejected":
				$status_attr->label = 'label-danger';
				break;
			case "revise":
				$status_attr->label = 'label-info';
				break;
			
			//document
			case "DRAFT":
				$status_attr->label = 'label-warning';
				break;
			case "AWAITING":
				$status_attr->label = 'label-info';
				break;
			case "IN REVIEW":
				$status_attr->label = 'label-info';
				break;
			case "ACTIVE":
				$status_attr->label = 'label-warning';
				break;
			case "CANCELLED":
				$status_attr->label = 'label-danger';
				break;			
			case "CLOSED":
				$status_attr->label = 'label-success';
				break;
				
			//logistic
			case "PENDING":
				$status_attr->label = 'label-danger';
				break;
			case "PARTIAL":
				$status_attr->label = 'label-warning';
				break;
			case "RECEIVED":
				$status_attr->label = 'label-success';
				break;
			case "DELIVERED":
				$status_attr->label = 'label-success';
				break;
				
			//billing
			case "UNBILLED":
				$status_attr->label = 'label-danger';
				break;
			case "BILLED":
				$status_attr->label = 'label-success';
				break;
				
			//financial
			case "UNPAID":
				$status_attr->label = 'label-danger';
				break;			
			case "PARTIALLY PAID":
				$status_attr->label = 'label-warning';
				break;
			case "PAID":
				$status_attr->label = 'label-success';
				break;
			case "OVERDUE":
				$status_attr->label = 'label-danger';
				break;
			default:
				$status_attr->label = 'label-info';
		}
		return $status_attr;
	}
		
	
}