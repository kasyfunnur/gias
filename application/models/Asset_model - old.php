<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asset_model extends CI_Model{
	var $sql_details = array('user' => 'altus_admin','pass' => 'LyWkFQNjMH3ItafaDFsr','db'   => 'erp_farisfab','host' => 'localhost');
	public function __construct(){
		parent::__construct();
		log_message("debug", "Asset helper model class initiated");

		}
		
	public function add_new_asset($data){
		$group_id = $data['group_id'];
		$query_data = array(
  		'asset_serial' 		=> (isset($data['serial']))?($data['serial']):uniqid(),
		'asset_name' 		=> (isset($data['name']))?($data['name']):'',
  		'asset_group_id' 	=> (isset($data['group_id']))?($data['group_id']):'',
		'asset_description' => (isset($data['description']))?($data['description']):'',
		'date_acquired' 	=> (isset($data['date_acquired']))?date('Y-m-d', strtotime(str_replace('/','-',$data['date_acquired'])))
		:date('Y-m-d')
		,
		'asset_status' 		=> (isset($data['status']))?($data['status']):'',
		'document_origin' 	=> (isset($data['document_origin']))?($data['document_origin']):'',
		'document_line' 	=> (isset($data['document_line']))?($data['document_line']):'',
		'asset_currency' 	=> (isset($data['currency']))?($data['currency']):'',
		'asset_location_id' => (isset($data['location_id']))?($data['location_id']):'',
		'asset_note'		=> (isset($data['note']))?($data['note']):'',
		'asset_value'		=> (isset($data['value']))?($data['value']):'',
		'custom_field_1'	=> (isset($data['custom_field'][$group_id][1]))?$data['custom_field'][$group_id][1]:NULL,
		'custom_field_2'	=> (isset($data['custom_field'][$group_id][2]))?$data['custom_field'][$group_id][2]:NULL,
		'custom_field_3'	=> (isset($data['custom_field'][$group_id][3]))?$data['custom_field'][$group_id][3]:NULL,
		'custom_field_4'	=> (isset($data['custom_field'][$group_id][4]))?$data['custom_field'][$group_id][4]:NULL,
		'custom_field_5'	=> (isset($data['custom_field'][$group_id][5]))?$data['custom_field'][$group_id][5]:NULL,
		'custom_field_6'	=> (isset($data['custom_field'][$group_id][6]))?$data['custom_field'][$group_id][6]:NULL,
		'custom_field_7'	=> (isset($data['custom_field'][$group_id][7]))?$data['custom_field'][$group_id][7]:NULL,
		'custom_field_8'	=> (isset($data['custom_field'][$group_id][8]))?$data['custom_field'][$group_id][8]:NULL,
		'custom_field_9'	=> (isset($data['custom_field'][$group_id][9]))?$data['custom_field'][$group_id][9]:NULL,
		'custom_field_10'	=> (isset($data['custom_field'][$group_id][10]))?$data['custom_field'][$group_id][10]:NULL
		);
		return ($this -> db -> insert('t_asset', $query_data)) ? "asset_added" : "asset_add_failed" ;
		}
	
	public function add_new_group($data){
		$query_data = array(
			'group_name' 		=> $data['name'],
			'group_description'	=> $data['description'],
			'group_tag'			=> $data['tag']//,
			/*'custom_field_1'	=> (isset($data['custom_field'][1]))?$data['custom_field'][1]:NULL,
			'custom_field_2'	=> (isset($data['custom_field'][2]))?$data['custom_field'][2]:NULL,
			'custom_field_3'	=> (isset($data['custom_field'][3]))?$data['custom_field'][3]:NULL,
			'custom_field_4'	=> (isset($data['custom_field'][4]))?$data['custom_field'][4]:NULL,
			'custom_field_5'	=> (isset($data['custom_field'][5]))?$data['custom_field'][5]:NULL,
			'custom_field_6'	=> (isset($data['custom_field'][6]))?$data['custom_field'][6]:NULL,
			'custom_field_7'	=> (isset($data['custom_field'][7]))?$data['custom_field'][7]:NULL,
			'custom_field_8'	=> (isset($data['custom_field'][8]))?$data['custom_field'][8]:NULL,
			'custom_field_9'	=> (isset($data['custom_field'][9]))?$data['custom_field'][9]:NULL,
			'custom_field_10'	=> (isset($data['custom_field'][10]))?$data['custom_field'][10]:NULL,
			'custom_type_1'		=> (isset($data['custom_type'][1]))?$data['custom_type'][1]:NULL,
			'custom_type_2'		=> (isset($data['custom_type'][2]))?$data['custom_type'][2]:NULL,
			'custom_type_3'		=> (isset($data['custom_type'][3]))?$data['custom_type'][3]:NULL,
			'custom_type_4'		=> (isset($data['custom_type'][4]))?$data['custom_type'][4]:NULL,
			'custom_type_5'		=> (isset($data['custom_type'][5]))?$data['custom_type'][5]:NULL,
			'custom_type_6'		=> (isset($data['custom_type'][6]))?$data['custom_type'][6]:NULL,
			'custom_type_7'		=> (isset($data['custom_type'][7]))?$data['custom_type'][7]:NULL,
			'custom_type_8'		=> (isset($data['custom_type'][8]))?$data['custom_type'][8]:NULL,
			'custom_type_9'		=> (isset($data['custom_type'][9]))?$data['custom_type'][9]:NULL,
			'custom_type_10'	=> (isset($data['custom_type'][10]))?$data['custom_type'][10]:NULL*/
		);
		return ($this -> db -> insert('t_asset_group', $query_data)) ? "asset_group_added" : "asset_group_add_failed" ;
	}
	public function add_new_sub_group($data){
		$query_data = array(
			'group_parent_id'	=> $data['selGroupParent'],
			'group_name' 		=> $data['name'],
			'group_description'	=> $data['description'],
			'group_tag'			=> $data['tag']
		);
		return ($this -> db -> insert('t_asset_group', $query_data)) ? "asset_group_added" : "asset_group_add_failed" ;
	}
	
	public function edit_asset_group($data){
		$query_data = array(
			'group_name'		=> $data['name'],
			'group_description'	=> $data['description'],
			'group_tag'			=> $data['tag'],
			'custom_field_1'	=> (isset($data['custom_field'][1]))?$data['custom_field'][1]:NULL,
			'custom_field_2'	=> (isset($data['custom_field'][2]))?$data['custom_field'][2]:NULL,
			'custom_field_3'	=> (isset($data['custom_field'][3]))?$data['custom_field'][3]:NULL,
			'custom_field_4'	=> (isset($data['custom_field'][4]))?$data['custom_field'][4]:NULL,
			'custom_field_5'	=> (isset($data['custom_field'][5]))?$data['custom_field'][5]:NULL,
			'custom_field_6'	=> (isset($data['custom_field'][6]))?$data['custom_field'][6]:NULL,
			'custom_field_7'	=> (isset($data['custom_field'][7]))?$data['custom_field'][7]:NULL,
			'custom_field_8'	=> (isset($data['custom_field'][8]))?$data['custom_field'][8]:NULL,
			'custom_field_9'	=> (isset($data['custom_field'][9]))?$data['custom_field'][9]:NULL,
			'custom_field_10'	=> (isset($data['custom_field'][10]))?$data['custom_field'][10]:NULL,
			'custom_type_1'		=> (isset($data['custom_field_type'][1]))?$data['custom_field_type'][1]:NULL,
			'custom_type_2'		=> (isset($data['custom_field_type'][2]))?$data['custom_field_type'][2]:NULL,
			'custom_type_3'		=> (isset($data['custom_field_type'][3]))?$data['custom_field_type'][3]:NULL,
			'custom_type_4'		=> (isset($data['custom_field_type'][4]))?$data['custom_field_type'][4]:NULL,
			'custom_type_5'		=> (isset($data['custom_field_type'][5]))?$data['custom_field_type'][5]:NULL,
			'custom_type_6'		=> (isset($data['custom_field_type'][6]))?$data['custom_field_type'][6]:NULL,
			'custom_type_7'		=> (isset($data['custom_field_type'][7]))?$data['custom_field_type'][7]:NULL,
			'custom_type_8'		=> (isset($data['custom_field_type'][8]))?$data['custom_field_type'][8]:NULL,
			'custom_type_9'		=> (isset($data['custom_field_type'][9]))?$data['custom_field_type'][9]:NULL,
			'custom_type_10'	=> (isset($data['custom_field_type'][10]))?$data['custom_field_type'][10]:NULL
		);
		$this->db->where('group_id',$data['group_id']);
		return ($this->db->update('t_asset_group',$query_data)) ? "asset_group_updated" : "asset_group_updated_failed";
	}
	
	public function asset_track($id){
		if ($id) $this -> db -> where ('asset_id',$id);
		$query = $this -> db -> get('t_asset_history');
		return  ($id) ? $query -> row_array() : $query ->result_array();
	}
	
	public function add_usage_request($data){
		
		$document_number = get_ref_number('asset_request');
		
		$this -> db -> trans_begin();
		
		// approval
		$this -> load-> helper ('approval');
		
		
		$query_data = array(
  		'document_ref' 		=> $document_number,
		'location_id'		=> $this -> session -> userdata ('user_location_id'),
		'requested_by'		=> $this -> session -> userdata ('user_id'),
		'requested_for'		=> $data['for'],
		'date_from'			=> date('Y-m-d', strtotime(str_replace('/','-',$data['date_from']))),
		'date_to'			=> date('Y-m-d', strtotime(str_replace('/','-',$data['date_to']))),
		'note'				=> $data['note'],
		'request_status'	=> 1 
		);
				
		$asset_requested = array_filter(array_map('trim',explode(";",$data['asset_requested'])));
		
		$this -> db -> insert('t_asset_request_header', $query_data);
		$insert_id = $this -> db -> insert_id();
		
		foreach ($asset_requested as $asset){
			$asset_data = array('request_id' => $insert_id, 'asset_id' =>$this -> get_asset_id($asset));
			$this -> db -> insert ('t_asset_request_detail',$asset_data);
			}
		
		$approval_status = requestApproval($this->session->userdata('user_id'), 'AR', $document_number);	
		
		$this -> db -> update ('t_asset_request_header', array("request_status" => $approval_status), array ('document_ref' => $document_number));
		
		if ($this->db->trans_status() === FALSE){
    		$this->db->trans_rollback();
			return "asset_request_failed";
			}
		else{
    		$this->db->trans_commit();
			return "asset_request_added";
			}
		}
	
	// approve asset usage request
	public function approve($doc_number, $approve_action)
	{
		$this->db->where('document_ref',$doc_number);
		return $this->db->update('t_asset_request_header',array('request_status'=>$approve_action));
	}
	public function ajax_asset_list($return = "datatable_array"){
		$table 		= 'v_asset_datatable';
		$primaryKey = 'asset_id';
		$columns = array(
			array( 'db' => 'asset_serial'	,'dt' => 0 ),
			array( 'db' => 'asset_name'		,'dt' => 1 ),
			array( 'db' => 'group_name'		,'dt' => 2 ),
			array( 'db' => 'location_name'	,'dt' => 3 ),
			array( 'db' => 'asset_value'	,'dt' => 4 ),
			array( 'db' => 'asset_status'	,'dt' => 5 )
			
			);
		$this -> load -> library('ssp');
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns);
		}
	
	public function ajax_asset_history($return = "datatable_array",$asset_id){
		//var_dump($_GET);
		//$_GET = "";
		$table 		= 't_asset_history';
		$primaryKey = 'asset_history_id';							
		$columns = array(
			array( 'db' => 'asset_history_date'		,'dt' => 0 ),
			array( 'db' => 'asset_group'			,'dt' => 1 ),
			array( 'db' => 'asset_name'				,'dt' => 2 ),
			array( 'db' => 'whse_id'				,'dt' => 3 ),
			array( 'db' => 'asset_status'			,'dt' => 4 ),
			array( 'db' => 'asset_group'			,'dt' => 5 )//,
			//array( 'db' => '(select group_concat(group_name) from t_asset_group where group_parent_id = group_id)'	,'dt' => 4 )
		);
		//$where = array('group_parent_id' => 0);
		$where = 'asset_id = '.$asset_id;
		$this -> load -> library('ssp');
		
		$datasource = SSP::sql_exec('altus_erp2',$this->sql_details,"select t_asset_history.*, t_asset_group.group_name, t_site_location.location_name from t_asset_history
inner join t_asset_group on t_asset_group.group_id = t_asset_history.asset_group
inner join t_site_location on t_site_location.location_id = t_asset_history.whse_id");


		//return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns, $where);
		return SSP::simple( $datasource, $this -> sql_details, $table, $primaryKey, $columns, $where);
	}
		
	public function ajax_group_list($return = "datatable_array"){
		$table 		= 't_asset_group';
		$primaryKey = 'group_id';
		$columns = array(
			array( 'db' => 'group_id'			,'dt' => 0 ),
			array( 'db' => 'group_name'			,'dt' => 1 ),
			array( 'db' => 'group_tag'			,'dt' => 2 ),
			array( 'db' => 'group_description'	,'dt' => 3 )//,
			//array( 'db' => '(select group_concat(group_name) from t_asset_group where group_parent_id = group_id)'	,'dt' => 4 )
		);
		//$where = array('group_parent_id' => 0);
		$where = ' group_parent_id = 0 ';
		$this -> load -> library('ssp');
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns, $where);
	}
		
	public function ajax_sub_group_list($return = "datatable_array",$group){
		$table		= 't_asset_group';
		$primaryKey	= 'group_id';
		$columns = array(
			//array( 'db' => 'id'					,'dt' => 'DT_RowId', 'formatter' => function( $d, $row ) { return 'row_'.$d;}),
			array( 'db' => 'group_id'			,'dt' => 0 ),
			array( 'db' => 'group_name'			,'dt' => 1 ),
			array( 'db' => 'group_tag'			,'dt' => 2 ),
			array( 'db' => 'group_description'	,'dt' => 3 )			
		);
		$where = ' group_parent_id = '.$group ;	
		$this -> load -> library('ssp');
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns, $where);
	}
	
		
	public function ajax_usage_request($return = "datatable_array"){
		$table 		 = 'v_asset_datatable';
		$primaryKey  = 'asset_id';
		$user_location_id = $this -> session -> userdata ('user_location_id');
		$where_query = "`location_id` =".$user_location_id;
		$columns = array(
			array( 'db' => 'asset_serial'	,'dt' => 0 ),
			array( 'db' => 'asset_name'		,'dt' => 1 ),
			array( 'db' => 'asset_status'	,'dt' => 2 ),
			array( 'db' => 'asset_serial'	,'dt' => 3 /*, 
			'formatter' => function ($d, $row) {
				return "<a href='javascript:void(0)'><button value='$d' onClick='add_asset(this.value)' class='btn btn-flat btn-info btn-sm btn-block'>Add</button></a>";
				}*/
			)
			);
		$this -> load -> library('ssp');
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns, $where_query);
		}	
		
	public function ajax_usage_list($return = "datatable_array",$own_request = TRUE){
		$table 		 = 'v_asset_request';
		$primaryKey  = 'request_id';
		$user_id = $this -> session -> userdata ('user_id');
		if($own_request)$where_query = "`requested_by` =".$user_id;
		else $where_query = '';
		$columns = array(
			array('db' => 'document_ref'	,'dt' => 0),
			array('db' => 'user_name'		,'dt' => 1),
			array('db' => 'date_from'		,'dt' => 2),
			array('db' => 'asset_name'		,'dt' => 3),
			array('db' => 'asset_serial'	,'dt' => 4),
			array('db' => 'note'			,'dt' => 5),
			array('db' => 'request_status'	,'dt' => 6/*,'formatter' => function ($d, $row){
				return strtoupper($d);
				}*/
			)
		);
		$this -> load -> library('ssp');
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns, $where_query);
		}
		
	function get_asset_group($group_id = FALSE){
		//$this -> db -> select ('group_id','group_name');
		if ($group_id) $this -> db -> where ('group_id',$group_id);
		$this->db->select("*,(select group_concat(group_name) from t_asset_group t1 where t1.group_parent_id = t0.group_id) as group_children ");
		$query = $this -> db -> get('t_asset_group');
		return  ($group_id) ? $query -> row_array() : $query ->result_array();
	}
	function get_asset_sub_group($group_id = FALSE){
		//$this -> db -> select ('group_id','group_name');
		if ($group_id) $this -> db -> where ('group_id',$group_id);
		//$this->db->select("*,(select group_concat(group_name) from t_asset_group t1 where t1.group_parent_id = t0.group_id) as group_children ");
		$query = $this -> db -> get('t_asset_group');
		return  ($group_id) ? $query -> row_array() : $query ->result_array();
	}
	
	function get_asset_sub_group_tag($group_id){
		$this->db->where('group_id', $group_id);
		$query1 = $this->db->get('t_asset_group');		
		$result1 = $query1->row_array();		
		$this->db->where('group_id', $result1['group_parent_id']);
		$query2 = $this->db->get('t_asset_group');		
		$result2 = $query2->row_array();		
		return $result2['group_tag'].$result1['group_tag'];
	}
	
	function get_asset_by_parent($parent_id){
		$this->db->where('group_parent_id',$parent_id);
		$query = $this->db->get('t_asset_group');		
		return $query->result_array();
	}
		
	function get_site_location($location_id = FALSE){
		if ($location_id) $this -> db -> where ('location_id',$location_id);
		$query = $this -> db -> get('t_site_location');
		return  ($location_id) ? $query -> row_array() : $query ->result_array();
		}
	function get_asset_all(){
		$this->db->select('*');
		$this->db->from('t_asset');
		$this->db->join('t_site_location','t_site_location.location_id = t_asset.asset_location_id','inner' );
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
		}
	function get_asset_info($id){
		$query  = $this -> db -> get_where ('t_asset',array('asset_id' => $id));
		$result = $query -> row_array();
		return $result;
		}
	function get_asset_id($asset_serial){
		$this   -> db -> select ('asset_id');
		$query  = $this -> db -> get_where ('t_asset',array('asset_serial' => $asset_serial));
		$result = $query -> row_array();
		return $result['asset_id'];
		}
		
	
}