<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report_model extends CI_Model{		
	public function __construct(){
		parent::__construct();
		log_message('debug', "Report helper model class initiated");
	}	
	public function get_report_header($report_code){
		$this->db->select("*");
		$this->db->where("report_code",$report_code);
		$query = $this->db->get('t_report');
		return $query->row_array();	
	}	
	public function get_report_config($report_code){
		$this->db->select("*");
		$this->db->from("t_report");
		$this->db->join("t_report_config","t_report_config.report_id = t_report.report_id","inner");
		$this->db->where("report_code",$report_code);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function stock_transaction($param){
		$param['sel_item']	= (is_array($param['sel_item'])) ? implode(',',$param['sel_item']) : $param['sel_item'];
		$param['sel_wh']	= (is_array($param['sel_wh'])) ? implode(',',$param['sel_wh']) : $param['sel_wh'];
		
		$query1 = sprintf("
			select * from t_inventory_transaction t0
			inner join t_inventory t1 on t0.item_id = t1.item_id
			inner join t_site_location t2 on t2.location_id = t0.whse_id
			where t0.item_id in (%s) 
			and t0.whse_id in (%s)
			and t0.doc_date >= '%s'
			and t0.doc_date <= '%s'
			and t0.item_qty <> 0
			order by t0.timestamp desc
		",$param['sel_item'],$param['sel_wh'][0],$param['date'][0],$param['date'][1]);
		return $this->db->query($query1)->result_array();
	}
	public function stock_balance($param){
		$param['sel_item']	= (is_array($param['sel_item'])) ? implode(',',$param['sel_item']) : $param['sel_item'];
		$param['sel_wh']	= (is_array($param['sel_wh'])) ? implode(',',$param['sel_wh']) : $param['sel_wh'];
		$query1 		= sprintf("
			select t0.item_id, t1.item_code, t1.item_name, t2.location_name, t0.whse_id, sum(item_qty) as 'balance_qty'
			from t_inventory_transaction t0
			inner join t_inventory t1 on t0.item_id = t1.item_id
			inner join t_site_location t2 on t2.location_id = t0.whse_id
			where t0.item_id in (%s) 
			and t0.whse_id in (%s)					
			and t0.doc_date >= '%s'
			and t0.doc_date <= '%s'
			group by t0.item_id, t0.whse_id
			order by t0.timestamp desc
		",$param['sel_item'],$param['sel_wh'],$param['date'][0],$param['date'][1]);
		return $this->db->query($query1)->result_array();
	}
	
	public function sales_transaction($param){
		$param['sel_item']	= (is_array($param['sel_item'])) ? implode(',',$param['sel_item']) : $param['sel_item'];
		$param['sel_cust']	= (is_array($param['sel_cust'])) ? implode(',',$param['sel_cust']) : $param['sel_cust'];
		$filter = ($param['sel_cust'] == 'all') ? "" :"and t0.buss_id in (".$param['sel_cust'].")";
		$query1 		= sprintf("
			select distinct t0.doc_num, t0.doc_status, t3.buss_name, t0.doc_dt, t0.doc_disc_amount, t0.doc_total
			from t_sales_invoice_header t0
			inner join t_sales_invoice_detail t1 on t0.doc_id = t1.doc_id			
			inner join t_business t3 on t3.buss_id = t0.buss_id
			left join t_inventory t2 on t1.item_id = t2.item_id
			where t0.doc_dt >= '%s'
			and t0.doc_dt <= '%s'
			and t2.item_id in (%s)
			%s
			and t0.doc_status in ('CLOSED','ACTIVE') 
			order by t0.doc_dt
		",$param['date'][0],$param['date'][1],$param['sel_item'],$filter);
		$query2 		= sprintf("
			select t0.doc_num, t0.doc_status, t3.buss_name, t0.doc_dt, 
				t2.item_name, t1.item_qty, t1.item_netprice, t1.item_total, t0.doc_disc_amount, t0.doc_total
			from t_sales_invoice_header t0
			inner join t_sales_invoice_detail t1 on t0.doc_id = t1.doc_id
			inner join t_inventory t2 on t1.item_id = t2.item_id
			inner join t_business t3 on t3.buss_id = t0.buss_id
			where t0.doc_dt >= '%s'
			and t0.doc_dt <= '%s'
			and t2.item_id in (%s)
			%s
			and t0.doc_status in ('CLOSED','ACTIVE') 
		",$param['date'][0],$param['date'][1],$param['sel_item'],$filter);
		
		$query3 		= sprintf("
			select buss_name from t_business where buss_id = %d		
		",$param['sel_cust']);

		$result['header'] = $this->db->query($query1)->result_array();
		$result['detail'] = $this->db->query($query2)->result_array();
		$result['buss'] = $this->db->query($query3)->result_array();
		//return $this->db->query($query1)->result_array();
		return $result;
	}
	
	
	public function sales_consignment($param){
		//$param['sel_item']	= (is_array($param['sel_item'])) ? implode(',',$param['sel_item']) : $param['sel_item'];
		$param['sel_cons']	= (is_array($param['sel_cons'])) ? implode(',',$param['sel_cons']) : $param['sel_cons'];
		$filter = ($param['sel_cons'] == 'all') ? "" :"and t0.buss_id in (".$param['sel_cons'].")";
		$query1 		= sprintf("
			select distinct t0.doc_num, t0.doc_status, t3.buss_name, t0.doc_dt, t0.doc_disc_amount, t0.doc_total
			from t_sales_order_header t0
			inner join t_sales_order_detail t1 on t0.doc_id = t1.doc_id			
			inner join t_business t3 on t3.buss_id = t0.buss_id
			left join t_inventory t2 on t1.item_id = t2.item_id
			where t0.doc_dt >= '%s'
			and t0.doc_dt <= '%s'
			and t0.is_cons = 1			
			%s
			and t0.doc_status = 'CLOSED'
			order by t0.doc_dt
		",$param['date'][0],$param['date'][1],$filter);
		$query2 		= sprintf("
			select t0.doc_num, t0.doc_status, t3.buss_name, t0.doc_dt, 
				t2.item_name, t1.item_qty_closed, t1.item_netprice, t1.item_total, t0.doc_disc_amount, t0.doc_total
			from t_sales_order_header t0
			inner join t_sales_order_detail t1 on t0.doc_id = t1.doc_id
			inner join t_inventory t2 on t1.item_id = t2.item_id
			inner join t_business t3 on t3.buss_id = t0.buss_id
			where t0.doc_dt >= '%s'
			and t0.doc_dt <= '%s'
			and t0.is_cons = 1
			%s
			and t0.doc_status = 'CLOSED'
		",$param['date'][0],$param['date'][1],$filter);
		
		$query3 		= sprintf("
			select buss_name from t_business where buss_id = %d		
		",$param['sel_cons']);

		$result['header'] = $this->db->query($query1)->result_array();
		$result['detail'] = $this->db->query($query2)->result_array();
		$result['buss'] = $this->db->query($query3)->result_array();
		return $result;
	}
}