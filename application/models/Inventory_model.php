<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory_model extends MY_Model{
	public $table;
	public function __construct(){
		parent::__construct();
		$this->load->model('location_model');
		$this->load->model('inventory_model');		
		$this->sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);
	}
	
	//AVER MASTER
	public function add($data){
		$this->load->model('warehouse_model');
		$this->db->insert('t_inventory',$data);
		$temp_id = $this->db->insert_id();
		$temp_wh = $this->warehouse_model->view();
		foreach($temp_wh as $i=>$wh){
			$this->warehouse_model->add_inv_wh(array('item_id'=>$temp_id,'location_id'=>$wh['location_id']));
		}
	}
	public function view($where=NULL,$order=NULL){
		$this->db->select("*");
		$this->db->from('t_inventory');
		if($where)$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function edit($data,$where){
		$this->db->update('t_inventory',$data,$where);
	}
	public function remove($where){

	}
	
	public function recalculate(){
		$this->db->query("call procUpdateQtyOnHand()");
	}

	//TRX
	public function add_trx($data){	
		if(count($data)>1){
			$this->db->insert_batch('t_inventory_transaction',$data);
		}else{
			$this->db->insert('t_inventory_transaction',$data[0]);
		}
		$this->recalculate();
	}
	public function view_trx($where){
		$this->db->select("*");
		$this->db->from('t_inventory_transaction');
		$this->db->where($where);
		$query=$this->db->get();
		return $query->result_array();
	}
	public function reverse_trx($where){
		//GET THE TRX BEING REVERSED
		$temp_data = $this->view_trx($where);
		$data_reverse = array('trx_status' => 'CLOSED');
		//SET THOSE DATA TO BE 'CLOSED'
		$this->db->update('t_inventory_transaction',$data_reverse,$where);
		unset($data[array_search('timestamp',$data)]);
		$data['item_qty'] = -$data['item_qty'];
		$data['trx_status'] = 'REVERSE';
		//INSERT NEW ONE AS 'REVERSED' AND OPPOSITE QTY
		$this->add_trx($data);
		$this->recalculate();
	}
	public function reverse_entry($doc){
		$this->db->where('doc_num',$doc);
		$this->db->where('trx_status','ORIGINAL');
		$query = $this->db->get('t_inventory_transaction');
		$temp = $query->result_array();
		
		for($x=0;$x<count($temp);$x++){
			$reverse_data = array(
				"doc_type"		=> $temp[$x]['doc_type'],
				"doc_date" 		=> $temp[$x]['doc_date'],
				"doc_num" 		=> $temp[$x]['doc_num'],
				"item_id" 		=> $temp[$x]['item_id'],
				"item_qty" 		=> $temp[$x]['item_qty'] * -1,
				"item_value"	=> $temp[$x]['item_value'],
				"total_value"	=> $temp[$x]['total_value'],
				"whse_id" 		=> $temp[$x]['whse_id'],
				"trx_status" 	=> 'REVERSE'
			);
			$this->db->insert('t_inventory_transaction',$reverse_data);
		}		
		$this->db->update('t_inventory_transaction',array("trx_status"=>"CLOSED"),array("doc_num"=>$doc,"trx_status"=>'ORIGINAL'));
		$this->db->query("call procUpdateQtyOnHand()");
	}
	

	//Transaction == same as //TRX above, but better de-coupling and more objective
	public function add_transaction($doc_code,$item_id,$qty,$value,$whse=null)
	{
		$total = $qty*$value;
		$this->db->set('doc_type','');
		$this->db->set('doc_num',$doc_code);
		$this->db->set('doc_date','');
		$this->db->set('item_id',$item_id);
		$this->db->set('item_qty',$qty);
		$this->db->set('value',$value);
		$this->db->set('total_value',$total);
		$this->db->set('whse',$whse_id);
		$this->db->insert('t_inventory_transaction');
		return $total;
	}

	///inventory
	function get_style_all(){
		$this->db->select("*");
		$this->db->from('t_inventory_style');
		$this->db->order_by('style_date','desc');
		$query = $this->db->get();
		return $query->result_array();
	}
	function get_style($code){
		$this->db->select('*');
		$this->db->from('t_inventory_style');
		$this->db->join('t_inventory_category','t_inventory_category.category_code = t_inventory_style.category_code','inner');
		$this->db->where('style_code',$code);
		$query = $this->db->get();		
		return $query -> result_array();
	}
	function get_group_all(){
		$this->db->select('item_g_id,group_label');
		$this->db->from('t_inventory_group');
		$this->db->where('group_on_web',1);
		$query = $this->db->get();		
		return $query -> result_array();
	}
	function get_category_all(){
		$this->db->select('category_id, category_code,category_name');
		$this->db->from('t_inventory_category');
		//$this->db->where('item_group',2);
		$query = $this->db->get();		
		return $query -> result_array();
	}

	function get_category($cat_id = null){
		if(is_numeric($cat_id)){
			$this->db->where('category_id', $cat_id);
		}
		$this->db->from('t_inventory_category');
		return (is_numeric($cat_id)) ? $this->db->get()->row_array() : $this->db->get()->result_array();
	}

	function add_category($data)
	{
		return $this->db->insert('t_inventory_category', $data);
	}

	function update_category($cat_id, $newdata)
	{
		$this->db->where('category_id', $cat_id);
		return $this->db->update('t_inventory_category', $newdata);
	}
	
	function get_warehouse_all(){
		$this->db->select("*");
		$this->db->from('t_site_location');
		$query = $this->db->get();		
		return $query -> result_array();	
	}

	function get_warehouse($whid){
		if (is_numeric($whid))
				$this -> db -> where ('location_id',$whid);

		$this->db->select("*");
		$this->db->from('t_site_location');	
		return (is_numeric($whid)) ? $this -> db -> get() -> row_array() : $this -> db -> get() -> result_array();
	}

	function update_warehouse($whid, $data)
	{
		if(is_numeric($whid) && count($data)){
			unset($data['location_id']);

			$data['is_active'] = (array_key_exists('is_active',$data)) ? true : false ;
			$data['is_sales'] = (array_key_exists('is_sales',$data)) ? true : false ;
			
			$this->db->where('location_id', $whid);
			if($this->db->update('t_site_location', $data))
				return true;
			else 
				return false;

		} else {
			return false;
		}
	}

	function get_stock_all(){
		$this->db->select('*');
		$this->db->from('t_inventory');
		//$this->db->where('item_style','Anne');
		//$this->db->limit(3);
		$query = $this->db->get();		
		return $query -> result_array();
	}
	function get_stock_by_vendor($vendor){
		$this->db->select('*');
		$this->db->from('t_inventory');
		$this->db->where('item_manufacturer_char',$vendor);
		$query = $this->db->get();		
		return $query -> result_array();
	}
	function get_stock_by_style($style){
		$this->db->select('*');
		$this->db->from('t_inventory');
		$this->db->where('item_style_char',$style);
		$query = $this->db->get();		
		return $query -> result_array();
	}
	function get_asset_info($id){
		$query  = $this -> db -> get_where ('t_inventory',array('item_id' => $id));
		$result = $query -> row_array();
		return $result;
	}
	function get_inventory_warehouse($id){
		$this->db->select('*');
		$this->db->from('t_inventory_warehouse');
		$this->db->join('t_site_location','t_site_location.location_id = t_inventory_warehouse.location_id','inner');
		$this->db->where('is_warehouse',1);
		$this->db->where('item_id',$id);		
		$query = $this->db->get();
		return $query -> result_array();
	}
	function get_inventory_transaction($type=NULL,$item_id=NULL,$whse_id=NULL,$date_from=NULL,$date_to=NULL){
		$this->db->select('t_inventory_transaction.*, T1.buss_name as "buss_name1", T2.buss_name as "buss_name2", t_site_location.*');
		$this->db->from('t_inventory_transaction');
		if($item_id != NULL){
			$this->db->where('item_id',$item_id);
		}
		if($whse_id != NULL){
			$this->db->where('whse_id',$whse_id);
		}
		if($type == "IN"){
			$this->db->where('item_qty >= 0');
		}else if($type == "OUT"){
			$this->db->where('item_qty <= 0');
		}
		$this->db->join('t_site_location','t_site_location.location_id = t_inventory_transaction.whse_id','inner');
		//////IF DELIVERY
		//if($type == "DEL"){
			$this->db->join('t_sales_delivery_header','t_sales_delivery_header.doc_num = t_inventory_transaction.doc_num','left outer');
			$this->db->join('t_business T1','T1.buss_id = t_sales_delivery_header.buss_id','left outer');
		//}
		//////IF RECEIPT
		//if($type == "RCV"){
			$this->db->join('t_purchase_receipt_header','t_purchase_receipt_header.doc_num = t_inventory_transaction.doc_num','left outer');
			$this->db->join('t_business T2','T2.buss_id = t_purchase_receipt_header.buss_id','left outer');
		//}		
		
		if($date_from != NULL){
			$this->db->where('');
		}
		if($date_to != NULL){
			$this->db->where('');
		}
		$this->db->order_by('timestamp','desc');
		$this->db->order_by('trans_id','desc');
		$query = $this->db->get();
		return $query -> result_array();
	}
	function get_stock_id($code){
		$this   -> db -> select ('item_id');
		$query  = $this -> db -> get_where ('t_inventory',array('item_code' => $code));
		$result = $query -> row_array();
		return $result['item_id'];
	}
	



	public function link_account($item_id,$type)
	{
		$this->db->select($type);
		$this->db->where("item_id",$item_id);
		$query = $this->db->get("t_inventory")->row_array();
		return $query[$type];
	}
	
	
	//ajax
	public function ajax_inventory_list(){
		$this->load->library('ssp');
		$join_query  = '';
		$where_query = '';
		$table 		= 'v_inventory';
		$primaryKey = 'item_id';
		$columns = array(
			array( 'db' => 'item_code' 		,'dt' => 0, 'formatter' => function ($d, $row){return "<a href='./detail/$d'>$d</a>";}),
			array( 'db' => 'item_name'		,'dt' => 1 ),
			array( 'db' => 'qty_onhand'		,'dt' => 2 ),
			array( 'db' => 'qty_tocome'		,'dt' => 3 ),
			array( 'db' => 'qty_togo'		,'dt' => 4 ),
			array( 'db' => 'qty_avail' 		,'dt' => 5 ),
			array( 'db' => 'item_status'	,'dt' => 6 )
		);
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns, $join_query, $where_query);
	}
	public function ajax_stock_conv(){
		$this->load->library('ssp');
		$join_query  = '';
		$where_query = '';
		$table 		= 't_stock_conv';
		$primaryKey = 'doc_no';
		$columns = array(
			array( 'db' => 'doc_no' 		,'dt' => 0, 'formatter' => function ($d, $row){return "<a href='./stock_conv_detail/$d'>$d</a>";}),
			array( 'db' => 'doc_date'		,'dt' => 1 ),
			array( 'db' => 'created_by'		,'dt' => 2 )
		);
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns, $join_query, $where_query);
	}
	public function ajax_style_list(){
		$this->load->library('ssp');
		$join_query  = '';
		$where_query = '';
		$table 		= 't_inventory_style';
		$primaryKey = 'style_id';
		$columns = array(
			array( 'db' => 'style_code' 	,'dt' => 0, 'formatter' => function ($d, $row){return "<a href='./style_detail/$d'>$d</a>";}),
			array( 'db' => 'style_name'		,'dt' => 1 ),
			array( 'db' => 'style_date'		,'dt' => 2 )
		);
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns, $join_query, $where_query);
	}
	public function ajax_warehouse_list(){
		$this->load->library('ssp');
		$join_query  = '';
		$where_query = 'is_warehouse = 1';
		$table 		= 't_site_location';
		$primaryKey = 'location_id';
		$columns = array(
			array( 'db' => 'location_id' 		,'dt' => 0, 'formatter' => function ($d, $row){return "<a href='". site_url('inventory/warehouse_detail')."/$d'>$d</a>";}),
			array( 'db' => 'location_name'		,'dt' => 1 ),
			array( 'db' => 'location_address'	,'dt' => 2 )
		);
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns, $join_query, $where_query);
	}
	public function ajax_inventory_style_list(){
		$this->load->library('ssp');
		$join_query  = '';
		$where_query = '';
		$table 		= 't_inventory';
		$primaryKey = 'item_id';
		$columns = array(
			array( 'db' => 'item_code' 		,'dt' => 0, 'formatter' => function ($d, $row){return "<a href='./detail/$d'>$d</a>";}),
			array( 'db' => 'item_name'		,'dt' => 1 ),
			array( 'db' => 'qty_onhand'		,'dt' => 2 ),
			array( 'db' => 'item_status'	,'dt' => 3 )
		);
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns, $join_query, $where_query);
	}	
	public function ajax_transfer_list(){
		$this->load->library('ssp');
		$join_query  = '';
		$where_query = '';
		$table 		= 't_inventory';
		$primaryKey = 'item_id';
		$columns = array(
			array( 'db' => 'item_code' 		,'dt' => 0 ),
			array( 'db' => 'item_name'		,'dt' => 1 ),
			array( 'db' => 'qty_onhand'		,'dt' => 2 ),
			array( 'db' => 'item_status'	,'dt' => 3 )
		);
		return SSP::simple( $_GET, $this -> sql_details, $table, $primaryKey, $columns, $join_query, $where_query);
	}	
	public function ajax_inventory_info($id=NULL){
		//$this->db->select('1 as `default_qty`, t_inventory.* ',FALSE);
		//$this->db->select(
		//$this->db->select('t_inventory.*');
		//$this->db->select("1 as default_qty, t_inventory.* ");
		if($id != NULL){
			$this->db->where_in('item_id',$id);
		}
		$query = $this->db->get('t_inventory');
		$result = $query -> result_array();
		return $result;
	}	
	public function ajax_style_info($id){
		$this->db->select("category_code");
		$this->db->where_in('style_code',$id);
		$query = $this->db->get('t_inventory_style');
		$result = $query -> row_array();
		return $result;
	}
	
	//typeahead
	public function ta_get(){
		$this->db->select("item_code");
		$query = $this->db->get('t_inventory');
		$result = $query->result_array();
		return $result;
	}

	/*=================================================
	=            Inventory Costing Section            =
	=================================================*/
	
	/**
	*
	* get_cost($itemid, $whid, $qty)
	* return the avg cost (HPP) of the item given
	**/
	public function get_cost($itemid, $whid, $qty){
		if(is_numeric($itemid) && is_numeric($whid) && is_numeric($qty)){
			//asume avg for now
			$this->db->select('item_id, sum(item_qty) as total_qty, sum(total_value) as total_value', false);
			$this->db->where('item_id', $itemid);
			$this->db->where('item_qty >', 0);
			$this->db->where('whse_id', $whid );
			$this->db->group_by('item_id');
			$result = $this->db->get('t_inventory_transaction',1)->row_array();
			$cost = (count($result)) ? $result['total_value'] / (($result['total_qty'] == 0)? 1: $result['total_qty']) : 0;
			return $cost;
		} else {
			return 0;
		}
	}
	
	
	/*-----  End of Inventory Costing Section  ------*/
	

	/**
	 * REPORT MODEL
	 */
	public function inventory_balance_report($filters){
		if($filters['date_from']){
			//$this->db->where('doc_dt >=', $filters['date_from']);
		}
		if($filters['date_to']){
			//$this->db->where('doc_dt <=', $filters['date_to']);
		}
		$this->db->from('v_itembalance');
		return $this->db->get()->result_array();
	}
	public function inventory_transaction_report($filters){
		if($filters['date_from']){
			//$this->db->where('doc_dt >=', $filters['date_from']);
		}
		if($filters['date_to']){
			//$this->db->where('doc_dt <=', $filters['date_to']);
		}
		$this->db->from('v_itemhistory');
		$this->db->order_by('doc_num','desc');
		
		return $this->db->get()->result_array();
	}
	public function inventory_warehouse_report($filters){
		if($filters['date_from']){
			//$this->db->where('doc_dt >=', $filters['date_from']);
		}
		if($filters['date_to']){
			//$this->db->where('doc_dt <=', $filters['date_to']);
		}
		$this->db->from('v_itemhistory');
		return $this->db->get()->result_array();
	}

	/*===================================
	=            uom section            =
	===================================*/
	public function get_uom($uom = null)
	{
		if(! empty($uom) )
			$this->db->where('uom', $uom);
		$this->db->order_by('uom', 'asc');
		return (! empty($uom) ) ? 
			$this->db->get('t_uom')->row_array() :
			$this->db->get('t_uom')->result_array() ;
	}
	
	
	/*-----  End of uom section  ------*/



	/*===================================
	=       stock conversion            =
	===================================*/

	public function add_stock_conversion($data){
		$this->db->insert('t_stock_conv',$data);
		$temp_id = $this->db->insert_id();
		
	}
	
	
}