<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Finance_model extends MY_Model{
	//var $sql_details = array('user' => 'altus_admin','pass' => 'LyWkFQNjMH3ItafaDFsr','db'   => 'erp_farisfab','host' => 'localhost');
	public $table;
	public function __construct(){
		parent::__construct();
		log_message("debug", "Asset helper model class initiated");
	}
	
	//LAVER
	public function list_all($where=NULL,$order=NULL)
	{
		$this->db->select("*");
		$this->db->from('t_payment_header');
		if($where)$this->db->where($where);
		return $this->db->get()->result_array();
	}
	public function add($data)
	{
		$this->db->insert('t_payment_header',$data['t_payment_header']);
		$temp_temp_id = $this->db->insert_id();
		$temp_id = $temp_temp_id;
		foreach($data['t_payment_detail'] as $detail){
			$detail['doc_id'] = $temp_id;
			$this->db->insert('t_payment_detail',$detail);

			$temp_piv = $this->db->select("*")->from("t_purchase_invoice_header")->where('doc_id',$detail['reff_id'])->get()->row_array();
			
			$current_paid = $temp_piv['doc_paid'];
			$new_paid = $current_paid + $detail['reff_to_pay'];
			if($new_paid >= $temp_piv['doc_total']){
				$paid_status = "PAID";
			} else { 
				$paid_status = "PARTIALLY PAID";
			}
			$data_piv_header = array('doc_paid'=>$new_paid,'doc_status'=>$paid_status);
			$this->db->update('t_purchase_invoice_header',$data_piv_header,array('doc_id'=>$detail['reff_id']));

		}
		$this->document_model->useDocSeries('PAY_OUT');
	}
	public function view($code)
	{
		$this->db->select("*");
		$this->db->from('t_payment_header');
		$this->db->join('t_payment_detail','t_payment_header.doc_id = t_payment_detail.doc_id');
		$this->db->where('doc_num',$code);
		return $this->db->get()->result_array();
	}
	public function edit($data,$where)
	{
		$check = $this->view($where);
		
	}

	public function getAll($type){
		$query = $this -> db -> query ("
			select * from t_payment_header 
			order by doc_id desc
		");
		return $query -> result_array();
	}
	
	public function get_ar($buss){
		$query = $this -> db -> query ("
			select 'SAL_ORD' as 'type',
				t1.doc_num, t1.doc_dt, t1.doc_ddt, t1.doc_curr, t1.doc_subtotal, 
				t1.doc_disc_amount, t1.doc_rounding, t1.doc_tax, t1.doc_total
			from
				t_sales_order_header t1 inner join t_business t2 on t1.buss_id = t2.buss_id
			where
				t2.buss_id = $buss
				and t1.doc_status <> 'CLOSED'
				and t1.doc_status <> 'CANCELLED'
			union all    
			select 'SAL_INV' as 'type',
				t1.doc_num, t1.doc_dt, t1.doc_ddt, t1.doc_curr, t1.doc_subtotal, 
				t1.doc_disc_amount, t1.doc_rounding, t1.doc_tax, t1.doc_total
			from
				t_sales_invoice_header t1 inner join t_business t2 on t1.buss_id = t2.buss_id
			where
				t2.buss_id = $buss
		");
		return $query -> result_array();
	}
	public function get_ap($buss){
		$query = $this -> db -> query ("
			select 'PUR_ORD' as 'type', t1.doc_id,
				t1.doc_num, t1.doc_dt, t1.doc_ddt, t1.doc_curr, t1.doc_subtotal, 
				t1.doc_disc_amount, t1.doc_rounding, t1.doc_tax, t1.doc_total
			from
				t_purchase_order_header t1 inner join t_business t2 on t1.buss_id = t2.buss_id
			where
				t2.buss_id = $buss
				and t1.doc_status <> 'CLOSED'
				and t1.doc_status <> 'CANCELLED'
			union all    
			select 'PUR_INV' as 'type', t1.doc_id,
				t1.doc_num, t1.doc_dt, t1.doc_ddt, t1.doc_curr, t1.doc_subtotal, 
				t1.doc_disc_amount, t1.doc_rounding, t1.doc_tax, t1.doc_total
			from
				t_purchase_invoice_header t1 inner join t_business t2 on t1.buss_id = t2.buss_id
			where
				t2.buss_id = $buss
				and t1.doc_status <> 'PAID'
		");
		return $query -> result_array();
	}


	public function getHeader($code){
		$this->db->select("*");
		$this->db->from('t_payment_header');
		$this->db->join('t_business','t_business.buss_id = t_payment_header.buss_id');
		$this->db->where('doc_num',$code);
		return $this->db->get()->result_array();
	}
	public function getDetail($code){
		$this->db->select("*");
		$this->db->from('t_payment_header');
		$this->db->join('t_payment_detail','t_payment_header.doc_id = t_payment_detail.doc_id');
		$this->db->where('t_payment_header.doc_num', $code);
		return $this->db->get()->result_array();
	}
	
}