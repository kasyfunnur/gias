<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Business_model extends MY_Model {	
public $table;
	function __construct()
    {
        parent::__construct();
    }

	public function getAll() {
		$query = $this -> db -> query ("
			select * from t_business 
			where is_vendor = 1
		");
		return $query -> result_array();
	}
	public function getVendor(){
		$this->db->where('is_vendor' , 1);
		$query=$this->db->get('t_business');
		return $query->result_array();
	}	
	public function getCustomer(){
		$this->db->where('is_customer' , 1);
		$query=$this->db->get('t_business');
		return $query->result_array();
	}
	public function detail($code){
		$this->db->where('buss_code',$code);
		$query = $this->db->get('t_business');
		return $query->result_array();
	}
	
	public function add($data,$type){
		$this->load->model("document_model");
		$data['buss_code'] = $this->document_model->getNextSeries($type);		
		$this->document_model->useDocSeries($type);
		$this->db->insert('t_business',$data);		
		return $this->db->insert_id();
	}

	public function edit($data,$where){		
		$this->db->update('t_business',$data,$where);		
	}

	public function update_business($data,$code){
		$this->db->update('t_business', $data, array('buss_code' => $code));
	}

	public function get_transaction($buss_code){
		$this->db->select("*");
		$this->db->from('t_purchase_invoice_header p');
		$this->db->join('t_business b','p.buss_id = b.buss_id');
		$this->db->where('b.buss_code',$buss_code);
		$query1 = $this->db->get()->result_array();

		$this->db->select("*");
		$this->db->from('t_sales_invoice_header s');
		$this->db->join('t_business b','s.buss_id = b.buss_id');
		$this->db->where('b.buss_code',$buss_code);
		$query2 = $this->db->get()->result_array();

		$query = array_merge($query1,$query2);

		return $query;
	}
}