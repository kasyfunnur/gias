<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once('passwordhash.php');
defined('PHPASS_HASH_STRENGTH') or define('PHPASS_HASH_STRENGTH', 8);
defined('PHPASS_HASH_PORTABLE') or define('PHPASS_HASH_PORTABLE', FALSE);

class login_lib extends CI_Model{

	function create($username = FALSE, $password = FALSE){
		
		if($username == FALSE OR $password == FALSE)return FALSE;
		if($this -> check_username($username))return FALSE;
		
		$hasher = new passwordhash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
		$user_pass_hashed = $hasher->HashPassword($password);

		$data = array(
					'user_name' => $username,
					'user_pass' => $user_pass_hashed,
					'user_date' => date('Y-m-d H:i:s'),
					'user_modified' => date('Y-m-d H:i:s')
				);

		$this -> db -> set($data); 
		if(!$this -> db -> insert('t_user'))return FALSE;								
		return TRUE;
	}

	function login($username = FALSE, $password = FALSE){

		if($username == FALSE || $password == FALSE)				return FALSE;
		if(!$this -> check_password ($username, $password))			return FALSE;
		if($this  -> session -> userdata('username') == $username)	return TRUE;
		
		$this -> db -> where  ('user_name', $username);
		$this -> db -> select ('user_id,user_name,user_location_id,full_name,user_email,designation');
		
		$query = $this -> db -> get_where ('t_user');
		
		if ($query->num_rows() > 0) 
		{
			$user_data = $query->row_array();
			
			$this -> session -> sess_destroy();
			//$this -> session -> sess_create();
			//$this->session->__construct(); // CI 3
			
			$this -> db -> update('t_user',array('user_last_login'=>date('Y-m-d H:i:s')),array('user_id'=>$user_data['user_id']));

			foreach($user_data as $key=>$value){if(($value==NULL)||($value==''))
				unset($_SESSION[$key]);}
				// unset($user_data[$key]);} //unsetting null or empty value
						
			//$user_data['logged_in'] = TRUE;
			$user_data['logged_in'] = 1;
			
			$user_group = $this -> sky -> get_user_group($user_data['user_id']);
			$menu_hide  = $this -> sky -> get_menu_hide($user_group);
			
			$user_data['menu_hidden'] = $menu_hide;
			$this->session->set_userdata($user_data);
			
			return TRUE;
		} 
		else return FALSE;
	}

	function logout(){
		// IP, delete last node menu history
		$this->load->helper('cookie');
		delete_cookie("_amlastnode");
		$this-> session -> sess_destroy();
		redirect('login');
	}
	
	
	//JM
	function check_next_reset($name){
		$this->db->where('user_name',$name);
		$this->db->select('reset_next_login');
		$query = $this->db->get_where('t_user');
		return $query->row_array();
	}
	
	//JM	
	function change_password($id,$pass,$newpass,$newpass2,$reset_next = FALSE){
		if(!$id || !$pass || !$newpass || !$newpass2)return FALSE;
		if($newpass != $newpass2)return FALSE;
		$this -> db -> where('user_id', $id);
		$this -> db -> select('user_pass');
		$query = $this -> db -> get_where('t_user');
		if ($query->num_rows() > 0){
			$user_data 	= $query -> row_array(); 
			$hasher = new passwordhash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
			//if(!$hasher -> CheckPassword($password, $user_data['user_pass']))return FALSE;
			if ($hasher->CheckPassword($pass, $user_data['user_pass'])) {
				$what = 'Authentication succeeded';
			} else {
				$what = 'Authentication failed';
				$op = 'fail'; // Definitely not 'change'
				return FALSE;
			}
			$hash = $hasher->HashPassword($newpass);
			if (strlen($hash) < 20)
				fail('Failed to hash new password');
			unset($hasher);
			
			if($reset_next)$this->db->update('t_user',array('reset_next_login'=>0),array('user_id'=>$id));
			$this->db->update('t_user',array('user_pass'=>$hash),array('user_id'=>$id));			
			return TRUE;
			//$login->logout();
			//$this->login->logout();
			/*($stmt = $this ->db->prepare('update t_user set user_pass=? where user_id=?'))
				|| fail('MySQL prepare', $db->error);
			$stmt->bind_param('ss', $hash, $id)
				|| fail('MySQL bind_param', $db->error);
			$stmt->execute()
				|| fail('MySQL execute', $db->error);*/
		}
	}

	function check_password($username = FALSE, $password = FALSE){
		if(!$username||!$password)return FALSE;
		$this -> db -> where('user_name', $username);
		$this -> db -> select('user_pass');
		$query = $this -> db -> get_where('t_user');
		
		if ($query->num_rows() > 0){
				$user_data 	= $query -> row_array(); 
				$hasher 	= new passwordhash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
				if(!$hasher -> CheckPassword($password, $user_data['user_pass']))return FALSE;
				else return TRUE;
			}
		else return FALSE;	
		}

	
	function check_username($username = FALSE, $return_data = FALSE){
		if(!$username)return FALSE;
		$this -> db -> where('user_name', $username);
		$query = $this->db->get_where('t_user');
		return ($query->num_rows()&&$return_data)?$query->row():(($query->num_rows())?TRUE:FALSE);
	}
	
	
}
//unused
/*
	function edit_password($user_email = FALSE, $new_pass = FALSE){
		if(!$user_email||!$new_pass)return FALSE;
		$hasher 			= new passwordhash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);	
		$user_pass_hashed 	= $hasher->HashPassword($new_pass);
		$data = array(
			'user_pass' => $user_pass_hashed,
			'user_modified' => date('Y-m-d H:i:s')
		);
		if($this -> db -> update('t_user', $data, array('user_email' => $user_email))) return TRUE;
		else return FALSE;
	}
	
		
	function check_email($user_email=FALSE,$return_data=FALSE){
		if(!$user_email)return FALSE;
		$this->db->where('user_email', $user_email);
		$query = $this->db->get_where('t_user');
		return ($query->num_rows()&&$return_data) ? $query -> row() : (($query -> num_rows()) ? TRUE : FALSE);
	}
	
	function generate_random_chars($l=32,$c='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890',$u = TRUE){ 
		if (!$u) for ($s = '', $i = 0, $z = strlen($c)-1; $i < $l; $x = rand(0,$z), $s .= $c{$x}, $i++); 
		else for ($i = 0, $z = strlen($c)-1, $s = $c{rand(0,$z)}, $i = 1; $i != $l; $x = rand(0,$z), $s .= $c{$x}, $s = ($s{$i} == $s{$i-1} ? substr($s,0,-1) : $s), $i=strlen($s)); 
		return $s; 
	} 
	
	function get_data($user=FALSE){
		if(!$user)return FALSE;
		if (preg_match('/^[0-9]+$/', $user)) $this->db->where('user_id', $user);
		else $this->db->where('user_email', $user);
		$query = $this->db->get_where('t_user');
		return $query->row();
	}
	*/