<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_model extends CI_Model {

	public function get_companies($cid = null)
	{
		if(is_numeric($cid)){
			$this->db->where('comp_id', $cid);
		}
		$this->db->from('t_company');

		return (is_numeric($cid)) ? $this->db->get()->row_array() : $this->db->get()->result_array();
	}

	public function add_company($newdata)
	{
		if(empty($newdata))
			return false;

		return $this->db->insert('t_company', $newdata);
	}	

	public function update_company($cid, $newdata)
	{
		if(!is_numeric($cid))
			return false;
		$this->db->where('comp_id', $cid);
		return $this->db->update('t_company', $newdata);
	}


}

/* End of file Company_model.php */
/* Location: ./application/models/Company_model.php */