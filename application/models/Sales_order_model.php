<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class sales_order_model extends MY_Model {
    var $details;
	public $table;
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->table = '';
    }
	
	//approval
	public function approve_sales_order($docnumber, $approval_status){		
		$this -> db -> trans_start();		
		$this -> db -> update ('t_sales_order_header', array("approval_status" => $approval_status));
		
		$msg = '';
		if($approval_status == '3' || $approval_status == '2'){
			if($approval_status == '3'){
				$this -> db -> query("call procUpdateQtyOnHand()");
			}
			$msg = 'Sales Order has been approved.';
		}
		elseif ($approval_status == '5')$msg = 'Sales Order has been rejected.';		
		$this-> db -> trans_complete();
		if ($this->db->trans_status() === FALSE){
			return array('success'=>0,'msg'=>'Failed to approve the document ');
		} else {
			return array('success'=>1,'msg'=>$msg);
		}
	}
	public function review_sales_order($docnumber){
		return $this -> db -> get_where('t_sales_order_header', array('doc_num'=>$docnumber))->result_array();
	}
	
	public function getAll(){
		$query = $this -> db -> query ("
			select * from t_sales_order_header 
			inner join t_business on t_sales_order_header.buss_id = t_business.buss_id
			inner join t_site_location on t_site_location.location_id = t_sales_order_header.whse_id
			
			order by doc_id desc
		");//where doc_status = 'ACTIVE'
		return $query -> result_array();
	}
	public function getAll_detail(){
		$query = $this -> db -> query ("
			select t_sales_order_detail.*, t_asset_group.group_name from t_sales_order_header 
			inner join t_sales_order_detail on t_sales_order_header.doc_id = t_sales_order_detail.doc_id
			inner join t_business on t_sales_order_header.buss_id = t_business.buss_id
			inner join t_site_location on t_site_location.location_id = t_sales_order_header.whse_id
			inner join t_asset_group on t_asset_group.group_id = t_sales_order_detail.item_group
			where doc_status = 'ACTIVE'
		");
		return $query -> result_array();
	}
	public function getAll_where($id,$where=NULL){
		if(count($where)<=1){ 
			//$this->db->where($where[0]['field'],$where[0]['value']);
		}else{ 
			for($j = 0 ; $j< count($where) ; $j++){
				$this->db->where($where[$j]['field'],$where[$j]['value']);
			}
		}	
		if($id != NULL){
			$this->db->where('t_business.buss_id',$id);
		}
		$this->db->select('*');
		$this->db->from('t_sales_order_header');
		$this->db->join('t_business','t_sales_order_header.buss_id = t_business.buss_id','inner');
		$this->db->join('t_site_location','t_site_location.location_id = t_sales_order_header.whse_id ','inner');
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	public function getAll_with_customer($id,$logistic_status=NULL,$billing_status=NULL,$doc_status=NULL){
		$this->db->select('*');
		$this->db->from('t_sales_order_header');
		$this->db->join('t_business','t_sales_order_header.buss_id = t_business.buss_id','inner');
		$this->db->join('t_site_location','t_site_location.location_id = t_sales_order_header.whse_id','inner');
		if($logistic_status != NULL){
			$this->db->where('logistic_status',$logistic_status);
		}else{
			$this->db->where_not_in('logistic_status','DELIVERED');
		}
		if($billing_status != NULL){
			$this->db->where('billing_status',$billing_status);
		}else{
			$this->db->where_not_in('billing_status','BILLED');
		}
		//$this->db->where('approval_status','approved');
		if($doc_status != NULL){
			$this->db->where('doc_status',$doc_status);
		}else{
			$this->db->where('doc_status','CLOSED');
		}
		
		if($id != NULL){
			$this->db->where('t_business.buss_id',$id);
		}
		$query = $this->db->get();
		$result = $query -> result_array();
		return $result;
	}
	
	public function set_item_close($code){
		$this->db->select('*');
		$this->db->from('t_sales_order_header');
		$this->db->join('t_sales_order_detail','t_sales_order_header.doc_id = t_sales_order_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		foreach($query->result_array() as $row){
			if($row['item_qty_closed'] >= $row['item_qty']){
				$data = array("line_close" => 1);
			}else{
				$data = array("line_close" => 0);
			}
			$this->db->where('doc_id',$row['doc_id']);
			$this->db->where('doc_line',$row['doc_line']);				
			$this->db->update('t_sales_order_detail', $data);	
		}
	}
	public function auto_doc_close(){
		$this->db->select("*");
		$this->db->from('t_sales_order_header');
		$query = $this->db->get();
		$temps = $query->result_array();
		foreach($temps as $temp){
			$this->set_doc_close($temp['doc_num']);
		}		
	}
	public function set_doc_close($code){ //echo $code;
		$this->db->select('line_close');
		$this->db->distinct('line_close');
		$this->db->from('t_sales_order_header');
		$this->db->join('t_sales_order_detail','t_sales_order_header.doc_id = t_sales_order_detail.doc_id','inner');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		$temp = $query->result_array();
		//var_dump($temp);
		//return false;
		$doc_status = "ACTIVE";
		$log_status = "PENDING";
		if(count($temp) > 1){
			$log_status = "PARTIAL";
		}else{
			if($temp[0]['line_close'] == 1){
				$doc_status = "CLOSED";
				$log_status = "DELIVERED";				
			}
		}
		$data = array("doc_status"=>$doc_status,"logistic_status"=>$log_status);
		//$this->db->where('doc_num',$code);
		$this->db->update('t_sales_order_header', $data, array('doc_num' => $code));
	}
	
	
	public function get_qty_close($code){
		$this->db->select('*');
		$this->db->from('t_sales_order_header');
		$this->db->join('t_sales_order_detail','t_sales_order_header.doc_id = t_sales_order_detail.doc_id','inner');
		$this->db->where('line_close',0);
		$this->db->where('item_qty_closed > 0');
		$query = $this->db->get();
		return $query -> result_array();	
	}
	public function get_close_line($code){
		$this->db->select('*');
		$this->db->from('t_sales_order_header');
		$this->db->join('t_sales_order_detail','t_sales_order_header.doc_id = t_sales_order_detail.doc_id','inner');
		$this->db->where('line_close',1);
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();		
	}
	public function get_open_line($code){
		$this->db->select('*');
		$this->db->from('t_sales_order_header');
		$this->db->join('t_sales_order_detail','t_sales_order_header.doc_id = t_sales_order_detail.doc_id','inner');
		$this->db->where('line_close',0);
		$this->db->where('doc_num',$code);
		$query = $this->db->get();
		return $query -> result_array();		
	}
	
	public function insert_header($data){
		$this->db->insert('t_sales_order_header',$data);
		return $this->db->insert_id();
	}
	public function insert_detail($data){
		$this->db->insert('t_sales_order_detail',$data);	
		return $this->db->insert_id();
	}
	
	public function update_header($data,$where){
		if(count($where)<=1){
			$this->db->where($where[0]['field'],$where[0]['value']);
		}else{
			for($j = 0 ; $j< count($where) ; $j++){
				$this->db->where($where[$j]['field'],$where[$j]['value']);
			}
		}
		return $this->db->update('t_sales_order_header', $data);	
	}
	
	public function delete_detail($code){
		$this->db->where('doc_id', $code);
		$this->db->delete('t_sales_order_detail');
		return $this->db->affected_rows();
	}
	
	public function getHeader($doc){
		/*, 
				sum(t_sales_delivery_header.doc_cost_freight) as 'doc_cost_freight', 
				sum(t_sales_delivery_header.doc_cost_other) as 'doc_cost_other'*/
				
		//left join t_sales_delivery_header on t_sales_delivery_header.doc_ref = t_sales_order_header.doc_num
		$query = $this -> db -> query ("
			select t_sales_order_header.*, t_business.*, t_site_location.*, 
				sum(t_sales_delivery_header.doc_cost_freight) as 'doc_cost_freight', 
				sum(t_sales_delivery_header.doc_cost_other) as 'doc_cost_other'
			from t_sales_order_header 
			inner join t_business on t_sales_order_header.buss_id = t_business.buss_id 
			inner join t_site_location on t_site_location.location_id = t_sales_order_header.whse_id
			left join t_sales_delivery_header on t_sales_delivery_header.doc_ref = t_sales_order_header.doc_num
			where t_sales_order_header.doc_num = '".$doc."'
		");
		return $query -> result_array();
	}
	public function getDetail($doc){
		$query = $this -> db -> query ("
			select *, t_sales_order_detail.item_name as 'item_name_so' from t_sales_order_detail
			inner join t_inventory on t_sales_order_detail.item_id = t_inventory.item_id
			where doc_id = (select doc_id from t_sales_order_header where doc_num = '".$doc."')
			order by doc_line
		");
		return $query -> result_array();
	}
	public function getDetail_line($doc,$line){
		$this->db->select('*');
		$this->db->from('t_sales_order_header');
		$this->db->join('t_sales_order_detail','t_sales_order_header.doc_id = t_sales_order_detail.doc_id','inner');
		$this->db->join('t_inventory','t_inventory.item_id = t_sales_order_detail.item_id','inner');
		$this->db->where('doc_num',$doc);
		$this->db->where('doc_line',$line);
		$query = $this->db->get();
		return $query -> row_array();
	}
	public function getDetail_so_open($doc){
		$query = $this -> db -> query ("
			select *, t_sales_order_detail.item_name as 'item_name_so' from t_sales_order_detail
			inner join t_inventory on t_sales_order_detail.item_id = t_inventory.item_id
			where doc_id = (select doc_id from t_sales_order_header where doc_num = '".$doc."')
			and t_sales_order_detail.item_qty_closed < t_sales_order_detail.item_qty 
			order by doc_line
		");
		return $query -> result_array();
	}
	
	public function getFreight(){
		$query = $this->db->query("
			select distinct doc_freight, doc_freight as 'freight_code' from t_sales_delivery_header
			where doc_freight <> ''
		");
		return $query->result_array();
	}
	
	public function getOverDue($code,$day){
		//$this->db->select('doc_ddt - doc_dt as dt_left');
		$this->db->select('DATEDIFF(doc_ddt,now()) as dt_left');
		$this->db->from('t_sales_order_header');
		$this->db->where('doc_num',$code);
		$query = $this->db->get();		
		$overdue = $query->row_array();
		return ($overdue['dt_left'] < 1) ? true : false;		
	}

	/**
	 * REPORT MODEL
	 */
	public function sales_order_report($filters){
		if($filters['date_from']){
			$this->db->where('doc_dt >=', $filters['date_from']);
		}
		if($filters['date_to']){
			$this->db->where('doc_dt <=', $filters['date_to']);
		}
		$this->db->from('v_sales_order');
		return $this->db->get()->result_array();
	}

	public function sales_order_detail_report($filters){
		if($filters['date_from']){
			$this->db->where('doc_dt >=', $filters['date_from']);
		}
		if($filters['date_to']){
			$this->db->where('doc_dt <=', $filters['date_to']);
		}
		$this->db->from('v_sales_order_detail');
		return $this->db->get()->result_array();
	}
}