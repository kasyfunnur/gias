<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Flow_model extends MY_Model{
	public $table;
	protected $CI;
	public function __construct(){
		parent::__construct();
		$this->load->model('document_model');
		$this -> CI = & get_instance();
	}

	/**
	 * Get List Flow of Module
	 */
	public function get_list_flow($module)
	{
		$this->db->select("*");
		$this->db->from("t_document_flow");
		$this->db->where("module",$module);
		return $this->db->get()->result_array();		
	}

	/**
	 * Get Position
	 */
	public function get_doc_position($module,$doc)
	{
		$this->db->select("order");
		$this->db->from("t_document_flow");
		$this->db->where("doc",$doc);
		$this->db->where("module",$module);
		return $this->db->get()->row_array();
	}

	/**
	 * Get Table Info
	 */
	public function get_table_info($type)
	{
		$this->db->select("table_info");
		$this->db->from("t_document_flow");
		$this->db->where("doc_type",$type);
		return $this->db->get()->result_array();
	}
	
	/**
	 * Get List of Flow before selected doc.
	 */
	public function get_flow_before($module,$doc)
	{
		//$list_flow = $this->get_list_flow($module);
		$curr_flow = $this->get_doc_position($module,$doc);		
		//if(is_null($curr_flow))return false;
		//var_dump($curr_flow);
		$this->db->select("*");
		$this->db->from("t_document_flow");
		$this->db->where("t_document_flow.order <= ", $curr_flow['order']);
		$this->db->order_by("order","desc");
		//$query = $this->db->get();
		//var_dump($query);
		return $this->db->get()->result_array();
		/*if($this->db->get()->count_row() > 1){
			return $this->db->get()->result_array();
		}else{
			return false;
		}*/
		
	}

	/**
	 * Get List of Flow after selected doc.
	 */
	public function get_flow_after($module,$doc)
	{
		$curr_flow = $this->get_doc_position($module,$doc);	
		$this->db->select("*");
		$this->db->from("t_document_flow");
		$this->db->where("t_document_flow.order > ", $curr_flow['order']);
		$this->db->order_by("order","asc");
		return $this->db->get()->result_array();
	}

	/**
	 * Get Data from Table
	 */
	public function get_data_before($table,$doc_id)
	{
		$this->db->select("reff_type,reff_id");
		$this->db->distinct("reff_type,reff_id");
		$this->db->from($table);
		$this->db->where('doc_id',$doc_id);
		return $this->db->get()->result_array();
	}
	
	/**
	 * Get Data After
	 */
	public function get_data_after($type, $table,$doc_id)
	{
		$this->db->select("'$type' as 'reff_type', doc_id");
		$this->db->distinct("reff_type,doc_id");
		$this->db->from($table);
		$this->db->where('reff_id',$doc_id);
		return $this->db->get()->result_array();
	}

	/**
	 * Get Doc
	 */
	public function get_doc($table,$doc_id)
	{
		//echo $table;
		//echo $doc_id;
		$this->db->select("doc_num,doc_dt,buss_name");
		$this->db->from($table);
		//$this->db->join("t_business","t_business.buss_id = ".$table.".buss_id");
		$this->db->join("t_business", $table.".buss_id = t_business.buss_id");
		$this->db->where("doc_id",$doc_id);
		return $this->db->get()->result_array();
	}

}