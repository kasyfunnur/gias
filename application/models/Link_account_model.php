<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Link_account_model extends CI_Model {

	public function get_account($code = null)	
	{
		if($code){
			$this->db->where('code', $code);
		}
		$this->db->select('code, account_id');
		$this->db->from('t_link_account');
		$result = $this->db->get();

		if (!$result->num_rows())
			return NULL;

		if($code){
			return $result->row_array()['account_id']; // return the account_id 
		} else {
			return $result->result_array(); // return all in array format
		}
	}

	

}

/* End of file Link_account_model.php */
/* Location: ./application/models/Link_account_model.php */