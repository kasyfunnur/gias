<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Comment_model extends MY_Model {	
public $table;
	function __construct()
    {
        parent::__construct();
    }

	public function getAll($attach=NULL) {
		$this->db->select("*");
		$this->db->from("t_comment_detail");
		if(!empty($attach)){
			$this->db->where_in("attach",$attach);
		}
		$query = $this->db->get();		
		return $query -> result_array();
	}
	public function postComment($attach,$comment){
		$comment = array(
			"attach"	=> $attach,
			"user"		=> $this->session->userdata("username"),
			"comment"	=> $comment
		);
		$this->db->insert('t_comment_detail',$comment);
	}
}