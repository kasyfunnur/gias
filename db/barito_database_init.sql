SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


DELIMITER $$
DROP PROCEDURE IF EXISTS `procUpdateQtyOnHand`$$
CREATE PROCEDURE `procUpdateQtyOnHand`()
BEGIN
  update t_inventory set qty_onhand = (
		select SUM(item_qty)
		from v_itembalance
		where v_itembalance.item_id = t_inventory.item_id   
    );      
  update t_inventory_warehouse set qty_onhand = (
    select SUM(item_qty)
		from v_itembalance
		where v_itembalance.item_id = t_inventory_warehouse.item_id
        and v_itembalance.whse_id = t_inventory_warehouse.location_id
    );
  update t_inventory set qty_tocome = (
    select SUM(qty_tocome)
    from v_itemincoming
    where v_itemincoming.item_id = t_inventory.item_id
    );
  update t_inventory_warehouse set qty_tocome = (
    select SUM(qty_tocome)
    from v_itemincoming
    where v_itemincoming.item_id = t_inventory_warehouse.item_id
        and v_itemincoming.whse_id = t_inventory_warehouse.location_id
    );
  update t_inventory set qty_togo = (
    select SUM(qty_togo)
    from v_itemoutgoing
    where v_itemoutgoing.item_id = t_inventory.item_id
    );
  update t_inventory_warehouse set qty_togo = (
    select SUM(qty_togo)
    from v_itemoutgoing
    where v_itemoutgoing.item_id = t_inventory_warehouse.item_id
        and v_itemoutgoing.whse_id = t_inventory_warehouse.location_id
    );
END$$

DELIMITER ;

DROP TABLE IF EXISTS `t_account`;
CREATE TABLE IF NOT EXISTS `t_account` (
  `account_id` int(11) NOT NULL,
  `account_number` varchar(25) NOT NULL,
  `account_name` varchar(250) NOT NULL,
  `account_currency` varchar(10) NOT NULL DEFAULT 'IDR',
  `account_parent_number` varchar(25) NOT NULL DEFAULT '0',
  `account_level` int(11) NOT NULL,
  `account_category` varchar(45) DEFAULT NULL,
  `account_class` varchar(25) DEFAULT NULL,
  `account_balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `is_postable` bit(1) NOT NULL DEFAULT b'0',
  `is_cash` bit(1) NOT NULL DEFAULT b'0',
  `is_bank` bit(1) NOT NULL DEFAULT b'0',
  `default` varchar(1) NOT NULL DEFAULT 'D',
  `account_usage` varchar(5) NOT NULL DEFAULT 'O'
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

INSERT INTO `t_account` (`account_id`, `account_number`, `account_name`, `account_currency`, `account_parent_number`, `account_level`, `account_category`, `account_class`, `account_balance`, `is_postable`, `is_cash`, `is_bank`, `default`, `account_usage`) VALUES
(1, '10000', 'ASSET1', 'IDR', '0', 1, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(2, '11000', 'Current Asset', 'IDR', '10000', 2, NULL, NULL, '0.00', b'0', b'1', b'0', 'D', 'O'),
(3, '11100', 'Cash and Bank', 'IDR', '11000', 3, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(4, '11101', 'Bank BCA', 'IDR', '11100', 4, NULL, NULL, '0.00', b'1', b'0', b'1', 'D', 'O'),
(5, '11111', 'Cash IDR', 'IDR', '11100', 4, NULL, NULL, '0.00', b'1', b'1', b'0', 'D', 'O'),
(6, '11200', 'Inventory', 'IDR', '11000', 3, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(7, '11210', 'Raw Material', 'IDR', '11200', 4, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(8, '11220', 'WIP', 'IDR', '11200', 4, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(9, '11230', 'Finished Good', 'IDR', '11200', 4, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(10, '11300', 'AR', 'IDR', '11000', 3, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(11, '11310', 'AR Retail', 'IDR', '11300', 4, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(12, '11320', 'AR Wholesale', 'IDR', '11300', 4, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(13, '11400', 'Other', 'IDR', '11000', 3, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(14, '11410', 'Ayat Silang', 'IDR', '11400', 4, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(15, '11420', 'Advance Payment', 'IDR', '11400', 4, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(16, '12000', 'Fixed Asset', 'IDR', '10000', 2, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(17, '12100', 'Machine', 'IDR', '12000', 3, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(18, '12200', 'Vehicle', 'IDR', '12000', 3, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(19, '20000', 'LIABILITY', 'IDR', '0', 1, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(20, '21000', 'Short Term Lia', 'IDR', '20000', 2, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(21, '21100', 'AP', 'IDR', '21000', 3, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(22, '21101', 'AP Accrued', 'IDR', '21100', 4, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(23, '21102', 'AP Invoiced', 'IDR', '21100', 4, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(24, '21200', 'Accrued Expense', 'IDR', '21000', 3, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(25, '21201', 'Accrued Expense', 'IDR', '21200', 4, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(26, '22000', 'Long Term Lia', 'IDR', '20000', 2, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(27, '22100', 'Bank Loan', 'IDR', '22000', 3, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(28, '30000', 'EQUITY', 'IDR', '0', 1, NULL, NULL, '0.00', b'0', b'0', b'0', 'C', 'O'),
(29, '31000', 'Share Capital', 'IDR', '30000', 2, NULL, NULL, '0.00', b'0', b'0', b'0', 'C', 'O'),
(30, '31001', 'Shareholder #1', 'IDR', '31000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'C', 'O'),
(31, '31002', 'Shareholder #2', 'IDR', '31000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'C', 'O'),
(32, '31003', 'Shareholder #3', 'IDR', '31000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'C', 'O'),
(33, '40000', 'SALES', 'IDR', '0', 1, NULL, NULL, '0.00', b'0', b'0', b'0', 'C', 'O'),
(34, '41000', 'Revenue', 'IDR', '40000', 2, NULL, NULL, '0.00', b'0', b'0', b'0', 'C', 'O'),
(35, '41001', 'Sales Revenue', 'IDR', '41000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'C', 'O'),
(36, '41002', 'Other Revenue', 'IDR', '41000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'C', 'O'),
(37, '42000', 'Discount', 'IDR', '40000', 2, NULL, NULL, '0.00', b'0', b'0', b'0', 'C', 'O'),
(38, '42001', 'Sales Discount', 'IDR', '42000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'C', 'O'),
(39, '42002', 'Other Discount', 'IDR', '42000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'C', 'O'),
(40, '43000', 'Other Revenue', 'IDR', '40000', 2, NULL, NULL, '0.00', b'0', b'0', b'0', 'C', 'O'),
(41, '43001', 'Bank Interest', 'IDR', '43000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'C', 'O'),
(42, '50000', 'EXPENSE', 'IDR', '0', 1, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(43, '51000', 'Cost of Sales', 'IDR', '50000', 2, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(44, '51100', 'COGS', 'IDR', '51000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(45, '51200', 'Misc Stock Cost', 'IDR', '51000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(46, '51300', 'Stock Difference Gain/Loss', 'IDR', '51000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(47, '51400', 'Promotion', 'IDR', '51000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(48, '52000', 'Labour', 'IDR', '50000', 2, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(49, '52100', 'Salary', 'IDR', '52000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(50, '52200', 'Commission', 'IDR', '52000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(51, '53000', 'Freight', 'IDR', '50000', 2, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(52, '53100', 'Sales Freight', 'IDR', '53000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(53, '53200', 'Other Freight', 'IDR', '53000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(54, '54000', 'Production', 'IDR', '50000', 2, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(55, '54100', 'Production - Service', 'IDR', '54000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(56, '54200', 'Production - Other Fee', 'IDR', '54000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(57, '54300', 'Production - PreProduction Expense', 'IDR', '54000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(58, '55000', 'Operational', 'IDR', '50000', 2, NULL, NULL, '0.00', b'0', b'0', b'0', 'D', 'O'),
(59, '55100', 'Operational - Banking', 'IDR', '55000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(60, '55200', 'Operational - Bank Charges', 'IDR', '55000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(70, '11112', 'Cash USD', 'USD', '11100', 4, NULL, NULL, '0.00', b'1', b'1', b'0', 'D', 'E'),
(71, '12101', 'Machine Accm Dep', 'IDR', '12000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(72, '12201', 'Vehicle Accm Dep', 'IDR', '12000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O'),
(73, '54400', 'Depreciation Expenses', 'IDR', '54000', 3, NULL, NULL, '0.00', b'1', b'0', b'0', 'D', 'O');

DROP TABLE IF EXISTS `t_account_journal_detail`;
CREATE TABLE IF NOT EXISTS `t_account_journal_detail` (
  `journal_id` int(11) NOT NULL,
  `journal_line` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `journal_dr` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `journal_cr` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `journal_cc` int(11) NOT NULL DEFAULT '0',
  `journal_curr` varchar(10) NOT NULL DEFAULT 'IDR'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Journal Detail';

INSERT INTO `t_account_journal_detail` (`journal_id`, `journal_line`, `account_id`, `journal_dr`, `journal_cr`, `journal_cc`, `journal_curr`) VALUES
(1, 1, 44, '0.0000', '0.0000', 0, 'IDR'),
(1, 2, 9, '0.0000', '0.0000', 0, 'IDR'),
(1, 3, 5, '350000.0000', '0.0000', 0, 'IDR'),
(1, 4, 35, '0.0000', '318182.0000', 0, 'IDR'),
(1, 5, 25, '0.0000', '31818.0000', 0, 'IDR');

DROP TABLE IF EXISTS `t_account_journal_header`;
CREATE TABLE IF NOT EXISTS `t_account_journal_header` (
  `journal_id` int(11) NOT NULL,
  `journal_code` varchar(50) NOT NULL,
  `posting_date` datetime NOT NULL,
  `document_date` datetime DEFAULT NULL,
  `journal_memo` varchar(500) DEFAULT NULL,
  `journal_type` varchar(20) DEFAULT NULL,
  `journal_ref1` varchar(50) DEFAULT NULL,
  `journal_ref2` varchar(50) DEFAULT NULL,
  `journal_ref3` varchar(50) DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `create_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `journaldate` datetime DEFAULT NULL,
  `journal_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Journal Header';

INSERT INTO `t_account_journal_header` (`journal_id`, `journal_code`, `posting_date`, `document_date`, `journal_memo`, `journal_type`, `journal_ref1`, `journal_ref2`, `journal_ref3`, `create_by`, `create_dt`, `journaldate`, `journal_date`) VALUES
(1, 'JR-0052', '2016-01-30 00:00:00', '2016-01-30 00:00:00', 'Sales by POS', 'SAL', 'POS2120160130', NULL, NULL, 5, '2016-01-30 09:46:36', NULL, NULL);

DROP TABLE IF EXISTS `t_account_journal_template_detail`;
CREATE TABLE IF NOT EXISTS `t_account_journal_template_detail` (
  `id` int(11) NOT NULL,
  `line` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `journal_dr` decimal(18,2) DEFAULT NULL,
  `journal_cr` decimal(18,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_account_journal_template_header`;
CREATE TABLE IF NOT EXISTS `t_account_journal_template_header` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_account_trans`;
CREATE TABLE IF NOT EXISTS `t_account_trans` (
  `id` int(11) NOT NULL,
  `trans_code` varchar(10) NOT NULL,
  `trans_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `t_account_trans` (`id`, `trans_code`, `trans_name`) VALUES
(1, ' ', ' ');

DROP TABLE IF EXISTS `t_approval`;
CREATE TABLE IF NOT EXISTS `t_approval` (
  `appr_id` int(11) NOT NULL,
  `appr_code` varchar(50) NOT NULL,
  `appr_name` varchar(100) NOT NULL,
  `appr_email` tinyint(1) NOT NULL DEFAULT '0',
  `appr_bypass` tinyint(1) NOT NULL DEFAULT '0',
  `appr_model` varchar(100) DEFAULT NULL,
  `appr_function` varchar(100) DEFAULT NULL,
  `review_function` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Master approval setting (header)';

INSERT INTO `t_approval` (`appr_id`, `appr_code`, `appr_name`, `appr_email`, `appr_bypass`, `appr_model`, `appr_function`, `review_function`) VALUES
(1, 'PUR_ORD', 'Purchase Order', 1, 0, 'purchase_order_model', 'approve_purchase_order', 'review_purchase_order'),
(2, 'SAL_ORD', 'Sales Order', 1, 1, 'sales_order_model', 'approve_sales_order', 'review_sales_order'),
(3, 'AST_DEP', 'Asset Depreciation', 0, 1, 'asset_model', 'approve_disposal_request', 'review_usage_request'),
(4, 'PUR_CON', 'Purchase Contract', 0, 0, 'purchase_contract_model', 'approve_purchase_contract', 'review_purchase_contract');

DROP TABLE IF EXISTS `t_approval_detail`;
CREATE TABLE IF NOT EXISTS `t_approval_detail` (
  `appr_detail_id` int(11) NOT NULL,
  `appr_id` int(11) NOT NULL,
  `auto_approve` tinyint(1) NOT NULL DEFAULT '0',
  `step_based` tinyint(1) NOT NULL DEFAULT '0',
  `requestor_position_id` int(11) NOT NULL,
  `approval_matrix` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Matrix Approval';

INSERT INTO `t_approval_detail` (`appr_detail_id`, `appr_id`, `auto_approve`, `step_based`, `requestor_position_id`, `approval_matrix`) VALUES
(52, 2, 1, 0, 2, '2'),
(56, 1, 0, 0, 5, '4'),
(57, 1, 0, 0, 4, '4'),
(58, 1, 0, 0, 2, '2/8+3/8'),
(59, 1, 0, 0, 10, '3+4/8'),
(60, 3, 1, 0, 2, '2'),
(61, 3, 1, 0, 5, '4'),
(62, 3, 1, 0, 4, '4'),
(81, 4, 1, 0, 4, '4'),
(82, 4, 0, 0, 7, '4'),
(83, 4, 0, 0, 2, '4');

DROP TABLE IF EXISTS `t_approval_setting`;
CREATE TABLE IF NOT EXISTS `t_approval_setting` (
  `appr_setting_id` int(11) NOT NULL,
  `appr_id` int(11) NOT NULL COMMENT 'Linked to t_approval.appr_id to determine the setting header',
  `requestor_user_id` int(11) NOT NULL COMMENT 'Requestor of the document',
  `approver_user_id1` int(11) DEFAULT NULL,
  `approver_user_id2` int(11) DEFAULT NULL,
  `approver_user_id3` int(11) DEFAULT NULL,
  `approver_user_id4` int(11) DEFAULT NULL,
  `approver_user_id5` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Approval Detail Settings';

INSERT INTO `t_approval_setting` (`appr_setting_id`, `appr_id`, `requestor_user_id`, `approver_user_id1`, `approver_user_id2`, `approver_user_id3`, `approver_user_id4`, `approver_user_id5`) VALUES
(1, 1, 1, 2, NULL, NULL, NULL, NULL),
(2, 1, 2, 1, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_asset`;
CREATE TABLE IF NOT EXISTS `t_asset` (
  `asset_id` int(11) unsigned NOT NULL,
  `asset_label` varchar(20) NOT NULL DEFAULT '',
  `asset_name` varchar(255) NOT NULL DEFAULT '',
  `asset_group_id` int(11) NOT NULL DEFAULT '0',
  `asset_condition` varchar(255) NOT NULL DEFAULT '',
  `asset_description` varchar(2000) NOT NULL DEFAULT '',
  `date_acquired` date NOT NULL DEFAULT '0000-00-00',
  `asset_location_id` int(11) NOT NULL DEFAULT '1',
  `asset_status` enum('incoming','active','lost','sold','written off','in transit','pending','disposed') NOT NULL DEFAULT 'active',
  `asset_note` varchar(2000) NOT NULL DEFAULT '',
  `asset_currency` varchar(20) NOT NULL DEFAULT 'IDR',
  `asset_value` int(11) NOT NULL DEFAULT '0',
  `asset_cost` decimal(20,2) NOT NULL DEFAULT '0.00',
  `custom_field_1` varchar(1000) DEFAULT NULL,
  `custom_field_2` varchar(1000) DEFAULT NULL,
  `custom_field_3` varchar(1000) DEFAULT NULL,
  `custom_field_4` varchar(1000) DEFAULT NULL,
  `custom_field_5` varchar(1000) DEFAULT NULL,
  `custom_field_6` varchar(1000) DEFAULT NULL,
  `custom_field_7` varchar(1000) DEFAULT NULL,
  `custom_field_8` varchar(1000) DEFAULT NULL,
  `custom_field_9` varchar(1000) DEFAULT NULL,
  `custom_field_10` varchar(1000) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `document_origin` varchar(255) NOT NULL DEFAULT '',
  `document_line` int(11) NOT NULL DEFAULT '1',
  `warranty_due_date` date DEFAULT NULL,
  `acc_id_cost` int(11) DEFAULT NULL,
  `acc_id_accdepre` int(11) DEFAULT NULL,
  `acc_id_depre` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

INSERT INTO `t_asset` (`asset_id`, `asset_label`, `asset_name`, `asset_group_id`, `asset_condition`, `asset_description`, `date_acquired`, `asset_location_id`, `asset_status`, `asset_note`, `asset_currency`, `asset_value`, `asset_cost`, `custom_field_1`, `custom_field_2`, `custom_field_3`, `custom_field_4`, `custom_field_5`, `custom_field_6`, `custom_field_7`, `custom_field_8`, `custom_field_9`, `custom_field_10`, `timestamp`, `document_origin`, `document_line`, `warranty_due_date`, `acc_id_cost`, `acc_id_accdepre`, `acc_id_depre`) VALUES
(1, 'LA00001', 'Filing Cabinet', 7, '', '', '2014-07-01', 1, 'in transit', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(2, 'LA00002', 'Toner', 6, '', '', '2014-07-01', 5, 'active', '', 'IDR', 0, '500000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(3, 'LA00003', 'Chair', 2, '', '', '2014-07-01', 2, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(4, 'LA00004', 'BlackBoard', 4, '', '', '2014-07-01', 1, 'in transit', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(5, 'LA00005', 'Pointer', 3, '', '', '2014-07-01', 5, 'sold', '', 'IDR', 0, '1500000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(6, 'LA00006', 'Camera', 4, '', '', '2014-07-01', 4, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(7, 'LA00007', 'Dictaphone', 2, '', '', '2014-07-01', 2, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(8, 'LA00008', 'Telephone', 5, '', '', '2014-07-01', 4, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(9, 'LA00009', 'Trash Can', 1, '', '', '2014-07-01', 2, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(10, 'LA00010', 'Camera', 4, '', '', '2014-07-01', 6, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(11, 'LA00011', 'Projector', 5, '', '', '2014-07-05', 2, 'written off', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(12, 'LA00012', 'Toner', 6, '', '', '2014-07-05', 5, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(13, 'LA00013', 'Laptop', 3, '', '', '2014-07-05', 5, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(14, 'LA00014', 'Thermostat', 4, '', '', '2014-07-05', 5, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(15, 'LA00015', 'Large Desk', 1, '', '', '2014-07-05', 2, 'sold', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(16, 'LA00016', 'Printer', 2, '', '', '2014-07-05', 3, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(17, 'LA00017', 'Dictaphone', 3, '', '', '2014-07-05', 6, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(18, 'LA00018', 'Projector', 4, '', '', '2014-07-05', 4, 'incoming', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(19, 'LA00019', 'Scanner', 2, '', '', '2014-07-05', 3, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(20, 'LA00020', 'Thermostat', 1, '', '', '2014-07-05', 1, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(21, 'LA00021', 'Paper Shredder', 1, '', '', '2014-07-10', 4, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(22, 'LA00022', 'Printer', 1, '', '', '2014-07-10', 1, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(23, 'LA00023', 'Telephone', 6, '', '', '2014-07-10', 1, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(24, 'LA00024', 'Toner', 2, '', '', '2014-07-10', 4, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(25, 'LA00025', 'Bicycle', 3, '', '', '2014-07-10', 6, 'incoming', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(26, 'LA00026', 'Bicycle', 4, '', '', '2014-07-10', 6, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(27, 'LA00027', 'Printer', 3, '', '', '2014-07-10', 1, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(28, 'LA00028', 'Printer', 1, '', '', '2014-07-10', 2, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(29, 'LA00029', 'Bicycle', 4, '', '', '2014-07-10', 5, 'sold', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(30, 'LA00030', 'Projector', 1, '', '', '2014-07-10', 5, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(31, 'LA00031', 'File Folder', 6, '', '', '2014-07-15', 6, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(32, 'LA00032', 'Desks', 1, '', '', '2014-07-15', 6, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(33, 'LA00033', 'Pointer', 2, '', '', '2014-07-15', 1, 'sold', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(34, 'LA00034', 'Printer', 4, '', '', '2014-07-15', 4, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(35, 'LA00035', 'Large Desk', 3, '', '', '2014-07-15', 4, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(36, 'LA00036', 'Pointer', 5, '', '', '2014-07-15', 6, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(37, 'LA00037', 'Paper Shredder', 3, '', '', '2014-07-15', 4, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(38, 'LA00038', 'File Folder', 6, '', '', '2014-07-15', 5, 'lost', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(39, 'LA00039', 'Laptop', 2, '', '', '2014-07-15', 4, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(40, 'LA00040', 'Calculator', 1, '', '', '2014-07-15', 3, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(41, 'LA00041', 'Pointer', 5, '', '', '2014-07-22', 5, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(42, 'LA00042', 'Telephone', 2, '', '', '2014-07-22', 6, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(43, 'LA00043', 'Trash Can', 3, '', '', '2014-07-22', 2, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(44, 'LA00044', 'Telephone', 2, '', '', '2014-07-22', 3, 'incoming', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(45, 'LA00045', 'Trash Can', 5, '', '', '2014-07-22', 1, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(46, 'LA00046', 'Trash Can', 2, '', '', '2014-07-22', 3, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(47, 'LA00047', 'Dictaphone', 4, '', '', '2014-07-22', 6, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(48, 'LA00048', 'Trash Can', 1, '', '', '2014-07-22', 1, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(49, 'LA00049', 'Scanner', 4, '', '', '2014-07-22', 6, 'active', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(50, 'LA00050', 'Chair', 4, '', '', '2014-07-22', 3, 'disposed', '', 'IDR', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-29 11:03:16', '', 1, NULL, NULL, NULL, NULL),
(51, '53de58ac82490', 'Acer DX450', 13, '', 'Acer Desktop DX450 Complete Set', '2013-07-03', 1, 'active', '', '', 0, '4500000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-03 15:43:40', '', 1, '2014-07-03', NULL, NULL, NULL),
(52, 'A020001', 'Acer Aspirce V5-122P', 14, '', 'Notebook Acer V5 Touch untuk Direksi', '2013-08-03', 1, 'active', 'For Directors', '', 6750000, '6750000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-03 15:47:53', '', 1, '2014-08-03', NULL, NULL, NULL),
(53, '53dfcf431c6d0', 'Safe locker for petty cash', 81, '', '', '2014-09-03', 2, 'active', '', 'IDR', 0, '1.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-04 18:21:55', '', 1, '2014-08-05', NULL, NULL, NULL),
(54, '5535e840da49c', 'Laptop Lenovo LaVie Z410', 1, '', 'Laptop Lenovo LaVie Z410', '2015-01-25', 3, 'active', '', '', 0, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-04-21 06:03:44', '', 1, '2015-04-21', NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_asset_group`;
CREATE TABLE IF NOT EXISTS `t_asset_group` (
  `group_id` int(11) unsigned NOT NULL,
  `group_parent_id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL DEFAULT '',
  `group_description` text NOT NULL,
  `group_tag` varchar(5) NOT NULL,
  `custom_field_1` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `custom_type_1` enum('varchar','datetime','bit','decimal') CHARACTER SET latin1 DEFAULT NULL,
  `custom_field_2` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `custom_type_2` enum('varchar','datetime','bit','decimal') CHARACTER SET latin1 DEFAULT NULL,
  `custom_field_3` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `custom_type_3` enum('varchar','datetime','bit','decimal') CHARACTER SET latin1 DEFAULT NULL,
  `custom_field_4` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `custom_type_4` enum('varchar','datetime','bit','decimal') CHARACTER SET latin1 DEFAULT NULL,
  `custom_field_5` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `custom_type_5` enum('varchar','datetime','bit','decimal') CHARACTER SET latin1 DEFAULT NULL,
  `custom_field_6` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `custom_type_6` enum('varchar','datetime','bit','decimal') CHARACTER SET latin1 DEFAULT NULL,
  `custom_field_7` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `custom_type_7` enum('varchar','datetime','bit','decimal') CHARACTER SET latin1 DEFAULT NULL,
  `custom_field_8` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `custom_type_8` enum('varchar','datetime','bit','decimal') CHARACTER SET latin1 DEFAULT NULL,
  `custom_field_9` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `custom_type_9` enum('varchar','datetime','bit','decimal') CHARACTER SET latin1 DEFAULT NULL,
  `custom_field_10` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `custom_type_10` enum('varchar','datetime','bit','decimal') CHARACTER SET latin1 DEFAULT NULL,
  `acc_id_cost` int(11) DEFAULT NULL,
  `acc_id_accdepre` int(11) DEFAULT NULL,
  `acc_id_depre` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8;

INSERT INTO `t_asset_group` (`group_id`, `group_parent_id`, `group_name`, `group_description`, `group_tag`, `custom_field_1`, `custom_type_1`, `custom_field_2`, `custom_type_2`, `custom_field_3`, `custom_type_3`, `custom_field_4`, `custom_type_4`, `custom_field_5`, `custom_type_5`, `custom_field_6`, `custom_type_6`, `custom_field_7`, `custom_type_7`, `custom_field_8`, `custom_type_8`, `custom_field_9`, `custom_type_9`, `custom_field_10`, `custom_type_10`, `acc_id_cost`, `acc_id_accdepre`, `acc_id_depre`) VALUES
(1, 0, 'Computer', 'Computer', 'A', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', 8, 31, 70),
(2, 0, 'Com. Device', 'Com. Device', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 0, 'Furniture', 'Furniture', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 0, 'Electronic', 'Electronic', 'D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 0, 'Vehicle', 'Vehicle', 'E', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 0, 'Equipment', 'Equipment', 'F', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 0, 'Heavy Equipment', 'Heavy Equipment', 'G', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 0, 'CCU', 'CCU', 'H', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 0, 'Tools', 'Tools', 'I', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 0, 'Safety', 'Safety', 'J', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 0, 'Parts', 'Parts', 'K', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 0, 'Building/Land', 'Building/Land', 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 1, 'PC', 'PC', '01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 1, 'Notebook', 'Notebook', '02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 1, 'Printer', 'Printer', '03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 1, 'Keyboard', 'Keyboard', '04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 1, 'Modem', 'Modem', '05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 1, 'Catridge', 'Catridge', '06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 1, 'Speaker', 'Speaker', '07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 1, 'LCD', 'LCD', '08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 1, 'Projector', 'Projector', '09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 1, 'Copy Machine', 'Copy Machine', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 1, 'Software', 'Software', '11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 1, 'Monitor', 'Monitor', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 1, 'Server', 'Server', '13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 1, 'Hardisk', 'Hardisk', '14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 1, 'Data Card', 'Data Card', '15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 1, 'Port Replicator', 'Port Replicator', '16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 1, 'Memory Card', 'Memory Card', '17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 1, 'Adaptor', 'Adaptor', '18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 1, 'UPS', 'UPS', '19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 1, 'Notebook Battery', 'Notebook Battery', '20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 1, 'Scanner', 'Scanner', '21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 1, 'Docking Station', 'Docking Station', '22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 1, 'Firewall', 'Firewall', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 1, 'SQL Server', 'SQL Server', '24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 1, 'Server FMS', 'Server FMS', '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 1, '3com Superstack', '3com Superstack', '26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 1, 'KVM Switch', 'KVM Switch', '27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 1, 'Cabling Server', 'Cabling Server', '28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 1, 'Memory Server', 'Memory Server', '29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 1, 'Switch', 'Switch', '30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 1, 'Broadcom NetXtreme', 'Broadcom NetXtreme', '31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 1, 'USB Port', 'USB Port', '32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 1, 'Barcode Scanner', 'Barcode Scanner', '33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 1, 'Backup Assist', 'Backup Assist', '34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 2, 'Blackberry', 'Blackberry', '01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 2, 'Handphone', 'Handphone', '02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 2, 'Handytalky', 'Handytalky', '03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 2, 'Radio Com.', 'Radio Com.', '04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 2, 'Telephone', 'Telephone', '05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 2, 'Fax', 'Fax', '06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 2, 'Headset', 'Headset', '07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 2, 'HT', 'HT', '08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 2, 'Icom', 'Icom', '09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 2, 'PABX', 'PABX', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 2, 'Confrence Call Tools', 'Confrence Call Tools', '11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 2, 'Teleconference', 'Teleconference', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 2, 'Router', 'Router', '13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 3, 'Desk/Table', 'Desk/Table', '01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 3, 'Chair', 'Chair', '02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 3, 'Spring Bed', 'Spring Bed', '03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 3, 'Drawer/Cabinet', 'Drawer/Cabinet', '04', 'Stack', 'decimal', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', NULL, NULL, NULL),
(64, 3, 'Mirror', 'Mirror', '05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 3, 'Spring Bed', 'Spring Bed', '06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 3, 'Pallet Round Bar', 'Pallet Round Bar', '07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 3, '', '', '08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 3, 'Refrigerator', 'Refrigerator', '09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 3, 'Treadmil', 'Treadmil', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 3, 'Upright Bike', 'Upright Bike', '11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 3, 'Gym System', 'Gym System', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 3, 'Kettler Rowing Cambridge', 'Kettler Rowing Cambridge', '13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 3, 'Massage Chair', 'Massage Chair', '14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 3, 'Dispenser', 'Dispenser', '15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 3, 'Washing Machine', 'Washing Machine', '16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 3, 'Workstation', 'Workstation', '17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 3, 'Bench', 'Bench', '18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 3, 'Bed', 'Bed', '19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, 3, 'Cupboard ', 'Cupboard ', '20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 3, 'Rack', 'Rack', '21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 3, 'Locker', 'Locker', '22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 3, 'Partition', 'Partition', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, 3, 'Mattress', 'Mattress', '24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, 3, 'File', 'File', '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, 3, 'Crezenda', 'Crezenda', '26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, 3, 'Kitchen Cabinet', 'Kitchen Cabinet', '27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 3, 'Roller Blind', 'Roller Blind', '28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 3, 'Fency', 'Fency', '29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, 3, 'Stand Lamp', 'Stand Lamp', '30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, 4, 'AC', 'AC', '01', 'PK', 'decimal', 'Size', 'decimal', '', 'datetime', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', '', 'varchar', NULL, NULL, NULL),
(91, 4, 'Dispenser', 'Dispenser', '02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, 4, 'Camera', 'Camera', '03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, 4, 'Power Supply', 'Power Supply', '04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, 4, 'Refrigerator', 'Refrigerator', '05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 4, 'CCTV', 'CCTV', '06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, 4, 'TV', 'TV', '07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, 4, 'Water Heater', 'Water Heater', '08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, 4, 'Fingerspot', 'Fingerspot', '09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, 4, 'Amplifier', 'Amplifier', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 4, 'Switchboard', 'Switchboard', '11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, 4, 'Microwave', 'Microwave', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, 4, 'Sound System', 'Sound System', '13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, 4, 'Air Cooler', 'Air Cooler', '14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 4, 'Paper Shredder', 'Paper Shredder', '15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, 4, 'CD MP3', 'CD MP3', '16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, 4, 'Fridge', 'Fridge', '17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, 4, 'Telkom Vision', 'Telkom Vision', '18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, 4, 'Freezer Box', 'Freezer Box', '19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, 4, 'DVR', 'DVR', '20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, 5, 'Motorcycle', 'Motorcycle', '01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, 5, 'Car', 'Car', '02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 5, 'Bicycle', 'Bicycle', '03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 6, 'Drum', 'Drum', '01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 6, 'Galoon', 'Galoon', '02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 6, 'Kettle', 'Kettle', '03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 6, 'Safety Box', 'Safety Box', '04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 6, 'White Board', 'White Board', '05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 6, 'Photo Frame', 'Photo Frame', '06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, 6, 'Plat', 'Plat', '07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, 6, 'Pipe', 'Pipe', '08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 6, 'Lighting Tower', 'Lighting Tower', '09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, 6, 'Trolley', 'Trolley', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, 6, 'Towable Bladder', 'Towable Bladder', '11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, 6, 'Hydraulic Jack', 'Hydraulic Jack', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, 6, 'Diesel Machine', 'Diesel Machine', '13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, 6, 'Fan', 'Fan', '14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, 6, 'Chain Block', 'Chain Block', '15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 6, 'Stairs', 'Stairs', '16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 6, 'Summersible Pump', 'Summersible Pump', '17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 6, 'Hydrant', 'Hydrant', '18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, 6, 'Pipe Cutting', 'Pipe Cutting', '19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, 6, 'Air Compressor', 'Air Compressor', '20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, 6, 'Dorkas Trunk', 'Dorkas Trunk', '21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, 6, 'Ladder', 'Ladder', '22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, 6, 'Box', 'Box', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(136, 6, 'Paint Storage', 'Paint Storage', '24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, 6, 'Basket', 'Basket', '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, 6, 'Offshore Basket', 'Offshore Basket', '26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, 6, 'Map', 'Map', '27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(140, 6, 'Brankas', 'Brankas', '28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(141, 6, 'Juicer Steel', 'Juicer Steel', '29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(142, 6, 'Antenna', 'Antenna', '30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(143, 6, 'Pallet', 'Pallet', '31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, 6, 'Tool Box', 'Tool Box', '32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, 6, 'Jet Pump', 'Jet Pump', '33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(146, 7, 'Tank Holder', 'Tank Holder', '01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(147, 7, 'Wheel Loader', 'Wheel Loader', '02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(148, 7, 'Baseblock', 'Baseblock', '03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, 8, 'Dry Container', 'Dry Container', '01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(150, 8, 'Office Container', 'Office Container', '02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(151, 8, 'Trash Skip', 'Trash Skip', '03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(152, 8, 'Conex', 'Conex', '04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(153, 8, 'Mini Container', 'Mini Container', '05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(154, 8, 'Storage Container', 'Storage Container', '06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(155, 8, 'Portacamp', 'Portacamp', '07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(156, 8, 'Toilet Container', 'Toilet Container', '08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(157, 8, 'Waste Skip', 'Waste Skip', '09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(158, 8, 'Climate Container', 'Climate Container', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(159, 8, 'Water Tank', 'Water Tank', '11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(160, 8, 'Solar Tank', 'Solar Tank', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(161, 8, 'Convex Mirror', 'Convex Mirror', '13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(162, 8, 'Fuel Tank', 'Fuel Tank', '14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(163, 8, 'Container', 'Container', '15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(164, 8, 'Tank Holder', 'Tank Holder', '16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(165, 9, 'Impact Drill', 'Impact Drill', '01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(166, 9, 'Nozzle', 'Nozzle', '02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(167, 9, 'Water Pump', 'Water Pump', '03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(168, 9, 'Genset Panel', 'Genset Panel', '04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(169, 9, 'Genset', 'Genset', '05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(170, 9, 'Fuel Pump', 'Fuel Pump', '06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(171, 9, 'Fogging Machine', 'Fogging Machine', '07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(172, 9, 'Water High Pressure', 'Water High Pressure', '08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(173, 9, 'Load Cell', 'Load Cell', '09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(174, 9, 'Fill Rite', 'Fill Rite', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(175, 9, 'Multi Skimmer', 'Multi Skimmer', '11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(176, 9, 'Metal Detector', 'Metal Detector', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(177, 9, 'Portable Gas Pipe', 'Portable Gas Pipe', '13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(178, 9, 'Welding Machine', 'Welding Machine', '14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(179, 9, 'Typing Machine', 'Typing Machine', '15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(180, 9, 'Smoke Detector', 'Smoke Detector', '16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(181, 9, 'Mobile Lighting Pole', 'Mobile Lighting Pole', '17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(182, 9, 'Torsimeter', 'Torsimeter', '18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(183, 9, 'Fresh Water Flexible Hose', 'Fresh Water Flexible Hose', '19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(184, 9, 'Mirror Detector', 'Mirror Detector', '20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(185, 9, 'Tent', 'Tent', '21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(186, 9, 'Grafting', 'Grafting', '22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(187, 9, 'Metal Sheet', 'Metal Sheet', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(188, 9, 'Oil Trap', 'Oil Trap', '24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(189, 9, 'File', 'File', '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(190, 9, 'Ragum', 'Ragum', '26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(191, 9, 'Dig Caliper', 'Dig Caliper', '27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(192, 9, 'Binding Machine', 'Binding Machine', '28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(193, 9, 'Laser Distance Meter', 'Laser Distance Meter', '29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(194, 9, 'Power Supply', 'Power Supply', '30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(195, 9, 'Access Lock Door', 'Access Lock Door', '31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(196, 9, 'GPS Map', 'GPS Map', '32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(197, 9, 'GPS', 'GPS', '33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(198, 9, 'Fingerscan Absence', 'Fingerscan Absence', '34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(199, 9, 'Vacuum Cleaner', 'Vacuum Cleaner', '35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(200, 9, 'Portable Lamp', 'Portable Lamp', '36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(201, 9, 'Light', 'Light', '37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(202, 9, 'Fuel Rite', 'Fuel Rite', '38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(203, 9, 'Cut Off Machine', 'Cut Off Machine', '39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(204, 9, 'Tool Storage', 'Tool Storage', '40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(205, 9, 'Steel Strapping Tool', 'Steel Strapping Tool', '41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(206, 9, 'Stretcher', 'Stretcher', '42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(207, 9, 'Folding Stacher', 'Folding Stacher', '43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(208, 9, 'Lamp', 'Lamp', '44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(209, 9, 'Lever Block', 'Lever Block', '45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(210, 9, 'Digital Weights', 'Digital Weights', '46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(211, 10, 'Fire Alarm', 'Fire Alarm', '01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(212, 10, 'Safety Sign', 'Safety Sign', '02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(213, 10, 'Mobile Security Post', 'Mobile Security Post', '03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(214, 10, 'Emergency Shower', 'Emergency Shower', '04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(215, 10, 'Fire Hose', 'Fire Hose', '05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(216, 10, 'Spine Board', 'Spine Board', '06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(217, 10, 'Immobilizer', 'Immobilizer', '07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(218, 10, 'Neck Collar', 'Neck Collar', '08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(219, 10, 'Gas Detector', 'Gas Detector', '09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(220, 10, 'Noise Meter', 'Noise Meter', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(221, 10, 'Breat Analyzer', 'Breat Analyzer', '11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(222, 10, 'Oil Spill', 'Oil Spill', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(223, 10, 'Boot ', 'Boot ', '13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(224, 10, 'Firex', 'Firex', '14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(225, 10, 'SCBA', 'SCBA', '15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(226, 10, 'Nomex', 'Nomex', '16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(227, 10, 'Helmet', 'Helmet', '17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(228, 10, 'Glove', 'Glove', '18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(229, 10, 'Eyewash', 'Eyewash', '19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(230, 10, 'Breath Alcohol Tester', 'Breath Alcohol Tester', '20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(231, 10, 'Fire Extinguisher Shelter', 'Fire Extinguisher Shelter', '21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(232, 10, 'Fire Extinguisher', 'Fire Extinguisher', '22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(233, 10, 'Bofy Harness', 'Bofy Harness', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(234, 10, 'Webbing Lanyard', 'Webbing Lanyard', '24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(235, 10, 'Guard Hut', 'Guard Hut', '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(236, 10, 'Sanchoc Lanyard', 'Sanchoc Lanyard', '26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(237, 10, 'Fire Clothing', 'Fire Clothing', '27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(238, 10, 'Safety Board', 'Safety Board', '28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(239, 10, 'Sanchoc Lanyard', 'Sanchoc Lanyard', '29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(240, 10, 'Shelter Eye Wash', 'Shelter Eye Wash', '30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(241, 11, 'Load Cell', 'Load Cell', '01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(242, 11, 'Fuel Filter', 'Fuel Filter', '02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(243, 11, 'Valve', 'Valve', '03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(244, 11, 'Sea Water Connection System', 'Sea Water Connection System', '04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(245, 11, 'Water Meter', 'Water Meter', '05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(246, 11, 'Valve Meter', 'Valve Meter', '06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(247, 11, 'Compressor AC', 'Compressor AC', '07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(248, 11, 'Ampere Meter', 'Ampere Meter', '08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(249, 11, 'MCCB', 'MCCB', '09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(250, 12, 'Container Shelter', 'Container Shelter', '01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(251, 12, 'Warehouse', 'Warehouse', '02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(252, 12, 'Lighting', 'Lighting', '03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(253, 12, 'Hedge', 'Hedge', '04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(254, 12, 'Electrical', 'Electrical', '05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(255, 12, 'Emergency Exit Door', 'Emergency Exit Door', '06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(256, 12, 'Paving', 'Paving', '07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(257, 12, 'Toilet', 'Toilet', '08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(258, 12, 'Drum Shelter', 'Drum Shelter', '09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(259, 12, 'Fuel Shelter', 'Fuel Shelter', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(260, 12, 'Paint Storage', 'Paint Storage', '11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(261, 12, 'Musholla', 'Musholla', '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(262, 12, 'Drive Rest Shelter', 'Drive Rest Shelter', '13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(263, 12, 'Security Pos', 'Security Pos', '14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(264, 12, 'Gresik Land', 'Gresik Land', '15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(265, 12, 'Office Decoration', 'Office Decoration', '16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(266, 12, 'Office Renovation', 'Office Renovation', '17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(267, 12, 'Floor Hings', 'Floor Hings', '18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(268, 12, 'HR Office Room', 'HR Office Room', '19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(269, 12, 'Accounting Room', 'Accounting Room', '20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(270, 12, 'Acoustic/Wiring', 'Acoustic/Wiring', '21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(271, 12, 'Door', 'Door', '22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(272, 12, 'Septic Tank', 'Septic Tank', '23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(273, 12, 'Waste Water Storage Tank', 'Waste Water Storage Tank', '24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_asset_group` (`group_id`, `group_parent_id`, `group_name`, `group_description`, `group_tag`, `custom_field_1`, `custom_type_1`, `custom_field_2`, `custom_type_2`, `custom_field_3`, `custom_type_3`, `custom_field_4`, `custom_type_4`, `custom_field_5`, `custom_type_5`, `custom_field_6`, `custom_type_6`, `custom_field_7`, `custom_type_7`, `custom_field_8`, `custom_type_8`, `custom_field_9`, `custom_type_9`, `custom_field_10`, `custom_type_10`, `acc_id_cost`, `acc_id_accdepre`, `acc_id_depre`) VALUES
(274, 12, 'Tower Emergency', 'Tower Emergency', '25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_asset_request_detail`;
CREATE TABLE IF NOT EXISTS `t_asset_request_detail` (
  `request_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_asset_request_detail` (`request_id`, `asset_id`) VALUES
(1, 23),
(1, 27),
(5, 23),
(5, 27),
(6, 1),
(6, 4),
(7, 1),
(9, 1),
(10, 1),
(11, 52),
(12, 52),
(13, 52),
(14, 52),
(15, 51),
(15, 52);

DROP TABLE IF EXISTS `t_asset_request_header`;
CREATE TABLE IF NOT EXISTS `t_asset_request_header` (
  `request_id` int(11) NOT NULL,
  `document_ref` varchar(20) NOT NULL,
  `location_id` int(11) NOT NULL,
  `requested_by` varchar(255) NOT NULL,
  `requested_for` varchar(255) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `note` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `request_status` enum('pending','partially approved','approved','revise','rejected') NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

INSERT INTO `t_asset_request_header` (`request_id`, `document_ref`, `location_id`, `requested_by`, `requested_for`, `date_from`, `date_to`, `note`, `timestamp`, `request_status`) VALUES
(1, 'AR0607140001', 1, '1', 'Self', '2014-07-06', '2014-07-06', '', '2014-07-06 14:11:05', 'pending'),
(5, 'AR0607140002', 1, '1', 'Self', '2014-07-06', '2014-07-06', '', '2014-07-06 14:13:12', 'pending'),
(6, 'AR0607140003', 1, '1', 'Self', '2014-07-06', '2014-07-06', 'Approval Test', '2014-07-06 14:17:55', 'pending'),
(7, 'AR0208140001', 1, '1', 'Self', '2014-08-02', '2014-08-02', 'test1', '2014-08-02 14:14:19', 'pending'),
(9, 'AR0208140002', 1, '1', 'Self', '2014-08-02', '2014-08-02', 'test1', '2014-08-02 14:32:24', 'pending'),
(10, 'AR0208140003', 1, '1', 'Self', '2014-08-02', '2014-08-02', 'test1', '2014-08-02 14:34:46', 'pending'),
(11, 'AR0408140001', 1, '1', 'Self', '2014-08-04', '2014-08-04', 'Request Laptop', '2014-08-04 16:44:06', 'pending'),
(12, 'AR0408140002', 1, '1', 'Self', '2014-08-04', '2014-08-04', 'test', '2014-08-04 16:52:47', 'pending'),
(13, 'AR0408140003', 1, '1', 'Self', '2014-08-04', '2014-08-04', 'test 2', '2014-08-04 16:55:32', 'pending'),
(14, 'AR0508140001', 1, '1', 'Self', '2014-08-04', '2014-08-04', 'test @', '2014-08-04 17:02:36', 'pending'),
(15, 'AR2104150001', 1, '1', 'Self', '2015-04-21', '2015-04-21', 'Usage request', '2015-04-21 06:35:22', 'pending');

DROP TABLE IF EXISTS `t_asset_trans`;
CREATE TABLE IF NOT EXISTS `t_asset_trans` (
  `request_id` int(11) NOT NULL,
  `document_ref` varchar(20) NOT NULL,
  `location_id` int(11) NOT NULL,
  `requested_by` varchar(255) NOT NULL,
  `transaction_type` enum('move','disposal','depreciation') NOT NULL DEFAULT 'move',
  `transaction_date` date NOT NULL DEFAULT '0000-00-00',
  `note` text NOT NULL,
  `request_status` enum('pending','partially approved','approved','revise','rejected') NOT NULL DEFAULT 'pending',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `new_location_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

INSERT INTO `t_asset_trans` (`request_id`, `document_ref`, `location_id`, `requested_by`, `transaction_type`, `transaction_date`, `note`, `request_status`, `timestamp`, `new_location_id`) VALUES
(2, 'DR0607140001', 1, '1', 'disposal', '2014-07-06', 'Note 1', 'approved', '2014-07-06 09:00:24', NULL),
(3, 'DR0607140002', 1, '1', 'disposal', '2014-07-06', 'Approval test', 'approved', '2014-07-06 09:51:05', NULL),
(4, 'DR0607140003', 1, '1', 'disposal', '2014-07-06', 'approval test', 'approved', '2014-07-06 09:55:37', NULL),
(5, 'DR0607140004', 1, '1', 'disposal', '2014-07-06', '', 'approved', '2014-07-06 10:06:19', NULL),
(6, 'DR0607140005', 1, '1', 'disposal', '2014-07-06', 'Test approval', 'approved', '2014-07-06 10:08:14', NULL),
(7, 'DR0607140006', 1, '1', 'disposal', '2014-07-06', 'Dispose 2', 'approved', '2014-07-06 10:08:28', NULL),
(8, 'DR0607140007', 1, '1', 'disposal', '2014-07-06', 'test msg approval', 'approved', '2014-07-06 10:13:09', NULL),
(9, 'DR0607140008', 1, '1', 'disposal', '2014-07-06', 'Note Email test approval at local', 'approved', '2014-07-06 11:20:56', NULL),
(11, 'DR0607140009', 1, '1', 'disposal', '2014-07-06', 'New Approval method test 1 by email', 'approved', '2014-07-06 12:45:07', NULL),
(12, 'DR0607140010', 1, '1', 'disposal', '2014-07-07', 'Test custom function ina pproval', 'approved', '2014-07-06 12:51:09', NULL),
(13, 'DR0607140011', 1, '1', 'disposal', '2014-07-04', 'Test muti user in position approval email', 'approved', '2014-07-06 12:57:22', NULL),
(14, 'MR0607140001', 1, '1', 'move', '2014-07-06', 'Move incoming goods from airport directly to project site at Senayan', 'pending', '2014-07-06 13:09:06', 4),
(16, 'MR0607140002', 1, '1', 'move', '2014-07-06', 'Move incoming goods from airport directly to project site at Senayan', 'pending', '2014-07-06 13:11:49', 4),
(17, 'MR0607140003', 1, '1', 'move', '2014-07-07', 'Move to Kemang!', 'pending', '2014-07-06 13:12:16', 1),
(19, 'MR0508140001', 1, '2', 'move', '2014-08-04', 'Untuk proyek cilandak', 'pending', '2014-08-04 17:45:39', 3),
(20, 'MR2104150001', 1, '1', 'move', '2015-04-21', 'Pindah kantor', 'pending', '2015-04-21 06:38:31', 2),
(21, 'MR2104150001', 1, '1', 'move', '2015-04-21', 'Pindah kantor', 'pending', '2015-04-21 06:38:52', 2),
(22, 'MR2705150001', 1, '1', 'move', '2015-05-27', '', 'pending', '2015-05-27 08:26:07', 2),
(23, 'AD2805150001', 1, '1', '', '2015-05-28', 'test', 'approved', '2015-05-27 19:23:44', NULL),
(24, 'AD2805150001', 1, '4', '', '2015-04-30', 'depre', 'approved', '2015-05-27 19:29:28', NULL),
(25, 'AD2805150001', 2, '5', '', '2015-05-28', 'test depre1', 'approved', '2015-05-27 19:36:05', NULL),
(26, 'AD2805150001', 0, '5', 'depreciation', '2015-05-28', '', 'approved', '2015-05-27 19:40:25', NULL),
(27, 'AD2805150001', 0, '5', 'depreciation', '2015-05-28', 'depreciation', 'approved', '2015-05-27 20:08:48', NULL);

DROP TABLE IF EXISTS `t_asset_trans_detail`;
CREATE TABLE IF NOT EXISTS `t_asset_trans_detail` (
  `request_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `status` enum('pending','in transit','received','disposed') NOT NULL DEFAULT 'pending',
  `transaction_date` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_asset_trans_detail` (`request_id`, `asset_id`, `status`, `transaction_date`) VALUES
(2, 3, 'pending', '0000-00-00'),
(3, 20, 'pending', '0000-00-00'),
(4, 7, 'pending', '0000-00-00'),
(5, 22, 'pending', '0000-00-00'),
(5, 23, 'pending', '0000-00-00'),
(6, 27, 'pending', '0000-00-00'),
(6, 45, 'pending', '0000-00-00'),
(6, 48, 'pending', '0000-00-00'),
(7, 27, 'pending', '0000-00-00'),
(8, 21, 'pending', '0000-00-00'),
(8, 24, 'pending', '0000-00-00'),
(9, 40, 'pending', '0000-00-00'),
(11, 9, 'pending', '0000-00-00'),
(12, 16, 'pending', '0000-00-00'),
(12, 19, 'pending', '0000-00-00'),
(12, 40, 'pending', '0000-00-00'),
(12, 46, 'pending', '0000-00-00'),
(12, 50, 'pending', '0000-00-00'),
(13, 6, 'pending', '0000-00-00'),
(13, 8, 'pending', '0000-00-00'),
(14, 12, 'pending', '0000-00-00'),
(14, 13, 'pending', '0000-00-00'),
(14, 14, 'pending', '0000-00-00'),
(16, 12, 'pending', '0000-00-00'),
(16, 13, 'pending', '0000-00-00'),
(16, 14, 'pending', '0000-00-00'),
(17, 10, 'pending', '0000-00-00'),
(17, 17, 'pending', '0000-00-00'),
(17, 26, 'pending', '0000-00-00'),
(17, 32, 'pending', '0000-00-00'),
(19, 51, 'pending', '0000-00-00'),
(20, 51, 'pending', '0000-00-00'),
(21, 51, 'pending', '0000-00-00'),
(22, 51, 'pending', '0000-00-00'),
(23, 2, 'pending', '0000-00-00'),
(24, 2, 'pending', '0000-00-00'),
(25, 52, 'pending', '0000-00-00'),
(26, 2, 'pending', '0000-00-00'),
(27, 2, 'pending', '0000-00-00');

DROP TABLE IF EXISTS `t_asset_type`;
CREATE TABLE IF NOT EXISTS `t_asset_type` (
  `group_id` int(11) unsigned NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_business`;
CREATE TABLE IF NOT EXISTS `t_business` (
  `buss_id` int(11) NOT NULL,
  `buss_code` varchar(50) NOT NULL,
  `buss_char` varchar(5) DEFAULT NULL,
  `buss_name` varchar(100) NOT NULL,
  `is_customer` tinyint(1) NOT NULL,
  `is_vendor` tinyint(1) NOT NULL,
  `buss_group` int(11) NOT NULL,
  `buss_type` varchar(100) DEFAULT NULL,
  `buss_addr` varchar(500) DEFAULT NULL,
  `buss_city` varchar(100) DEFAULT NULL,
  `buss_state` varchar(100) DEFAULT NULL,
  `buss_country` varchar(100) DEFAULT NULL,
  `buss_website` varchar(100) DEFAULT NULL,
  `buss_tlp1` varchar(25) DEFAULT NULL,
  `buss_tlp2` varchar(25) DEFAULT NULL,
  `buss_email` varchar(200) DEFAULT NULL,
  `buss_contact` varchar(50) DEFAULT NULL,
  `buss_curr` varchar(5) DEFAULT NULL,
  `limit_local` decimal(10,0) DEFAULT '0',
  `buss_status` varchar(50) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

INSERT INTO `t_business` (`buss_id`, `buss_code`, `buss_char`, `buss_name`, `is_customer`, `is_vendor`, `buss_group`, `buss_type`, `buss_addr`, `buss_city`, `buss_state`, `buss_country`, `buss_website`, `buss_tlp1`, `buss_tlp2`, `buss_email`, `buss_contact`, `buss_curr`, `limit_local`, `buss_status`) VALUES
(1, 'SPL-0001', 'C01', 'Charles', 0, 1, 1, 'Maklun', 'Taman Kopo Indah, Blok Z No.99, Margahayu Utara, Babakan Ciparay', 'Bandung', 'Jawa Barat', 'Indonesia', '', '08112515xxxx', NULL, NULL, 'Charles', 'IDR', NULL, 'ACTIVE'),
(2, 'SPL-0002', 'D01', 'Darryl', 0, 1, 1, 'Maklun', 'Jl. Lengkong No.39, Cikawao, Lengkong', 'Bandung', 'Jawa Barat', 'Indonesia', '', '08193211xxxx', '', '', 'Darryl', 'IDR', NULL, 'ACTIVE'),
(3, 'SPL-0003', 'S01', 'Shirley', 0, 1, 1, 'Maklun', 'Jl. Peta No.137, Margasari, Buahbatu', 'Bandung', 'Jawa Barat', 'Indonesia', '', '08172271xxxx', NULL, NULL, 'Shirley', 'IDR', NULL, 'ACTIVE'),
(4, 'SPL-0004', 'D02', 'Desmond', 0, 1, 1, 'Maklun', 'Jl. Pajajaran No.993, Pajajaran, Cicendo', 'Bandung', 'Jawa Barat', 'Indonesia', '', '08199201xxxx', NULL, NULL, 'Desmond', 'IDR', NULL, 'ACTIVE'),
(5, 'RSL-01', 'ST1', 'Steve', 1, 0, 0, 'Reseller', 'Jl. Tomang Raya No.312, Tomang, Grogol Petamburan', 'Jakarta Barat', 'DKI Jakarta', 'Indonesia', '', '08571866xxxx', '', '', 'Steve', 'IDR', '3000000', 'ACTIVE'),
(6, 'RSL-02', 'BR1', 'Brian', 1, 0, 0, 'Reseller', 'Grand Slipi Tower Lt 99 No. 134, Jl. Letjen S. Parman Kav 22-24', 'Jakarta Barat', 'DKI Jakarta', 'Indonesia', '', '08135559xxxx', NULL, '', 'Brian', 'IDR', '3000000', 'ACTIVE'),
(7, 'OTC', 'OTC', 'One-Time Customer', 1, 0, 0, 'Customer', '', '', '', '', '', '08193338xxxx', NULL, NULL, 'One-Time Customer', 'IDR', '0', 'ACTIVE'),
(8, 'OTC-0001', '', 'Lucy', 1, 0, 0, 'Customer', 'Mediterania Boulevard, Flower 1 No. 998, Kamal Muara, Penjaringan', 'Jakarta Utara', 'DKI Jakarta', 'Indonesia', '', '08170702xxxx', '', '', 'Lucy', 'IDR', '1000000', 'ACTIVE'),
(9, 'OTC-0002', '', 'Jeremy', 1, 0, 0, 'Customer', 'Tanjung Duren Utara No. 19, Tanjung Duren Utara, Grogol Petamburan', 'Jakarta Barat', 'DKI Jakarta', 'Indonesia', '', '08116837xxxx', NULL, NULL, 'Jeremy', 'IDR', '1500000', 'ACTIVE'),
(10, 'OTC-0003', '', 'Hansen', 1, 0, 0, 'Customer', 'Jl. Pahlawan No.6629, Medan Johor', 'Medan', 'Sumatra Utara', 'Indonesia', '', '08572866xxxx', NULL, NULL, 'Hansen', 'IDR', '1000000', 'ACTIVE'),
(11, 'OTC-0004', '', 'Yessy', 1, 0, 0, 'Customer', 'Jl. Presiden No.121, Manggala, Manggala', 'Makassar', 'Sulawesi Selatan', 'Indonesia', '', '08789233xxxx', NULL, NULL, 'Yessy', 'IDR', '2000000', 'ACTIVE'),
(12, 'OTC-0005', '', 'Susanne', 1, 0, 0, 'Customer', 'Jl. Patimura No.158, Desa Latuhalat, Nusaniwe', 'Ambon', 'Maluku', 'Indonesia', '', '08397248xxxx', NULL, NULL, 'Susanne', 'IDR', '1500000', 'ACTIVE'),
(13, 'OTC-0006', '', 'Linda', 1, 0, 0, 'Customer', 'Jl. Panjang No.277, Oetete, Oebobo', 'Kupang', 'Nusa Tenggara Timur', 'Indonesia', '', '08127542xxxx', NULL, NULL, 'Linda', 'IDR', '1500000', 'ACTIVE'),
(14, 'OTC-0007', '', 'Adriana', 1, 0, 0, 'Customer', 'Jl. Merdeka No.129, Pantoloan, Palu Utara', 'Palu', 'Sulawesi Barat', 'Indonesia', '', '08137750xxxx', NULL, NULL, 'Adriana', 'IDR', '1000000', 'ACTIVE'),
(15, 'OTC-0008', '', 'Freddy', 1, 0, 0, 'Customer', 'Jl. Bambu No.18, Joglo, Kembangan', 'Jakarta Barat', 'DKI Jakarta', 'Indonesia', '', '08193271xxxx', NULL, NULL, 'Freddy', 'IDR', '2000000', 'ACTIVE'),
(19, 'OTC-0012', '', 'Stephanie', 1, 0, 0, 'Customer', 'Jl. Cendikiawan No.4, Pondok Labu, Cilandak', 'Jakarta Selatan', 'DKI Jakarta', 'Indonesia', '', '08153814xxxx', '', NULL, 'Stephanie', 'IDR', '1500000', 'ACTIVE'),
(38, 'OTC-0021', '', 'Sanjay', 1, 0, 0, 'Customer', 'Jl. Gegerkalong Blok B No.75, Gegerkalong, Sukasari', 'Bandung', 'Jawa Barat', 'Indonesia', '', '08571298xxxx', '', NULL, 'Sanjay', 'IDR', '1000000', 'ACTIVE'),
(39, 'OTC-0022', '', 'Ellaine', 1, 0, 0, 'Customer', 'Jl. Elang No.329, Cisarua, Cikole', 'Sukabumi', 'Jawa Barat', 'Indonesia', '', '08912737xxxx', '', NULL, 'Ellaine', 'IDR', '1000000', 'ACTIVE'),
(40, 'OTC-0023', '', 'Ivy', 1, 0, 0, 'Customer', 'Jl. Tentara No.84, Sukamanah, Cipedes', 'Tasikmalaya', 'Jawa Barat', 'Indonesia', '', '08112148xxxx', '', NULL, 'Ivy', 'IDR', '2000000', 'ACTIVE'),
(41, 'OTC-0024', '', 'Stacy', 1, 0, 0, 'Customer', 'Jl. Pelajar No.39, Gondangdia, Menteng', 'Jakarta Pusat', 'DKI Jakarta', 'Indonesia', '', '08788323xxxx', '', NULL, 'Stacy', 'IDR', '2000000', 'ACTIVE'),
(42, 'OTC-0025', '', 'Benn', 1, 0, 0, 'Customer', 'Jl. Prajurit No.118, Rawasari, Cempaka Putih', 'Jakarta Pusat', 'DKI Jakarta', 'Indonesia', '', '08193722xxxx', '', NULL, 'Benn', 'IDR', '1000000', 'ACTIVE'),
(43, 'OTC-0026', '', 'Martin', 1, 0, 0, 'Customer', 'Jl. Cibadak No. 204, Cibadak, Astana Anyar', 'Bandung', 'Jawa Barat', 'Indonesia', '', '08124030xxxx', '', NULL, 'Martin', 'IDR', '1000000', 'ACTIVE'),
(44, 'OTC-0027', '', 'Leo', 1, 0, 0, 'Customer', 'Jl. Batik Blok D3 No.133, Pluit, Penjaringan', 'Jakarta Utara', 'DKI Jakarta', 'Indonesia', '', '08143977xxxx', '', NULL, 'Leo', 'IDR', '1500000', 'ACTIVE'),
(45, 'OTC-0028', '', 'Alex', 1, 0, 0, 'Customer', 'Jl. Wayang No.73, Duren Tiga, Pancoran', 'Jakarta Selatan', 'DKI Jakarta', 'Indonesia', '', '08359826xxxx', '', NULL, 'Alex', 'IDR', '1000000', 'ACTIVE'),
(46, 'OTC-0029', '', 'Febby', 1, 0, 0, 'Customer', 'Jl. Raya Besar No.3, Kelapa Dua, Kebon Jeruk', 'Jakarta Barat', 'DKI Jakarta', 'Indonesia', '', '08157329xxxx', '', NULL, 'Febby', 'IDR', '1000000', 'ACTIVE'),
(47, 'SPL-0005', '', 'PT. ABC', 0, 1, 1, 'Supplier', 'Jl. ABC No. 456', 'Bandung', 'Jawa Barat', 'Indonesia', 'www.abc.com', '021-41243234', '022-52038433', 'info@abc.com', NULL, 'IDR', '1000000000', 'ACTIVE'),
(48, 'SPL-0006', '', 'PT. Kreasi Digital Prima', 0, 1, 1, 'Supplier', 'Meruya Ilir', 'Jakarta Barat', 'DKI Jakarta', 'Indonesia', 'www.indoskyware.com', '021-2930-1423', '', 'developer@indoskyware.com', NULL, '', '0', 'ACTIVE'),
(49, 'SPL-0007', '', 'PT. Dika Maheswara', 0, 1, 0, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE'),
(50, 'SPL-0008', '', 'Charles', 0, 1, 0, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'INACTIVE'),
(51, 'SPL-0013', NULL, '', 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'ACTIVE'),
(52, 'SPL-0014', NULL, 'PT Hotel Indoniesa', 0, 1, 1, 'Supplier', 'Sudirman 2', 'Jakarta', 'DKI Jakarta', 'Indonesia', 'hotelindonesia.com', '021-112323', '', '', NULL, '', '0', 'ACTIVE'),
(53, 'SPL-0015', NULL, 'PT ABC', 0, 1, 1, 'Supplier', 'Jakarta', 'Jakarta', 'DKI Jakarta', 'Indonesia', '', '', '', '', NULL, '', '0', 'ACTIVE'),
(54, 'SPL-0016', NULL, 'PT XYZ', 0, 1, 1, 'Supplier', 'Jakarya', 'Jakarta', 'DKI Jakarta', 'Indonesia', '', '', '', '', NULL, '', '0', 'ACTIVE'),
(55, 'SPL-0017', NULL, 'PT IndoRaya', 0, 1, 1, 'Supplier', '', '', '', '', '', '', '', '', NULL, '', '0', 'ACTIVE'),
(56, 'SPL-0018', NULL, '', 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'ACTIVE'),
(57, 'SPL-0019', NULL, 'Coklatin', 0, 1, 1, 'Supplier', '', '', '', '', '', '', '', 'dika@coklat.in', NULL, NULL, NULL, 'ACTIVE');

DROP TABLE IF EXISTS `t_business_contact`;
CREATE TABLE IF NOT EXISTS `t_business_contact` (
  `contact_id` int(11) NOT NULL,
  `buss_id` int(11) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `contact_div` varchar(50) DEFAULT NULL,
  `contact_dept` varchar(50) DEFAULT NULL,
  `contact_email1` varchar(100) DEFAULT NULL,
  `contact_email2` varchar(100) DEFAULT NULL,
  `contact_email3` varchar(100) DEFAULT NULL,
  `contact_tlp1` varchar(50) DEFAULT NULL,
  `contact_tlp2` varchar(50) DEFAULT NULL,
  `contact_tlp3` varchar(50) DEFAULT NULL,
  `contact_address` varchar(1000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_ci_sessions`;
CREATE TABLE IF NOT EXISTS `t_ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('02f5f044d5600fc34243cd3372d88ac8784091ca', '::1', 1432755261, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735343937333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373230383138223b6d656e755f68696464656e7c4e3b),
('05599b6ac21f6010b255bc01090c3f924e21bcb2', '::1', 1434635124, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343633313230303b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363230353734223b6d656e755f68696464656e7c4e3b),
('070c8ae0366a5023dc4ef0d286d16ad0113ac279', '::1', 1454146947, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435343134363934363b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343534313436353938223b6d656e755f68696464656e7c4e3b),
('078cb2750bb9d4c18eff440e832841e34bf523e8', '139.194.87.130', 1435349596, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353334393335343b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335323036333932223b6d656e755f68696464656e7c4e3b),
('07f733afc938aae86d49bdae186fc57c45bc0784', '::1', 1454148110, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435343134373831333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343534313436353938223b6d656e755f68696464656e7c4e3b),
('08fd00af019812228504809572d2b2367731c4ec', '139.228.224.3', 1434682994, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343638323937323b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363538333037223b6d656e755f68696464656e7c4e3b),
('1050223fd5fe9bb26fb2ac9af8b380a300e8a543', '::1', 1433506716, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333530363639333b6964656e746974797c733a353a227374616666223b757365726e616d657c733a353a227374616666223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2234223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333343234373736223b6d656e755f68696464656e7c4e3b666c6173685f6e6f74696669636174696f6e7c733a32373a224572726f72206372656174696e67206e65772043617465676f7279223b5f5f63695f766172737c613a313a7b733a31383a22666c6173685f6e6f74696669636174696f6e223b733a333a226e6577223b7d),
('11bfe8c83fa9ac11ed9467af5785f3f45881fe5f', '209.190.113.85', 1436144948, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363134343934383b),
('12165b1bf8f0cacee59aa930c7e19a93474d9008', '157.55.39.230', 1436016585, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363031363538353b),
('14347ec58000c9a75bb0bb3cd144a565aafa6697', '::1', 1432748757, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323734383733333b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373131373338223b6d656e755f68696464656e7c4e3b),
('154b6d6c9b1f0ff7cd664896339d3f8d6fc1b07d', '202.51.110.178', 1435302008, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353330323030383b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334393330353931223b6d656e755f68696464656e7c4e3b),
('1692b363b9c2f574e1934182e6264c8541487fec', '::1', 1434653921, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635333834373b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('17cd2dae45dfe2fee0e694edada29262b330d463', '111.95.118.32', 1434658601, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635383330333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363437363733223b6d656e755f68696464656e7c4e3b),
('1933d4a4c8fd3b1291a2d11d78dbc1e92747dd62', '::1', 1434123599, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343132333539303b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334303434363533223b6d656e755f68696464656e7c4e3b),
('19f6ef78db24ea0ccd2fa4a68a7e5bcd9018654c', '209.190.113.85', 1436145119, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363134353131393b),
('1b68ba96e2e6fe99e3d9684eee6e978eb10b16bb', '::1', 1433431086, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333433303937363b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333323730393135223b6d656e755f68696464656e7c4e3b),
('1c9ba3172cce9c8c0f25bb4b632bd78b7f76ff2b', '139.228.224.3', 1434697478, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343639373433393b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363832393837223b6d656e755f68696464656e7c4e3b),
('1d207402b757d311f9ff284c4a1b7ea704802165', '139.0.78.139', 1435478599, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353437383333343b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335343038313931223b6d656e755f68696464656e7c4e3b),
('2592565cd884656afbc011224c35408718936581', '209.190.113.85', 1436145119, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363134353131393b),
('26a8cc4167c5b74e6042de60b217d69e893cf164', '::1', 1432788341, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323738383238383b6964656e746974797c733a353a227374616666223b757365726e616d657c733a353a227374616666223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2234223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373534393238223b6d656e755f68696464656e7c4e3b),
('27d53d97b44feeb2a50b89cf327035aa258e9ed5', '::1', 1436944045, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363934343034353b),
('28a53876fb1850f0000ea2859a4682d186241982', '139.0.78.139', 1435478686, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353437383638363b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335343038313931223b6d656e755f68696464656e7c4e3b),
('2aa23513c33ab4821242eb5c3ee444cc18899c9e', '::1', 1434045231, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343034353134303b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333393935323331223b6d656e755f68696464656e7c4e3b),
('2c1e89c854806a3655dc61c75d2ee42b53fd141e', '111.68.126.42', 1435207588, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353230373535393b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334393330353336223b6d656e755f68696464656e7c4e3b),
('2c583e358aa22e0a0a130a95a1f4503d9185507c', '::1', 1454149631, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435343134393334343b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343534313436353938223b6d656e755f68696464656e7c4e3b),
('2de9939e1af5f5e22d6bcdb61194ca0a9da05127', '207.46.13.19', 1436000494, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363030303439343b),
('2f46c4ae18ba04a88f470682ac7adc7a4ecc2012', '::1', 1454148751, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435343134383537353b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343534313436353938223b6d656e755f68696464656e7c4e3b),
('30f7c9461342c40de98748c0e65e3545b2792bcc', '::1', 1434621666, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343632313037303b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334313138323235223b6d656e755f68696464656e7c4e3b),
('32c72045d5a311dd099c978f6cee18194607b667', '139.228.224.3', 1434693504, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343639333530333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363832393837223b6d656e755f68696464656e7c4e3b),
('336148d443f2e9ff0035c7fc7b43b6c76a60ca63', '::1', 1454148229, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435343134383131343b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343534313436353938223b6d656e755f68696464656e7c4e3b),
('38db9ae8ce4278f1d6d3cd53c6eafb359f83e808', '::1', 1434652310, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635323239333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('3a36aecd720f588930f11032d3f62dc1ba70434a', '::1', 1434620574, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343632303335303b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334313138323235223b6d656e755f68696464656e7c4e3b6d6573736167657c733a32393a223c703e4c6f6767656420496e205375636365737366756c6c793c2f703e223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226f6c64223b7d),
('3a8c4026bd7d0c47f5b143bd576bfe191a41d0bb', '::1', 1433270932, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333237303930333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373937373030223b6d656e755f68696464656e7c4e3b),
('3bc323a442bd4f058269f77de427a3385a43f403', '::1', 1433428865, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333432383734373b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333323730393135223b6d656e755f68696464656e7c4e3b),
('3c68a2c91ff686243d32d49402e19b7518f111ba', '::1', 1432753586, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735333338343b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373131373338223b6d656e755f68696464656e7c4e3b),
('3d41888574f855ee21b80bf7f9de91c7b7fcf87c', '111.68.127.6', 1435564563, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353536343536333b),
('3e98ebde8657cad69946589e38a098f91ea663fc', '::1', 1434630911, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343633303835383b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363230353734223b6d656e755f68696464656e7c4e3b),
('403acad9a0d7d9ed5869f8a68f0836c632ef2be8', '142.4.218.201', 1434877101, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343837373130313b),
('42508cfbe865771a536da242e8c08d58fedaf402', '::1', 1432755627, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735353631373b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373230383138223b6d656e755f68696464656e7c4e3b),
('444306e66f5ad271672956f22e446151eb10f221', '::1', 1434655887, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635353637303b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('449832d7df635bd3a63e473dbab8d13933f2af9d', '::1', 1434044928, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343034343634373b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333393935323331223b6d656e755f68696464656e7c4e3b),
('464c7ac2f1fb13d729e986dc800db85b316d2160', '220.181.108.83', 1435303968, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353330333936373b),
('46c1e85a02871ea3925ab90625cf898fd5431441', '::1', 1432757504, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735373331353b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373230383138223b6d656e755f68696464656e7c4e3b),
('4a31505004029ddd53ede66183eba20d490d50b2', '::1', 1432750946, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735303635393b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373131373338223b6d656e755f68696464656e7c4e3b),
('4d38835217f045a95a1586c9a9def8798e23f48c', '209.190.113.85', 1436145119, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363134353131393b),
('4dd9dbad50efdcc15a00478f466b85bf71f13d7b', '139.228.224.3', 1434692023, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343639323030393b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363832393837223b6d656e755f68696464656e7c4e3b),
('4dfa906ff1913bdce59eb755b64c1a03ff75e5ef', '64.233.173.177', 1436872080, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363837323038303b),
('4e3b27cd56b34d293bb00b006bf475444ce96d71', '::1', 1434651458, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635313432353b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('50fce5061278c49771b68b5cd06b598723b6dc78', '::1', 1452305103, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435323330353038393b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343337343034323638223b6d656e755f68696464656e7c4e3b6d6573736167657c733a32393a223c703e4c6f6767656420496e205375636365737366756c6c793c2f703e223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226f6c64223b7d),
('5152ab4a13c56476434fada60dfe034c34656165', '207.46.13.138', 1435936592, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353933363539323b),
('51931d07f51f36ed4d2d933b65b76a2574d6fca2', '::1', 1433425986, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333432353936353b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333323730393135223b6d656e755f68696464656e7c4e3b),
('551cb31618bafd69944fce0e53e2c2555dd363da', '::1', 1434647980, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343634373637333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('56234673dc40de59a1d56af0b3e4cf22cf464f89', '111.95.118.32', 1434658613, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635383631333b),
('57ab76f02e73ce2c63b5d067ca6b66cd9989b765', '139.195.59.169', 1434930634, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343933303633343b),
('587a93e2b3fb9f11f0243ef98482065f3830a254', '209.190.113.85', 1436145045, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363134353034353b),
('5a927cedfc2e26fdaf946a0a84b6f8892fac02d9', '::1', 1454146809, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435343134363630393b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343534313436353938223b6d656e755f68696464656e7c4e3b),
('5becf976adaac260506ce2f7dc4e0ad69c126e2d', '::1', 1434638287, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343633383136333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363230353734223b6d656e755f68696464656e7c4e3b),
('5c049b1394102ad9d1b1232a257bbc4826eb9b60', '::1', 1433436029, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333433363032393b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333323730393135223b6d656e755f68696464656e7c4e3b),
('5c6193b2f2955d7982d526d6f0b4d408a335e290', '::1', 1432754266, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735343032373b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373131373338223b6d656e755f68696464656e7c4e3b),
('5d5d19da89ab4a74dd39b150a43c3ac651e39607', '::1', 1454149994, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435343134393639343b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343534313436353938223b6d656e755f68696464656e7c4e3b),
('5d95f897a7ffec24749af8bf86d182919890ee00', '207.46.13.19', 1436000493, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363030303439333b),
('5db543e1ed99c6a768501d13a6211fa3623b77b7', '::1', 1437404311, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433373430343331313b),
('61045b59dc77457437927b4312ae2e3bb4d6a0b5', '111.94.242.52', 1436169243, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363136393234333b),
('615373ed2e4c0b6d0f73da64cc3e61234935c8f5', '::1', 1433273033, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333237333030393b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373937373030223b6d656e755f68696464656e7c4e3b),
('6203748902d3c1444e73134f338916c468b14228', '::1', 1434620991, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343632303730373b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334313138323235223b6d656e755f68696464656e7c4e3b),
('62240e19782541d4e3136072401d0a2b934a068e', '111.68.126.42', 1435034273, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353033343237333b),
('62743a82cff6155d24044329731b674ba974365d', '64.233.173.172', 1436859348, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363835393334343b),
('6276ee5a56e43dc17a65059e408695d7e8144e1e', '207.46.13.16', 1436617506, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363631373530363b),
('65b5e9fb03bde031764696e82313a7a889ae94eb', '::1', 1434052579, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343035323437333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333393935323331223b6d656e755f68696464656e7c4e3b),
('664c3c24991a473e95eea9344ed97c25871b710a', '::1', 1433425612, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333432353334393b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333323730393135223b6d656e755f68696464656e7c4e3b),
('66ffbe608ffbd08720d8bdbd25eeb259d14476e2', '::1', 1432797954, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323739373639303b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373534393737223b6d656e755f68696464656e7c4e3b),
('678c8475dd389a65758c34883364c45df96c4ff5', '209.190.113.85', 1436144948, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363134343934383b),
('684cc35cc06a6208437f5c5fdf3ec0357765ab31', '220.181.108.96', 1436329800, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363332393739383b),
('69a2c7565c7efc71156fca899088ce3df1404bb2', '::1', 1434124633, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343132343439383b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334303434363533223b6d656e755f68696464656e7c4e3b),
('6d669e3e9c9c574170aede3bc1811916a1940c27', '::1', 1434621913, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343632313636383b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334313138323235223b6d656e755f68696464656e7c4e3b666c6173685f6e6f74696669636174696f6e7c733a32313a22556e61626c6520746f20757064617465206d656e75223b5f5f63695f766172737c613a313a7b733a31383a22666c6173685f6e6f74696669636174696f6e223b733a333a226e6577223b7d),
('6e7a8fddc9c04a2c27c0d727de7454c2d849b377', '::1', 1433424848, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333432343831363b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333323730393135223b6d656e755f68696464656e7c4e3b),
('70a314c87cf73f926ee86441eced0c4f9500a073', '112.215.66.74', 1436859302, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363835393330313b),
('76b047bc1176933f9d245f71bda45d8e72955986', '::1', 1449559000, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434393535383939393b),
('775b1ced30a4fd5bd9989a0be17186f55bb3edc0', '::1', 1432752700, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735323633383b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373131373338223b6d656e755f68696464656e7c4e3b),
('7912e1fe51dc10b85fdbdab4115a378e99b6cd96', '::1', 1434128682, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343132383638313b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334303434363533223b6d656e755f68696464656e7c4e3b),
('7ab883b4815eeb17954b11634ab239a8caf403ff', '139.0.78.139', 1435479446, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353437393138323b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335343738333437223b6d656e755f68696464656e7c4e3b),
('7c8b23b5216b2d1ae568fa83803860ecff3766cd', '::1', 1432749502, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323734393231313b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373131373338223b6d656e755f68696464656e7c4e3b),
('7c93ea64c9e0f4874ce4201c2b1c6ab60a6d4b88', '::1', 1434048218, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343034383231373b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333393935323331223b6d656e755f68696464656e7c4e3b),
('7e8b6a430b0736386644bdf41219961ac0d4d948', '::1', 1434651110, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635303832313b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('7f2a472563a4fbb47911d768a6cfb55753e02a70', '111.68.126.42', 1435213131, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353231333130343b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334393330353336223b6d656e755f68696464656e7c4e3b),
('8341c0917f464c78ec55387e9b23e1645df8727d', '::1', 1434622205, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343632313938333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334313138323235223b6d656e755f68696464656e7c4e3b),
('862c6f9f59d6fea11de1c7602b3be3aa499aee52', '123.125.71.46', 1436734550, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363733343534393b),
('8728e3d0adc98b7af1bed0d96749105e59322382', '::1', 1432748389, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323734383234323b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373131373338223b6d656e755f68696464656e7c4e3b),
('8a87b0e24739a483707c70c89d2bc4f82ea2efbf', '::1', 1434635133, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343633353132363b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363230353734223b6d656e755f68696464656e7c4e3b),
('8ad47f72b2a3f6920cde80cf5f480a86e26f0042', '::1', 1433429181, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333432393137363b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333323730393135223b6d656e755f68696464656e7c4e3b),
('8c04103f6d204df655b0d5747ff5e574189470cd', '139.228.224.3', 1434701695, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343730313639343b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363832393837223b6d656e755f68696464656e7c4e3b),
('8c1a0c31d1efe6d275fe25b7f8b2e58d346d1770', '::1', 1434657899, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635373839373b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('8e3ed622ab484e63f20c7685e89cca175b37bb90', '::1', 1434118232, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343131383230343b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334303434363533223b6d656e755f68696464656e7c4e3b),
('9048e6da81c9064cc9a19c7dd17951914129bc58', '209.190.113.85', 1436144970, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363134343937303b),
('9125e54f08e753da002fba7a0a035503abfaa4bd', '::1', 1434045661, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343034353530343b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333393935323331223b6d656e755f68696464656e7c4e3b),
('91fa0b6b89e5fba924c80628b010270368642329', '::1', 1432788903, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323738383930333b),
('92aa2593d01dbaef364941f576a635fa05196b4a', '::1', 1433272950, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333237323637393b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373937373030223b6d656e755f68696464656e7c4e3b),
('976f8ec5454f6a660c7a4546830320ae8407624e', '139.0.78.139', 1435479649, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353437393534313b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335343738333437223b6d656e755f68696464656e7c4e3b),
('993393435ab1207f223925ca9546f836d8f661c9', '::1', 1432754627, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735343537303b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373131373338223b6d656e755f68696464656e7c4e3b),
('997515ff8d19e7810021816d90bc48aa1b512db4', '::1', 1433427983, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333432373738393b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333323730393135223b6d656e755f68696464656e7c4e3b),
('9b9e10894203cca3c0dcc9127a3e9a094af95dda', '220.181.108.112', 1434877234, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343837373233323b),
('9bf03a2f0d876f781cde3b9f5d9e5df835940bd0', '::1', 1434637752, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343633373735323b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363230353734223b6d656e755f68696464656e7c4e3b),
('9cb07ffd8fb2ded2acbb0102b23e9acc0c100133', '::1', 1434656200, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635353938353b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('9d4422e34684c4160b77d3c2b908505cdd34111a', '::1', 1432749553, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323734393532393b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373131373338223b6d656e755f68696464656e7c4e3b),
('9e139164ade7595a312084863234f2eecc1f7456', '::1', 1432756053, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735353939313b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373230383138223b6d656e755f68696464656e7c4e3b),
('9e813f3f6a1eeafeb047135ac7b36e2299ce384b', '::1', 1433997594, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333939373538363b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333343234383230223b6d656e755f68696464656e7c4e3b),
('9edd6c8f6e1c159876355aa421bead320d585663', '::1', 1434655326, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635353239353b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('a0e2088303b81bbc24556faf4688bcc1d7c0c27f', '157.55.39.209', 1435910102, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353931303130323b),
('a14fcefc42e59772371f238c3c44a4404c937d46', '::1', 1434647981, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343634373938313b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('a3291195f2885b56de6f3934596eac27995aa721', '207.46.13.16', 1436617508, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363631373530383b),
('a3fcd86883ecaebeb075124d6e5986445a05a560', '::1', 1432801195, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323830313136373b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373534393737223b6d656e755f68696464656e7c4e3b),
('a6bc4f4515cd487fabbe6652ca50c15760cabdb2', '139.0.78.139', 1435479229, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353437393232393b),
('a78496ea46f21329244b908d1f9af708215542a4', '::1', 1433430405, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333433303131383b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333323730393135223b6d656e755f68696464656e7c4e3b),
('a8ac14f249b40a072c4db1ffc98a69d6f2d3808e', '111.68.126.42', 1435218424, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353231383432323b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334393330353336223b6d656e755f68696464656e7c4e3b),
('aa8879f4b583c3267098f91d6a0e567363aefbd5', '::1', 1433430758, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333433303437363b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333323730393135223b6d656e755f68696464656e7c4e3b),
('ab1240b453e5dc70f48b6609f95a24db2eaa833a', '::1', 1432755483, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735353331303b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373230383138223b6d656e755f68696464656e7c4e3b),
('af3862574f9f4c1e34b6adc14f5d4bd26f5f62cb', '::1', 1434124115, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343132343037383b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334303434363533223b6d656e755f68696464656e7c4e3b),
('b1b39f397b28847c0777197ab935021b07dc1143', '::1', 1434639150, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343633393134383b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363230353734223b6d656e755f68696464656e7c4e3b),
('b2108df29cbe4ab1984863fa00307360441dafe3', '::1', 1434125394, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343132353131383b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334303434363533223b6d656e755f68696464656e7c4e3b),
('b28fe99732b37d8b8e51fd482920b02c9e4167dc', '::1', 1434098722, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343039383732323b),
('b5a9aa750ce7f403860a5978be52fe8b26b2b6c2', '::1', 1434052903, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343035323837333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333393935323331223b6d656e755f68696464656e7c4e3b),
('b606fa5eb3f85d5f7f318fe839930e03bdeebdee', '::1', 1433995324, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333939353137363b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333343234383230223b6d656e755f68696464656e7c4e3b),
('b6bc2e183458f92313a009b58890a7b9ab20d6a2', '::1', 1433271722, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333237313532333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373937373030223b6d656e755f68696464656e7c4e3b),
('b7134fd06b98b3c791334b3042e76aaf69a1f34c', '::1', 1434220143, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343232303134333b),
('b748f3e93cf00ee5c0e19bd81c2acef040c87214', '115.164.57.197', 1435408203, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353430383031343b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335333030313233223b6d656e755f68696464656e7c4e3b6d6573736167657c733a32393a223c703e4c6f6767656420496e205375636365737366756c6c793c2f703e223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226f6c64223b7d),
('b79be4a1f011a0401d9805378c835273450f59ff', '::1', 1433273359, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333237333334313b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373937373030223b6d656e755f68696464656e7c4e3b),
('b9bd701d94d6d73264407db0434ed6b1a5f77fbb', '::1', 1432753956, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735333639333b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373131373338223b6d656e755f68696464656e7c4e3b),
('ba10db801efc8d343d3df13dc052315364a8caa4', '::1', 1432751567, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735313337323b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373131373338223b6d656e755f68696464656e7c4e3b),
('bd1ba4b4ff397c8ec1007d6303767757903b6da7', '182.253.73.32', 1435558220, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353535383136373b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335333437393832223b6d656e755f68696464656e7c4e3b),
('bdac25190b6635d8155e470a2e0a30ed6afce4e1', '::1', 1434656992, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635363934383b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('be5d66174107795c8ca5bfe1c715b1898a4b2768', '::1', 1434048769, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343034383539383b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333393935323331223b6d656e755f68696464656e7c4e3b),
('c085deaecd76baeb500f32a3a303e916167ce834', '111.68.126.42', 1435206534, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353230363335313b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334393330353336223b6d656e755f68696464656e7c4e3b),
('c21a9fc119f483c06544d2d80db089bcc97baeb7', '::1', 1434651423, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635313132333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('c6a217e39c2ba968692bbe69f5ff3f11a67280cc', '::1', 1434654463, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635343436303b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('c8f9b038499c5ed1bce813c2b814ba85877c5cbc', '182.253.73.32', 1435557732, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353535373531383b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335333437393832223b6d656e755f68696464656e7c4e3b),
('cb769441b54879c661cb2da0be5ee5360565591b', '::1', 1434125100, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343132343831303b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334303434363533223b6d656e755f68696464656e7c4e3b),
('cb8c0552c507028b36c82332431a81fe981b0918', '::1', 1434047281, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343034373132373b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333393935323331223b6d656e755f68696464656e7c4e3b),
('d518d9a7602af5fd7c46e04be79625ec5df06f7d', '207.46.13.138', 1435936593, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353933363539333b),
('d9472d5f9678f01639902ebe9413ec9eb55543c9', '182.253.73.32', 1435558131, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353535373835393b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335333437393832223b6d656e755f68696464656e7c4e3b);
INSERT INTO `t_ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('daab692341b8c8ee81d9070843fcc95ed466f53b', '54.69.76.86', 1435753665, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353735333636353b),
('dccf3371d7805551d8274dfeab126f500b18e0a0', '209.190.113.85', 1436144971, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363134343937303b),
('dfa00ddc6417ca3e9e690d25a14525c4378d5533', '::1', 1432798730, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323739383732313b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373534393737223b6d656e755f68696464656e7c4e3b),
('e06c0ee9db3e5c75ee2801d40afd81dc401a2052', '223.255.225.76', 1436859518, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363835393331363b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335353337323233223b6d656e755f68696464656e7c4e3b),
('e10c031d69a660f209d16ee5ec369ab85720f947', '::1', 1434652224, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343635313936383b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363330383637223b6d656e755f68696464656e7c4e3b),
('e272288f5da42ea1df2fcdf0c14a9d0bf1e0061c', '103.246.200.26', 1435026861, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353032363836303b),
('e27d1156b941e1a25ec9d1655b318b890b16c90d', '::1', 1454147683, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435343134373339323b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343534313436353938223b6d656e755f68696464656e7c4e3b),
('e3b12c20e986b44c12cf4c9312485da459138231', '209.190.113.85', 1436145046, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363134353034363b),
('e406699c347f889dbcfd696fb8e0f3ba96fa3f33', '202.51.110.178', 1435300311, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353330303031313b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334393330353931223b6d656e755f68696464656e7c4e3b),
('e4c918a1f51a17d9e5bbd9a6e8ff7b5c8db06c79', '157.55.39.209', 1435910103, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353931303130333b),
('e8c7100780dd2178679d612b0f12c9edb6114f82', '111.68.126.42', 1435218101, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353231383039323b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334393330353336223b6d656e755f68696464656e7c4e3b),
('ebb3d8c28432fe4785a61b66aebbbf1b55959618', '111.68.126.42', 1435213906, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353231333739383b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334393330353336223b6d656e755f68696464656e7c4e3b),
('ed1e0ed9f13476ca0fb064d11008f3316e4f75be', '111.68.126.42', 1435219393, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353231393331343b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334393330353336223b6d656e755f68696464656e7c4e3b),
('ed411ccf7cfcacce9201a22933465cdc037fe82f', '139.228.224.3', 1434691422, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343639313432323b),
('ee12b63581a1419956805ee6d8cb33b822f8ca41', '157.55.39.230', 1436016586, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363031363538363b),
('ef03f9761d5c7ce65c2c55b99719621dcc84d249', '::1', 1433296424, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333239363332373b6964656e746974797c733a353a227374616666223b757365726e616d657c733a353a227374616666223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2234223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373838323932223b6d656e755f68696464656e7c4e3b),
('f1540b726c437b06ad83ed71562e069cf8ed7d9b', '::1', 1434128473, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343132383233363b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334303434363533223b6d656e755f68696464656e7c4e3b),
('f2ce334928107384ac81955d98a37974583b8d99', '::1', 1434637895, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343633373735323b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334363230353734223b6d656e755f68696464656e7c4e3b),
('f3d2e01d0ca29241ca44d51f33fe60db2f78d2ab', '::1', 1454150364, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435343135303336323b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343534313436353938223b6d656e755f68696464656e7c4e3b),
('f43c32afc2438142da6cbe27408ce005db2f415c', '202.51.110.178', 1435300399, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353330303335333b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343334393330353931223b6d656e755f68696464656e7c4e3b),
('f4a3d00a84d43ee5c3d44cb0f48a76282a48f873', '::1', 1433428664, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433333432383339383b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343333323730393135223b6d656e755f68696464656e7c4e3b),
('f4f4deb07d832380faac8e348a48136592893b09', '14.192.205.175', 1435537282, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353533373231323b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335343739323034223b6d656e755f68696464656e7c4e3b),
('f68060ab8a099da0a65fe0eed2b209167ec61cf9', '139.194.87.130', 1435348165, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353334373930373b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335323036333932223b6d656e755f68696464656e7c4e3b),
('f6e4cf090644c49e633edab7cc7858b4abadf01b', '::1', 1434646561, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343634363536303b),
('f71b2397027ac49aa413b9f1ae0c02ce885c1fd0', '123.125.71.111', 1436029317, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433363032393331373b),
('fca7dc0b2f34357930335fe807f177c3f3078b88', '139.195.59.169', 1434930562, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433343933303236353b6964656e746974797c733a31333a2261646d696e6973747261746f72223b757365726e616d657c733a31333a2261646d696e6973747261746f72223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373339303839223b6d656e755f68696464656e7c4e3b),
('fd214a06aa931dab102c247f01f0f53b99ce51e4', '220.181.108.159', 1435704107, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353730343130363b),
('fdf19bcce95baa4d860dd1d7b8bc9478f69cf7f8', '115.164.57.197', 1435408452, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433353430383435323b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343335333030313233223b6d656e755f68696464656e7c4e3b),
('ff6b1fe00d0bf521f35e624038de85620ee745c7', '::1', 1454147198, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435343134363934373b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343534313436353938223b6d656e755f68696464656e7c4e3b),
('ffe71827d60db9e722b1e7f9f7160526d7ed05ac', '::1', 1432757144, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433323735363837323b6964656e746974797c733a373a226d616e61676572223b757365726e616d657c733a373a226d616e61676572223b656d61696c7c733a31383a22697072617374686140676d61696c2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343332373230383138223b6d656e755f68696464656e7c4e3b);

DROP TABLE IF EXISTS `t_comment_detail`;
CREATE TABLE IF NOT EXISTS `t_comment_detail` (
  `comment_id` int(11) NOT NULL,
  `attach` varchar(50) NOT NULL,
  `user` varchar(255) NOT NULL,
  `dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comment` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

INSERT INTO `t_comment_detail` (`comment_id`, `attach`, `user`, `dt`, `comment`) VALUES
(1, 'PO-0065', 'manager', '2014-10-23 08:35:45', 'Apakah qualitas bagus?'),
(2, 'PO-0063', 'manager', '2014-10-23 09:13:57', 'Proses nya mudah, mantap!!'),
(3, 'PO-0065', 'anne', '2014-10-23 09:13:57', 'Bagus koq..'),
(4, 'PO-0061', 'esther', '2014-10-23 09:13:57', 'ada bbrp aneh nih quality nya.. 1 malah rusak.. :('),
(18, 'PO-0065', 'manager', '2014-10-24 02:01:09', 'serius?'),
(19, 'PO-0065', 'manager', '2014-10-24 02:02:04', 'yang bener?'),
(20, 'PO-0065', 'manager', '2014-10-24 03:04:16', 'Hello?'),
(21, 'PO-0065', 'manager', '2014-10-24 03:04:25', 'heyy!');

DROP TABLE IF EXISTS `t_comment_header`;
CREATE TABLE IF NOT EXISTS `t_comment_header` (
  `post_id` int(11) NOT NULL,
  `post_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `t_company`;
CREATE TABLE IF NOT EXISTS `t_company` (
  `comp_id` int(11) NOT NULL COMMENT 'Skyware Company Detail',
  `comp_name` varchar(100) NOT NULL,
  `comp_addr` varchar(500) NOT NULL,
  `comp_city` varchar(100) NOT NULL,
  `comp_state` varchar(100) NOT NULL,
  `comp_country` varchar(100) NOT NULL,
  `comp_zipcode` varchar(10) NOT NULL DEFAULT '0',
  `comp_website` varchar(100) NOT NULL,
  `comp_tlp1` varchar(25) NOT NULL,
  `comp_tlp2` varchar(25) NOT NULL,
  `comp_fax1` varchar(25) DEFAULT NULL,
  `comp_fax2` varchar(25) DEFAULT NULL,
  `comp_email` varchar(200) NOT NULL,
  `comp_tax_num` varchar(50) NOT NULL,
  `comp_tax_office` varchar(100) NOT NULL,
  `default_curr` varchar(5) NOT NULL DEFAULT 'IDR',
  `default_sell_whse` int(11) NOT NULL DEFAULT '1',
  `default_purc_whse` int(11) NOT NULL DEFAULT '1',
  `default_cust_pymt` int(11) NOT NULL DEFAULT '1',
  `default_vend_pymt` int(11) NOT NULL DEFAULT '1',
  `set_negative_stock` tinyint(4) NOT NULL DEFAULT '0',
  `default_sell_prcl` int(11) NOT NULL DEFAULT '1',
  `default_purc_prcl` int(11) NOT NULL DEFAULT '1',
  `logo_login` varchar(500) DEFAULT NULL,
  `logo_icon` varchar(500) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `t_company` (`comp_id`, `comp_name`, `comp_addr`, `comp_city`, `comp_state`, `comp_country`, `comp_zipcode`, `comp_website`, `comp_tlp1`, `comp_tlp2`, `comp_fax1`, `comp_fax2`, `comp_email`, `comp_tax_num`, `comp_tax_office`, `default_curr`, `default_sell_whse`, `default_purc_whse`, `default_cust_pymt`, `default_vend_pymt`, `set_negative_stock`, `default_sell_prcl`, `default_purc_prcl`, `logo_login`, `logo_icon`) VALUES
(1, 'Digital Buana', 'Cempaka Putih', 'Jakarta', 'DKI Jakarta', 'Indonesia', '11620', 'www.digitalbuana.com', '021 1234 567', '', '', NULL, 'support@digitalbuana.com', '', '', 'IDR', 1, 1, 1, 1, 0, 1, 1, NULL, NULL),
(2, 'PT Perodua', 'Jakarta', 'Jakarta', 'DKI Jakarta', 'Indonesia', '12220', 'www.perodua.co.id', '021-123456', '', '', NULL, 'test@digitalbuana.com', '', '', 'IDR', 1, 1, 1, 1, 0, 1, 1, NULL, NULL),
(3, 'PT ABC', 'Jakarta', 'Jakarta', 'DKI Jakarta', 'Indonesia', '122220', '', '', '', '', NULL, 'test@example.com', '', '', 'IDR', 1, 1, 1, 1, 0, 1, 1, NULL, NULL);

DROP TABLE IF EXISTS `t_costcenter`;
CREATE TABLE IF NOT EXISTS `t_costcenter` (
  `id` int(11) NOT NULL,
  `cc_code` varchar(45) NOT NULL,
  `cc_name` varchar(255) NOT NULL,
  `cc_description` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Cost center master';

INSERT INTO `t_costcenter` (`id`, `cc_code`, `cc_name`, `cc_description`) VALUES
(1, 'HO', '304', 'All cost related to head office'),
(2, 'MKT', 'Marketing', 'All expenses related to marketing expenses'),
(3, 'GA', 'General Affairs', 'All expenses related to GA Department'),
(4, 'PUR', 'Purchasing', 'Purchasing Department');

DROP TABLE IF EXISTS `t_credit_voucher`;
CREATE TABLE IF NOT EXISTS `t_credit_voucher` (
  `id` int(11) NOT NULL,
  `voucher_code` varchar(50) NOT NULL,
  `value` decimal(18,2) NOT NULL DEFAULT '0.00',
  `issued_date` datetime NOT NULL,
  `disabled` bit(1) NOT NULL DEFAULT b'0',
  `active_date` datetime NOT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_datetime` datetime NOT NULL,
  `voucher_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Credit vouchers used in POS for payment';

INSERT INTO `t_credit_voucher` (`id`, `voucher_code`, `value`, `issued_date`, `disabled`, `active_date`, `expiry_date`, `created_by`, `created_datetime`, `voucher_count`) VALUES
(2, 'V50', '50000.00', '2015-05-06 00:00:00', b'0', '2015-05-01 00:00:00', '2015-09-06 00:00:00', 1, '2015-05-06 10:38:53', 50),
(3, 'V25', '25000.00', '2015-05-06 00:00:00', b'0', '2015-05-06 00:00:00', '2015-12-06 00:00:00', 1, '2015-05-06 10:43:42', 100),
(4, 'V100', '100000.00', '2015-05-07 00:00:00', b'0', '2015-05-07 00:00:00', '2015-12-31 00:00:00', 1, '2015-05-07 12:07:47', 10),
(5, 'TEST10', '10000.00', '2015-05-07 00:00:00', b'0', '2015-05-07 00:00:00', '2015-05-12 00:00:00', 1, '2015-05-07 12:23:16', 10),
(6, 'EXP1000', '1000.00', '2015-05-13 00:00:00', b'0', '2015-05-01 00:00:00', '2015-05-10 00:00:00', 1, '2015-05-13 02:29:51', 5),
(8, 'A', '5000.00', '2015-05-13 00:00:00', b'0', '2015-05-13 00:00:00', '2015-05-13 00:00:00', 1, '2015-05-13 02:36:50', 1);

DROP TABLE IF EXISTS `t_credit_voucher_detail`;
CREATE TABLE IF NOT EXISTS `t_credit_voucher_detail` (
  `id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `voucher_code` varchar(50) NOT NULL,
  `voucher_number` varchar(50) NOT NULL,
  `used` bit(1) NOT NULL DEFAULT b'0',
  `disabled` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8 COMMENT='Each Voucher';

INSERT INTO `t_credit_voucher_detail` (`id`, `voucher_id`, `voucher_code`, `voucher_number`, `used`, `disabled`) VALUES
(1, 2, 'V50', 'V506515000010610', b'1', b'0'),
(2, 2, 'V50', 'V506515000089128', b'0', b'0'),
(3, 2, 'V50', 'V506515000184638', b'0', b'0'),
(4, 2, 'V50', 'V506515000229630', b'0', b'0'),
(5, 2, 'V50', 'V506515000268546', b'0', b'0'),
(6, 2, 'V50', 'V506515000326928', b'0', b'0'),
(7, 2, 'V50', 'V506515000401643', b'0', b'0'),
(8, 2, 'V50', 'V506515000413292', b'0', b'0'),
(9, 2, 'V50', 'V506515000471441', b'0', b'0'),
(10, 2, 'V50', 'V506515000531370', b'0', b'0'),
(11, 2, 'V50', 'V506515000577014', b'0', b'0'),
(12, 2, 'V50', 'V506515000586156', b'0', b'0'),
(13, 2, 'V50', 'V506515000617984', b'0', b'0'),
(14, 2, 'V50', 'V506515000678405', b'0', b'0'),
(15, 2, 'V50', 'V506515000706049', b'0', b'0'),
(16, 2, 'V50', 'V506515000782601', b'0', b'0'),
(17, 2, 'V50', 'V506515000878440', b'0', b'0'),
(18, 2, 'V50', 'V506515000909655', b'0', b'0'),
(19, 2, 'V50', 'V506515000918706', b'0', b'0'),
(20, 2, 'V50', 'V506515001014859', b'0', b'0'),
(21, 2, 'V50', 'V506515001036859', b'0', b'0'),
(22, 2, 'V50', 'V506515001111374', b'0', b'0'),
(23, 2, 'V50', 'V506515001205165', b'0', b'0'),
(24, 2, 'V50', 'V506515001264196', b'0', b'0'),
(25, 2, 'V50', 'V506515001331194', b'0', b'0'),
(26, 2, 'V50', 'V506515001398093', b'0', b'0'),
(27, 2, 'V50', 'V506515001446810', b'0', b'0'),
(28, 2, 'V50', 'V506515001496204', b'0', b'0'),
(29, 2, 'V50', 'V506515001587575', b'0', b'0'),
(30, 2, 'V50', 'V506515001625340', b'0', b'0'),
(31, 2, 'V50', 'V506515001637446', b'0', b'0'),
(32, 2, 'V50', 'V506515001649950', b'0', b'0'),
(33, 2, 'V50', 'V506515001670527', b'0', b'0'),
(34, 2, 'V50', 'V506515001756006', b'0', b'0'),
(35, 2, 'V50', 'V506515001771097', b'0', b'0'),
(36, 2, 'V50', 'V506515001785807', b'0', b'0'),
(37, 2, 'V50', 'V506515001803589', b'0', b'0'),
(38, 2, 'V50', 'V506515001873014', b'0', b'0'),
(39, 2, 'V50', 'V506515001949451', b'0', b'0'),
(40, 2, 'V50', 'V506515002019999', b'0', b'0'),
(41, 2, 'V50', 'V506515002094427', b'0', b'0'),
(42, 2, 'V50', 'V506515002153063', b'0', b'0'),
(43, 2, 'V50', 'V506515002160498', b'0', b'0'),
(44, 2, 'V50', 'V506515002207880', b'0', b'0'),
(45, 2, 'V50', 'V506515002269072', b'0', b'0'),
(46, 2, 'V50', 'V506515002361382', b'0', b'0'),
(47, 2, 'V50', 'V506515002454717', b'0', b'0'),
(48, 2, 'V50', 'V506515002464248', b'0', b'0'),
(49, 2, 'V50', 'V506515002508684', b'0', b'0'),
(50, 2, 'V50', 'V506515002543509', b'0', b'0'),
(51, 3, 'V25', 'V256515000026124', b'0', b'0'),
(52, 3, 'V25', 'V256515000086875', b'0', b'0'),
(53, 3, 'V25', 'V256515000172699', b'0', b'0'),
(54, 3, 'V25', 'V256515000266834', b'0', b'0'),
(55, 3, 'V25', 'V256515000307747', b'0', b'0'),
(56, 3, 'V25', 'V256515000371745', b'0', b'0'),
(57, 3, 'V25', 'V256515000401060', b'0', b'0'),
(58, 3, 'V25', 'V256515000417713', b'0', b'0'),
(59, 3, 'V25', 'V256515000514996', b'0', b'0'),
(60, 3, 'V25', 'V256515000521425', b'0', b'0'),
(61, 3, 'V25', 'V256515000549894', b'0', b'0'),
(62, 3, 'V25', 'V256515000606548', b'0', b'0'),
(63, 3, 'V25', 'V256515000634053', b'0', b'0'),
(64, 3, 'V25', 'V256515000706284', b'0', b'0'),
(65, 3, 'V25', 'V256515000748481', b'0', b'0'),
(66, 3, 'V25', 'V256515000788932', b'0', b'0'),
(67, 3, 'V25', 'V256515000794941', b'0', b'0'),
(68, 3, 'V25', 'V256515000830488', b'0', b'0'),
(69, 3, 'V25', 'V256515000834756', b'0', b'0'),
(70, 3, 'V25', 'V256515000906404', b'0', b'0'),
(71, 3, 'V25', 'V256515000963121', b'0', b'0'),
(72, 3, 'V25', 'V256515001020971', b'0', b'0'),
(73, 3, 'V25', 'V256515001088021', b'0', b'0'),
(74, 3, 'V25', 'V256515001098335', b'0', b'0'),
(75, 3, 'V25', 'V256515001191806', b'0', b'0'),
(76, 3, 'V25', 'V256515001275847', b'0', b'0'),
(77, 3, 'V25', 'V256515001374916', b'0', b'0'),
(78, 3, 'V25', 'V256515001377547', b'0', b'0'),
(79, 3, 'V25', 'V256515001453425', b'0', b'0'),
(80, 3, 'V25', 'V256515001533684', b'0', b'0'),
(81, 3, 'V25', 'V256515001557192', b'0', b'0'),
(82, 3, 'V25', 'V256515001629018', b'0', b'0'),
(83, 3, 'V25', 'V256515001640492', b'0', b'0'),
(84, 3, 'V25', 'V256515001680305', b'0', b'0'),
(85, 3, 'V25', 'V256515001740506', b'0', b'0'),
(86, 3, 'V25', 'V256515001838031', b'0', b'0'),
(87, 3, 'V25', 'V256515001917492', b'0', b'0'),
(88, 3, 'V25', 'V256515001937794', b'0', b'0'),
(89, 3, 'V25', 'V256515002020038', b'0', b'0'),
(90, 3, 'V25', 'V256515002095360', b'0', b'0'),
(91, 3, 'V25', 'V256515002108309', b'0', b'0'),
(92, 3, 'V25', 'V256515002137587', b'0', b'0'),
(93, 3, 'V25', 'V256515002158394', b'0', b'0'),
(94, 3, 'V25', 'V256515002162091', b'0', b'0'),
(95, 3, 'V25', 'V256515002256084', b'0', b'0'),
(96, 3, 'V25', 'V256515002277604', b'0', b'0'),
(97, 3, 'V25', 'V256515002311837', b'0', b'0'),
(98, 3, 'V25', 'V256515002393942', b'0', b'0'),
(99, 3, 'V25', 'V256515002421235', b'0', b'0'),
(100, 3, 'V25', 'V256515002444549', b'0', b'0'),
(101, 3, 'V25', 'V256515002542591', b'0', b'0'),
(102, 3, 'V25', 'V256515002614940', b'0', b'0'),
(103, 3, 'V25', 'V256515002641807', b'0', b'0'),
(104, 3, 'V25', 'V256515002694569', b'0', b'0'),
(105, 3, 'V25', 'V256515002703460', b'0', b'0'),
(106, 3, 'V25', 'V256515002794495', b'0', b'0'),
(107, 3, 'V25', 'V256515002879107', b'0', b'0'),
(108, 3, 'V25', 'V256515002928661', b'0', b'0'),
(109, 3, 'V25', 'V256515002981807', b'0', b'0'),
(110, 3, 'V25', 'V256515002984106', b'0', b'0'),
(111, 3, 'V25', 'V256515003056099', b'0', b'0'),
(112, 3, 'V25', 'V256515003133285', b'0', b'0'),
(113, 3, 'V25', 'V256515003199371', b'0', b'0'),
(114, 3, 'V25', 'V256515003273865', b'0', b'0'),
(115, 3, 'V25', 'V256515003283684', b'0', b'0'),
(116, 3, 'V25', 'V256515003304769', b'0', b'0'),
(117, 3, 'V25', 'V256515003308164', b'0', b'0'),
(118, 3, 'V25', 'V256515003378302', b'0', b'0'),
(119, 3, 'V25', 'V256515003466643', b'0', b'0'),
(120, 3, 'V25', 'V256515003502272', b'0', b'0'),
(121, 3, 'V25', 'V256515003550288', b'0', b'0'),
(122, 3, 'V25', 'V256515003577013', b'0', b'0'),
(123, 3, 'V25', 'V256515003646628', b'0', b'0'),
(124, 3, 'V25', 'V256515003719125', b'0', b'0'),
(125, 3, 'V25', 'V256515003727052', b'0', b'0'),
(126, 3, 'V25', 'V256515003822804', b'0', b'0'),
(127, 3, 'V25', 'V256515003853179', b'0', b'0'),
(128, 3, 'V25', 'V256515003893530', b'0', b'0'),
(129, 3, 'V25', 'V256515003980520', b'0', b'0'),
(130, 3, 'V25', 'V256515004005070', b'0', b'0'),
(131, 3, 'V25', 'V256515004102423', b'0', b'0'),
(132, 3, 'V25', 'V256515004119737', b'0', b'0'),
(133, 3, 'V25', 'V256515004132169', b'0', b'0'),
(134, 3, 'V25', 'V256515004136404', b'0', b'0'),
(135, 3, 'V25', 'V256515004180728', b'0', b'0'),
(136, 3, 'V25', 'V256515004266893', b'0', b'0'),
(137, 3, 'V25', 'V256515004352940', b'0', b'0'),
(138, 3, 'V25', 'V256515004356393', b'0', b'0'),
(139, 3, 'V25', 'V256515004422147', b'0', b'0'),
(140, 3, 'V25', 'V256515004427959', b'0', b'0'),
(141, 3, 'V25', 'V256515004481697', b'0', b'0'),
(142, 3, 'V25', 'V256515004485842', b'0', b'0'),
(143, 3, 'V25', 'V256515004518211', b'0', b'0'),
(144, 3, 'V25', 'V256515004589128', b'0', b'0'),
(145, 3, 'V25', 'V256515004642603', b'0', b'0'),
(146, 3, 'V25', 'V256515004683652', b'0', b'0'),
(147, 3, 'V25', 'V256515004712178', b'0', b'0'),
(148, 3, 'V25', 'V256515004719380', b'0', b'0'),
(149, 3, 'V25', 'V256515004750634', b'0', b'0'),
(150, 3, 'V25', 'V256515004835808', b'0', b'0'),
(151, 4, 'V100', 'V1007515000013716', b'0', b'0'),
(152, 4, 'V100', 'V1007515000102739', b'0', b'0'),
(153, 4, 'V100', 'V1007515000198119', b'0', b'0'),
(154, 4, 'V100', 'V1007515000223346', b'0', b'0'),
(155, 4, 'V100', 'V1007515000308466', b'0', b'0'),
(156, 4, 'V100', 'V1007515000339560', b'0', b'0'),
(157, 4, 'V100', 'V1007515000391440', b'0', b'0'),
(158, 4, 'V100', 'V1007515000444363', b'0', b'0'),
(159, 4, 'V100', 'V1007515000543879', b'0', b'0'),
(160, 4, 'V100', 'V1007515000575224', b'0', b'0'),
(161, 5, 'TEST10', 'TEST107515000045587', b'0', b'1'),
(162, 5, 'TEST10', 'TEST107515000116691', b'0', b'1'),
(163, 5, 'TEST10', 'TEST107515000211884', b'0', b'1'),
(164, 5, 'TEST10', 'TEST107515000258078', b'0', b'1'),
(165, 5, 'TEST10', 'TEST107515000346419', b'0', b'1'),
(166, 5, 'TEST10', 'TEST107515000381501', b'0', b'1'),
(167, 5, 'TEST10', 'TEST107515000437415', b'0', b'1'),
(168, 5, 'TEST10', 'TEST107515000457412', b'0', b'1'),
(169, 5, 'TEST10', 'TEST107515000537659', b'0', b'1'),
(170, 5, 'TEST10', 'TEST107515000631444', b'0', b'1'),
(171, 6, 'EXP1000', 'EXP100013515000052179', b'0', b'0'),
(172, 6, 'EXP1000', 'EXP100013515000136408', b'0', b'0'),
(173, 6, 'EXP1000', 'EXP100013515000233607', b'0', b'0'),
(174, 6, 'EXP1000', 'EXP100013515000323383', b'0', b'0'),
(175, 6, 'EXP1000', 'EXP100013515000402986', b'0', b'0'),
(176, 8, 'A', 'A13515000018163', b'0', b'0');

DROP TABLE IF EXISTS `t_currency`;
CREATE TABLE IF NOT EXISTS `t_currency` (
  `curr_code` varchar(5) NOT NULL,
  `curr_name` varchar(100) NOT NULL,
  `curr_decimal` int(11) NOT NULL,
  `curr_symbol` varchar(5) NOT NULL,
  `is_base` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_currency` (`curr_code`, `curr_name`, `curr_decimal`, `curr_symbol`, `is_base`) VALUES
('IDR', 'IDR', 0, 'Rp', 1),
('MYR', 'MYR', 2, 'RM', 0),
('USD', 'USD', 2, 'US$', 0);

DROP TABLE IF EXISTS `t_currency_rate`;
CREATE TABLE IF NOT EXISTS `t_currency_rate` (
  `curr_from` varchar(5) NOT NULL,
  `curr_to` varchar(5) NOT NULL,
  `value` decimal(18,5) NOT NULL,
  `inverse_value` decimal(18,5) NOT NULL,
  `effective_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_currency_rate` (`curr_from`, `curr_to`, `value`, `inverse_value`, `effective_date`) VALUES
('MYR', 'IDR', '3550.00000', '0.00000', '2015-01-01 00:00:00'),
('USD', 'IDR', '13300.00000', '0.00000', '2015-01-01 00:00:00'),
('USD', 'MYR', '3.75000', '0.00000', '2015-01-01 00:00:00');

DROP TABLE IF EXISTS `t_document`;
CREATE TABLE IF NOT EXISTS `t_document` (
  `document_id` int(11) NOT NULL,
  `document_code` varchar(45) NOT NULL,
  `document_name` varchar(255) NOT NULL,
  `document_series` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

INSERT INTO `t_document` (`document_id`, `document_code`, `document_name`, `document_series`) VALUES
(1, 'PUR_ORD', 'Purchase Order', '1'),
(2, 'ITM_TRF', 'Stock Transfer', '2'),
(3, 'PUR_RCV', 'Purchase Receipt', '3'),
(4, 'PUR_INV', 'Purchase Invoice', '4'),
(5, 'ITM_RCP', 'Transfer Receipt', '5'),
(6, 'SAL_ORD', 'Sales Order', '6'),
(7, 'SAL_DEL', 'Sales Delivery', '7'),
(8, 'SAL_INV', 'Sales Invoice', '8'),
(9, 'PAY_OUT', 'Payment Out', '9'),
(10, 'PAY_IN', 'Payment In', '10'),
(11, 'SAL_DP', 'Sales DP', '11'),
(12, 'PUR_DP', 'Purchase DP', '12'),
(13, 'JRNL', 'Journal', '13'),
(14, 'VEND', 'Vendor', '14'),
(15, 'CUST', 'Customer', '15'),
(16, 'ITM_ADJ', 'Stock Adjustment', '16'),
(17, 'PUR_RET', 'Purchase Return', '17'),
(18, 'SAL_RET', 'Sales Return', '18'),
(19, 'PUR_REQ', 'Purchase Request', '19'),
(20, 'PUR_CON', 'Purchase Contract', '20');

DROP TABLE IF EXISTS `t_document_approval`;
CREATE TABLE IF NOT EXISTS `t_document_approval` (
  `doc_approval_id` int(11) NOT NULL,
  `appr_detail_id` int(11) NOT NULL,
  `doc_number` varchar(255) NOT NULL,
  `requestor_position_id` int(11) NOT NULL,
  `requestor_user_id` int(11) NOT NULL,
  `request_datetime` datetime NOT NULL,
  `approver_step` int(11) NOT NULL DEFAULT '1',
  `approver_position_id` int(11) NOT NULL,
  `approver_user_id` int(11) DEFAULT NULL,
  `approver_action` smallint(6) NOT NULL DEFAULT '0' COMMENT '0,1,2,3',
  `action_datetime` datetime DEFAULT NULL,
  `unique_key` varchar(32) NOT NULL DEFAULT '0',
  `email_flag` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='List of Documents to be Approved';

INSERT INTO `t_document_approval` (`doc_approval_id`, `appr_detail_id`, `doc_number`, `requestor_position_id`, `requestor_user_id`, `request_datetime`, `approver_step`, `approver_position_id`, `approver_user_id`, `approver_action`, `action_datetime`, `unique_key`, `email_flag`) VALUES
(1, 4, 'SAL-0001', 2, 4, '2014-11-07 11:04:44', 1, 2, NULL, 1, NULL, 'Nb1Bw8bcZQ78Y0Vo4jf1DoKYOTFRPiAc', b'0'),
(2, 5, 'PO-0014', 5, 4, '2015-04-29 12:24:54', 1, 4, NULL, 3, NULL, 'pvz0z8FDE0XDWLiVgA25Afl8HEB9VNH5', b'0'),
(3, 5, 'PO-0015', 5, 4, '2015-04-29 12:28:32', 1, 4, NULL, 3, NULL, 'hqYdVqWS3a6dE9IsZVwfrEHOmANAfsUA', b'0'),
(4, 5, 'PO-0016', 5, 4, '2015-04-29 14:20:49', 1, 4, NULL, 3, NULL, 'CqinaPEXiL2VQEkw65UpKsyC50N6yEFU', b'0'),
(7, 5, 'RCV-0019', 5, 4, '2015-04-29 14:22:57', 1, 4, NULL, 1, NULL, 'FqaFVpXqlhixeDGLYZzrk6xgA4duK4W5', b'0'),
(8, 5, 'PO-0017', 5, 4, '2015-05-05 12:18:30', 1, 4, NULL, 1, NULL, 'SbJNKVtCtMRslNkOaAmr7hN1GBPLTKzT', b'0'),
(9, 5, 'PO-0018', 5, 4, '2015-05-05 12:19:06', 1, 4, NULL, 1, NULL, 'xL8sIqr2mfUw8h3EzE1aKpClc1Wy5Qn2', b'0'),
(10, 5, 'PO-0019', 5, 4, '2015-05-05 12:20:43', 1, 4, NULL, 1, NULL, '54DSrljEr8plPZ79Zs1Ad78Kwmf1nm6A', b'0'),
(11, 5, 'PO-0020', 5, 4, '2015-05-05 12:25:19', 1, 4, NULL, 3, NULL, 'ySkzgSaNIvi41Oi0WEasuiV8spkzWdQe', b'0'),
(12, 5, 'PO-0021', 5, 4, '2015-05-07 10:03:38', 1, 4, NULL, 3, NULL, 'nzvRdNDp3amuM5Yf15TRo3C7ErgJAfXd', b'0'),
(13, 5, 'PO-0022', 5, 4, '2015-05-07 10:06:31', 1, 4, NULL, 3, NULL, 'GxNaQLxJH8S5RfQCM36WQiRCJlYr9Xtp', b'0'),
(14, 5, 'PO-0023', 5, 4, '2015-05-07 10:55:12', 1, 4, NULL, 3, NULL, 'Gs7hHLUQnezVBXWQl71x5eMvhtRforzP', b'0'),
(15, 5, 'PO-0024', 5, 4, '2015-05-08 15:20:03', 1, 4, NULL, 3, NULL, 'Q8lLiDG83YPW0w74Uhja6Q0W6wKfLFfR', b'0'),
(16, 30, 'PO-0025', 2, 1, '2015-05-13 15:54:52', 1, 2, NULL, 1, NULL, 'Hw3YFyxDRXC2Y0V7Xgznx6FGtyTQjE9q', b'0'),
(17, 30, 'PO-0025', 2, 1, '2015-05-13 15:54:52', 1, 8, NULL, 1, NULL, '5ejtz49aze1V1B4HUiGsDS4LeZX7iIhZ', b'0'),
(18, 30, 'PO-0025', 2, 1, '2015-05-13 15:54:52', 2, 3, NULL, 1, NULL, 'aMn5T7iw27YlBqJXNHuqdgBiYeSIqo6B', b'0'),
(19, 30, 'PO-0025', 2, 1, '2015-05-13 15:54:52', 2, 8, NULL, 1, NULL, 'H2cAiO4U0JgTMXFKDbWD3ZTEHQoYjFDF', b'0'),
(20, 56, 'PO-0026', 5, 4, '2015-05-18 16:41:57', 1, 4, NULL, 3, NULL, '9tWsRCHyn9jaEsHF8Z4cy7evfF0ui569', b'0'),
(21, 57, 'PO-0028', 4, 5, '2015-05-21 11:54:31', 1, 4, NULL, 3, NULL, 'UPdJgFINm1CoTKx4Psnu4IhUWJMIuIOo', b'0'),
(22, 56, 'PO-0030', 5, 4, '2015-05-25 02:34:34', 1, 4, NULL, 3, NULL, '2VczKYbcoxS5Xns4k1b9jISLgcMmO90x', b'0'),
(23, 56, 'RCV-0020', 5, 4, '2015-05-25 02:40:28', 1, 4, NULL, 5, NULL, 'ePXmK3UZvwYkXwAgWEoMG63Twasadp1U', b'0'),
(24, 56, 'PO-0032', 5, 4, '2015-05-25 02:41:54', 1, 4, NULL, 3, NULL, 'tv5UNUFG9ikUgnHzfnmtT5NY29unOmk2', b'0'),
(25, 61, 'AD2805150001', 5, 4, '2015-05-28 02:29:28', 1, 4, NULL, 3, NULL, 'GdrBu4apvsj8N36AwAYLyV2dNmrn28Mr', b'0'),
(26, 62, 'AD2805150001', 4, 5, '2015-05-28 02:36:05', 1, 4, NULL, 1, NULL, 'dAVo7XA7FQFPTznKA4jTyay219rnrqRE', b'0'),
(27, 62, 'AD2805150001', 4, 5, '2015-05-28 02:40:25', 1, 4, NULL, 1, NULL, 'hOhCGCspM7xJmO5Rep5rBnbfJOwnTzZs', b'0'),
(28, 62, 'AD2805150001', 4, 5, '2015-05-28 03:08:48', 1, 4, NULL, 1, NULL, 'b5Atbdl27ZYCXOwCEUzKMuUrOUejZC54', b'0'),
(29, 56, 'PO-0033', 5, 4, '2015-06-04 20:33:32', 1, 4, NULL, 3, NULL, 'mV2TlY2BKaDHIt7wub6MplUIr8EJ14LD', b'0'),
(30, 57, 'PO-0034', 4, 5, '2015-06-18 21:37:09', 1, 4, NULL, 3, NULL, 'OU8YUslmhLceJdFUPXcEhB0NUYTpS3zV', b'0'),
(31, 57, 'RCV-0022', 4, 5, '2015-06-18 21:38:05', 1, 4, NULL, 1, NULL, 't2oH4rWHEo10JPtBPtutC8lAcfZPjMhg', b'0'),
(32, 81, 'PCT-0003', 4, 5, '2015-06-19 01:17:37', 1, 4, NULL, 3, NULL, '4A5Vo9XRDV1yBHMdWVsUuKIHcdEkvYkm', b'0'),
(33, 57, 'PO-0035', 4, 5, '2015-06-19 03:12:04', 1, 4, NULL, 3, NULL, 'X1AHNabRGocPAyWt6y7hVMxqejDgV1Ot', b'0'),
(34, 81, 'PCT-0004', 4, 5, '2015-06-19 03:14:28', 1, 4, NULL, 3, NULL, 'PHUsoMeMi5hNIGb1L2SV4IZzy9hX4xeA', b'0');

DROP TABLE IF EXISTS `t_document_flow`;
CREATE TABLE IF NOT EXISTS `t_document_flow` (
  `id` int(11) NOT NULL,
  `module` varchar(50) NOT NULL,
  `doc` varchar(50) NOT NULL,
  `doc_type` varchar(10) NOT NULL,
  `table_reff` varchar(50) NOT NULL,
  `table_info` varchar(50) DEFAULT NULL,
  `order` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `t_document_flow` (`id`, `module`, `doc`, `doc_type`, `table_reff`, `table_info`, `order`) VALUES
(1, 'purchase', 'purchase_order', 'PO', 't_purchase_order_detail', 't_purchase_order_header', 1),
(2, 'purchase', 'purchase_receipt', 'RCV', 't_purchase_receipt_detail', 't_purchase_receipt_header', 2),
(3, 'purchase', 'purchase_invoice', 'PIV', 't_purchase_invoice_detail', 't_purchase_invoice_header', 3),
(4, 'purchase', 'payment_out', 'PYO', 't_payment_detail', 't_payment_header', 4);

DROP TABLE IF EXISTS `t_document_series`;
CREATE TABLE IF NOT EXISTS `t_document_series` (
  `series_id` int(11) NOT NULL,
  `document_id` varchar(45) NOT NULL,
  `series_name` varchar(255) NOT NULL,
  `series_prefix` varchar(45) DEFAULT NULL,
  `first_num` int(11) NOT NULL,
  `next_num` int(11) NOT NULL,
  `last_num` int(11) NOT NULL,
  `series_suffix` varchar(45) DEFAULT NULL,
  `is_active` bit(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

INSERT INTO `t_document_series` (`series_id`, `document_id`, `series_name`, `series_prefix`, `first_num`, `next_num`, `last_num`, `series_suffix`, `is_active`) VALUES
(1, '1', 'Auto', 'PO', 1, 36, 9999, NULL, b'1'),
(2, '2', 'Auto', 'TRF', 1, 14, 9999, NULL, b'1'),
(3, '3', 'Auto', 'RCV', 1, 23, 9999, NULL, b'1'),
(4, '4', 'Auto', 'PIV', 1, 9, 9999, NULL, b'1'),
(5, '5', 'Auto', 'ACP', 1, 1, 9999, NULL, b'1'),
(6, '6', 'Auto', 'SAL', 1, 3, 9999, NULL, b'1'),
(7, '7', 'Auto', 'DEL', 1, 1, 9999, NULL, b'1'),
(8, '8', 'Auto', 'SIV', 1, 1, 9999, NULL, b'1'),
(9, '9', 'Auto', 'PYO', 1, 5, 9999, NULL, b'1'),
(10, '10', 'Auto', 'PYI', 1, 2, 9999, NULL, b'1'),
(11, '11', 'Auto', 'SDP', 1, 1, 9999, NULL, b'1'),
(12, '12', 'Auto', 'PDP', 1, 1, 9999, NULL, b'1'),
(13, '13', 'Auto', 'JR', 1, 54, 99999, NULL, b'1'),
(14, '14', 'Auto', 'SPL', 1, 20, 99, NULL, b'1'),
(15, '15', 'Auto', 'OTC', 1, 1, 99, NULL, b'1'),
(16, '16', 'Auto', 'ADJ', 1, 1, 9999, NULL, b'1'),
(17, '17', 'Auto', 'PRT', 1, 1, 9999, NULL, b'1'),
(18, '18', 'Auto', 'SRT', 1, 1, 9999, NULL, b'1'),
(19, '19', 'Auto', 'PRQ', 1, 5, 9999, NULL, b'1'),
(20, '20', 'Auto', 'PCT', 1, 5, 9999, NULL, b'1');

DROP TABLE IF EXISTS `t_email_queue`;
CREATE TABLE IF NOT EXISTS `t_email_queue` (
  `id` int(11) NOT NULL,
  `from_email` varchar(255) NOT NULL DEFAULT '',
  `from_name` varchar(255) NOT NULL DEFAULT '',
  `to_email` varchar(255) NOT NULL DEFAULT '',
  `reply_to` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(1000) NOT NULL DEFAULT '',
  `message` varchar(8000) NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sent_flag` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_employee`;
CREATE TABLE IF NOT EXISTS `t_employee` (
  `id` int(11) NOT NULL,
  `emp_no` varchar(45) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `birthdate` datetime DEFAULT NULL,
  `position_id` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Employee Data Store';

INSERT INTO `t_employee` (`id`, `emp_no`, `first_name`, `last_name`, `birthdate`, `position_id`, `user_id`, `location_id`, `gender`) VALUES
(1, NULL, 'Admin', '', '2015-01-01 00:00:00', '2', 1, 1, 'M'),
(2, 'EMP0090', 'Staff', 'Admin', '1980-01-01 00:00:00', '5', 4, 1, 'male'),
(3, 'EMP0002', 'Manager', 'Admin', '1980-01-01 00:00:00', '4', 5, 1, 'male'),
(4, 'EMP0056', 'kasir', 'kasir', '1980-01-01 00:00:00', '5', 6, 2, 'female'),
(5, 'EMP0012', 'Marketing', 'Supervisor', '1980-01-01 00:00:00', '10', 7, 1, 'female');

DROP TABLE IF EXISTS `t_expense`;
CREATE TABLE IF NOT EXISTS `t_expense` (
  `exp_id` int(11) NOT NULL,
  `exp_dt` datetime NOT NULL,
  `exp_doc` varchar(50) NOT NULL,
  `exp_desc` varchar(500) DEFAULT NULL,
  `exp_curr` varchar(5) NOT NULL,
  `exp_amount` decimal(10,2) NOT NULL,
  `is_accrue` bit(1) DEFAULT NULL,
  `accrue_to` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `t_group_menu_hide`;
CREATE TABLE IF NOT EXISTS `t_group_menu_hide` (
  `group_id` int(11) unsigned NOT NULL,
  `menu_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_group_menu_hide` (`group_id`, `menu_id`) VALUES
(4, 1),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(4, 6),
(4, 7),
(4, 9),
(4, 10),
(4, 11),
(4, 14),
(4, 16),
(4, 17),
(4, 18),
(4, 19),
(4, 20),
(4, 21),
(4, 22),
(4, 23),
(4, 24),
(4, 25),
(4, 26),
(4, 27),
(4, 28),
(4, 29),
(4, 30),
(4, 31),
(4, 32),
(4, 33),
(4, 35),
(4, 36),
(4, 38),
(4, 39),
(4, 40),
(4, 41),
(4, 42),
(4, 43),
(4, 44),
(4, 45),
(4, 46),
(4, 47),
(4, 48),
(4, 49),
(4, 50),
(4, 51),
(4, 52),
(4, 53),
(4, 54),
(4, 55),
(4, 56),
(4, 57),
(4, 58),
(4, 59),
(4, 60),
(4, 61),
(4, 62),
(4, 63),
(4, 64),
(4, 65),
(4, 66),
(4, 67),
(4, 68),
(4, 69),
(4, 70),
(4, 71),
(4, 72),
(4, 73),
(4, 74),
(4, 75),
(4, 76),
(4, 77),
(4, 78),
(4, 80),
(4, 81),
(4, 82),
(4, 83),
(4, 88),
(4, 89),
(4, 90),
(4, 91),
(4, 92),
(4, 93),
(4, 94),
(4, 95),
(4, 96),
(4, 97),
(4, 98),
(4, 99),
(4, 100),
(4, 101);

DROP TABLE IF EXISTS `t_inventory`;
CREATE TABLE IF NOT EXISTS `t_inventory` (
  `item_id` int(11) NOT NULL COMMENT 'Skyware Item Master',
  `item_g_id` int(11) NOT NULL,
  `item_code` varchar(50) NOT NULL COMMENT 'Sku',
  `item_name` varchar(100) NOT NULL,
  `item_tag` varchar(500) DEFAULT NULL,
  `item_category` varchar(50) DEFAULT NULL,
  `item_category_char` varchar(5) DEFAULT NULL,
  `item_style` varchar(50) DEFAULT NULL COMMENT 'article',
  `item_style_char` varchar(5) DEFAULT NULL,
  `item_style_launch` datetime DEFAULT NULL,
  `item_manufacturer` varchar(50) DEFAULT NULL,
  `item_manufacturer_char` varchar(5) DEFAULT NULL,
  `item_color` varchar(50) DEFAULT NULL,
  `item_color_char` varchar(20) DEFAULT NULL,
  `item_image` varchar(100) DEFAULT NULL,
  `item_inventory` tinyint(1) DEFAULT NULL,
  `item_sales` tinyint(1) DEFAULT NULL,
  `item_purchase` tinyint(1) DEFAULT NULL,
  `qty_onhand` decimal(10,2) NOT NULL DEFAULT '0.00',
  `qty_tocome` decimal(10,2) NOT NULL DEFAULT '0.00',
  `qty_togo` decimal(10,2) NOT NULL DEFAULT '0.00',
  `qty_minlevel` decimal(10,2) DEFAULT '0.00',
  `qty_maxlevel` decimal(10,2) DEFAULT '0.00',
  `unit_cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `uom` varchar(45) NOT NULL DEFAULT 'Pcs',
  `uom_inventory` int(11) DEFAULT NULL,
  `uom_sales` int(11) DEFAULT NULL,
  `uom_purchase` int(11) DEFAULT NULL,
  `unit_barcode` varchar(20) DEFAULT NULL,
  `item_brand` varchar(50) DEFAULT NULL,
  `item_valuation` varchar(20) NOT NULL DEFAULT 'AVG',
  `item_mrp` varchar(5) NOT NULL DEFAULT 'NON',
  `item_leadtime` int(11) DEFAULT NULL,
  `item_prod_method` varchar(10) DEFAULT NULL,
  `item_sell_price` decimal(10,2) DEFAULT NULL,
  `item_buy_price` decimal(10,2) DEFAULT NULL,
  `item_status` varchar(50) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

INSERT INTO `t_inventory` (`item_id`, `item_g_id`, `item_code`, `item_name`, `item_tag`, `item_category`, `item_category_char`, `item_style`, `item_style_char`, `item_style_launch`, `item_manufacturer`, `item_manufacturer_char`, `item_color`, `item_color_char`, `item_image`, `item_inventory`, `item_sales`, `item_purchase`, `qty_onhand`, `qty_tocome`, `qty_togo`, `qty_minlevel`, `qty_maxlevel`, `unit_cost`, `uom`, `uom_inventory`, `uom_sales`, `uom_purchase`, `unit_barcode`, `item_brand`, `item_valuation`, `item_mrp`, `item_leadtime`, `item_prod_method`, `item_sell_price`, `item_buy_price`, `item_status`) VALUES
(1, 2, '1125380', 'T Shirt A', '', '', 'CS', '', '', NULL, '', '', 'Blue', 'BLUE', '', 1, 1, 1, '98.00', '264.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, '', 'AVG', 'NON', 0, NULL, '175000.00', '85000.00', 'ACTIVE'),
(2, 2, '1241093', 'T Shirt B', '', '', 'MB', '', '', NULL, '', '', 'Red', 'RED', '', 1, 1, 1, '100.00', '364.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, '', 'AVG', 'NON', 0, NULL, '185000.00', '85000.00', 'ACTIVE'),
(3, 2, '1508349', 'T Shirt C', '', '', 'MB', '', '', NULL, '', '', 'Red', 'RED', '', 1, 1, 1, '100.00', '306.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, '', 'AVG', 'NON', 0, NULL, '169900.00', '85000.00', 'ACTIVE'),
(4, 2, '1350284', 'T Shirt D', '', '', 'MT', '', '', NULL, '', '', 'Pink', 'PINK', '', 1, 1, 1, '100.00', '24.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, '', 'AVG', 'NON', 0, NULL, '179900.00', '85000.00', 'ACTIVE'),
(5, 2, '2503439', 'Jeans A', '', '', 'MT', '', '', NULL, '', '', 'Brown', 'BROWN', '', 1, 1, 1, '100.00', '5.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, '', 'AVG', 'NON', 0, NULL, '149500.00', '85000.00', 'ACTIVE'),
(6, 2, '2304079', 'Jeans B', '', '', 'CS', '', '', NULL, '', '', 'Beige', 'BEIGE', '', 1, 1, 1, '100.00', '6.00', '1.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, '', 'AVG', 'NON', 0, NULL, '209900.00', '85000.00', 'ACTIVE'),
(7, 2, '2140392', 'Jeans C', '', '', 'RM', '', '', NULL, '', '', 'Purple', 'PURPLE', '', 1, 1, 1, '100.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, '', 'AVG', 'NON', 0, NULL, '129900.00', '85000.00', 'ACTIVE'),
(8, 2, '1408345', 'Jeans D', '', '', 'HD', '', '', NULL, '', '', 'Mustard', 'MUSTARD', '', 1, 1, 1, '100.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, '', 'AVG', 'NON', 0, NULL, '129900.00', '85000.00', 'ACTIVE'),
(9, 2, '3150194', 'Jeans E', '', '', 'HD', '', '', NULL, '', '', 'Black', 'BLACK', '', 1, 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, '', 'AVG', 'NON', 0, NULL, '129900.00', '85000.00', 'ACTIVE'),
(10, 2, '6153089', 'Tas A', '', '', 'RM', '', '', NULL, '', '', 'Blue', 'BLUE', '', 1, 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, '', 'AVG', 'NON', 0, NULL, '345500.00', '85000.00', 'ACTIVE'),
(11, 3, '4318947', 'Tas B', '', '', 'LP', '', '', NULL, '', '', 'White', 'WHITE', '', 1, 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, NULL, 'AVG', 'NON', 0, NULL, '369900.00', '85000.00', 'ACTIVE'),
(12, 3, '6916231', 'Tas C', '', '', 'LP', '', '', NULL, '', '', 'Pink', 'PINK', '', 1, 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, NULL, 'AVG', 'NON', 0, NULL, '299900.00', '85000.00', 'ACTIVE'),
(13, 3, '1461284', 'Tas D', '', '', 'LP', '', '', NULL, '', '', 'Grey', 'GREY', '', 1, 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, NULL, 'AVG', 'NON', NULL, NULL, '349500.00', '85000.00', 'ACTIVE'),
(14, 3, '1597477', 'Tas E', '', '', 'LP', '', '', NULL, '', '', 'Cream', 'CREAM', '', 1, 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, NULL, 'AVG', 'NON', NULL, NULL, '399000.00', '85000.00', 'ACTIVE'),
(15, 3, '1428698', 'Sendal A', '', '', 'LP', '', '', NULL, '', '', 'Green', 'GREEN', '', 1, 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, NULL, 'AVG', 'NON', NULL, NULL, '99000.00', '85000.00', 'ACTIVE'),
(16, 3, '4792516', 'Sendal B', '', '', 'LP', '', '', NULL, '', '', 'Black', 'BLACK', '', 1, 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, NULL, 'AVG', 'NON', NULL, NULL, '89900.00', '85000.00', 'ACTIVE'),
(17, 4, '7843782', 'Sendal C', '', '', 'KM', '', '', NULL, '', '', 'Black', 'BLACK', '', 1, 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, NULL, 'AVG', 'NON', NULL, NULL, '65500.00', '85000.00', 'ACTIVE'),
(18, 4, '4091994', 'Sepatu A', '', '', 'KM', '', '', NULL, '', '', 'Blue', 'BLUE', '', 1, 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, NULL, 'AVG', 'NON', NULL, NULL, '79900.00', '85000.00', 'ACTIVE'),
(19, 4, '2575372', 'Sepatu B', '', '', 'KM', '', '', NULL, '', '', 'Brown', 'BROWN', '', 1, 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, NULL, 'AVG', 'NON', NULL, NULL, '65500.00', '85000.00', 'ACTIVE'),
(20, 4, '4991827', 'Jas A', '', '', 'KM', '', '', NULL, '', '', 'Red', 'RED', '', 1, 1, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Pcs', 1, NULL, NULL, NULL, NULL, 'AVG', 'NON', NULL, NULL, '1119000.00', '85000.00', 'ACTIVE'),
(23, 3, 'LENOVO_LAVIEZ', 'Lenovo LaVie Z105', NULL, NULL, NULL, 'F', 'F', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Unit', NULL, NULL, NULL, NULL, NULL, 'AVG', 'NON', NULL, NULL, NULL, NULL, 'ACTIVE');

DROP TABLE IF EXISTS `t_inventory_category`;
CREATE TABLE IF NOT EXISTS `t_inventory_category` (
  `category_id` int(11) NOT NULL,
  `category_code` varchar(10) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `item_group` int(11) NOT NULL,
  `acc_id_inventory` int(11) DEFAULT NULL,
  `acc_id_expense` int(11) DEFAULT NULL,
  `acc_id_sales` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO `t_inventory_category` (`category_id`, `category_code`, `category_name`, `item_group`, `acc_id_inventory`, `acc_id_expense`, `acc_id_sales`) VALUES
(1, 'MB', 'MotherBoard', 2, NULL, NULL, NULL),
(2, 'CS', 'Casing', 2, NULL, NULL, NULL),
(3, 'MT', 'Monitor', 2, NULL, NULL, NULL),
(4, 'HD', 'Hard Disk', 2, NULL, NULL, NULL),
(5, 'RM', 'RAM', 2, NULL, NULL, NULL),
(6, 'VC', 'Video Card', 3, NULL, NULL, NULL),
(7, 'KM', 'Keyboard and Mouse', 4, NULL, NULL, NULL),
(8, 'LP', 'Laptop', 3, NULL, NULL, NULL),
(9, 'TB', 'Tablet', 3, NULL, NULL, NULL),
(10, 'ABC', 'ABC2', 0, 4, 5, 70);

DROP TABLE IF EXISTS `t_inventory_group`;
CREATE TABLE IF NOT EXISTS `t_inventory_group` (
  `item_g_id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `group_label` varchar(255) NOT NULL,
  `group_on_web` int(11) NOT NULL,
  `group_image` varchar(500) DEFAULT NULL,
  `group_char` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO `t_inventory_group` (`item_g_id`, `group_name`, `group_label`, `group_on_web`, `group_image`, `group_char`) VALUES
(1, 'Raw Material', 'RM', 0, NULL, ''),
(2, 'FG - Computer', 'Computer', 1, 'assets/img/prod/computer/_group.jpg', ''),
(3, 'FG - Laptop', 'Laptop', 1, 'assets/img/prod/laptop/_group.jpg', ''),
(4, 'FG - Accessories', 'Accessories', 1, 'assets/img/prod/accs/_group.jpg', ''),
(5, 'SP - Office Supplies', 'Office Supplies', 1, '', ''),
(6, 'SP - Others', 'Others', 1, '', '');

DROP TABLE IF EXISTS `t_inventory_image`;
CREATE TABLE IF NOT EXISTS `t_inventory_image` (
  `item_id` int(11) NOT NULL,
  `image_path` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `t_inventory_style`;
CREATE TABLE IF NOT EXISTS `t_inventory_style` (
  `style_id` int(11) NOT NULL,
  `style_code` varchar(10) NOT NULL,
  `style_name` varchar(50) NOT NULL,
  `style_date` datetime DEFAULT NULL,
  `style_default_image` varchar(250) DEFAULT NULL,
  `category_code` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `t_inventory_style` (`style_id`, `style_code`, `style_name`, `style_date`, `style_default_image`, `category_code`) VALUES
(1, 'A', 'A', '2014-05-01 00:00:00', '', ''),
(2, 'B', 'B', '2014-06-01 00:00:00', '', ''),
(3, 'C', 'C', '2014-06-01 00:00:00', '', ''),
(4, 'D', 'D', '2014-06-01 00:00:00', '', ''),
(5, 'E', 'E', '2014-08-01 00:00:00', '', ''),
(6, 'F', 'F', '2014-08-01 00:00:00', NULL, ''),
(7, 'G', 'G', '2014-09-20 00:00:00', NULL, ''),
(8, 'H', 'H', '2014-09-20 00:00:00', NULL, '');

DROP TABLE IF EXISTS `t_inventory_transaction`;
CREATE TABLE IF NOT EXISTS `t_inventory_transaction` (
  `trans_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `doc_type` varchar(250) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_date` datetime NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_qty` decimal(10,2) NOT NULL,
  `item_value` decimal(10,2) NOT NULL,
  `total_value` decimal(10,2) NOT NULL,
  `whse_id` int(11) NOT NULL,
  `trx_status` varchar(25) NOT NULL DEFAULT 'ORIGINAL',
  `create_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO `t_inventory_transaction` (`trans_id`, `timestamp`, `doc_type`, `doc_num`, `doc_date`, `item_id`, `item_qty`, `item_value`, `total_value`, `whse_id`, `trx_status`, `create_by`) VALUES
(1, '2014-12-31 17:00:00', 'OPBAL', 'OPBAL', '2015-01-01 00:00:00', 1, '100.00', '50000.00', '5000000.00', 1, 'OPEN', '1'),
(2, '2014-12-31 17:00:00', 'OPBAL', 'OPBAL', '2015-01-01 00:00:00', 2, '100.00', '50000.00', '5000000.00', 1, 'OPEN', '1'),
(3, '2014-12-31 17:00:00', 'OPBAL', 'OPBAL', '2015-01-01 00:00:00', 3, '100.00', '50000.00', '5000000.00', 1, 'OPEN', '1'),
(4, '2014-12-31 17:00:00', 'OPBAL', 'OPBAL', '2015-01-01 00:00:00', 4, '100.00', '50000.00', '5000000.00', 1, 'OPEN', '1'),
(5, '2014-12-31 17:00:00', 'OPBAL', 'OPBAL', '2015-01-01 00:00:00', 5, '100.00', '50000.00', '5000000.00', 1, 'OPEN', '1'),
(6, '2014-12-31 17:00:00', 'OPBAL', 'OPBAL', '2015-01-01 00:00:00', 6, '100.00', '50000.00', '5000000.00', 1, 'OPEN', '1'),
(7, '2014-12-31 17:00:00', 'OPBAL', 'OPBAL', '2015-01-01 00:00:00', 7, '100.00', '50000.00', '5000000.00', 1, 'OPEN', '1'),
(8, '2014-12-31 17:00:00', 'OPBAL', 'OPBAL', '2015-01-01 00:00:00', 8, '100.00', '50000.00', '5000000.00', 1, 'OPEN', '1'),
(9, '2016-01-30 09:46:36', 'SAL', 'POS2120160130', '2016-01-30 00:00:00', 1, '-2.00', '0.00', '0.00', 2, 'ORIGINAL', NULL);

DROP TABLE IF EXISTS `t_inventory_warehouse`;
CREATE TABLE IF NOT EXISTS `t_inventory_warehouse` (
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `qty_onhand` decimal(10,2) NOT NULL DEFAULT '0.00',
  `qty_tocome` decimal(10,2) NOT NULL DEFAULT '0.00',
  `qty_togo` decimal(10,2) NOT NULL DEFAULT '0.00',
  `qty_minlevel` decimal(10,2) NOT NULL DEFAULT '0.00',
  `qty_maxlevel` decimal(10,2) NOT NULL DEFAULT '0.00',
  `unit_cost` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_inventory_warehouse` (`item_id`, `location_id`, `qty_onhand`, `qty_tocome`, `qty_togo`, `qty_minlevel`, `qty_maxlevel`, `unit_cost`) VALUES
(1, 1, '100.00', '262.00', '0.00', '0.00', '0.00', '0.00'),
(1, 2, '-2.00', '2.00', '0.00', '0.00', '0.00', '0.00'),
(1, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(1, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(1, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(2, 1, '100.00', '362.00', '0.00', '0.00', '0.00', '0.00'),
(2, 2, '0.00', '2.00', '0.00', '0.00', '0.00', '0.00'),
(2, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(2, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(2, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(3, 1, '100.00', '306.00', '0.00', '0.00', '0.00', '0.00'),
(3, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(3, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(3, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(3, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(4, 1, '100.00', '24.00', '0.00', '0.00', '0.00', '0.00'),
(4, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(4, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(4, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(4, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(5, 1, '100.00', '5.00', '0.00', '0.00', '0.00', '0.00'),
(5, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(5, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(5, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(5, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(6, 1, '100.00', '6.00', '1.00', '0.00', '0.00', '0.00'),
(6, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(6, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(6, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(6, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(7, 1, '100.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(7, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(7, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(7, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(7, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(8, 1, '100.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(8, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(8, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(8, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(8, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(9, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(9, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(9, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(9, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(9, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(10, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(10, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(10, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(10, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(10, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(11, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(11, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(11, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(11, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(11, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(12, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(12, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(12, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(12, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(12, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(13, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(13, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(13, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(13, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(13, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(14, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(14, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(14, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(14, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(14, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(15, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(15, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(15, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(15, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(15, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(16, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(16, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(16, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(16, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(16, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(17, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(17, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(17, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(17, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(17, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(18, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(18, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(18, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(18, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(18, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(19, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(19, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(19, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(19, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(19, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(20, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(20, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(20, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(20, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(20, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(23, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(23, 2, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(23, 3, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(23, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00'),
(23, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');

DROP TABLE IF EXISTS `t_ion_groups`;
CREATE TABLE IF NOT EXISTS `t_ion_groups` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `t_ion_groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'manager', 'Managers'),
(3, 'staff', 'Staff'),
(4, 'kasir', 'Cashier POS');

DROP TABLE IF EXISTS `t_ion_login_attempts`;
CREATE TABLE IF NOT EXISTS `t_ion_login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_ion_users`;
CREATE TABLE IF NOT EXISTS `t_ion_users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO `t_ion_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1435564537, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(4, '::1', 'staff', '$2y$08$5pSYoG5UUBjgs08wPxIsnO2XlisS1KFLT7yIJU9JpfInKmXeXh9R2', NULL, 'iprastha@gmail.com', NULL, NULL, NULL, NULL, 1430280405, 1434930628, 1, 'Staff', 'Admin', NULL, NULL),
(5, '::1', 'manager', '$2y$08$cjRkoeCOxqB3/OEIQuWkWOIvJ8M.wbxgNtuN4R3VjHwuWwd.Mh7SG', NULL, 'iprastha@gmail.com', NULL, NULL, NULL, NULL, 1430280504, 1454146700, 1, 'Manager', 'Admin', NULL, NULL),
(6, '::1', 'kasir', '$2y$08$JsIWxewOUyh07DzGFS.ZS.yeljWfqhjUWPhq3veCoe3p/52Yfbzyy', NULL, 'iprastha@gmail.com', NULL, NULL, NULL, NULL, 1430280592, 1430293280, 1, 'kasir', 'kasir', NULL, NULL),
(7, '::1', 'marketing', '$2y$08$wnVYrGC72xVwtKLhv6hjZ.DeeCcEr3x0oX942S4VNewBMgae29qMO', NULL, 'marketing@indoskyware.com', NULL, NULL, NULL, NULL, 1431587318, NULL, 1, 'Marketing', 'Supervisor', NULL, NULL);

DROP TABLE IF EXISTS `t_ion_users_groups`;
CREATE TABLE IF NOT EXISTS `t_ion_users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO `t_ion_users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(5, 4, 3),
(6, 5, 2),
(7, 6, 4),
(8, 7, 2);

DROP TABLE IF EXISTS `t_link_account`;
CREATE TABLE IF NOT EXISTS `t_link_account` (
  `id` int(11) NOT NULL,
  `code` varchar(45) NOT NULL,
  `account_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `t_link_account` (`id`, `code`, `account_id`) VALUES
(1, 'sales.cogs', 44),
(2, 'inventory.asset', 9),
(3, 'sales.cash', 5),
(4, 'sales.revenue', 35),
(5, 'sales.vat_out', 25),
(6, 'purchase.accrue', 22);

DROP TABLE IF EXISTS `t_maint_order`;
CREATE TABLE IF NOT EXISTS `t_maint_order` (
  `maint_order_id` int(11) NOT NULL,
  `maint_order_pic` int(11) NOT NULL,
  `maint_order_date` date NOT NULL,
  `maint_order_schedule_id` int(11) DEFAULT NULL,
  `maint_order_vendor_id` int(11) DEFAULT NULL,
  `maint_order_cost_currency` enum('RP','USD','SGD','') NOT NULL,
  `maint_order_cost` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `t_maint_order` (`maint_order_id`, `maint_order_pic`, `maint_order_date`, `maint_order_schedule_id`, `maint_order_vendor_id`, `maint_order_cost_currency`, `maint_order_cost`) VALUES
(1, 2, '2014-08-03', 1, 0, 'RP', 0);

DROP TABLE IF EXISTS `t_maint_order_detail`;
CREATE TABLE IF NOT EXISTS `t_maint_order_detail` (
  `maint_order_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `maint_type_id` int(11) NOT NULL,
  `maint_note` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `t_maint_order_detail` (`maint_order_id`, `asset_id`, `maint_type_id`, `maint_note`) VALUES
(1, 28, 1, 'Clean Up AC ini secara internal oleh team GA');

DROP TABLE IF EXISTS `t_maint_schedule`;
CREATE TABLE IF NOT EXISTS `t_maint_schedule` (
  `schedule_id` int(11) NOT NULL,
  `schedule_code` varchar(100) NOT NULL,
  `schedule_pic` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `t_maint_schedule` (`schedule_id`, `schedule_code`, `schedule_pic`) VALUES
(1, 'Service AC Gedung Utama 2014', 2),
(2, 'Car Regular Service', 2),
(3, 'Admin''s Schedule', 1),
(4, 'Ganti Oli Mobil', 2),
(5, '2014 - Ganti Oli Mobil', 2);

DROP TABLE IF EXISTS `t_maint_schedule_detail`;
CREATE TABLE IF NOT EXISTS `t_maint_schedule_detail` (
  `schedule_id` int(10) unsigned NOT NULL,
  `asset_id` int(10) unsigned NOT NULL,
  `maint_type_id` int(10) unsigned NOT NULL,
  `maint_status` enum('scheduled','overdue','rescheduled','in progress','finished') NOT NULL,
  `scheduled_date` datetime NOT NULL,
  `maint_note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_maint_schedule_detail` (`schedule_id`, `asset_id`, `maint_type_id`, `maint_status`, `scheduled_date`, `maint_note`) VALUES
(1, 28, 1, 'scheduled', '2014-08-10 00:00:00', 'Service 1'),
(1, 2, 1, 'scheduled', '2014-08-14 00:00:00', 'Service 2'),
(1, 14, 1, 'scheduled', '2014-08-20 00:00:00', 'Service 3'),
(2, 2, 1, 'scheduled', '2014-07-31 00:00:00', 'This is overdue'),
(3, 4, 1, 'scheduled', '2014-08-03 00:00:00', 'Admin''s Schedule'),
(4, 12, 1, 'scheduled', '2012-08-05 00:00:00', 'Gunakan Top-1  150W'),
(4, 10, 1, 'scheduled', '2012-08-05 00:00:00', 'Gunakan Shell Oil 140W'),
(5, 26, 1, 'scheduled', '2014-08-05 00:00:00', 'Mobil 1'),
(5, 28, 1, 'scheduled', '2014-08-05 00:00:00', 'Shell 140W');

DROP TABLE IF EXISTS `t_maint_type`;
CREATE TABLE IF NOT EXISTS `t_maint_type` (
  `maint_type_id` int(11) NOT NULL,
  `maint_type_code` varchar(10) NOT NULL,
  `maint_type_name` varchar(255) NOT NULL,
  `maint_type_desc` varchar(1000) NOT NULL,
  `maint_type_colour_code` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `t_maint_type` (`maint_type_id`, `maint_type_code`, `maint_type_name`, `maint_type_desc`, `maint_type_colour_code`) VALUES
(1, 'AC', 'Service AC', 'Service AC Gedung Utama', 'FFFFFF'),
(2, 'OLI', 'Ganti Oli', 'Ganti Oli di Mobil', 'AABBCC');

DROP TABLE IF EXISTS `t_payment_detail`;
CREATE TABLE IF NOT EXISTS `t_payment_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `reff_type` varchar(25) DEFAULT NULL,
  `reff_id` int(11) DEFAULT NULL,
  `reff_to_pay` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `t_payment_detail` (`doc_id`, `doc_line`, `reff_type`, `reff_id`, `reff_to_pay`) VALUES
(6, 1, 'PIV', 6, '935000.00'),
(7, 1, 'PIV', 1, '25500000.00'),
(7, 2, 'PIV', 4, '500000.00');

DROP TABLE IF EXISTS `t_payment_header`;
CREATE TABLE IF NOT EXISTS `t_payment_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime DEFAULT NULL,
  `doc_ddt` datetime DEFAULT NULL,
  `doc_status` varchar(25) DEFAULT NULL,
  `buss_id` int(11) DEFAULT NULL,
  `doc_type` varchar(10) DEFAULT NULL,
  `doc_curr` varchar(5) DEFAULT NULL,
  `doc_rate` decimal(10,2) DEFAULT NULL,
  `doc_subtotal` decimal(10,2) DEFAULT NULL,
  `doc_disc_percent` decimal(10,2) DEFAULT NULL,
  `doc_disc_amount` decimal(10,2) DEFAULT NULL,
  `doc_rounding` decimal(10,2) DEFAULT NULL,
  `doc_tax` decimal(10,2) DEFAULT NULL,
  `doc_total` decimal(10,2) DEFAULT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` datetime DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO `t_payment_header` (`doc_id`, `doc_num`, `doc_dt`, `doc_ddt`, `doc_status`, `buss_id`, `doc_type`, `doc_curr`, `doc_rate`, `doc_subtotal`, `doc_disc_percent`, `doc_disc_amount`, `doc_rounding`, `doc_tax`, `doc_total`, `doc_note`, `create_by`, `create_dt`, `update_by`, `update_dt`, `cancel_by`, `cancel_dt`, `cancel_reason`) VALUES
(1, 'PYI-0001', '2014-11-10 00:00:00', '2014-11-10 00:00:00', 'ACTIVE', 0, 'IN', 'IDR', '1.00', '0.00', NULL, NULL, NULL, NULL, '0.00', '', 'operation', '2014-11-10 13:11:13', NULL, NULL, NULL, NULL, NULL),
(4, 'PYO-0001', '2015-04-29 00:00:00', '2015-04-29 00:00:00', 'ACTIVE', 1, 'OUT', 'IDR', '1.00', '0.00', NULL, NULL, NULL, NULL, '0.00', '', 'administrator', '2015-04-29 15:04:02', NULL, NULL, NULL, NULL, NULL),
(5, 'PYO-0002', '2015-05-05 00:00:00', '2015-05-05 00:00:00', 'ACTIVE', 2, 'OUT', 'IDR', '1.00', '0.00', NULL, NULL, NULL, NULL, '0.00', '', 'administrator', '2015-05-05 11:05:59', NULL, NULL, NULL, NULL, NULL),
(6, 'PYO-0003', '2015-05-25 00:00:00', '2015-05-25 00:00:00', 'ACTIVE', 3, 'OUT', 'IDR', '1.00', '0.00', NULL, NULL, NULL, NULL, '0.00', '', 'staff', '2015-05-25 02:05:43', NULL, NULL, NULL, NULL, NULL),
(7, 'PYO-0004', '2015-05-27 00:00:00', '2015-05-27 00:00:00', 'ACTIVE', 1, 'OUT', 'IDR', '1.00', '0.00', NULL, NULL, NULL, NULL, '0.00', '', 'administrator', '2015-05-27 15:05:34', NULL, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_payment_mean`;
CREATE TABLE IF NOT EXISTS `t_payment_mean` (
  `doc_id` int(11) NOT NULL,
  `bank_code` varchar(50) DEFAULT NULL,
  `bank_ref` varchar(50) DEFAULT NULL,
  `bank_amt` decimal(10,2) DEFAULT NULL,
  `cash_code` varchar(50) DEFAULT NULL,
  `cash_amt` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `t_position`;
CREATE TABLE IF NOT EXISTS `t_position` (
  `position_id` int(11) NOT NULL,
  `position_code` varchar(100) NOT NULL,
  `position_name` varchar(500) NOT NULL,
  `position_desc` varchar(2000) DEFAULT NULL,
  `position_type` enum('division','position') NOT NULL DEFAULT 'position',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `last_node` int(11) DEFAULT '0',
  `order_no` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Master Division and Position of the Organization';

INSERT INTO `t_position` (`position_id`, `position_code`, `position_name`, `position_desc`, `position_type`, `parent_id`, `last_node`, `order_no`) VALUES
(1, 'BOD', 'Board of Directors', 'The Board of Directors and Commissioners of the company', 'division', 0, 0, 1),
(2, 'OPT', 'Operation Manager', 'Operation Manager', 'position', 1, 1, 2),
(3, 'MKT', 'Marketing Manager', 'Marketing Manager', 'position', 1, 1, 3),
(4, 'FIN', 'Finance Manager', 'Finance Manager', 'position', 1, 1, 4),
(5, 'STA', 'Staff', 'Staff', 'position', 1, 1, 5),
(6, 'FIN', 'Finance Division', 'Finance Division', 'division', 0, 0, 6),
(7, 'FIN.STAFF', 'Finance Staff', 'Finance Staff', 'position', 6, 1, 7),
(8, 'FIN.SPV', 'Finance Supervisor', 'Finance Supervisor', 'position', 6, 1, 8),
(9, 'MKT', 'Marketing', 'Marketing Division', 'division', 0, 0, 9),
(10, 'MKT.SPV', 'Marketing Supervisor', 'Marketing Supervisor', 'position', 9, 1, 10);

DROP TABLE IF EXISTS `t_purchase_contract_detail`;
CREATE TABLE IF NOT EXISTS `t_purchase_contract_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(1000) NOT NULL,
  `item_qty` decimal(18,2) NOT NULL,
  `item_price` decimal(18,2) NOT NULL,
  `item_uom` varchar(25) NOT NULL,
  `item_total` decimal(18,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_purchase_contract_detail` (`doc_id`, `doc_line`, `item_id`, `item_name`, `item_qty`, `item_price`, `item_uom`, `item_total`) VALUES
(1, 1, 1, 'T Shirt A', '1.00', '85000.00', '', '85000.00'),
(1, 2, 2, 'T Shirt B', '1.00', '85000.00', '', '85000.00'),
(2, 1, 1, 'T Shirt A', '1200.00', '85000.00', '', '102000000.00'),
(2, 2, 2, 'T Shirt B', '1200.00', '85000.00', '', '102000000.00'),
(3, 1, 1, 'T Shirt A', '100.00', '85000.00', '', '8500000.00'),
(3, 2, 2, 'T Shirt B', '100.00', '85000.00', '', '8500000.00'),
(3, 3, 3, 'T Shirt C', '100.00', '85000.00', '', '8500000.00'),
(3, 4, 4, 'T Shirt D', '100.00', '85000.00', '', '8500000.00'),
(4, 1, 2, 'T Shirt B', '100.00', '85000.00', '', '8500000.00'),
(4, 2, 3, 'T Shirt C', '100.00', '85000.00', '', '8500000.00');

DROP TABLE IF EXISTS `t_purchase_contract_header`;
CREATE TABLE IF NOT EXISTS `t_purchase_contract_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime NOT NULL,
  `doc_ddt` datetime NOT NULL,
  `doc_status` varchar(25) NOT NULL DEFAULT 'OPENED',
  `approval_status` enum('PENDING','PARTIALLY APPROVED','APPROVED','REVISE','REJECTED') NOT NULL DEFAULT 'PENDING',
  `buss_id` int(11) NOT NULL,
  `contact_name` varchar(100) DEFAULT NULL,
  `contact_phone` varchar(50) DEFAULT NULL,
  `contact_email` varchar(100) DEFAULT NULL,
  `agreement_method` varchar(100) DEFAULT NULL,
  `start_dt` datetime DEFAULT NULL,
  `end_dt` datetime DEFAULT NULL,
  `terminate_dt` datetime DEFAULT NULL,
  `sign_dt` datetime DEFAULT NULL,
  `payment_term` varchar(100) DEFAULT NULL,
  `payment_method` varchar(100) DEFAULT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `int_note` varchar(1000) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` datetime DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `t_purchase_contract_header` (`doc_id`, `doc_num`, `doc_dt`, `doc_ddt`, `doc_status`, `approval_status`, `buss_id`, `contact_name`, `contact_phone`, `contact_email`, `agreement_method`, `start_dt`, `end_dt`, `terminate_dt`, `sign_dt`, `payment_term`, `payment_method`, `doc_note`, `int_note`, `create_by`, `create_dt`, `update_by`, `update_dt`, `cancel_by`, `cancel_dt`, `cancel_reason`, `post_id`) VALUES
(1, 'PCT-0001', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'AWAITING', 'PENDING', 1, '', '', '', 'GENERAL', '2015-06-12 00:00:00', '2015-06-12 00:00:00', '0000-00-00 00:00:00', '2015-06-12 00:00:00', '', '', '', NULL, 'manager', '2015-06-12 23:06:56', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'PCT-0002', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'AWAITING', 'PENDING', 2, 'Mr Daryl', '021-12344', 'daryl@example.com', 'GENERAL', '2015-06-19 00:00:00', '2016-01-19 00:00:00', '0000-00-00 00:00:00', '2015-06-19 00:00:00', 'Net 30 days', 'Bank Transfer MANDIRI Only', '', NULL, 'manager', '2015-06-19 00:06:40', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'PCT-0003', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ACTIVE', 'APPROVED', 3, '', '', '', 'GENERAL', '2015-06-19 00:00:00', '2015-07-19 00:00:00', '0000-00-00 00:00:00', '2015-06-18 00:00:00', '', '', '', NULL, 'manager', '2015-06-19 01:06:37', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'PCT-0004', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ACTIVE', 'APPROVED', 4, '', '', '', 'GENERAL', '2015-06-19 00:00:00', '2015-12-19 00:00:00', '0000-00-00 00:00:00', '2015-06-19 00:00:00', '', '', '', NULL, 'manager', '2015-06-19 03:06:28', NULL, NULL, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_purchase_invoice_detail`;
CREATE TABLE IF NOT EXISTS `t_purchase_invoice_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `doc_line_type` varchar(5) DEFAULT NULL,
  `item_group` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(1000) NOT NULL,
  `item_qty` decimal(10,2) NOT NULL,
  `item_qty_closed` decimal(10,2) NOT NULL DEFAULT '0.00',
  `item_price` decimal(10,2) NOT NULL,
  `item_netprice` decimal(10,2) NOT NULL,
  `item_uom` varchar(25) DEFAULT NULL,
  `item_cost` decimal(10,2) DEFAULT NULL,
  `item_disc_amount` decimal(10,2) DEFAULT NULL,
  `item_disc_pct1` decimal(10,2) DEFAULT NULL,
  `item_disc_pct2` decimal(10,2) DEFAULT NULL,
  `item_tax` varchar(50) DEFAULT NULL,
  `item_total` decimal(10,2) DEFAULT NULL,
  `line_close` tinyint(4) NOT NULL DEFAULT '0',
  `reff_type` varchar(25) DEFAULT NULL,
  `reff_id` int(11) DEFAULT NULL,
  `reff_line` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_purchase_invoice_detail` (`doc_id`, `doc_line`, `doc_line_type`, `item_group`, `item_id`, `item_name`, `item_qty`, `item_qty_closed`, `item_price`, `item_netprice`, `item_uom`, `item_cost`, `item_disc_amount`, `item_disc_pct1`, `item_disc_pct2`, `item_tax`, `item_total`, `line_close`, `reff_type`, `reff_id`, `reff_line`) VALUES
(1, 1, 'ITEM', 2, 6, 'Simbadda', '200.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '99999999.99', 0, 'RCV', 4, 1),
(1, 2, 'ITEM', 2, 7, 'Corsair 4GB', '200.00', '0.00', '1400000.00', '1400000.00', NULL, '1400000.00', NULL, NULL, NULL, NULL, '99999999.99', 0, 'RCV', 4, 2),
(1, 3, 'ITEM', 2, 8, 'WD HDD 2.5, 500 GB', '200.00', '0.00', '700000.00', '700000.00', NULL, '700000.00', NULL, NULL, NULL, NULL, '99999999.99', 0, 'RCV', 4, 3),
(3, 1, 'ITEM', 2, 1, 'Casing A', '10.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '5000000.00', 0, 'RCV', 5, 1),
(3, 2, 'ITEM', 2, 2, 'Mobo XYZ', '15.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '22500000.00', 0, 'RCV', 5, 2),
(3, 3, 'ITEM', 2, 3, 'Mobo MP', '15.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '22500000.00', 0, 'RCV', 5, 3),
(4, 1, 'ITEM', 2, 1, 'Casing A', '10.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '5000000.00', 0, 'RCV', 6, 1),
(4, 2, 'ITEM', 2, 2, 'Mobo XYZ', '10.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '15000000.00', 0, 'RCV', 6, 2),
(4, 3, 'ITEM', 2, 3, 'Mobo MP', '15.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '22500000.00', 0, 'RCV', 6, 3),
(5, 1, 'ITEM', 2, 1, 'Casing A', '10.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '5000000.00', 0, 'RCV', 12, 1),
(5, 2, 'ITEM', 2, 2, 'Mobo XYZ', '10.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '15000000.00', 0, 'RCV', 12, 2),
(5, 3, 'ITEM', 2, 3, 'Mobo MP', '10.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '15000000.00', 0, 'RCV', 12, 3),
(6, 1, 'ITEM', 2, 1, 'T Shirt A', '5.00', '0.00', '93500.00', '93500.00', NULL, '93500.00', NULL, NULL, NULL, NULL, '467500.00', 0, 'RCV', 13, 1),
(6, 2, 'ITEM', 2, 2, 'T Shirt B', '5.00', '0.00', '93500.00', '93500.00', NULL, '93500.00', NULL, NULL, NULL, NULL, '467500.00', 0, 'RCV', 13, 2),
(7, 1, 'ITEM', 2, 1, 'T Shirt A', '5.00', '0.00', '93500.00', '93500.00', NULL, '93500.00', NULL, NULL, NULL, NULL, '467500.00', 0, 'RCV', 13, 1),
(7, 2, 'ITEM', 2, 2, 'T Shirt B', '5.00', '0.00', '93500.00', '93500.00', NULL, '93500.00', NULL, NULL, NULL, NULL, '467500.00', 0, 'RCV', 13, 2),
(8, 1, 'ITEM', 2, 1, 'T Shirt A', '10.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '5000000.00', 0, 'RCV', 12, 1),
(8, 2, 'ITEM', 2, 2, 'T Shirt B', '10.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '15000000.00', 0, 'RCV', 12, 2),
(8, 3, 'ITEM', 2, 3, 'T Shirt C', '10.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '15000000.00', 0, 'RCV', 12, 3);

DROP TABLE IF EXISTS `t_purchase_invoice_header`;
CREATE TABLE IF NOT EXISTS `t_purchase_invoice_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime NOT NULL,
  `doc_ddt` datetime NOT NULL,
  `doc_status` varchar(25) NOT NULL DEFAULT 'ACTIVE',
  `financial_status` varchar(25) NOT NULL DEFAULT 'UNPAID',
  `buss_id` int(11) NOT NULL,
  `ship_addr` varchar(1000) DEFAULT NULL,
  `bill_addr` varchar(1000) DEFAULT NULL,
  `whse_id` int(11) NOT NULL,
  `doc_type` varchar(10) DEFAULT NULL,
  `doc_ref` varchar(50) DEFAULT NULL,
  `doc_ext_ref` varchar(50) DEFAULT NULL,
  `doc_curr` varchar(5) DEFAULT NULL,
  `doc_rate` decimal(10,2) DEFAULT NULL,
  `doc_subtotal` decimal(10,2) DEFAULT NULL,
  `doc_disc_percent` decimal(10,2) DEFAULT NULL,
  `doc_disc_amount` decimal(10,2) DEFAULT NULL,
  `doc_rounding` decimal(10,2) DEFAULT NULL,
  `doc_tax` decimal(10,2) DEFAULT NULL,
  `doc_total` decimal(10,2) DEFAULT NULL,
  `doc_paid` decimal(10,2) DEFAULT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `int_note` varchar(1000) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` datetime DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO `t_purchase_invoice_header` (`doc_id`, `doc_num`, `doc_dt`, `doc_ddt`, `doc_status`, `financial_status`, `buss_id`, `ship_addr`, `bill_addr`, `whse_id`, `doc_type`, `doc_ref`, `doc_ext_ref`, `doc_curr`, `doc_rate`, `doc_subtotal`, `doc_disc_percent`, `doc_disc_amount`, `doc_rounding`, `doc_tax`, `doc_total`, `doc_paid`, `doc_note`, `int_note`, `create_by`, `create_dt`, `update_by`, `update_dt`, `cancel_by`, `cancel_dt`, `cancel_reason`) VALUES
(1, 'PIV-0001', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'PAID', 'UNPAID', 2, NULL, NULL, 1, 'ITEM', 'RCV-0011', NULL, 'IDR', '1.00', NULL, '0.00', '0.00', NULL, NULL, NULL, '25500000.00', 'Invoiced 100%', NULL, 'administrator', '2015-04-23 17:04:05', NULL, NULL, NULL, NULL, NULL),
(2, 'PIV-0002', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'APPROVED', 'UNPAID', 1, NULL, NULL, 1, 'ITEM', '', NULL, 'IDR', '1.00', NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, '', NULL, 'administrator', '2015-04-23 17:04:33', NULL, NULL, NULL, NULL, NULL),
(3, 'PIV-0003', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'APPROVED', 'UNPAID', 54, NULL, NULL, 1, 'ITEM', 'RCV-0012', NULL, 'IDR', '1.00', NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, 'Invoice by vendor XYZ', NULL, 'administrator', '2015-04-23 17:04:18', NULL, NULL, NULL, NULL, NULL),
(4, 'PIV-0004', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'PAID', 'UNPAID', 54, NULL, NULL, 1, 'ITEM', 'RCV-0013', NULL, 'IDR', '1.00', NULL, '0.00', '0.00', NULL, NULL, NULL, '500000.00', 'receive 2', NULL, 'administrator', '2015-04-23 17:04:46', NULL, NULL, NULL, NULL, NULL),
(5, 'PIV-0005', '2015-04-29 00:00:00', '2015-04-29 00:00:00', 'APPROVED', 'UNPAID', 1, NULL, NULL, 1, 'ITEM', 'RCV-0019', NULL, 'IDR', '1.00', NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, '', NULL, 'staff', '2015-04-29 14:04:39', NULL, NULL, NULL, NULL, NULL),
(6, 'PIV-0006', '2015-05-25 00:00:00', '2015-05-25 00:00:00', 'PAID', 'UNPAID', 3, NULL, NULL, 1, 'ITEM', 'RCV-0020', '', 'IDR', '1.00', '935000.00', '0.00', '0.00', NULL, NULL, '935000.00', '935000.00', '', NULL, 'staff', '2015-05-25 02:05:17', NULL, NULL, NULL, NULL, NULL),
(7, 'PIV-0007', '2015-05-25 00:00:00', '2015-05-25 00:00:00', 'APPROVED', 'UNPAID', 3, NULL, NULL, 1, 'ITEM', 'RCV-0020', '', 'IDR', '1.00', '935000.00', '0.00', '0.00', NULL, NULL, '935000.00', NULL, '', NULL, 'staff', '2015-05-25 02:05:51', NULL, NULL, NULL, NULL, NULL),
(8, 'PIV-0008', '2015-06-03 00:00:00', '2015-06-03 00:00:00', 'APPROVED', 'UNPAID', 1, NULL, NULL, 1, 'ITEM', 'RCV-0019', '', 'IDR', '1.00', '35000000.00', '0.00', '0.00', NULL, NULL, '35000000.00', NULL, '', NULL, 'manager', '2015-06-03 02:06:45', NULL, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_purchase_order_detail`;
CREATE TABLE IF NOT EXISTS `t_purchase_order_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `doc_line_type` varchar(5) DEFAULT NULL,
  `item_group` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(1000) NOT NULL,
  `item_qty` decimal(10,2) NOT NULL,
  `item_qty_closed` decimal(10,2) NOT NULL DEFAULT '0.00',
  `item_price` decimal(10,2) NOT NULL,
  `item_netprice` decimal(10,2) NOT NULL,
  `item_uom` varchar(25) DEFAULT NULL,
  `item_cost` decimal(10,2) DEFAULT NULL,
  `item_disc_amount` decimal(10,2) DEFAULT NULL,
  `item_disc_pct1` decimal(10,2) DEFAULT NULL,
  `item_disc_pct2` decimal(10,2) DEFAULT NULL,
  `item_tax` varchar(50) DEFAULT NULL,
  `item_total` decimal(10,2) DEFAULT NULL,
  `line_close` tinyint(4) NOT NULL DEFAULT '0',
  `reff_type` varchar(25) DEFAULT NULL,
  `reff_id` int(11) DEFAULT NULL,
  `reff_line` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_purchase_order_detail` (`doc_id`, `doc_line`, `doc_line_type`, `item_group`, `item_id`, `item_name`, `item_qty`, `item_qty_closed`, `item_price`, `item_netprice`, `item_uom`, `item_cost`, `item_disc_amount`, `item_disc_pct1`, `item_disc_pct2`, `item_tax`, `item_total`, `line_close`, `reff_type`, `reff_id`, `reff_line`) VALUES
(1, 1, 'ITEM', 2, 1, 'Casing A', '2.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '1000000.00', 0, NULL, NULL, NULL),
(1, 2, 'ITEM', 2, 2, 'Mobo XYZ', '2.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '3000000.00', 0, NULL, NULL, NULL),
(1, 3, 'ITEM', 2, 3, 'Mobo MP', '3.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '4500000.00', 0, NULL, NULL, NULL),
(1, 4, 'ITEM', 2, 4, 'LG 14 inch', '3.00', '0.00', '2000000.00', '2000000.00', NULL, '2000000.00', NULL, NULL, NULL, NULL, '6000000.00', 0, NULL, NULL, NULL),
(1, 5, 'ITEM', 2, 5, 'SAMSUNG 15.4', '4.00', '0.00', '2000000.00', '2000000.00', NULL, '2000000.00', NULL, NULL, NULL, NULL, '8000000.00', 0, NULL, NULL, NULL),
(1, 6, 'ITEM', 2, 6, 'Simbadda', '6.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '3000000.00', 0, NULL, NULL, NULL),
(3, 1, 'ITEM', 2, 1, 'Casing A', '150.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '75000000.00', 0, NULL, NULL, NULL),
(3, 2, 'ITEM', 2, 2, 'Mobo XYZ', '150.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '99999999.99', 0, NULL, NULL, NULL),
(3, 3, 'ITEM', 2, 3, 'Mobo MP', '150.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '99999999.99', 0, NULL, NULL, NULL),
(4, 1, 'ITEM', 2, 1, 'Casing A', '1.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '500000.00', 0, NULL, NULL, NULL),
(5, 1, 'ITEM', 2, 1, 'Casing A', '10.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '5000000.00', 0, NULL, NULL, NULL),
(5, 2, 'ITEM', 2, 2, 'Mobo XYZ', '10.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '15000000.00', 0, NULL, NULL, NULL),
(5, 3, 'ITEM', 2, 3, 'Mobo MP', '10.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '15000000.00', 0, NULL, NULL, NULL),
(6, 1, 'ITEM', 2, 1, 'Casing A', '100.00', '50.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '50000000.00', 0, NULL, NULL, NULL),
(6, 2, 'ITEM', 2, 2, 'Mobo XYZ', '100.00', '50.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '99999999.99', 0, NULL, NULL, NULL),
(6, 3, 'ITEM', 2, 3, 'Mobo MP', '120.00', '120.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '99999999.99', 1, NULL, NULL, NULL),
(7, 1, 'ITEM', 2, 3, 'Mobo MP', '20.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '30000000.00', 0, NULL, NULL, NULL),
(7, 2, 'ITEM', 2, 4, 'LG 14 inch', '20.00', '0.00', '2000000.00', '2000000.00', NULL, '2000000.00', NULL, NULL, NULL, NULL, '40000000.00', 0, NULL, NULL, NULL),
(8, 1, 'ITEM', 2, 6, 'Simbadda', '200.00', '200.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '99999999.99', 1, NULL, NULL, NULL),
(8, 2, 'ITEM', 2, 7, 'Corsair 4GB', '200.00', '200.00', '1400000.00', '1400000.00', NULL, '1400000.00', NULL, NULL, NULL, NULL, '99999999.99', 1, NULL, NULL, NULL),
(8, 3, 'ITEM', 2, 8, 'WD HDD 2.5, 500 GB', '200.00', '200.00', '700000.00', '700000.00', NULL, '700000.00', NULL, NULL, NULL, NULL, '99999999.99', 1, NULL, NULL, NULL),
(9, 1, 'ITEM', 2, 1, 'Casing A', '15.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '7500000.00', 0, NULL, NULL, NULL),
(9, 2, 'ITEM', 2, 2, 'Mobo XYZ', '15.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '22500000.00', 0, NULL, NULL, NULL),
(9, 3, 'ITEM', 2, 3, 'Mobo MP', '15.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '22500000.00', 0, NULL, NULL, NULL),
(10, 1, 'ITEM', 2, 1, 'Casing A', '20.00', '20.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '10000000.00', 1, NULL, NULL, NULL),
(10, 2, 'ITEM', 2, 2, 'Mobo XYZ', '25.00', '25.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '37500000.00', 1, NULL, NULL, NULL),
(10, 3, 'ITEM', 2, 3, 'Mobo MP', '30.00', '30.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '45000000.00', 1, NULL, NULL, NULL),
(11, 1, 'ITEM', 2, 1, 'Casing A', '100.00', '100.00', '275000.00', '275000.00', NULL, '275000.00', NULL, NULL, NULL, NULL, '27500000.00', 1, NULL, NULL, NULL),
(12, 1, 'ITEM', 2, 1, 'Casing A', '100.00', '100.00', '275000.00', '275000.00', NULL, '275000.00', NULL, NULL, NULL, NULL, '27500000.00', 1, NULL, NULL, NULL),
(13, 1, 'ITEM', 2, 1, 'Casing A', '3.00', '3.00', '78500.00', '78500.00', NULL, '78500.00', NULL, NULL, NULL, NULL, '235500.00', 1, NULL, NULL, NULL),
(13, 2, 'ITEM', 2, 2, 'Mobo XYZ', '2.00', '2.00', '89000.00', '89000.00', NULL, '89000.00', NULL, NULL, NULL, NULL, '178000.00', 1, NULL, NULL, NULL),
(13, 3, 'ITEM', 2, 3, 'Mobo MP', '10.00', '10.00', '87500.00', '87500.00', NULL, '87500.00', NULL, NULL, NULL, NULL, '875000.00', 1, NULL, NULL, NULL),
(14, 1, 'ITEM', 2, 1, 'Casing A', '5.00', '0.00', '199000.00', '199000.00', NULL, '199000.00', NULL, NULL, NULL, NULL, '995000.00', 0, NULL, NULL, NULL),
(14, 2, 'ITEM', 2, 2, 'Mobo XYZ', '5.00', '0.00', '250000.00', '250000.00', NULL, '250000.00', NULL, NULL, NULL, NULL, '1250000.00', 0, NULL, NULL, NULL),
(15, 1, 'ITEM', 2, 2, 'Mobo XYZ', '2.00', '0.00', '185000.00', '185000.00', NULL, '185000.00', NULL, NULL, NULL, NULL, '370000.00', 0, NULL, NULL, NULL),
(15, 2, 'ITEM', 2, 3, 'Mobo MP', '3.00', '0.00', '658000.00', '658000.00', NULL, '658000.00', NULL, NULL, NULL, NULL, '1974000.00', 0, NULL, NULL, NULL),
(16, 1, 'ITEM', 2, 1, 'Casing A', '10.00', '10.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '5000000.00', 1, NULL, NULL, NULL),
(16, 2, 'ITEM', 2, 2, 'Mobo XYZ', '10.00', '10.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '15000000.00', 1, NULL, NULL, NULL),
(16, 3, 'ITEM', 2, 3, 'Mobo MP', '10.00', '10.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '15000000.00', 1, NULL, NULL, NULL),
(17, 1, 'ITEM', 2, 1, 'Casing A', '1.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '500000.00', 0, NULL, NULL, NULL),
(17, 2, 'ITEM', 2, 2, 'Mobo XYZ', '1.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '1500000.00', 0, NULL, NULL, NULL),
(18, 1, 'ITEM', 2, 1, 'Casing A', '1.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '500000.00', 0, NULL, NULL, NULL),
(18, 2, 'ITEM', 2, 2, 'Mobo XYZ', '1.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '1500000.00', 0, NULL, NULL, NULL),
(19, 1, 'ITEM', 2, 1, 'Casing A', '1.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '500000.00', 0, NULL, NULL, NULL),
(19, 2, 'ITEM', 2, 2, 'Mobo XYZ', '1.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '1500000.00', 0, NULL, NULL, NULL),
(20, 1, 'ITEM', 2, 1, 'Casing A', '1.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '500000.00', 0, NULL, NULL, NULL),
(20, 2, 'ITEM', 2, 2, 'Mobo XYZ', '1.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '1500000.00', 0, NULL, NULL, NULL),
(21, 1, 'ITEM', 2, 1, 'Casing A', '12.00', '0.00', '500000.00', '0.00', NULL, '500000.00', NULL, '0.00', NULL, NULL, '0.00', 0, NULL, NULL, NULL),
(21, 2, 'ITEM', 2, 2, 'Mobo XYZ', '12.00', '0.00', '1500000.00', '0.00', NULL, '1500000.00', NULL, '0.00', NULL, NULL, '0.00', 0, NULL, NULL, NULL),
(22, 1, 'ITEM', 2, 1, 'Casing A', '2.00', '0.00', '500000.00', '550000.00', NULL, '500000.00', NULL, '0.00', NULL, '10.00', '1100000.00', 0, NULL, NULL, NULL),
(22, 2, 'ITEM', 2, 2, 'Mobo XYZ', '2.00', '0.00', '1500000.00', '1650000.00', NULL, '1500000.00', NULL, '0.00', NULL, '10.00', '3300000.00', 0, NULL, NULL, NULL),
(23, 1, 'ITEM', 2, 1, 'Casing A', '2.00', '0.00', '500000.00', '550000.00', NULL, '500000.00', NULL, '0.00', NULL, '10.00', '1100000.00', 0, NULL, NULL, NULL),
(23, 2, 'ITEM', 2, 2, 'Mobo XYZ', '3.00', '0.00', '1500000.00', '1650000.00', NULL, '1500000.00', NULL, '0.00', NULL, '10.00', '4950000.00', 0, NULL, NULL, NULL),
(23, 3, 'ITEM', 2, 3, 'Mobo MP', '5.00', '0.00', '1500000.00', '1650000.00', NULL, '1500000.00', NULL, '0.00', NULL, '10.00', '8250000.00', 0, NULL, NULL, NULL),
(24, 1, 'ITEM', 2, 1, 'Casing A', '1.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, '0.00', NULL, '0.00', '500000.00', 0, NULL, NULL, NULL),
(26, 1, 'ITEM', 2, 2, 'T Shirt B', '100.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '8500000.00', 0, NULL, NULL, NULL),
(26, 2, 'ITEM', 2, 3, 'T Shirt C', '100.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '8500000.00', 0, NULL, NULL, NULL),
(27, 1, 'ITEM', 2, 1, 'T Shirt A', '5.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '425000.00', 0, NULL, NULL, NULL),
(27, 2, 'ITEM', 2, 2, 'T Shirt B', '5.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '425000.00', 0, NULL, NULL, NULL),
(28, 1, 'ITEM', 2, 1, 'T Shirt A', '1.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '85000.00', 0, NULL, NULL, NULL),
(29, 1, 'ITEM', 2, 1, 'T Shirt A', '2.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '170000.00', 0, NULL, NULL, NULL),
(29, 2, 'ITEM', 2, 2, 'T Shirt B', '2.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '170000.00', 0, NULL, NULL, NULL),
(30, 1, 'ITEM', 2, 1, 'T Shirt A', '5.00', '5.00', '85000.00', '93500.00', NULL, '85000.00', NULL, '0.00', NULL, '10.00', '467500.00', 1, NULL, NULL, NULL),
(30, 2, 'ITEM', 2, 2, 'T Shirt B', '5.00', '5.00', '85000.00', '93500.00', NULL, '85000.00', NULL, '0.00', NULL, '10.00', '467500.00', 1, NULL, NULL, NULL),
(31, 1, 'ITEM', 2, 1, 'T Shirt A', '1.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '85000.00', 0, NULL, NULL, NULL),
(31, 2, 'ITEM', 2, 2, 'T Shirt B', '1.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '85000.00', 0, NULL, NULL, NULL),
(32, 1, 'ITEM', 2, 4, 'T Shirt D', '1.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '85000.00', 0, NULL, NULL, NULL),
(32, 2, 'ITEM', 2, 5, 'Jeans A', '1.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '85000.00', 0, NULL, NULL, NULL),
(33, 1, 'ITEM', 4, 18, 'Sepatu A', '150.00', '150.00', '85000.00', '93500.00', NULL, '85000.00', NULL, '0.00', NULL, '10.00', '14025000.00', 1, NULL, NULL, NULL),
(33, 2, 'ITEM', 4, 19, 'Sepatu B', '150.00', '150.00', '85000.00', '93500.00', NULL, '85000.00', NULL, '0.00', NULL, '10.00', '14025000.00', 1, NULL, NULL, NULL),
(34, 1, 'ITEM', 2, 1, 'T Shirt A', '1.00', '1.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '85000.00', 1, NULL, NULL, NULL),
(34, 2, 'ITEM', 2, 2, 'T Shirt B', '1.00', '1.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '85000.00', 1, NULL, NULL, NULL),
(35, 1, 'ITEM', 2, 1, 'T Shirt A', '1.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '85000.00', 0, NULL, NULL, NULL),
(35, 2, 'ITEM', 2, 2, 'T Shirt B', '1.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '85000.00', 0, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_purchase_order_header`;
CREATE TABLE IF NOT EXISTS `t_purchase_order_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime NOT NULL,
  `doc_ddt` datetime NOT NULL,
  `doc_status` varchar(25) NOT NULL DEFAULT 'ACTIVE',
  `logistic_status` varchar(25) NOT NULL DEFAULT 'PENDING',
  `billing_status` varchar(25) NOT NULL DEFAULT 'UNBILLED',
  `financial_status` varchar(25) NOT NULL DEFAULT 'UNPAID',
  `approval_status` enum('PENDING','PARTIALLY APPROVED','APPROVED','REVISE','REJECTED') NOT NULL DEFAULT 'PENDING',
  `buss_id` int(11) NOT NULL,
  `ship_addr` varchar(1000) DEFAULT NULL,
  `bill_addr` varchar(1000) DEFAULT NULL,
  `whse_id` int(11) NOT NULL,
  `doc_type` varchar(10) DEFAULT NULL,
  `doc_ref` varchar(50) DEFAULT NULL,
  `doc_ext_ref` varchar(50) DEFAULT NULL,
  `doc_curr` varchar(5) DEFAULT NULL,
  `doc_rate` decimal(10,2) DEFAULT NULL,
  `doc_subtotal` decimal(10,2) DEFAULT NULL,
  `doc_disc_percent` decimal(10,2) DEFAULT NULL,
  `doc_disc_amount` decimal(10,2) DEFAULT NULL,
  `doc_rounding` decimal(10,2) DEFAULT NULL,
  `doc_tax` decimal(10,2) DEFAULT NULL,
  `doc_total` decimal(10,2) DEFAULT NULL,
  `doc_paid` decimal(10,2) DEFAULT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `int_note` varchar(1000) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` datetime DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

INSERT INTO `t_purchase_order_header` (`doc_id`, `doc_num`, `doc_dt`, `doc_ddt`, `doc_status`, `logistic_status`, `billing_status`, `financial_status`, `approval_status`, `buss_id`, `ship_addr`, `bill_addr`, `whse_id`, `doc_type`, `doc_ref`, `doc_ext_ref`, `doc_curr`, `doc_rate`, `doc_subtotal`, `doc_disc_percent`, `doc_disc_amount`, `doc_rounding`, `doc_tax`, `doc_total`, `doc_paid`, `doc_note`, `int_note`, `create_by`, `create_dt`, `update_by`, `update_dt`, `cancel_by`, `cancel_dt`, `cancel_reason`, `post_id`) VALUES
(1, 'PO-0001', '2015-04-16 00:00:00', '2015-04-16 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 1, 'Jl. Raya Meruya Ilir No.88, Jakarta Barat, DKI Jakarta, Indonesia', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '25500000.00', '0.00', '0.00', NULL, NULL, '25500000.00', NULL, '', NULL, NULL, '2015-04-16 17:04:00', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'PO-0002', '2015-04-21 00:00:00', '2015-04-21 00:00:00', 'CANCELLED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 51, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '0.00', '0.00', '0.00', NULL, NULL, '0.00', NULL, '', NULL, NULL, '2015-04-21 14:04:00', NULL, NULL, NULL, '2015-04-21 14:04:13', '1', NULL),
(3, 'PO-0003', '2015-04-22 00:00:00', '2015-04-25 00:00:00', 'DRAFT', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 2, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '99999999.99', '0.00', '0.00', NULL, NULL, '99999999.99', NULL, 'Re stock', NULL, 'administrator', '2015-04-22 15:04:48', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'PO-0004', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '500000.00', '0.00', '0.00', NULL, NULL, '500000.00', NULL, '', NULL, 'administrator', '2015-04-23 15:04:35', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'PO-0005', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'DRAFT', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 47, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '35000000.00', '0.00', '0.00', NULL, NULL, '35000000.00', NULL, '', NULL, 'administrator', '2015-04-23 17:04:42', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'PO-0006', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'ACTIVE', 'PARTIAL', 'UNBILLED', 'UNPAID', 'APPROVED', 48, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '99999999.99', '0.00', '0.00', NULL, NULL, '99999999.99', NULL, 'Purchase of components', NULL, 'administrator', '2015-04-23 17:04:57', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'PO-0007', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 2, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '70000000.00', '0.00', '0.00', NULL, NULL, '70000000.00', NULL, 'Purchase for today', NULL, 'administrator', '2015-04-23 17:04:26', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'PO-0008', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'CLOSED', 'RECEIVED', 'UNBILLED', 'UNPAID', 'APPROVED', 2, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '99999999.99', '0.00', '0.00', NULL, NULL, '99999999.99', NULL, '', NULL, 'administrator', '2015-04-23 17:04:35', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'PO-0009', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 2, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '52500000.00', '0.00', '0.00', NULL, NULL, '52500000.00', NULL, '', NULL, 'administrator', '2015-04-23 17:04:25', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'PO-0010', '2015-04-23 00:00:00', '2015-04-25 00:00:00', 'CLOSED', 'RECEIVED', 'UNBILLED', 'UNPAID', 'APPROVED', 54, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '92500000.00', '0.00', '0.00', NULL, NULL, '92500000.00', NULL, '', NULL, 'administrator', '2015-04-23 17:04:50', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'PO-0011', '2015-04-27 00:00:00', '2015-04-27 00:00:00', 'CLOSED', 'RECEIVED', 'UNBILLED', 'UNPAID', 'APPROVED', 3, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '27500000.00', '0.00', '0.00', NULL, NULL, '27500000.00', NULL, '', NULL, 'administrator', '2015-04-27 17:04:31', NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'PO-0012', '2015-04-27 00:00:00', '2015-04-27 00:00:00', 'CLOSED', 'RECEIVED', 'UNBILLED', 'UNPAID', 'APPROVED', 2, '', NULL, 2, 'ITEM', NULL, NULL, 'IDR', '1.00', '27500000.00', '0.00', '0.00', NULL, NULL, '27500000.00', NULL, '', NULL, 'administrator', '2015-04-27 18:04:48', NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'PO-0013', '2015-04-28 00:00:00', '2015-04-28 00:00:00', 'CLOSED', 'RECEIVED', 'UNBILLED', 'UNPAID', 'APPROVED', 55, '', NULL, 2, 'ITEM', NULL, NULL, 'IDR', '1.00', '1288500.00', '0.00', '0.00', NULL, NULL, '1288500.00', NULL, '', NULL, 'administrator', '2015-04-28 01:04:37', NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'PO-0014', '2015-04-29 00:00:00', '2015-04-29 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '2245000.00', '0.00', '0.00', NULL, NULL, '2245000.00', NULL, '', NULL, 'staff', '2015-04-29 12:04:54', NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'PO-0015', '2015-04-29 00:00:00', '2015-04-29 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 2, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '2344000.00', '0.00', '0.00', NULL, NULL, '2344000.00', NULL, '', NULL, 'staff', '2015-04-29 12:04:32', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'PO-0016', '2015-04-29 00:00:00', '2015-04-29 00:00:00', 'CLOSED', 'RECEIVED', 'UNBILLED', 'UNPAID', 'APPROVED', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '35000000.00', '0.00', '0.00', NULL, NULL, '35000000.00', NULL, '', NULL, 'staff', '2015-04-29 14:04:49', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'PO-0017', '2015-05-05 00:00:00', '2015-05-05 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'PENDING', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '2000000.00', '0.00', '0.00', NULL, NULL, '2000000.00', NULL, '', NULL, 'staff', '2015-05-05 12:05:30', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'PO-0018', '2015-05-05 00:00:00', '2015-05-05 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'PENDING', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '2000000.00', '0.00', '0.00', NULL, NULL, '2000000.00', NULL, '', NULL, 'staff', '2015-05-05 12:05:06', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'PO-0019', '2015-05-05 00:00:00', '2015-05-05 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'PENDING', 2, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '2000000.00', '0.00', '0.00', NULL, NULL, '2000000.00', NULL, '', NULL, 'staff', '2015-05-05 12:05:43', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'PO-0020', '2015-05-05 00:00:00', '2015-05-05 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 1, '', NULL, 2, 'ITEM', NULL, NULL, 'IDR', '1.00', '2000000.00', '0.00', '0.00', NULL, NULL, '2000000.00', NULL, '', NULL, 'staff', '2015-05-05 12:05:19', NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'PO-0021', '2015-05-07 00:00:00', '2015-05-07 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '0.00', '0.00', '0.00', NULL, NULL, '0.00', NULL, 'test', NULL, 'staff', '2015-05-07 10:05:38', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'PO-0022', '2015-05-07 00:00:00', '2015-05-07 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '4400000.00', '5.00', '220000.00', NULL, NULL, '4180000.00', NULL, '', NULL, 'staff', '2015-05-07 10:05:31', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'PO-0023', '2015-05-07 00:00:00', '2015-05-10 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 3, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '14300000.00', '10.00', '1430000.00', NULL, NULL, '12870000.00', NULL, 'Test PO', NULL, 'staff', '2015-05-07 10:05:12', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'PO-0024', '2015-05-08 00:00:00', '2015-05-08 00:00:00', 'APPROVED', 'PENDING', 'UNBILLED', 'UNPAID', 'PARTIALLY APPROVED', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '500000.00', '0.00', '0.00', NULL, NULL, '500000.00', NULL, '', NULL, 'staff', '2015-05-08 15:05:03', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'PO-0025', '2015-05-13 00:00:00', '2015-05-13 00:00:00', 'PENDING', 'PENDING', 'UNBILLED', 'UNPAID', 'PENDING', 56, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '0.00', '0.00', '0.00', NULL, NULL, '0.00', NULL, '', NULL, 'administrator', '2015-05-13 15:05:52', NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'PO-0026', '2015-05-18 00:00:00', '2015-05-18 00:00:00', 'ACTIVE', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 2, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '17000000.00', '0.00', '0.00', NULL, NULL, '17000000.00', NULL, '', NULL, 'staff', '2015-05-18 16:05:57', NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'PO-0027', '2015-05-18 00:00:00', '2015-05-18 00:00:00', 'DRAFT', 'PENDING', 'UNBILLED', 'UNPAID', 'PENDING', 3, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '850000.00', '0.00', '0.00', NULL, NULL, '850000.00', NULL, '', NULL, 'staff', '2015-05-18 16:05:23', NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'PO-0028', '2015-05-21 00:00:00', '2015-05-21 00:00:00', 'ACTIVE', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '85000.00', '0.00', '0.00', NULL, NULL, '85000.00', NULL, '', NULL, 'manager', '2015-05-21 11:05:23', 'manager', '2015-05-21 11:05:31', 'administrator', '2015-05-27 15:05:32', '123', NULL),
(29, 'PO-0029', '2015-05-25 00:00:00', '2015-05-25 00:00:00', 'DRAFT', 'PENDING', 'UNBILLED', 'UNPAID', 'PENDING', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '340000.00', '0.00', '0.00', NULL, NULL, '340000.00', NULL, '', NULL, 'staff', '2015-05-25 02:05:13', NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'PO-0030', '2015-05-25 00:00:00', '2015-05-25 00:00:00', 'CLOSED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 3, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '935000.00', '0.00', '0.00', NULL, NULL, '935000.00', NULL, '', NULL, 'staff', '2015-05-25 02:05:34', NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'PO-0031', '2015-05-25 00:00:00', '2015-05-25 00:00:00', 'DRAFT', 'PENDING', 'UNBILLED', 'UNPAID', 'PENDING', 1, '', NULL, 2, 'ITEM', NULL, NULL, 'IDR', '1.00', '170000.00', '0.00', '0.00', NULL, NULL, '170000.00', NULL, '', NULL, 'staff', '2015-05-25 02:05:41', NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'PO-0032', '2015-05-25 00:00:00', '2015-05-25 00:00:00', 'ACTIVE', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '170000.00', '0.00', '0.00', NULL, NULL, '170000.00', NULL, '', NULL, 'staff', '2015-05-25 02:05:54', NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'PO-0033', '2015-06-04 00:00:00', '2015-06-04 00:00:00', 'CLOSED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 3, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '28050000.00', '0.00', '0.00', NULL, NULL, '28050000.00', NULL, '', NULL, 'staff', '2015-06-04 20:06:32', NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'PO-0034', '2015-06-18 00:00:00', '2015-06-18 00:00:00', 'CLOSED', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '170000.00', '0.00', '0.00', NULL, NULL, '170000.00', NULL, '', NULL, 'manager', '2015-06-18 21:06:09', NULL, NULL, NULL, NULL, NULL, NULL),
(35, 'PO-0035', '2015-06-19 00:00:00', '2015-06-19 00:00:00', 'ACTIVE', 'PENDING', 'UNBILLED', 'UNPAID', 'APPROVED', 1, '', NULL, 1, 'ITEM', NULL, NULL, 'IDR', '1.00', '170000.00', '0.00', '0.00', NULL, NULL, '170000.00', NULL, '', NULL, 'manager', '2015-06-19 03:06:04', NULL, NULL, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_purchase_quotation_detail`;
CREATE TABLE IF NOT EXISTS `t_purchase_quotation_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `doc_line_type` varchar(5) DEFAULT NULL,
  `item_group` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(1000) NOT NULL,
  `item_qty` decimal(18,2) NOT NULL,
  `item_qty_closed` decimal(18,2) NOT NULL DEFAULT '0.00',
  `item_price` decimal(18,2) NOT NULL,
  `item_netprice` decimal(18,2) NOT NULL,
  `item_uom` varchar(25) DEFAULT NULL,
  `item_cost` decimal(18,2) DEFAULT NULL,
  `item_disc_amount` decimal(18,2) DEFAULT NULL,
  `item_disc_pct1` decimal(18,2) DEFAULT NULL,
  `item_disc_pct2` decimal(18,2) DEFAULT NULL,
  `item_tax` varchar(50) DEFAULT NULL,
  `item_total` decimal(18,2) DEFAULT NULL,
  `line_close` tinyint(4) NOT NULL DEFAULT '0',
  `reff_type` varchar(25) DEFAULT NULL,
  `reff_id` int(11) DEFAULT NULL,
  `reff_line` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_purchase_quotation_header`;
CREATE TABLE IF NOT EXISTS `t_purchase_quotation_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime NOT NULL,
  `doc_ddt` datetime NOT NULL,
  `doc_status` varchar(25) NOT NULL DEFAULT 'ACTIVE',
  `approval_status` enum('PENDING','PARTIALLY APPROVED','APPROVED','REVISE','REJECTED') NOT NULL DEFAULT 'PENDING',
  `buss_id` int(11) NOT NULL,
  `ship_addr` varchar(1000) DEFAULT NULL,
  `bill_addr` varchar(1000) DEFAULT NULL,
  `whse_id` int(11) NOT NULL,
  `doc_type` varchar(10) DEFAULT NULL,
  `doc_ref` varchar(50) DEFAULT NULL,
  `doc_ext_ref` varchar(50) DEFAULT NULL,
  `doc_curr` varchar(5) DEFAULT NULL,
  `doc_rate` decimal(10,2) DEFAULT NULL,
  `doc_subtotal` decimal(18,2) DEFAULT NULL,
  `doc_disc_percent` decimal(18,2) DEFAULT NULL,
  `doc_disc_amount` decimal(18,2) DEFAULT NULL,
  `doc_rounding` decimal(18,2) DEFAULT NULL,
  `doc_tax` decimal(18,2) DEFAULT NULL,
  `doc_total` decimal(18,2) DEFAULT NULL,
  `doc_paid` decimal(18,2) DEFAULT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `int_note` varchar(1000) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` datetime DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_purchase_receipt_detail`;
CREATE TABLE IF NOT EXISTS `t_purchase_receipt_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `doc_line_type` varchar(5) DEFAULT NULL,
  `item_group` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(1000) NOT NULL,
  `item_qty` decimal(10,2) NOT NULL,
  `item_qty_closed` decimal(10,2) NOT NULL DEFAULT '0.00',
  `item_price` decimal(10,2) NOT NULL,
  `item_netprice` decimal(10,2) NOT NULL,
  `item_uom` varchar(25) DEFAULT NULL,
  `item_cost` decimal(10,2) DEFAULT NULL,
  `item_disc_amount` decimal(10,2) DEFAULT NULL,
  `item_disc_pct1` decimal(10,2) DEFAULT NULL,
  `item_disc_pct2` decimal(10,2) DEFAULT NULL,
  `item_tax` varchar(50) DEFAULT NULL,
  `item_total` decimal(10,2) DEFAULT NULL,
  `line_close` tinyint(4) NOT NULL DEFAULT '0',
  `reff_type` varchar(25) DEFAULT NULL,
  `reff_id` int(11) DEFAULT NULL,
  `reff_line` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_purchase_receipt_detail` (`doc_id`, `doc_line`, `doc_line_type`, `item_group`, `item_id`, `item_name`, `item_qty`, `item_qty_closed`, `item_price`, `item_netprice`, `item_uom`, `item_cost`, `item_disc_amount`, `item_disc_pct1`, `item_disc_pct2`, `item_tax`, `item_total`, `line_close`, `reff_type`, `reff_id`, `reff_line`) VALUES
(3, 1, 'ITEM', 2, 1, 'Casing A', '50.00', '0.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '25000000.00', 0, 'PO', 6, 1),
(3, 2, 'ITEM', 2, 2, 'Mobo XYZ', '50.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '75000000.00', 0, 'PO', 6, 2),
(3, 3, 'ITEM', 2, 3, 'Mobo MP', '120.00', '0.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '99999999.99', 0, 'PO', 6, 3),
(4, 1, 'ITEM', 2, 6, 'Simbadda', '200.00', '6.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '99999999.99', 0, 'PO', 8, 1),
(4, 2, 'ITEM', 2, 7, 'Corsair 4GB', '200.00', '7.00', '1400000.00', '1400000.00', NULL, '1400000.00', NULL, NULL, NULL, NULL, '99999999.99', 0, 'PO', 8, 2),
(4, 3, 'ITEM', 2, 8, 'WD HDD 2.5, 500 GB', '200.00', '8.00', '700000.00', '700000.00', NULL, '700000.00', NULL, NULL, NULL, NULL, '99999999.99', 0, 'PO', 8, 3),
(5, 1, 'ITEM', 2, 1, 'Casing A', '10.00', '1.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '5000000.00', 0, 'PO', 10, 1),
(5, 2, 'ITEM', 2, 2, 'Mobo XYZ', '15.00', '2.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '22500000.00', 0, 'PO', 10, 2),
(5, 3, 'ITEM', 2, 3, 'Mobo MP', '15.00', '3.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '22500000.00', 0, 'PO', 10, 3),
(6, 1, 'ITEM', 2, 1, 'Casing A', '10.00', '1.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '5000000.00', 0, 'PO', 10, 1),
(6, 2, 'ITEM', 2, 2, 'Mobo XYZ', '10.00', '2.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '15000000.00', 0, 'PO', 10, 2),
(6, 3, 'ITEM', 2, 3, 'Mobo MP', '15.00', '3.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '22500000.00', 0, 'PO', 10, 3),
(7, 1, 'ITEM', 2, 1, 'Casing A', '100.00', '0.00', '275000.00', '275000.00', NULL, '275000.00', NULL, NULL, NULL, NULL, '27500000.00', 0, 'PO', 11, 1),
(8, 1, 'ITEM', 2, 1, 'Casing A', '100.00', '0.00', '275000.00', '275000.00', NULL, '275000.00', NULL, NULL, NULL, NULL, '27500000.00', 0, 'PO', 12, 1),
(9, 1, 'ITEM', 2, 1, 'Casing A', '3.00', '0.00', '78500.00', '78500.00', NULL, '78500.00', NULL, NULL, NULL, NULL, '235500.00', 0, 'PO', 13, 1),
(9, 2, 'ITEM', 2, 2, 'Mobo XYZ', '2.00', '0.00', '89000.00', '89000.00', NULL, '89000.00', NULL, NULL, NULL, NULL, '178000.00', 0, 'PO', 13, 2),
(9, 3, 'ITEM', 2, 3, 'Mobo MP', '10.00', '0.00', '87500.00', '87500.00', NULL, '87500.00', NULL, NULL, NULL, NULL, '875000.00', 0, 'PO', 13, 3),
(12, 1, 'ITEM', 2, 1, 'Casing A', '10.00', '1.00', '500000.00', '500000.00', NULL, '500000.00', NULL, NULL, NULL, NULL, '5000000.00', 0, 'PO', 16, 1),
(12, 2, 'ITEM', 2, 2, 'Mobo XYZ', '10.00', '2.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '15000000.00', 0, 'PO', 16, 2),
(12, 3, 'ITEM', 2, 3, 'Mobo MP', '10.00', '3.00', '1500000.00', '1500000.00', NULL, '1500000.00', NULL, NULL, NULL, NULL, '15000000.00', 0, 'PO', 16, 3),
(13, 1, 'ITEM', 2, 1, 'T Shirt A', '5.00', '1.00', '93500.00', '93500.00', NULL, '93500.00', NULL, NULL, NULL, NULL, '467500.00', 0, 'PO', 30, 1),
(13, 2, 'ITEM', 2, 2, 'T Shirt B', '5.00', '2.00', '93500.00', '93500.00', NULL, '93500.00', NULL, NULL, NULL, NULL, '467500.00', 0, 'PO', 30, 2),
(14, 1, 'ITEM', 4, 18, 'Sepatu A', '150.00', '0.00', '93500.00', '93500.00', NULL, '93500.00', NULL, NULL, NULL, NULL, '14025000.00', 0, 'PO', 33, 1),
(14, 2, 'ITEM', 4, 19, 'Sepatu B', '150.00', '0.00', '93500.00', '93500.00', NULL, '93500.00', NULL, NULL, NULL, NULL, '14025000.00', 0, 'PO', 33, 2),
(15, 1, 'ITEM', 2, 1, 'T Shirt A', '1.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '85000.00', 0, 'PO', 34, 1),
(15, 2, 'ITEM', 2, 2, 'T Shirt B', '1.00', '0.00', '85000.00', '85000.00', NULL, '85000.00', NULL, '0.00', NULL, '0.00', '85000.00', 0, 'PO', 34, 2);

DROP TABLE IF EXISTS `t_purchase_receipt_header`;
CREATE TABLE IF NOT EXISTS `t_purchase_receipt_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime NOT NULL,
  `doc_ddt` datetime NOT NULL,
  `doc_status` varchar(25) NOT NULL DEFAULT 'ACTIVE',
  `billing_status` varchar(25) NOT NULL DEFAULT 'UNBILLED',
  `financial_status` varchar(25) NOT NULL DEFAULT 'UNPAID',
  `buss_id` int(11) NOT NULL,
  `ship_addr` varchar(1000) DEFAULT NULL,
  `bill_addr` varchar(1000) DEFAULT NULL,
  `whse_id` int(11) NOT NULL,
  `doc_type` varchar(10) DEFAULT NULL,
  `doc_ref` varchar(50) DEFAULT NULL,
  `doc_ext_ref` varchar(50) DEFAULT NULL,
  `doc_curr` varchar(5) DEFAULT NULL,
  `doc_rate` decimal(10,2) DEFAULT NULL,
  `doc_subtotal` decimal(10,2) DEFAULT NULL,
  `doc_disc_percent` decimal(10,2) DEFAULT NULL,
  `doc_disc_amount` decimal(10,2) DEFAULT NULL,
  `doc_rounding` decimal(10,2) DEFAULT NULL,
  `doc_tax` decimal(10,2) DEFAULT NULL,
  `doc_total` decimal(10,2) DEFAULT NULL,
  `doc_paid` decimal(10,2) DEFAULT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `int_note` varchar(1000) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` datetime DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

INSERT INTO `t_purchase_receipt_header` (`doc_id`, `doc_num`, `doc_dt`, `doc_ddt`, `doc_status`, `billing_status`, `financial_status`, `buss_id`, `ship_addr`, `bill_addr`, `whse_id`, `doc_type`, `doc_ref`, `doc_ext_ref`, `doc_curr`, `doc_rate`, `doc_subtotal`, `doc_disc_percent`, `doc_disc_amount`, `doc_rounding`, `doc_tax`, `doc_total`, `doc_paid`, `doc_note`, `int_note`, `create_by`, `create_dt`, `update_by`, `update_dt`, `cancel_by`, `cancel_dt`, `cancel_reason`) VALUES
(3, 'RCV-0010', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'APPROVED', 'UNBILLED', 'UNPAID', 48, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', 'PO-0006', NULL, 'IDR', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Purchase of components', NULL, 'administrator', '2015-04-23 17:04:37', NULL, NULL, NULL, NULL, NULL),
(4, 'RCV-0011', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'ACTIVE', 'UNBILLED', 'UNPAID', 2, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', 'PO-0008', NULL, 'IDR', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'administrator', '2015-04-23 17:04:07', NULL, NULL, NULL, NULL, NULL),
(5, 'RCV-0012', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'ACTIVE', 'UNBILLED', 'UNPAID', 54, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', 'PO-0010', NULL, 'IDR', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'administrator', '2015-04-23 17:04:12', NULL, NULL, NULL, NULL, NULL),
(6, 'RCV-0013', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'ACTIVE', 'UNBILLED', 'UNPAID', 54, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', 'PO-0010', NULL, 'IDR', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'receive 2', NULL, 'administrator', '2015-04-23 17:04:43', NULL, NULL, NULL, NULL, NULL),
(7, 'RCV-0014', '2015-04-27 00:00:00', '2015-04-27 00:00:00', 'APPROVED', 'UNBILLED', 'UNPAID', 3, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', 'PO-0011', NULL, 'IDR', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'administrator', '2015-04-27 17:04:40', NULL, NULL, NULL, NULL, NULL),
(8, 'RCV-0015', '2015-04-27 00:00:00', '2015-04-27 00:00:00', 'APPROVED', 'UNBILLED', 'UNPAID', 2, 'Central Park Boulevard #01-20, Bandung, Bandung, Indonesia', NULL, 2, 'ITEM', 'PO-0012', NULL, 'IDR', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'administrator', '2015-04-27 18:04:03', NULL, NULL, NULL, NULL, NULL),
(9, 'RCV-0016', '2015-04-28 00:00:00', '2015-04-28 00:00:00', 'APPROVED', 'UNBILLED', 'UNPAID', 55, 'Central Park Boulevard #01-20, Bandung, Bandung, Indonesia', NULL, 2, 'ITEM', 'PO-0013', NULL, 'IDR', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'administrator', '2015-04-28 01:04:33', NULL, NULL, NULL, NULL, NULL),
(12, 'RCV-0019', '2015-04-29 00:00:00', '2015-04-29 00:00:00', 'ACTIVE', 'UNBILLED', 'UNPAID', 1, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', 'PO-0016', NULL, 'IDR', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'staff', '2015-04-29 14:04:57', NULL, NULL, NULL, NULL, NULL),
(13, 'RCV-0020', '2015-05-25 00:00:00', '2015-05-25 00:00:00', 'ACTIVE', 'UNBILLED', 'UNPAID', 3, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', 'PO-0030', NULL, 'IDR', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'staff', '2015-05-25 02:05:28', NULL, NULL, NULL, NULL, NULL),
(14, 'RCV-0021', '2015-06-04 00:00:00', '2015-06-04 00:00:00', 'ACTIVE', 'UNBILLED', 'UNPAID', 3, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', 'PO-0033', NULL, 'IDR', '1.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'manager', '2015-06-04 20:06:07', NULL, NULL, NULL, NULL, NULL),
(15, 'RCV-0022', '2015-06-18 00:00:00', '2015-06-18 00:00:00', 'ACTIVE', 'UNBILLED', 'UNPAID', 1, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', 'PO-0034', NULL, 'IDR', '1.00', '170000.00', '0.00', '0.00', NULL, NULL, '170000.00', NULL, '', NULL, 'manager', '2015-06-18 21:06:05', NULL, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_purchase_request_detail`;
CREATE TABLE IF NOT EXISTS `t_purchase_request_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `doc_line_type` varchar(5) DEFAULT NULL,
  `item_group` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(1000) NOT NULL,
  `item_qty` decimal(10,2) NOT NULL,
  `item_qty_closed` decimal(10,2) NOT NULL DEFAULT '0.00',
  `item_price` decimal(10,2) NOT NULL,
  `item_netprice` decimal(10,2) NOT NULL,
  `item_uom` varchar(25) DEFAULT NULL,
  `item_cost` decimal(10,2) DEFAULT NULL,
  `item_disc_amount` decimal(10,2) DEFAULT NULL,
  `item_disc_pct1` decimal(10,2) DEFAULT NULL,
  `item_disc_pct2` decimal(10,2) DEFAULT NULL,
  `item_tax` varchar(50) DEFAULT NULL,
  `item_total` decimal(10,2) DEFAULT NULL,
  `line_close` tinyint(4) NOT NULL DEFAULT '0',
  `reff_type` varchar(25) DEFAULT NULL,
  `reff_id` int(11) DEFAULT NULL,
  `reff_line` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_purchase_request_detail` (`doc_id`, `doc_line`, `doc_line_type`, `item_group`, `item_id`, `item_name`, `item_qty`, `item_qty_closed`, `item_price`, `item_netprice`, `item_uom`, `item_cost`, `item_disc_amount`, `item_disc_pct1`, `item_disc_pct2`, `item_tax`, `item_total`, `line_close`, `reff_type`, `reff_id`, `reff_line`) VALUES
(1, 1, 'ITEM', 2, 1, 'T Shirt A', '5.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(1, 2, 'ITEM', 2, 2, 'T Shirt B', '5.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(2, 1, 'ITEM', 2, 3, 'T Shirt C', '2.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(2, 2, 'ITEM', 2, 2, 'T Shirt B', '5.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(3, 1, 'ITEM', 2, 1, 'T Shirt A', '5.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(3, 2, 'ITEM', 2, 2, 'T Shirt B', '5.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(4, 1, 'ITEM', 2, 2, 'T Shirt B', '1.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(4, 2, 'ITEM', 2, 3, 'T Shirt C', '1.00', '0.00', '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_purchase_request_header`;
CREATE TABLE IF NOT EXISTS `t_purchase_request_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime NOT NULL,
  `doc_ddt` datetime NOT NULL,
  `doc_status` varchar(25) NOT NULL,
  `buss_id` int(11) DEFAULT NULL,
  `request_for` varchar(50) NOT NULL,
  `request_dept` varchar(50) DEFAULT NULL,
  `ship_addr` varchar(1000) DEFAULT NULL,
  `bill_addr` varchar(1000) DEFAULT NULL,
  `whse_id` int(11) NOT NULL,
  `doc_type` varchar(10) DEFAULT NULL,
  `doc_ref` varchar(50) DEFAULT NULL,
  `doc_curr` varchar(5) DEFAULT NULL,
  `doc_rate` decimal(10,2) DEFAULT NULL,
  `doc_subtotal` decimal(10,2) DEFAULT NULL,
  `doc_disc_percent` decimal(10,2) DEFAULT NULL,
  `doc_disc_amount` decimal(10,2) DEFAULT NULL,
  `doc_rounding` decimal(10,2) DEFAULT NULL,
  `doc_tax` decimal(10,2) DEFAULT NULL,
  `doc_total` decimal(10,2) DEFAULT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `int_note` varchar(1000) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` datetime DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `t_purchase_request_header` (`doc_id`, `doc_num`, `doc_dt`, `doc_ddt`, `doc_status`, `buss_id`, `request_for`, `request_dept`, `ship_addr`, `bill_addr`, `whse_id`, `doc_type`, `doc_ref`, `doc_curr`, `doc_rate`, `doc_subtotal`, `doc_disc_percent`, `doc_disc_amount`, `doc_rounding`, `doc_tax`, `doc_total`, `doc_note`, `int_note`, `create_by`, `create_dt`, `update_by`, `update_dt`, `cancel_by`, `cancel_dt`, `cancel_reason`) VALUES
(1, 'PRQ-0001', '2015-06-03 00:00:00', '2015-06-03 00:00:00', 'ACTIVE', NULL, '', NULL, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Diperlukan sangat cepat', NULL, 'manager', '2015-06-03 02:06:27', NULL, NULL, NULL, NULL, NULL),
(2, 'PRQ-0002', '2015-06-03 00:00:00', '2015-06-03 00:00:00', 'ACTIVE', NULL, '', NULL, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'manager', '2015-06-03 02:06:18', NULL, NULL, NULL, NULL, NULL),
(3, 'PRQ-0003', '2015-06-12 00:00:00', '2015-06-12 00:00:00', 'ACTIVE', NULL, '', NULL, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'manager', '2015-06-12 00:06:52', NULL, NULL, NULL, NULL, NULL),
(4, 'PRQ-0004', '2015-06-18 00:00:00', '2015-06-18 00:00:00', 'ACTIVE', NULL, '', NULL, 'Jl. Raya Bandung, Bandung, Bandung, Indonesia', NULL, 1, 'ITEM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'manager', '2015-06-18 17:06:04', NULL, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_purchase_return_detail`;
CREATE TABLE IF NOT EXISTS `t_purchase_return_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `doc_line_type` varchar(5) DEFAULT NULL,
  `item_group` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(1000) NOT NULL,
  `item_qty` decimal(10,2) NOT NULL,
  `item_qty_closed` decimal(10,2) NOT NULL DEFAULT '0.00',
  `item_price` decimal(10,2) NOT NULL,
  `item_netprice` decimal(10,2) NOT NULL,
  `item_uom` varchar(25) DEFAULT NULL,
  `item_cost` decimal(10,2) DEFAULT NULL,
  `item_disc_amount` decimal(10,2) DEFAULT NULL,
  `item_disc_pct1` decimal(10,2) DEFAULT NULL,
  `item_disc_pct2` decimal(10,2) DEFAULT NULL,
  `item_tax` varchar(50) DEFAULT NULL,
  `item_total` decimal(10,2) DEFAULT NULL,
  `line_close` tinyint(4) NOT NULL DEFAULT '0',
  `reff_type` varchar(25) DEFAULT NULL,
  `reff_id` int(11) DEFAULT NULL,
  `reff_line` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_purchase_return_header`;
CREATE TABLE IF NOT EXISTS `t_purchase_return_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime NOT NULL,
  `doc_ddt` datetime NOT NULL,
  `doc_status` varchar(25) NOT NULL DEFAULT 'ACTIVE',
  `logistic_status` varchar(25) NOT NULL DEFAULT 'PENDING',
  `billing_status` varchar(25) NOT NULL DEFAULT 'UNBILLED',
  `financial_status` varchar(25) NOT NULL DEFAULT 'UNPAID',
  `approval_status` enum('pending','partially approved','approved','revise','rejected') NOT NULL DEFAULT 'pending',
  `buss_id` int(11) NOT NULL,
  `ship_addr` varchar(1000) DEFAULT NULL,
  `bill_addr` varchar(1000) DEFAULT NULL,
  `whse_id` int(11) NOT NULL,
  `doc_type` varchar(10) DEFAULT NULL,
  `doc_ref` varchar(50) DEFAULT NULL,
  `doc_curr` varchar(5) DEFAULT NULL,
  `doc_rate` decimal(10,2) DEFAULT NULL,
  `doc_subtotal` decimal(10,2) DEFAULT NULL,
  `doc_disc_percent` decimal(10,2) DEFAULT NULL,
  `doc_disc_amount` decimal(10,2) DEFAULT NULL,
  `doc_rounding` decimal(10,2) DEFAULT NULL,
  `doc_tax` decimal(10,2) DEFAULT NULL,
  `doc_total` decimal(10,2) DEFAULT NULL,
  `doc_paid` decimal(10,2) DEFAULT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` datetime DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_report`;
CREATE TABLE IF NOT EXISTS `t_report` (
  `report_id` int(11) NOT NULL,
  `report_code` varchar(50) NOT NULL,
  `report_name` varchar(50) NOT NULL,
  `report_module` varchar(50) NOT NULL,
  `report_status` varchar(50) DEFAULT NULL,
  `report_query` varchar(500) DEFAULT NULL,
  `report_model` varchar(500) DEFAULT NULL,
  `report_view` varchar(500) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `t_report` (`report_id`, `report_code`, `report_name`, `report_module`, `report_status`, `report_query`, `report_model`, `report_view`) VALUES
(1, 'stock_balance', 'Stock Balance', 'Inventory', NULL, 'report/query/stock_balance', NULL, 'inventory/stock_balance'),
(2, 'stock_transaction', 'Stock Transaction', 'Inventory', NULL, 'report/query/stock_transaction', NULL, 'inventory/stock_transaction'),
(3, 'sales_transaction', 'Sales Transaction', 'Sales', NULL, 'report/query/sales_transaction', NULL, 'sales/sales_transaction');

DROP TABLE IF EXISTS `t_report_config`;
CREATE TABLE IF NOT EXISTS `t_report_config` (
  `report_id` int(11) NOT NULL,
  `config_id` int(11) NOT NULL,
  `label` varchar(50) NOT NULL,
  `object_type` varchar(50) NOT NULL,
  `object_name` varchar(50) NOT NULL,
  `data` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `t_report_config` (`report_id`, `config_id`, `label`, `object_type`, `object_name`, `data`) VALUES
(1, 1, 'Date From,Date To', 'date_multi', 'date', NULL),
(1, 2, 'Warehouse(s)', 'warehouse_multi', 'sel_wh', 'warehouse,consignment'),
(1, 3, 'Item(s)', 'item_multi', 'sel_item', NULL),
(2, 1, 'Date From,Date To', 'date_multi', 'date', NULL),
(2, 2, 'Warehouse(s)', 'warehouse_multi', 'sel_wh', 'warehouse,consignment'),
(2, 3, 'Item(s)', 'item_multi', 'sel_item', NULL),
(3, 1, 'Date From,Date To', 'date_multi', 'date', NULL),
(3, 2, 'Item(s)', 'item_multi', 'sel_item', NULL),
(3, 3, 'Customer', 'cust_single', 'sel_cust', NULL);

DROP TABLE IF EXISTS `t_sales_delivery_detail`;
CREATE TABLE IF NOT EXISTS `t_sales_delivery_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `doc_line_type` varchar(5) DEFAULT NULL,
  `item_group` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(1000) NOT NULL,
  `item_qty` decimal(10,2) NOT NULL,
  `item_qty_closed` decimal(10,2) NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `item_netprice` decimal(10,2) NOT NULL,
  `item_uom` varchar(25) DEFAULT NULL,
  `item_cost` decimal(10,2) DEFAULT NULL,
  `item_disc_amount` decimal(10,2) DEFAULT NULL,
  `item_disc_pct1` decimal(10,2) DEFAULT NULL,
  `item_disc_pct2` decimal(10,2) DEFAULT NULL,
  `item_tax` varchar(50) DEFAULT NULL,
  `item_total` decimal(10,2) DEFAULT NULL,
  `line_close` tinyint(4) NOT NULL DEFAULT '0',
  `reff_type` varchar(25) DEFAULT NULL,
  `reff_id` int(11) DEFAULT NULL,
  `reff_line` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_sales_delivery_header`;
CREATE TABLE IF NOT EXISTS `t_sales_delivery_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime NOT NULL,
  `doc_ddt` datetime NOT NULL,
  `doc_status` varchar(25) NOT NULL DEFAULT 'ACTIVE',
  `billing_status` varchar(25) NOT NULL DEFAULT 'UNBILLED',
  `financial_status` varchar(25) NOT NULL DEFAULT 'UNPAID',
  `buss_id` int(11) NOT NULL,
  `ship_addr` varchar(1000) DEFAULT NULL,
  `bill_addr` varchar(1000) DEFAULT NULL,
  `doc_buss_tlp1` varchar(25) DEFAULT NULL,
  `doc_buss_tlp2` varchar(25) DEFAULT NULL,
  `whse_id` int(11) NOT NULL,
  `doc_type` varchar(10) DEFAULT NULL,
  `doc_ref` varchar(50) DEFAULT NULL,
  `doc_ext_ref` varchar(50) DEFAULT NULL,
  `doc_curr` varchar(5) DEFAULT NULL,
  `doc_rate` decimal(10,0) DEFAULT NULL,
  `doc_subtotal` decimal(10,0) DEFAULT NULL,
  `doc_disc_percent` decimal(10,0) DEFAULT NULL,
  `doc_disc_amount` decimal(10,0) DEFAULT NULL,
  `doc_rounding` decimal(10,0) DEFAULT NULL,
  `doc_tax` decimal(10,0) DEFAULT NULL,
  `doc_total` decimal(10,0) DEFAULT NULL,
  `doc_cost_freight` decimal(10,0) DEFAULT NULL,
  `doc_cost_other` decimal(10,0) DEFAULT NULL,
  `doc_cost_freight_type` varchar(50) DEFAULT NULL,
  `doc_cost_other_type` varchar(50) DEFAULT NULL,
  `doc_cost_freight_party` varchar(250) DEFAULT NULL,
  `doc_cost_other_party` varchar(250) DEFAULT NULL,
  `doc_paid` decimal(10,0) DEFAULT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `doc_freight` varchar(25) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` datetime DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_sales_invoice_detail`;
CREATE TABLE IF NOT EXISTS `t_sales_invoice_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `doc_line_type` varchar(5) DEFAULT NULL,
  `item_group` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(1000) NOT NULL,
  `item_qty` decimal(10,2) NOT NULL,
  `item_qty_closed` decimal(10,2) NOT NULL DEFAULT '0.00',
  `item_price` decimal(10,2) NOT NULL,
  `item_netprice` decimal(10,2) NOT NULL,
  `item_uom` varchar(25) DEFAULT NULL,
  `item_cost` decimal(10,2) DEFAULT NULL,
  `item_disc_amount` decimal(10,2) DEFAULT NULL,
  `item_disc_pct1` decimal(10,2) DEFAULT NULL,
  `item_disc_pct2` decimal(10,2) DEFAULT NULL,
  `item_tax` varchar(50) DEFAULT NULL,
  `item_total` decimal(10,2) DEFAULT NULL,
  `line_close` tinyint(4) NOT NULL DEFAULT '0',
  `reff_type` varchar(25) DEFAULT NULL,
  `reff_id` int(11) DEFAULT NULL,
  `reff_line` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_sales_invoice_header`;
CREATE TABLE IF NOT EXISTS `t_sales_invoice_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime NOT NULL,
  `doc_ddt` datetime NOT NULL,
  `doc_status` varchar(25) NOT NULL DEFAULT 'ACTIVE',
  `financial_status` varchar(25) NOT NULL DEFAULT 'UNPAID',
  `buss_id` int(11) NOT NULL,
  `ship_addr` varchar(1000) DEFAULT NULL,
  `bill_addr` varchar(1000) DEFAULT NULL,
  `whse_id` int(11) NOT NULL,
  `doc_type` varchar(10) DEFAULT NULL,
  `doc_ref` varchar(50) DEFAULT NULL,
  `doc_ext_ref` varchar(50) DEFAULT NULL,
  `doc_curr` varchar(5) DEFAULT NULL,
  `doc_rate` decimal(10,2) DEFAULT NULL,
  `doc_subtotal` decimal(10,2) DEFAULT NULL,
  `doc_disc_percent` decimal(10,2) DEFAULT NULL,
  `doc_disc_amount` decimal(10,2) DEFAULT NULL,
  `doc_rounding` decimal(10,2) DEFAULT NULL,
  `doc_tax` decimal(10,2) DEFAULT NULL,
  `doc_total` decimal(10,2) DEFAULT NULL,
  `doc_cost_freight` decimal(10,2) DEFAULT NULL,
  `doc_cost_other` decimal(10,2) DEFAULT NULL,
  `doc_paid` decimal(10,2) DEFAULT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` datetime DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_sales_order_detail`;
CREATE TABLE IF NOT EXISTS `t_sales_order_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `doc_line_type` varchar(5) DEFAULT NULL,
  `item_group` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(1000) NOT NULL,
  `item_qty` decimal(10,2) NOT NULL,
  `item_qty_closed` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `item_price` decimal(10,2) NOT NULL,
  `item_netprice` decimal(10,2) NOT NULL,
  `item_uom` varchar(25) DEFAULT NULL,
  `item_cost` decimal(10,2) DEFAULT NULL,
  `item_disc_amount` decimal(10,2) DEFAULT NULL,
  `item_disc_pct1` decimal(10,2) DEFAULT NULL,
  `item_disc_pct2` decimal(10,2) DEFAULT NULL,
  `item_tax` varchar(50) DEFAULT NULL,
  `item_total` decimal(10,2) DEFAULT NULL,
  `line_close` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_sales_order_detail` (`doc_id`, `doc_line`, `doc_line_type`, `item_group`, `item_id`, `item_name`, `item_qty`, `item_qty_closed`, `item_price`, `item_netprice`, `item_uom`, `item_cost`, `item_disc_amount`, `item_disc_pct1`, `item_disc_pct2`, `item_tax`, `item_total`, `line_close`) VALUES
(1, 1, 'ITEM', 2, 6, 'Simbadda', '1.00', '0.00', '675000.00', '675000.00', NULL, '675000.00', NULL, NULL, NULL, NULL, '675000.00', 0);

DROP TABLE IF EXISTS `t_sales_order_header`;
CREATE TABLE IF NOT EXISTS `t_sales_order_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime NOT NULL,
  `doc_ddt` datetime NOT NULL,
  `doc_status` varchar(25) NOT NULL,
  `logistic_status` varchar(25) NOT NULL DEFAULT 'PENDING',
  `billing_status` varchar(25) NOT NULL DEFAULT 'UNBILLED',
  `financial_status` varchar(25) NOT NULL DEFAULT 'UNPAID',
  `approval_status` enum('pending','partially approved','approved','revise','rejected') NOT NULL DEFAULT 'pending',
  `buss_id` int(11) NOT NULL,
  `ship_addr` varchar(1000) DEFAULT NULL,
  `bill_addr` varchar(1000) DEFAULT NULL,
  `doc_buss_tlp1` varchar(25) DEFAULT NULL,
  `doc_buss_tlp2` varchar(25) DEFAULT NULL,
  `whse_id` int(11) NOT NULL,
  `doc_type` varchar(10) DEFAULT NULL,
  `doc_ref` varchar(50) DEFAULT NULL,
  `doc_curr` varchar(5) DEFAULT NULL,
  `doc_rate` decimal(10,0) DEFAULT NULL,
  `doc_subtotal` decimal(10,0) DEFAULT NULL,
  `doc_disc_percent` decimal(10,0) DEFAULT NULL,
  `doc_disc_amount` decimal(10,0) DEFAULT NULL,
  `doc_rounding` decimal(10,0) DEFAULT NULL,
  `doc_tax` decimal(10,0) DEFAULT NULL,
  `doc_total` decimal(10,0) DEFAULT NULL,
  `doc_paid` decimal(10,2) DEFAULT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `int_note` varchar(1000) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` datetime DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `t_sales_order_header` (`doc_id`, `doc_num`, `doc_dt`, `doc_ddt`, `doc_status`, `logistic_status`, `billing_status`, `financial_status`, `approval_status`, `buss_id`, `ship_addr`, `bill_addr`, `doc_buss_tlp1`, `doc_buss_tlp2`, `whse_id`, `doc_type`, `doc_ref`, `doc_curr`, `doc_rate`, `doc_subtotal`, `doc_disc_percent`, `doc_disc_amount`, `doc_rounding`, `doc_tax`, `doc_total`, `doc_paid`, `doc_note`, `int_note`, `create_by`, `create_dt`, `update_by`, `update_dt`, `cancel_by`, `cancel_dt`, `cancel_reason`) VALUES
(1, 'SAL-0001', '2014-11-07 00:00:00', '2014-11-07 00:00:00', 'ACTIVE', 'PENDING', 'UNBILLED', 'UNPAID', 'approved', 8, 'Mediterania Boulevard, Flower 1 No. 998, Kamal Muara, Penjaringan Jakarta Utara Indonesia', NULL, '08170702xxxx', '', 1, 'ITEM', NULL, 'IDR', '1', '675000', '0', '0', NULL, NULL, '675000', NULL, '', NULL, 'operation', '2014-11-07 11:11:44', NULL, NULL, NULL, NULL, NULL),
(2, 'SAL-0002', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'ACTIVE', 'PENDING', 'UNBILLED', 'UNPAID', 'pending', 5, 'Jl. Tomang Raya No.312, Tomang, Grogol Petamburan Jakarta Barat Indonesia', NULL, '08571866xxxx', '', 2, 'ITEM', NULL, 'IDR', '1', '4725000', '0', '0', NULL, NULL, '4725000', NULL, '', NULL, 'administrator', '2015-04-23 18:04:10', NULL, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_sales_pos`;
CREATE TABLE IF NOT EXISTS `t_sales_pos` (
  `trx_id` int(11) NOT NULL,
  `trx_code` varchar(50) NOT NULL,
  `trx_datetime` datetime DEFAULT NULL,
  `trx_user_id` int(11) NOT NULL,
  `trx_cashier_id` int(11) NOT NULL,
  `trx_sales_amount` decimal(18,2) NOT NULL DEFAULT '0.00',
  `posted` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

INSERT INTO `t_sales_pos` (`trx_id`, `trx_code`, `trx_datetime`, `trx_user_id`, `trx_cashier_id`, `trx_sales_amount`, `posted`) VALUES
(1, '1-2-1429215280', '2015-04-17 03:14:40', 1, 1, '4725000.00', b'0'),
(2, '1-2-1429240501', '2015-04-17 10:15:01', 1, 1, '4725000.00', b'0'),
(3, '1-2-1429254146', '2015-04-17 14:02:26', 1, 1, '6075000.00', b'0'),
(4, '1-2-1429254497', '2015-04-17 14:08:17', 1, 1, '6075000.00', b'0'),
(5, '1-2-1429255264', '2015-04-17 14:21:04', 1, 1, '675000.00', b'0'),
(6, '1-2-1429785435', '2015-04-23 17:37:15', 1, 1, '3375000.00', b'0'),
(7, '1-2-1429786877', '2015-04-23 18:01:17', 1, 1, '6075000.00', b'1'),
(8, '1-2-1429787044', '2015-04-23 18:04:04', 1, 1, '2700000.00', b'1'),
(9, '1-2-1430128295', '2015-04-27 16:51:35', 1, 1, '5400000.00', b'0'),
(10, '1-2-1430128334', '2015-04-27 16:52:14', 1, 1, '2700000.00', b'0'),
(11, '1-2-1430132286', '2015-04-27 17:58:06', 1, 1, '675000.00', b'0'),
(12, '1-2-1430208271', '2015-04-28 15:04:31', 1, 1, '2700000.00', b'0'),
(13, '1-2-1430208284', '2015-04-28 15:04:44', 1, 1, '1350000.00', b'0'),
(14, '1-2-1430208300', '2015-04-28 15:05:00', 1, 1, '7425000.00', b'0'),
(15, '1-2-1430292266', '2015-04-29 14:24:26', 4, 1, '675000.00', b'0'),
(16, '1-2-1430292288', '2015-04-29 14:24:48', 4, 1, '2700000.00', b'0'),
(17, '1-2-1430292924', '2015-04-29 14:35:24', 4, 1, '675000.00', b'0'),
(18, '1-2-1430934172', '2015-05-07 00:42:52', 1, 1, '675000.00', b'1'),
(19, '1-2-1430934209', '2015-05-07 00:43:29', 1, 1, '2700000.00', b'1'),
(22, '1-2-1430937420', '2015-05-07 01:37:00', 1, 1, '675000.00', b'0'),
(23, '1-2-1430938093', '2015-05-07 01:48:13', 1, 1, '675000.00', b'0'),
(24, '1-2-1430938320', '2015-05-07 01:52:00', 1, 1, '2025000.00', b'0'),
(25, '1-2-1430938650', '2015-05-07 01:57:30', 1, 1, '2700000.00', b'0'),
(26, '1-2-1430941685', '2015-05-07 02:48:05', 1, 1, '2700000.00', b'0'),
(27, '1-2-1430941814', '2015-05-07 02:50:14', 1, 1, '2025000.00', b'0'),
(28, '1-2-1430941977', '2015-05-07 02:52:57', 1, 1, '675000.00', b'0'),
(29, '1-2-1430942273', '2015-05-07 02:57:53', 1, 1, '675000.00', b'0'),
(30, '1-2-1430942665', '2015-05-07 03:04:25', 1, 1, '2700000.00', b'0'),
(31, '1-2-1431081189', '2015-05-08 17:33:09', 5, 1, '1350000.00', b'0'),
(32, '1-2-1454147157', '2016-01-30 16:45:57', 5, 1, '350000.00', b'1');

DROP TABLE IF EXISTS `t_sales_pos_item`;
CREATE TABLE IF NOT EXISTS `t_sales_pos_item` (
  `id` int(11) NOT NULL,
  `trx_id` int(11) NOT NULL,
  `line_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_qty` decimal(10,2) NOT NULL DEFAULT '0.00',
  `item_price` decimal(18,2) NOT NULL DEFAULT '0.00',
  `item_total_price` decimal(18,2) NOT NULL DEFAULT '0.00',
  `include_tax` bit(1) DEFAULT b'1'
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

INSERT INTO `t_sales_pos_item` (`id`, `trx_id`, `line_id`, `item_id`, `item_qty`, `item_price`, `item_total_price`, `include_tax`) VALUES
(1, 1, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(2, 1, 2, 2, '2.00', '2025000.00', '4050000.00', b'1'),
(3, 2, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(4, 2, 2, 2, '2.00', '2025000.00', '4050000.00', b'1'),
(5, 3, 1, 1, '2.00', '675000.00', '1350000.00', b'1'),
(6, 3, 2, 2, '1.00', '2025000.00', '2025000.00', b'1'),
(7, 3, 3, 4, '1.00', '2700000.00', '2700000.00', b'1'),
(8, 4, 1, 1, '2.00', '675000.00', '1350000.00', b'1'),
(9, 4, 2, 4, '1.00', '2700000.00', '2700000.00', b'1'),
(10, 4, 3, 2, '1.00', '2025000.00', '2025000.00', b'1'),
(11, 5, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(12, 6, 1, 1, '2.00', '675000.00', '1350000.00', b'1'),
(13, 6, 2, 2, '1.00', '2025000.00', '2025000.00', b'1'),
(14, 7, 1, 1, '2.00', '675000.00', '1350000.00', b'1'),
(15, 7, 2, 2, '1.00', '2025000.00', '2025000.00', b'1'),
(16, 7, 3, 4, '1.00', '2700000.00', '2700000.00', b'1'),
(17, 8, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(18, 8, 2, 2, '1.00', '2025000.00', '2025000.00', b'1'),
(19, 9, 1, 1, '2.00', '675000.00', '1350000.00', b'1'),
(20, 9, 2, 2, '2.00', '2025000.00', '4050000.00', b'1'),
(21, 10, 1, 4, '1.00', '2700000.00', '2700000.00', b'1'),
(22, 11, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(23, 12, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(24, 12, 2, 2, '1.00', '2025000.00', '2025000.00', b'1'),
(25, 13, 1, 1, '2.00', '675000.00', '1350000.00', b'1'),
(26, 14, 1, 2, '1.00', '2025000.00', '2025000.00', b'1'),
(27, 14, 2, 4, '2.00', '2700000.00', '5400000.00', b'1'),
(28, 15, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(29, 16, 1, 4, '1.00', '2700000.00', '2700000.00', b'1'),
(30, 17, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(31, 18, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(32, 19, 1, 2, '1.00', '2025000.00', '2025000.00', b'1'),
(33, 19, 2, 1, '1.00', '675000.00', '675000.00', b'1'),
(36, 22, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(37, 23, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(38, 24, 1, 2, '1.00', '2025000.00', '2025000.00', b'1'),
(39, 25, 1, 4, '1.00', '2700000.00', '2700000.00', b'1'),
(40, 26, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(41, 26, 2, 2, '1.00', '2025000.00', '2025000.00', b'1'),
(42, 27, 1, 2, '1.00', '2025000.00', '2025000.00', b'1'),
(43, 28, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(44, 29, 1, 1, '1.00', '675000.00', '675000.00', b'1'),
(45, 30, 1, 4, '1.00', '2700000.00', '2700000.00', b'1'),
(46, 31, 1, 1, '2.00', '675000.00', '1350000.00', b'1'),
(47, 32, 1, 1, '2.00', '175000.00', '350000.00', b'1');

DROP TABLE IF EXISTS `t_sales_pos_payment`;
CREATE TABLE IF NOT EXISTS `t_sales_pos_payment` (
  `id` int(11) NOT NULL,
  `trx_id` int(11) NOT NULL,
  `line_id` int(11) NOT NULL,
  `method` varchar(45) NOT NULL DEFAULT 'cash',
  `amount` decimal(18,2) NOT NULL DEFAULT '0.00',
  `identifier_code` varchar(1000) DEFAULT NULL COMMENT 'Magnetic number on card or Voucher code ID',
  `bank` varchar(45) DEFAULT NULL,
  `card_holder` varchar(255) DEFAULT NULL,
  `card_number` varchar(50) DEFAULT NULL,
  `card_expiry` datetime DEFAULT NULL,
  `edc_trx_number` varchar(1000) DEFAULT NULL,
  `change_amount` decimal(18,2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

INSERT INTO `t_sales_pos_payment` (`id`, `trx_id`, `line_id`, `method`, `amount`, `identifier_code`, `bank`, `card_holder`, `card_number`, `card_expiry`, `edc_trx_number`, `change_amount`) VALUES
(1, 1, 1, 'cash', '4800000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 1, 'cash', '4800000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 1, 'cash', '6100000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 4, 1, 'cash', '6100000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 5, 1, 'cash', '700000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 6, 1, 'cash', '3400000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 7, 1, 'cash', '6100000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 8, 1, 'cash', '1000000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 8, 2, 'creditcard', '1700000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 9, 1, 'cash', '5500000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 10, 1, 'cash', '2700000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 11, 1, 'cash', '700000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 12, 1, 'cash', '2700000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 13, 1, 'cash', '1400000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 14, 1, 'cash', '7500000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 15, 1, 'cash', '680000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 16, 1, 'cash', '2800000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 17, 1, 'cash', '680000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 18, 1, 'cash', '680000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 19, 1, 'cash', '1000000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 19, 2, 'creditcard', '1700000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 22, 2, 'cash', '675000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 23, 2, 'cash', '675000.00', NULL, NULL, NULL, NULL, NULL, NULL, '25000.00'),
(24, 24, 2, 'cash', '2025000.00', NULL, NULL, NULL, NULL, NULL, NULL, '25000.00'),
(25, 25, 1, 'cash', '1000000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 25, 2, 'creditcard', '1700000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 26, 1, 'voucher', '50000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 26, 2, 'voucher', '50000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 26, 3, 'cash', '2000000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 26, 4, 'creditcard', '600000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 27, 1, 'voucher', '50000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 27, 2, 'voucher', '50000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 27, 3, 'cash', '1925000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 28, 1, 'voucher', '50000.00', '506515000229630', NULL, NULL, NULL, NULL, NULL, NULL),
(35, 28, 2, 'creditcard', '625000.00', '', NULL, NULL, NULL, NULL, NULL, NULL),
(36, 29, 1, 'voucher', '50000.00', '506515000268546', NULL, NULL, NULL, NULL, NULL, NULL),
(37, 29, 2, 'cash', '625000.00', '', NULL, NULL, NULL, NULL, NULL, NULL),
(38, 30, 1, 'voucher', '50000.00', 'V506515000010610', NULL, NULL, NULL, NULL, NULL, NULL),
(39, 30, 2, 'cash', '2650000.00', '', NULL, NULL, NULL, NULL, NULL, NULL),
(40, 31, 2, 'cash', '1350000.00', '', NULL, NULL, NULL, NULL, NULL, '50000.00'),
(41, 32, 2, 'cash', '350000.00', '', NULL, NULL, NULL, NULL, NULL, '50000.00');

DROP TABLE IF EXISTS `t_side_menu`;
CREATE TABLE IF NOT EXISTS `t_side_menu` (
  `menu_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `menu_name` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `menu_link` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `menu_description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `name_prefix` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `name_postfix` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `last_node` tinyint(1) NOT NULL DEFAULT '0',
  `order_no` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(255) NOT NULL DEFAULT 'fa fa-angle-double-right',
  `active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;

INSERT INTO `t_side_menu` (`menu_id`, `parent_id`, `menu_name`, `menu_link`, `menu_description`, `name_prefix`, `name_postfix`, `last_node`, `order_no`, `icon`, `active`) VALUES
(1, 0, 'Purchase', '', 'Purchase Module', '<i class="fa fa-shopping-cart"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 3, 'fa fa-shopping-cart', b'1'),
(2, 1, 'Purchase Order', 'purchase/purchase_order', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 4, 'fa fa-angle-double-right', b'1'),
(3, 1, 'Purchase Receipt', 'purchase/purchase_receipt', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 5, 'fa fa-angle-double-right', b'1'),
(4, 1, 'Purchase Invoice', 'purchase/purchase_invoice', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 6, 'fa fa-angle-double-right', b'1'),
(5, 0, 'Inventory', '', 'Inventory Module', '<i class="fa fa-tags"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 4, 'fa fa-tags', b'1'),
(6, 5, 'Stock List', 'inventory/stock', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-angle-double-right', b'1'),
(7, 5, 'Warehouse', 'inventory/warehouse', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 4, 'fa fa-angle-double-right', b'1'),
(8, 0, 'Sales', '', 'Sales Module', '<i class="fa fa-usd"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 5, 'fa fa-usd', b'1'),
(9, 8, 'Sales Order', 'sales/sales_order', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 9, 'fa fa-angle-double-right', b'1'),
(10, 8, 'Sales Delivery', 'sales/sales_delivery', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 10, 'fa fa-angle-double-right', b'1'),
(11, 8, 'Sales Invoice', 'sales/sales_invoice', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 11, 'fa fa-angle-double-right', b'1'),
(14, 5, 'Stock Transfer', 'inventory/transfer', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 3, 'fa fa-angle-double-right', b'1'),
(16, 0, 'Accounting', '', 'Accounting Module', '<i class="fa fa-book"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 1, 'fa fa-book', b'1'),
(17, 16, 'Chart of Account', 'account/coa', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 17, 'fa fa-angle-double-right', b'1'),
(18, 16, 'Journal Entry', 'account/journal', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 18, 'fa fa-angle-double-right', b'1'),
(19, 0, 'Finance', '', 'Finance Module', '<i class="fa fa-money"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 2, 'fa fa-money', b'1'),
(20, 19, 'Payment In', 'finance/payment_in', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 3, 'fa fa-angle-double-right', b'1'),
(21, 19, 'Payment Out', 'finance/payment_out', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 4, 'fa fa-angle-double-right', b'1'),
(22, 1, 'Purchase Return', 'purchase/purchase_return', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 7, 'fa fa-angle-double-right', b'0'),
(23, 5, 'Style List', 'inventory/style', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'0'),
(24, 5, 'Inventory Adjustment', 'inventory/adjustment', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 4, 'fa fa-angle-double-right', b'1'),
(25, 0, 'Business Partner', 'business/partner', 'Business Partner Module', '<i class="fa fa-building-o"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 3, 'fa fa-building-o', b'1'),
(26, 25, 'Vendors', 'business/vendor_list', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-angle-double-right', b'1'),
(27, 25, 'Customers', 'business/customer_list', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'1'),
(28, 0, 'Reports', '', 'Report', '<i class="fa fa-file-text-o"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 8, 'fa fa-file-text-o', b'1'),
(29, 28, 'Inventory', '', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 4, 'fa fa-angle-double-right', b'1'),
(30, 29, 'Stock Balance', 'report/prepare/inventory_balance', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-angle-double-right', b'1'),
(31, 29, 'Stock Card', 'report/prepare/inventory_transaction', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'1'),
(32, 28, 'Sales', '', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 5, 'fa fa-angle-double-right', b'1'),
(33, 32, 'Sales Report', 'report/prepare/sales_order', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-angle-double-right', b'1'),
(35, 0, 'Settings', '', 'Setting', '<i class="fa fa-gear"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 9, 'fa fa-gear', b'1'),
(36, 49, 'Accounting', '', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 1, 'fa fa-angle-double-right', b'1'),
(38, 36, 'Journal Template', 'setting/acc_journaltemplate', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'1'),
(39, 28, 'Purchasing', '', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 3, 'fa fa-angle-double-right', b'1'),
(40, 28, 'Financials', '', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 2, 'fa fa-angle-double-right', b'1'),
(41, 28, 'Accounting', '', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 1, 'fa fa-angle-double-right', b'1'),
(42, 35, 'Company', '', 'Company Details', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 0, 'fa fa-angle-double-right', b'1'),
(43, 35, 'General Setting', 'setting/general', 'General Setting', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 0, 'fa fa-angle-double-right', b'0'),
(44, 35, 'Document Numbering', 'setting/doc_number', 'Document Numbering', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 0, 'fa fa-angle-double-right', b'0'),
(45, 35, 'User', 'setting/users', 'User Setting', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 0, 'fa fa-angle-double-right', b'1'),
(46, 35, 'Authorization', 'setting/auth', 'Authorization', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 0, 'fa fa-angle-double-right', b'0'),
(47, 35, 'Approval', 'setting/approval', 'Approval', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 0, 'fa fa-angle-double-right', b'1'),
(48, 35, 'Alert', 'setting/alert', 'Alert', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 0, 'fa fa-angle-double-right', b'0'),
(49, 35, 'Master Data', '', '', '', '', 0, 0, '', b'0'),
(50, 49, 'Finance', '', '', '', '', 0, 1, '', b'0'),
(51, 49, 'Tax', '', '', '', '', 0, 2, '', b'0'),
(52, 49, 'Purchasing', '', '', '', '', 0, 3, '', b'0'),
(53, 49, 'Business', '', '', '', '', 0, 4, '', b'0'),
(54, 49, 'Banking', '', '', '', '', 0, 5, '', b'0'),
(55, 49, 'Inventory', '', '', '', '', 0, 6, '', b'0'),
(56, 50, 'Cash Account', 'setting/master_fin_cash_account', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-angle-double-right', b'0'),
(57, 50, 'GL Link Account', 'setting/master_fin_link_account', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'0'),
(58, 50, 'Currency', 'setting/master_fin_curr', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 3, 'fa fa-angle-double-right', b'0'),
(59, 50, 'Currency Rate', 'setting/master_fin_curr_rate', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 4, 'fa fa-angle-double-right', b'0'),
(60, 50, 'Transaction Code', 'setting/master_fin_trans_code', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 5, 'fa fa-angle-double-right', b'0'),
(61, 50, 'Cash Flow Line', 'setting/master_fin_cash_flow', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 6, 'fa fa-angle-double-right', b'0'),
(62, 50, 'Payment Term', 'setting/master_fin_payment_term', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 7, 'fa fa-angle-double-right', b'0'),
(63, 51, 'Tax Group', 'setting/master_tax_group', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-angle-double-right', b'0'),
(64, 51, 'Tax Code', 'setting/master_tax_code', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'0'),
(65, 51, 'Withholding Tax', 'setting/master_tax_withholding', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 3, 'fa fa-angle-double-right', b'0'),
(66, 52, 'Landed Cost', 'setting/master_pur_landcost', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-angle-double-right', b'0'),
(67, 53, 'Customer Group', 'setting/master_buss_cust_group', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-angle-double-right', b'0'),
(68, 53, 'Vendor Group', 'setting/master_buss_vend_group', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'0'),
(69, 53, 'Customer List', 'setting/master_buss_cust', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 3, 'fa fa-angle-double-right', b'0'),
(70, 53, 'Vendor List', 'setting/master_buss_vend', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 4, 'fa fa-angle-double-right', b'0'),
(71, 54, 'Bank List', 'setting/master_bank', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-angle-double-right', b'0'),
(72, 54, 'Bank Account', 'setting/master_bank_account', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'0'),
(73, 55, 'Item Group', 'setting/master_item_group', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-angle-double-right', b'0'),
(74, 55, 'Item List', 'setting/master_item', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'0'),
(75, 55, 'Warehouse', 'setting/master_wh', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 3, 'fa fa-angle-double-right', b'0'),
(76, 55, 'UoM', 'setting/master_uom', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 4, 'fa fa-angle-double-right', b'0'),
(77, 55, 'Manufacturer', 'setting/master_manuf', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 5, 'fa fa-angle-double-right', b'0'),
(78, 55, 'Brand', 'setting/master_brand', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 6, 'fa fa-angle-double-right', b'0'),
(79, 8, 'POS', 'sales/pos', 'POS Sales', '<i class="fa fa-angle-double-right"></i>', '', 1, 1, 'fa fa-angle-double-right', b'1'),
(80, 0, 'Fixed Asset', '', '', '<i class="fa fa-building-o"></i>', '', 0, 3, 'fa fa-building-o', b'1'),
(81, 80, 'Asset Information', '', '', '<i class="fa fa-info"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 2, 'fa fa-info', b'1'),
(82, 81, 'Asset List', 'asset/asset_list', '', '<i class="fa fa-angle-double-right"></i>', '', 1, 3, 'fa fa-angle-double-right', b'1'),
(83, 81, 'Asset Group', 'asset/asset_group', '', '<i class="fa fa-angle-double-right"></i>', '', 1, 4, 'fa fa-angle-double-right', b'1'),
(88, 80, 'Asset Transaction', '', '', '<i class="fa fa-external-link-square"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 0, 12, 'fa fa-external-link-square', b'1'),
(89, 88, 'Non Financial Transaction', 'asset/non_financial_trans', '', '<i class="fa fa-angle-double-right"></i>', '', 1, 13, 'fa fa-angle-double-right', b'1'),
(90, 88, 'Financial Transaction', 'asset/financial_trans', '', '<i class="fa fa-angle-double-right"></i>', '', 1, 14, 'fa fa-angle-double-right', b'1'),
(91, 80, 'Asset Usage', '', '', '<i class="fa fa-tasks"></i>', '<span><i class="fa fa-angle-left pull-right"></i>', 0, 5, 'fa fa-tasks', b'1'),
(92, 91, 'Usage Request', 'asset/usage_list', '', '<i class="fa fa-angle-double-right"></i>', '', 1, 6, 'fa fa-angle-double-right', b'1'),
(93, 16, 'Cost Center', 'account/costcenter', 'Cost Center Master', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'1'),
(94, 28, 'Assets', '', 'Asset Reports', '<i class="fa fa-file-text"></i>', '', 0, 25, 'fa fa-file-text', b'1'),
(95, 94, 'Asset List', 'report/prepare/asset_list', 'Display a list of assets', '<i class="fa fa-angle-double-right"></i>', '', 1, 26, 'fa fa-angle-double-right', b'1'),
(96, 94, 'Asset Movement', 'report/prepare/asset_move', 'Display Asset Movement', '<i class="fa fa-angle-double-right"></i>', '', 1, 27, 'fa fa-angle-double-right', b'1'),
(97, 94, 'Asset History', 'report/prepare/asset_history', 'Display Asset History', '<i class="fa fa-angle-double-right"></i>', '', 1, 28, 'fa fa-angle-double-right', b'1'),
(98, 40, 'Balance Sheet', 'report/prepare/balance_sheet', 'Display Company Balance Sheet', '<i class="fa fa-angle-double-right"></i>', '', 1, 1, 'fa fa-angle-double-right', b'1'),
(99, 40, 'Profit &amp; Loss', 'report/prepare/profit_loss', 'Display Company Balance Sheet', '<i class="fa fa-angle-double-right"></i>', '', 1, 1, 'fa fa-angle-double-right', b'1'),
(100, 41, 'Ledger Report', 'report/prepare/acc_ledger', 'Accounting Ledger Report', '<i class="fa fa-book"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-book', b'1'),
(101, 41, 'Trial Balance Report', 'report/prepare/trial_balance', 'Trial Balance Report', '<i class="fa fa-book"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-book', b'1'),
(102, 39, 'Purchase Report', 'report/prepare/purchase_order', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 1, 'fa fa-angle-double-right', b'1'),
(103, 39, 'Purchase Report Detail', 'report/prepare/purchase_order_detail', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'1'),
(104, 8, 'Voucher List', 'sales/vouchers', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'1'),
(105, 42, 'Org Structure', 'setting/position', 'Organisation Structure', '<i class="fa fa-sitemap"></i>', '', 1, 2, 'fa fa-sitemap', b'1'),
(106, 35, 'System Settings', '', '', '<i class="fa fa-gear"></i>', '', 0, 2, 'fa fa-gear', b'1'),
(107, 106, 'Menu Management', 'setting/menu', 'Manage the sidebar', '<i class="fa fa-server"></i>', '', 0, 2, 'fa fa-server', b'1'),
(108, 42, 'Companies', 'setting/company', 'Manage multiple companies', '', '', 1, 1, 'fa fa-building', b'1'),
(109, 1, 'Purchase Request', 'purchase/purchase_request', 'Purchase Request', '', '', 1, 1, 'fa fa-angle-double-right', b'1'),
(110, 5, 'Stock Category', 'inventory/category', 'Item Category', '', '', 1, 1, 'fa fa-angle-double-right', b'1'),
(111, 1, 'Purchase Contract', 'purchase/purchase_contract', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 2, 'fa fa-angle-double-right', b'1'),
(112, 1, 'Purchase Quotation', 'purchase/purchase_quotation', '', '<i class="fa fa-angle-double-right"></i>', '</span><i class="fa fa-angle-left pull-right"></i>', 1, 3, 'fa fa-angle-double-right', b'1');

DROP TABLE IF EXISTS `t_site_cashier`;
CREATE TABLE IF NOT EXISTS `t_site_cashier` (
  `cashier_id` int(11) NOT NULL,
  `cashier_code` varchar(255) DEFAULT NULL,
  `location_id` int(11) NOT NULL,
  `is_closed` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `t_site_cashier` (`cashier_id`, `cashier_code`, `location_id`, `is_closed`) VALUES
(1, '#1', 2, b'0'),
(2, '#2', 2, b'0');

DROP TABLE IF EXISTS `t_site_location`;
CREATE TABLE IF NOT EXISTS `t_site_location` (
  `location_id` int(11) unsigned NOT NULL,
  `location_code` varchar(20) NOT NULL,
  `location_name` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `location_address` varchar(1000) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `location_phone` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `location_city` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `location_state` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `location_country` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT 'Indonesia',
  `location_description` text CHARACTER SET latin1 NOT NULL,
  `is_active` bit(1) DEFAULT b'1',
  `is_warehouse` bit(1) DEFAULT b'1',
  `is_production` bit(1) DEFAULT b'0',
  `is_virtual` bit(1) DEFAULT b'0',
  `is_sales` bit(1) DEFAULT b'1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `t_site_location` (`location_id`, `location_code`, `location_name`, `location_address`, `location_phone`, `location_city`, `location_state`, `location_country`, `location_description`, `is_active`, `is_warehouse`, `is_production`, `is_virtual`, `is_sales`) VALUES
(1, 'HO', 'Bandung Head Office', 'Jl. Raya Bandung', '022 1234 5678', 'Bandung', 'Bandung', 'Indonesia', 'Main Warehouse and Head Office', b'1', b'1', b'0', b'0', b'1'),
(2, 'BDG02', 'Bandung Outlet 02', 'Central Park Boulevard #01-20', '', 'Bandung', 'Bandung', 'Indonesia', '', b'1', b'1', b'0', b'0', b'1'),
(3, 'CONS', 'CONS', 'Consignment', '', '', '', 'Indonesia', '', b'1', b'0', b'0', b'1', b'1'),
(4, 'PROD', 'PROD', 'Production', '', '', '', 'Indonesia', '', b'1', b'0', b'1', b'0', b'1'),
(5, 'TRANS', 'TRANS', 'Head Office', '', '', '', 'Indonesia', '', b'1', b'1', b'0', b'1', b'1');

DROP TABLE IF EXISTS `t_stock_adjust_detail`;
CREATE TABLE IF NOT EXISTS `t_stock_adjust_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_qty` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `t_stock_adjust_header`;
CREATE TABLE IF NOT EXISTS `t_stock_adjust_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime NOT NULL,
  `doc_ddt` datetime NOT NULL,
  `doc_status` varchar(45) NOT NULL,
  `approval_status` enum('pending','partially approved','approved','revise','rejected') NOT NULL,
  `whse_id` int(11) NOT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` timestamp NULL DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` timestamp NULL DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` timestamp NULL DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `t_stock_adjust_header` (`doc_id`, `doc_num`, `doc_dt`, `doc_ddt`, `doc_status`, `approval_status`, `whse_id`, `doc_note`, `create_by`, `create_dt`, `update_by`, `update_dt`, `cancel_by`, `cancel_dt`, `cancel_reason`) VALUES
(1, 'ADJ-0001', '2015-04-21 00:00:00', '2015-04-21 00:00:00', 'ACTIVE', 'pending', 1, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'ADJ-0001', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'ACTIVE', 'pending', 2, '', 'administrator', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'ADJ-0001', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'ACTIVE', 'pending', 2, '', 'administrator', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'ADJ-0001', '2015-05-28 00:00:00', '2015-05-28 00:00:00', 'ACTIVE', 'pending', 1, 'Add laptop', 'administrator', NULL, NULL, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_stock_moving_detail`;
CREATE TABLE IF NOT EXISTS `t_stock_moving_detail` (
  `doc_id` int(11) NOT NULL,
  `doc_line` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_qty` int(11) NOT NULL,
  `item_qty_closed` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `t_stock_moving_detail` (`doc_id`, `doc_line`, `item_id`, `item_qty`, `item_qty_closed`) VALUES
(5, 1, 1, 20, 20),
(5, 2, 2, 20, 20),
(6, 1, 1, 10, 10),
(7, 1, 1, 10, 10),
(7, 2, 2, 10, 10),
(8, 1, 1, 5, 5),
(8, 2, 2, 5, 5),
(9, 1, 1, 1, 0),
(9, 2, 2, 1, 0);

DROP TABLE IF EXISTS `t_stock_moving_header`;
CREATE TABLE IF NOT EXISTS `t_stock_moving_header` (
  `doc_id` int(11) NOT NULL,
  `doc_num` varchar(50) NOT NULL,
  `doc_dt` datetime NOT NULL,
  `doc_ddt` datetime NOT NULL,
  `doc_status` varchar(25) NOT NULL,
  `approval_status` enum('pending','partially approved','approved','revise','rejected') NOT NULL DEFAULT 'pending',
  `whse_from` int(11) NOT NULL,
  `whse_to` int(11) NOT NULL,
  `doc_note` varchar(1000) DEFAULT NULL,
  `receipt_note` varchar(1000) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `update_dt` datetime DEFAULT NULL,
  `cancel_by` varchar(50) DEFAULT NULL,
  `cancel_dt` datetime DEFAULT NULL,
  `cancel_reason` varchar(1000) DEFAULT NULL,
  `receipt_dt` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO `t_stock_moving_header` (`doc_id`, `doc_num`, `doc_dt`, `doc_ddt`, `doc_status`, `approval_status`, `whse_from`, `whse_to`, `doc_note`, `receipt_note`, `create_by`, `create_dt`, `update_by`, `update_dt`, `cancel_by`, `cancel_dt`, `cancel_reason`, `receipt_dt`) VALUES
(1, 'TRF-0001', '2015-04-17 00:00:00', '2015-04-17 00:00:00', 'ACTIVE', 'pending', 1, 2, '', NULL, NULL, '2015-04-17 13:04:21', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'TRF-0002', '2015-04-17 00:00:00', '2015-04-17 00:00:00', 'ACTIVE', 'pending', 1, 2, 'Transfer ke toko 03', NULL, NULL, '2015-04-17 13:04:03', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'TRF-0004', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'ACTIVE', 'pending', 1, 2, '', NULL, 'administrator', '2015-04-23 18:04:21', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'TRF-0005', '2015-04-23 00:00:00', '2015-04-23 00:00:00', 'ACTIVE', 'pending', 1, 2, '', NULL, 'administrator', '2015-04-23 18:04:55', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'TRF-0008', '2015-04-28 00:00:00', '2015-04-28 00:00:00', 'CLOSED', 'pending', 1, 2, '', 'terima barang', 'administrator', '2015-04-28 17:04:49', 'administrator', '2015-04-28 17:04:10', NULL, NULL, NULL, '2015-04-28 00:00:00'),
(6, 'TRF-0009', '2015-04-28 00:00:00', '2015-04-28 00:00:00', 'CLOSED', 'pending', 1, 1, 'kirim ke toko', 'di terima di toko 2', 'administrator', '2015-04-28 17:04:59', 'administrator', '2015-04-28 17:04:55', NULL, NULL, NULL, '2015-04-28 00:00:00'),
(7, 'TRF-0010', '2015-04-28 00:00:00', '2015-04-28 00:00:00', 'CLOSED', 'pending', 1, 2, 'pindah ke toko 2 (outlet 02)', 'penerimaan di toko 02', 'administrator', '2015-04-28 17:04:36', 'administrator', '2015-04-28 17:04:55', NULL, NULL, NULL, '2015-04-28 00:00:00'),
(8, 'TRF-0011', '2015-04-29 00:00:00', '2015-04-29 00:00:00', 'CLOSED', 'pending', 1, 2, 'Pindah ke toko 02', 'di terima di toko', 'staff', '2015-04-29 14:04:18', 'staff', '2015-04-29 14:04:52', NULL, NULL, NULL, '2015-04-29 00:00:00'),
(9, 'TRF-0012', '2015-05-07 00:00:00', '2015-05-07 00:00:00', 'ACTIVE', 'pending', 1, 2, '', NULL, 'manager', '2015-05-07 11:05:41', NULL, NULL, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `t_tax`;
CREATE TABLE IF NOT EXISTS `t_tax` (
  `id` int(11) NOT NULL,
  `tax_code` varchar(50) NOT NULL,
  `tax_name` varchar(100) NOT NULL,
  `tax_percent` decimal(10,2) NOT NULL,
  `tax_operator` varchar(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `t_tax` (`id`, `tax_code`, `tax_name`, `tax_percent`, `tax_operator`) VALUES
(1, 'NONE', 'No Tax', '0.00', '+'),
(2, 'PPN10', 'PPN 10%', '10.00', '+'),
(3, 'PPH23', 'PPh 23', '2.00', '-');

DROP TABLE IF EXISTS `t_uom`;
CREATE TABLE IF NOT EXISTS `t_uom` (
  `id` int(11) NOT NULL,
  `uom` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Unit of Measurement master';

INSERT INTO `t_uom` (`id`, `uom`) VALUES
(1, 'Pcs'),
(2, 'Unit'),
(3, 'Manhour'),
(4, 'Manday'),
(5, 'Litre'),
(6, 'Kg'),
(7, 'Tonne');

DROP TABLE IF EXISTS `t_user`;
CREATE TABLE IF NOT EXISTS `t_user` (
  `user_id` int(10) unsigned NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_pass` varchar(60) NOT NULL,
  `user_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_last_ip` varchar(45) NOT NULL,
  `user_location_id` int(11) NOT NULL DEFAULT '1',
  `position_id` int(11) NOT NULL DEFAULT '1',
  `department` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `birthdate` date NOT NULL DEFAULT '0000-00-00',
  `gender` enum('male','female') NOT NULL DEFAULT 'male',
  `designation` varchar(255) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `mobile_number` varchar(100) NOT NULL,
  `extension_number` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reset_next_login` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

INSERT INTO `t_user` (`user_id`, `user_name`, `user_email`, `user_pass`, `user_date`, `user_modified`, `user_last_login`, `user_last_ip`, `user_location_id`, `position_id`, `department`, `full_name`, `birthdate`, `gender`, `designation`, `phone_number`, `mobile_number`, `extension_number`, `timestamp`, `reset_next_login`) VALUES
(1, 'Admin', 'indra.prastha@indoskyware.com', '$2a$08$IvOQjoi0WECCv6JmGTgc1eqSCjipePbIMOceFh1WGK.TK0ALt0p6O', '2014-06-29 08:15:02', '2014-06-29 08:15:02', '2014-11-07 10:57:01', '', 1, 2, 'Management', 'Administrator', '1972-12-21', 'male', 'Admin', '', '', '', '2015-05-13 07:15:41', 0),
(2, 'operation', 'jun.martin@indoskyware.com', '$2a$08$N6GUA3RPf20YhBNr0IFIeOneKxoFk1XYE6yccIYP8.kIThEwyQZTK', '2014-06-29 14:15:02', '2014-06-29 14:15:02', '2015-04-05 14:18:35', '', 1, 2, 'Operation', 'Evita', '1986-06-09', 'female', '', '', '', '', '2015-04-05 07:18:35', 0),
(3, 'marketing', 'indra.prastha@indoskyware.com', '$2a$08$IvOQjoi0WECCv6JmGTgc1eqSCjipePbIMOceFh1WGK.TK0ALt0p6O', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2014-11-04 23:23:36', '', 1, 3, 'Sales', 'Anne', '1986-07-22', 'female', '', '', '', '', '2014-11-07 04:17:10', 1),
(4, 'finance', 'jun.martin@indoskyware.com', '$2a$08$IvOQjoi0WECCv6JmGTgc1eqSCjipePbIMOceFh1WGK.TK0ALt0p6O', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2014-10-27 12:57:52', '', 1, 4, 'Finance', 'Susanne', '1986-04-20', 'female', '', '', '', '', '2014-11-07 04:17:15', 1);

DROP TABLE IF EXISTS `t_user_group`;
CREATE TABLE IF NOT EXISTS `t_user_group` (
  `group_id` int(11) unsigned NOT NULL,
  `group_name` varchar(100) NOT NULL,
  `group_desc` varchar(8000) NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Master Group';

INSERT INTO `t_user_group` (`group_id`, `group_name`, `group_desc`) VALUES
(1, 'Admin', 'Administrator Access'),
(2, 'Manager', 'Manager Access'),
(3, 'Staff', 'Staff Access');

DROP TABLE IF EXISTS `t_user_group_junc`;
CREATE TABLE IF NOT EXISTS `t_user_group_junc` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store which user_id joins to which group_id';

INSERT INTO `t_user_group_junc` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 2),
(3, 2),
(4, 2);
DROP VIEW IF EXISTS `v_approval_detail`;
CREATE TABLE IF NOT EXISTS `v_approval_detail` (
`appr_detail_id` int(11)
,`appr_id` int(11)
,`appr_code` varchar(50)
,`appr_name` varchar(100)
,`appr_email` tinyint(1)
,`auto_approve` tinyint(1)
,`step_based` tinyint(1)
,`requestor_position_id` int(11)
,`position_code` varchar(100)
,`position_name` varchar(500)
,`approval_matrix` varchar(1000)
,`appr_model` varchar(100)
,`appr_function` varchar(100)
,`review_function` varchar(100)
);
DROP VIEW IF EXISTS `v_approval_setting`;
CREATE TABLE IF NOT EXISTS `v_approval_setting` (
`appr_setting_id` int(11)
,`appr_id` int(11)
,`appr_code` varchar(50)
,`appr_name` varchar(100)
,`appr_email` tinyint(1)
,`appr_bypass` tinyint(1)
,`requestor_user_id` int(11)
,`requestor_user_name` varchar(255)
,`approver_user_id1` int(11)
,`approver_user_name1` varchar(255)
,`approver_user_email1` varchar(255)
,`approver_user_id2` int(11)
,`approver_user_name2` varchar(255)
,`approver_user_email2` varchar(255)
,`approver_user_id3` int(11)
,`approver_user_name3` varchar(255)
,`approver_user_email3` varchar(255)
,`approver_user_id4` int(11)
,`approver_user_name4` varchar(255)
,`approver_user_email4` varchar(255)
,`approver_user_id5` int(11)
,`approver_user_name5` varchar(255)
,`approver_user_email5` varchar(255)
);
DROP VIEW IF EXISTS `v_asset_datatable`;
CREATE TABLE IF NOT EXISTS `v_asset_datatable` (
`asset_id` int(11) unsigned
,`asset_label` varchar(20)
,`asset_name` varchar(255)
,`asset_group_id` int(11)
,`asset_location_id` int(11)
,`asset_status` enum('incoming','active','lost','sold','written off','in transit','pending','disposed')
,`asset_cost` decimal(20,2)
,`location_id` int(11) unsigned
,`location_name` varchar(255)
,`group_id` int(11) unsigned
,`group_name` varchar(255)
);
DROP VIEW IF EXISTS `v_asset_history`;
CREATE TABLE IF NOT EXISTS `v_asset_history` (
`asset_id` int(11) unsigned
,`asset_name` varchar(255)
,`asset_label` varchar(20)
,`document_ref` varchar(20)
,`transaction_type` enum('move','disposal','depreciation')
,`memo` text
,`document_status` enum('pending','partially approved','approved','revise','rejected')
,`transaction_date` date
,`timestamp` timestamp
,`applied_status` enum('pending','in transit','received','disposed')
,`full_name` varchar(255)
);
DROP VIEW IF EXISTS `v_asset_request`;
CREATE TABLE IF NOT EXISTS `v_asset_request` (
`request_id` int(11)
,`document_ref` varchar(20)
,`location_id` int(11)
,`requested_by` varchar(255)
,`requested_for` varchar(255)
,`date_from` date
,`date_to` date
,`note` text
,`request_status` enum('pending','partially approved','approved','revise','rejected')
,`asset_id` int(11) unsigned
,`asset_label` varchar(20)
,`asset_name` varchar(255)
,`user_name` varchar(255)
,`full_name` varchar(255)
);
DROP VIEW IF EXISTS `v_asset_trans`;
CREATE TABLE IF NOT EXISTS `v_asset_trans` (
`request_id` int(11)
,`document_ref` varchar(20)
,`location_id` int(11)
,`requested_by` varchar(255)
,`transaction_type` enum('move','disposal','depreciation')
,`note` text
,`request_status` enum('pending','partially approved','approved','revise','rejected')
,`transaction_date` date
,`timestamp` timestamp
,`status` enum('pending','in transit','received','disposed')
,`asset_id` int(11) unsigned
,`asset_name` varchar(255)
,`asset_label` varchar(20)
,`user_id` int(10) unsigned
,`user_name` varchar(255)
,`full_name` varchar(255)
);
DROP VIEW IF EXISTS `v_asset_trans_datatable`;
CREATE TABLE IF NOT EXISTS `v_asset_trans_datatable` (
`request_id` int(11)
,`document_ref` varchar(20)
,`location_id` int(11)
,`requested_by` varchar(255)
,`transaction_type` enum('move','disposal','depreciation')
,`note` text
,`new_location_id` int(10) unsigned
,`request_status` enum('pending','partially approved','approved','revise','rejected')
,`transaction_date` date
,`timestamp` timestamp
,`user_id` int(10) unsigned
,`user_name` varchar(255)
,`full_name` varchar(255)
,`new_location_name` varchar(255)
,`location_name` varchar(255)
);
DROP VIEW IF EXISTS `v_asset_trans_history`;
CREATE TABLE IF NOT EXISTS `v_asset_trans_history` (
`asset_id` int(11) unsigned
,`asset_label` varchar(20)
,`asset_name` varchar(255)
,`group_id` int(11) unsigned
,`group_name` varchar(255)
,`group_parent_id` int(11)
,`trans_type` varchar(8)
,`trans_date` date
,`trans_doc` varchar(20)
,`trans_user` varchar(255)
,`trans_status` varchar(18)
,`trans_memo` text
,`trans_detail` text
);
DROP VIEW IF EXISTS `v_daily_asset_reminder`;
CREATE TABLE IF NOT EXISTS `v_daily_asset_reminder` (
`remind_type` varchar(19)
,`asset_id` int(11) unsigned
,`asset_label` varchar(20)
,`asset_name` varchar(255)
,`asset_location_id` int(11)
,`location_name` varchar(255)
,`user_id` bigint(11)
,`user_name` varchar(255)
,`remind_notes` text
);
DROP VIEW IF EXISTS `v_document_approval`;
CREATE TABLE IF NOT EXISTS `v_document_approval` (
`doc_approval_id` int(11)
,`appr_id` int(11)
,`appr_code` varchar(50)
,`appr_name` varchar(100)
,`appr_email` tinyint(1)
,`appr_detail_id` int(11)
,`doc_number` varchar(255)
,`requestor_user_id` int(11)
,`requestor_position_id` int(11)
,`requestor_full_name` varchar(511)
,`requestor_email` varchar(100)
,`request_datetime` datetime
,`approver_position_id` int(11)
,`approver_user_id` int(11)
,`approver_full_name` varchar(511)
,`approver_email` varchar(100)
,`approver_action` smallint(6)
,`action_datetime` datetime
,`unique_key` varchar(32)
,`appr_model` varchar(100)
,`appr_function` varchar(100)
,`review_function` varchar(100)
,`step_based` tinyint(1)
,`approver_step` int(11)
);
DROP VIEW IF EXISTS `v_employees`;
CREATE TABLE IF NOT EXISTS `v_employees` (
`emp_id` int(11)
,`emp_no` varchar(45)
,`first_name` varchar(255)
,`last_name` varchar(255)
,`full_name` varchar(511)
,`location_id` int(11)
,`location_name` varchar(255)
,`position_id` varchar(45)
,`position_name` varchar(500)
,`user_id` int(11) unsigned
,`username` varchar(100)
,`email` varchar(100)
);
DROP VIEW IF EXISTS `v_inventory`;
CREATE TABLE IF NOT EXISTS `v_inventory` (
`item_id` int(11)
,`item_g_id` int(11)
,`item_code` varchar(50)
,`item_name` varchar(100)
,`item_tag` varchar(500)
,`item_category` varchar(50)
,`item_category_char` varchar(5)
,`item_style` varchar(50)
,`item_style_char` varchar(5)
,`item_style_launch` datetime
,`item_manufacturer` varchar(50)
,`item_manufacturer_char` varchar(5)
,`item_color` varchar(50)
,`item_color_char` varchar(20)
,`item_image` varchar(100)
,`item_inventory` tinyint(1)
,`item_sales` tinyint(1)
,`item_purchase` tinyint(1)
,`qty_onhand` decimal(10,2)
,`qty_tocome` decimal(10,2)
,`qty_togo` decimal(10,2)
,`qty_minlevel` decimal(10,2)
,`qty_maxlevel` decimal(10,2)
,`unit_cost` decimal(10,2)
,`uom_inventory` int(11)
,`uom_sales` int(11)
,`uom_purchase` int(11)
,`unit_barcode` varchar(20)
,`item_brand` varchar(50)
,`item_valuation` varchar(20)
,`item_mrp` varchar(5)
,`item_leadtime` int(11)
,`item_prod_method` varchar(10)
,`item_sell_price` decimal(10,2)
,`item_buy_price` decimal(10,2)
,`item_status` varchar(50)
,`qty_avail` decimal(12,2)
);
DROP VIEW IF EXISTS `v_itembalance`;
CREATE TABLE IF NOT EXISTS `v_itembalance` (
`whse_id` int(11)
,`whse_code` varchar(20)
,`whse_name` varchar(255)
,`category` varchar(255)
,`item_id` int(11)
,`item_code` varchar(50)
,`item_name` varchar(100)
,`item_qty` decimal(32,2)
);
DROP VIEW IF EXISTS `v_itemhistory`;
CREATE TABLE IF NOT EXISTS `v_itemhistory` (
`doc_type` varchar(250)
,`doc_num` varchar(50)
,`item_id` int(11)
,`system_date` timestamp
,`doc_date` datetime
,`item_code` varchar(50)
,`item_name` varchar(100)
,`item_qty` decimal(10,2)
,`item_value` decimal(10,2)
,`whse_id` int(11)
,`whse_code` varchar(20)
,`whse_name` varchar(255)
,`Category` varchar(255)
);
DROP VIEW IF EXISTS `v_itemincoming`;
CREATE TABLE IF NOT EXISTS `v_itemincoming` (
`doc_id` int(11)
,`whse_id` int(11) unsigned
,`whse_code` varchar(20)
,`whse_name` varchar(255)
,`group_name` varchar(255)
,`item_id` int(11)
,`item_code` varchar(50)
,`item_name` varchar(100)
,`qty_tocome` decimal(33,2)
);
DROP VIEW IF EXISTS `v_itemoutgoing`;
CREATE TABLE IF NOT EXISTS `v_itemoutgoing` (
`doc_id` int(11)
,`whse_id` int(11) unsigned
,`whse_code` varchar(20)
,`whse_name` varchar(255)
,`group_name` varchar(255)
,`item_id` int(11)
,`item_code` varchar(50)
,`item_name` varchar(100)
,`qty_togo` decimal(33,2)
);
DROP VIEW IF EXISTS `v_journals`;
CREATE TABLE IF NOT EXISTS `v_journals` (
`journal_id` int(11)
,`journal_code` varchar(50)
,`posting_date` datetime
,`journal_memo` varchar(500)
,`reff` varchar(50)
,`journal_type` varchar(20)
,`account_id` int(11)
,`account_number` varchar(25)
,`account_name` varchar(250)
,`journal_curr` varchar(10)
,`journal_dr` decimal(18,4)
,`journal_cr` decimal(18,4)
);
DROP VIEW IF EXISTS `v_position`;
CREATE TABLE IF NOT EXISTS `v_position` (
`position_id` int(11)
,`position_code` varchar(100)
,`position_name` varchar(500)
,`position_desc` varchar(2000)
,`position_type` enum('division','position')
,`parent_id` int(11)
,`last_node` int(11)
,`order_no` int(11)
,`division_id` int(11)
,`division_code` varchar(100)
,`division_name` varchar(500)
,`child_count` bigint(21)
,`user_count` bigint(21)
);
DROP VIEW IF EXISTS `v_pos_transactions`;
CREATE TABLE IF NOT EXISTS `v_pos_transactions` (
`location_id` int(11) unsigned
,`location_code` varchar(20)
,`location_name` varchar(255)
,`cashier_id` int(11)
,`cashier_code` varchar(255)
,`emp_no` varchar(45)
,`input_by` varchar(101)
,`input_date` datetime
,`sales_code` varchar(50)
,`sales_amount` decimal(18,2)
,`posted` bit(1)
);
DROP VIEW IF EXISTS `v_pos_transactions_items`;
CREATE TABLE IF NOT EXISTS `v_pos_transactions_items` (
`id` int(11)
,`trx_id` int(11)
,`trx_code` varchar(50)
,`input_datetime` datetime
,`pos_date` date
,`cashier_id` int(11)
,`cashier_code` varchar(255)
,`location_id` int(11) unsigned
,`location_code` varchar(20)
,`location_name` varchar(255)
,`line_id` int(11)
,`item_id` int(11)
,`item_code` varchar(50)
,`item_name` varchar(100)
,`item_qty` decimal(10,2)
,`move_item_qty` decimal(11,2)
,`item_price` decimal(18,2)
,`item_total_price` decimal(18,2)
,`include_tax` bit(1)
,`tax_value` decimal(18,2)
,`sales_value` decimal(18,2)
);
DROP VIEW IF EXISTS `v_purchase_order`;
CREATE TABLE IF NOT EXISTS `v_purchase_order` (
`doc_num` varchar(50)
,`doc_dt` datetime
,`doc_ddt` datetime
,`doc_status` varchar(25)
,`approval_status` enum('PENDING','PARTIALLY APPROVED','APPROVED','REVISE','REJECTED')
,`doc_total` decimal(10,2)
,`buss_code` varchar(50)
,`buss_name` varchar(100)
,`buss_addr` varchar(500)
,`buss_contact` varchar(50)
,`location_code` varchar(20)
,`location_name` varchar(255)
,`location_address` varchar(1000)
);
DROP VIEW IF EXISTS `v_purchase_order_detail`;
CREATE TABLE IF NOT EXISTS `v_purchase_order_detail` (
`doc_num` varchar(50)
,`doc_dt` datetime
,`doc_ddt` datetime
,`doc_status` varchar(25)
,`approval_status` enum('PENDING','PARTIALLY APPROVED','APPROVED','REVISE','REJECTED')
,`doc_total` decimal(10,2)
,`item_qty` decimal(10,2)
,`item_qty_closed` decimal(10,2)
,`item_price` decimal(10,2)
,`item_netprice` decimal(10,2)
,`item_total` decimal(10,2)
,`line_close` tinyint(4)
,`buss_code` varchar(50)
,`buss_name` varchar(100)
,`buss_addr` varchar(500)
,`buss_contact` varchar(50)
,`location_code` varchar(20)
,`location_name` varchar(255)
,`location_address` varchar(1000)
,`item_code` varchar(50)
,`item_name` varchar(100)
);
DROP VIEW IF EXISTS `v_sales_order`;
CREATE TABLE IF NOT EXISTS `v_sales_order` (
`doc_num` varchar(50)
,`doc_dt` datetime
,`doc_ddt` datetime
,`doc_status` varchar(25)
,`approval_status` enum('pending','partially approved','approved','revise','rejected')
,`doc_total` decimal(10,0)
,`buss_code` varchar(50)
,`buss_name` varchar(100)
,`buss_addr` varchar(500)
,`buss_contact` varchar(50)
,`location_code` varchar(20)
,`location_name` varchar(255)
,`location_address` varchar(1000)
);
DROP VIEW IF EXISTS `v_sales_order_detail`;
CREATE TABLE IF NOT EXISTS `v_sales_order_detail` (
`doc_num` varchar(50)
,`doc_dt` datetime
,`doc_ddt` datetime
,`doc_status` varchar(25)
,`approval_status` enum('pending','partially approved','approved','revise','rejected')
,`doc_total` decimal(10,0)
,`item_qty` decimal(10,2)
,`item_qty_closed` decimal(10,2) unsigned
,`item_price` decimal(10,2)
,`item_netprice` decimal(10,2)
,`item_total` decimal(10,2)
,`line_close` tinyint(4)
,`buss_code` varchar(50)
,`buss_name` varchar(100)
,`buss_addr` varchar(500)
,`buss_contact` varchar(50)
,`location_code` varchar(20)
,`location_name` varchar(255)
,`location_address` varchar(1000)
,`item_code` varchar(50)
,`item_name` varchar(100)
);
DROP VIEW IF EXISTS `v_users`;
CREATE TABLE IF NOT EXISTS `v_users` (
`emp_id` int(11)
,`emp_no` varchar(45)
,`first_name` varchar(255)
,`last_name` varchar(255)
,`full_name` varchar(511)
,`location_id` int(11)
,`location_name` varchar(255)
,`position_id` varchar(45)
,`position_name` varchar(500)
,`user_id` int(11) unsigned
,`username` varchar(100)
,`email` varchar(100)
);
DROP VIEW IF EXISTS `v_user_datatable`;
CREATE TABLE IF NOT EXISTS `v_user_datatable` (
`user_id` int(10) unsigned
,`user_name` varchar(255)
,`user_email` varchar(255)
,`user_pass` varchar(60)
,`user_date` datetime
,`user_modified` datetime
,`user_last_login` datetime
,`user_last_ip` varchar(45)
,`user_location_id` int(11)
,`department` varchar(255)
,`full_name` varchar(255)
,`birthdate` date
,`gender` enum('male','female')
,`position_code` varchar(100)
,`position_name` varchar(500)
,`designation` varchar(255)
,`phone_number` varchar(100)
,`mobile_number` varchar(100)
,`extension_number` varchar(100)
,`timestamp` timestamp
,`location_id` int(11) unsigned
,`location_name` varchar(255)
,`location_address` varchar(1000)
,`location_phone` varchar(50)
,`location_city` varchar(255)
,`location_state` varchar(255)
,`location_country` varchar(255)
,`location_description` text
);
DROP TABLE IF EXISTS `v_approval_detail`;

CREATE VIEW `v_approval_detail` AS select `appd`.`appr_detail_id` AS `appr_detail_id`,`appd`.`appr_id` AS `appr_id`,`app`.`appr_code` AS `appr_code`,`app`.`appr_name` AS `appr_name`,`app`.`appr_email` AS `appr_email`,`appd`.`auto_approve` AS `auto_approve`,`appd`.`step_based` AS `step_based`,`appd`.`requestor_position_id` AS `requestor_position_id`,`pos`.`position_code` AS `position_code`,`pos`.`position_name` AS `position_name`,`appd`.`approval_matrix` AS `approval_matrix`,`app`.`appr_model` AS `appr_model`,`app`.`appr_function` AS `appr_function`,`app`.`review_function` AS `review_function` from ((`t_approval_detail` `appd` join `t_approval` `app` on((`app`.`appr_id` = `appd`.`appr_id`))) left join `t_position` `pos` on((`pos`.`position_id` = `appd`.`requestor_position_id`)));
DROP TABLE IF EXISTS `v_approval_setting`;

CREATE VIEW `v_approval_setting` AS select `appd`.`appr_setting_id` AS `appr_setting_id`,`app`.`appr_id` AS `appr_id`,`app`.`appr_code` AS `appr_code`,`app`.`appr_name` AS `appr_name`,`app`.`appr_email` AS `appr_email`,`app`.`appr_bypass` AS `appr_bypass`,`appd`.`requestor_user_id` AS `requestor_user_id`,`r`.`user_name` AS `requestor_user_name`,`appd`.`approver_user_id1` AS `approver_user_id1`,`a1`.`full_name` AS `approver_user_name1`,`a1`.`user_email` AS `approver_user_email1`,`appd`.`approver_user_id2` AS `approver_user_id2`,`a2`.`full_name` AS `approver_user_name2`,`a2`.`user_email` AS `approver_user_email2`,`appd`.`approver_user_id3` AS `approver_user_id3`,`a3`.`full_name` AS `approver_user_name3`,`a3`.`user_email` AS `approver_user_email3`,`appd`.`approver_user_id4` AS `approver_user_id4`,`a4`.`full_name` AS `approver_user_name4`,`a4`.`user_email` AS `approver_user_email4`,`appd`.`approver_user_id5` AS `approver_user_id5`,`a5`.`full_name` AS `approver_user_name5`,`a5`.`user_email` AS `approver_user_email5` from (((((((`t_approval` `app` left join `t_approval_setting` `appd` on((`appd`.`appr_id` = `app`.`appr_id`))) left join `t_user` `r` on((`r`.`user_id` = `appd`.`requestor_user_id`))) left join `t_user` `a1` on((`a1`.`user_id` = `appd`.`approver_user_id1`))) left join `t_user` `a2` on((`a2`.`user_id` = `appd`.`approver_user_id2`))) left join `t_user` `a3` on((`a3`.`user_id` = `appd`.`approver_user_id3`))) left join `t_user` `a4` on((`a4`.`user_id` = `appd`.`approver_user_id4`))) left join `t_user` `a5` on((`a5`.`user_id` = `appd`.`approver_user_id5`)));
DROP TABLE IF EXISTS `v_asset_datatable`;

CREATE VIEW `v_asset_datatable` AS select `a`.`asset_id` AS `asset_id`,`a`.`asset_label` AS `asset_label`,`a`.`asset_name` AS `asset_name`,`a`.`asset_group_id` AS `asset_group_id`,`a`.`asset_location_id` AS `asset_location_id`,`a`.`asset_status` AS `asset_status`,`a`.`asset_cost` AS `asset_cost`,`b`.`location_id` AS `location_id`,`b`.`location_name` AS `location_name`,`c`.`group_id` AS `group_id`,`c`.`group_name` AS `group_name` from ((`t_asset` `a` left join `t_site_location` `b` on((`a`.`asset_location_id` = `b`.`location_id`))) left join `t_asset_group` `c` on((`a`.`asset_group_id` = `c`.`group_id`)));
DROP TABLE IF EXISTS `v_asset_history`;

CREATE VIEW `v_asset_history` AS select `trans`.`asset_id` AS `asset_id`,`trans`.`asset_name` AS `asset_name`,`trans`.`asset_label` AS `asset_label`,`trans`.`document_ref` AS `document_ref`,`trans`.`transaction_type` AS `transaction_type`,`trans`.`note` AS `memo`,`trans`.`request_status` AS `document_status`,`trans`.`transaction_date` AS `transaction_date`,`trans`.`timestamp` AS `timestamp`,`trans`.`status` AS `applied_status`,`trans`.`full_name` AS `full_name` from `v_asset_trans` `trans` order by `trans`.`asset_name`,`trans`.`transaction_date` desc;
DROP TABLE IF EXISTS `v_asset_request`;

CREATE VIEW `v_asset_request` AS select `a`.`request_id` AS `request_id`,`a`.`document_ref` AS `document_ref`,`a`.`location_id` AS `location_id`,`a`.`requested_by` AS `requested_by`,`a`.`requested_for` AS `requested_for`,`a`.`date_from` AS `date_from`,`a`.`date_to` AS `date_to`,`a`.`note` AS `note`,`a`.`request_status` AS `request_status`,`c`.`asset_id` AS `asset_id`,`c`.`asset_label` AS `asset_label`,`c`.`asset_name` AS `asset_name`,`d`.`user_name` AS `user_name`,`d`.`full_name` AS `full_name` from (((`t_asset_request_detail` `b` left join `t_asset_request_header` `a` on((`a`.`request_id` = `b`.`request_id`))) left join `t_asset` `c` on((`b`.`asset_id` = `c`.`asset_id`))) left join `t_user` `d` on((`a`.`requested_by` = `d`.`user_id`)));
DROP TABLE IF EXISTS `v_asset_trans`;

CREATE VIEW `v_asset_trans` AS select `a`.`request_id` AS `request_id`,`a`.`document_ref` AS `document_ref`,`a`.`location_id` AS `location_id`,`a`.`requested_by` AS `requested_by`,`a`.`transaction_type` AS `transaction_type`,`a`.`note` AS `note`,`a`.`request_status` AS `request_status`,`a`.`transaction_date` AS `transaction_date`,`a`.`timestamp` AS `timestamp`,`b`.`status` AS `status`,`c`.`asset_id` AS `asset_id`,`c`.`asset_name` AS `asset_name`,`c`.`asset_label` AS `asset_label`,`d`.`user_id` AS `user_id`,`d`.`user_name` AS `user_name`,`d`.`full_name` AS `full_name` from (((`t_asset_trans_detail` `b` left join `t_asset_trans` `a` on((`a`.`request_id` = `b`.`request_id`))) left join `t_asset` `c` on((`c`.`asset_id` = `b`.`asset_id`))) left join `t_user` `d` on((`d`.`user_id` = `a`.`requested_by`)));
DROP TABLE IF EXISTS `v_asset_trans_datatable`;

CREATE VIEW `v_asset_trans_datatable` AS select `a`.`request_id` AS `request_id`,`a`.`document_ref` AS `document_ref`,`a`.`location_id` AS `location_id`,`a`.`requested_by` AS `requested_by`,`a`.`transaction_type` AS `transaction_type`,`a`.`note` AS `note`,`a`.`new_location_id` AS `new_location_id`,`a`.`request_status` AS `request_status`,`a`.`transaction_date` AS `transaction_date`,`a`.`timestamp` AS `timestamp`,`d`.`user_id` AS `user_id`,`d`.`user_name` AS `user_name`,`d`.`full_name` AS `full_name`,`e`.`location_name` AS `new_location_name`,`f`.`location_name` AS `location_name` from (((`t_asset_trans` `a` left join `t_user` `d` on((`d`.`user_id` = `a`.`requested_by`))) left join `t_site_location` `e` on((`a`.`new_location_id` = `e`.`location_id`))) left join `t_site_location` `f` on((`a`.`location_id` = `f`.`location_id`)));
DROP TABLE IF EXISTS `v_asset_trans_history`;

CREATE VIEW `v_asset_trans_history` AS select `a`.`asset_id` AS `asset_id`,`a`.`asset_label` AS `asset_label`,`a`.`asset_name` AS `asset_name`,`ag`.`group_id` AS `group_id`,`ag`.`group_name` AS `group_name`,`ag`.`group_parent_id` AS `group_parent_id`,'Acquired' AS `trans_type`,`a`.`date_acquired` AS `trans_date`,'' AS `trans_doc`,0 AS `trans_user`,'approved' AS `trans_status`,'' AS `trans_memo`,'' AS `trans_detail` from (`t_asset` `a` left join `t_asset_group` `ag` on((`ag`.`group_id` = `a`.`asset_group_id`))) union select `atrd`.`asset_id` AS `asset_id`,`a`.`asset_label` AS `asset_label`,`a`.`asset_name` AS `asset_name`,`ag`.`group_id` AS `group_id`,`ag`.`group_name` AS `group_name`,`ag`.`group_parent_id` AS `group_parent_id`,'Moved' AS `trans_type`,`atr`.`transaction_date` AS `trans_date`,`atr`.`document_ref` AS `trans_doc`,`atr`.`requested_by` AS `trans_user`,`atr`.`request_status` AS `trans_status`,`atr`.`note` AS `trans_memo`,concat('Moved from ',`from_location`.`location_name`,' to ',`to_location`.`location_name`) AS `trans_detail` from (((((`t_asset_trans_detail` `atrd` join `t_asset_trans` `atr` on((`atr`.`request_id` = `atrd`.`request_id`))) left join `t_asset` `a` on((`a`.`asset_id` = `atrd`.`asset_id`))) left join `t_asset_group` `ag` on((`ag`.`group_id` = `a`.`asset_group_id`))) left join `t_site_location` `from_location` on((`from_location`.`location_id` = `atr`.`location_id`))) left join `t_site_location` `to_location` on((`to_location`.`location_id` = `atr`.`new_location_id`))) where (`atr`.`transaction_type` = 'move') union select `atrd`.`asset_id` AS `asset_id`,`a`.`asset_label` AS `asset_label`,`a`.`asset_name` AS `asset_name`,`ag`.`group_id` AS `group_id`,`ag`.`group_name` AS `group_name`,`ag`.`group_parent_id` AS `group_parent_id`,'Disposed' AS `trans_type`,`atr`.`transaction_date` AS `trans_date`,`atr`.`document_ref` AS `trans_doc`,`atr`.`requested_by` AS `trans_user`,`atr`.`request_status` AS `trans_status`,`atr`.`note` AS `trans_memo`,concat('Disposed from ',`location`.`location_name`) AS `trans_detail` from ((((`t_asset_trans_detail` `atrd` join `t_asset_trans` `atr` on((`atr`.`request_id` = `atrd`.`request_id`))) left join `t_asset` `a` on((`a`.`asset_id` = `atrd`.`asset_id`))) left join `t_asset_group` `ag` on((`ag`.`group_id` = `a`.`asset_group_id`))) left join `t_site_location` `location` on((`location`.`location_id` = `atr`.`location_id`))) where (`atr`.`transaction_type` = 'disposal') union select `ard`.`asset_id` AS `asset_id`,`a`.`asset_label` AS `asset_label`,`a`.`asset_name` AS `asset_name`,`ag`.`group_id` AS `group_id`,`ag`.`group_name` AS `group_name`,`ag`.`group_parent_id` AS `group_parent_id`,'Assigned' AS `trans_type`,`arh`.`date_from` AS `trans_date`,`arh`.`document_ref` AS `trans_doc`,`arh`.`requested_by` AS `trans_user`,`arh`.`request_status` AS `trans_status`,`arh`.`note` AS `trans_memo`,concat('Assigned to ',`u`.`full_name`) AS `trans_detail` from ((((`t_asset_request_detail` `ard` join `t_asset_request_header` `arh` on((`arh`.`request_id` = `ard`.`request_id`))) left join `t_asset` `a` on((`a`.`asset_id` = `ard`.`asset_id`))) left join `t_asset_group` `ag` on((`ag`.`group_id` = `a`.`asset_group_id`))) left join `t_user` `u` on((`u`.`user_id` = `arh`.`requested_by`))) order by `asset_id`;
DROP TABLE IF EXISTS `v_daily_asset_reminder`;

CREATE VIEW `v_daily_asset_reminder` AS select 'Warranty Expiration' AS `remind_type`,`a`.`asset_id` AS `asset_id`,`a`.`asset_label` AS `asset_label`,`a`.`asset_name` AS `asset_name`,`a`.`asset_location_id` AS `asset_location_id`,`loc`.`location_name` AS `location_name`,0 AS `user_id`,'Everyone' AS `user_name`,concat(`a`.`asset_name`,' (',`a`.`asset_label`,') warranty is expiring today') AS `remind_notes` from (`t_asset` `a` left join `t_site_location` `loc` on((`loc`.`location_id` = `a`.`asset_location_id`))) where (`a`.`warranty_due_date` = curdate()) union select 'Maintenance Due' AS `remind_type`,`a`.`asset_id` AS `asset_id`,`a`.`asset_label` AS `asset_label`,`a`.`asset_name` AS `asset_name`,`a`.`asset_location_id` AS `asset_location_id`,`loc`.`location_name` AS `location_name`,`mnt`.`schedule_pic` AS `user_id`,`usr`.`full_name` AS `user_name`,concat(`a`.`asset_name`,' (',`a`.`asset_label`,') maintenance (',`mtype`.`maint_type_name`,') is due today') AS `remind_notes` from (((((`t_asset` `a` join `t_maint_schedule_detail` `mntd` on((`mntd`.`asset_id` = `a`.`asset_id`))) join `t_maint_schedule` `mnt` on((`mnt`.`schedule_id` = `mntd`.`schedule_id`))) join `t_maint_type` `mtype` on((`mtype`.`maint_type_id` = `mntd`.`maint_type_id`))) left join `t_user` `usr` on((`usr`.`user_id` = `mnt`.`schedule_pic`))) left join `t_site_location` `loc` on((`loc`.`location_id` = `a`.`asset_location_id`))) where (`mntd`.`scheduled_date` = curdate());
DROP TABLE IF EXISTS `v_document_approval`;

CREATE VIEW `v_document_approval` AS select `appd`.`doc_approval_id` AS `doc_approval_id`,`app`.`appr_id` AS `appr_id`,`app`.`appr_code` AS `appr_code`,`app`.`appr_name` AS `appr_name`,`app`.`appr_email` AS `appr_email`,`appd`.`appr_detail_id` AS `appr_detail_id`,`appd`.`doc_number` AS `doc_number`,`appd`.`requestor_user_id` AS `requestor_user_id`,`appd`.`requestor_position_id` AS `requestor_position_id`,`requestor`.`full_name` AS `requestor_full_name`,`requestor`.`email` AS `requestor_email`,`appd`.`request_datetime` AS `request_datetime`,`appd`.`approver_position_id` AS `approver_position_id`,`appd`.`approver_user_id` AS `approver_user_id`,`approver`.`full_name` AS `approver_full_name`,`approver`.`email` AS `approver_email`,`appd`.`approver_action` AS `approver_action`,`appd`.`action_datetime` AS `action_datetime`,`appd`.`unique_key` AS `unique_key`,`app`.`appr_model` AS `appr_model`,`app`.`appr_function` AS `appr_function`,`app`.`review_function` AS `review_function`,`apps`.`step_based` AS `step_based`,`appd`.`approver_step` AS `approver_step` from ((((`t_document_approval` `appd` join `t_approval_detail` `apps` on((`apps`.`appr_detail_id` = `appd`.`appr_detail_id`))) join `t_approval` `app` on((`app`.`appr_id` = `apps`.`appr_id`))) left join `v_users` `requestor` on((`requestor`.`user_id` = `appd`.`requestor_user_id`))) left join `v_users` `approver` on((`approver`.`user_id` = `appd`.`approver_user_id`)));
DROP TABLE IF EXISTS `v_employees`;

CREATE VIEW `v_employees` AS select `emp`.`id` AS `emp_id`,`emp`.`emp_no` AS `emp_no`,`emp`.`first_name` AS `first_name`,`emp`.`last_name` AS `last_name`,concat(`emp`.`first_name`,' ',`emp`.`last_name`) AS `full_name`,`emp`.`location_id` AS `location_id`,`loc`.`location_name` AS `location_name`,`emp`.`position_id` AS `position_id`,`pos`.`position_name` AS `position_name`,`usr`.`id` AS `user_id`,`usr`.`username` AS `username`,`usr`.`email` AS `email` from (((`t_employee` `emp` left join `t_ion_users` `usr` on((`usr`.`id` = `emp`.`user_id`))) left join `t_position` `pos` on((`pos`.`position_id` = `emp`.`position_id`))) left join `t_site_location` `loc` on((`loc`.`location_id` = `emp`.`location_id`)));
DROP TABLE IF EXISTS `v_inventory`;

CREATE VIEW `v_inventory` AS select `t_inventory`.`item_id` AS `item_id`,`t_inventory`.`item_g_id` AS `item_g_id`,`t_inventory`.`item_code` AS `item_code`,`t_inventory`.`item_name` AS `item_name`,`t_inventory`.`item_tag` AS `item_tag`,`t_inventory`.`item_category` AS `item_category`,`t_inventory`.`item_category_char` AS `item_category_char`,`t_inventory`.`item_style` AS `item_style`,`t_inventory`.`item_style_char` AS `item_style_char`,`t_inventory`.`item_style_launch` AS `item_style_launch`,`t_inventory`.`item_manufacturer` AS `item_manufacturer`,`t_inventory`.`item_manufacturer_char` AS `item_manufacturer_char`,`t_inventory`.`item_color` AS `item_color`,`t_inventory`.`item_color_char` AS `item_color_char`,`t_inventory`.`item_image` AS `item_image`,`t_inventory`.`item_inventory` AS `item_inventory`,`t_inventory`.`item_sales` AS `item_sales`,`t_inventory`.`item_purchase` AS `item_purchase`,`t_inventory`.`qty_onhand` AS `qty_onhand`,`t_inventory`.`qty_tocome` AS `qty_tocome`,`t_inventory`.`qty_togo` AS `qty_togo`,`t_inventory`.`qty_minlevel` AS `qty_minlevel`,`t_inventory`.`qty_maxlevel` AS `qty_maxlevel`,`t_inventory`.`unit_cost` AS `unit_cost`,`t_inventory`.`uom_inventory` AS `uom_inventory`,`t_inventory`.`uom_sales` AS `uom_sales`,`t_inventory`.`uom_purchase` AS `uom_purchase`,`t_inventory`.`unit_barcode` AS `unit_barcode`,`t_inventory`.`item_brand` AS `item_brand`,`t_inventory`.`item_valuation` AS `item_valuation`,`t_inventory`.`item_mrp` AS `item_mrp`,`t_inventory`.`item_leadtime` AS `item_leadtime`,`t_inventory`.`item_prod_method` AS `item_prod_method`,`t_inventory`.`item_sell_price` AS `item_sell_price`,`t_inventory`.`item_buy_price` AS `item_buy_price`,`t_inventory`.`item_status` AS `item_status`,((`t_inventory`.`qty_onhand` + `t_inventory`.`qty_tocome`) - `t_inventory`.`qty_togo`) AS `qty_avail` from `t_inventory`;
DROP TABLE IF EXISTS `v_itembalance`;

CREATE VIEW `v_itembalance` AS select `v_itemhistory`.`whse_id` AS `whse_id`,`v_itemhistory`.`whse_code` AS `whse_code`,`v_itemhistory`.`whse_name` AS `whse_name`,`v_itemhistory`.`Category` AS `category`,`v_itemhistory`.`item_id` AS `item_id`,`v_itemhistory`.`item_code` AS `item_code`,`v_itemhistory`.`item_name` AS `item_name`,sum(`v_itemhistory`.`item_qty`) AS `item_qty` from `v_itemhistory` group by `v_itemhistory`.`whse_id`,`v_itemhistory`.`whse_code`,`v_itemhistory`.`whse_name`,`v_itemhistory`.`Category`,`v_itemhistory`.`item_code`,`v_itemhistory`.`item_name`;
DROP TABLE IF EXISTS `v_itemhistory`;

CREATE VIEW `v_itemhistory` AS select `trx`.`doc_type` AS `doc_type`,`trx`.`doc_num` AS `doc_num`,`trx`.`item_id` AS `item_id`,`trx`.`timestamp` AS `system_date`,`trx`.`doc_date` AS `doc_date`,`item`.`item_code` AS `item_code`,`item`.`item_name` AS `item_name`,`trx`.`item_qty` AS `item_qty`,`trx`.`item_value` AS `item_value`,`trx`.`whse_id` AS `whse_id`,`wh`.`location_code` AS `whse_code`,`wh`.`location_name` AS `whse_name`,`itemgroup`.`group_name` AS `Category` from (((`t_inventory_transaction` `trx` join `t_inventory` `item` on((`item`.`item_id` = `trx`.`item_id`))) join `t_site_location` `wh` on((`wh`.`location_id` = `trx`.`whse_id`))) join `t_inventory_group` `itemgroup` on((`itemgroup`.`item_g_id` = `item`.`item_g_id`))) order by `trx`.`doc_date` desc,`trx`.`doc_num` desc;
DROP TABLE IF EXISTS `v_itemincoming`;

CREATE VIEW `v_itemincoming` AS select `t0`.`doc_id` AS `doc_id`,`t4`.`location_id` AS `whse_id`,`t4`.`location_code` AS `whse_code`,`t4`.`location_name` AS `whse_name`,`t3`.`group_name` AS `group_name`,`t2`.`item_id` AS `item_id`,`t2`.`item_code` AS `item_code`,`t2`.`item_name` AS `item_name`,sum((`t1`.`item_qty` - `t1`.`item_qty_closed`)) AS `qty_tocome` from ((((`t_purchase_order_header` `t0` join `t_purchase_order_detail` `t1` on((`t0`.`doc_id` = `t1`.`doc_id`))) join `t_inventory` `t2` on((`t2`.`item_id` = `t1`.`item_id`))) join `t_inventory_group` `t3` on((`t3`.`item_g_id` = `t2`.`item_g_id`))) join `t_site_location` `t4` on((`t0`.`whse_id` = `t4`.`location_id`))) where ((`t0`.`doc_status` <> 'PARTIAL') and (`t0`.`doc_status` <> 'RECEIVED') and (`t1`.`line_close` = 0)) group by `t4`.`location_id`,`t4`.`location_code`,`t4`.`location_name`,`t3`.`group_name`,`t2`.`item_id`,`t2`.`item_code`,`t2`.`item_name` having (sum((`t1`.`item_qty` - `t1`.`item_qty_closed`)) > 0);
DROP TABLE IF EXISTS `v_itemoutgoing`;

CREATE VIEW `v_itemoutgoing` AS select `t0`.`doc_id` AS `doc_id`,`t4`.`location_id` AS `whse_id`,`t4`.`location_code` AS `whse_code`,`t4`.`location_name` AS `whse_name`,`t3`.`group_name` AS `group_name`,`t2`.`item_id` AS `item_id`,`t2`.`item_code` AS `item_code`,`t2`.`item_name` AS `item_name`,sum((`t1`.`item_qty` - `t1`.`item_qty_closed`)) AS `qty_togo` from ((((`t_sales_order_header` `t0` join `t_sales_order_detail` `t1` on((`t0`.`doc_id` = `t1`.`doc_id`))) join `t_inventory` `t2` on((`t2`.`item_id` = `t1`.`item_id`))) join `t_inventory_group` `t3` on((`t3`.`item_g_id` = `t2`.`item_g_id`))) join `t_site_location` `t4` on((`t0`.`whse_id` = `t4`.`location_id`))) where ((`t0`.`doc_status` <> 'PARTIAL') and (`t0`.`doc_status` <> 'DELIVERED') and (`t1`.`line_close` = 0)) group by `t4`.`location_id`,`t4`.`location_code`,`t4`.`location_name`,`t3`.`group_name`,`t2`.`item_id`,`t2`.`item_code`,`t2`.`item_name` having (sum((`t1`.`item_qty` - `t1`.`item_qty_closed`)) > 0);
DROP TABLE IF EXISTS `v_journals`;

CREATE VIEW `v_journals` AS select `jh`.`journal_id` AS `journal_id`,`jh`.`journal_code` AS `journal_code`,`jh`.`posting_date` AS `posting_date`,`jh`.`journal_memo` AS `journal_memo`,`jh`.`journal_ref1` AS `reff`,`jh`.`journal_type` AS `journal_type`,`jd`.`account_id` AS `account_id`,`coa`.`account_number` AS `account_number`,`coa`.`account_name` AS `account_name`,`jd`.`journal_curr` AS `journal_curr`,`jd`.`journal_dr` AS `journal_dr`,`jd`.`journal_cr` AS `journal_cr` from ((`t_account_journal_header` `jh` join `t_account_journal_detail` `jd` on((`jd`.`journal_id` = `jh`.`journal_id`))) join `t_account` `coa` on((`coa`.`account_id` = `jd`.`account_id`))) order by `jh`.`posting_date` desc,`jh`.`journal_id` desc;
DROP TABLE IF EXISTS `v_position`;

CREATE VIEW `v_position` AS select `pos`.`position_id` AS `position_id`,`pos`.`position_code` AS `position_code`,`pos`.`position_name` AS `position_name`,`pos`.`position_desc` AS `position_desc`,`pos`.`position_type` AS `position_type`,`pos`.`parent_id` AS `parent_id`,`pos`.`last_node` AS `last_node`,`pos`.`order_no` AS `order_no`,`dvs`.`position_id` AS `division_id`,`dvs`.`position_code` AS `division_code`,`dvs`.`position_name` AS `division_name`,(select count(1) from `t_position` where (`t_position`.`parent_id` = `pos`.`position_id`)) AS `child_count`,(select count(1) from `t_user` where (`t_user`.`position_id` = `pos`.`position_id`)) AS `user_count` from (`t_position` `pos` left join `t_position` `dvs` on((`dvs`.`position_id` = `pos`.`parent_id`))) order by `pos`.`order_no`;
DROP TABLE IF EXISTS `v_pos_transactions`;

CREATE VIEW `v_pos_transactions` AS select `loc`.`location_id` AS `location_id`,`loc`.`location_code` AS `location_code`,`loc`.`location_name` AS `location_name`,`csr`.`cashier_id` AS `cashier_id`,`csr`.`cashier_code` AS `cashier_code`,`emp`.`emp_no` AS `emp_no`,concat(`usr`.`first_name`,' ',`usr`.`last_name`) AS `input_by`,`pos`.`trx_datetime` AS `input_date`,`pos`.`trx_code` AS `sales_code`,`pos`.`trx_sales_amount` AS `sales_amount`,`pos`.`posted` AS `posted` from ((((`t_sales_pos` `pos` left join `t_ion_users` `usr` on((`usr`.`id` = `pos`.`trx_user_id`))) left join `t_employee` `emp` on((`emp`.`user_id` = `usr`.`id`))) left join `t_site_cashier` `csr` on((`csr`.`cashier_id` = `pos`.`trx_cashier_id`))) left join `t_site_location` `loc` on((`loc`.`location_id` = `csr`.`location_id`)));
DROP TABLE IF EXISTS `v_pos_transactions_items`;

CREATE VIEW `v_pos_transactions_items` AS select `posi`.`id` AS `id`,`pos`.`trx_id` AS `trx_id`,`pos`.`trx_code` AS `trx_code`,`pos`.`trx_datetime` AS `input_datetime`,cast(`pos`.`trx_datetime` as date) AS `pos_date`,`csr`.`cashier_id` AS `cashier_id`,`csr`.`cashier_code` AS `cashier_code`,`loc`.`location_id` AS `location_id`,`loc`.`location_code` AS `location_code`,`loc`.`location_name` AS `location_name`,`posi`.`line_id` AS `line_id`,`i`.`item_id` AS `item_id`,`i`.`item_code` AS `item_code`,`i`.`item_name` AS `item_name`,`posi`.`item_qty` AS `item_qty`,(`posi`.`item_qty` * -(1)) AS `move_item_qty`,`posi`.`item_price` AS `item_price`,`posi`.`item_total_price` AS `item_total_price`,`posi`.`include_tax` AS `include_tax`,cast((case when (`posi`.`include_tax` = 1) then (`posi`.`item_total_price` - round((`posi`.`item_total_price` / 1.1),0)) else (`posi`.`item_total_price` * 0.1) end) as decimal(18,2)) AS `tax_value`,cast((case when (`posi`.`include_tax` = 1) then round((`posi`.`item_total_price` / 1.1),0) else `posi`.`item_total_price` end) as decimal(18,2)) AS `sales_value` from ((((`t_sales_pos_item` `posi` join `t_sales_pos` `pos` on((`pos`.`trx_id` = `posi`.`trx_id`))) join `t_site_cashier` `csr` on((`csr`.`cashier_id` = `pos`.`trx_cashier_id`))) left join `t_site_location` `loc` on((`loc`.`location_id` = `csr`.`location_id`))) left join `t_inventory` `i` on((`i`.`item_id` = `posi`.`item_id`)));
DROP TABLE IF EXISTS `v_purchase_order`;

CREATE VIEW `v_purchase_order` AS select `t0`.`doc_num` AS `doc_num`,`t0`.`doc_dt` AS `doc_dt`,`t0`.`doc_ddt` AS `doc_ddt`,`t0`.`doc_status` AS `doc_status`,`t0`.`approval_status` AS `approval_status`,`t0`.`doc_total` AS `doc_total`,`t1`.`buss_code` AS `buss_code`,`t1`.`buss_name` AS `buss_name`,`t1`.`buss_addr` AS `buss_addr`,`t1`.`buss_contact` AS `buss_contact`,`t2`.`location_code` AS `location_code`,`t2`.`location_name` AS `location_name`,`t2`.`location_address` AS `location_address` from ((`t_purchase_order_header` `t0` join `t_business` `t1` on((`t0`.`buss_id` = `t1`.`buss_id`))) join `t_site_location` `t2` on((`t2`.`location_id` = `t0`.`whse_id`)));
DROP TABLE IF EXISTS `v_purchase_order_detail`;

CREATE VIEW `v_purchase_order_detail` AS select `t0`.`doc_num` AS `doc_num`,`t0`.`doc_dt` AS `doc_dt`,`t0`.`doc_ddt` AS `doc_ddt`,`t0`.`doc_status` AS `doc_status`,`t0`.`approval_status` AS `approval_status`,`t0`.`doc_total` AS `doc_total`,`t1`.`item_qty` AS `item_qty`,`t1`.`item_qty_closed` AS `item_qty_closed`,`t1`.`item_price` AS `item_price`,`t1`.`item_netprice` AS `item_netprice`,`t1`.`item_total` AS `item_total`,`t1`.`line_close` AS `line_close`,`t2`.`buss_code` AS `buss_code`,`t2`.`buss_name` AS `buss_name`,`t2`.`buss_addr` AS `buss_addr`,`t2`.`buss_contact` AS `buss_contact`,`t3`.`location_code` AS `location_code`,`t3`.`location_name` AS `location_name`,`t3`.`location_address` AS `location_address`,`t4`.`item_code` AS `item_code`,`t4`.`item_name` AS `item_name` from ((((`t_purchase_order_header` `t0` join `t_purchase_order_detail` `t1` on((`t0`.`doc_id` = `t1`.`doc_id`))) join `t_business` `t2` on((`t0`.`buss_id` = `t2`.`buss_id`))) join `t_site_location` `t3` on((`t0`.`whse_id` = `t3`.`location_id`))) join `t_inventory` `t4` on((`t1`.`item_id` = `t4`.`item_id`)));
DROP TABLE IF EXISTS `v_sales_order`;

CREATE VIEW `v_sales_order` AS select `t0`.`doc_num` AS `doc_num`,`t0`.`doc_dt` AS `doc_dt`,`t0`.`doc_ddt` AS `doc_ddt`,`t0`.`doc_status` AS `doc_status`,`t0`.`approval_status` AS `approval_status`,`t0`.`doc_total` AS `doc_total`,`t1`.`buss_code` AS `buss_code`,`t1`.`buss_name` AS `buss_name`,`t1`.`buss_addr` AS `buss_addr`,`t1`.`buss_contact` AS `buss_contact`,`t2`.`location_code` AS `location_code`,`t2`.`location_name` AS `location_name`,`t2`.`location_address` AS `location_address` from ((`t_sales_order_header` `t0` join `t_business` `t1` on((`t0`.`buss_id` = `t1`.`buss_id`))) join `t_site_location` `t2` on((`t2`.`location_id` = `t0`.`whse_id`)));
DROP TABLE IF EXISTS `v_sales_order_detail`;

CREATE VIEW `v_sales_order_detail` AS select `t0`.`doc_num` AS `doc_num`,`t0`.`doc_dt` AS `doc_dt`,`t0`.`doc_ddt` AS `doc_ddt`,`t0`.`doc_status` AS `doc_status`,`t0`.`approval_status` AS `approval_status`,`t0`.`doc_total` AS `doc_total`,`t1`.`item_qty` AS `item_qty`,`t1`.`item_qty_closed` AS `item_qty_closed`,`t1`.`item_price` AS `item_price`,`t1`.`item_netprice` AS `item_netprice`,`t1`.`item_total` AS `item_total`,`t1`.`line_close` AS `line_close`,`t2`.`buss_code` AS `buss_code`,`t2`.`buss_name` AS `buss_name`,`t2`.`buss_addr` AS `buss_addr`,`t2`.`buss_contact` AS `buss_contact`,`t3`.`location_code` AS `location_code`,`t3`.`location_name` AS `location_name`,`t3`.`location_address` AS `location_address`,`t4`.`item_code` AS `item_code`,`t4`.`item_name` AS `item_name` from ((((`t_sales_order_header` `t0` join `t_sales_order_detail` `t1` on((`t0`.`doc_id` = `t1`.`doc_id`))) join `t_business` `t2` on((`t0`.`buss_id` = `t2`.`buss_id`))) join `t_site_location` `t3` on((`t0`.`whse_id` = `t3`.`location_id`))) join `t_inventory` `t4` on((`t1`.`item_id` = `t4`.`item_id`)));
DROP TABLE IF EXISTS `v_users`;

CREATE VIEW `v_users` AS select `emp`.`id` AS `emp_id`,`emp`.`emp_no` AS `emp_no`,`emp`.`first_name` AS `first_name`,`emp`.`last_name` AS `last_name`,concat(`emp`.`first_name`,' ',`emp`.`last_name`) AS `full_name`,`emp`.`location_id` AS `location_id`,`loc`.`location_name` AS `location_name`,`emp`.`position_id` AS `position_id`,`pos`.`position_name` AS `position_name`,`usr`.`id` AS `user_id`,`usr`.`username` AS `username`,`usr`.`email` AS `email` from (((`t_ion_users` `usr` left join `t_employee` `emp` on((`emp`.`user_id` = `usr`.`id`))) left join `t_position` `pos` on((`pos`.`position_id` = `emp`.`position_id`))) left join `t_site_location` `loc` on((`loc`.`location_id` = `emp`.`location_id`)));
DROP TABLE IF EXISTS `v_user_datatable`;

CREATE VIEW `v_user_datatable` AS select `a`.`user_id` AS `user_id`,`a`.`user_name` AS `user_name`,`a`.`user_email` AS `user_email`,`a`.`user_pass` AS `user_pass`,`a`.`user_date` AS `user_date`,`a`.`user_modified` AS `user_modified`,`a`.`user_last_login` AS `user_last_login`,`a`.`user_last_ip` AS `user_last_ip`,`a`.`user_location_id` AS `user_location_id`,`a`.`department` AS `department`,`a`.`full_name` AS `full_name`,`a`.`birthdate` AS `birthdate`,`a`.`gender` AS `gender`,`p`.`position_code` AS `position_code`,`p`.`position_name` AS `position_name`,`a`.`designation` AS `designation`,`a`.`phone_number` AS `phone_number`,`a`.`mobile_number` AS `mobile_number`,`a`.`extension_number` AS `extension_number`,`a`.`timestamp` AS `timestamp`,`b`.`location_id` AS `location_id`,`b`.`location_name` AS `location_name`,`b`.`location_address` AS `location_address`,`b`.`location_phone` AS `location_phone`,`b`.`location_city` AS `location_city`,`b`.`location_state` AS `location_state`,`b`.`location_country` AS `location_country`,`b`.`location_description` AS `location_description` from ((`t_user` `a` left join `t_site_location` `b` on((`a`.`user_location_id` = `b`.`location_id`))) left join `t_position` `p` on((`p`.`position_id` = `a`.`position_id`)));


ALTER TABLE `t_account`
  ADD PRIMARY KEY (`account_id`);

ALTER TABLE `t_account_journal_detail`
  ADD PRIMARY KEY (`journal_id`,`journal_line`);

ALTER TABLE `t_account_journal_header`
  ADD PRIMARY KEY (`journal_id`),
  ADD UNIQUE KEY `journal_code_UNIQUE` (`journal_code`);

ALTER TABLE `t_account_journal_template_detail`
  ADD PRIMARY KEY (`id`,`line`);

ALTER TABLE `t_account_journal_template_header`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_UNIQUE` (`code`);

ALTER TABLE `t_account_trans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `trans_code_UNIQUE` (`trans_code`);

ALTER TABLE `t_approval`
  ADD PRIMARY KEY (`appr_id`),
  ADD UNIQUE KEY `appr_code_UNIQUE` (`appr_code`);

ALTER TABLE `t_approval_detail`
  ADD PRIMARY KEY (`appr_detail_id`);

ALTER TABLE `t_approval_setting`
  ADD PRIMARY KEY (`appr_setting_id`),
  ADD UNIQUE KEY `UQ_APPR_ID_USER_ID` (`appr_id`,`requestor_user_id`);

ALTER TABLE `t_asset`
  ADD PRIMARY KEY (`asset_id`),
  ADD UNIQUE KEY `asset_id` (`asset_id`),
  ADD UNIQUE KEY `asset_serial` (`asset_label`);

ALTER TABLE `t_asset_group`
  ADD PRIMARY KEY (`group_id`);

ALTER TABLE `t_asset_request_header`
  ADD PRIMARY KEY (`request_id`);

ALTER TABLE `t_asset_trans`
  ADD PRIMARY KEY (`request_id`);

ALTER TABLE `t_business`
  ADD PRIMARY KEY (`buss_id`);

ALTER TABLE `t_business_contact`
  ADD PRIMARY KEY (`contact_id`);

ALTER TABLE `t_ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

ALTER TABLE `t_comment_detail`
  ADD PRIMARY KEY (`comment_id`);

ALTER TABLE `t_comment_header`
  ADD PRIMARY KEY (`post_id`);

ALTER TABLE `t_company`
  ADD PRIMARY KEY (`comp_id`);

ALTER TABLE `t_costcenter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cc_code_UNIQUE` (`cc_code`);

ALTER TABLE `t_credit_voucher`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `voucher_code_UNIQUE` (`voucher_code`);

ALTER TABLE `t_credit_voucher_detail`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `voucher_number_UNIQUE` (`voucher_number`);

ALTER TABLE `t_currency`
  ADD PRIMARY KEY (`curr_code`);

ALTER TABLE `t_currency_rate`
  ADD PRIMARY KEY (`curr_from`,`curr_to`);

ALTER TABLE `t_document`
  ADD PRIMARY KEY (`document_id`),
  ADD UNIQUE KEY `document_code_UNIQUE` (`document_code`);

ALTER TABLE `t_document_approval`
  ADD PRIMARY KEY (`doc_approval_id`);

ALTER TABLE `t_document_flow`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `t_document_series`
  ADD PRIMARY KEY (`series_id`);

ALTER TABLE `t_email_queue`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `t_employee`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `t_expense`
  ADD PRIMARY KEY (`exp_id`);

ALTER TABLE `t_inventory`
  ADD PRIMARY KEY (`item_id`),
  ADD UNIQUE KEY `item_code_UNIQUE` (`item_code`);

ALTER TABLE `t_inventory_category`
  ADD PRIMARY KEY (`category_id`);

ALTER TABLE `t_inventory_group`
  ADD PRIMARY KEY (`item_g_id`);

ALTER TABLE `t_inventory_image`
  ADD PRIMARY KEY (`item_id`);

ALTER TABLE `t_inventory_style`
  ADD PRIMARY KEY (`style_id`),
  ADD UNIQUE KEY `style_code_UNIQUE` (`style_code`);

ALTER TABLE `t_inventory_transaction`
  ADD PRIMARY KEY (`trans_id`);

ALTER TABLE `t_inventory_warehouse`
  ADD PRIMARY KEY (`item_id`,`location_id`);

ALTER TABLE `t_ion_groups`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `t_ion_login_attempts`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `t_ion_users`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `t_ion_users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

ALTER TABLE `t_link_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_UNIQUE` (`code`);

ALTER TABLE `t_maint_order`
  ADD PRIMARY KEY (`maint_order_id`);

ALTER TABLE `t_maint_schedule`
  ADD PRIMARY KEY (`schedule_id`);

ALTER TABLE `t_maint_type`
  ADD PRIMARY KEY (`maint_type_id`),
  ADD UNIQUE KEY `maint_type_code` (`maint_type_code`);

ALTER TABLE `t_payment_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_payment_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_payment_mean`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_position`
  ADD PRIMARY KEY (`position_id`);

ALTER TABLE `t_purchase_contract_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_purchase_contract_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_purchase_invoice_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_purchase_invoice_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_purchase_order_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_purchase_order_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_purchase_quotation_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_purchase_quotation_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_purchase_receipt_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_purchase_receipt_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_purchase_request_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_purchase_request_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_purchase_return_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_purchase_return_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_report`
  ADD PRIMARY KEY (`report_id`);

ALTER TABLE `t_report_config`
  ADD PRIMARY KEY (`report_id`,`config_id`);

ALTER TABLE `t_sales_delivery_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_sales_delivery_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_sales_invoice_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_sales_invoice_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_sales_order_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_sales_order_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_sales_pos`
  ADD PRIMARY KEY (`trx_id`),
  ADD UNIQUE KEY `trx_code_UNIQUE` (`trx_code`);

ALTER TABLE `t_sales_pos_item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `TRX_LINE_UQ` (`trx_id`,`line_id`);

ALTER TABLE `t_sales_pos_payment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `TRX_LINE_UQ_PAYMENT` (`trx_id`,`line_id`);

ALTER TABLE `t_side_menu`
  ADD PRIMARY KEY (`menu_id`);

ALTER TABLE `t_site_cashier`
  ADD PRIMARY KEY (`cashier_id`);

ALTER TABLE `t_site_location`
  ADD PRIMARY KEY (`location_id`);

ALTER TABLE `t_stock_adjust_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_stock_adjust_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_stock_moving_detail`
  ADD PRIMARY KEY (`doc_id`,`doc_line`);

ALTER TABLE `t_stock_moving_header`
  ADD PRIMARY KEY (`doc_id`);

ALTER TABLE `t_tax`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tax_code_UNIQUE` (`tax_code`);

ALTER TABLE `t_uom`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `t_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`);

ALTER TABLE `t_user_group`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `group_name_UNIQUE` (`group_name`);

ALTER TABLE `t_user_group_junc`
  ADD PRIMARY KEY (`user_id`,`group_id`);


ALTER TABLE `t_account`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=74;
ALTER TABLE `t_account_journal_header`
  MODIFY `journal_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `t_account_journal_template_header`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_account_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `t_approval`
  MODIFY `appr_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
ALTER TABLE `t_approval_detail`
  MODIFY `appr_detail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=84;
ALTER TABLE `t_approval_setting`
  MODIFY `appr_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `t_asset`
  MODIFY `asset_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
ALTER TABLE `t_asset_group`
  MODIFY `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=275;
ALTER TABLE `t_asset_request_header`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
ALTER TABLE `t_asset_trans`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
ALTER TABLE `t_business`
  MODIFY `buss_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
ALTER TABLE `t_business_contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_comment_detail`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
ALTER TABLE `t_comment_header`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_company`
  MODIFY `comp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Skyware Company Detail',AUTO_INCREMENT=4;
ALTER TABLE `t_costcenter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
ALTER TABLE `t_credit_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
ALTER TABLE `t_credit_voucher_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=177;
ALTER TABLE `t_document`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
ALTER TABLE `t_document_approval`
  MODIFY `doc_approval_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
ALTER TABLE `t_document_flow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
ALTER TABLE `t_document_series`
  MODIFY `series_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
ALTER TABLE `t_email_queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
ALTER TABLE `t_expense`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_inventory`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Skyware Item Master',AUTO_INCREMENT=24;
ALTER TABLE `t_inventory_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
ALTER TABLE `t_inventory_group`
  MODIFY `item_g_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
ALTER TABLE `t_inventory_style`
  MODIFY `style_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
ALTER TABLE `t_inventory_transaction`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
ALTER TABLE `t_ion_groups`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
ALTER TABLE `t_ion_login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_ion_users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
ALTER TABLE `t_ion_users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
ALTER TABLE `t_link_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
ALTER TABLE `t_maint_order`
  MODIFY `maint_order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `t_maint_schedule`
  MODIFY `schedule_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
ALTER TABLE `t_maint_type`
  MODIFY `maint_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `t_payment_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
ALTER TABLE `t_position`
  MODIFY `position_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
ALTER TABLE `t_purchase_contract_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
ALTER TABLE `t_purchase_invoice_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
ALTER TABLE `t_purchase_order_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
ALTER TABLE `t_purchase_quotation_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_purchase_receipt_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
ALTER TABLE `t_purchase_request_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
ALTER TABLE `t_purchase_return_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_report`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
ALTER TABLE `t_sales_delivery_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_sales_invoice_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_sales_order_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `t_sales_pos`
  MODIFY `trx_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
ALTER TABLE `t_sales_pos_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
ALTER TABLE `t_sales_pos_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
ALTER TABLE `t_side_menu`
  MODIFY `menu_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=113;
ALTER TABLE `t_site_cashier`
  MODIFY `cashier_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `t_site_location`
  MODIFY `location_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
ALTER TABLE `t_stock_adjust_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
ALTER TABLE `t_stock_moving_header`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
ALTER TABLE `t_tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
ALTER TABLE `t_uom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
ALTER TABLE `t_user`
  MODIFY `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
ALTER TABLE `t_user_group`
  MODIFY `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

ALTER TABLE `t_ion_users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `t_ion_groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `t_ion_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
