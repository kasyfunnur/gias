ALTER TABLE `t_inventory` 
ADD COLUMN `acc_inventory` INT NULL AFTER `item_status`,
ADD COLUMN `acc_expense` INT NULL AFTER `acc_inventory`,
ADD COLUMN `acc_revenue` INT NULL AFTER `acc_expense`,
ADD COLUMN `acc_cogs` INT NULL AFTER `acc_revenue`,
ADD COLUMN `acc_allocation` INT NULL AFTER `acc_cogs`;
