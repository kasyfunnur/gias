-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: zadmin_erpdb
-- ------------------------------------------------------
-- Server version	5.6.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_stock_conv`
--

DROP TABLE IF EXISTS `t_stock_conv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_stock_conv` (
  `doc_no` varchar(25) NOT NULL,
  `doc_date` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL,
  `updated_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`doc_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_stock_conv`
--

LOCK TABLES `t_stock_conv` WRITE;
/*!40000 ALTER TABLE `t_stock_conv` DISABLE KEYS */;
INSERT INTO `t_stock_conv` VALUES ('SC001','2016-01-01 11:24:00','admin','2016-01-01 11:24:00',NULL,NULL),('SC002','2016-01-02 08:30:00','admin','2016-01-02 08:30:00',NULL,NULL);
/*!40000 ALTER TABLE `t_stock_conv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_stock_conv_detail`
--

DROP TABLE IF EXISTS `t_stock_conv_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_stock_conv_detail` (
  `doc_no` varchar(25) DEFAULT NULL,
  `outgoing` int(11) DEFAULT NULL,
  `incoming` int(11) DEFAULT NULL,
  `blend` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_stock_conv_detail`
--

LOCK TABLES `t_stock_conv_detail` WRITE;
/*!40000 ALTER TABLE `t_stock_conv_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_stock_conv_detail` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-03 16:02:43
